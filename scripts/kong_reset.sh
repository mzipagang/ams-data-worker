curl -X DELETE http://localhost:8001/apis/AMS_API
curl -X DELETE http://localhost:8001/apis/AMS_EMAIL
curl -X DELETE http://localhost:8001/apis/AMS_FILE_IMPORT_GROUP
curl -X DELETE http://localhost:8001/apis/AMS_UPLOAD

curl -X POST \
  http://localhost:8001/apis \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "name": "AMS_API",
    "uris": "/api",
    "upstream_url": "http://host.docker.internal:7000",
    "strip_uri": false,
    "preserve_host" : true
}'

curl -X POST \
  http://localhost:8001/apis \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "name": "AMS_EMAIL",
    "methods": "POST",
    "uris": "/api/v1/email",
    "upstream_url": "http://host.docker.internal:5000",
    "strip_uri": false,
    "preserve_host" : true
}'

curl -X POST \
  http://localhost:8001/apis \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "name": "AMS_FILE_IMPORT_GROUP",
    "methods": "PATCH",
    "uris": "/api/v1/file_import_group/([^/]+)$",
    "upstream_url": "http://host.docker.internal:5000",
    "strip_uri": false,
    "preserve_host" : true
}'

curl -X POST \
  http://localhost:8001/apis \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "name": "AMS_UPLOAD",
    "methods": "POST",
    "uris": "/api/v1/file_import_group/([^/]+)/upload$",
    "upstream_url": "http://host.docker.internal:5000",
    "strip_uri": false,
    "preserve_host" : true
}'

curl -X POST \
  http://localhost:8001/apis \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "name": "AMS_LOGIN",
    "methods": "POST",
    "uris": "/api/v1/user/login",
    "upstream_url": "http://host.docker.internal:5000",
    "strip_uri": false,
    "preserve_host" : true
}'
