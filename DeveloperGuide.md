# Developer Guide - Data Worker

Getting Started
---------

Prerequisites:
 - Node.js = 8.9, It is recommended that you use [NVM](https://github.com/creationix/nvm).
 - [Git](https://git-scm.com/)
 - MongoDB >= 3.4.
 - Docker
 - Docker Compose

Clone the codebase and initialize it by running the following commands:
```shell
$ git clone https://github.cms.gov/CMMI/AMS-data-worker
$ cd AMS-data-worker
$ npm install
```

#### For development

Start up the app
```shell
npm run start:dev
```
Note: `start:dev` uses nodemon to automatically restart your node service when files change.

#### For production
```shell
export PORT=4000
export NODE_ENV=production
npm run start
```


Environment variables
---------

Configuration of the app is based entirely on environment variables. The app does not store any variables for any environment at all. All configuration from environment variables is applied in `./app/config/index.js`.

| Environment Variable  | Default                                   | Options                                             |
| --------------------- | ----------------------------------------- | --------------------------------------------------- |
| NODE_ENV              | `"dev"`                                   | `production`, `dev`, or any other environment name  |
| PORT                  | `7000`                                    | any valid port number                               |


Tests
---------
```
npm run test
```
AMS-api is using Mocha to run tests. The test pattern is `test/**/*.spec.js` and is using Mocha's `bdd` interface. Feel free to use any Mocha runner to run the tests.


Lint
---------
```
npm run lint
```
The linting rules are based on Airbnb's Javascript Style Guide which can be found here: https://github.com/airbnb/javascript. Please run the linter before each commit.


Code Coverage
---------
```
npm run coverage:dev
```
`npm run coverage:dev` will automatically open the generated HTML file in your browser. For CI, run `npm run coverage` to generate but not open. The generated docs will be available at `./coverage/lcov-report/index.html`.


Swagger UI
---------
Coming Soon - Currently accessible json at ./api/v1/swagger.json
A swagger ui will be added soon as well.


Additional Notes
---------

#### Accessing GitHub w/ 2FA
Because CMS GitHub requires 2FA, you will need to checkout the repository using HTTPS, your username, and a Personal Access Token. Go to Settings -> Personal Access Tokens to create a Personal Access Token with "repo" access. Use this password, along with your GitHub username to access the repo. [More Info](https://help.github.com/articles/providing-your-2fa-authentication-code/#when-youll-be-asked-for-a-personal-access-token-as-a-password)
