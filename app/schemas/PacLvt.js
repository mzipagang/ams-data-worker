const Joi = require('joi');
const PerformanceYear = require('./PerformanceYear').Schema;
const ComplexPatientScore = require('./ComplexPatientScore').Schema;
const Execution = require('./Execution').Schema;
const Run = require('./Run').Schema;

const Schema = Joi.object({
  entity_id: Joi.string().example('A1234'),
  status: Joi.string().example('Y'),
  patients: Joi.number().example(10000.00),
  payments: Joi.number().example(12345),
  small_status: Joi.string().example('Y').allow(null),
  complex_patient_score: ComplexPatientScore.allow(null),
  performance_year: PerformanceYear,
  run: Run,
  execution: Execution
}).label('PacLVT');

module.exports = {
  Schema
};
