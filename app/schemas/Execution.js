const Joi = require('joi');

const Schema = Joi.number().integer().min(1).max(9)
  .example(1)
  .label('ExecutionEnum');

module.exports = {
  Schema
};
