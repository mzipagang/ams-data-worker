const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const YesNoPartial = require('./YesNoPartial').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const PatchSchema = Joi.object({
  id: Joi.string(),
  apm_id: Joi.string().example('A1001'),
  name: Joi.string(),
  short_name: Joi.string().allow(null, ''),
  start_date: Joi.date().allow(null),
  end_date: Joi.date().allow(null),
  advanced_apm_flag: YesNoPartial,
  mips_apm_flag: YesNoPartial,
  additional_information: Joi.string().allow(null, ''),
  updated_by: Joi.string().allow(null, ''),
  updated_date: Joi.date().allow(null),
  performance_year: PerformanceYear,
  snapshot: Snapshot,
  execution: Execution
}).label('PatchPacSubdivision');

const Schema = PatchSchema.requiredKeys(
  'id'
).label('PacSubdivision');

module.exports = {
  Schema,
  PatchSchema
};
