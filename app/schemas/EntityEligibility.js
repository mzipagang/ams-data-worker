const Joi = require('joi');
const PerformanceYear = require('./PerformanceYear').Schema;
const ComplexPatientScore = require('./ComplexPatientScore').Schema;
const Execution = require('./Execution').Schema;
const Run = require('./Run').Schema;

const Schema = Joi.object({
  entity_id: Joi.string().required(),
  claims_types: Joi.string().required(),
  lv_status_code: Joi.string().required(),
  lv_status_reason: Joi.string().allow(''),
  lv_part_b_expenditures: Joi.number().required(),
  lv_bene_count: Joi.number().integer().required(),
  small_status_apm_entity_code: Joi.string(),
  small_status_clinician_count: Joi.number().integer(),
  performance_year: PerformanceYear,
  run: Run,
  complex_patient_score: ComplexPatientScore.required(),
  execution: Execution
}).label('EntityEligibility');

module.exports = {
  Schema
};
