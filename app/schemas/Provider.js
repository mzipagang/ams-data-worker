const Joi = require('joi');

const PatchSchema = Joi.object({
  entity_id: Joi.string(),
  start_date: Joi.date(),
  end_date: Joi.date(),
  tin: Joi.string(),
  npi: Joi.string(),
  ccn: Joi.string(),
  tin_type_code: Joi.string(),
  specialty_code: Joi.string(),
  organization_name: Joi.string(),
  first_name: Joi.string(),
  middle_name: Joi.string(),
  last_name: Joi.string(),
  participant_type_code: Joi.string()
}).label('PatchProvider');

const Schema = PatchSchema.requiredKeys(
  'entity_id',
  'start_date',
  'end_date',
  'tin'
).label('Provider');

const MapFieldNameToErrorRule = {
  start_date: 'start_date_required',
  end_date: 'end_date_required'
};

module.exports = {
  Schema,
  PatchSchema,
  MapFieldNameToErrorRule
};
