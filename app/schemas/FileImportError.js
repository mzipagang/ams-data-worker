const Joi = require('joi');

const PatchSchema = Joi.object({
  file_import_group_id: Joi.string(),
  file_id: Joi.string(),
  error: Joi.string(),
  line_number: Joi.number().integer(),
  createdAt: Joi.date()
}).label('PatchFileImportError');

const Schema = PatchSchema.requiredKeys(
  'file_import_group_id',
  'file_id',
  'error',
  'line_number'
).label('FileImportError');

module.exports = {
  Schema,
  PatchSchema
};
