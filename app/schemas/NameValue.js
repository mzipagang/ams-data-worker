const Joi = require('joi');

const Schema = Joi.object({
  value: Joi.string(), // TODO: .required() breaks with our seed data
  text: Joi.string()
});

module.exports = {
  Schema
};
