const Joi = require('joi');
const RunSnaphot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;

const ChildECCountReport = Joi.object({
  apm_id: Joi.string().example('08').description('The APM id'),
  subdiv_id: Joi.string().example('01').description('The APM subdivsion id'),
  apm_name: Joi.string().example('Medicare Shared Savings Program Accountable Care Organizations')
    .description('The APM name'),
  subdiv_name: Joi.string().example('MSSP ACO - Track 1').description('The APM subdivsion name'),
  unique_ec_count: Joi.number().integer().description('The sum of unique TIN-NPI combinations')
});

const ECCountReportSchema = Joi.object({
  apms: Joi.array().items(ChildECCountReport).required().description('The apms from the mongo model collection'),
  totalec: Joi.number().integer(),
  mipsECbyAPM: Joi.array().items(ChildECCountReport).required().description('ECs in MIPS APM\'s'),
  mipsECbySubdivision: Joi.array().items(ChildECCountReport).required()
    .description('ECs in MIPS APM\'s by subdivision'),
  nonMipsECbyAPM: Joi.array().items(ChildECCountReport).required().description('ECs in non-MIPS APM\'s'),
  nonMipsECbySubdivision: Joi.array().items(ChildECCountReport).required()
    .description('ECs in non-MIPS APM\'s by subdivision')
});

const ChildQPMetricsReportSchema = Joi.object({
  apm_id: Joi.string().example('08').description('The APM id'),
  model_name: Joi.string().example('Medicare Shared Savings Program Accountable Care Organizations')
    .description('The APM name'),
  subdivision_name: Joi.string().example('MSSP ACO - Track 1').description('The APM subdivsion name'),
  subdiv_id: Joi.string().example('01').description('The APM subdivsion id'),
  total_qp: Joi.number().integer().required().description('The total number of qp for that apm that are Y'),
  total_partial_qp: Joi.number().integer().description('The total number of qp for that apm that are P'),
  total_not_qp: Joi.number().integer().description('The total number of qp for that apm that are N')
});

const QPMetricsReportSchema = Joi.array().items(ChildQPMetricsReportSchema);

const PastReportSchema = Joi.object({
  performance_year: PerformanceYear.required(),
  run_snapshot: RunSnaphot.required(),
  date: Joi.date().required().default(Date, 'Now'),
  ec_count: ECCountReportSchema,
  qp_metrics: QPMetricsReportSchema
});

module.exports = {
  ECCountReportSchema,
  QPMetricsReportSchema,
  PastReportSchema
};
