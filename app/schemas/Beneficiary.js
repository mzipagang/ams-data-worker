const Joi = require('joi');

const PatchSchema = Joi.object({
  program_id: Joi.string(),
  entity_id: Joi.string(),
  link_key: Joi.string(),
  program_org_id: Joi.string(),
  id: Joi.string(),
  hicn: Joi.string(),
  first_name: Joi.string(),
  middle_name: Joi.string(),
  last_name: Joi.string(),
  dob: Joi.date(),
  gender: Joi.string(),
  start_date: Joi.date(),
  end_date: Joi.date(),
  category_code: Joi.string(),
  org_payment_track: Joi.string(),
  org_payment_track_start_date: Joi.string(),
  org_payment_track_end_date: Joi.string(),
  voluntary_alignment: Joi.string(),
  other_id: Joi.string(),
  other_id_type: Joi.string(),
  mbi: Joi.string()
}).label('PatchBeneficiary');

const Schema = PatchSchema.requiredKeys(
  'start_date',
  'end_date'
).label('Beneficiary');

const MapFieldNameToErrorRule = {
  start_date: 'start_date_required',
  end_date: 'end_date_required'
};

module.exports = {
  Schema,
  PatchSchema,
  MapFieldNameToErrorRule
};
