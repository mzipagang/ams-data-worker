const Joi = require('joi');

const Schema = Joi.object({
  address_1: Joi.string(),
  address_2: Joi.string(),
  city: Joi.string(),
  zip4: Joi.string().allow(''),
  zip5: Joi.string(),
  state: Joi.string()
}).label('Address');

module.exports = {
  Schema
};
