const Joi = require('joi');

const Schema = Joi.number().integer().min(2017).example(2017)
  .description('Performance Year')
  .label('PerformanceYear');

module.exports = {
  Schema
};
