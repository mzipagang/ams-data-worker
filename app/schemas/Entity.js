const Joi = require('joi');
const Address = require('./Address').Schema;

const PatchSchema = Joi.object({
  apm_id: Joi.string(),
  subdiv_id: Joi.string(),
  id: Joi.string(),
  start_date: Joi.date(),
  end_date: Joi.date(),
  name: Joi.string(),
  dba: Joi.string(),
  type: Joi.string().allow(''),
  tin: Joi.string(),
  npi: Joi.string(),
  ccn: Joi.string().allow(''),
  additional_information: Joi.string().allow(''),
  address: Address
}).label('PatchEntity');

const Schema = PatchSchema.requiredKeys(
  'apm_id',
  'id',
  'start_date',
  'end_date',
  'name'
).label('Entity');

const MapFieldNameToErrorRule = {
  start_date: 'start_date_required',
  end_date: 'end_date_required'
};

module.exports = {
  Schema,
  PatchSchema,
  MapFieldNameToErrorRule
};
