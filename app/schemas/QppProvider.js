const PacModel = require('./PacModel').Schema;
const PacSubdivision = require('./PacSubdivision').Schema;
const PacEntity = require('./PacEntity').Schema;
const PacProvider = require('./PacProvider').Schema;
const PacLvt = require('./PacLvt').Schema;
const PacQpStatus = require('./PacQpStatus').Schema;
const PacQpScore = require('./PacQpScore').Schema;
const PacQpcScore = require('./PacQualityPerformanceCategoryScoreSummary').Schema;

const Schema = PacProvider.keys({
  qpStatus: PacQpStatus,
  entity: PacEntity,
  LVT: PacLvt,
  model: PacModel,
  qpScore: PacQpScore,
  qpcScore: PacQpcScore,
  subdivision: PacSubdivision
}).label('QppProvider');

module.exports = {
  Schema
};

