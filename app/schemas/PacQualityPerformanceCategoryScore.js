const Joi = require('joi');
const YesNo = require('./YesNo').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;
const Run = require('./Run').Schema;

const Schema = Joi.object({
  entity_id: Joi.string(),
  measure_id: Joi.string(),
  measure_score: Joi.number(),
  decile_score: Joi.number().max(10.0),
  meets_scoring_criteria: YesNo,
  high_priority_measure: YesNo,
  cehrt_measure: YesNo,
  run: Run,
  execution: Execution,
  file_id: Joi.string(),
  performance_year: PerformanceYear,
  meta: Joi.any()
});

module.exports = {
  Schema
};
