const Joi = require('joi');

const values = {
  P: 'Predictive',
  I: 'Initial',
  S: 'Second',
  F: 'Final'
};

const Schema = Joi.string().valid(Object.keys(values)).label('ReportTypeEnum');

module.exports = {
  Schema
};
