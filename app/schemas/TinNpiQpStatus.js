const Joi = require('joi');
const ReportType = require('./ReportType').Schema;
const YesNoPartial = require('./YesNoPartial').Schema;

const Schema = Joi.object({
  apm_patient_count_denominator: Joi.number().integer().required(),
  apm_patient_count_numerator: Joi.number().integer().required(),
  apm_patient_threshold_met: YesNoPartial.required(),
  apm_patient_threshold_score: Joi.number().required(),
  apm_payment_denominator: Joi.number().required(),
  apm_payment_numerator: Joi.number().required(),
  apm_payment_threshold_met: YesNoPartial.required(),
  npi: Joi.string().required(),
  entity_id: Joi.string().required(),
  apm_id: Joi.string().required(),
  qp_reporting_period: ReportType.required(),
  qp_status: YesNoPartial.required(),
  qp_year: Joi.string().required(),
  run_date: Joi.date().required(),
  run_number: Joi.string().valid('P', 'I', 'S', 'F', '1', '2', '3', '4').required(),
  subdiv: Joi.string().required(),
  tin: Joi.string().required()
}).label('TinNpiQpStatusEntity');

module.exports = {
  Schema
};
