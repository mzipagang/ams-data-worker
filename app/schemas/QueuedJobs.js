const Joi = require('joi');

const UserVisibleFields = {
  id: Joi.string().required(),
  status: Joi.string().valid('queued', 'running', 'error', 'complete-error', 'complete-success').required()
};

const UserVisibleSchema = Joi.object(UserVisibleFields);

const PatchSchema = Joi.object({
  ...UserVisibleFields,
  type: Joi.string().required(),
  parameters: Joi.any().allow(null),
  results: Joi.object({
    signedUrlConfirmationId: Joi.string().allow(null)
  }),
  resultsError: Joi.object({
    code: Joi.string().allow(null),
    message: Joi.string().allow(null)
  }),
  details: Joi.object({ // keep details hidden from client
    submittedOn: Joi.date(),
    lastUpdatedOn: Joi.date(),
    updatedBy: Joi.string().allow(null),
    sqsJobRequestId: Joi.string(),
    s3Bucket: Joi.string(),
    s3FileName: Joi.string()
  })
}).label('PatchQueuedJobs');

const Schema = PatchSchema.requiredKeys(
  'id',
  'type',
  'status'
).label('QueuedJobs');

module.exports = {
  Schema,
  PatchSchema,
  UserVisibleSchema
};
