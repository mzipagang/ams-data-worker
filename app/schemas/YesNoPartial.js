const Joi = require('joi');

const values = {
  N: 'No',
  Y: 'Yes',
  P: 'Partial'
};

const Schema = Joi.string().valid(Object.keys(values)).default('N').label('YesNoPartialEnum');

module.exports = {
  Schema
};
