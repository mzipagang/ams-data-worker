const Joi = require('joi');
const ReportType = require('./ReportType').Schema;
const YesNoPartial = require('./YesNoPartial').Schema;

const Schema = Joi.object({
  run_date: Joi.date().required(),
  qp_year: Joi.string().required(),
  qp_reporting_period: ReportType.required(),
  run_number: Joi.string().valid('P', 'I', 'S', 'F', '1', '2', '3', '4').required(),
  npi: Joi.string().required(),
  qp_status: YesNoPartial.required()
}).label('NpiQpStatus');

module.exports = {
  Schema
};
