const Joi = require('joi');
const YesNoPartial = require('./YesNoPartial').Schema;

const PatchSchema = Joi.object({
  id: Joi.string(),
  apm_id: Joi.string(),
  name: Joi.string(),
  short_name: Joi.string().allow(null, ''),
  start_date: Joi.date().allow(null),
  end_date: Joi.date().allow(null),
  advanced_apm_flag: YesNoPartial,
  mips_apm_flag: YesNoPartial,
  additional_information: Joi.string().allow(null, ''),
  updated_by: Joi.string().allow(null, ''),
  updated_date: Joi.date().allow(null),
  user: Joi.object({
    username: Joi.string()
  })
}).label('PatchSubdivision');

const Schema = PatchSchema.requiredKeys(
  'id',
  'apm_id',
  'name'
).label('Provider');

module.exports = {
  Schema,
  PatchSchema
};
