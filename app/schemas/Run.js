const Joi = require('joi');

const Schema = Joi.number().integer().min(1).max(2)
  .example(1)
  .label('Run');

module.exports = {
  Schema
};
