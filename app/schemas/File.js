const uuid = require('uuid');
const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const FileTypes = [
  'pac-model', 'pac-subdivision', 'pac-entity', 'pac-provider', 'pac-qp_status', 'pac-qp_score', 'pac-lvt'
];

const Statuses = {
  CLOSED: 'closed',
  OPEN: 'open',
  PROCESSING: 'processing',
  PUBLISH: 'publish',
  PUBLISHED: 'published',
  PUBLISHING: 'publishing'
};

const ValidationErrorSchema = Joi.object({
  error: Joi.string().example('A2'),
  rowsAffected: Joi.number().integer().example(1337)
});

const ValidationSummarySchema = Joi.object({
  totalRecords: Joi.number().integer(),
  totalErrorCount: Joi.number().integer(),
  validationErrors: Joi.object({
    HARD_VALIDATION: Joi.array().items(ValidationErrorSchema),
    SOFT_VALIDATION: Joi.array().items(ValidationErrorSchema)
  }),
  warningRecordCount: Joi.number().integer(),
  errorRecordCount: Joi.number().integer(),
  validRecordCount: Joi.number().integer()
});

const Schema = Joi.object({
  id: Joi.string().default(uuid.v4, 'UUID').example('1111-1111-1111-1111'),
  file_name: Joi.string().example('pac-model.txt'),
  file_location: Joi.string().example('ams-dev-upoads/0000-0000-0000-0000/1111-1111-1111-1111/pac-model.txt'),
  status: Joi.string().valid(Object.keys(Statuses).map(key => Statuses[key])).example('pending'),
  error: Joi.string().default(null),
  apm_id: Joi.string().default(null),
  import_file_type: Joi.string().valid(...FileTypes).example('pac-model'),
  createdAt: Joi.date().default(Date, 'Now').example('2017-07-25T19:25:18.437Z'),
  updatedAt: Joi.date().default(Date, 'Now').example('2017-07-25T19:25:18.437Z'),
  uploadedBy: Joi.string().default(null),
  publishedAt: Joi.date(),
  performance_year: PerformanceYear,
  execution: Execution,
  run_snapshot: Snapshot,
  snapshot: Snapshot,
  run_number: Joi.number().integer(),
  validations: ValidationSummarySchema,
  fileValidations: Joi.array().items(Joi.any()),
  first_detail_row: Joi.number().integer(),
  last_detail_row: Joi.number().integer()
});

module.exports = {
  FileTypes,
  Schema,
  Statuses
};
