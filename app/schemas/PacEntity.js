const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const PatchSchema = Joi.object({
  id: Joi.string().example('A1001'),
  apm_id: Joi.string().example('A1001'),
  subdiv_id: Joi.string().example('01'),
  start_date: Joi.date(),
  end_date: Joi.date(),
  tin: Joi.string().allow('', null).example('123456789'),
  name: Joi.string().example('Palm Beach Accountable Care Organization, LLC'),
  performance_year: PerformanceYear,
  snapshot: Snapshot,
  execution: Execution
}).label('PatchPacEntity');

const Schema = PatchSchema.requiredKeys(
  'id'
).label('PacEntity');

module.exports = {
  Schema,
  PatchSchema
};
