const Joi = require('joi');
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const Schema = Joi.object({
  npi: Joi.string().required(),
  qp_status: Joi.string(),
  performance_year: PerformanceYear,
  snapshot: Joi.string().valid('1', '2', '3', '4').example('1'),
  execution: Execution
});

module.exports = {
  Schema
};
