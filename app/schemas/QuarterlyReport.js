const Joi = require('joi');
const NameValue = require('./NameValue').Schema;
const uuid = require('uuid');

const PatchSchema = Joi.object({
  id: Joi.string().default(uuid.v4, 'UUID').example('6bf148a7-9950-4589-8584-93b751a1e813')
    .description('unique quarterly report form id'),
  model_id: Joi.string().example('01').description('apm model id'),
  model_name: Joi.string().example('Healthy Model').description('the name of the model'),
  model_nickname: Joi.string().allow(null, ''),
  group: Joi.array().items(
    Joi.string().allow(null, '').example('ABC').description('the cmmi program group that administers the model')
  ),
  sub_group: Joi.string().allow(null, '').example('Healthy Subgroup')
    .description('the name of the subgroup, if participating in one'),
  team_lead: Joi.string().allow(null, '').example('Test User')
    .description('person in charge of the Model administration'),
  statutory_authority: Joi.string().allow(null, '').example('ACA Sec. 12345'),
  waivers: Joi.array().items(
    NameValue.example('3 Day Hospital Stay Requirement')
      .description('waiver associated with administration of the Model')
  ),
  announcement_date: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
    .description('date the Model is announced to the public'),
  award_date: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
    .description('Expected or actual date when selected participants are notified that they have been selected'),
  performance_start_date: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
    .description('The first date that a participant may begin participating in a model'),
  performance_end_date: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
    .description('The last date that a participant may begin participating in a model'),
  additional_information: Joi.string().allow(null, '').example('extra info').description('additional information'),
  num_ffs_beneficiaries: Joi.number().integer().example(5)
    .description('count of Medicare Fee for Service beneficiaries participating in the model'),
  num_medicare_beneficiaries: Joi.number().integer().example(5)
    .description('count of Medicare beneficiaries participating in the model'),
  num_advantage_beneficiaries: Joi.number().integer().example(5)
    .description('count of Medicare Advantage beneficiaries participating in the model'),
  num_medicaid_beneficiaries: Joi.number().integer().example(5)
    .description('count of Medicaid beneficiaries participating in the model'),
  num_chip_beneficiaries: Joi.number().integer().example(5)
    .description('count of CHIP beneficiaries participating in the model'),
  num_dually_eligible_beneficiaries: Joi.number().integer().example(5)
    .description('count of Medicare & Medicaid Dually Eligible beneficiaries participating in the model'),
  num_private_insurance_beneficiaries: Joi.number().integer().example(5)
    .description('count of Private Insurance beneficiaries participating in the model'),
  num_other_beneficiaries: Joi.number().integer().example(5)
    .description('count of beneficiaries not identified that are participating in the model'),
  total_beneficiaries: Joi.number().integer().example(35)
    .description('sum of the previous beneficiary counts'),
  notes_for_beneficiary_counts: Joi.string().allow(null, '').example('actual counts as of 1st Quarter of 2018')
    .description('notes for beneficiary counts'),
  num_physicians: Joi.number().integer().example(5)
    .description('count of physicians participating in model'),
  num_other_individual_human_providers: Joi.number().integer().example(5)
    .description('count of non-physician individuals participating in model'),
  num_hospitals: Joi.number().integer().example(5)
    .description('count of hospitals participating in model'),
  num_non_hospital_health_care_orgs: Joi.number().integer().example(5)
    .description('description: count of non-hospital health care organizations participating in model'),
  num_other_providers: Joi.number().integer().example(5)
    .description('count of all other not yet identified providers participating in the model'),
  total_providers: Joi.number().integer().example(25).description('sum of provider counts'),
  notes_provider_count: Joi.string().allow(null, '').example('actual counts as of 1st Quarter of 2018')
    .description('notes for provider counts'),
  year: Joi.number().integer().min(2017).example(2018)
    .description('performance year in which report is created'),
  quarter: Joi.number().integer().example(1).description('quarter of the year in which report is created'), // TODO: Missing custom validation
  updated_at: Joi.date().required().default(Date, 'Now')
    .example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
    .description('date the report was last updated'),
  updated_by: Joi.string().allow(null, '').example('ABCD')
    .description('username of the user to last update the report'),
  created_at: Joi.date().required().default(Date, 'Now')
    .example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
    .description('date the report was created'),
  created_by: Joi.string().allow(null, '').example('ABCD').description('username of the user to create the report')
}).label('QuarterlyReport');

const Schema = PatchSchema.requiredKeys(
  'id',
  'model_id',
  'model_name'
);

module.exports = {
  Schema
};
