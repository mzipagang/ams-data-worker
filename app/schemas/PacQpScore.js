const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const PatchSchema = Joi.object({
  entity_id: Joi.string().example('A1001'),
  tin: Joi.string().example('123456789'),
  npi: Joi.string().example('9876543210'),
  payment_threshold_score: Joi.number().allow(null).example(44),
  patient_threshold_score: Joi.number().allow(null).example(46),
  score_type: Joi.string().valid('E', 'I', null).example('E')
    .description('Indicator that score is Entity or Individual'),
  performance_year: PerformanceYear,
  snapshot: Snapshot,
  execution: Execution
}).label('PatchPacQpScore');

const Schema = PatchSchema.requiredKeys(
  'entity_id',
  'tin',
  'npi'
).label('PacQpScore');

module.exports = {
  Schema,
  PatchSchema
};
