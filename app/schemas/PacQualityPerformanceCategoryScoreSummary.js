const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const Schema = Joi.object({
  entity_id: Joi.string(),
  final_qpc_score: Joi.number().min(0).max(999.9),
  current_achievement_points: Joi.number().min(0).max(999.9),
  prior_achievement_points: Joi.number().min(0).max(999.9),
  high_priority_bonus_points: Joi.number().min(0).max(99),
  cehrt_reporting_bonus: Joi.number().min(0).max(99),
  quality_improvement_points: Joi.string(),
  scored_measures: Joi.number().min(0).max(99),
  excluded_measures: Joi.number().min(0).max(99),
  excluded_measures_pay_for_reporting: Joi.number().min(0).max(99),
  excluded_measures_case_size: Joi.number().min(0).max(99),
  file_id: Joi.string(),
  performance_year: PerformanceYear,
  run_snapshot: Snapshot,
  execution: Execution,
  meta: Joi.any()
});

module.exports = {
  Schema
};
