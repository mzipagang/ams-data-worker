const Joi = require('joi');
const _ = require('lodash');

const valueMap = { P: 0, I: 1, S: 2, F: 3 };
const numericSnapshotValue = runSnapshot => (isNaN(runSnapshot) ? valueMap[runSnapshot] : parseInt(runSnapshot));

const getHighestValue = runSnapshotValuesArray =>
  _.uniq(runSnapshotValuesArray).sort((a, b) => numericSnapshotValue(b) - numericSnapshotValue(a))[0];

const values = {
  P: 'Preliminary',
  I: 'Initial',
  S: 'Second',
  F: 'Final',
  1: '1',
  2: '2',
  3: '3',
  4: '4'
};

const Schema = Joi.string().valid(Object.keys(values)).example('F').label('SnapshotEnum');

module.exports = {
  Schema,
  getHighestValue
};
