const Joi = require('joi');
const PerformanceYear = require('./PerformanceYear').Schema;
const ComplexPatientScore = require('./ComplexPatientScore').Schema;

const ProviderLVTFields = {
  lvt_flag: Joi.string().allow(null).example('F'),
  lvt_payments: Joi.number().allow(null).example(163848651.97),
  lvt_patients: Joi.number().integer().allow(null).example(147241)
};

const ProviderLVTV2Fields = {
  lvt_small_status: Joi.string().allow(null).example('Y'),
  lvt_performance_year: PerformanceYear.allow(null)
};

const ProviderFlagFields = {
  advanced_apm_flag: Joi.string().example('N'),
  mips_apm_flag: Joi.string().example('Y')
};

const ProviderInfoFields = {
  provider_relationship_code: Joi.string().allow(null).example('Y'),
  qp_payment_threshold_score: Joi.number().allow(null).example(44),
  qp_patient_threshold_score: Joi.number().allow(null).example(46)
};

const EntityInfoFields = {
  entity_id: Joi.string().example('A1001'),
  entity_name: Joi.string().example('Palm Beach Accountable Care Organization, LLC')
};

const ProviderV3Scores = {
  complex_patient_score: ComplexPatientScore.allow(null),
  final_qpc_score: Joi.number().min(0).max(999.9).example(999.9)
    .allow(null)
    .description('Final QPC Score')
};

const ProviderV3Indicators = {
  score_type: Joi.string().valid('E', 'I', null).example('E')
    .description('Indicator that score is Entity or Individual'),
  mips_ec_indicator: Joi.string().valid('Y', 'N', null).example('Y')
    .description('MIPS Eligible Clinician (EC) Indicator')
};

const EntityInfoSchema = Joi.object(EntityInfoFields);

const ApmInfoFields = {
  apm_id: Joi.string().example('A1001'),
  apm_name: Joi.string().example('Palm Beach Accountable Care Organization, LLC')
};

const ApmInfoSchema = Joi.object(ApmInfoFields).label('QPPEntityListData');

const ApmAndSubdivisionInfoFields = {
  ...ApmInfoFields,
  subdivision_id: Joi.string().example('01'),
  subdidivision_name: Joi.string().example('MSSP ACO - Track 1')
};

const ApmAndSubdivisionInfoSchema = Joi.object(ApmAndSubdivisionInfoFields);

const Schema = Joi.array().items(ApmInfoSchema);

module.exports = {
  Schema,
  ProviderLVTFields,
  ProviderLVTV2Fields,
  ProviderFlagFields,
  ProviderInfoFields,
  EntityInfoFields,
  EntityInfoSchema,
  ApmInfoFields,
  ApmInfoSchema,
  ApmAndSubdivisionInfoFields,
  ApmAndSubdivisionInfoSchema,
  ProviderV3Scores,
  ProviderV3Indicators
};
