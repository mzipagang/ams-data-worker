const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const PatchSchema = Joi.object({
  entity_id: Joi.string().example('A1001'),
  tin: Joi.string().example('123456789'),
  npi: Joi.string().example('9876543210'),
  provider_relationship_code: Joi.string().allow(null).example('Y'),
  duplicate: Joi.boolean().example(false),
  performance_year: PerformanceYear,
  snapshot: Snapshot,
  execution: Execution,
  mips_ec_indicator: Joi.string().valid('Y', 'N', null).example('Y')
    .description('MIPS Eligible Clinician (EC) Indicator')
}).label('PatchPacProvider');

const Schema = PatchSchema.requiredKeys(
  'entity_id',
  'tin',
  'npi'
).label('PacProvider');

module.exports = {
  Schema,
  PatchSchema
};
