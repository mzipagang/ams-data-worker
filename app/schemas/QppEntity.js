const Joi = require('joi');
const {
  ProviderLVTFields, ProviderLVTV2Fields, ProviderFlagFields, ProviderV3Scores,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields, ProviderV3Indicators
} = require('./QPPEntityListData');

const PatchSchema = Joi.object({
  entity_tin: Joi.string().allow(null, '').example('123456789'),
  ...EntityInfoFields,
  ...ProviderLVTFields,
  ...ProviderLVTV2Fields,
  ...ApmAndSubdivisionInfoFields,
  ...ProviderFlagFields,
  ...ProviderV3Scores,
  tins: Joi.array().items(Joi.object({
    tin: Joi.string().example('123456789'),
    npis: Joi.array().items(Joi.object({
      npi: Joi.string().example('9876543210'),
      qp_status: Joi.string().allow(null).example('Y'),
      ...ProviderInfoFields,
      ...ProviderV3Indicators
    }))
  }))
}).label('PatchQppEntity');

const Schema = PatchSchema.requiredKeys('entity_id').label('QppEntity');

module.exports = {
  PatchSchema,
  Schema
};

