const Joi = require('joi');

const Permission = Joi.object({
  name: Joi.string().required().meta({ unique: true, index: true }),
  description: Joi.string().required()
}).label('Permission');

const Role = Joi.object({
  name: Joi.string().required().meta({ unique: true, index: true }),
  description: Joi.string().required(),
  permissions: Joi.array().items(Permission).required().meta({ ref: 'Permission' })
}).label('Role');

const Schema = Joi.object({
  username: Joi.string().required().meta({ unique: true, index: true }),
  mail: Joi.string().required(),
  firstName: Joi.string(),
  lastName: Joi.string(),
  roles: Joi.array().items(Role).meta({ ref: 'Role' })
}).label('User');

const PatchSchema = Joi.object({
  username: Joi.string().required()
}).label('PatchUser');

module.exports = {
  Permission,
  Role,
  Schema,
  PatchSchema
};
