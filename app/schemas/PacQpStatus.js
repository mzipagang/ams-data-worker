const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const PatchSchema = Joi.object({
  npi: Joi.string().example('9876543210'),
  qp_status: Joi.string().example('Y'),
  performance_year: PerformanceYear,
  snapshot: Snapshot,
  execution: Execution
}).label('PatchPacQpStatus');

const Schema = PatchSchema.requiredKeys(
  'npi'
).label('PacQpStatus');

module.exports = {
  Schema,
  PatchSchema
};
