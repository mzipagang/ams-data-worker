const Joi = require('joi');

const Schema = Joi.string().valid('Y', 'N');

module.exports = {
  Schema
};
