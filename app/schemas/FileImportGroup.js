const uuid = require('uuid');
const Joi = require('joi');
const FileSchema = require('./File').Schema;

const Schema = Joi.object({
  id: Joi.string().default(uuid.v4, 'UUID').example('0000-0000-0000-0000'),
  status: Joi.string().default('open').example('open'),
  createdAt: Joi.date().default(Date, 'Now').example('2017-12-31T19:25:18.437Z'),
  updatedAt: Joi.date().default(Date, 'Now').example('2017-12-31T19:25:18.437Z'),
  createdBy: Joi.string().allow('', null).default(null),
  files: Joi.array().items(FileSchema)
}).label('FileImportGroup');

module.exports = {
  Schema
};
