const Joi = require('joi');

const Schema = Joi.number().integer().min(1).max(5)
  .example(1)
  .description('Complex Patient Score')
  .label('ComplexPatientScore');

module.exports = {
  Schema
};
