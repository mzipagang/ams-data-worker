const Joi = require('joi');

const ConvertToErrorCodes = (validationResults, mapFieldNameToErrorRule, errorConfig) => {
  const errorCodes = [];
  validationResults.error.details.forEach(error =>
    error.path.forEach((errorField) => {
      const errorRuleName = mapFieldNameToErrorRule[errorField];
      const errorMetadata = errorConfig.find(config => config.ruleName === errorRuleName && config.isActive);
      if (errorMetadata) {
        errorCodes.push(errorMetadata.errorCode);
      }
    })
  );
  return errorCodes;
};

const isValidDate = d => d instanceof Date && !isNaN(d);

const removeMongoFields = (obj) => {
  if (!obj) {
    return null;
  }

  if (typeof obj !== 'object' || isValidDate(obj)) {
    return obj;
  }

  if (Array.isArray(obj)) {
    return obj.map(childDocument => removeMongoFields(childDocument));
  }

  return Object.keys(obj).reduce((acc, key) => {
    if (obj[key] && ['_id', '__v'].indexOf(key) === -1) {
      acc[key] = removeMongoFields(obj[key]);
    }
    return acc;
  }, {});
};

const validate = (value, schema) => {
  // TODO: Remove this after we stop sending Mongo fields to the client (i.e. _id and __v)
  const plainValue = removeMongoFields(value);

  // TODO: Remove this after we get rid of the AMS-api middleware where user is added to the payload
  if (plainValue.user) {
    delete plainValue.user;
  }

  return Joi.validate(plainValue, schema);
};

module.exports = {
  ConvertToErrorCodes,
  isValidDate,
  removeMongoFields,
  validate
};
