const Joi = require('joi');
const YesNoPartial = require('./YesNoPartial').Schema;
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const Schema = Joi.object({
  apm_id: Joi.string(),
  subdivision_id: Joi.string(),
  entity_id: Joi.string(),
  tin: Joi.string(),
  npi: Joi.string().required(),
  qp_status: YesNoPartial.required(),
  expenditures_numerator: Joi.number().required(),
  expenditures_denominator: Joi.number().required(),
  payment_threshold_score: Joi.number().required(),
  payment_threshold_met: YesNoPartial.required(),
  beneficiaries_numerator: Joi.number().required(),
  beneficiaries_denominator: Joi.number().required(),
  patient_threshold_score: Joi.number().required(),
  patient_threshold_met: YesNoPartial.required(),
  performance_year: PerformanceYear,
  snapshot: Snapshot.required(),
  execution: Execution
});

module.exports = {
  Schema
};
