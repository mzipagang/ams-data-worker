const Joi = require('joi');
const Snapshot = require('./Snapshot').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const Schema = Joi.object({
  npi: Joi.string().required(),
  qp_status: Joi.string(),
  performance_year: PerformanceYear,
  snapshot: Snapshot.required(),
  execution: Execution
});

module.exports = {
  Schema
};
