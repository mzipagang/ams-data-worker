const Joi = require('joi');
const YesNoPartial = require('./YesNoPartial').Schema;

const MetaSchema = Joi.object({
  file_name: Joi.string(),
  file_location: Joi.string(),
  file_import_group_id: Joi.string(),
  file_import_group_file_id: Joi.string(),
  file_line_number: Joi.string()
});

const PatchSchema = Joi.object({
  id: Joi.string(),
  name: Joi.string(),
  short_name: Joi.string(),
  start_date: Joi.date(),
  end_date: Joi.date(),
  advanced_apm_flag: YesNoPartial,
  mips_special_scoring_flag: YesNoPartial,
  apm_quality_reporting_category: Joi.number().integer(),
  file_id: Joi.string().required(),
  meta: MetaSchema
}).label('PatchModel');

const Schema = PatchSchema.requiredKeys(
  'id',
  'name',
  'short_name',
  'start_date',
  'advanced_apm_flag',
  'mips_special_scoring_flag',
  'apm_quality_reporting_category',
  'file_id'
).label('Model');

module.exports = {
  Schema,
  PatchSchema
};
