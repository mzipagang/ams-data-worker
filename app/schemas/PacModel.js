const Joi = require('joi');
const NameValue = require('./NameValue').Schema;
const Snapshot = require('./Snapshot').Schema;
const YesNoPartial = require('./YesNoPartial').Schema;
const PerformanceYear = require('./PerformanceYear').Schema;
const Execution = require('./Execution').Schema;

const PatchSchema = Joi.object({
  id: Joi.string().allow(null, ''),
  name: Joi.string(),
  short_name: Joi.string().allow(''),
  apm_qpp: Joi.string().default('No'),
  advanced_apm_flag: YesNoPartial.allow(null),
  mips_apm_flag: YesNoPartial.allow(null),
  team_lead_name: Joi.string(),
  start_date: Joi.date(),
  end_date: Joi.date(),
  updated_by: Joi.string(),
  updated_date: Joi.date(),
  status: Joi.string().default('Active'),
  cancel_reason: Joi.string().allow(''),
  additional_information: Joi.string().allow(''),
  quality_reporting_category_code: Joi.string().valid('1', '2', '3', '4', 'Unknown').default('Unknown'),
  model_category: NameValue,
  target_beneficiary: Joi.array().items(NameValue),
  target_provider: Joi.array().items(NameValue),
  target_participant: Joi.array().items(NameValue),
  statutory_authority_tier_1: Joi.string().allow(''),
  statutory_authority_tier_2: Joi.string().allow(''),
  waivers: Joi.array().items(NameValue),
  group_center: Joi.array().items(Joi.string()),
  group_cmmi: Joi.string().allow(null, ''),
  performance_year: PerformanceYear,
  run_snapshot: Snapshot,
  snapshot: Joi.string().valid('P', '1', '2', '3', '4').example('1'),
  execution: Execution
}).label('PatchPacModel');

const Schema = PatchSchema.requiredKeys(
  'name',
  'quality_reporting_category_code'
).label('PacModel');

module.exports = {
  Schema,
  PatchSchema
};
