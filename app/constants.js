module.exports = {
  CORE_FILE_TYPES: ['pac-model', 'pac-subdivision', 'pac-entity', 'pac-provider', 'pac-qp_status'],
  BATCH: {
    JOB_TOPIC: 'Batch Job',
    WORKER_READY_MESSAGE_TYPE: 'ready',
    WORKER_WAITING_MESSAGE_TYPE: 'Worker:Waiting',
    WORKER_ERROR_MESSAGE_TYPE: 'Worker:Error',
    WORKER_COMPLETE_MESSAGE_TYPE: 'Worker:Complete',
    JOB_START_MESSAGE_TYPE: 'Job:Start',
    JOB_TYPES: {
      TEST_JOB: 'test-job',
      STREAM_BULK_ENTITY_DATA_TO_S3: 'stream-bulk-entity-data-to-s3',
      CREATE_QPP_ENTITIES_COLLECTION: 'create-qpp-entities-collection',
      UPDATE_QPP_PROVIDER_COLLECTION: 'UpdateQppProviderCollection'
    }
  },
  TAGS: {
    BATCH_PROCESSING: 'Batch Processing',
    EMAIL: 'Email',
    FILE_IMPORT_GROUP: 'File Import Group',
    HEALTH_CHECK: 'Health Check',
    PAC_MODEL: 'PAC Model',
    PUBLISH_HISTORY: 'Publish History',
    QP_REPORT: 'QP Report',
    QPPV2: 'QPP v2',
    QPPV3: 'QPP v3',
    QUARTERLY_REPORT: 'Quarterly Report',
    REPORT: 'Report',
    SNAPSHOT: 'Snapshot',
    SUBDIVISION: 'Subdivision',
    USER: 'User',
    VALIDATION: 'Validation'
  },
  UNAUTHENTICATED_ROUTES: [
    { method: 'GET', path: '/api/v1/health_check' },
    { method: 'GET', path: '/api/v1/queue_check' },
    { method: 'GET', path: '/api/v1/session' },
    { method: 'POST', path: '/api/v1/session' },
    { method: 'DELETE', path: '/api/v1/session' },
    { method: 'POST', path: '/api/v1/user' },
    { method: 'POST', path: '/api/v1/user/login' },
    { method: 'GET', path: '/api/v1/user/logout' },
    { method: 'GET', path: '/api/v1/user/session_info' }
  ]
};
