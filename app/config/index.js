/**
 * Configuration File - Use this config rather than directly using process.env.
 *
 * NEVER put environment specific configuration in here.
 * Always require environment specific configurations to be passed in
 * as environment variables.
 */
const LOCAL_AWS =
  process.env.NODE_ENV === 'production'
    ? !!process.env.LOCAL_AWS
    : !(process.env.AWS_ACCESS_KEY_ID || process.env.AWS_SECRET_ACCESS_KEY);

const config = {
  NODE_ENV: process.env.NODE_ENV || 'dev',
  LDAP: {
    URI: process.env.LDAP_URI || 'ldap://127.0.0.1:389',
    USER: {
      DN: process.env.LDAP_USER_DN || 'cn=admin,dc=cms,dc=hhs,dc=gov',
      PW: process.env.LDAP_USER_PW || 'admin'
    },
    REJECT_UNAUTHORIZED: process.env.LDAP_REJECT_UNAUTHORIZED !== 'false',
    JOB_CODE: process.env.JOB_CODE || 'AMS_DEV_AWS'
  },
  AWS: {
    LOCAL_AWS,
    ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID || (LOCAL_AWS ? 'id' : ''),
    SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY || (LOCAL_AWS ? 'key' : ''),
    SQS_REQUEST_QUEUE_URL:
      process.env.SQS_REQUEST_QUEUE_URL || (LOCAL_AWS ? 'http://0.0.0.0:4100/ams-request-queue' : ''),
    CLEAN_S3_BUCKET: process.env.CLEAN_S3_BUCKET || 'ams-uploads-clean',
    JOB_S3_BUCKET: process.env.JOB_S3_BUCKET || 'ams-uploads-clean',
    SIGNED_URL_TIMEOUT_IN_SECONDS: parseInt(process.env.DEFAULT_SIGNED_URL_TIMEOUT_SECONDS) || 60 * 60
  },
  EXPRESS: {
    PORT: parseInt(process.env.PORT) || 7000,
    INTERFACE: process.env.INTERFACE || '0.0.0.0'
  },
  JWT: {
    SECRET: process.env.JWT_SECRET || 'secret'
  },
  NEW_RELIC: {
    APP_NAME: process.env.NEW_RELIC_APP_NAME || 'AMS-data-worker',
    LICENSE_KEY: process.env.NEW_RELIC_LICENSE_KEY || null
  },
  EMAIL_API: {
    URI: process.env.EMAIL_URI || 'localhost',
    PORT: process.env.EMAIL_PORT || '2525',
    USERNAME: process.env.EMAIL_USERNAME || null,
    PASSWORD: process.env.EMAIL_PASSWORD || null,
    IS_SECURE: process.env.EMAIL_IS_SECURE === 'true',
    NOTIFICATION_SENDER: process.env.NOTIFICATION_SENDER || 'fakePerson@semanticbits.com',
    NOTIFICATION_RECEIVERS: process.env.NOTIFICATION_RECEIVERS || 'fakePerson@semanticbits.com'
  },
  TEST: {
    ENABLE_TEST_USERS: (process.env.ENABLE_TEST_USERS === 'true'),
    USERS: [
      {
        username: 'test-user-1',
        password: 'test-user-1-pw',
        givenName: 'Test User1',
        mail: 'randall.groff@semanticbits.com',
        dn: 'uid=test-user-1,ou=people,dc=cms,dc=hhs,dc=gov',
        ismemberof: [
          'cn=AMS_DEV_AWS,ou=Groups,dc=cms,dc=hhs,dc=gov',
          'cn=AMS_TEST_AWS,ou=Groups,dc=cms,dc=hhs,dc=gov',
          'cn=AMS_VAL_AWS,ou=Groups,dc=cms,dc=hhs,dc=gov'
        ]
      }
    ]
  },
  SESSION: {
    TIMEOUT_IN_SECONDS: parseInt(process.env.DEFAULT_SESSION_TIMEOUT_SECONDS) || 60 * 15
  },
  MONGOOSE: {
    URI: process.env.MONGODB || 'mongodb://localhost:20017/ams-api'
  },
  ENCRYPTION: {
    KEY: process.env.ENCRYPTION_KEY || 'asdfjkl;asdfjkl;asdfjkl;asdfjkl;',
    IV: process.env.ENCRYPTION_IV || 'asdfjkl;asdfjkl;'
  },
  DEV: {
    SQS: process.env.DEV_SQS || 'http://localhost:4100',
    S3: process.env.DEV_S3 || 'http://localhost:4569'
  },
  REDIS: {
    HOST: process.env.REDIS_HOST || '127.0.0.1',
    PORT: process.env.REDIS_PORT || '6379',
    PW: process.env.REDIS_PW || null
  },
  FEATURE_FLAGS: {
    DISABLE_SOURCE_FILES: process.env.DISABLE_SOURCE_FILES === 'true'
  },
  CACHE: {
    ENABLED: process.env.CACHE_ENABLED ? /* istanbul ignore next */ process.env.CACHE_ENABLED === 'true' : true
  }
};

module.exports = config;
