require('dotenv').config({ path: require('path').join(__dirname, '../.env') });

if (process.env.NEW_RELIC_LICENSE_KEY) {
  require('newrelic');
}

const moment = require('moment');
const config = require('./config');
const mongoose = require('./services/mongoose');
const express = require('./services/express');
const logger = require('./services/logger');
const expressFileRouter = require('./services/expressFileRouter');
const redis = require('./services/redis');

const onError = (err) => {
  logger.error('Server failed to start:', err.message);
  logger.error(err);
  process.exit(1);
};

const main = new Promise(async (resolve) => {
  try {
    await mongoose.connect(config.MONGOOSE.URI, { useNewUrlParser: true });
    mongoose.set('useCreateIndex', true);
    mongoose.set('useFindAndModify', false);

    /* istanbul ignore next */
    if (config.CACHE.ENABLED) {
      await redis.connectToRedis();
    }

    await mongoose.getMutex();

    for (let i = 0; i < mongoose.ams_migrations.length; i += 1) {
      const err = await mongoose.ams_migrations[i].migration(); // eslint-disable-line no-await-in-loop
      if (err) {
        /* eslint-disable no-await-in-loop */
        await mongoose.releaseMutex();
        /* eslint-enable no-await-in-loop */
        onError(err);
        break;
      }
    }

    await mongoose.releaseMutex();

    await express.start(expressFileRouter.load()).catch(onError);
    logger.info(`Server started at ${moment().format()} on port ${config.EXPRESS.PORT}`);
    return resolve();
  } catch (err) {
    logger.error('Server error:');
    logger.error(err);
    return resolve();
  }
});

module.exports = {
  express: main
};
