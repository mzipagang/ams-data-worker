const url = require('url');
const generateSwaggerJson = require('../../../services/generateSwaggerJson');
const logger = require('../../../services/logger');

const handler = async (req, res) => {
  const { headers, query } = req;
  const { origin } = headers;
  const { version } = query;

  let host;
  let protocol;
  let port;
  let defaultPort;

  if (origin) {
    const parsedOrigin = url.parse(origin);
    host = parsedOrigin.hostname;
    port = parsedOrigin.port;
    protocol = parsedOrigin.protocol === 'https:' ? 'https' : 'http';
    defaultPort = parsedOrigin.protocol === 'https:' ? 443 : 80;
  } else {
    host = req.hostname;
    port = req.port;
    protocol = req.protocol;
    defaultPort = req.protocol === 'https' ? 443 : 80;
  }

  const filter = !version || version === 'all' ? null : RegExp(`/${version}`);

  try {
    const result = await generateSwaggerJson({
      host,
      port: port || defaultPort,
      protocol,
      title: 'API',
      routesRoot: 'app/express-routes',
      version,
      filter
    });
    res.json(result);
  } catch (ex) {
    logger.error(ex);
    return res.status(500).json({ message: 'There was an error while creating the swagger documentation' });
  }
};

module.exports = {
  handler
};
