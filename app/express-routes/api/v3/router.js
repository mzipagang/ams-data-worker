const sessionCheckMiddleware = require('../../../services/sessionCheckMiddleware');

module.exports = (router) => {
  router.use(sessionCheckMiddleware);
  return router;
};
