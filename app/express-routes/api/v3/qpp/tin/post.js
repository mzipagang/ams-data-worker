const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const {
  ProviderLVTFields, ProviderLVTV2Fields, ProviderFlagFields, ProviderV3Scores,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields, ProviderV3Indicators
} = require('../../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  res.json({}); // TODO
};

module.exports = {
  handler,
  tags: [TAGS.QPPV3],
  description: 'TIN Endpoint',
  input: {
    payload: Joi.object({
      tins: Joi.string().required().description('Comma delimited list of TINs')
    }),
    query: Joi.object({
      performance_year: PerformanceYear
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          tin: Joi.string().example('123456789'),
          entities: Joi.array().items(Joi.object({
            ...EntityInfoFields,
            ...ProviderLVTFields,
            ...ProviderLVTV2Fields,
            entity_tin: Joi.string().example('123456789'),
            npis: Joi.array().items(Joi.object({
              npi: Joi.string().example('1234567890'),
              qp_status: Joi.string().example('Y'),
              ...ProviderInfoFields,
              ...ProviderV3Indicators
            })),
            ...ApmAndSubdivisionInfoFields,
            ...ProviderFlagFields,
            ...ProviderV3Scores
          }))
        }).label('QPPTINDataV3'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
