const _ = require('lodash');
const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/QppEntity');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const Snapshot = require('../../../../../schemas/Snapshot').Schema;
const { getEntities } = require('../../../../../services/qpp');

const input = {
  headers: Joi.object({
    'x-entity': Joi.string().regex(/^[^,]+(,[^,]+)*$/).required().description('Comma delimited list of Entity IDs')
  }),
  query: Joi.object({
    performance_year: PerformanceYear.required()
  })
};

const handler = async (req, res) => {
  const headerValidation = Joi.validate(req.headers, input.headers, { allowUnknown: true });
  if (headerValidation.error) {
    return res.status(400).json({ message: headerValidation.error.message });
  }

  const queryValidation = Joi.validate(req.query, input.query);
  if (queryValidation.error) {
    return res.status(400).json({ message: queryValidation.error.message });
  }

  const performanceYear = parseInt(req.query.performance_year);
  const entityIds = _.uniq(req.headers['x-entity'].split(','));
  const entities = await getEntities(performanceYear, entityIds);
  return res.json(entities);
};

module.exports = {
  handler,
  tags: [TAGS.QPPV3],
  description: 'Entity Endpoint',
  input,
  output: {
    schema: Joi.object({
      data: {
        performance_year: PerformanceYear,
        snapshot: Snapshot,
        apm_data: Joi.array().items(Schema)
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
