const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/QPPEntityListData');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;

const handler = async (req, res) => {
  res.json({}); // TODO
};

module.exports = {
  handler,
  tags: [TAGS.QPPV3],
  description: 'Entity List Endpoint',
  input: {
    query: Joi.object({
      performance_year: PerformanceYear,
      publish_history_id: Joi.string()
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Schema
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
