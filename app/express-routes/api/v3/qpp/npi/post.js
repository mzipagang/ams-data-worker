const _ = require('lodash');
const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const QppProvider = require('../../../../../services/mongoose/models/qpp_provider');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const Snapshot = require('../../../../../schemas/Snapshot');
const { getAdvancedApmFlag, getLvtFlag, getMipsApmFlag } = require('../../../../../services/qpp');
const {
  ProviderLVTFields, ProviderLVTV2Fields, ProviderFlagFields, ProviderV3Scores,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields, ProviderV3Indicators
} = require('../../../../../schemas/QPPEntityListData');

const input = {
  payload: Joi.object({
    npis: Joi.array().items(Joi.string().regex(/^\d{10}$/).example('1234567890')).min(1).required()
      .description('Comma delimited list of NPIs')
  }),
  query: Joi.object({
    performance_year: PerformanceYear.required()
  })
};

const handler = async (req, res) => {
  const payloadValidation = Joi.validate(req.body, input.payload);
  if (payloadValidation.error) {
    return res.status(400).json({ message: payloadValidation.error.message });
  }

  const queryValidation = Joi.validate(req.query, input.query);
  if (queryValidation.error) {
    return res.status(400).json({ message: queryValidation.error.message });
  }

  const performanceYear = parseInt(req.query.performance_year);
  const { npis } = req.body;

  const providers = await QppProvider.aggregate([
    {
      $match: {
        performance_year: performanceYear,
        npi: { $in: _.uniq(npis) }
      }
    },
    {
      $unwind: '$tin'
    },
    {
      $group: {
        _id: { npi: '$npi', snapshot: '$snapshot' },
        tins: {
          $push: {
            entity: '$entity',
            LVT: '$LVT',
            mips_ec_indicator: '$mips_ec_indicator',
            model: '$model',
            provider_relationship_code: '$provider_relationship_code',
            qpCategoryScoreSummary: '$qpCategoryScoreSummary',
            qpScore: '$qpScore',
            qpStatus: '$qpStatus',
            snapshot: '$snapshot',
            subdivision: '$subdivision',
            tin: '$tin'
          }
        }
      }
    },
    {
      $project: {
        'tins.entity.apm_id': true,
        'tins.entity.id': true,
        'tins.entity.name': true,
        'tins.entity.tin': true,
        'tins.LVT.complex_patient_score': true,
        'tins.LVT.patients': true,
        'tins.LVT.payments': true,
        'tins.LVT.performance_year': true,
        'tins.LVT.small_status': true,
        'tins.LVT.status': true,
        'tins.mips_ec_indicator': true,
        'tins.model.advanced_apm_flag': true,
        'tins.model.id': true,
        'tins.model.mips_apm_flag': true,
        'tins.model.name': true,
        'tins.provider_relationship_code': true,
        'tins.qpCategoryScoreSummary.final_qpc_score': true,
        'tins.qpScore.payment_threshold_score': true,
        'tins.qpScore.patient_threshold_score': true,
        'tins.qpScore.score_type': true,
        'tins.qpStatus.qp_status': true,
        'tins.run_snapshot': true,
        'tins.subdivision.id': true,
        'tins.subdivision.name': true,
        'tins.subdivision.advanced_apm_flag': true,
        'tins.subdivision.mips_apm_flag': true,
        'tins.tin': true
      }
    }
  ]);

  return res.json({
    data: {
      performance_year: performanceYear,
      snapshot: Snapshot.getHighestValue(providers.map(provider => provider._id.snapshot)),
      apm_data: providers.map(provider => ({
        npi: provider._id.npi,
        qp_status:
          provider.tins && provider.tins.length && provider.tins[0].qpStatus && provider.tins[0].qpStatus.qp_status,
        tins: provider.tins.map(tin => ({
          tin: tin.tin,
          entities: [{ // Always a single element
            entity_id: tin.entity && tin.entity.id,
            entity_name: tin.entity && tin.entity.name,
            lvt_flag: tin.LVT && getLvtFlag(tin.LVT.status),
            lvt_payments: tin.LVT && tin.LVT.payments,
            lvt_patients: tin.LVT && tin.LVT.patients,
            lvt_small_status: tin.LVT && tin.LVT.small_status,
            lvt_performance_year: tin.LVT && tin.LVT.performance_year,
            entity_tin: tin.entity && tin.entity.tin,
            provider_relationship_code: tin.provider_relationship_code,
            qp_payment_threshold_score: tin.qpScore && tin.qpScore.payment_threshold_score,
            qp_patient_threshold_score: tin.qpScore && tin.qpScore.patient_threshold_score,
            apm_id: tin.model && tin.model.id,
            apm_name: tin.model && tin.model.name,
            subdivision_id: tin.subdivision && tin.subdivision.id,
            subdivision_name: tin.subdivision && tin.subdivision.name,
            advanced_apm_flag: getAdvancedApmFlag(tin),
            mips_apm_flag: getMipsApmFlag(tin),
            complex_patient_score: tin.LVT && tin.LVT.complex_patient_score,
            final_qpc_score: tin.qpCategoryScoreSummary && tin.qpCategoryScoreSummary.final_qpc_score,
            score_type: tin.qpScore && tin.qpScore.score_type,
            mips_ec_indicator: tin.mips_ec_indicator
          }]
        }))
      }))
    }
  });
};

module.exports = {
  handler,
  tags: [TAGS.QPPV3],
  description: 'NPI Endpoint',
  input,
  output: {
    schema: Joi.object({
      data: {
        performance_year: PerformanceYear,
        snapshot: Snapshot.Schema,
        apm_data: Joi.array().items(Joi.object({
          npi: Joi.string().example('1234567890'),
          qp_status: Joi.string().example('Y'),
          tins: Joi.array().items(Joi.object({
            tin: Joi.string().example('123456789'),
            entities: Joi.array().items(Joi.object({
              ...EntityInfoFields,
              ...ProviderLVTFields,
              ...ProviderLVTV2Fields,
              entity_tin: Joi.string().example('123456789'),
              ...ProviderInfoFields,
              ...ApmAndSubdivisionInfoFields,
              ...ProviderFlagFields,
              ...ProviderV3Scores,
              ...ProviderV3Indicators
            }))
          }))
        }).label('QPPNPIDataV3'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
