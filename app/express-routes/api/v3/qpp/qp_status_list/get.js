const Joi = require('joi');
const { QPPApiQPStatusList } = require('../../../../../services/qpp');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const { TAGS } = require('../../../../../constants');
const asyncHandler = require('express-async-handler');

const API_INPUT_SCHEMA = Joi.object({
  performance_year: PerformanceYear.required()
});

const handler = asyncHandler(async (req, res) => {
  const performanceYear = parseInt(req.query.performance_year);

  const result = Joi.validate({ performance_year: performanceYear }, API_INPUT_SCHEMA);
  if (result.error) {
    return res.status(400).json({
      message: 'performance_year is a required query string parameter, must be an integer >= 2017.'
    });
  }

  const data = await QPPApiQPStatusList(performanceYear);

  return res.status(200).json({
    data: {
      apm_data: data
    }
  });
});

module.exports = {
  handler,
  tags: [TAGS.QPPV3],
  description: 'QP Status Endpoint',
  input: {
    query: API_INPUT_SCHEMA
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          performance_year: PerformanceYear,
          npis: Joi.array().items(Joi.object({
            npi: Joi.string().example('1234567890'),
            qp_status: Joi.string().example('Y')
          })),
          snapshot: Joi.string()
        }).label('QPPQPStatusListDataV3'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      400: 'Bad parameters',
      500: 'Unexpected error'
    }
  }
};
