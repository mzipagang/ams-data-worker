const handler = (_req, res) => {
  res.json({ message: 'Welcome to the Data Worker.' });
};

module.exports = {
  handler
};
