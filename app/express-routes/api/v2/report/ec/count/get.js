const logger = require('../../../../../../services/logger');
const Report = require('../../../../../../services/report');
const PerformanceYear = require('../../../../../../schemas/PerformanceYear').Schema;
const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');
const Snapshot = require('../../../../../../schemas/Snapshot').Schema;
const Schema = require('../../../../../../schemas/PastReport').ECCountReportSchema;

const handler = asyncHandler(async (req, res) => {
  try {
    const { performance_year: performanceYear, run_snapshot: runSnapshot, bypass_cache: bypassCache } = req.query;

    let date = new Date();

    if (req.query.date) {
      date = new Date(req.query.date.replace(' ', '+'));
    }

    const snapshot = await Report.snapshot.getModels(performanceYear, runSnapshot);

    if (snapshot.error) {
      logger.error(snapshot.error);
      res.status(500).json({
        message: snapshot.error
      });
    } else {
      const result = await Report.ecCount.report(performanceYear, runSnapshot, date, bypassCache, snapshot);
      res.status(200).json(result);
    }
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error in getting the EC metrics report'
    });
  }
});

module.exports = {
  handler,
  tags: [TAGS.REPORT],
  description: 'Generates the Eligible Clinicians (EC) count report for a given performance year and run_snapshot',
  input: {
    query: Joi.object({
      performance_year: PerformanceYear,
      run_snapshot: Snapshot.required().description('The run snapshot flag, for example "F"'),
      date: Joi.date(),
      bypass_cache: Joi.number().integer().valid(0, 1)
    })
  },
  output: {
    schema: Schema,
    status: {
      200: 'An object that represents the EC report',
      500: 'Unexpected error'
    }
  }
};
