const logger = require('../../../../../../services/logger');
const Report = require('../../../../../../services/report');
const PerformanceYear = require('../../../../../../schemas/PerformanceYear').Schema;
const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');

const handler = asyncHandler(
  async (req, res) => {
    try {
      const snapshots = await Report.snapshot.byYear();
      res.status(200).json(snapshots);
    } catch (err) {
      logger.error(err);
      res.status(500).json({
        message: 'There was an error in getting the list of snapshots'
      });
    }
  }
);

module.exports = {
  handler,
  tags: [TAGS.QP_REPORT],
  description: 'Retrieve all snapshots that are available for qp reporting',
  output: {
    schema: Joi.object({
      performance_year: Joi.array().items(Joi.object({
        year: PerformanceYear,
        run_snapshot: Joi.string().example('F').description('run_snapshot (F,P, etc)')
      }))
    }),
    status: {
      200: 'Return all snapshots',
      500: 'Unexpected error'
    }
  }
};
