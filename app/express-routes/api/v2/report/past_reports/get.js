const logger = require('../../../../../services/logger');
const PastReport = require('../../../../../services/mongoose/models/past_report');
const asyncHandler = require('express-async-handler');

const handler = asyncHandler(async (req, res) => {
  try {
    const queries = {
      ec_count: { $exists: true, $ne: null },
      qp_metrics: { $exists: true, $ne: null, $not: { $size: 0 } }
    };
    const query = queries[req.query.type];
    if (!query) {
      const err = 'Unsupported report type';
      logger.error(err);
      res.status(500).json({
        message: err
      });
    } else {
      const fields = `-_id -__v ${Object.keys(queries).map(type => `-${type}`).join(' ')}`;
      const reports = await PastReport.find({ [req.query.type]: query }, fields)
        .sort({ date: -1 })
        .lean();
      res.status(200).json(reports);
    }
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error in getting the reports'
    });
  }
});

module.exports = {
  handler
};
