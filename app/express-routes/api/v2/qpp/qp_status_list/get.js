const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiQPStatusList } = require('../../../../../services/qpp');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const { TAGS } = require('../../../../../constants');

const handler = async (req, res) => {
  try {
    let performanceYear = 2017;
    if (req.query.performance_year) {
      performanceYear = parseInt(req.query.performance_year);
    }

    const data = await QPPApiQPStatusList(performanceYear);

    res.status(200).json({
      data: {
        apm_data: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.QPPV2],
  description: 'QP Status Endpoint',
  input: {
    query: Joi.object({
      performance_year: PerformanceYear
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          performance_year: PerformanceYear,
          npis: Joi.array().items(Joi.object({
            npi: Joi.string().example('1234567890'),
            qp_status: Joi.string().example('Y')
          }))
        }).label('QPPQPStatusListDataV2'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
