const Joi = require('joi');
const Promise = require('bluebird');
const logger = require('../../../../services/logger');
const { GenericQPPAPI } = require('../../../../services/qpp');
const PerformanceYear = require('../../../../schemas/PerformanceYear').Schema;
const flatten = require('lodash.flatten');
const { TAGS } = require('../../../../constants');
const {
  ProviderLVTFields, ProviderLVTV2Fields, ProviderFlagFields,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields
} = require('../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    const npis = req.headers['x-npi'] ? req.headers['x-npi'].split(',') : [];
    const tins = req.headers['x-tin'] ? req.headers['x-tin'].split(',') : [];
    const entityIds = req.headers['x-entity'] ? req.headers['x-entity'].split(',') : [];
    const performanceYears = (req.query.performanceYears || ['2017']).sort();
    const dataByYear = await Promise.all(performanceYears.map(year =>
      GenericQPPAPI(npis, tins, entityIds, parseInt(year), req.query.publish_history_id, req.query)));

    return res.status(200).json({
      data: {
        apm_data: flatten(dataByYear)
      }
    });
  } catch (e) {
    logger.error(e);
    if (e.message === 'Search Limit Exceeded') {
      return res.status(403).json({
        message: 'The result set is too big to display in detail. Please update your search to narrow the results.'
      });
    }

    return res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.QPPV2],
  description: 'Retrieve APM data for QPP',
  input: {
    headers: Joi.object({
      'x-npi': Joi.string().required().description('Comma delimited list of NPIs'),
      'x-tin': Joi.string().required().description('Comma delimited list of TINs'),
      'x-entity': Joi.string().required().description('Comma delimited list of Entity IDs')
    }),
    query: Joi.object({
      performance_year: PerformanceYear
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          tin: Joi.string().example('123456789'),
          npi: Joi.string().example('1234567890'),
          qp_status: Joi.string().example('Y'),
          ...EntityInfoFields,
          ...ProviderLVTFields,
          ...ProviderLVTV2Fields,
          entity_tin: Joi.string().example('123456789'),
          ...ProviderInfoFields,
          ...ApmAndSubdivisionInfoFields,
          ...ProviderFlagFields
        }).label('QPPGenericDataV2'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
