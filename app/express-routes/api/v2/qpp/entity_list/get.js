const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiEntitylist } = require('../../../../../services/qpp');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/QPPEntityListData');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;

const handler = async (req, res) => {
  try {
    let performanceYear = 2017;
    if (req.query.performance_year) {
      performanceYear = parseInt(req.query.performance_year);
    }

    const data = await QPPApiEntitylist(performanceYear, req.query.publish_history_id);

    res.status(200).json({
      data: {
        apm_data: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.QPPV2],
  description: 'Entity List Endpoint',
  input: {
    query: Joi.object({
      performance_year: PerformanceYear,
      publish_history_id: Joi.string()
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Schema
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
