const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiByEntity } = require('../../../../../services/qpp');
const { TAGS } = require('../../../../../constants');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const {
  ProviderLVTFields, ProviderLVTV2Fields, ProviderFlagFields,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields
} = require('../../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    if (!req.headers['x-entity']) {
      res.status(400).json({
        message: 'x-entity is a required header parameter.'
      });
      return;
    }

    let performanceYear = 2017;
    if (req.query.performance_year) {
      performanceYear = parseInt(req.query.performance_year);
    }

    const entityIds = req.headers['x-entity'].split(',');

    const data = await QPPApiByEntity(entityIds, performanceYear, req.query.publish_history_id);

    res.status(200).json({
      data: {
        apm_data: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.QPPV2],
  description: 'Entity Endpoint',
  input: {
    headers: Joi.object({
      'x-entity': Joi.string().required().description('Comma delimited list of Entity IDs')
    }),
    query: Joi.object({
      performance_year: PerformanceYear
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          ...EntityInfoFields,
          ...ProviderLVTFields,
          ...ProviderLVTV2Fields,
          entity_tin: Joi.string().example('123456789'),
          ...ApmAndSubdivisionInfoFields,
          ...ProviderFlagFields,
          tins: Joi.array().items(Joi.object({
            tin: Joi.string().example('123456789'),
            npis: Joi.array().items(Joi.object({
              npi: Joi.string().example('9876543210'),
              qp_status: Joi.string().example('Y'),
              ...ProviderInfoFields
            }))
          }))
        }).label('QPPEntityDataV2'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
