const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiByNPI } = require('../../../../../services/qpp');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const { TAGS } = require('../../../../../constants');
const {
  ProviderLVTFields, ProviderLVTV2Fields, ProviderFlagFields,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields
} = require('../../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    const performanceYear = req.query.performance_year ? parseInt(req.query.performance_year) : 2017;
    const { npis } = req.body;

    if (!npis || !Array.isArray(npis) || !npis.length) {
      res.status(400).json({
        message: 'npis are a required in the body.'
      });
      return;
    }

    const data = await QPPApiByNPI(npis, performanceYear, req.query.publish_history_id);

    res.status(200).json({
      data: {
        apm_data: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.QPPV2],
  description: 'NPI Endpoint (higher limit)',
  input: {
    payload: Joi.object({
      npis: Joi.string().required().description('Comma delimited list of NPIs')
    }),
    query: Joi.object({
      performance_year: PerformanceYear
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          npi: Joi.string().example('1234567890'),
          qp_status: Joi.string().example('Y'),
          tins: Joi.array().items(Joi.object({
            tin: Joi.string().example('123456789'),
            entities: Joi.array().items(Joi.object({
              ...EntityInfoFields,
              ...ProviderLVTFields,
              ...ProviderLVTV2Fields,
              entity_tin: Joi.string().example('123456789'),
              ...ProviderInfoFields,
              ...ApmAndSubdivisionInfoFields,
              ...ProviderFlagFields
            }))
          }))
        }).label('QPPNPIDataV2'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
