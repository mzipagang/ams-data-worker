const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiByNPI } = require('../../../../../services/qpp');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const { TAGS } = require('../../../../../constants');
const {
  ProviderLVTFields, ProviderLVTV2Fields, ProviderFlagFields,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields
} = require('../../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    if (!req.headers['x-npi']) {
      res.status(400).json({
        message: 'x-npi is a required header parameter.'
      });
      return;
    }

    let performanceYear = 2017;
    if (req.query.performance_year) {
      performanceYear = parseInt(req.query.performance_year);
    }

    const npis = (req.headers['x-npi'].split(','));

    const data = await QPPApiByNPI(npis, performanceYear, req.query.publish_history_id);

    res.status(200).json({
      data: {
        apm_data: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.QPPV2],
  description: 'NPI Endpoint',
  input: {
    headers: Joi.object({
      'x-npi': Joi.string().required().description('Comma delimited list of NPIs')
    }),
    query: Joi.object({
      performance_year: PerformanceYear
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          npi: Joi.string().example('1234567890'),
          qp_status: Joi.string().example('Y'),
          tins: Joi.array().items(Joi.object({
            tin: Joi.string().example('123456789'),
            entities: Joi.array().items(Joi.object({
              ...EntityInfoFields,
              ...ProviderLVTFields,
              ...ProviderLVTV2Fields,
              entity_tin: Joi.string().example('123456789'),
              ...ProviderInfoFields,
              ...ApmAndSubdivisionInfoFields,
              ...ProviderFlagFields
            }))
          }))
        }).label('QPPNPIDataV2'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
