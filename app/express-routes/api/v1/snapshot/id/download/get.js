const Json2csvTransform = require('json2csv').parse;
const logger = require('../../../../../../services/logger');

const SnapshotModel = require('../../../../../../services/mongoose/models/snapshot');

const handler = (req, res) => {
  const snapshotId = req.params.id;
  const fields = ['qppYear',
    'qpPeriod',
    'snapshotRun',
    'initNotes',
    'createdBy',
    'status',
    'reviewedAt',
    'createdAt',
    'id'];
  const opts = { fields, quote: '' };
  return SnapshotModel.find({ id: snapshotId }, []).lean()
    .then((snapshot) => {
      res.setHeader('Content-Type', 'text/csv; charset=Shift_JIS');
      return res.send(Json2csvTransform(snapshot, opts));
    })
    .catch((err) => {
      logger.error(err);
      res.status(500).json({
        message: 'There was an error'
      });
    });
};

module.exports = {
  handler
};
