const logger = require('../../../../../services/logger');
const Snapshot = require('../../../../../services/mongoose/models/snapshot');

const handler = async (req, res) => {
  try {
    const snapshot = await Snapshot.findOne({ id: req.params.id });

    res.status(200).json({
      data: {
        snapshot
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error'
    });
  }
};

module.exports = {
  handler
};
