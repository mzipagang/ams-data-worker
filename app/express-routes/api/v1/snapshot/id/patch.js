const logger = require('../../../../../services/logger');
const moment = require('moment');
const Snapshot = require('../../../../../services/mongoose/models/snapshot');
const { getUsername } = require('../../../../../services/utils/jwt');

const RESTRICTED_STATUSES = {
  processing: true,
  error: true,
  approved: true,
  rejected: true
};

const VALID_STATUSES = {
  approved: true,
  rejected: true
};

const handler = async (req, res) => {
  try {
    const { status } = await Snapshot.findOne({ id: req.params.id });

    if (RESTRICTED_STATUSES[status]) {
      res.status(400).json({
        message: `Snapshot cannot be changed once the status is '${status}'`
      });
      return;
    }

    if (!VALID_STATUSES[req.body.status]) {
      res.status(400).json({
        message: `'${req.body.status}' is not a valid status`
      });
      return;
    }

    const update = Object.assign({}, {
      reviewedBy: getUsername(req),
      status: req.body.status,
      rejectNotes: req.body.rejectNotes || '',
      reviewedAt: moment.utc().toDate()

    });

    const snapshot = await Snapshot.findOneAndUpdate(
      { id: req.params.id },
      { $set: update },
      { new: true }
    ).lean();

    res.status(200).json({
      data: {
        snapshot
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while updating the file import group.' });
  }
};

module.exports = {
  handler
};
