const Joi = require('joi');
const logger = require('../../../../services/logger');
const moment = require('moment');

const DynamicPACModelSnapshot = require('../../../../services/mongoose/dynamic_name_models/pac_model_snapshot');
const DynamicPACSubdivisionSnapshot = require(
  '../../../../services/mongoose/dynamic_name_models/pac_subdivision_snapshot'
);
const DynamicECStatus = require('../../../../services/mongoose/dynamic_name_models/ec_status.js');

const DynamicApmEntitySnapshot = require('../../../../services/mongoose/dynamic_name_models/apm_entity_snapshot');
const DynamicApmProviderSnapshot = require('../../../../services/mongoose/dynamic_name_models/apm_provider_snapshot');
const PACModel = require('../../../../services/mongoose/models/pac_model');
const PACSubdivision = require('../../../../services/mongoose/models/pac_subdivision');
const ApmEntity = require('../../../../services/mongoose/models/apm_entity');
const ApmProvider = require('../../../../services/mongoose/models/apm_provider');

const ProviderECCodes = require('../../../../services/utils/provider_ec_codes');

const Snapshot = require('../../../../services/mongoose/models/snapshot');
const SnapshotService = require('../../../../services/snapshot');
const { getUsername } = require('../../../../services/utils/jwt');
const { TAGS } = require('../../../../constants');

const handler = async (req, res) => {
  try {
    const snapshot = new Snapshot({
      qppYear: req.body.qppYear,
      qpPeriod: req.body.qpPeriod,
      snapshotRun: req.body.snapshotRun,
      initNotes: req.body.initNotes,
      createdBy: getUsername(req),
      status: 'processing'
    });
    await snapshot.save();

    try {
      const id = snapshot.id;
      const qppYear = snapshot.qppYear;

      const modelSnapshotCollection = DynamicPACModelSnapshot(id);
      const subdivisionSnapshotCollection = DynamicPACSubdivisionSnapshot(id);
      const entitySnapshotCollection = DynamicApmEntitySnapshot(id);
      const providerSnapshotCollection = DynamicApmProviderSnapshot(id);
      const ecStatusCollection = DynamicECStatus(id);

      await Promise.resolve(entitySnapshotCollection.init());
      await Promise.resolve(providerSnapshotCollection.init());
      const collections = await Promise.all([
        PACModel.aggregate([{ $out: `${modelSnapshotCollection.collection.name}` }])
          .then(() => ({ type: 'pac-model', name: modelSnapshotCollection.collection.name })),
        PACSubdivision.aggregate([{ $out: `${subdivisionSnapshotCollection.collection.name}` }])
          .then(() => ({ type: 'pac-subdivision', name: subdivisionSnapshotCollection.collection.name })),
        ApmEntity.aggregate([{ $out: `${entitySnapshotCollection.collection.name}` }])
          .then(() => ({ type: 'apm-entity', name: entitySnapshotCollection.collection.name })),
        ApmProvider.aggregate([{ $out: `${providerSnapshotCollection.collection.name}` }])
          .then(() => ({ type: 'apm-provider', name: providerSnapshotCollection.collection.name }))
      ]);
      snapshot.collections.push(...collections);

      await ProviderECCodes(qppYear, providerSnapshotCollection, ecStatusCollection.collection.name);

      snapshot.collections.push({ type: 'provider-ec-status', name: ecStatusCollection.collection.name });
      snapshot.status = 'complete';

      await snapshot.save();

      await SnapshotService.addRelationshipCode(
        providerSnapshotCollection,
        entitySnapshotCollection,
        modelSnapshotCollection,
        qppYear,
        moment.utc(),
        subdivisionSnapshotCollection
      );

      await SnapshotService.entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, qppYear);
      await SnapshotService.providerStatusFilter(
        providerSnapshotCollection,
        entitySnapshotCollection,
        modelSnapshotCollection,
        qppYear
      );
    } catch (err) {
      logger.error(err);
      snapshot.status = 'error';
      await snapshot.save();
    }

    res.status(200).json({
      data: {
        snapshot
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while creating a new snapshot.' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.SNAPSHOT],
  description: 'Create a new Snapshot',
  input: {
    payload: Joi.object({
      name: Joi.string().required().example('snapshot_01052018')
    })
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        snapshot: Joi.array().items(Joi.object({
          id: Joi.string().example('359fa4c2-a72c-45d4-97c0-ca982da5d54c'),
          name: Joi.string().example('snapshot_01052018'),
          createdBy: Joi.string().example('test-user-1'),
          createdAt: Joi.date().example('2018-01-05T18:59:21.530Z'),
          collection: Joi.array().items(Joi.object({
            name: Joi.string().example('apm_providers_snapshot_359fa4c2-a72c-45d4-97c0-ca982da5d54cs'),
            type: Joi.string().example('apm-provider')
          }))
        }))
      })
    }),
    status: {
      200: 'Return newly created Snapshot name',
      500: 'Unexpected error'
    }
  }
};
