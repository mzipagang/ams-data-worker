const logger = require('../../../../services/logger');
const Snapshot = require('../../../../services/mongoose/models/snapshot');

const handler = async (_req, res) => {
  try {
    const snapshots = await Snapshot.find({}).sort({ createdAt: -1 });

    res.status(200).json({
      data: {
        snapshots
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error'
    });
  }
};

module.exports = {
  handler
};
