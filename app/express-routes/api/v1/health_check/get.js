const User = require('../../../../services/mongoose/models/user');
const logger = require('../../../../services/logger');
const Joi = require('joi');
const { TAGS } = require('../../../../constants');

const handler = async (_req, res) => {
  let time = null;
  try {
    time = setTimeout(() => {
      throw new Error('timed out');
    }, 10000);

    await User.count({});

    clearTimeout(time);
    res.status(200).json({
      success: true
    });
  } catch (err) {
    clearTimeout(time);
    logger.error(err);
    res.status(500).json({
      success: false
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.HEALTH_CHECK],
  description: 'Checks the AMS system availability',
  output: {
    schema: Joi.object({
      success: Joi.boolean().required()
    }),
    status: {
      200: 'Returns success value as true if mongodb is up',
      500: 'Returns success value as false if mongodb is down'
    }
  }
};

