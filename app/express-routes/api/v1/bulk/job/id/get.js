const JobQueueService = require('../../../../../../services/JobQueueService');
const logger = require('../../../../../../services/logger');
const asyncHandler = require('express-async-handler');

const getJobQueueStatus = async (req, res) => {
  try {
    const id = req.params.id;
    const queuedJobStatus = await JobQueueService.getJobById(id);
    if (!queuedJobStatus) {
      res.status(404).json({
        message: 'This job id does not exist'
      });
      return;
    }

    const response = JobQueueService.shapeJobStatusResponse(queuedJobStatus);
    res.status(200).json({
      job: response
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error in getting the job queue status'
    });
  }
};

module.exports = asyncHandler(getJobQueueStatus);
