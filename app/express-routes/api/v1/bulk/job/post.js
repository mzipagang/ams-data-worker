const JobQueueService = require('../../../../../services/JobQueueService');
const logger = require('../../../../../services/logger');
const asyncHandler = require('express-async-handler');
const { getUsername } = require('../../../../../services/utils/jwt');
const { TAGS, BATCH } = require('../../../../../constants');
const Joi = require('joi');
const { UserVisibleSchema } = require('../../../../../schemas/QueuedJobs');
const handler = asyncHandler(async (req, res) => {
  try {
    const type = req.body.type;
    const parameters = req.body.parameters;

    if (!type) {
      res.status(400).json({
        resultsError: {
          code: 1235, // TODO: come up with a coding convention
          message: 'invalid job id'
        }
      });
      return;
    }
    const username = getUsername(req);
    const queuedJob = await JobQueueService.createJob(type, parameters, username);

    res.status(202).json({
      job: {
        id: queuedJob.id,
        status: queuedJob.status
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error in creating the queued job'
    });
  }
});

module.exports = {
  handler,
  tags: [TAGS.BATCH_PROCESSING],
  description: 'Creates a job to queue',
  input: {
    payload: Joi.object({
      type: Joi.string().valid(Object.values(BATCH.JOB_TYPES)).required().example('test-job')
        .description('Job type'),
      parameters: Joi.any().example({ performanceYear: 2017 })
    })
  },
  output: {
    schema: UserVisibleSchema,
    status: {
      202: 'Returns a success message if job was queued',
      500: 'Unexpected error'
    }
  }
};
