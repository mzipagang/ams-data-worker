const logger = require('../../../../../../services/logger');
const Json2csvTransform = require('json2csv').Transform;
const map = require('map-stream');
const validation = require('../../../../../../services/validation');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');

const FileImportGroup = require('../../../../../../services/mongoose/models/file_import_group');
const SourceFileEntityData = require('../../../../../../services/mongoose/models/source_file_entity_data');
const SourceFileEntityEligibilityData =
  require('../../../../../../services/mongoose/models/source_file_entity_eligibility');
const SourceFileProviderData = require('../../../../../../services/mongoose/models/source_file_provider_data');
const SourceFileBeneficiaryData = require('../../../../../../services/mongoose/models/source_file_beneficiary_data');
const QpcScore = require('../../../../../../services/mongoose/dynamic_name_models/pac_qp_category_score');
const QpcScoreSummary = require('../../../../../../services/mongoose/dynamic_name_models/pac_qp_category_score_summary');

const streamify = require('stream-array');

const { FileImportSourceMap } = require('../../../../../../services/apm-models');

const FILE_IMPORT_TYPES = {
  'apm-entity': SourceFileEntityData,
  'entity-eligibility': SourceFileEntityEligibilityData,
  'apm-provider': SourceFileProviderData,
  beneficiary: SourceFileBeneficiaryData,
  beneficiary_mdm: SourceFileBeneficiaryData,
  'pac-qp_category_score': [QpcScore, QpcScoreSummary]
};


const FILE_IMPORT_TYPE_SHARED_MAP = {
  'apm-entity': 'apm_entity',
  'entity-eligibility': 'entity_eligibility',
  'apm-provider': 'apm_provider',
  beneficiary: 'beneficiary',
  beneficiary_mdm: 'beneficiary_mdm',
  'pac-qp_category_score': 'pac-qp_category_score'
};

// TODO: this needs to be refactor on a project level when we organize all the file import types, file types, etc.
// But for now we only have 2 dynamic models when trying to download validations
const isDynamicModel = (model) => {
  return model === QpcScore || model === QpcScoreSummary;
}

/**
 * Ensures that the Mongoose.Model that is passed in correctly resolves even if it is a function
 * that returns a Mongoose.Model (i.e. it is dynamic). It is essentially a pass-through function
 * if the Mongoose.Model is static
 * @param {Mongoose.Model | function} modelOrFunction a Mongoose.Model or a function that given an ID returns a Mongoose.Model
 * @param {string} fileId the id that is passed if modelOrFunction is a function to retrieve the dynamic Mongoose.Model
 * @returns {Mongoose.Model} a Mongoose.Model
 */
const safeGetModel = (modelOrFunction, fileId) => {
  if (isDynamicModel(modelOrFunction)) {
    return modelOrFunction(fileId);
  }

  return modelOrFunction;
}

/**
 * Retrieves the necessary Mongoose.Model or Models that are required to retrieve the validations for the given fileId
 * @param {*} fileType the FileType to retrive the Mongoose.Model or Models
 * @param {*} fileId the ID of the file
 * @returns {Mongoose.Model | [Mongoose.Model]} a Mongoose.Model or an array of Mongoose.Models used to retrieve the validations for the given fileId
 */
const getModelByFileType = (fileType, fileId) => {
  const modelOrModels = FILE_IMPORT_TYPES[fileType];

  if (Array.isArray(modelOrModels)) {
    return modelOrModels.map(m => safeGetModel(m, fileId));
  }

  return safeGetModel(modelOrModels, fileId);
};

const getCsvSourceCursor = async (fileType, fileId) => {
  const modelOrModels = getModelByFileType(FileImportSourceMap[fileType], fileId);

  if (!Array.isArray(modelOrModels)) {
    const model = safeGetModel(modelOrModels, fileId);
    return model.find({ file_id: fileId }).lean().cursor();
  }

  let records = [];
  for (const model of modelOrModels) {
    const results = await model.find({ file_id: fileId }).lean();
    records = records.concat(results);
  }

  return streamify(records);

};

const handler = async (req, res) => {
  try {
    const fileId = req.params.id;

    const fileImportGroup = await FileImportGroup.findOne().elemMatch('files', { id: fileId }).exec();
    const file = fileImportGroup.files.find(f => (f.id === fileId));

    if (!file) {
      return res.status(404).json({ message: 'Cannot find file for download' });
    }

    const isValidTypeForValidation = FILE_IMPORT_TYPES[FileImportSourceMap[file.import_file_type]];

    if (!isValidTypeForValidation) {
      return res.status(422).json({
        message: 'Validation results cannot be provided for this file type'
      });
    }

    const fields = ['line_number', 'error_type', 'error_code', 'error'];
    const opts = { fields, quote: '\'' };
    const transformOpts = { highWaterMark: 16384, encoding: 'utf-8' };
    const json2csv = new Json2csvTransform(opts, transformOpts);

    const source = await getCsvSourceCursor(file.import_file_type, file.id);

    const result = source
      .pipe(map((record, callback) => {
        if (!record.validations || !record.validations.length > 0) {
        // this acts as a filter; if there are no validation errors/warnings for this record then we don't need to include
        // it in the generated CSV file
        // we use `setImmediate` to limit the call stack size
          return setImmediate(() => callback());
        }

        const newValidation = validation.applyValidationHierarchy(record.validations,
          FILE_IMPORT_TYPE_SHARED_MAP[FileImportSourceMap[file.import_file_type]])
          .map(validations => Object.assign({}, validations, { line_number: record.meta.file_line_number }));

        // we use `setImmediate` to limit the call stack size
        return setImmediate(() => callback(null, JSON.stringify(newValidation)));
      }))
      .pipe(json2csv)
      .pipe(res);
    return result;
  } catch (err) {
    logger.error(err);
    return res.status(500).json({ message: 'There was an error' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.VALIDATION],
  description: 'Returns a file containing a summary of the validations for the given file',
  input: {
    params: Joi.object({
      fileId: Joi.string().required()
    })
  },
  output: {
    schema: undefined,
    status: {
      200: 'Returns the validation summary as a CSV',
      404: 'Cannot find file for download',
      500: 'There was an error while retrieving APM Model with id of #'
    }
  }
};
