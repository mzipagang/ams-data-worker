const sessionCheckMiddleware = require('../../../services/sessionCheckMiddleware');

module.exports = (router) => {
  router.use(sessionCheckMiddleware);

  // In order to check permissions, use the ensurePermission middleware
  // permission check must always be after session check

  // router.get('/', ensurePermission('report'));

  return router;
};
