const asyncHandler = require('express-async-handler');
const SessionService = require('../../../../services/SessionService');

const handler = asyncHandler(SessionService.createSession);

module.exports = {
  handler
};
