const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { Schema } = require('../../../../schemas/User');
const SessionService = require('../../../../services/SessionService');
const { TAGS } = require('../../../../constants');

const handler = asyncHandler(SessionService.getSession);

module.exports = {
  handler,
  tags: [TAGS.USER],
  description: 'Checks whether still logged in',
  input: {
    query: Joi.object({
      token: Joi.string(),
      extend: Joi.boolean()
    })
  },
  output: {
    schema: Joi.object({
      data: {
        user: Schema
      }
    }),
    status: {
      200: 'Returns the user',
      500: 'Returns null'
    }
  }
};
