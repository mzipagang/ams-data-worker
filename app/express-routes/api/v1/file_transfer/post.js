const Busboy = require('busboy');
const logger = require('../../../../services/logger');
const AWS = require('../../../../services/aws');

const handler = (req, res) => {
  const busboy = new Busboy({ headers: req.headers, limits: { files: 1 } });
  const uploads = [];

  busboy.on('file', async (_fieldname, file, _filename, _encoding, mimetype) => {
    uploads.push(
      AWS.util.uploadS3(req.params[0], file, mimetype, req.query.meta)
    );
  });

  busboy.on('finish', () => {
    Promise.all(uploads)
      .then(() => {
        if (uploads.length === 0) {
          throw new Error('No files were found in request.');
        }
        res.json({ data: { success: true } });
      })
      .catch((error) => {
        logger.error(error);
        res.status(500).json({ data: { success: false, message: 'Error while uploading' } });
      });
  });

  req.pipe(busboy);
};

module.exports = {
  handler
};
