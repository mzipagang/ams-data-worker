const Promise = require('bluebird');
const logger = require('../../../../services/logger');
const aws = require('../../../../services/aws');

module.exports = (req, res) => Promise.try(() => {
  aws.util.getObject(req.params[0])
    .on('httpHeaders', function handler(statusCode, headers) {
      res.set('Content-Length', headers['content-length']);
      res.set('Content-Type', headers['content-type']);
      this.response.httpResponse.createUnbufferedStream()
        .pipe(res);
    })
    .send();
})
  .catch((err) => {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  });
