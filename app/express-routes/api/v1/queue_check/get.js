const logger = require('../../../../services/logger');
const createSQSAPIMessage = require('../../../../services/createSQSAPIMessage');
const Joi = require('joi');
const { TAGS } = require('../../../../constants');

const handler = async (_req, res) => {
  try {
    await createSQSAPIMessage(1, 'GENERATE_QP_METRICS_REPORT', { performanceYear: 2017, runSnapshot: 'F' });
    res.status(200).json({ success: true });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ success: false });
  }
};

module.exports = {
  handler,
  tags: [TAGS.HEALTH_CHECK],
  description: 'Checks the AMS queue availability',
  output: {
    schema: Joi.object({
      success: Joi.boolean().required()
    }),
    status: {
      200: 'Returns success value as true if queue worked',
      500: 'Returns success value as false if queue did not work'
    }
  }
};

