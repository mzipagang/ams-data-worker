const logger = require('../../../../services/logger');
const User = require('../../../../services/mongoose/models/user');
const { Schema } = require('../../../../schemas/User');
const Joi = require('joi');
const { TAGS } = require('../../../../constants');

const handler = async (_req, res) => {
  try {
    const users = await User.find();

    res.status(200).json({
      data: {
        users
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while getting the user'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.USER],
  description: 'Retrieve users',
  output: {
    schema: Joi.object({
      data: Joi.object({
        users: Joi.array().items(Schema)
      })
    }),
    status: {
      200: 'Return users',
      500: 'There was an error while retrieving a list of users'
    }
  }
};
