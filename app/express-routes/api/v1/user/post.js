const logger = require('../../../../services/logger');
const User = require('../../../../services/mongoose/models/user');
const { validate } = require('../../../../schemas');
const UserSchema = require('../../../../schemas/User').Schema;

const handler = (req, res) => {
  if (!validate(req.body, UserSchema).error) {
    User.upsertUser(req.body)
      .then((user) => {
        res.status(200).json({
          data: {
            user
          }
        });
      })
      .catch((err) => {
        logger.error(err);
        res.status(500).json({
          message: 'There was an error while saving the user'
        });
      });
  } else {
    res.status(400).json({
      data: {
        message: 'The request was malformed'
      }
    });
  }
};

module.exports = {
  handler
};
