const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { deleteSession } = require('../../../../../services/SessionService');
const { TAGS } = require('../../../../../constants');

const handler = asyncHandler(async (req, res) => {
  const token = req.headers.authorization;
  if (token) {
    deleteSession({ query: { token: token.replace('Bearer ', '') } }, res);
  } else {
    res.status(400).json({ message: 'No token provided to logout' });
  }
});

module.exports = {
  handler,
  tags: [TAGS.USER],
  description: 'End session token',
  output: {
    schema: Joi.object({
      data: Joi.object({
        success: Joi.boolean().example(true)
      })
    }),
    status: {
      200: 'Returns true if successful',
      400: 'No token provided to logout',
      500: 'Unexpected error'
    }
  }
};
