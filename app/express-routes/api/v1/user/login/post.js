const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const logger = require('../../../../../services/logger');
const User = require('../../../../../services/mongoose/models/user');
const { createSession } = require('../../../../../services/SessionService');
const EUALDAP = require('../../../../../services/euaLdap');
const { TAGS } = require('../../../../../constants');

const handler = asyncHandler(async (req, res) => {
  try {
    const usernameIn = req.body.username;
    const password = req.body.password;

    if (Joi.validate(usernameIn, Joi.string()).error || Joi.validate(password, Joi.string()).error) {
      res.status(400).json({ message: 'A valid username and password is required' });
      return;
    }

    const username = usernameIn.toLowerCase();

    const euaUser = await EUALDAP.checkUserPassword(username, password);

    if (!euaUser) {
      res.status(403).json({ message: 'Sorry, your username and/or password are incorrect' });
      return;
    }

    const upsertedUser = await User.upsertUser({
      username,
      mail: euaUser.mail,
      firstName: euaUser.givenName,
      lastName: euaUser.sn
    });

    await createSession({
      body: {
        username: upsertedUser.username,
        email: upsertedUser.mail,
        firstName: upsertedUser.firstName,
        lastName: upsertedUser.lastName,
        roles: upsertedUser.roles,
        permissions: upsertedUser.permissions
      }
    }, res);
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Unexpected error' });
  }
});

module.exports = {
  handler,
  tags: [TAGS.USER],
  description: 'Get session token from username and password',
  input: {
    payload: Joi.object({
      username: Joi.string().example('bbid').description('Username'),
      password: Joi.string().example('test').description('Password')
    }).description('Credentials')
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        token: Joi.string().example('eyJhbGciOiJIUzI1NiJ9.e30.ZRrHA1JJJW8opsbCGfG_HACGpVUMN_a9IV7pAx_Zmeo')
      })
    }),
    status: {
      200: 'Returns a session token',
      400: 'A valid username and password is required',
      403: 'Sorry, your username and/or password are incorrect',
      500: 'Unexpected error'
    }
  }
};
