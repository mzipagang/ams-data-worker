const logger = require('../../../../../services/logger');
const User = require('../../../../../services/mongoose/models/user');
const Joi = require('joi');
const { Schema } = require('../../../../../schemas/User');

const handler = (req, res) => {
  User.getUser(req.params.username)
    .then((user) => {
      res.status(200).json({
        data: {
          user
        }
      });
    })
    .catch((err) => {
      logger.error(err);
      res.status(500).json({
        message: 'There was an error while getting the user'
      });
    });
};

module.exports = {
  handler,
  description: 'Retrieve users',
  output: {
    schema: Joi.object({
      data: Joi.object({
        users: Joi.array().items(Schema)
      })
    }),
    status: {
      200: 'Return all users',
      500: 'There was an error while retrieving a list of users'
    }
  }
};
