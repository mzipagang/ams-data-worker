const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { getSession } = require('../../../../../services/SessionService');
const { TAGS } = require('../../../../../constants');

const handler = asyncHandler(async (req, res) => {
  const token = req.headers.authorization;
  if (token) {
    getSession({ query: { token: token.replace('Bearer ', '') } }, res);
  } else {
    res.status(400).json({ message: 'No token provided' });
  }
});

module.exports = {
  handler,
  tags: [TAGS.USER],
  description: 'Gets the session info from a given token',
  output: {
    schema: Joi.object({
      data: Joi.object({
        session_info: Joi.object({
          success: Joi.boolean().example(true).description('whether the request was successfully made'),
          expiresInSeconds: Joi.number().integer().example(100)
            .description('how many seconds until the session token expires'),
          isActive: Joi.boolean().example(true).description('tells if the token is still active')
        }).label('SessionInfo')
      })
    }),
    status: {
      200: 'Return session info',
      400: 'No token provided',
      500: 'Unexpected error'
    }
  }
};
