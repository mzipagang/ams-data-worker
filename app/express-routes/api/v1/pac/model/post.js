const logger = require('../../../../../services/logger');
const PacModel = require('../../../../../services/mongoose/models/pac_model');
const DataLog = require('../../../../../services/mongoose/models/data_log');
const { Schema } = require('../../../../../schemas/PacModel');
const { validate } = require('../../../../../schemas');
const moment = require('moment');
const Joi = require('joi');
const { TAGS } = require('../../../../../constants');

const handler = async (req, res) => {
  try {
    if (req.body.start_date) {
      req.body.start_date = moment.utc(req.body.start_date).startOf('day').toDate();
    }
    if (req.body.end_date) {
      req.body.end_date = moment.utc(req.body.end_date).startOf('day').toDate();
    }
    const validationResult = validate(req.body, Schema);

    if (validationResult.error) {
      return res.status(422).json({ message: validationResult.error.toString() });
    }
    const pacModel = new PacModel(req.body);
    const models = await PacModel.find({}, { id: true }).lean();

    if (!req.body.id) {
      pacModel.id = PacModel.createNewPacModelId(models);
    }

    const idAlreadyExists = models.some(model => model.id === pacModel.id);
    if (idAlreadyExists) {
      return res.status(409).json({ message: `id ${pacModel.id} already exists` });
    }

    const result = await pacModel.save();

    await DataLog.createLog(DataLog.actions.CREATE, pacModel, null);

    return res.status(200).json({
      pac_model: result
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({ message: 'There was an error while creating a new PAC Model' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.PAC_MODEL],
  description: 'Create a new APM Model',
  input: {
    payload: Schema
  },
  output: {
    schema: Joi.object({
      pac_model: Schema
    }),
    status: {
      200: 'Return newly created APM Model',
      409: 'id # already exists',
      422: 'Failed validation',
      500: 'There was an error while creating a new APM Model'
    }
  }
};
