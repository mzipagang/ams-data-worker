const logger = require('../../../../../../services/logger');
const PacModel = require('../../../../../../services/mongoose/models/pac_model');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');
const { Schema } = require('../../../../../../schemas/PacModel');

const handler = async (req, res) => {
  try {
    const pacModel = await PacModel.findOne({ id: req.params.apmId });

    res.status(200).json({
      pac_model: pacModel
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: `There was an error while retrieving PAC Model with id of ${req.params.apmId}` });
  }
};

module.exports = {
  handler,
  tags: [TAGS.PAC_MODEL],
  description: 'Get the APM Model with the given ID',
  input: {
    params: Joi.object({
      apmId: Joi.string().required()
    })
  },
  output: {
    schema: Joi.object({
      pac_model: Schema
    }),
    status: {
      200: 'Return APM model and metadata',
      500: 'There was an error while retrieving APM Model with id of #'
    }
  }
};
