const logger = require('../../../../../../services/logger');
const PacModel = require('../../../../../../services/mongoose/models/pac_model');
const DataLog = require('../../../../../../services/mongoose/models/data_log');
const { Schema } = require('../../../../../../schemas/PacModel');
const { validate } = require('../../../../../../schemas');
const moment = require('moment');
const { getUsername } = require('../../../../../../services/utils/jwt');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');

const handler = async (req, res) => {
  try {
    if (req.body.start_date) {
      req.body.start_date = moment.utc(req.body.start_date)
        .startOf('day')
        .toDate();
    }
    if (req.body.end_date) {
      req.body.end_date = moment.utc(req.body.end_date)
        .startOf('day')
        .toDate();
    }
    const validationResult = validate(req.body, Schema);

    if (validationResult.error) {
      return res.status(422)
        .json({ message: validationResult.error.toString() });
    }
    req.body.updated_by = getUsername(req);
    req.body.updated_date = moment.utc().toDate();
    const update = await PacModel.findOneAndUpdate({ id: req.params.apmId }, req.body, { upsert: true, new: true });
    await DataLog.createLog(DataLog.actions.UPDATE, update, null);
    return res.status(200).json({
      result: update
    });
  } catch (err) {
    logger.error(err);
    return res.status(500)
      .json({ message: 'There was an error while updating the PAC Model' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.PAC_MODEL],
  description: 'Update the APM Model with the given ID',
  input: {
    params: Joi.object({
      apmId: Joi.string().required()
    }),
    payload: Schema
  },
  output: {
    schema: Joi.object({
      result: Schema
    }),
    status: {
      200: 'Returns the newly edited APM Model',
      404: 'There was an error while finding the APM Model',
      500: 'There was an error while creating a new APM Model'
    }
  }
};
