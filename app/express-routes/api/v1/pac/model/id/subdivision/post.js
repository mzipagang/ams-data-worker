const logger = require('../../../../../../../services/logger');
const PacSubdivision = require('../../../../../../../services/mongoose/models/pac_subdivision');
const DataLog = require('../../../../../../../services/mongoose/models/data_log');
const { Schema } = require('../../../../../../../schemas/Subdivision');
const { validate } = require('../../../../../../../schemas');
const moment = require('moment');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../constants');

const handler = async (req, res) => {
  try {
    req.body.apm_id = req.params.apmId;
    if (req.body.start_date) {
      req.body.start_date = moment.utc(req.body.start_date).startOf('day').toDate();
    }
    if (req.body.end_date) {
      req.body.end_date = moment.utc(req.body.end_date).startOf('day').toDate();
    }
    const validationResult = validate(req.body, Schema);

    if (validationResult.error) {
      return res.status(422).json({ message: validationResult.error.toString() });
    }

    const pacSubdivision = new PacSubdivision(req.body);
    if (!req.body.id) {
      const subdivisions = await PacSubdivision.find({}, { id: true }).lean();
      pacSubdivision.id = PacSubdivision.createNewPacSubdivisionId(subdivisions);
    }

    const result = await pacSubdivision.save();
    await DataLog.createLog(DataLog.actions.CREATE, result, null);

    return res.status(200).json({
      pac_subdivision: result
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({ message: 'There was an error while creating the PAC Subdivision' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.SUBDIVISION],
  description: 'Create a new APM Subdivision',
  input: {
    params: Joi.object({
      apmId: Joi.string().required()
    }),
    payload: Schema
  },
  output: {
    schema: Joi.object({
      pac_subdivision: Schema
    }),
    status: {
      200: 'Create a new APM Subdivision',
      422: 'Failed validation',
      500: 'There was an error while creating a new APM Subdivision'
    }
  }
};
