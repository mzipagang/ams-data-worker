const logger = require('../../../../../../../../services/logger');
const PacSubdivision = require('../../../../../../../../services/mongoose/models/pac_subdivision');
const DataLog = require('../../../../../../../../services/mongoose/models/data_log');
const { Schema } = require('../../../../../../../../schemas/Subdivision');
const { validate } = require('../../../../../../../../schemas');
const moment = require('moment');
const { getUsername } = require('../../../../../../../../services/utils/jwt');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../../constants');

const handler = async (req, res) => {
  try {
    req.body.apm_id = req.params.apmId;
    if (req.body.start_date) {
      req.body.start_date = moment.utc(req.body.start_date).startOf('day').toDate();
    }
    if (req.body.end_date) {
      req.body.end_date = moment.utc(req.body.end_date).startOf('day').toDate();
    }
    const validationResult = validate(req.body, Schema);

    if (validationResult.error) {
      return res.status(422).json({ message: validationResult.error.toString() });
    }
    req.body.updated_by = getUsername(req);
    req.body.updated_date = moment.utc().toDate();
    const update = await PacSubdivision.findOneAndUpdate({
      apm_id: req.params.apmId,
      id: req.params.subdivisionId
    }, req.body, { new: true });
    await DataLog.createLog(DataLog.actions.UPDATE, update, null);

    return res.status(200).json({
      result: update
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({ message: 'There was an error while updating the PAC Subdivision' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.SUBDIVISION],
  description: 'Update the APM Subdivision with the given ID',
  input: {
    params: Joi.object({
      apmId: Joi.string().required(),
      subdivisionId: Joi.string().required()
    }),
    payload: Schema
  },
  output: {
    schema: Joi.object({
      result: Schema
    }),
    status: {
      200: 'Returns the result of the operation',
      404: 'There was an error while finding the APM Subdivision',
      500: 'There was an error while updating the APM Subdivision'
    }
  }
};
