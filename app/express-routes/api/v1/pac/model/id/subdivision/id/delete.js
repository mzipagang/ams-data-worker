const logger = require('../../../../../../../../services/logger');
const PacSubdivision = require('../../../../../../../../services/mongoose/models/pac_subdivision');
const DataLog = require('../../../../../../../../services/mongoose/models/data_log');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../../constants');

const handler = async (req, res) => {
  try {
    const deleteResult = await PacSubdivision.findOneAndRemove({
      apm_id: req.params.apmId,
      id: req.params.subdivisionId
    });

    await DataLog.createLog(DataLog.actions.DELETE, deleteResult, null);

    res.status(200).json({
      success: deleteResult !== null
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while deleting the PAC Subdivision' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.SUBDIVISION],
  description: 'Delete the APM Subdivision with the given ID',
  input: {
    params: Joi.object({
      apmId: Joi.string().required(),
      subdivisionId: Joi.string().required()
    })
  },
  output: {
    schema: Joi.object({
      success: Joi.boolean().required()
    }),
    status: {
      200: 'Returns the result of the operation',
      500: 'There was an error while deleting the APM Subdivision'
    }
  }
};
