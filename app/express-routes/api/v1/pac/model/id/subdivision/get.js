const logger = require('../../../../../../../services/logger');
const PacSubdivision = require('../../../../../../../services/mongoose/models/pac_subdivision');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../constants');
const { Schema } = require('../../../../../../../schemas/Subdivision');

const handler = async (req, res) => {
  try {
    const all = await PacSubdivision.find({ apm_id: req.params.apmId }).lean();

    res.status(200).json({
      pac_subdivisions: all
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while retrieving a list of PAC Subdivisions' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.SUBDIVISION],
  description: 'Retrieve all APM subdivisions associated with a APM model',
  input: {
    params: Joi.object({
      apmId: Joi.string().required()
    })
  },
  output: {
    schema: Joi.object({
      pac_subdivisions: Joi.array().items(Schema)
    }),
    status: {
      200: 'Return all APM subdivisions and metadata',
      500: 'There was an error while retrieving a list of APM Subdivisions'
    }
  }
};

