const logger = require('../../../../../../services/logger');
const PacModel = require('../../../../../../services/mongoose/models/pac_model');
const DataLog = require('../../../../../../services/mongoose/models/data_log');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');

const handler = async (req, res) => {
  try {
    const deleteResult = await PacModel.findOneAndRemove({ id: req.params.apmId });
    await DataLog.createLog(DataLog.actions.DELETE, deleteResult, null);

    res.status(200).json({
      success: deleteResult !== null
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while deleting the PAC Model' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.PAC_MODEL],
  description: 'Delete the APM Model with the given ID',
  input: {
    params: Joi.object({
      apmId: Joi.string().required()
    })
  },
  output: {
    schema: Joi.object({
      success: Joi.boolean().required()
    }),
    status: {
      200: 'Returns the result of the operation',
      500: 'There was an error while deleting the APM Model'
    }
  }
};
