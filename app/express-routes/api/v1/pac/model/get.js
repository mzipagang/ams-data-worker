const logger = require('../../../../../services/logger');
const PacModel = require('../../../../../services/mongoose/models/pac_model');
const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/PacModel');

const handler = async (_req, res) => {
  try {
    const all = await PacModel.find({});

    res.status(200).json({
      pac_models: all
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while retrieving a list of PAC Models' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.PAC_MODEL],
  description: 'Retrieve all APM Models and metadata',
  output: {
    schema: Joi.object({
      pac_models: Joi.array().items(Schema)
    }),
    status: {
      200: 'Return all APM models and metadata',
      500: 'There was an error while retrieving a list of APM Models'
    }
  }
};
