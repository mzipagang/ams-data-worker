const Joi = require('joi');
const { TAGS } = require('../../../../constants');

module.exports = {
  handler: () => {},
  tags: [TAGS.EMAIL],
  description: 'Send a notification email',
  input: {
    payload: Joi.object({
      fileName: Joi.string().example('testFile.txt').description('Name of file being imported'),
      status: Joi.string().example('file was successfully uploaded').description('Status of file importing'),
      id: Joi.string().example('XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX')
        .description('Internal file id for processing file')
    }).description('Email parameters for notification email')
  },
  output: {
    schema: Joi.object({
      accepted: Joi.string().example('fake@semanticbits.com')
        .description('recipient addresses that were accepted by the server'),
      rejected: Joi.string().example('fake@semanticbits.com')
        .description('recipient addresses that were rejected by the server'),
      response: Joi.string().example('250 Ok').description('SMTP response from the server'),
      envelope: Joi.any().description('envelope object for the message'),
      messageId: Joi.string().example('<XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX@semanticbits.com>')
        .description('message id for sent message')
    }),
    status: {
      200: 'Returns a success message if email was sent',
      500: 'Unexpected error'
    }
  }
};
