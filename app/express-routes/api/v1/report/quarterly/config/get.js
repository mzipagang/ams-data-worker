const asyncHandler = require('express-async-handler');
const logger = require('../../../../../../services/logger');
const QuarterlyReportConfig = require('../../../../../../services/mongoose/models/quarterly_report_config');
const moment = require('moment');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');

const handler = asyncHandler(async (_req, res) => {
  try {
    let config = await QuarterlyReportConfig
      .findOne({
        quarter1_start: { $lte: new Date() },
        quarter4_end: { $gte: new Date() }
      }).lean();

    if (!config) {
      config = await QuarterlyReportConfig
        .findOne({
          year: moment().year(),
          quarter1_start: { $gt: new Date() }
        }).lean();
    }

    if (!config) {
      return res.status(404).json({
        message: 'Config not found'
      });
    }
    return res.status(200).json({
      data: {
        quarterly_report_config: config
      }
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({
      message: `There was an error getting a quarterly report config: ${err}`
    });
  }
});

module.exports = {
  handler,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Gets the quarterly report configuration',
  output: {
    schema: Joi.object({
      data: Joi.object({
        quarterly_report_config: Joi.object({
          id: Joi.string().required().example('6bf148a7-9950-4589-8584-93b751a1e813')
            .description('unique quarterly report config id'),
          quarter1_start: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 1st reporting quarter opens (inclusive)'),
          quarter1_end: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 1st reporting quarter ends (inclusive)'),
          quarter2_start: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 2nd reporting quarter opens (inclusive)'),
          quarter2_end: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 2nd reporting quarter ends (inclusive)'),
          quarter3_start: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 3rd reporting quarter opens (inclusive)'),
          quarter3_end: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 3rd reporting quarter ends (inclusive)'),
          quarter4_start: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 4th reporting quarter opens (inclusive)'),
          quarter4_end: Joi.date().example('Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)')
            .description('date the 4th reporting quarter ends (inclusive)')
        }).label('QuarterlyReportConfig')
      })
    }),
    status: {
      200: 'Returns the quarterly report configuration',
      500: 'Unexpected error'
    }
  }
};

// *       id:
//  *         description: unique quarterly report config id
//  *         required: true
//  *         type: string
//  *         example: 6bf148a7-9950-4589-8584-93b751a1e813
//  *       quarter1_start:
//  *         description: date the 1st reporting quarter opens (inclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  *       quarter1_end:
//  *         description: date the 1st reporting quarter ends (exclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  *       quarter2_start:
//  *         description: date the 2nd reporting quarter opens (inclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  *       quarter2_end:
//  *         description: date the 2nd reporting quarter ends (exclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  *       quarter3_start:
//  *         description: date the 3rd reporting quarter opens (inclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  *       quarter3_end:
//  *         description: date the 3rd reporting quarter ends (exclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  *       quarter4_start:
//  *         description: date the 4th reporting quarter opens (inclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  *       quarter4_end:
//  *         description: date the 4th reporting quarter ends (exclusive)
//  *         type: date
//  *         example: Thu Jul 19 2018 13:14:47 GMT-0400 (Eastern Daylight Time)
//  */
