const QuarterlyReport = require('../../../../../../../services/mongoose/models/quarterly_report');
const logger = require('../../../../../../../services/logger');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../constants');

class ReportOptions {
  static getAvailableYearsForReports() {
    return QuarterlyReport
      .distinct('year')
      .lean();
  }

  static async getReportOptions(req, res) {
    try {
      const options = await ReportOptions.getAvailableYearsForReports();
      return res.status(200).json({ options });
    } catch (err) {
      logger.error(err);
      return res.status(500)
        .json({ message: 'There was an error in getting the year options.' });
    }
  }
}

module.exports = {
  handler: ReportOptions.getReportOptions,
  ReportOptions,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Retrieve all years that are available for existing model reports',
  output: {
    schema: Joi.object({
      options: Joi.array().items(
        Joi.number().integer().valid(1, 2, 3, 4).example(1)
      )
    }),
    status: {
      200: 'Return all years for available existing model reports',
      500: 'Unexpected error'
    }
  }
};
