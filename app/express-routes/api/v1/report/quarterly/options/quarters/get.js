const QuarterlyReport = require('../../../../../../../services/mongoose/models/quarterly_report');
const logger = require('../../../../../../../services/logger');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../constants');

class ReportOptions {
  static getAvailableQuartersForYear(year) {
    return QuarterlyReport
      .distinct('quarter', { year })
      .lean();
  }

  static async getReportOptions(req, res) {
    try {
      const year = req.query.year;
      if (!year || isNaN(year)) {
        return res.status(422).json({ message: 'Invalid value for year' });
      }
      const options = await ReportOptions.getAvailableQuartersForYear(year);

      return res.status(200).json({ options });
    } catch (err) {
      logger.error(err);
      return res.status(500)
        .json({ message: 'There was an error in getting the quarter for year provided' });
    }
  }
}

module.exports = {
  handler: ReportOptions.getReportOptions,
  ReportOptions,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Retrieve all quarters that are available for existing model reports',
  input: {
    query: Joi.object({
      year: Joi.number().integer().min(2017).example(2017)
        .description('year in which report was created')
    })
  },
  output: {
    schema: Joi.object({
      options: Joi.array().items(
        Joi.number().integer().valid(1, 2, 3, 4).example(1)
      )
    }),
    status: {
      200: 'Return all quarters available existing model report year',
      500: 'Unexpected error'
    }
  }
};
