const QuarterlyReport = require('../../../../../../services/mongoose/models/quarterly_report');
const logger = require('../../../../../../services/logger');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');
const { Schema } = require('../../../../../../schemas/QuarterlyReport');

class HistoricalPastReports {
  static getReportsByYearAndQuarter(year, quarter) {
    return QuarterlyReport
      .find({ year, quarter })
      .lean();
  }

  static getAllReports() {
    return QuarterlyReport
      .find({})
      .lean();
  }

  static async getReportList(req, res) {
    try {
      const year = req.query.year;
      const quarter = req.query.quarter;

      if (year && isNaN(year)) {
        return res.status(422).json({
          message: 'Invalid value for year'
        });
      } else if (quarter && isNaN(quarter)) {
        return res.status(422).json({
          message: 'Invalid value for quarter'
        });
      }

      const reports = year && quarter ? await HistoricalPastReports.getReportsByYearAndQuarter(year, quarter)
        : await HistoricalPastReports.getAllReports();
      return res.status(200).json({ reports });
    } catch (err) {
      logger.error(err);
      return res.status(500).json({
        message: 'There was an error in getting the historical reports'
      });
    }
  }
}

module.exports = {
  handler: HistoricalPastReports.getReportList,
  HistoricalPastReports,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Returns the history of reports submitted for during a quarter and year',
  input: {
    query: Joi.object({
      quarter: Joi.number().integer().valid(1, 2, 3, 4).description('quarter in which report was created'),
      year: Joi.number().integer().min(2017).example(2017)
        .description('year in which report was created')
    })
  },
  output: {
    schema: Joi.object({
      reports: Joi.array().items(Schema)
    }),
    status: {
      200: 'Return a list of quarters and years for previously submitted reports',
      500: 'Unexpected error'
    }
  }
};

