const QuarterlyReport = require('../../../../../../../services/mongoose/models/quarterly_report');
const logger = require('../../../../../../../services/logger');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../constants');

class ModelPastReports {
  static async getReportListByModelId(req, res) {
    try {
      const modelId = req.query.apm_id;
      if (!modelId || isNaN(modelId)) {
        return res.status(422).json({
          message: 'Invalid value for apm_id'
        });
      }
      const reports = await QuarterlyReport
        .find({ model_id: req.query.apm_id })
        .select('year quarter')
        .lean();
      return res.status(200).json({ reports });
    } catch (err) {
      logger.error(err);
      return res.status(500).json({
        message: 'There was an error in getting the model historical reports'
      });
    }
  }
}

module.exports = {
  handler: ModelPastReports.getReportListByModelId,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Returns the history of reports submitted for a model',
  input: {
    query: Joi.object({
      apm_id: Joi.string().description('apm model id')
    })
  },
  output: {
    schema: Joi.object({
      reports: Joi.array().items(Joi.object({
        year: Joi.number().integer().min(2017).example(2018)
          .description('performance year in which report is created'),
        quarter: Joi.number().integer().example(1)
          .description('quarter of the year in which report is created')
      }))
    }),
    status: {
      200: 'Return a list of quarters and years for previously submitted reports',
      500: 'Unexpected error'
    }
  }
};
