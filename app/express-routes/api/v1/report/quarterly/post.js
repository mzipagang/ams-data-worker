const QuarterlyReport = require('../../../../../services/mongoose/models/quarterly_report');
const { getUsername } = require('../../../../../services/utils/jwt');
const logger = require('../../../../../services/logger');
const moment = require('moment');
const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/QuarterlyReport');

const handler = asyncHandler(async (req, res) => {
  try {
    req.body.created_by = getUsername(req);
    req.body.updated_date = moment.utc().toDate();
    const newReport = new QuarterlyReport(req.body);
    await newReport.save();
    res.status(200).json({
      data: {
        quarterly_report: newReport
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: `There was an error saving quarterly report: ${err}`
    });
  }
});


module.exports = {
  handler,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Create a new Quarterly Report',
  input: {
    payload: Schema.required().description('New Quarterly Report to create')
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        quarterly_report: Schema
      })
    }),
    status: {
      200: 'Return all snapshots',
      500: 'Unexpected error'
    }
  }
};
