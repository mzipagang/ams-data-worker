const mongoose = require('mongoose');
const logger = require('../../../../../../services/logger');
const QuarterlyReport = require('../../../../../../services/mongoose/models/quarterly_report');
const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');
const { Schema } = require('../../../../../../schemas/QuarterlyReport');

const handler = asyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    if (!id || !mongoose.Types.ObjectId.isValid(id)) {
      return res.status(422).json({
        message: 'Invalid value for id'
      });
    }
    const report = await QuarterlyReport
      .findOne({ _id: id })
      .lean();

    if (!report) {
      return res.status(404).json({
        message: 'Report not found'
      });
    }
    return res.status(200).json({
      data: {
        quarterly_report: report
      }
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({
      message: 'Error while getting quarterly report'
    });
  }
});

module.exports = {
  handler,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Gets a quarterly report based on report id',
  input: {
    params: Joi.object({
      reportId: Joi.string().required()
    })
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        quarterly_report: Schema
      })
    }),
    status: {
      200: 'Returns the quarterly report for a given report id',
      500: 'Unexpected error'
    }
  }
};
