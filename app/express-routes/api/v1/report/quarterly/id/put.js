const logger = require('../../../../../../services/logger');
const QuarterlyReport = require('../../../../../../services/mongoose/models/quarterly_report');
const { getUsername } = require('../../../../../../services/utils/jwt');
const moment = require('moment');
const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');
const { Schema } = require('../../../../../../schemas/QuarterlyReport');

const handler = asyncHandler(async (req, res) => {
  try {
    req.body.updated_by = getUsername(req);
    req.body.updated_date = moment.utc().toDate();

    const update = await QuarterlyReport.findOneAndUpdate({
      id: req.params.id, quarter: req.body.quarter, year: req.body.year
    }, req.body, { upsert: false, new: true });
    res.status(200)
      .json({
        result: update
      });
  } catch (err) {
    logger.error(err);
    res.status(500)
      .json({ message: 'There was an error while updating the Quarterly Report' });
  }
});

module.exports = {
  handler,
  tags: [TAGS.QUARTERLY_REPORT],
  description: 'Updates a quarterly report based on report id',
  input: {
    params: Joi.object({
      reportId: Joi.string().required()
    })
  },
  output: {
    schema: Joi.object({
      result: Schema
    }),
    status: {
      200: 'Returns the quarterly report for a given report id',
      500: 'Unexpected error'
    }
  }
};
