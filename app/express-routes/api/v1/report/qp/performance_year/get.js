const Joi = require('joi');
const PacSnapshots = require('../../../../../../services/mongoose/models/pac_snapshot');
const logger = require('../../../../../../services/logger');
const asyncHandler = require('express-async-handler');
const { TAGS } = require('../../../../../../constants');

class QppPerformanceYear {
  static getPerformanceYears() {
    return PacSnapshots.distinct('performance_year');
  }

  static async getYears(req, res) {
    try {
      const performanceYears = await QppPerformanceYear.getPerformanceYears();
      res.status(200).json({
        performance_years: performanceYears
      });
    } catch (err) {
      logger.error(err);
      res.status(500).json({
        message: 'There was an error in getting the performance year'
      });
    }
  }
}

module.exports = {
  handler: asyncHandler(QppPerformanceYear.getYears),
  QppPerformanceYear,
  tags: [TAGS.QP_REPORT],
  description: 'Retrieve all performance years that are available for qp reporting',
  output: {
    schema: Joi.object({
      performance_years: Joi.array().items(Joi.number().integer().min(2017).example(2017))
    }),
    status: {
      200: 'Return all performance years',
      500: 'Unexpected error'
    }
  }
};
