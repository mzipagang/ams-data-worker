const mockResponse = {
  totalQP: 97027,
  apm_and_subdivision: [
    {
      name: 'Comprehensive Care for Joint',
      qp: 196,
      partialQp: 50,
      notPartialOrQp: 0
    },
    {
      name: 'Comprehensive Care for Joint 02',
      qp: 196,
      partialQp: 50,
      notPartialOrQp: 0
    },
    {
      name: 'Comprehensive ESRD Care Model',
      qp: 1077,
      partialQp: 0,
      notPartialOrQp: 12
    },
    {
      name: 'Comprehensive ESRD Care Model 04',
      qp: 1000,
      partialQp: 0,
      notPartialOrQp: 12
    },
    {
      name: 'Comprehensive ESRD Care Model 05',
      qp: 80,
      partialQp: 0,
      notPartialOrQp: 12
    },
    {
      name: 'Comprehensive Primary Care Plus',
      qp: 7302,
      partialQp: 530,
      notPartialOrQp: 1
    },
    {
      name: 'Comprehensive Primary Care Plus 09',
      qp: 7302,
      partialQp: 530,
      notPartialOrQp: 1
    },
    {
      name: 'Medicare Shared Savings Program',
      qp: 46277,
      partialQp: 0,
      notPartialOrQp: 1
    },
    {
      name: 'Medicare Shared Savings Program 01',
      qp: 46207,
      partialQp: 0,
      notPartialOrQp: 1
    },
    {
      name: 'Medicare Shared Savings Program 06',
      qp: 289,
      partialQp: 0,
      notPartialOrQp: 1
    },
    {
      name: 'Next Generation ACO Model',
      qp: 41954,
      partialQp: 0,
      notPartialOrQp: 320
    },
    {
      name: 'Total',
      qp: 96107,
      partialQp: 555,
      notPartialOrQp: 365
    }
  ]
};

const handler = (_req, res) => {
  const mockJson = JSON.stringify(mockResponse);
  res.setHeader('Content-Type', 'text/csv; charset=Shift_JIS');
  return res.send(mockJson);
};

module.exports = {
  handler
};
