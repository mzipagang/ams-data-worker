const PacSnapshots = require('../../../../../../services/mongoose/models/pac_snapshot');
const logger = require('../../../../../../services/logger');
const asyncHandler = require('express-async-handler');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');
const PerformanceYear = require('../../../../../../schemas/PerformanceYear').Schema;
const Snapshot = require('../../../../../../schemas/Snapshot').Schema;

class QppSnapshotRun {
  static getSnapshotRun(performanceYear) {
    return PacSnapshots.distinct('run_snapshot', { performance_year: performanceYear });
  }

  static async getRun(req, res) {
    try {
      const performanceYear = req.query.performance_year;

      if (performanceYear === undefined || performanceYear === 'undefined') {
        res.status(200).json({
          snapshot_run: []
        });
      }

      const run = await QppSnapshotRun.getSnapshotRun(performanceYear);

      res.status(200).json({
        snapshot_run: run
      });
    } catch (err) {
      logger.error(err);
      res.status(500).json({
        message: 'There was an error in getting the snapshot run'
      });
    }
  }
}

module.exports = {
  handler: asyncHandler(QppSnapshotRun.getRun),
  QppSnapshotRun,
  tags: [TAGS.QP_REPORT],
  description: 'Retrieve all snapshot runs that are available for qp reporting',
  input: {
    query: Joi.object({
      performance_year: PerformanceYear
    })
  },
  output: {
    schema: Joi.object({
      snapshot_run: Joi.array().items(Snapshot)
    }),
    status: {
      200: 'Return all snapshot run values',
      500: 'Unexpected error'
    }
  }
};
