const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiQPStatusList } = require('../../../../../services/qpp');
const { TAGS } = require('../../../../../constants');

const handler = async (req, res) => {
  try {
    const data = await QPPApiQPStatusList();
    delete data.performance_year;

    res.status(200).json({
      data: {
        apm_data: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  description: 'QP Status Endpoint',
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          npis: Joi.array().items(Joi.object({
            npi: Joi.string().example('1234567890'),
            qp_status: Joi.string().example('Y')
          }))
        }).label('QPPQPStatusListDataV1'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
