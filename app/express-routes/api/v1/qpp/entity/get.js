const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiByEntity } = require('../../../../../services/qpp');
const { TAGS } = require('../../../../../constants');
const { ProviderLVTFields, ProviderFlagFields, ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields } =
  require('../../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    if (!req.headers['x-entity']) {
      return res.status(400).json({
        message: 'x-entity is a required header parameter.'
      });
    }

    const entityIds = req.headers['x-entity'].split(',');

    const data = await QPPApiByEntity(entityIds, null, req.query.publish_history_id);

    return res.status(200).json({
      data: {
        apm_data: data.map(d => ({
          entity_id: d.entity_id,
          entity_name: d.entity_name,
          lvt_flag: d.lvt_flag,
          lvt_payments: d.lvt_payments,
          lvt_patients: d.lvt_patients,
          entity_tin: d.entity_tin,
          apm_id: d.apm_id,
          apm_name: d.apm_name,
          subdivision_id: d.subdivision_id,
          subdivision_name: d.subdivision_name,
          advanced_apm_flag: d.advanced_apm_flag,
          mips_apm_flag: d.mips_apm_flag,
          tins: d.tins
        }))
      }
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  description: 'Entity Endpoint',
  input: {
    headers: Joi.object({
      'x-entity': Joi.string().required().description('Comma delimited list of Entity IDs')
    }),
    query: Joi.object({
      pac_publish_id: Joi.string().description('pac publish id')
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          ...EntityInfoFields,
          ...ProviderLVTFields,
          entity_tin: Joi.string().example('123456789'),
          ...ApmAndSubdivisionInfoFields,
          ...ProviderFlagFields,
          tins: Joi.array().items(Joi.object({
            tin: Joi.string().example('123456789'),
            npis: Joi.array().items(Joi.object({
              npi: Joi.string().example('9876543210'),
              qp_status: Joi.string().example('Y'),
              ...ProviderInfoFields
            }))
          }))
        }).label('QPPEntityDataV1'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
