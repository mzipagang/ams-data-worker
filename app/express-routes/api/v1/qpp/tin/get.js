const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiByTIN } = require('../../../../../services/qpp');
const {
  ProviderLVTFields, ProviderFlagFields,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields
} = require('../../../../../schemas/QPPEntityListData');
const qppUtil = require('../../../../../services/qpp/utility.js');

const handler = async (req, res) => {
  try {
    if (!req.headers['x-tin']) {
      return res.status(400).json({
        message: 'x-tin is a required header parameter.'
      });
    }

    qppUtil.xtinHeaderCheck(req.headers['x-tin']);

    const tins = req.headers['x-tin'].split(',');

    const data = await QPPApiByTIN(tins, null, req.query.publish_history_id);

    return res.status(200).json({
      data: {
        apm_data: data.map(d => ({
          tin: d.tin,
          entities: d.entities.map(entity => ({
            entity_id: entity.entity_id,
            entity_name: entity.entity_name,
            lvt_flag: entity.lvt_flag,
            lvt_payments: entity.lvt_payments,
            lvt_patients: entity.lvt_patients,
            entity_tin: entity.entity_tin,
            npis: entity.npis,
            apm_id: entity.apm_id,
            apm_name: entity.apm_name,
            subdivision_id: entity.subdivision_id,
            subdivision_name: entity.subdivision_name,
            advanced_apm_flag: entity.advanced_apm_flag,
            mips_apm_flag: entity.mips_apm_flag
          }))
        }))
      }
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  description: 'TIN Endpoint',
  input: {
    headers: Joi.object({
      'x-tin': Joi.string().required().description('Comma delimited list of TINs')
    }),
    query: Joi.object({
      pac_publish_id: Joi.string().description('pac publish id')
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          tin: Joi.string().example('123456789'),
          entities: Joi.array().items(Joi.object({
            ...EntityInfoFields,
            ...ProviderLVTFields,
            entity_tin: Joi.string().example('123456789'),
            npis: Joi.array().items(Joi.object({
              npi: Joi.string().example('1234567890'),
              qp_status: Joi.string().example('Y'),
              ...ProviderInfoFields
            })),
            ...ApmAndSubdivisionInfoFields,
            ...ProviderFlagFields
          }))
        })).label('QPPTINDataV1')
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
