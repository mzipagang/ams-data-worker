const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiEntitylist } = require('../../../../../services/qpp');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    const data = await QPPApiEntitylist(null, req.query.publish_history_id);

    res.status(200).json({
      data: {
        apm_data: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  description: 'Entity List Endpoint',
  input: {
    query: Joi.object({
      pac_publish_id: Joi.string().description('pac publish id')
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Schema
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
