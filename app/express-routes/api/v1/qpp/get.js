const Joi = require('joi');
const logger = require('../../../../services/logger');
const { GenericQPPAPI } = require('../../../../services/qpp');
const { TAGS } = require('../../../../constants');
const {
  ProviderLVTFields, ProviderFlagFields,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields
} = require('../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    if (!req.headers['x-npi'] && !req.headers['x-tin'] && !req.headers['x-entity']) {
      return res.status(400).json({
        message: 'x-npi, x-tin, or x-entity is a required header parameter.'
      });
    }

    const npis = req.headers['x-npi'] ? req.headers['x-npi'].split(',') : [];
    const tins = req.headers['x-tin'] ? req.headers['x-tin'].split(',') : [];
    const entityIds = req.headers['x-entity'] ? req.headers['x-entity'].split(',') : [];

    const data = await GenericQPPAPI(npis, tins, entityIds, null, req.query.publish_history_id);

    return res.status(200).json({
      data: {
        apm_data: data.map(d => ({
          tin: d.tin,
          npi: d.npi,
          qp_status: d.qp_status,
          entity_id: d.entity_id,
          entity_name: d.entity_name,
          lvt_flag: d.lvt_flag,
          lvt_payments: d.lvt_payments,
          lvt_patients: d.lvt_patients,
          entity_tin: d.entity_tin,
          provider_relationship_code: d.provider_relationship_code,
          qp_payment_threshold_score: d.qp_payment_threshold_score,
          qp_patient_threshold_score: d.qp_patient_threshold_score,
          apm_id: d.apm_id,
          apm_name: d.apm_name,
          subdivision_id: d.subdivision_id,
          subdivision_name: d.subdivision_name,
          advanced_apm_flag: d.advanced_apm_flag,
          mips_apm_flag: d.mips_apm_flag
        }))
      }
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  description: 'Retrieve APM data for QPP',
  input: {
    headers: Joi.object({
      'x-npi': Joi.string().required().description('Comma delimited list of NPIs'),
      'x-tin': Joi.string().required().description('Comma delimited list of TINs'),
      'x-entity': Joi.string().required().description('Comma delimited list of Entity IDs')
    }),
    query: Joi.object({
      pac_publish_id: Joi.string().description('pac publish id')
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          tin: Joi.string().example('123456789'),
          npi: Joi.string().example('1234567890'),
          qp_status: Joi.string().example('Y'),
          ...EntityInfoFields,
          ...ProviderLVTFields,
          entity_tin: Joi.string().example('123456789'),
          ...ProviderInfoFields,
          ...ApmAndSubdivisionInfoFields,
          ...ProviderFlagFields
        }).label('QPPGenericDataV1'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
