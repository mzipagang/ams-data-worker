const Joi = require('joi');
const logger = require('../../../../../services/logger');
const { QPPApiByNPI } = require('../../../../../services/qpp');
const { TAGS } = require('../../../../../constants');
const {
  ProviderLVTFields, ProviderFlagFields,
  ProviderInfoFields, ApmAndSubdivisionInfoFields, EntityInfoFields
} = require('../../../../../schemas/QPPEntityListData');

const handler = async (req, res) => {
  try {
    if (!req.headers['x-npi']) {
      return res.status(400).json({
        message: 'x-npi is a required header parameter.'
      });
    }

    const npis = (req.headers['x-npi'].split(','));

    const data = await QPPApiByNPI(npis, null, req.query.publish_history_id);

    return res.status(200).json({
      data: {
        apm_data: data.map(d => Object.assign({}, d, {
          tins: d.tins.map(tinData => Object.assign({}, tinData, {
            entities: tinData.entities.map(entityData => ({
              entity_id: entityData.entity_id,
              entity_name: entityData.entity_name,
              lvt_flag: entityData.lvt_flag,
              lvt_payments: entityData.lvt_payments,
              lvt_patients: entityData.lvt_patients,
              entity_tin: entityData.entity_tin,
              provider_relationship_code: entityData.provider_relationship_code,
              qp_payment_threshold_score: entityData.qp_payment_threshold_score,
              qp_patient_threshold_score: entityData.qp_patient_threshold_score,
              apm_id: entityData.apm_id,
              apm_name: entityData.apm_name,
              subdivision_id: entityData.subdivision_id,
              subdivision_name: entityData.subdivision_name,
              advanced_apm_flag: entityData.advanced_apm_flag,
              mips_apm_flag: entityData.mips_apm_flag
            }))
          }))
        }))
      }
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  description: 'NPI Endpoint',
  input: {
    headers: Joi.object({
      'x-npi': Joi.string().required().description('Comma delimited list of NPIs')
    }),
    query: Joi.object({
      pac_publish_id: Joi.string().description('pac publish id')
    })
  },
  output: {
    schema: Joi.object({
      data: {
        apm_data: Joi.array().items(Joi.object({
          npi: Joi.string().example('1234567890'),
          qp_status: Joi.string().example('Y'),
          tins: Joi.array().items(Joi.object({
            tin: Joi.string().example('123456789'),
            entities: Joi.array().items(Joi.object({
              ...EntityInfoFields,
              ...ProviderLVTFields,
              entity_tin: Joi.string().example('123456789'),
              ...ProviderInfoFields,
              ...ApmAndSubdivisionInfoFields,
              ...ProviderFlagFields
            }))
          }))
        }).label('QPPNPIDataV1'))
      }
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};
