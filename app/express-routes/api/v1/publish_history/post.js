const logger = require('../../../../services/logger');
const PublishHistory = require('../../../../services/mongoose/models/publish_history');

const handler = async (req, res) => {
  try {
    const publishHistory = new PublishHistory(req.body);
    await publishHistory.save();

    res.status(200).json({
      data: {
        publish_history: publishHistory
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while getting latest publish history' });
  }
};

module.exports = {
  handler
};
