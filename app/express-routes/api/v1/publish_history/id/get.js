const logger = require('../../../../../services/logger');
const PublishHistory = require('../../../../../services/mongoose/models/publish_history');

const handler = async (req, res) => {
  try {
    const latestPublished = await PublishHistory.findOne({ id: req.params.id }).lean();

    res.status(200).json({
      data: {
        publish_history: latestPublished
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while getting latest publish history' });
  }
};

module.exports = {
  handler
};
