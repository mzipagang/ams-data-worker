const logger = require('../../../../../../../services/logger');
const PublishHistory = require('../../../../../../../services/mongoose/models/publish_history');
const { FileImportSourceMap, Models } = require('../../../../../../../services/apm-models');

const handler = async (req, res) => {
  try {
    const latestPublished = await PublishHistory.findOne({ id: req.params.id })
      .sort({ published_at: -1 })
      .lean();

    const providerModels = {
      'pac-provider': null,
      'apm-provider': null
    };

    latestPublished.files.forEach((file) => {
      const fileType = FileImportSourceMap[file.import_file_type];
      const ModelData = Models[fileType];
      if (fileType === 'pac-provider') {
        providerModels[fileType] = ModelData.dynamic(file.file_id);
      } else if (fileType === 'apm-provider') {
        providerModels[fileType] = ModelData.dynamic(latestPublished.id);
      }
    });

    let tins = [];
    const requests = Object.keys(providerModels)
      .filter(key => !!providerModels[key]).map(key => providerModels[key].aggregate([
        {
          $group: {
            _id: '$tin'
          }
        }
      ]).then(data => // eslint-disable-line no-return-assign
        tins = [...tins, ...data.map(d => d._id)]));

    await Promise.all(requests);

    res.status(200).json({
      data: {
        tins
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while getting latest publish history' });
  }
};

module.exports = {
  handler
};
