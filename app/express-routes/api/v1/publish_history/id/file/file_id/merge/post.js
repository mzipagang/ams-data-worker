const logger = require('../../../../../../../../services/logger');
const publisher = require('../../../../../../../../services/publisher');
const PublishHistory = require('../../../../../../../../services/mongoose/models/publish_history');
const { FileImportSourceMap, Models } = require('../../../../../../../../services/apm-models');

const handler = async (req, res) => {
  try {
    const publishHistory = await PublishHistory.findOne({ id: req.params.id });
    const file = publishHistory.files.find(f => f.file_id === req.params.file_id);

    const { fromRowNumber, toRowNumber } = req.body;

    const modelData = Models[FileImportSourceMap[file.import_file_type]];
    if (modelData.sourceFileData) {
      await publisher
        .merge(file.file_id, modelData.sourceFileData, modelData.dynamic(publishHistory.id), modelData.keyFn,
          file.import_file_type, fromRowNumber, toRowNumber);
    } else {
      // Technically we don't need to do anything as we use the original collections to do our querying
    }

    const updatedPublishHistory = await PublishHistory.findOneAndUpdate(
      {
        id: publishHistory.id,
        'files.file_id': file.file_id
      },
      { $set: { 'files.$.status': 'published' } },
      { new: true }
    );

    res.status(200).json({
      data: {
        publish_history: updatedPublishHistory
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while updating the file import group.' });
  }
};

module.exports = {
  handler
};
