const moment = require('moment');
const uuid = require('uuid');
const logger = require('../../../../../../services/logger');
const FileImportGroup = require('../../../../../../services/mongoose/models/file_import_group');
const { getUsername } = require('../../../../../../services/utils/jwt');

const handler = async (req, res) => {
  try {
    const update = {
      files: {
        id: uuid.v4(),
        file_name: req.body.file_name,
        file_location: null,
        status: req.body.status,
        import_file_type: req.body.import_file_type,
        uploadedBy: getUsername(req)
      }
    };

    const fileImportGroup = await FileImportGroup.findOneAndUpdate({
      id: req.params.id
    }, { $push: update, updatedAt: moment.utc().toDate() }, { new: true });

    const validatingFiles = fileImportGroup.files.filter(f => f.status === 'validating');
    await Promise.all(validatingFiles.map(({ id }) => FileImportGroup.findOneAndUpdate({
      id: req.params.id,
      'files.id': id
    }, { 'files.$.status': 'finished' })));

    const newFile = fileImportGroup.files.find(file => file.id === update.files.id);

    res.status(200).json({
      data: {
        file_import_group: fileImportGroup,
        file: newFile
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while updating the file import group.' });
  }
};

module.exports = {
  handler
};
