const moment = require('moment');
const Promise = require('bluebird');
const publisher = require('../../../../../services/publisher');
const logger = require('../../../../../services/logger');
const PublishHistory = require('../../../../../services/mongoose/models/publish_history');
const FileImportGroup = require('../../../../../services/mongoose/models/file_import_group');
const { FileImportSourceMap, Models } = require('../../../../../services/apm-models');

const handler = async (req, res) => {
  try {
    const { status } = await PublishHistory.findOne({ id: req.params.id });

    const update = Object.assign({}, req.body, {
      updatedAt: moment.utc().toDate()
    });

    const publishHistory = await PublishHistory.findOneAndUpdate({ id: req.params.id }, {
      $set: req.body
    }, { new: true });

    if (status === 'publishing' && update.status === 'published') {
      const publishHistories = await PublishHistory.find({
        file_import_group_id: publishHistory.file_import_group_id
      });

      if (!publishHistories.some(ph => ph.status !== 'published')) {
        await FileImportGroup.findOneAndUpdate({
          id: publishHistory.file_import_group_id
        }, {
          $set: {
            status: 'published'
          }
        });
      }

      if (publishHistory.group_type === 'apm') {
        const types = Object.keys(publishHistory.files.filter(file => file.new_file)
          .map(file => file.import_file_type)
          .reduce((current, importFileType) => Object
            .assign({}, current, { [FileImportSourceMap[importFileType]]: true }), {}));

        await Promise.each(types, type => publisher.publish(Models[type]
          .dynamic(publishHistory.id), Models[type].model));
      }
    }

    res.status(200).json({
      data: {
        publish_history: publishHistory
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while getting latest publish history' });
  }
};

module.exports = {
  handler
};
