const logger = require('../../../../../../services/logger');
const PublishHistory = require('../../../../../../services/mongoose/models/publish_history');

const handler = async (req, res) => {
  try {
    const cacheUpdate = {};
    Object.keys(req.body).forEach((key) => {
      cacheUpdate[`cache.${key}`] = req.body[key];
    });

    const publishHistory = await PublishHistory.findOneAndUpdate({ id: req.params.id },
      { $set: cacheUpdate },
      { new: true });

    res.status(200).json({
      data: {
        publish_history: publishHistory
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while getting latest publish history' });
  }
};

module.exports = {
  handler
};
