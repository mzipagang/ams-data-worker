const logger = require('../../../../services/logger');
const PublishHistory = require('../../../../services/mongoose/models/publish_history');

const handler = async (req, res) => {
  try {
    const publishHistories = await PublishHistory.find(req.query || {})
      .sort({ published_at: -1 })
      .lean();

    res.status(200).json({
      data: {
        publish_histories: publishHistories
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while getting latest publish history' });
  }
};

module.exports = {
  handler
};
