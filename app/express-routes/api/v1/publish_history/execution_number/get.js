const PublishHistory = require('../../../../../services/mongoose/models/publish_history');
const { TAGS } = require('../../../../../constants');
const Joi = require('joi');
const asyncHandler = require('express-async-handler');
const PerformanceYear = require('../../../../../schemas/PerformanceYear').Schema;
const Execution = require('../../../../../schemas/Execution').Schema;

const API_INPUT_SCHEMA = Joi.object({
  snapshot: Joi.string().required().description('snapshot'),
  performance_year: PerformanceYear.required(),
  import_file_type: Joi.string().required().example('pac-model')
    .description('File Type')
});

const handler = asyncHandler(async (req, res) => {
  const validationResult = Joi.validate(req.query, API_INPUT_SCHEMA);
  if (validationResult.error) {
    return res.status(400).json({ message: 'bad parameters' });
  }

  const results = await PublishHistory.aggregate([
    {
      $match: {
        performance_year: parseInt(req.query.performance_year),
        'files.snapshot': req.query.snapshot,
        'files.import_file_type': req.query.import_file_type
      }
    },
    { $sort: { published_at: -1 } },
    { $limit: 1 },
    {
      $project: {
        'files.snapshot': 1,
        'files.execution': 1,
        'files.import_file_type': 1
      }
    },
    {
      $match: {
        'files.snapshot': req.query.snapshot,
        'files.import_file_type': req.query.import_file_type
      }
    },
    { $unwind: '$files' },
    { $sort: { 'files.execution': -1 } },
    { $limit: 1 },
    { $project: { _id: -1, execution: '$files.execution' } }
  ]);
  if (!results.length) {
    return res.json({ execution: 0 });
  }
  return res.json({ execution: parseInt(results[0].execution) });
});

module.exports = {
  handler,
  tags: [TAGS.PUBLISH_HISTORY],
  description: 'Execution Number Endpoint',
  input: {
    query: API_INPUT_SCHEMA
  },
  output: {
    schema: Joi.object({
      execution: Execution
    }),
    status: {
      200: 'Returns a list APM Data',
      500: 'Unexpected error'
    }
  }
};

