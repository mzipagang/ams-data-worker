const handler = (req, res) => {
  res.json({ message: 'Welcome to the v1 Data Worker.' });
};

module.exports = {
  handler
};
