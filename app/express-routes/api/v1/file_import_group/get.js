const Promise = require('bluebird');
const logger = require('../../../../services/logger');
const FileImportGroup = require('../../../../services/mongoose/models/file_import_group');
const Joi = require('joi');
const { TAGS } = require('../../../../constants');
const { Schema } = require('../../../../schemas/FileImportGroup');

const handler = (req, res) => {
  const query = {};
  let limit = 10;

  if (req.query.status) {
    query.status = req.query.status;
  }

  if (!isNaN(req.query.limit)) {
    limit = parseInt(req.query.limit);

    if (limit > 1000) {
      limit = 1000;
    }
  }

  Promise.try(() => FileImportGroup.find(query).sort({ createdAt: -1 }).limit(limit))
    .then((fileImportGroups) => {
      res.status(200).json({
        data: {
          file_import_groups: fileImportGroups
        }
      });
    })
    .catch((err) => {
      logger.error(err);
      res.status(500).json({ message: 'There was an error while getting file import groups.' });
    });
};

module.exports = {
  handler,
  tags: [TAGS.FILE_IMPORT_GROUP],
  description: 'Retrieve a list of file import groups',
  output: {
    schema: Joi.object({
      data: Joi.object({
        file_import_groups: Joi.array().items(Schema)
      })
    }),
    status: {
      200: 'Returns a list of file import groups',
      500: 'Unexpected error'
    }
  }
};
