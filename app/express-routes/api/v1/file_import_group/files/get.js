const logger = require('../../../../../services/logger');
const FileImportGroup = require('../../../../../services/mongoose/models/file_import_group');

const handler = async (req, res) => {
  try {
    const sortBy = req.query.sortBy || 'createdAt';
    const sortOrder = parseInt(req.query.sortOrder) || -1;

    const data = await FileImportGroup.aggregate([
      {
        $match: {
          status: 'published'
        }
      },
      {
        $project: {
          files: true,
          _id: false
        }
      },
      {
        $unwind: '$files'
      },
      {
        $replaceRoot: {
          newRoot: '$files'
        }
      },
      {
        $sort: {
          [sortBy]: sortOrder
        }
      }
    ]);

    res.status(200).json({
      data: {
        files: data
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler
};
