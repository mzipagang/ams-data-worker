const Promise = require('bluebird');
const logger = require('../../../../services/logger');
const FileImportGroup = require('../../../../services/mongoose/models/file_import_group');
const { getUsername } = require('../../../../services/utils/jwt');
const Joi = require('joi');
const { TAGS } = require('../../../../constants');
const { Schema } = require('../../../../schemas/FileImportGroup');

const handler = (req, res) => {
  Promise.try(() => new FileImportGroup({
    createdBy: getUsername(req)
  }).save())
    .then((fileImportGroup) => {
      res.status(200).json({
        data: {
          file_import_group: fileImportGroup
        }
      });
    })
    .catch((err) => {
      logger.error(err);
      res.status(500).json({ message: 'There was an error while creating a new file import group.' });
    });
};


module.exports = {
  handler,
  tags: [TAGS.FILE_IMPORT_GROUP],
  description: 'Create a new File Import Group',
  input: {
    payload: Schema
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        file_import_group: Schema
      })
    }),
    status: {
      200: 'Return newly created file import group',
      500: 'Unexpected error'
    }
  }
};
