const config = require('../../../../../../../../config');
const sessionCheckMiddleware = require('../../../../../../../../services/sessionCheckMiddleware');

module.exports = (router) => {
  router.use((_req, res, next) => {
    if (config.FEATURE_FLAGS.DISABLE_SOURCE_FILES) {
      res.status(404).json({ message: 'Route not found.' });
      return;
    }
    next();
  });
  router.use(sessionCheckMiddleware);

  // In order to check permissions, use the ensurePermission middleware
  // permission check must always be after session check

  // router.get('/', ensurePermission('report'));

  return router;
};
