const logger = require('../../../../../../../../../../services/logger');
const FileImportGroup = require('../../../../../../../../../../services/mongoose/models/file_import_group');
const DataLog = require('../../../../../../../../../../services/mongoose/models/data_log');
const DynamicNamePacModel = require('../../../../../../../../../../services/mongoose/dynamic_name_models/pac_model');

const handler = async (req, res) => {
  try {
    const fileImportGroup = await FileImportGroup.findOne({ id: req.params.id });
    if (!fileImportGroup) {
      res.status(404).json({ message: 'File Import Group does not exist' });
      return;
    }

    if (fileImportGroup.status !== 'open') {
      res.status(400).json({ message: 'This file import group is no longer accepting new data' });
      return;
    }

    const PacModel = DynamicNamePacModel(req.params.file_id);

    const modelData = req.body.data.values.map(modelInfo => modelInfo.value);
    const commonMeta = req.body.data.meta || {};

    const importedModels = await PacModel.insertMany(modelData);
    const logValuesAndMeta = importedModels.map((model, index) => ({
      value: model,
      meta: Object.assign({}, commonMeta, req.body.data.values[index].meta || {})
    }));

    await DataLog.createLogBatch(DataLog.actions.CREATE, logValuesAndMeta);

    const result = importedModels.map(() => ({
      success: true
    }));

    res.status(200).json({
      data: {
        values: result
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while importing data' });
  }
};

module.exports = {
  handler
};
