const logger = require('../../../../../../../../../../services/logger');
const bulkUpsert = require('../../../../../../../../../../services/bulkUpsert');
const FileImportGroup = require('../../../../../../../../../../services/mongoose/models/file_import_group');
const SourceFileEntityData = require('../../../../../../../../../../services/mongoose/models/source_file_entity_data');
const apmFileFormatter = require('../../../../../../../../../../services/fileFormatter/apm');

const handler = async (req, res) => {
  try {
    const fileImportGroup = await FileImportGroup.findOne({ id: req.params.id });
    if (!fileImportGroup) {
      res.status(404).json({ message: 'File Import Group does not exist' });
      return;
    }

    if (fileImportGroup.status !== 'open') {
      res.status(400).json({ message: 'This file import group is no longer accepting new data' });
      return;
    }

    const file = fileImportGroup.files.find(f => f.id === req.params.file_id);
    if (!file) {
      res.status(404).json({ message: 'File does not exist' });
      return;
    }

    if (file.status !== 'processing') {
      res.status(400).json({ message: 'This file is no longer accepting new data' });
      return;
    }

    const keyFn = data => ({ file_id: file.id, file_line_number: data.meta.file_line_number });
    const meta = Object.assign({}, req.body.data.meta, {
      file_import_group_id: fileImportGroup.id
    });
    const entityData = apmFileFormatter.format(req.body.data.values, meta)
      .map(entityInfo => Object.assign({}, entityInfo, {
        file_id: file.id,
        file_line_number: entityInfo.meta.file_line_number,
        tin: entityInfo.tin,
        subdiv_id: entityInfo.apm_id === '21' ? '00' : entityInfo.subdiv_id
      }));

    const results = await bulkUpsert(SourceFileEntityData, entityData, keyFn);

    res.status(200).json({
      data: {
        values: results
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while importing that data'
    });
  }
};

module.exports = handler;
