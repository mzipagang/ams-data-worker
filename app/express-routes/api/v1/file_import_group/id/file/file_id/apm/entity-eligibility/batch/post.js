const logger = require('../../../../../../../../../../services/logger');
const bulkUpsert = require('../../../../../../../../../../services/bulkUpsert');
const FileImportGroup = require('../../../../../../../../../../services/mongoose/models/file_import_group');
const SourceEntityEligibility =
  require('../../../../../../../../../../services/mongoose/models/source_file_entity_eligibility');
const apmFileFormatter = require('../../../../../../../../../../services/fileFormatter/apm');

const handler = async (req, res) => {
  try {
    const fileImportGroup = await FileImportGroup.findOne({ id: req.params.id });
    if (!fileImportGroup) {
      return res.status(404).json({ message: 'File Import Group does not exist' });
    }

    if (fileImportGroup.status !== 'open') {
      return res.status(400).json({ message: 'This file import group is no longer accepting new data' });
    }

    const file = fileImportGroup.files.find(f => f.id === req.params.file_id);
    if (!file) {
      return res.status(404).json({ message: 'File does not exist' });
    }

    if (file.status !== 'processing') {
      res.status(400).json({ message: 'This file is no longer accepting new data' });
    }

    const meta = Object.assign({}, req.body.data.meta, {
      file_import_group_id: fileImportGroup.id,
      file_import_group_file_id: file.id
    });

    const EntityEligibilityData = apmFileFormatter.format(req.body.data.values, meta)
      .map(data => Object.assign({}, data, { file_id: file.id, file_line_number: data.meta.file_line_number }));

    const results = await bulkUpsert(SourceEntityEligibility, EntityEligibilityData);

    return res.status(200).json({
      data: {
        values: results
      }
    });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({ message: 'There was an error while importing that data' });
  }
};

module.exports = {
  handler
};
