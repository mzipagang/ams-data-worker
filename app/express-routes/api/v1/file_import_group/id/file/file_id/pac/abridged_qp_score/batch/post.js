const logger = require('../../../../../../../../../../services/logger');
const FileImportGroup = require('../../../../../../../../../../services/mongoose/models/file_import_group');
const DataLog = require('../../../../../../../../../../services/mongoose/models/data_log');
const AbridgedQpScore =
  require('../../../../../../../../../../services/mongoose/dynamic_name_models/abridged_qp_score');

const handler = async (req, res) => {
  try {
    const fileImportGroup = await FileImportGroup.findOne({ id: req.params.id });
    if (!fileImportGroup) {
      res.status(404).json({ message: 'File Import Group does not exist' });
      return;
    }

    if (fileImportGroup.status !== 'open') {
      res.status(400).json({ message: 'This file import group is no longer accepting new data' });
      return;
    }

    const snapshotId = req.body.data.values[0] && req.body.data.values[0].value.snapshot_id; // TODO: is there a better way
    const abridgedQpScoreCollection = AbridgedQpScore(snapshotId);

    const qpScoreData = req.body.data.values.map(qpScoreInfo => Object.assign({}, qpScoreInfo.value, {
      tin: qpScoreInfo.value.tin
    }));
    const commonMeta = req.body.data.meta || {};

    const importedQpScores = await abridgedQpScoreCollection.insertMany(qpScoreData);
    const logValuesAndMeta = importedQpScores.map((qpScore, index) => ({
      value: qpScore,
      meta: Object.assign({}, commonMeta, req.body.data.values[index].meta || {})
    }));

    await DataLog.createLogBatch(DataLog.actions.CREATE, logValuesAndMeta);

    const result = importedQpScores.map(() => ({
      success: true
    }));

    res.status(200).json({
      data: {
        values: result
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while importing data' });
  }
};

module.exports = {
  handler
};
