const Promise = require('bluebird');
const moment = require('moment');
const logger = require('../../../../../../../services/logger');
const FileImportGroup = require('../../../../../../../services/mongoose/models/file_import_group');
const { validateCollection, validateMultipleTypesInSingleFileAndSummarize } = require('../../../../../../../services/validation');
const { FileImportSourceMap, Models } = require('../../../../../../../services/apm-models');
const Joi = require('joi');
const { TAGS } = require('../../../../../../../constants');
const FileSchema = require('../../../../../../../schemas/File').Schema;
const FileImportGroupSchema = require('../../../../../../../schemas/FileImportGroup').Schema;

const PacQpcScore = require('../../../../../../../services/mongoose/dynamic_name_models/pac_qp_category_score');
const PacQpcScoreSummary = require('../../../../../../../services/mongoose/dynamic_name_models/pac_qp_category_score_summary');

function isAbridgedFile(fileType) {
  const abridgedFiles = {
    abridged_qp_score: true,
    abridged_qp_status: true,
    abridged_lvt: true
  };
  return !!abridgedFiles[fileType];
}

const handler = async (req, res) => {
  try {
    const originalFileImportGroup = await FileImportGroup.findOne({
      id: req.params.id,
      'files.id': req.params.file_id
    });

    // TODO: validate body
    const updatedReqBody = {
      'files.$.updatedAt': moment.utc().toDate()
    };
    Object.keys(req.body).forEach((key) => {
      updatedReqBody[`files.$.${key}`] = req.body[key];
    });
    const update = Object.assign({}, updatedReqBody, { updatedAt: moment.utc().toDate() });

    if (originalFileImportGroup && originalFileImportGroup.status !== 'finished' && req.body.status === 'finished') {
      const currentFile = originalFileImportGroup.files.find(file => file.id === req.params.file_id);
      const fileType = FileImportSourceMap[currentFile.import_file_type];

      if (['apm-entity',
        'apm-provider',
        'beneficiary',
        'beneficiary_mdm',
        'individual_qp_status',
        'individual_qp_threshold',
        'entity-eligibility',
        'entity_qp_status',
        'entity_qp_threshold'].includes(fileType)) {
        const validationUpdate = {};
        update['files.$.status'] = 'validating';

        // TODO: move to a separate service later
        const firstDetailRow = Models[fileType].sourceFileData
          .findOne()
          .where({ file_id: currentFile.id })
          .sort({ file_line_number: 1 });

        const lastDetailRow = Models[fileType].sourceFileData
          .findOne()
          .where({ file_id: currentFile.id })
          .sort({ file_line_number: -1 });

        const firstRow = await firstDetailRow;
        const lastRow = await lastDetailRow;
        const firstRowLineNumber = firstRow ? firstRow.file_line_number : 0;
        const lastRowLineNumber = lastRow ? lastRow.file_line_number : 0;

        const rowDetails = {
          'files.$.first_detail_row': firstRowLineNumber || 0,
          'files.$.last_detail_row': lastRowLineNumber || 0
        };

        await FileImportGroup.findOneAndUpdate({
          id: req.params.id,
          'files.id': req.params.file_id
        }, { $set: rowDetails }, { new: true });

        setImmediate(() => Promise.coroutine(function* validateByFileType() {
          yield validateCollection(Models[fileType].sourceFileData, fileType, currentFile);
          validationUpdate['files.$.status'] = 'finished';
        })()
          .catch((err) => {
            validationUpdate['files.$.status'] = 'error';
            logger.error(err);
          })
          .finally(() => {
            validationUpdate['files.$.updatedAt'] = moment.utc().toDate();
            return FileImportGroup.findOneAndUpdate({
              id: req.params.id,
              'files.id': req.params.file_id
            }, { $set: validationUpdate }, { new: true });
          })
        );
      } else {
        // loop through each file and give the record count
        let recordCount;
        if (fileType === 'individual_qp_status' ||
            fileType === 'individual_qp_threshold' ||
            fileType === 'entity-eligibility' ||
            fileType === 'entity_qp_status' ||
            fileType === 'entity_qp_threshold') {
          recordCount =
            await Models[fileType].sourceFileData.count({ file_id: req.params.file_id });
        } else if (isAbridgedFile(fileType)) {
          // noop
        } else if (fileType === 'pac-qp_category_score') {
          const validationUpdate = {};
          setImmediate(() => Promise.coroutine(function* validateByFileType() {
            yield validateMultipleTypesInSingleFileAndSummarize([{
              collection: PacQpcScore(currentFile.id),
              type: 'pac-qp_category_score'
            }, {
              collection: PacQpcScoreSummary(currentFile.id),
              type: 'pac-qp_category_score_summary'
            }], currentFile);
            validationUpdate['files.$.status'] = 'finished';
          })()
            .catch((err) => {
              validationUpdate['files.$.status'] = 'error';
              logger.error(err);
            })
            .finally(() => {
              validationUpdate['files.$.updatedAt'] = moment.utc().toDate();
              return FileImportGroup.findOneAndUpdate({
                id: req.params.id,
                'files.id': req.params.file_id
              }, { $set: validationUpdate }, { new: true });
            })
          );
        } else {
          recordCount =
            await Models[fileType].dynamic(req.params.file_id).count({});
        }

        await FileImportGroup.findOneAndUpdate(
          { 'files.id': req.params.file_id },
          { $set: { 'files.$.validations': { totalRecords: recordCount } } }
        );
      }
    }

    const fileImportGroup = await FileImportGroup.findOneAndUpdate({
      id: req.params.id,
      'files.id': req.params.file_id
    }, { $set: update }, { new: true });

    res.status(200).json({
      data: {
        file_import_group: fileImportGroup
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'There was an error while updating the file import group.' });
  }
};

module.exports = {
  handler,
  tags: [TAGS.FILE_IMPORT_GROUP],
  description: 'Update a file in a file import group',
  input: {
    params: Joi.object({
      fileImportGroupId: Joi.string().required().description('ID of File Import Group'),
      fileId: Joi.string().required().description('ID of File to update')
    }),
    payload: FileSchema.required().description('Parts of File to update')
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        file_import_group: FileImportGroupSchema
      })
    }),
    status: {
      200: 'Returns the updated file import group',
      500: 'Unexpected error'
    }
  }
};
