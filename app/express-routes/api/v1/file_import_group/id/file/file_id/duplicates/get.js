const logger = require('../../../../../../../../services/logger');
const FileImportGroup = require('../../../../../../../../services/mongoose/models/file_import_group');
const PacProviderDynamic = require('../../../../../../../../services/mongoose/dynamic_name_models/pac_provider');

const handler = async (req, res) => {
  try {
    const fileId = req.params.file_id;

    const fileImportGroup = await FileImportGroup.findOne({
      id: req.params.id,
      'files.id': fileId
    });
    const file = fileImportGroup.files.find(f => f.id === fileId);

    if (!file) {
      res.status(404).json({ message: 'Cannot find file' });
      return;
    }

    const providerModel = PacProviderDynamic(file.id);

    const duplicates = await providerModel.aggregate([
      {
        $group: {
          _id: {
            entity_id: '$entity_id',
            tin: '$tin',
            npi: '$npi'
          },
          sum: {
            $sum: 1
          }
        }
      },
      {
        $match: {
          sum: {
            $gt: 1
          }
        }
      },
      {
        $group: {
          _id: '$_id.tin',
          items: {
            $push: {
              entity_id: '$_id.entity_id',
              npi: '$_id.npi'
            }
          }
        }
      }
    ])
      .allowDiskUse(true);

    // Grouping into tin -> entity id -> npi to reduce request size
    // Mutating original array to save memory
    duplicates.forEach((duplicate) => {
      const tin = duplicate._id;
      delete duplicate._id;
      duplicate.tin = tin;

      const entityIds = {};
      duplicate.items.forEach((item) => {
        let npis = entityIds[item.entity_id];
        if (!npis) {
          npis = [];
          entityIds[item.entity_id] = npis;
        }

        npis.push(item.npi);
      });

      delete duplicate.items;
      duplicate.entities = Object.keys(entityIds).map(entityId => ({
        entity_id: entityId,
        npis: entityIds[entityId]
      }));
    });

    res.status(200).json({
      data: {
        duplicates
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error'
    });
  }
};

module.exports = {
  handler
};
