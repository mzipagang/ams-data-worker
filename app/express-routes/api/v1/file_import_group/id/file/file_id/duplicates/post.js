const logger = require('../../../../../../../../services/logger');
const FileImportGroup = require('../../../../../../../../services/mongoose/models/file_import_group');
const PacProviderDynamic = require('../../../../../../../../services/mongoose/dynamic_name_models/pac_provider');

const handler = async (req, res) => {
  try {
    const fileId = req.params.file_id;

    const fileImportGroup = await FileImportGroup.findOne({
      id: req.params.id,
      'files.id': fileId
    });
    const file = fileImportGroup.files.find(f => f.id === fileId);

    if (!file) {
      res.status(404).json({ message: 'Cannot find file' });
      return;
    }

    const providerModel = PacProviderDynamic(file.id);

    const duplicates = req.body.duplicates_providers;

    let actions = 0;
    let bulk = providerModel.collection.initializeUnorderedBulkOp();

    // Mark duplicate providers
    for (let j = 0; j < duplicates.length; j += 1) {
      const dup = duplicates[j];
      const entityId = dup.entity_id;
      const tin = dup.tin;
      const npi = dup.npi;

      const providers = await providerModel.find({ entity_id: entityId, tin, npi }); // eslint-disable-line no-await-in-loop
      const partialProvider = providers.find(provider => provider.provider_relationship_code === 'P')
          || providers[providers.length - 1];

      if (partialProvider) {
        const duplicateProvidersIds = providers.filter(p => p._id !== partialProvider._id)
          .map(provider => provider._id);
        bulk.find({ _id: { $in: duplicateProvidersIds } }).update({ $set: { duplicate: true } });
        actions += 1;
        if (actions >= 5000) {
          await bulk.execute(); // eslint-disable-line no-await-in-loop
          bulk = providerModel.collection.initializeUnorderedBulkOp();
          actions = 0;
        }
      }
    }

    if (actions > 0) {
      await bulk.execute();
    }

    res.status(200).json({
      data: {
        success: true
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error'
    });
  }
};

module.exports = {
  handler
};
