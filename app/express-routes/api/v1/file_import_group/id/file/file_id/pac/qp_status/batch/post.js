const logger = require('../../../../../../../../../../services/logger');
const FileImportGroup = require('../../../../../../../../../../services/mongoose/models/file_import_group');
const DataLog = require('../../../../../../../../../../services/mongoose/models/data_log');
const DynamicNamePacQpStatus =
  require('../../../../../../../../../../services/mongoose/dynamic_name_models/pac_qp_status');

const handler = async (req, res) => {
  try {
    const fileImportGroup = await FileImportGroup.findOne({ id: req.params.id });
    if (!fileImportGroup) {
      res.status(404).json({ message: 'File Import Group does not exist' });
      return;
    }

    if (fileImportGroup.status !== 'open') {
      res.status(400).json({ message: 'This file import group is no longer accepting new data' });
      return;
    }

    const PacQpStatus = DynamicNamePacQpStatus(req.params.file_id);

    const qpStatusData = req.body.data.values.map(qpStatusInfo => qpStatusInfo.value);
    const commonMeta = req.body.data.meta || {};

    const importedQpStatuss = await PacQpStatus.insertMany(qpStatusData);
    const logValuesAndMeta = importedQpStatuss.map((qpStatus, index) => ({
      value: qpStatus,
      meta: Object.assign({}, commonMeta, req.body.data.values[index].meta || {})
    }));

    await DataLog.createLogBatch(DataLog.actions.CREATE, logValuesAndMeta);

    const result = importedQpStatuss.map(() => ({
      success: true
    }));

    res.status(200).json({
      data: {
        values: result
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while importing data' });
  }
};

module.exports = {
  handler
};
