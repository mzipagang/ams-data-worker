const FileImportGroup = require('../../../../../../../services/mongoose/models/file_import_group');

const handler = async (req, res) => {
  const fileId = req.params.file_id;

  const file = await FileImportGroup.getFileById(fileId);

  res.status(200).json({ file });
};

module.exports = {
  handler
};
