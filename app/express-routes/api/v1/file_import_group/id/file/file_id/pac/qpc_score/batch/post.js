const logger = require('../../../../../../../../../../services/logger');
const FileImportGroup = require('../../../../../../../../../../services/mongoose/models/file_import_group');
const DynamicPacQpcScore =
  require('../../../../../../../../../../services/mongoose/dynamic_name_models/pac_qp_category_score');
  const DynamicPacQpcScoreSummary = require('../../../../../../../../../../services/mongoose/dynamic_name_models/pac_qp_category_score_summary');

const handler = async (req, res) => {
  try {
    const fileImportGroup = await FileImportGroup.findOne({ id: req.params.id });
    if (!fileImportGroup) {
      res.status(404).json({ message: 'File Import Group does not exist' });
      return;
    }

    if (fileImportGroup.status !== 'open') {
      res.status(400).json({ message: 'This file import group is no longer accepting new data' });
      return;
    }

    const file = fileImportGroup.files.find(f => f.id === req.params.file_id);
    if (!file) {
      res.status(404).json({ message: 'File does not exist' });
      return;
    }

    if (file.status !== 'processing') {
      res.status(400).json({ message: 'This file is no longer accepting new data' });
      return;
    }

    const PacQpcScore = DynamicPacQpcScore(file.id);
    const PacQpcScoreSummary = DynamicPacQpcScoreSummary(file.id);
    const allRecords = req.body.data.values.map(detailRecord => Object.assign({}, { meta: detailRecord.meta }, detailRecord.value, { file_id: req.params.file_id }));


    const summaryRecords = allRecords.filter(d => d.hasOwnProperty('final_qpc_score'));
    const detailRecords = allRecords.filter(d => !d.hasOwnProperty('final_qpc_score'));

    const detailRecordInsertResult = await PacQpcScore.insertMany(detailRecords);
    const summaryRecordInsertResult = await PacQpcScoreSummary.insertMany(summaryRecords);

    const allResults = detailRecordInsertResult.concat(summaryRecordInsertResult).map(() => ({
      success: true
    }));

    res.status(200).json({
      data: {
        values: allResults
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ message: 'Error while importing data' });
  }
};

module.exports = {
  handler
};
