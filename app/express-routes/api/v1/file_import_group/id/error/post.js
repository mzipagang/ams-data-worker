const logger = require('../../../../../../services/logger');
const FileImportError = require('../../../../../../services/mongoose/models/file_import_error');
const { Schema } = require('../../../../../../schemas/FileImportError');
const { validate } = require('../../../../../../schemas');
const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');

const handler = async (req, res) => {
  try {
    const result = validate(req.body, Schema);
    if (result.error) {
      res.status(400).json({
        message: result.error.toString()
      });
      return;
    }

    const fileImportError = await FileImportError.create(req.body);

    res.status(200).json({
      data: {
        file_import_error: fileImportError.toJson()
      }
    });
  } catch (err) {
    logger.error(err);
    res.status(500).json({
      message: 'There was an error while processing your request'
    });
  }
};

module.exports = {
  handler,
  tags: [TAGS.FILE_IMPORT_GROUP],
  description: 'Create error message when importing a line from a file',
  input: {
    params: Joi.object({
      fileImportGroupId: Joi.string().required().description('ID of File Import Group')
    })
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        file_import_error: Schema
      })
    }),
    status: {
      200: 'Returns the selected file import group error',
      500: 'Unexpected error'
    }
  }
};
