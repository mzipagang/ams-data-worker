const Promise = require('bluebird');
const logger = require('../../../../../services/logger');
const FileImportGroup = require('../../../../../services/mongoose/models/file_import_group');
const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/FileImportGroup');

const handler = (req, res) => {
  Promise.try(() => FileImportGroup.findOne({ id: req.params.id }))
    .then((fileImportGroup) => {
      res.status(200).json({
        data: {
          file_import_group: fileImportGroup
        }
      });
    })
    .catch((err) => {
      logger.error(err);
      res.status(500).json({ message: 'There was an error while getting file import groups.' });
    });
};

module.exports = {
  handler,
  tags: [TAGS.FILE_IMPORT_GROUP],
  description: 'Retrieve a file import group',
  input: {
    params: Joi.object({
      id: Joi.string().required().description('ID of File Import Group')
    })
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        file_import_group: Schema
      })
    }),
    status: {
      200: 'Returns the selected file import group',
      500: 'Unexpected error'
    }
  }
};
