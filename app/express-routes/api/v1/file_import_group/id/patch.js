const Promise = require('bluebird');
const moment = require('moment');
const logger = require('../../../../../services/logger');
const createSQSAPIMessage = require('../../../../../services/createSQSAPIMessage');
const FileImportGroup = require('../../../../../services/mongoose/models/file_import_group');
const PublishHistory = require('../../../../../services/mongoose/models/publish_history');
const PacSnapshot = require('../../../../../services/mongoose/models/pac_snapshot');
const { CORE_FILE_TYPES } = require('../../../../../constants');
const Joi = require('joi');
const { TAGS } = require('../../../../../constants');
const { Schema } = require('../../../../../schemas/FileImportGroup');
const JobQueueService = require('../../../../../services/JobQueueService');
const { getUsername } = require('../../../../../services/utils/jwt');

// TODO: Move these consts out to a separate location, it's useful and reusable
const RESTRICTED_STATUSES = {
  publish: true,
  closed: true,
  published: true
};

const RESTRICTED_PAC_SNAPSHOT_FILE_TYPES = [
  'pac-lvt'
];

const applyFile = (newPublishHistory, file) => {
  const newFile = file;
  while (true) { // eslint-disable-line no-constant-condition
    const fileToReplaceIndex = newPublishHistory.files.findIndex(originalFile =>
      originalFile.performance_year === file.performance_year &&
      originalFile.import_file_type === file.import_file_type &&
      originalFile.apm_id === file.apm_id);

    if (fileToReplaceIndex === -1) {
      break;
    }

    newFile.new_file = true;
    newFile.status = 'publishing';
    newPublishHistory.files.splice(fileToReplaceIndex, 1);
  }

  newPublishHistory.files.push(newFile);
};

const determineGroupType = fileType => ((fileType.includes('pac') || fileType.includes('abridged')) ? 'pac' : 'apm');

const createNewPublishHistory = async (fileImportGroup, groupType, performanceYear, files) => {
  const currentPublishHistory = await PublishHistory.findOne({
    group_type: groupType,
    performance_year: performanceYear
  })
    .sort({ published_at: -1 });

  const newPublishHistory = {
    file_import_group_id: fileImportGroup.id,
    status: 'publishing',
    files: [],
    group_type: groupType,
    performance_year: performanceYear
  };

  // Apply each file from old publish history
  ((currentPublishHistory || {}).files || []).forEach(file => applyFile(newPublishHistory, {
    file_id: file.file_id,
    import_file_type: file.import_file_type,
    apm_id: file.apm_id,
    snapshot: file.snapshot,
    execution: file.execution,
    status: 'published',
    new_file: false
  }));

  // Apply each new file
  files.forEach((file) => {
    applyFile(newPublishHistory, {
      file_id: file.id,
      import_file_type: file.import_file_type,
      apm_id: file.apm_id,
      snapshot: file.snapshot,
      execution: file.execution,
      status: 'publishing',
      new_file: true
    });
  });

  await new PublishHistory(newPublishHistory).save();
};

const createNewPacSnapshot = (file) => {
  const newPacSnapshotObj = {

    performance_year: file.performance_year,
    run_snapshot: file.run_snapshot || null,
    run_number: file.run_number || null,
    file_id: file.id,
    type: file.import_file_type

  };
  return new PacSnapshot(newPacSnapshotObj).save();
};

const getReportPromises = async (performanceYear, runSnapshot) => {
  const files = await PacSnapshot.find({ performance_year: performanceYear, run_snapshot: runSnapshot }, '-_id type')
    .sort({ createdAt: -1 })
    .lean();

  const missingFiles = CORE_FILE_TYPES.filter(fileType => !files.some(file => file.type === fileType));

  if (missingFiles.length) {
    // eslint-disable-next-line max-len
    logger.info(`Will not generate reports for ${performanceYear}/${runSnapshot}. There are missing file(s) for ${missingFiles.join(', ')}`);
  } else {
    logger.info(`Will generate reports for ${performanceYear}/${runSnapshot}.`);
  }

  return missingFiles.length ? [] : [
    createSQSAPIMessage(1, 'GENERATE_QP_METRICS_REPORT', { performanceYear, runSnapshot }),
    createSQSAPIMessage(1, 'GENERATE_EC_COUNT_REPORT', { performanceYear, runSnapshot })
  ];
};

const preCacheReports = async (files) => {
  // Getting unique pairs of needed report params
  const uniqueReportParams = files
    .filter(file => file.performance_year && file.run_snapshot)
    .reduce((acc, current) => {
      acc[`${current.performance_year}-${current.run_snapshot}`] = null;
      return acc;
    }, {});

  const reportPromises = [].concat([], ...Object.keys(uniqueReportParams).map((report) => {
    const [performanceYear, runSnapshot] = report.split('-');
    return getReportPromises(performanceYear, runSnapshot);
  }));

  Promise.all(reportPromises);
};

const handler = (req, res) => {
  (async () => {
    const { status } = await FileImportGroup.findOne({ id: req.params.id });

    if (RESTRICTED_STATUSES[status]) {
      res.status(400).json({
        message: `File Import Group cannot be changed once the status is '${status}'`
      });
      return;
    }
    // the status is changed from publish to publishing as the status 'publish' is used to signify
    // to the server we want to begin the publishing process
    // in effect, the status 'publish' has no meaning to the server
    const update = Object.assign({}, req.body, {
      updatedAt: moment.utc().toDate()
    });

    let publishing = false;
    if (status === FileImportGroup.Statuses.OPEN && update.status === FileImportGroup.Statuses.PUBLISH) {
      update.status = FileImportGroup.Statuses.PUBLISHING;
      publishing = true;
    }

    const fileImportGroup = await FileImportGroup.findOneAndUpdate(
      { id: req.params.id },
      { $set: update },
      { new: true }
    ).lean();

    // Create a publish history document per group type
    if (publishing) {
      const publishedTypesMap =
        fileImportGroup.files.filter(file => file.status !== 'removed').reduce((current, file) => {
          const performanceYear = file.performance_year;
          const groupType = determineGroupType(file.import_file_type);

          /* istanbul ignore next */
          if (current[`${performanceYear}_${groupType}`]) {
            current[`${performanceYear}_${groupType}`].files.push(file);
          } else {
            current[`${performanceYear}_${groupType}`] = {
              performanceYear,
              groupType,
              files: [file]
            };
          }

          return current;
        }, {});

      const publishedTypes = Object.keys(publishedTypesMap).map(key => publishedTypesMap[key]);
      await Promise.all(publishedTypes.map(({ performanceYear, groupType, files }) =>
        createNewPublishHistory(fileImportGroup, groupType, performanceYear, files)
      ));

      // create new record in pac_snapshots per pac file
      const pacFiles = fileImportGroup.files.filter(({ status: pacFileStatus, import_file_type: importFileType }) =>
        pacFileStatus !== 'removed' &&
        determineGroupType(importFileType) === 'pac' &&
        !RESTRICTED_PAC_SNAPSHOT_FILE_TYPES.includes(importFileType));

      const tasks = pacFiles.map(p => createNewPacSnapshot(p));

      await Promise.all(tasks);
      await preCacheReports(fileImportGroup.files);
      if (fileImportGroup.files[0]) {
        await JobQueueService.createJob('UpdateQppProviderCollection',
          { performanceYear: fileImportGroup.files[0].performance_year }, getUsername(req));
      }
    }

    res.status(202).json({
      data: {
        file_import_group: fileImportGroup
      }
    });
  })()
    .catch((err) => {
      logger.error('queue_error', err);
      res.status(500).json({ message: 'There was an error while updating the file import group.' });
    });
};

module.exports = {
  handler,
  tags: [TAGS.FILE_IMPORT_GROUP],
  description: 'Retrieve a file import group',
  input: {
    params: Joi.object({
      id: Joi.string().required().description('ID of File Import Group')
    })
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        file_import_group: Schema
      })
    }),
    status: {
      200: 'Returns the updated file import group',
      500: 'Unexpected error'
    }
  }
};
