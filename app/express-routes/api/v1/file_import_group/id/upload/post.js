const Joi = require('joi');
const { TAGS } = require('../../../../../../constants');
const { FileTypes } = require('../../../../../../schemas/File');

//  *     responses:
//  *       200:
//  *         description: Returns array of newly created file ids
//  *         schema:
//  *           type: object
//  *           properties:
//  *             data:
//  *               type: object
//  *               properties:
//  *                 file_ids:
//  *                   type: array
//  *                   items:
//  *                     type: string
//  *                     example: 5a59081f-87aa-48b8-8dba-680b5cbf2ac3
//  */

module.exports = {
  handler: () => {},
  tags: [TAGS.FILE_IMPORT_GROUP],
  description: 'Upload a file',
  input: {
    params: Joi.object({
      id: Joi.string().required().description('ID of File Import Group')
    }),
    payload: Joi.object({
      file: Joi.any().required().meta({ swaggerType: 'file' }).description('The file to upload')
    }),
    query: Joi.object({
      'import-file-type': Joi.string().valid(...FileTypes).description('The type of file to upload')
    })
  },
  output: {
    schema: Joi.object({
      data: Joi.object({
        file_ids: Joi.array().items(Joi.string().example('5a59081f-87aa-48b8-8dba-680b5cbf2ac3'))
      })
    }),
    status: {
      200: 'Returns array of newly created file ids',
      500: 'Unexpected error'
    }
  }
};
