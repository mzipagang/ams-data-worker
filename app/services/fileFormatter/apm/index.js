const moment = require('moment');
const parser = require('../../mongoose/setters/number-parser');
const removeUndefined = require('../../utils/removeUndefined');

const format = (rows, commonMeta) => rows.map((data) => {
  const value = data.value;
  const meta = Object.assign({}, commonMeta, data.meta);

  if (value.start_date) {
    value.start_date = moment.utc(value.start_date, 'YYYY-MM-DD').toDate();
  }

  if (value.end_date) {
    value.end_date = moment.utc(value.end_date, 'YYYY-MM-DD').toDate();
  }

  if (value.dob) {
    value.dob = moment.utc(value.dob, 'YYY-MM-DD').toDate();
  }

  value.invalid = false;
  value.meta = meta;
  value.expenditures_numerator = parser.asDecimalIfNumeric(value.expenditures_numerator);
  value.expenditures_denominator = parser.asDecimalIfNumeric(value.expenditures_denominator);
  value.beneficiaries_numerator = parser.asDecimalIfNumeric(value.beneficiaries_numerator);
  value.beneficiaries_denominator = parser.asDecimalIfNumeric(value.beneficiaries_denominator);
  value.patient_threshold_score = parser.asIntegerIfNumeric(value.patient_threshold_score);
  value.payment_threshold_score = parser.asIntegerIfNumeric(value.payment_threshold_score);

  // mongo interperits `undefined` as `null` so we need to make sure to remove that here
  return removeUndefined(value);
});

module.exports = {
  format
};
