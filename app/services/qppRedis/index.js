const Promise = require('bluebird');
const Redis = require('../redis');

const getKeyName = (
  searchType, // eslint-disable-line no-confusing-arrow
  searchParam,
  publishHistoryId,
  performanceYear
) =>
  (searchParam
    ? `qpp_${searchType}_${performanceYear}_${searchParam}_${publishHistoryId}`
    : `qpp_${searchType}_${performanceYear}_${publishHistoryId}`);

const getQppCache = Promise.coroutine(function* getQppCache(
  searchType,
  searchParam,
  publishHistoryId,
  performanceYear
) {
  const key = getKeyName(searchType, searchParam, publishHistoryId, performanceYear);
  const redisClient = Redis.getClient();
  if (!redisClient) {
    return null;
  }

  try {
    return JSON.parse(yield redisClient.getAsync(key) || null);
  } catch (err) {
    return null;
  }
});

const setQppCache = Promise.coroutine(function* setQppCache(
  searchType,
  searchParam,
  publishHistoryId,
  performanceYear,
  data
) {
  const key = getKeyName(searchType, searchParam, publishHistoryId, performanceYear);
  const redisClient = Redis.getClient();
  if (!redisClient) {
    return null;
  }

  try {
    return yield redisClient.setAsync(key, JSON.stringify(data));
  } catch (err) {
    return null;
  }
});

module.exports = (searchType, searchParam, publishHistoryId, performanceYear) =>
  getQppCache(getKeyName(searchType, searchParam, publishHistoryId, performanceYear));
module.exports.getByEntityId = getQppCache.bind(null, 'entity');
module.exports.setByEntityId = setQppCache.bind(null, 'entity');
module.exports.getByNpi = getQppCache.bind(null, 'npi');
module.exports.setByNpi = setQppCache.bind(null, 'npi');
module.exports.getByTin = getQppCache.bind(null, 'tin');
module.exports.setByTin = setQppCache.bind(null, 'tin');
module.exports.getEntityList = getQppCache.bind(null, 'entity_list', null);
module.exports.setEntityList = setQppCache.bind(null, 'entity_list', null);
