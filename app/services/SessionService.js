const moment = require('moment');
const config = require('../config/index');
const logger = require('./logger/index');
const Session = require('./mongoose/models/session');
const User = require('./mongoose/models/user');

class SessionService {
  static handleError(res, err, message) {
    logger.error(err);
    res.status(500).json({
      message
    });
  }

  static getSessionInfo(session) {
    const sessionResults = {
      success: true,
      expiresInSeconds: 0,
      isActive: false
    };
    if (session) {
      if (!session.expiresAt) {
        sessionResults.expiresInSeconds = Number.MAX_VALUE;
      } else {
        sessionResults.expiresInSeconds = moment
          .utc(session.expiresAt)
          .diff(moment.utc(), 'seconds');
      }
      sessionResults.isActive = sessionResults.expiresInSeconds > 0;

      if (session.data) {
        sessionResults.username = session.data.username;
      }
    }

    return sessionResults;
  }

  static async createSession(req, res) {
    try {
      const { session, token } = await Session.createSession(req.body, config.SESSION.TIMEOUT_IN_SECONDS);

      const user = await User.getUser(session.data.username);
      const sessionInfo = SessionService.getSessionInfo(session);

      res.status(200).json({
        data: {
          user: Object.assign({}, user, { token, session: sessionInfo })
        }
      });
    } catch (err) {
      SessionService.handleError(res, err, 'There was an error while getting the session');
    }
  }

  static async deleteSession(req, res) {
    try {
      await Session.deleteSession(req.query.token);
      res.status(200).json({
        data: {
          success: true
        }
      });
    } catch (err) {
      SessionService.handleError(res, err, 'There was an error while deleting the session');
    }
  }

  static async getSession(req, res) {
    try {
      const token = req.query.token.replace('Bearer ', '');

      let sessionResult;
      if (req.query.extend === 'true') {
        sessionResult = await Session.getAndExtendSession(token);
      } else {
        sessionResult = await Session.getSession(token);
      }
      const { session } = sessionResult;

      if (!session) {
        res.status(200).json({
          data: {
            user: null
          }
        });
        return;
      }

      const user = await User.getUser(session.data.username);
      const sessionInfo = SessionService.getSessionInfo(session);

      res.status(200).json({
        data: {
          user: Object.assign({}, user, { token, session: sessionInfo })
        }
      });
    } catch (err) {
      SessionService.handleError(res, err, 'There was an error while getting the session');
    }
  }
}

module.exports = SessionService;
