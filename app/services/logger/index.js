const sharedLogger = require('qpp-shared-logger-node');
const uuid = require('uuid');

const name = 'ams-data-worker';
sharedLogger.configure(
  process.env.NODE_ENV === 'production'
    ? {
      projectSlug: name,
      logDirectory: '/var/log/amsapp',
      logFilenamePrefix: 'amsapp.app',
      accessLog: {
        logDirectory: '/var/log/amsapp',
        logFilenamePrefix: 'amsapp.access',
        rotationMaxsize: 'none'
      }
    }
    : {
      projectSlug: name
    }
);

module.exports = sharedLogger.contextLogger({ process_id: uuid.v4() });
module.exports.accessLogger = sharedLogger.accessLogger.bind(sharedLogger);
