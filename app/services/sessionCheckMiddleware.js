const jwt = require('jsonwebtoken');
const config = require('../config');
const Session = require('./mongoose/models/session');
const { requiresAuthentication } = require('./utils/jwt');

module.exports = async (req, res, next) => {
  try {
    if (!requiresAuthentication(req.method, `${req.baseUrl}${req.path}`)) {
      return next();
    }

    const token = (req.headers.authorization || '').replace('Bearer ', '');

    if (!token) {
      return res.status(401).json({ message: 'No authentication token provided.' });
    }

    if (jwt.verify(token, config.JWT.SECRET).type === 'system') {
      return next();
    }

    await Session.getAndExtendSession(token);
    return next();
  } catch (err) {
    return next(err);
  }
};
