const Promise = require('bluebird');
const mongoose = require('mongoose');
const moment = require('moment');
const logger = require('../logger');

class PublishService {
  static async publish(sourceModel, targetModel, attempt = 0) {
    try {
      const sourceRename = Promise.promisify(sourceModel.collection.rename.bind(sourceModel.collection));
      const targetRename = Promise.promisify(targetModel.collection.rename.bind(targetModel.collection));

      const sourceName = sourceModel.collection.collectionName;
      const targetName = targetModel.collection.collectionName;

      const collectionsNames = await mongoose.connection.db
        .listCollections({
          name: targetName
        })
        .toArray();

      if (collectionsNames.length >= 1) {
        const historicalName = `${targetName}_${moment.utc().format()}`;
        logger.info(`Renaming ${targetName} to ${historicalName}`);
        await targetRename(historicalName);
        logger.info(`Renamed ${targetName} to ${historicalName}`);
      }

      logger.info(`Renaming ${sourceName} to ${targetName}`);
      await sourceRename(targetName);
      return logger.info(`Renamed ${sourceName} to ${targetName}`);
    } catch (err) {
      logger.error(err);
      if (attempt >= 2) {
        throw err;
      }
      const newAttempt = attempt + 1;
      const result = await PublishService.publish(sourceModel, targetModel, newAttempt);
      return result;
    }
  }

  static async merge(fileId, sourceModel, targetModel, keyFn, sourceFileType, fromRow, toRow) {
    const sourceName = sourceModel.collection.collectionName;
    const targetName = targetModel.collection.collectionName;
    const query = { file_id: fileId, invalid: false };

    logger.info(`Merging ${sourceName} to ${targetName}`);

    if (fromRow || toRow) {
      query.file_line_number = { $gte: fromRow, $lte: toRow };
      logger.info(`Merging rows ${fromRow}-${toRow}`);
    }

    const cursor = sourceModel
      .find(query, { _id: false, invalid: false })
      .lean()
      .cursor();

    let bulk = targetModel.collection.initializeUnorderedBulkOp();
    let updates = 0;

    await cursor.eachAsync((data) => {
      updates += 1;

      if (sourceModel.collection.collectionName === 'source_file_provider_datas') {
        const formattedData = PublishService.formatProvider(data, sourceFileType, fileId);
        const participation = formattedData.participation;
        delete formattedData.participation;

        bulk
          .find(keyFn(data))
          .upsert()
          .updateOne({
            $set: formattedData,
            $addToSet: {
              participation
            }
          });
      } else {
        bulk
          .find(keyFn(data))
          .upsert()
          .updateOne({ $set: data });
      }

      if (updates === 500) {
        return new Promise((resolve, reject) => {
          bulk.execute((err, result) => {
            if (err && !result) {
              reject(err);
            } else {
              resolve(result);
            }
          });
        }).then((/* upsertResult */) => {
          // TODO: retry failed upsertResults as upsert may have failed due to non-atomic upsert
          updates = 0;
          bulk = targetModel.collection.initializeUnorderedBulkOp();
        });
      }

      return Promise.resolve();
    });

    if (updates > 0) {
      await new Promise((resolve, reject) => {
        bulk.execute((err, result) => {
          if (err && !result) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      }).then((/* upsertResult */) => {
        // TODO: retry failed upsertResults as upsert may have failed due to non-atomic upsert
        updates = 0;
      });
    }

    logger.info(`Merged ${sourceName} to ${targetName}`);
  }

  static formatProvider(provider, fileSourceType, fileId) {
    const participation = {
      source: {
        source_type: fileSourceType,
        file_id: fileId
      },
      start_date: provider.start_date,
      end_date: provider.end_date,
      ccn: provider.ccn,
      file_id: provider.file_id,
      validations: provider.validations,
      tin_type_code: provider.tin_type_code,
      specialty_code: provider.specialty_code,
      organization_name: provider.organization_name,
      first_name: provider.first_name,
      middle_name: provider.middle_name,
      last_name: provider.last_name,
      participant_type_code: provider.participant_type_code
    };
    return {
      entity_id: provider.entity_id,
      tin: provider.tin,
      npi: provider.npi,
      participation
    };
  }
}

module.exports = PublishService;
