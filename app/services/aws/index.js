const Promise = require('bluebird');
const AWS = require('aws-sdk');
const config = require('../../config');
const S3UploadStream = require('s3-upload-stream');

AWS.config.setPromisesDependency(Promise);

AWS.config.update({
  region: 'us-east-1'
});

const setUpS3 = () => {
  const s3Config = {
    region: 'us-east-1',
    s3ForcePathStyle: true,
    accessKeyId: config.AWS.ACCESS_KEY_ID,
    secretAccessKey: config.AWS.SECRET_ACCESS_KEY
  };

  /* istanbul ignore if */
  /* istanbul ignore else */
  if (config.AWS.LOCAL_AWS) {
    s3Config.endpoint = new AWS.Endpoint(config.DEV.S3);
  }

  return new AWS.S3(s3Config);
};

const s3 = setUpS3();

const setupSQS = () => {
  const sqsConfig = {
    region: 'us-east-1',
    apiVersion: '2012-11-05',
    accessKeyId: config.AWS.ACCESS_KEY_ID,
    secretAccessKey: config.AWS.SECRET_ACCESS_KEY
  };

  /* istanbul ignore if */
  /* istanbul ignore else */
  if (config.AWS.LOCAL_AWS) {
    sqsConfig.endpoint = new AWS.Endpoint(config.DEV.SQS);
  }

  return new AWS.SQS(sqsConfig);
};

const getSignedUrl = (key, expiresSeconds = 60 * 15, bucket = config.AWS.CLEAN_S3_BUCKET) =>
  s3.getSignedUrl('getObject', {
    Bucket: bucket,
    Key: key,
    Expires: expiresSeconds
  });

const getObject = (key, bucket = config.AWS.CLEAN_S3_BUCKET) => s3.getObject({ Bucket: bucket, Key: key });

const uploadS3 = (key, readStream, fileType, metadata, bucket = config.AWS.CLEAN_S3_BUCKET) =>
  new Promise((resolve, reject) => {
    const s3Stream = S3UploadStream(s3);
    try {
      const upload = s3Stream.upload({
        Bucket: bucket,
        Key: key,
        ContentType: fileType,
        Metadata: metadata
      });

      upload.on('error', reject);
      upload.on('uploaded', resolve);

      readStream.pipe(upload);
    } catch (err) {
      reject(err);
    }
  });


module.exports = {
  S3: s3,
  SQS: setupSQS(),
  util: { getSignedUrl, getObject, uploadS3 }
};
