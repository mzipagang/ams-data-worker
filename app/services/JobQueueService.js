const QueuedJobs = require('./mongoose/models/queued_jobs');
const moment = require('moment');

/**
 * Service for interacting with the job queue
 */
class JobQueueService {
  /**
   * Get the queued job record with a given id
   * @param id
   * @returns {*}
   */
  static getJobById(id) {
    return QueuedJobs.findOne({ id }, '-_id -__v').lean();
  }

  /**
   * Creates a job in mongo with a given type and parameters
   * @param type the type of job requested
   * @param parameters specific parameters for the job
   * @param username name of the user requesting the job
   * @returns {*}
   */
  static createJob(type, parameters, username) {
    if (!type) {
      return null;
    }

    return new QueuedJobs({
      type,
      status: 'queued',
      parameters,
      details: {
        submittedOn: moment.utc().toDate(),
        lastUpdatedOn: moment.utc().toDate(),
        updatedBy: username
      }
    }).save();
  }

  /**
   * Update the status of a job entry with the given id
   * @param jobId the id of a job to be updated
   * @param status the status of the given job
   * @returns {*}
   */
  static async updateJobStatus(jobId, status, results) {
    return QueuedJobs.findOneAndUpdate(
      { id: jobId },
      { status, results, lastUpdatedOn: moment.utc().toDate() },
      { new: true, fields: '-_id -__v' }
    ).lean();
  }

  /**
   * Sets the status of a job to running for given a job id
   * @param jobId the id of a job to be updated
   * @returns {*}
   */
  static async updateJobStatusToRunning(jobId) {
    return JobQueueService.updateJobStatus(jobId, 'running');
  }

  /**
   * Sets the status of a job to complete for given a job id
   * @param jobId the id of a job to be updated
   * @returns {*}
   */
  static async updateJobStatusToCompleted(jobId, results) {
    return JobQueueService.updateJobStatus(jobId, 'complete-success', results);
  }

  /**
   * Sets the resultsError field of a job for a given job id
   * @param jobId the id of a job to be updated with errors
   * @param resultsError object containing message and code of error
   * @returns {*}
   */
  static async updateJobError(jobId, resultsError) {
    return QueuedJobs.findOneAndUpdate(
      { id: jobId },
      {
        resultsError,
        status: 'complete-error',
        lastUpdatedOn: moment.utc().toDate()
      },
      { new: true, fields: '-_id -__v' }
    ).lean();
  }

  /**
   * Removes specific fields from the queued_jobs objects given the status of the job
   * @param queuedJob
   * @returns {*}
   */
  static shapeJobStatusResponse(queuedJob) {
    const response = queuedJob;

    if (response.status !== 'complete-success') {
      delete response.results;
    }
    delete response.details;

    return response;
  }

  static async lockNextQueuedJob() {
    const query = {
      status: 'queued'
    };
    const update = {
      $set: {
        status: 'running'
      }
    };
    const options = {
      new: true,
      upsert: false,
      sort: { submittedOn: 1 },
      fields: '-_id -__v'
    };

    return QueuedJobs.findOneAndUpdate(query, update, options).lean();
  }
}

module.exports = JobQueueService;
