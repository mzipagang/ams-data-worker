const Promise = require('bluebird');
const redis = require('redis');
const config = require('../../config');
const logger = require('../../services/logger');

Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);

let redisClient;
let quitingRedis = false;

const connectToRedis = () =>
  new Promise((resolve, reject) => {
    try {
      const options = {
        host: config.REDIS.HOST,
        port: config.REDIS.PORT
      };

      if (config.REDIS.PW) {
        options.password = config.REDIS.PW;
      }

      const localRedisClient = redis.createClient(options);

      localRedisClient.on('connect', () => {
        if (quitingRedis) {
          localRedisClient.quit();
          resolve();
          return;
        }

        redisClient = localRedisClient;
        resolve(
          new Promise((eResolve, eReject) => {
            redisClient.on('error', (err) => {
              eReject(err);
            });
          })
        );
      });

      localRedisClient.on('error', (err) => {
        reject(err);
      });
    } catch (err) {
      reject(err);
    }
  });

module.exports = {
  connectToRedis: () => { // eslint-disable-line consistent-return
    if (quitingRedis) {
      redisClient = null;
      return new Promise(() => {
        /* Never resolve */
      });
    }

    connectToRedis().catch((err) => {
      redisClient = null;
      logger.error(err);

      return Promise.delay(5000).then(() => module.exports.connectToRedis());
    });
  },
  disconnectFromRedis: () =>
    new Promise((resolve) => {
      quitingRedis = true;

      if (redisClient) {
        redisClient.quit();
        redisClient = null;
      }

      resolve();
    }),
  getClient: () => redisClient
};
