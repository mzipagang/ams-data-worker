/**
 * A collection of utility functions for dealing with the metadata of an AMS Provider object.
 */

/**
 * Returns a list of file types where the given provider was sourced from
 */
function sourcedFrom(provider) {
  // adding items to a Set (and then going back to an array)
  // ensures we will not return duplicate values in the array
  const sourceTypes = new Set(
    provider.participation.map(participation => participation.source && participation.source.source_type)
  );
  return Array.from(sourceTypes.values());
}

const sourcedFromNgacoPreferredProvider = provider => sourcedFrom(provider).indexOf('ngaco-preferred-provider') > -1;
const sourcedFromMdmProvider = provider => sourcedFrom(provider).indexOf('mdm-provider') > -1;

module.exports = {
  sourcedFrom,
  sourcedFromNgacoPreferredProvider,
  sourcedFromMdmProvider
};
