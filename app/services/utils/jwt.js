const jwt = require('jsonwebtoken');

const { UNAUTHENTICATED_ROUTES } = require('../../constants');
const config = require('../../config');

const getTokenData = (req) => {
  if (!req || !req.headers || !req.headers.authorization) return null;
  const token = req.headers.authorization.replace('Bearer ', '');
  return jwt.verify(token, config.JWT.SECRET);
};

const getUsername = (req) => {
  const tokenData = getTokenData(req);
  if (!tokenData) return null;
  return tokenData.username;
};

const requiresAuthentication = (method, path) =>
  !UNAUTHENTICATED_ROUTES.some(route => route.method === method && route.path === path);

module.exports = {
  getTokenData,
  getUsername,
  requiresAuthentication
};
