const moment = require('moment');
/**
 * A collection of utility functions that help utilize objects that have activity date properties.
 * Activity date properties are `start_date` and `end_date`. These properties are used to determine
 * when an AMS Object is active.
 */

/**
 * Determines if obj is/was active on date
 * @param {object} obj an object with a `start_date` and `end_date` used to determine an active period
 * @param {object|string} obj.start_date the start date of the given obj
 * @param {object|string} obj.end_date the end date of the given obj
 * @param {moment-like} date the date to test whether the APM was active during or not
 * @returns {boolen} true if the given object was active during the given date; false otherwise
 */
function activeOn(obj, date) {
  const startDate = obj.start_date;
  const endDate = obj.end_date;

  return moment(date).isBetween(startDate, endDate, null, '[]');
}

/**
 * Determines if obj was terminated before the given date
 * @param {object} obj an object with a `start_date` and `end_date` to check against
 * @param {object|string} obj.start_date the start date of the given obj
 * @param {object|string} obj.end_date the end date of the given obj
 * @param {momnent-like} date a date to test entity against
 * @returns {boolean} true if the obj was terminated before date; false otherwise
 */
function terminatesBefore(obj, date) {
  return moment(obj.end_date).isBefore(moment(date));
}

/**
 * Determines if an objects start date is before the given date
 * @param {object} obj an object with a `start_date` and `end_date` to check against
 * @param {object|string} obj.start_date the start date of the given obj
 * @param {object|string} obj.end_date the end date of the given obj
 * @param {momnent-like} date a date to test `start_date` against
 * @returns {boolean} true if the entity was started after given date; false otherwise
 */
function startsAfter(obj, date) {
  return moment(obj.start_date).isAfter(moment(date));
}

module.exports = {
  activeOn,
  terminatesBefore,
  startsAfter
};
