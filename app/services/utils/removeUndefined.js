module.exports = (obj) => {
  const shallow = Object.assign({}, obj);

  Object.keys(shallow).forEach((key) => {
    if (shallow[key] === undefined) {
      delete shallow[key];
    }
  });

  return shallow;
};
