module.exports = async (year, providerCollection, outputCollection) => providerCollection.aggregate([
  {
    $lookup: {
      from: 'provider_specialty_codes',
      localField: 'npi',
      foreignField: 'npi',
      as: 'code'
    }
  },
  { $unwind: { path: '$code', preserveNullAndEmptyArrays: true } },
  // Next we have to remove any codes that are marked as deleted.
  {
    $addFields: {
      code: {
        $cond: {
          if: { $eq: ['$code.deleted', true] },
          then: undefined,
          else: '$code'
        }
      }
    }
  },
  { $unwind: { path: '$code.specialtyCodes', preserveNullAndEmptyArrays: true } },
  {
    $lookup: {
      from: 'specialty_codes',
      localField: 'code.specialtyCodes',
      foreignField: 'code',
      as: 'specialty_code'
    }
  },
  { $unwind: { path: '$specialty_code', preserveNullAndEmptyArrays: true } },
  // match only our year, or documents where we dont' have any matches at all.
  {
    $match: {
      $or: [{ 'specialty_code.year': year }, { specialty_code: null }]
    }
  },
  {
    $project: {
      code: '$specialty_code.code',
      isIndividual: {
        $cond: {
          if: {
            $or: [
              { $eq: ['$code.providerTypeCode', 'BP'] },
              { $eq: ['$code.providerTypeCode', 'D'] },
              { $eq: ['$code.providerTypeCode', 'UI'] }
            ]
          },
          then: true,
          else: false
        }
      },
      npi: '$npi',

      eligible_clinician: {
        $cond: {
          if: { $not: ['$specialty_code.eligible_clinician'] },
          then: 0,
          else: 1
        }
      },

      mips_eligibleClinician: {
        $cond: {
          if: { $not: ['$specialty_code.mips_eligibleClinician'] },
          then: 0,
          else: 1
        }
      }
    }
  },
  {
    $group: {
      _id: '$npi',
      isIndividual: { $max: '$isIndividual' },
      eligible_clinician: { $max: '$eligible_clinician' },
      mips_eligibleClinician: { $max: '$mips_eligibleClinician' },
      codes: { $push: '$code' }
    }
  },
  // I ran in to what I think was a bug in mongo, so I had to take our resulting set and re-lookup the provider specialty codes...
  {
    $lookup: {
      from: 'provider_specialty_codes',
      localField: '_id',
      foreignField: 'npi',
      as: 'raw_codes'
    }
  },
  { $unwind: { path: '$raw_codes', preserveNullAndEmptyArrays: true } },
  {
    $project: {
      _id: '$_id',
      isIndividual: '$isIndividual',
      isNotFound: {
        $cond: {
          if: { $eq: ['$raw_codes.deleted', false] },
          then: false,
          else: true
        }
      },
      isMissingSpecialityCode:
          {
            $cond: {
              if: { $gt: [{ $size: { $ifNull: ['$codes', []] } }, 0] },
              then: false,
              else: true
            }
          },
      codes: '$codes',
      npi: '$_id',
      eligibleClinician: {
        $cond: {
          if: {
            $and: [{ $eq: ['$eligible_clinician', 1] }, { $eq: ['$isIndividual', true] }]
          },
          then: true,
          else: false
        }
      },

      mipsEligibleClinician: {
        $cond: {
          if: {
            $and: [{ $eq: ['$mips_eligibleClinician', 1] }, { $eq: ['$isIndividual', true] }]
          },
          then: true,
          else: false
        }
      }
    }
  },
  {
    $out: outputCollection
  }
]);
