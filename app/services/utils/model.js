/**
 * A collection of utility functions for dealing with the metadata of an AMS Models object.
 */

/**
 * Given an APM ID of a resource (i.e. provider, entity) determine if
 * it is participating in an Advanced Payment Model (APM)
 * @param {string} apmId the APM ID of the resource
 * @param {object[]} pacModels a collection of PAC Models to check against
 * @returns {boolean} true if the resource is participating in an APM; false otherwise
 */
const participatingInApm = (apmId, pacModels) => apmId && pacModels.some(model => model.id === apmId);

/**
 * Returns a boolean indicating weither the given Provider is fully participating in an Advanced APM or not
 * @returns {boolean} true if the Provider is participating in an Advanced APM; false otherwise
 */
function isFullAdvancedApm(model) {
  return model.advanced_apm_flag === 'Y';
}

/**
 * Returns a boolean indicating weither the given Provider is partially participating in an Advanced APM or not
 * @returns {boolean} true if the Provider is participating in an Advanced APM; false otherwise
 */
function isPartialAdvancedApm(model) {
  return model.advanced_apm_flag === 'P';
}

/**
 * Returns a boolean indicating weither the given Provider is either fully or partially participating in an Advanced APM or not
 * @returns {boolean} true if the Provider is participating in an Advanced APM; false otherwise
 */
function isFullOrPartialAdvancedApm(model) {
  return isFullAdvancedApm(model) || isPartialAdvancedApm(model);
}

/**
 * Returns a boolean indicating whether the given model is a full MIPS Alternative Payment Model (APM)
 * @param {model} model The APM to check against
 * @returns {boolean} true if the Provider is participating in a MIPS APM; false otherwise
 */
function isFullMipsApm(model) {
  return model.mips_apm_flag === 'Y';
}

/**
 * Returns a boolean indicating whether the given model is a partial MIPS Alternative Payment Model (APM)
 * @param {model} model The APM to check against
 * @returns {boolean} true if the Provider is participating in a MIPS APM; false otherwise
 */
function isPartialMipsApm(model) {
  return model.mips_apm_flag === 'P';
}

/**
 * Returns a boolean indicating whether the given model is either a full or partial MIPS Alternative Payment Model (APM)
 * @param {model} model The APM to check against
 * @returns {boolean} true if the Provider is participating in a MIPS APM; false otherwise
 */
function isFullOrPartialMipsApm(model) {
  return isFullMipsApm(model) || isPartialMipsApm(model);
}

/**
 * Returns a boolean indicating whether the given model is a Next Generation ACO (NGACO);
 * @param {model} model The Advanced Payment Model to check against
 * @returns {boolean} true if model is NGACO
 */
function isNextGenerationAco(model) {
  return model.name === 'Next Generation ACO Model';
}

module.exports = {
  participatingInApm,
  isFullAdvancedApm,
  isPartialAdvancedApm,
  isFullOrPartialAdvancedApm,
  isFullMipsApm,
  isPartialMipsApm,
  isFullOrPartialMipsApm,
  isNextGenerationAco
};
