const _ = require('lodash');
const logger = require('../logger');

const parseTins = (input) => {
  const goodTins = [];
  const badTins = [];

  let tmpTinsArray = [];

  if (Array.isArray(input)) {
    tmpTinsArray = input;
  } else { // we might want to do a typeof tins === 'string'
    tmpTinsArray = input.split(',');
  }

  _.chain(tmpTinsArray)
    .map(tin => tin.trim()) // Trim whitespace from all input
    .uniq() // Remove duplicates
    .each((tin) => {
      if (tin === '' || typeof tin !== 'string') {
        return;
      }

      const result = /^\d{9}$/.test(tin);

      if (result) {
        goodTins.push(tin);
      } else {
        badTins.push(tin);
      }
    })
    .value();

  return {
    goodTins,
    badTins
  };
};

const xtinHeaderCheck = (xtinHeader) => {
  const tins = parseTins(xtinHeader);
  if (tins.badTins.length > 0) {
    const redactedXTinHeader = xtinHeader.replace(new RegExp('\\d', 'g'), '#');
    logger.info(`x-tins header contained invalid tins. Redacted x-tins header was: "${redactedXTinHeader}"`);
  }
};

module.exports = {
  parseTins,
  xtinHeaderCheck
};
