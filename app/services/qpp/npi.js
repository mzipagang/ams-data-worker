const Promise = require('bluebird');
const _ = require('lodash');
const config = require('../../config');
const DynamicPACModel = require('../mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../mongoose/dynamic_name_models/pac_entity');
const DynamicPACProvider = require('../mongoose/dynamic_name_models/pac_provider');
const DynamicPACLvt = require('../mongoose/dynamic_name_models/pac_lvt');
const DynamicPACQPScore = require('../mongoose/dynamic_name_models/pac_qp_score');
const DynamicPACQPStatus = require('../mongoose/dynamic_name_models/pac_qp_status');
const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');
const qppRedis = require('../qppRedis');
const logger = require('../logger');

module.exports = Promise.coroutine(function* handle(npisIn, performanceYear = 2017, publishHistoryId = null) {
  if (!performanceYear) {
    performanceYear = 2017;
  }

  const npis = _.uniq(npisIn);

  const {
    pacModelFile,
    pacSubdivisionFile,
    pacEntityFile,
    pacProviderFile,
    pacLvtFile,
    pacQpStatusFile,
    pacQpScoreFile,
    allPACDataExists,
    publishHistory
  } = yield pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear, publishHistoryId);

  // return empty array if one collection is missing
  if (!allPACDataExists) {
    return [];
  }

  const cachedNPIData = [];
  const nonCachedNpis = [];

  if (config.CACHE.ENABLED) {
    yield Promise.all(
      npis.map(
        Promise.coroutine(function* getCache(npi) {
          const cache = yield qppRedis.getByNpi(npi, publishHistory.id, performanceYear);
          if (cache) {
            cachedNPIData.push(cache);
          } else {
            nonCachedNpis.push(npi);
          }
        })
      )
    );
  } else {
    nonCachedNpis.push(...npis);
  }

  if (nonCachedNpis.length > 0) {
    logger.debug('NPI cache miss');

    const PACModel = DynamicPACModel(pacModelFile.file_id);
    const PACSubdivision = DynamicPACSubdivision(pacSubdivisionFile.file_id);
    const PACEntity = DynamicPACEntity(pacEntityFile.file_id);
    const PACProvider = DynamicPACProvider(pacProviderFile.file_id);
    const PACLvt = DynamicPACLvt(pacLvtFile.file_id);
    const PACQPScore = DynamicPACQPScore(pacQpScoreFile.file_id);
    const PACQPStatus = DynamicPACQPStatus(pacQpStatusFile.file_id);

    const data = yield PACProvider.aggregate([
      {
        $match: {
          npi: { $in: nonCachedNpis },
          duplicate: false
        }
      },
      {
        $lookup: {
          from: PACEntity.collection.name,
          localField: 'entity_id',
          foreignField: 'id',
          as: 'entity_data'
        }
      },
      {
        $project: {
          _id: false,
          entity_id: true,
          tin: true,
          npi: true,
          provider_relationship_code: true,
          entity_data: { $arrayElemAt: ['$entity_data', 0] }
        }
      },
      {
        $lookup: {
          from: PACLvt.collection.name,
          localField: 'entity_data.id',
          foreignField: 'entity_id',
          as: 'lvt_data'
        }
      },
      {
        $lookup: {
          from: PACModel.collection.name,
          localField: 'entity_data.apm_id',
          foreignField: 'id',
          as: 'model_data'
        }
      },
      {
        $lookup: {
          from: PACSubdivision.collection.name,
          localField: 'entity_data.subdiv_id',
          foreignField: 'id',
          as: 'subdivision_data'
        }
      },
      {
        $lookup: {
          from: PACQPScore.collection.name,
          localField: 'npi',
          foreignField: 'npi',
          as: 'qp_score_data'
        }
      },
      {
        $project: {
          entity_id: true,
          tin: true,
          npi: true,
          provider_relationship_code: true,
          entity_data: true,
          model_data: { $arrayElemAt: ['$model_data', 0] },
          lvt_data: { $arrayElemAt: ['$lvt_data', 0] },
          subdivision_data: {
            $filter: {
              input: '$subdivision_data',
              as: 'subdivisiondata',
              cond: { $eq: ['$$subdivisiondata.apm_id', '$entity_data.apm_id'] }
            }
          },
          qp_score_data: {
            $filter: {
              input: '$qp_score_data',
              as: 'qpscoredata',
              cond: {
                $and: [{ $eq: ['$$qpscoredata.tin', '$tin'] }, { $eq: ['$$qpscoredata.entity_id', '$entity_data.id'] }]
              }
            }
          }
        }
      },
      {
        $project: {
          entity_id: true,
          tin: true,
          npi: true,
          provider_relationship_code: true,
          entity_data: true,
          model_data: true,
          lvt_data: true,
          subdivision_data: { $arrayElemAt: ['$subdivision_data', 0] },
          qp_score_data: { $arrayElemAt: ['$qp_score_data', 0] }
        }
      },
      {
        $group: {
          _id: '$npi',
          data: { $push: '$$ROOT' }
        }
      },
      {
        $project: {
          _id: false,
          npi: '$_id',
          data: true
        }
      },
      {
        $lookup: {
          from: PACQPStatus.collection.name,
          localField: 'npi',
          foreignField: 'npi',
          as: 'qp_status_data'
        }
      },
      {
        $project: {
          _id: false,
          npi: true,
          data: true,
          qp_status_data: { $arrayElemAt: ['$qp_status_data', 0] }
        }
      },
      {
        $project: {
          _id: false,
          npi: true,
          data: true,
          qp_status: '$qp_status_data.qp_status'
        }
      }
    ]);

    const mapped = data.map((npiData) => {
      const tinMap = {};
      npiData.data.forEach((tinData) => {
        if (!tinMap[tinData.tin]) {
          tinMap[tinData.tin] = [];
        }
        tinMap[tinData.tin].push(tinData);
      });

      return {
        npi: npiData.npi || null,
        qp_status: npiData.qp_status || null,
        tins: Object.keys(tinMap).map(tin => ({
          tin,
          entities: tinMap[tin].map((raw) => {
            let lvtFlag = (raw.lvt_data || {}).status;
            if (lvtFlag === 'A') {
              lvtFlag = 'F';
            } else if (lvtFlag === 'B') {
              lvtFlag = 'T';
            } else if (lvtFlag === 'Y') {
              lvtFlag = 'T';
            } else if (lvtFlag === 'N') {
              lvtFlag = 'F';
            }

            let advancedAPMFlag = (raw.model_data || {}).advanced_apm_flag;
            if (advancedAPMFlag === 'P') {
              advancedAPMFlag = (raw.subdivision_data || {}).advanced_apm_flag;
            }

            let mipsAPMFlag = (raw.model_data || {}).mips_apm_flag;
            if (mipsAPMFlag === 'P') {
              mipsAPMFlag = (raw.subdivision_data || {}).mips_apm_flag;
            }

            return {
              entity_id: (raw.entity_data || {}).id || null,
              entity_name: (raw.entity_data || {}).name || null,
              lvt_flag: lvtFlag || null,
              lvt_payments: (raw.lvt_data || {}).payments || null,
              lvt_patients: (raw.lvt_data || {}).patients || null,
              lvt_small_status: (raw.lvt_data || {}).small_status || null,
              lvt_performance_year: (raw.lvt_data || {}).performance_year || null,
              entity_tin: (raw.entity_data || {}).tin,
              provider_relationship_code: raw.provider_relationship_code || null,
              qp_payment_threshold_score: (raw.qp_score_data || {}).payment_threshold_score || null,
              qp_patient_threshold_score: (raw.qp_score_data || {}).patient_threshold_score || null,
              apm_id: (raw.model_data || {}).id || null,
              apm_name: (raw.model_data || {}).name || null,
              subdivision_id: (raw.subdivision_data || {}).id || null,
              subdivision_name: (raw.subdivision_data || {}).name || null,
              advanced_apm_flag: advancedAPMFlag || null,
              mips_apm_flag: mipsAPMFlag || null
            };
          })
        }))
      };
    });

    if (config.CACHE.ENABLED) {
      yield Promise.all(
        mapped.map(
          Promise.coroutine(function* getFromDataWorker(npiData) {
            cachedNPIData.push(npiData);
            yield qppRedis.setByNpi(npiData.npi, publishHistory.id, performanceYear, npiData);
          })
        )
      );
    } else {
      cachedNPIData.push(...mapped);
    }
  }

  return cachedNPIData;
});
