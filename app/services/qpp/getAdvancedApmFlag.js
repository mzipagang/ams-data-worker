module.exports = (provider) => {
  const flag = provider.model.advanced_apm_flag;
  if (flag === 'P' && provider.subdivision) {
    return provider.subdivision.advanced_apm_flag;
  }
  return flag;
};
