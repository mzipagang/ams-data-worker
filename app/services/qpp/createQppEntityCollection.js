const Promise = require('bluebird');
const _ = require('lodash');
const DynamicPACModel = require('../mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../mongoose/dynamic_name_models/pac_entity');
const DynamicPACProvider = require('../mongoose/dynamic_name_models/pac_provider');
const DynamicPACLvt = require('../mongoose/dynamic_name_models/pac_lvt');
const DynamicPACQPScore = require('../mongoose/dynamic_name_models/pac_qp_score');
const DynamicPACQPStatus = require('../mongoose/dynamic_name_models/pac_qp_status');
const SourceFileQpcScoreSummary = require('../mongoose/models/source_file_qpc_score_summary');
const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');
const qppEntityList = require('./entity_list');
const QppEntity = require('../mongoose/models/qpp_entity');
const toChunks = require('../../services/utils/toChunks');
const { getHighestValue } = require('../../schemas/Snapshot');

class CreateQppEntityCollection {
  static async getPerformanceYears() {
    return pacPublishHistoryUtils.getPerformanceYears();
  }

  static async getEntityList(performanceYear) {
    return qppEntityList(performanceYear);
  }

  static async getQppEntitiesByPerformanceYear(entityIdsIn, performanceYear = 2017, publishHistoryId = null) {
    const entityIds = _.uniq(entityIdsIn);

    const {
      pacModelFile,
      pacSubdivisionFile,
      pacEntityFile,
      pacProviderFile,
      pacLvtFile,
      pacQpStatusFile,
      pacQpScoreFile,
      allPACDataExists
    } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear, publishHistoryId);

    // return empty array if one collection is missing
    if (!allPACDataExists) {
      return [];
    }

    const nonCachedEntityIds = [];

    nonCachedEntityIds.push(...entityIds);

    const PACModel = DynamicPACModel(pacModelFile.file_id);
    const PACSubdivision = DynamicPACSubdivision(pacSubdivisionFile.file_id);
    const PACEntity = DynamicPACEntity(pacEntityFile.file_id);
    const PACProvider = DynamicPACProvider(pacProviderFile.file_id);
    const PACLvt = DynamicPACLvt(pacLvtFile.file_id);
    const PACQPScore = DynamicPACQPScore(pacQpScoreFile.file_id);
    const PACQPStatus = DynamicPACQPStatus(pacQpStatusFile.file_id);


    const data = await PACEntity.aggregate([
      {
        $match: {
          id: { $in: nonCachedEntityIds }
        }
      },
      {
        $lookup: {
          from: PACLvt.collection.name,
          localField: 'id',
          foreignField: 'entity_id',
          as: 'lvt_data'
        }
      },
      {
        $lookup: {
          from: PACModel.collection.name,
          localField: 'apm_id',
          foreignField: 'id',
          as: 'model_data'
        }
      },
      {
        $lookup: {
          from: PACSubdivision.collection.name,
          localField: 'subdiv_id',
          foreignField: 'id',
          as: 'subdivision_data'
        }
      },
      {
        $lookup: {
          from: SourceFileQpcScoreSummary.collection.name,
          localField: 'id',
          foreignField: 'entity_id',
          as: 'qpc_data'
        }
      },
      {
        $project: {
          id: true,
          tin: true,
          name: true,
          model_data: { $arrayElemAt: ['$model_data', 0] },
          qpc_data: { $arrayElemAt: ['$qpc_data', 0] },
          subdivision_data: {
            $arrayElemAt: [
              {
                $filter: {
                  input: '$subdivision_data',
                  as: 'subdivisiondata',
                  cond: { $eq: ['$$subdivisiondata.apm_id', '$apm_id'] }
                }
              },
              0
            ]
          },
          lvt_data: { $arrayElemAt: ['$lvt_data', 0] }
        }
      },
      {
        $project: {
          _id: false,
          entity_id: '$id',
          entity_name: '$name',
          lvt_flag: '$lvt_data.status',
          lvt_payments: '$lvt_data.payments',
          lvt_patients: '$lvt_data.patients',
          lvt_small_status: '$lvt_data.small_status',
          lvt_performance_year: '$lvt_data.performance_year',
          complex_patient_score: '$lvt_data.complex_patient_score',
          final_qpc_score: '$qpc_data.final_qpc_score',
          entity_tin: '$tin',
          apm_id: '$model_data.id',
          apm_name: '$model_data.name',
          subdivision_id: '$subdivision_data.id',
          subdivision_name: '$subdivision_data.name',
          model_advanced_apm_flag: '$model_data.advanced_apm_flag',
          model_mips_apm_flag: '$model_data.mips_apm_flag',
          subdivision_advanced_apm_flag: '$subdivision_data.advanced_apm_flag',
          subdivision_mips_apm_flag: '$subdivision_data.mips_apm_flag'
        }
      }
    ]).allowDiskUse(true);

    const providerData = await PACProvider.aggregate([
      {
        $match: {
          entity_id: { $in: entityIds },
          duplicate: false
        }
      },
      {
        $lookup: {
          from: PACQPStatus.collection.name,
          localField: 'npi',
          foreignField: 'npi',
          as: 'qp_status_data'
        }
      },
      {
        $lookup: {
          from: PACQPScore.collection.name,
          localField: 'npi',
          foreignField: 'npi',
          as: 'qp_score_data'
        }
      },
      {
        $project: {
          entity_id: true,
          tin: true,
          npi: true,
          provider_relationship_code: true,
          run_snapshot: true,
          qp_status_data: { $arrayElemAt: ['$qp_status_data', 0] },
          qp_score_data: {
            $arrayElemAt: [
              {
                $filter: {
                  input: '$qp_score_data',
                  as: 'qpscoredata',
                  cond: {
                    $and: [{ $eq: ['$$qpscoredata.tin', '$tin'] }, { $eq: ['$$qpscoredata.entity_id', '$entity_id'] }]
                  }
                }
              },
              0
            ]
          }
        }
      },
      {
        $project: {
          entity_id: true,
          tin: true,
          npi: true,
          mips_ec_indicator: true,
          provider_relationship_code: true,
          run_snapshot: true,
          qp_status: '$qp_status_data.qp_status',
          score_type: '$qp_score_data.score_type',
          qp_payment_threshold_score: '$qp_score_data.payment_threshold_score',
          qp_patient_threshold_score: '$qp_score_data.patient_threshold_score'
        }
      },
      {
        $group: {
          _id: {
            entity_id: '$entity_id',
            tin: '$tin',
            run_snapshot: '$run_snapshot'
          },
          data: { $push: '$$ROOT' }
        }
      },
      {
        $project: {
          _id: false,
          entity_id: '$_id.entity_id',
          tin: '$_id.tin',
          run_snapshot: '$_id.run_snapshot',
          'data.npi': true,
          'data.provider_relationship_code': true,
          'data.qp_status': true,
          'data.score_type': true,
          'data.qp_payment_threshold_score': true,
          'data.qp_patient_threshold_score': true
        }
      }
    ]).allowDiskUse(true);

    return data.map((entityData) => {
      const finalEntityData = Object.assign({}, entityData);

      finalEntityData.performance_year = performanceYear;

      const lvtFlag = finalEntityData.lvt_flag;
      if (lvtFlag === 'A') {
        finalEntityData.lvt_flag = 'F';
      } else if (lvtFlag === 'B') {
        finalEntityData.lvt_flag = 'T';
      } else if (lvtFlag === 'Y') {
        finalEntityData.lvt_flag = 'T';
      } else if (lvtFlag === 'N') {
        finalEntityData.lvt_flag = 'F';
      }

      finalEntityData.lvt_flag = finalEntityData.lvt_flag || null;
      finalEntityData.lvt_payments = finalEntityData.lvt_payments || null;
      finalEntityData.lvt_patients = finalEntityData.lvt_patients || null;
      finalEntityData.lvt_small_status = finalEntityData.lvt_small_status || null;
      finalEntityData.lvt_performance_year = finalEntityData.lvt_performance_year || null;
      finalEntityData.complex_patient_score = finalEntityData.complex_patient_score || null;
      finalEntityData.final_qpc_score = finalEntityData.final_qpc_score || null;

      let advancedAPMFlag = finalEntityData.model_advanced_apm_flag;
      if (advancedAPMFlag === 'P') {
        advancedAPMFlag = finalEntityData.subdivision_advanced_apm_flag;
      }
      finalEntityData.advanced_apm_flag = advancedAPMFlag;
      delete finalEntityData.model_advanced_apm_flag;
      delete finalEntityData.subdivision_advanced_apm_flag;

      let mipsAPMFlag = finalEntityData.model_mips_apm_flag;
      if (mipsAPMFlag === 'P') {
        mipsAPMFlag = finalEntityData.subdivision_mips_apm_flag;
      }
      finalEntityData.mips_apm_flag = mipsAPMFlag;
      delete finalEntityData.model_mips_apm_flag;
      delete finalEntityData.subdivision_mips_apm_flag;

      const entityId = entityData.entity_id;

      // Finding the higher run_snapshot value. According to Joe it has to be from the provider level
      finalEntityData.run_snapshot = getHighestValue(providerData.map(provider => provider.run_snapshot));

      finalEntityData.tins = providerData
        .filter(provider => provider.entity_id === entityId)
        .map(provider => ({
          tin: provider.tin,
          npis: provider.data.map(npiData => ({
            npi: npiData.npi || null,
            mips_ec_indicator: npiData.mips_ec_indicator || null,
            qp_status: npiData.qp_status || null,
            final_qpc_score: npiData.final_qpc_score || null,
            score_type: npiData.score_type || null,
            qp_payment_threshold_score: npiData.qp_payment_threshold_score || null,
            qp_patient_threshold_score: npiData.qp_patient_threshold_score || null,
            provider_relationship_code: npiData.provider_relationship_code || null
          }))
        }));

      return finalEntityData;
    });
  }

  static async create() {
    const performanceYears = await CreateQppEntityCollection.getPerformanceYears();
    await Promise.all(performanceYears.map(async (performanceYear) => {
      const entityList = await CreateQppEntityCollection.getEntityList(performanceYear);
      const entityIds = entityList.map(entity => entity.id);
      const entities = await CreateQppEntityCollection.getQppEntitiesByPerformanceYear(entityIds, performanceYear);
      await CreateQppEntityCollection.clearEntitiesInPerformanceYear(performanceYear);
      await CreateQppEntityCollection.saveEntities(entities);
    }));
  }

  static async clearEntitiesInPerformanceYear(performanceYear) {
    return QppEntity.deleteMany({ performance_year: performanceYear });
  }

  static async saveEntities(entities) {
    const chunks = toChunks(entities, 10);

    for (let i = 0; i < chunks.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      await QppEntity.insertMany(chunks[i]);
    }
  }
}

module.exports = CreateQppEntityCollection;
