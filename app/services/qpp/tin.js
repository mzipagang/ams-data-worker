const Promise = require('bluebird');
const config = require('../../config');
const DynamicPACModel = require('../mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../mongoose/dynamic_name_models/pac_entity');
const DynamicPACProvider = require('../mongoose/dynamic_name_models/pac_provider');
const DynamicPACLvt = require('../mongoose/dynamic_name_models/pac_lvt');
const DynamicPACQPScore = require('../mongoose/dynamic_name_models/pac_qp_score');
const DynamicPACQPStatus = require('../mongoose/dynamic_name_models/pac_qp_status');
const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');
const qppRedis = require('../qppRedis');
const logger = require('../logger');
const qppUtility = require('./utility');

module.exports = Promise.coroutine(function* handle(tinsIn, performanceYear = 2017, publishHistoryId = null) {
  if (!performanceYear) {
    performanceYear = 2017;
  }

  const parsedTins = qppUtility.parseTins(tinsIn);

  const tins = parsedTins.goodTins;

  const {
    publishHistory,
    pacModelFile,
    pacSubdivisionFile,
    pacEntityFile,
    pacProviderFile,
    pacLvtFile,
    pacQpStatusFile,
    pacQpScoreFile,
    allPACDataExists
  } = yield pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear, publishHistoryId);

  // return empty array if one collection is missing
  if (!allPACDataExists) {
    return [];
  }

  const cachedTINData = [];
  const nonCachedTins = [];

  if (config.CACHE.ENABLED) {
    yield Promise.all(
      tins.map(
        Promise.coroutine(function* getCache(tin) {
          const cache = yield qppRedis.getByTin(tin, publishHistory.id, performanceYear);
          if (cache) {
            cachedTINData.push(cache);
          } else {
            nonCachedTins.push(tin);
          }
        })
      )
    );
  } else {
    nonCachedTins.push(...tins);
  }

  if (nonCachedTins.length > 0) {
    logger.debug('TIN cache miss');

    const PACModel = DynamicPACModel(pacModelFile.file_id);
    const PACSubdivision = DynamicPACSubdivision(pacSubdivisionFile.file_id);
    const PACEntity = DynamicPACEntity(pacEntityFile.file_id);
    const PACProvider = DynamicPACProvider(pacProviderFile.file_id);
    const PACLvt = DynamicPACLvt(pacLvtFile.file_id);
    const PACQPScore = DynamicPACQPScore(pacQpScoreFile.file_id);
    const PACQPStatus = DynamicPACQPStatus(pacQpStatusFile.file_id);

    const providers = yield PACProvider.find({ tin: { $in: nonCachedTins }, duplicate: false }).lean();
    const providerNPIs = providers.map(provider => provider.npi).reduce((npis, npi) => {
      if (npi && !npis.some(n => n === npi)) {
        npis.push(npi);
      }

      return npis;
    }, []);

    const entityIds = providers
      .map(provider => provider.entity_id)
      .filter(id => !!id)
      .reduce((entities, entityId) => {
        if (entityId && !entities.some(e => e === entityId)) {
          entities.push(entityId);
        }

        return entities;
      }, []);
    const entities = entityIds.length > 0 ? yield PACEntity.find({ id: { $in: entityIds } }).lean() : [];
    const entityMap = entities.reduce((current, entity) => Object.assign({}, current, { [entity.id]: entity }), {});

    const modelIds = entities
      .map(provider => provider.apm_id)
      .filter(id => !!id)
      .reduce((models, modelId) => {
        if (modelId && !models.some(e => e === modelId)) {
          models.push(modelId);
        }

        return models;
      }, []);
    const models = modelIds.length > 0 ? yield PACModel.find({ id: { $in: modelIds } }).lean() : [];
    const modelMap = models.reduce((current, model) => Object.assign({}, current, { [model.id]: model }), {});

    const subdivisionQuery = entities
      .map(entity => ({ id: entity.subdiv_id, apm_id: entity.apm_id }))
      .reduce((query, item) => {
        if (!query.some(q => q.id === item.id && q.apm_id === item.apm_id)) {
          query.push(item);
        }

        return query;
      }, []);

    const subdivisions =
      subdivisionQuery.length > 0
        ? yield PACSubdivision.find({
          $or: subdivisionQuery
        }).lean()
        : [];
    const subdivisionMap = subdivisions.reduce((map, subdivision) => {
      if (!map[subdivision.id]) {
        map[subdivision.id] = {};
      }

      if (!map[subdivision.id][subdivision.apm_id]) {
        map[subdivision.id][subdivision.apm_id] = subdivision;
      }

      return map;
    }, {});

    const lvts = entityIds.length > 0 ? yield PACLvt.find({ entity_id: { $in: entityIds } }).lean() : [];
    const lvtsMapByEntity = lvts.reduce((current, lvt) => Object.assign({}, current, { [lvt.entity_id]: lvt }), {});

    const qpStatuses = providerNPIs.length > 0 ? yield PACQPStatus.find({ npi: { $in: providerNPIs } }).lean() : [];
    const qpStatusMap = qpStatuses.reduce(
      (current, qpStatus) => Object.assign({}, current, { [qpStatus.npi]: qpStatus }),
      {}
    );

    const qpScoresQuery = providers
      .map(provider => ({
        npi: provider.npi,
        tin: provider.tin,
        entity_id: provider.entity_id
      }))
      .reduce((query, item) => {
        if (!query.some(q => q.npi === item.npi && q.tin === item.tin && q.entity_id === item.entity_id)) {
          query.push(item);
        }

        return query;
      }, []);
    const qpScores =
      qpScoresQuery.length > 0
        ? yield PACQPScore.find({
          $or: qpScoresQuery
        }).lean()
        : [];
    const qpScoreMap = qpScores.reduce((current, score) => {
      current[`${score.npi}|${score.tin}|${score.entity_id}`] = score;
      return current;
    }, {});

    const mapped = tins.map((tin) => {
      const providersWithTIN = providers.filter(provider => tin === provider.tin);

      return {
        tin,
        entities: providersWithTIN
          .map(provider => entityMap[provider.entity_id])
          .reduce((current, entity) => {
            if (entity && !current.some(e => e.id === entity.id)) {
              current.push(entity);
            }

            return current;
          }, [])
          .map((entity) => {
            let lvtFlag = (lvtsMapByEntity[entity.id] || {}).status;
            if (lvtFlag === 'A') {
              lvtFlag = 'F';
            } else if (lvtFlag === 'B') {
              lvtFlag = 'T';
            } else if (lvtFlag === 'Y') {
              lvtFlag = 'T';
            } else if (lvtFlag === 'N') {
              lvtFlag = 'F';
            }

            const model = modelMap[entity.apm_id] || {};
            const subdivision = (subdivisionMap[entity.subdiv_id] || {})[entity.apm_id] || {};

            let advancedAPMFlag = model.advanced_apm_flag;
            if (advancedAPMFlag === 'P') {
              advancedAPMFlag = subdivision.advanced_apm_flag;
            }

            let mipsAPMFlag = model.mips_apm_flag;
            if (mipsAPMFlag === 'P') {
              mipsAPMFlag = subdivision.mips_apm_flag;
            }

            return {
              entity_id: entity.id,
              entity_name: entity.name,
              lvt_flag: lvtFlag || null,
              lvt_payments: (lvtsMapByEntity[entity.id] || {}).payments || null,
              lvt_patients: (lvtsMapByEntity[entity.id] || {}).patients || null,
              lvt_small_status: (lvtsMapByEntity[entity.id] || {}).small_status || null,
              lvt_performance_year: (lvtsMapByEntity[entity.id] || {}).performance_year || null,
              entity_tin: entity.tin,
              npis: providersWithTIN.filter(provider => provider.entity_id === entity.id).map((provider) => {
                const qpscore = qpScoreMap[`${provider.npi}|${provider.tin}|${provider.entity_id}`];

                return {
                  npi: provider.npi || null,
                  qp_status: (qpStatusMap[provider.npi] || {}).qp_status || null,
                  qp_payment_threshold_score: (qpscore || {}).payment_threshold_score || null,
                  qp_patient_threshold_score: (qpscore || {}).patient_threshold_score || null,
                  provider_relationship_code: provider.provider_relationship_code || null
                };
              }),
              apm_id: model.id || null,
              apm_name: model.name || null,
              subdivision_id: subdivision.id || null,
              subdivision_name: subdivision.name || null,
              advanced_apm_flag: advancedAPMFlag || null,
              mips_apm_flag: mipsAPMFlag || null
            };
          })
      };
    });

    if (config.CACHE.ENABLED) {
      yield Promise.all(
        mapped.map(
          Promise.coroutine(function* getFromDataWorker(tinData) {
            cachedTINData.push(tinData);
            yield qppRedis.setByTin(tinData.tin, publishHistory.id, performanceYear, tinData);
          })
        )
      );
    } else {
      cachedTINData.push(...mapped);
    }
  }

  const result = cachedTINData;

  parsedTins.badTins.forEach((badTin) => {
    result.push(
      {
        tin: badTin,
        entities: []
      }
    );
  });
  return result;
});
