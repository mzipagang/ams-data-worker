const _ = require('lodash');
const QppProvider = require('../mongoose/models/qpp_provider');
const { getHighestValue } = require('../../schemas/Snapshot');
const getLvtFlag = require('./getLvtFlag');
const getAdvancedApmFlag = require('./getAdvancedApmFlag');
const getMipsApmFlag = require('./getMipsApmFlag');

const getEntities = async (performanceYear, entityIds) => {
  const query = {
    performance_year: performanceYear,
    entity: { $exists: true }
  };

  if (entityIds) {
    query.entity_id = { $in: entityIds };
  }

  const entities = await QppProvider.aggregate([
    { $match: query },
    {
      $group: {
        _id: {
          entity_id: '$entity_id',
          tin: '$tin'
        },
        entity: { $first: '$entity' },
        LVT: { $first: '$LVT' },
        model: { $first: '$model' },
        qpCategoryScoreSummary: { $first: '$qpCategoryScoreSummary' },
        qpcScore: { $first: '$qpcScore' },
        qpScore: { $first: '$qpScore' },
        snapshot: { $first: '$snapshot' },
        subdivision: { $first: '$subdivision' },
        npis: {
          $push: {
            npi: '$npi',
            qp_status: '$qpStatus.qp_status',
            provider_relationship_code: '$provider_relationship_code',
            qp_payment_threshold_score: '$qpScore.payment_threshold_score',
            qp_patient_threshold_score: '$qpScore.patient_threshold_score',
            score_type: '$qpScore.score_type',
            mips_ec_indicator: '$mips_ec_indicator'
          }
        }
      }
    },
    {
      $group: {
        _id: {
          entity_id: '$_id.entity_id'
        },
        entity: { $first: '$entity' },
        LVT: { $first: '$LVT' },
        model: { $first: '$model' },
        qpCategoryScoreSummary: { $first: '$qpCategoryScoreSummary' },
        qpcScore: { $first: '$qpcScore' },
        qpScore: { $first: '$qpScore' },
        snapshot: { $first: '$snapshot' },
        subdivision: { $first: '$subdivision' },
        tins: {
          $push: {
            tin: '$_id.tin',
            npis: '$npis'
          }
        }
      }
    },
    {
      $project: {
        'entity.apm_id': true,
        'entity.id': true,
        'entity.name': true,
        'entity.tin': true,
        'LVT.complex_patient_score': true,
        'LVT.patients': true,
        'LVT.payments': true,
        'LVT.performance_year': true,
        'LVT.small_status': true,
        'LVT.status': true,
        'model.advanced_apm_flag': true,
        'model.id': true,
        'model.mips_apm_flag': true,
        'model.name': true,
        'qpCategoryScoreSummary.final_qpc_score': true,
        'qpcScore.final_qpc_score': true,
        snapshot: true,
        'subdivision.id': true,
        'subdivision.name': true,
        'subdivision.advanced_apm_flag': true,
        'subdivision.mips_apm_flag': true,
        'tins.tin': true,
        'tins.npis': true
      }
    }
  ]).allowDiskUse(true);

  return {
    data: {
      performance_year: performanceYear,
      snapshot: getHighestValue(_.uniq(entities.map(entity => entity.snapshot))),
      apm_data: entities.map((provider) => {
        const { _id, entity, LVT, model, subdivision, qpCategoryScoreSummary, tins } = provider;
        return {
          entity_tin: entity && entity.tin,
          entity_id: _id.entity_id,
          entity_name: entity && entity.name,
          lvt_flag: LVT && getLvtFlag(LVT.status),
          lvt_payments: LVT && LVT.payments,
          lvt_patients: LVT && LVT.patients,
          lvt_small_status: LVT && LVT.small_status,
          lvt_performance_year: LVT && LVT.performance_year,
          apm_id: entity && entity.apm_id,
          apm_name: model && model.name,
          subdivision_id: subdivision && subdivision.id,
          subdivision_name: subdivision && subdivision.name,
          advanced_apm_flag: getAdvancedApmFlag(provider),
          mips_apm_flag: getMipsApmFlag(provider),
          complex_patient_score: LVT && LVT.complex_patient_score,
          final_qpc_score: qpCategoryScoreSummary && qpCategoryScoreSummary.final_qpc_score,
          tins
        };
      })
    }
  };
};

module.exports = getEntities;
