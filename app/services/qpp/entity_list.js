const Promise = require('bluebird');
const config = require('../../config');
const DynamicPACModel = require('../mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../mongoose/dynamic_name_models/pac_entity');
const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');
const qppRedis = require('../qppRedis');

module.exports = Promise.coroutine(function* handle(performanceYear = 2017, publishHistoryId = null) {
  if (!performanceYear) {
    performanceYear = 2017;
  }

  const {
    publishHistory,
    pacModelFile,
    pacSubdivisionFile,
    pacEntityFile,
    allPACDataExists
  } = yield pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear, publishHistoryId);

  // return empty array if one collection is missing
  if (!allPACDataExists) {
    return [];
  }

  let data;
  if (config.CACHE.ENABLED) {
    data = yield qppRedis.getEntityList(publishHistory.id, performanceYear);
  }

  if (!data) {
    const PACModel = DynamicPACModel(pacModelFile.file_id);
    const PACSubdivision = DynamicPACSubdivision(pacSubdivisionFile.file_id);
    const PACEntity = DynamicPACEntity(pacEntityFile.file_id);

    data = yield PACEntity.aggregate([
      {
        $lookup: {
          from: PACModel.collection.name,
          localField: 'apm_id',
          foreignField: 'id',
          as: 'model_data'
        }
      },
      {
        $lookup: {
          from: PACSubdivision.collection.name,
          localField: 'subdiv_id',
          foreignField: 'id',
          as: 'subdivision_data'
        }
      },
      {
        $project: {
          name: true,
          id: true,
          model_data: { $arrayElemAt: ['$model_data', 0] },
          subdivision_data: {
            $arrayElemAt: [
              {
                $filter: {
                  input: '$subdivision_data',
                  as: 'subdivisiondata',
                  cond: { $eq: ['$$subdivisiondata.apm_id', '$apm_id'] }
                }
              },
              0
            ]
          }
        }
      },
      {
        $match: {
          $or: [
            {
              'model_data.mips_apm_flag': 'Y'
            },
            {
              'model_data.mips_apm_flag': 'P',
              'subdivision_data.mips_apm_flag': 'Y'
            }
          ]
        }
      },
      {
        $project: {
          _id: false,
          name: true,
          id: true
        }
      }
    ]);

    if (config.CACHE.ENABLED) {
      yield qppRedis.setEntityList(publishHistory.id, performanceYear, data);
    }
  }

  return data;
});
