const DynamicPACProvider = require('../mongoose/dynamic_name_models/pac_provider');
const DynamicPACQPStatus = require('../mongoose/dynamic_name_models/pac_qp_status');
const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');

module.exports = async function handle(performanceYear = 2017) {
  if (!performanceYear) {
    performanceYear = 2017;
  }

  const {
    pacProviderFile,
    pacQpStatusFile,
    allPACDataExists,
    snapshot
  } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);

  // return empty array if one collection is missing
  if (!allPACDataExists) {
    return {
      snapshot: '',
      performance_year: performanceYear,
      npis: []
    };
  }

  const PACProvider = DynamicPACProvider(pacProviderFile.file_id);
  const PACQPStatus = DynamicPACQPStatus(pacQpStatusFile.file_id);

  const data = await PACQPStatus.aggregate([
    {
      $lookup: {
        from: PACProvider.collection.name,
        localField: 'npi',
        foreignField: 'npi',
        as: 'provider_data'
      }
    },
    {
      $project: {
        npi: true,
        qp_status: true,
        providers: { $size: '$provider_data' }
      }
    },
    {
      $match: {
        providers: { $gt: 0 }
      }
    },
    {
      $project: {
        _id: false,
        npi: true,
        qp_status: true
      }
    }
  ]).allowDiskUse(true);

  return {
    snapshot,
    performance_year: performanceYear,
    npis: data
  };
};
