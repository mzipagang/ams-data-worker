module.exports = (status) => {
  if (['A', 'N'].indexOf(status) !== -1) {
    return 'F';
  }
  if (['B', 'Y'].indexOf(status) !== -1) {
    return 'T';
  }
  return status;
};
