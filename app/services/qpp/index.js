const QPPApiByNPI = require('./npi');
const QPPApiByTIN = require('./tin');
const QPPApiByEntity = require('./entity');
const QPPApiEntitylist = require('./entity_list');
const QPPApiQPStatusList = require('./qp_status_list');
const GenericQPPAPI = require('./generic');
const getEntities = require('./getEntities');
const getLvtFlag = require('./getLvtFlag');
const getAdvancedApmFlag = require('./getAdvancedApmFlag');
const getMipsApmFlag = require('./getMipsApmFlag');

module.exports = {
  getEntities,
  getLvtFlag,
  getAdvancedApmFlag,
  getMipsApmFlag,
  QPPApiByNPI,
  QPPApiByTIN,
  QPPApiByEntity,
  QPPApiEntitylist,
  QPPApiQPStatusList,
  GenericQPPAPI
};
