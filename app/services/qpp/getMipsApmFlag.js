module.exports = (provider) => {
  const flag = provider.model.mips_apm_flag;
  if (flag === 'P' && provider.subdivision) {
    return provider.subdivision.mips_apm_flag;
  }
  return flag;
};
