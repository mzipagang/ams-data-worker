const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');
const uuid = require('uuid');
const qppProvider = require('../mongoose/models/qpp_provider.js');
const mongoose = require('../mongoose');
const logger = require('../logger');
const batch = require('through-batch');
const { Writable } = require('stream');


module.exports = {
  updateCollection: async (performanceYear) => {
    const snapshot = await pacPublishHistoryUtils.getLatestPacCollectionsByPerformanceYear(performanceYear);
    logger.info('Snapshot being used:',
      {
        PACModelCollectionName: snapshot.PACModelCollectionName,
        PACSubdivisionCollectionName: snapshot.PACSubdivisionCollectionName,
        PACEntityCollectionName: snapshot.PACEntityCollectionName,
        PACProviderCollectionName: snapshot.PACProviderCollectionName,
        PACLvtCollectionName: snapshot.PACLvtCollectionName,
        PACQPScoreCollectionName: snapshot.PACQPScoreCollectionName,
        PACQPStatusCollectionName: snapshot.PACQPStatusCollectionName,
        PACQPCategoryScoreCollectionName: snapshot.PACQPCategoryScoreCollectionName,
        PACQPCategoryScoreSummaryCollectionName: snapshot.PACQPCategoryScoreSummaryCollectionName
      });
    const tmpCollectionName = `qpp_provider_tmp_${uuid.v4()}`;
    logger.info(`Starting update of qpp_provider, temp collection name: ${tmpCollectionName}`);

    await snapshot.PACProvider.aggregate([
      {
        $lookup: {
          from: snapshot.PACQPStatusCollectionName,
          localField: 'npi',
          foreignField: 'npi',
          as: 'qpStatus'
        }
      },
      {
        $unwind: {
          path: '$qpStatus',
          preserveNullAndEmptyArrays: true
        }
      },

      {
        $lookup: {
          from: snapshot.PACEntityCollectionName,
          let: {
            entity_id: '$entity_id'
          },
          pipeline: [{
            $match: {
              $expr: {
                $and: [
                  {
                    $eq: ['$id', '$$entity_id']
                  }
                ]
              }
            }
          }],
          as: 'entity'
        }
      },
      {
        $unwind: {
          path: '$entity',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: snapshot.PACQPCategoryScoreCollectionName,
          localField: 'entity.id',
          foreignField: 'entity_id',
          as: 'qpCategoryScore'
        }
      },
      {
        $lookup: {
          from: snapshot.PACQPCategoryScoreSummaryCollectionName,
          localField: 'entity.id',
          foreignField: 'entity_id',
          as: 'qpCategoryScoreSummary'
        }
      },
      {
        $unwind: {
          path: '$qpCategoryScoreSummary',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: snapshot.PACLvtCollectionName,
          localField: 'entity_id',
          foreignField: 'entity_id',
          as: 'LVT'
        }
      },

      {
        $unwind: {
          path: '$LVT',
          preserveNullAndEmptyArrays: true
        }
      },


      {
        $lookup: {
          from: snapshot.PACModelCollectionName,
          localField: 'entity.apm_id',
          foreignField: 'id',
          as: 'model'
        }
      },

      {
        $unwind: {
          path: '$model',
          preserveNullAndEmptyArrays: true
        }
      },

      {
        $lookup: {
          from: snapshot.PACQPScoreCollectionName,
          let: {
            npi: '$npi',
            tin: '$tin',
            entity_id: '$entity_id'
          },
          pipeline: [{
            $match: {
              $expr: {
                $and: [{
                  $eq: ['$npi', '$$npi']
                },
                {
                  $eq: ['$tin', '$$tin']
                },
                {
                  $eq: ['$entity_id', '$$entity_id']
                }
                ]
              }
            }
          }],
          as: 'qpScore'
        }
      },
      {
        $unwind: {
          path: '$qpScore',
          preserveNullAndEmptyArrays: true
        }
      },

      {
        $lookup: {
          from: snapshot.PACSubdivisionCollectionName,
          let: {
            apm_id: '$entity.apm_id',
            subdiv_id: '$entity.subdiv_id'
          },
          pipeline: [{
            $match: {
              $expr: {
                $and: [{
                  $eq: ['$apm_id', '$$apm_id']
                },
                {
                  $eq: ['$id', '$$subdiv_id']
                }
                ]
              }
            }
          }],
          as: 'subdivision'
        }
      },
      {
        $unwind: {
          path: '$subdivision',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $out: tmpCollectionName
      }
    ]);

    logger.info(`In qpp_providers deleting performance year ${performanceYear}`);
    await mongoose.connection.db.collection(qppProvider.collection.name).deleteMany(
      { performance_year: performanceYear });

    logger.info(`In qpp_providers starting insert of performance year ${performanceYear}`);

    const updatePromise = new Promise((resolve, reject) => {
      mongoose.connection.db.collection(tmpCollectionName).find().stream()
        .pipe(batch(10000))
        .pipe(
          new Writable({
            objectMode: true,
            write: async (docs, _, done) => {
              await mongoose.connection.db.collection(qppProvider.collection.name).insertMany(docs);
              done();
            }
          })
        )
        .on('error', (error) => { reject(error); })
        .on('finish', () => { resolve(); });
    });

    await updatePromise;

    logger.info(`In qpp_providers dropping temp collection ${tmpCollectionName}`);
    await mongoose.connection.db.collection(tmpCollectionName).drop();
  }
};
