const _ = require('lodash');
const DynamicPACModel = require('../mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../mongoose/dynamic_name_models/pac_entity');
const DynamicPACProvider = require('../mongoose/dynamic_name_models/pac_provider');
const DynamicPACLvt = require('../mongoose/dynamic_name_models/pac_lvt');
const DynamicPACQPScore = require('../mongoose/dynamic_name_models/pac_qp_score');
const DynamicPACQPStatus = require('../mongoose/dynamic_name_models/pac_qp_status');
const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');

const SEARCH_LIMIT = 1000;

function buildApmAndSubdivMongoMatchObject(queryParameters = {}) {
  const apmMatch = {};

  if (queryParameters && queryParameters.modelIds) {
    apmMatch['entity_data.apm_id'] = { $in: queryParameters.modelIds };
  }

  if (queryParameters && queryParameters.subdivisionIds) {
    apmMatch['entity_data.subdiv_id'] = { $in: queryParameters.subdivisionIds };
  }

  return apmMatch;
}

function buildAdvancedSearchMongoMatchObject(queryParameters = {}) {
  const match = {};
  if (queryParameters.modelIds) {
    match['model_data.id'] = { $in: queryParameters.modelIds };
  }

  if (queryParameters.modelNames) {
    match['model_data.name'] = { $in: queryParameters.modelNames };
  }

  if (queryParameters.subdivisionIds) {
    match['subdivision_data.id'] = { $in: queryParameters.subdivisionIds };
  }

  if (queryParameters.subdivisionNames) {
    match['subdivision_data.name'] = { $in: queryParameters.subdivisionNames };
  }

  if (queryParameters.qpStatuses) {
    match['qp_status_data.qp_status'] = { $in: queryParameters.qpStatuses };
  }

  if (queryParameters.lvtFlags) {
    match['lvt_data.status'] = { $in: queryParameters.lvtFlags };
  }

  if (queryParameters.smallStatusFlags) {
    match['lvt_data.small_status'] = { $in: queryParameters.smallStatusFlags };
  }

  if (queryParameters.snapshots) {
    match.run_snapshot = { $in: queryParameters.snapshots };
  }

  return match;
}

module.exports = async function handle(
  npisIn,
  tinsIn,
  entityIdsIn,
  performanceYear = 2017,
  publishHistoryId = null,
  queryParameters
) {
  const match = {
    duplicate: false
  };

  const npis = _.uniq(npisIn);
  const tins = _.uniq(tinsIn);
  const entityIds = _.uniq(entityIdsIn);

  if (npis.length > 0) {
    match.npi = { $in: npis };
  }

  if (tins.length > 0) {
    match.tin = { $in: tins };
  }

  if (entityIds.length > 0) {
    match.entity_id = { $in: entityIds };
  }

  const modelAndSubidivionRefinedSearchObject = buildApmAndSubdivMongoMatchObject(queryParameters);
  const advancedSearchMatch = buildAdvancedSearchMongoMatchObject(queryParameters);

  const {
    pacModelFile,
    pacSubdivisionFile,
    pacEntityFile,
    pacProviderFile,
    pacLvtFile,
    pacQpStatusFile,
    pacQpScoreFile,
    allPACDataExists
  } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear, publishHistoryId);

  // return empty array if one collection is missing
  if (!allPACDataExists) {
    return [];
  }

  const PACModel = DynamicPACModel(pacModelFile.file_id);
  const PACSubdivision = DynamicPACSubdivision(pacSubdivisionFile.file_id);
  const PACEntity = DynamicPACEntity(pacEntityFile.file_id);
  const PACProvider = DynamicPACProvider(pacProviderFile.file_id);
  const PACLvt = DynamicPACLvt(pacLvtFile.file_id);
  const PACQPScore = DynamicPACQPScore(pacQpScoreFile.file_id);
  const PACQPStatus = DynamicPACQPStatus(pacQpStatusFile.file_id);

  const data = await PACProvider.aggregate([
    {
      $match: match
    },
    {
      $sort: {
        npi: 1,
        entity_id: 1
      }
    },
    {
      $lookup: {
        from: PACEntity.collection.name,
        localField: 'entity_id',
        foreignField: 'id',
        as: 'entity_data'
      }
    },
    {
      $match: modelAndSubidivionRefinedSearchObject
    },
    {
      $project: {
        _id: false,
        entity_id: true,
        tin: true,
        npi: true,
        provider_relationship_code: true,
        performance_year: true,
        run_snapshot: true,
        entity_data: { $arrayElemAt: ['$entity_data', 0] }
      }
    },
    {
      $lookup: {
        from: PACLvt.collection.name,
        localField: 'entity_data.id',
        foreignField: 'entity_id',
        as: 'lvt_data'
      }
    },
    {
      $lookup: {
        from: PACModel.collection.name,
        localField: 'entity_data.apm_id',
        foreignField: 'id',
        as: 'model_data'
      }
    },
    {
      $lookup: {
        from: PACSubdivision.collection.name,
        localField: 'entity_data.subdiv_id',
        foreignField: 'id',
        as: 'subdivision_data'
      }
    },
    {
      $lookup: {
        from: PACQPScore.collection.name,
        localField: 'npi',
        foreignField: 'npi',
        as: 'qp_score_data'
      }
    },
    {
      $lookup: {
        from: PACQPStatus.collection.name,
        localField: 'npi',
        foreignField: 'npi',
        as: 'qp_status_data'
      }
    },
    {
      $match: advancedSearchMatch
    },
    {
      $limit: SEARCH_LIMIT + 1 // add 1 to the search limit because we don't allow partial results; if there are more than the search limit in results we simply return nothing
    },
    {
      $project: {
        entity_id: true,
        tin: true,
        npi: true,
        run_snapshot: true,
        provider_relationship_code: true,
        entity_data: true,
        performance_year: true,
        model_data: { $arrayElemAt: ['$model_data', 0] },
        lvt_data: { $arrayElemAt: ['$lvt_data', 0] },
        subdivision_data: {
          $arrayElemAt: [
            {
              $filter: {
                input: '$subdivision_data',
                as: 'subdivisiondata',
                cond: { $eq: ['$$subdivisiondata.apm_id', '$entity_data.apm_id'] }
              }
            },
            0
          ]
        },
        qp_score_data: {
          $arrayElemAt: [
            {
              $filter: {
                input: '$qp_score_data',
                as: 'qpscoredata',
                cond: {
                  $and: [
                    { $eq: ['$$qpscoredata.tin', '$tin'] },
                    { $eq: ['$$qpscoredata.entity_id', '$entity_data.id'] }
                  ]
                }
              }
            },
            0
          ]
        },
        qp_status_data: { $arrayElemAt: ['$qp_status_data', 0] }
      }
    },
    {
      $project: {
        tin: true,
        npi: true,
        qp_status: '$qp_status_data.qp_status',
        entity_id: true,
        run_snapshot: true,
        performance_year: true,
        entity_name: '$entity_data.name',
        lvt_flag: '$lvt_data.status',
        lvt_payments: '$lvt_data.payments',
        lvt_patients: '$lvt_data.patients',
        lvt_small_status: '$lvt_data.small_status',
        lvt_performance_year: '$lvt_data.performance_year',
        entity_tin: '$entity_data.tin',
        provider_relationship_code: true,
        qp_payment_threshold_score: '$qp_score_data.payment_threshold_score',
        qp_patient_threshold_score: '$qp_score_data.patient_threshold_score',
        apm_id: '$model_data.id',
        apm_name: '$model_data.name',
        subdivision_id: '$subdivision_data.id',
        subdivision_name: '$subdivision_data.name',
        model_advanced_apm_flag: '$model_data.advanced_apm_flag',
        model_mips_apm_flag: '$model_data.mips_apm_flag',
        subdivision_advanced_apm_flag: '$subdivision_data.advanced_apm_flag',
        subdivision_mips_apm_flag: '$subdivision_data.mips_apm_flag'
      }
    }
  ]);

  if (data.length > SEARCH_LIMIT) {
    throw new Error('Search Limit Exceeded');
  }

  const mapped = data.map((d) => {
    let lvtFlag = d.lvt_flag;
    if (lvtFlag === 'A') {
      lvtFlag = 'F';
    } else if (lvtFlag === 'B') {
      lvtFlag = 'T';
    } else if (lvtFlag === 'Y') {
      lvtFlag = 'T';
    } else if (lvtFlag === 'N') {
      lvtFlag = 'F';
    }

    let advancedAPMFlag = d.model_advanced_apm_flag;
    if (advancedAPMFlag === 'P') {
      advancedAPMFlag = d.subdivision_advanced_apm_flag;
    }

    let mipsAPMFlag = d.model_mips_apm_flag;
    if (mipsAPMFlag === 'P') {
      mipsAPMFlag = d.subdivision_mips_apm_flag;
    }

    return {
      tin: d.tin || null,
      npi: d.npi || null,
      qp_status: d.qp_status || null,
      entity_id: d.entity_id || null,
      entity_name: d.entity_name || null,
      lvt_flag: lvtFlag || null,
      lvt_payments: d.lvt_payments || null,
      lvt_patients: d.lvt_patients || null,
      lvt_small_status: d.lvt_small_status || null,
      lvt_performance_year: d.lvt_performance_year || null,
      entity_tin: d.entity_tin || null,
      provider_relationship_code: d.provider_relationship_code || null,
      qp_payment_threshold_score: d.qp_payment_threshold_score || null,
      qp_patient_threshold_score: d.qp_patient_threshold_score || null,
      apm_id: d.apm_id || null,
      apm_name: d.apm_name || null,
      subdivision_id: d.subdivision_id || null,
      subdivision_name: d.subdivision_name || null,
      advanced_apm_flag: advancedAPMFlag || null,
      mips_apm_flag: mipsAPMFlag || null,
      performance_year: d.performance_year || null,
      snapshot: d.run_snapshot
    };
  });

  return mapped;
};
