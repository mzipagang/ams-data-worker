const Promise = require('bluebird');
const _ = require('lodash');
const config = require('../../config');
const DynamicPACModel = require('../mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../mongoose/dynamic_name_models/pac_entity');
const DynamicPACProvider = require('../mongoose/dynamic_name_models/pac_provider');
const DynamicPACLvt = require('../mongoose/dynamic_name_models/pac_lvt');
const DynamicPACQPScore = require('../mongoose/dynamic_name_models/pac_qp_score');
const DynamicPACQPStatus = require('../mongoose/dynamic_name_models/pac_qp_status');
const pacPublishHistoryUtils = require('../pacPublishHistoryUtils');
const qppRedis = require('../qppRedis');
const logger = require('../logger');

module.exports = Promise.coroutine(function* handle(entityIdsIn, performanceYear = 2017, publishHistoryId = null) {
  if (!performanceYear) {
    performanceYear = 2017;
  }

  const entityIds = _.uniq(entityIdsIn);

  const {
    publishHistory,
    pacModelFile,
    pacSubdivisionFile,
    pacEntityFile,
    pacProviderFile,
    pacLvtFile,
    pacQpStatusFile,
    pacQpScoreFile,
    allPACDataExists
  } = yield pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear, publishHistoryId);

  // return empty array if one collection is missing
  if (!allPACDataExists) {
    return [];
  }

  const cachedEntityData = [];
  const nonCachedEntityIds = [];

  if (config.CACHE.ENABLED) {
    yield Promise.all(
      entityIds.map(
        Promise.coroutine(function* getCache(entityId) {
          const cache = yield qppRedis.getByEntityId(entityId, publishHistory.id, performanceYear);
          if (cache) {
            cachedEntityData.push(cache);
          } else {
            nonCachedEntityIds.push(entityId);
          }
        })
      )
    );
  } else {
    nonCachedEntityIds.push(...entityIds);
  }

  const PACModel = DynamicPACModel(pacModelFile.file_id);
  const PACSubdivision = DynamicPACSubdivision(pacSubdivisionFile.file_id);
  const PACEntity = DynamicPACEntity(pacEntityFile.file_id);
  const PACProvider = DynamicPACProvider(pacProviderFile.file_id);
  const PACLvt = DynamicPACLvt(pacLvtFile.file_id);
  const PACQPScore = DynamicPACQPScore(pacQpScoreFile.file_id);
  const PACQPStatus = DynamicPACQPStatus(pacQpStatusFile.file_id);

  if (nonCachedEntityIds.length > 0) {
    logger.debug('Entity cache miss');

    const data = yield PACEntity.aggregate([
      {
        $match: {
          id: { $in: nonCachedEntityIds }
        }
      },
      {
        $lookup: {
          from: PACLvt.collection.name,
          localField: 'id',
          foreignField: 'entity_id',
          as: 'lvt_data'
        }
      },
      {
        $lookup: {
          from: PACModel.collection.name,
          localField: 'apm_id',
          foreignField: 'id',
          as: 'model_data'
        }
      },
      {
        $lookup: {
          from: PACSubdivision.collection.name,
          localField: 'subdiv_id',
          foreignField: 'id',
          as: 'subdivision_data'
        }
      },
      {
        $project: {
          id: true,
          tin: true,
          name: true,
          model_data: { $arrayElemAt: ['$model_data', 0] },
          subdivision_data: {
            $arrayElemAt: [
              {
                $filter: {
                  input: '$subdivision_data',
                  as: 'subdivisiondata',
                  cond: { $eq: ['$$subdivisiondata.apm_id', '$apm_id'] }
                }
              },
              0
            ]
          },
          lvt_data: { $arrayElemAt: ['$lvt_data', 0] }
        }
      },
      {
        $project: {
          _id: false,
          entity_id: '$id',
          entity_name: '$name',
          lvt_flag: '$lvt_data.status',
          lvt_payments: '$lvt_data.payments',
          lvt_patients: '$lvt_data.patients',
          lvt_small_status: '$lvt_data.small_status',
          lvt_performance_year: '$lvt_data.performance_year',
          entity_tin: '$tin',
          apm_id: '$model_data.id',
          apm_name: '$model_data.name',
          subdivision_id: '$subdivision_data.id',
          subdivision_name: '$subdivision_data.name',
          model_advanced_apm_flag: '$model_data.advanced_apm_flag',
          model_mips_apm_flag: '$model_data.mips_apm_flag',
          subdivision_advanced_apm_flag: '$subdivision_data.advanced_apm_flag',
          subdivision_mips_apm_flag: '$subdivision_data.mips_apm_flag'
        }
      }
    ]);

    const providerData = yield PACProvider.aggregate([
      {
        $match: {
          entity_id: { $in: entityIds },
          duplicate: false
        }
      },
      {
        $lookup: {
          from: PACQPStatus.collection.name,
          localField: 'npi',
          foreignField: 'npi',
          as: 'qp_status_data'
        }
      },
      {
        $lookup: {
          from: PACQPScore.collection.name,
          localField: 'npi',
          foreignField: 'npi',
          as: 'qp_score_data'
        }
      },
      {
        $project: {
          entity_id: true,
          tin: true,
          npi: true,
          provider_relationship_code: true,
          qp_status_data: { $arrayElemAt: ['$qp_status_data', 0] },
          qp_score_data: {
            $arrayElemAt: [
              {
                $filter: {
                  input: '$qp_score_data',
                  as: 'qpscoredata',
                  cond: {
                    $and: [{ $eq: ['$$qpscoredata.tin', '$tin'] }, { $eq: ['$$qpscoredata.entity_id', '$entity_id'] }]
                  }
                }
              },
              0
            ]
          }
        }
      },
      {
        $project: {
          entity_id: true,
          tin: true,
          npi: true,
          qp_status: '$qp_status_data.qp_status',
          qp_payment_threshold_score: '$qp_score_data.payment_threshold_score',
          qp_patient_threshold_score: '$qp_score_data.patient_threshold_score',
          provider_relationship_code: true
        }
      },
      {
        $group: {
          _id: {
            entity_id: '$entity_id',
            tin: '$tin'
          },
          data: { $push: '$$ROOT' }
        }
      },
      {
        $project: {
          _id: false,
          entity_id: '$_id.entity_id',
          tin: '$_id.tin',
          'data.npi': true,
          'data.provider_relationship_code': true,
          'data.qp_status': true,
          'data.qp_payment_threshold_score': true,
          'data.qp_patient_threshold_score': true
        }
      }
    ]);

    const mapped = data.map((entityData) => {
      const finalEntityData = Object.assign({}, entityData);

      const lvtFlag = finalEntityData.lvt_flag;
      if (lvtFlag === 'A') {
        finalEntityData.lvt_flag = 'F';
      } else if (lvtFlag === 'B') {
        finalEntityData.lvt_flag = 'T';
      } else if (lvtFlag === 'Y') {
        finalEntityData.lvt_flag = 'T';
      } else if (lvtFlag === 'N') {
        finalEntityData.lvt_flag = 'F';
      }

      finalEntityData.lvt_flag = finalEntityData.lvt_flag || null;
      finalEntityData.lvt_payments = finalEntityData.lvt_payments || null;
      finalEntityData.lvt_patients = finalEntityData.lvt_patients || null;
      finalEntityData.lvt_small_status = finalEntityData.lvt_small_status || null;
      finalEntityData.lvt_performance_year = finalEntityData.lvt_performance_year || null;

      let advancedAPMFlag = finalEntityData.model_advanced_apm_flag;
      if (advancedAPMFlag === 'P') {
        advancedAPMFlag = finalEntityData.subdivision_advanced_apm_flag;
      }
      finalEntityData.advanced_apm_flag = advancedAPMFlag;
      delete finalEntityData.model_advanced_apm_flag;
      delete finalEntityData.subdivision_advanced_apm_flag;

      let mipsAPMFlag = finalEntityData.model_mips_apm_flag;
      if (mipsAPMFlag === 'P') {
        mipsAPMFlag = finalEntityData.subdivision_mips_apm_flag;
      }
      finalEntityData.mips_apm_flag = mipsAPMFlag;
      delete finalEntityData.model_mips_apm_flag;
      delete finalEntityData.subdivision_mips_apm_flag;

      const entityId = entityData.entity_id;

      const providersByTin = providerData.filter(p => p.entity_id === entityId).map(providerTinData =>
        Object.assign(
          {},
          {
            tin: providerTinData.tin,
            npis: providerTinData.data.map(npiData => ({
              npi: npiData.npi || null,
              qp_status: npiData.qp_status || null,
              qp_payment_threshold_score: npiData.qp_payment_threshold_score || null,
              qp_patient_threshold_score: npiData.qp_patient_threshold_score || null,
              provider_relationship_code: npiData.provider_relationship_code || null
            }))
          }
        )
      );

      return Object.assign({}, finalEntityData, {
        tins: providersByTin
      });
    });

    if (config.CACHE.ENABLED) {
      yield Promise.all(
        mapped.map(
          Promise.coroutine(function* getFromDataWorker(entityData) {
            cachedEntityData.push(entityData);
            yield qppRedis.setByEntityId(entityData.entity_id, publishHistory.id, performanceYear, entityData);
          })
        )
      );
    } else {
      cachedEntityData.push(...mapped);
    }
  }

  return cachedEntityData.map(entityData =>
    Object.assign({}, entityData, {
      entity_tin: entityData.entity_tin,
      tins: entityData.tins.map(tinData =>
        Object.assign({}, tinData, {
          tin: tinData.tin
        })
      )
    })
  );
});
