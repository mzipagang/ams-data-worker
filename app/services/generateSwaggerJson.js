const glob = require('glob');
const Path = require('path');
const { requiresAuthentication } = require('../services/utils/jwt');
const { getSwaggerJSON } = require('hapi-swagger/lib/builder');

const extractMethodAndPath = (filePath, rootPath, routeInput) => {
  const url = filePath.substring(rootPath.length);
  const method = url.substring(url.lastIndexOf('/') + 1, url.length - 3).toUpperCase();
  let path = url.substring(0, url.lastIndexOf('/'));
  if (routeInput && routeInput.params) {
    routeInput.params._inner.children.forEach(({ key }) => {
      path = path.replace(/\/(file_)?id/, `/{${key}}`);
    });
  }
  return { method, path };
};

const generateSwaggerJson = async ({ host, port, protocol, title, version, routesRoot, filter }) => {
  const routes = [];

  glob.sync(`${routesRoot}/api/**/*.js`).forEach((file) => {
    if (filter && !filter.test(file)) return;
    let { handler, tags, description, input, output } = require(Path.join(__dirname, `../../${file}`)); // eslint-disable-line prefer-const
    if (!tags) return;
    const { method, path } = extractMethodAndPath(file, routesRoot, input);
    const hasFiles = input && input.payload &&
      input.payload._inner.children.some(child => child.schema._meta.some(m => m.swaggerType === 'file'));

    if (!output) {
      throw new Error(`File ${file} has no output specification`);
    }

    routes.push({
      method,
      path,
      config: {
        tags: ['api', ...tags],
        description,
        handler,
        validate: input,
        response: !output.schema ? undefined : {
          schema: output.schema,
          status: {}
        },
        payload: !hasFiles ? undefined : {
          maxBytes: 50 * 1024 * 1024,
          parse: true,
          output: 'stream'
        },
        plugins: {
          'hapi-swagger': {
            payloadType: !hasFiles ? undefined : 'form',
            security: requiresAuthentication(method, path) ? [{ Bearer: [] }] : [{ ApiKeyAuth: [] }],
            responses: !output.status ? undefined : Object.keys(output.status).reduce((acc, key) => {
              acc[key] = { description: output.status[key] };
              return acc;
            }, {})
          }
        }
      }
    });
  });

  const isDefaultPort = (protocol === 'http' && port === 80) || (protocol === 'https' && port === 443);
  const swaggerObject = await getSwaggerJSON({
    swagger: '2.0',
    host: `${host}${isDefaultPort ? '' : `:${port}`}`,
    basePath: '/',
    schemes: [protocol],
    debug: false,
    jsonPath: '/.vscode/swagger.json',
    documentationPath: '/api',
    documentationRouteTags: [],
    swaggerUIPath: '/swaggerui/',
    auth: false,
    pathPrefixSize: 1,
    payloadType: 'json',
    documentationPage: false,
    swaggerUI: false,
    jsonEditor: false,
    expanded: 'list',
    lang: 'en',
    sortTags: 'default',
    sortEndpoints: 'path',
    sortPaths: 'unsorted',
    grouping: 'tags',
    tagsGroupingFilter: tag => tag !== 'api',
    xProperties: true,
    reuseDefinitions: true,
    definitionPrefix: 'default',
    deReference: false,
    validatorUrl: '//online.swagger.io/validator',
    acceptToProduce: true,
    pathReplacements: [],
    info: { title, version },
    securityDefinitions: {
      Bearer: {
        type: 'apiKey',
        name: 'Authorization',
        in: 'header'
      }
    },
    log: () => { }
  }, {
    headers: {},
    query: {},
    info: { hostname: host, port, protocol },
    server: {
      info: { host, port, protocol },
      table: () => routes.map(route => Object.assign({}, route, {
        config: undefined,
        settings: route.config
      }))
    }
  });

  return swaggerObject;
};

module.exports = generateSwaggerJson;
