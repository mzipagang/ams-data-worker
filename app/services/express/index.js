const path = require('path');
const app = require('express')();
const server = require('http').Server(app);
const express = require('express');
const router = require('express').Router();
const bodyParser = require('body-parser');
const config = require('../../config');
const logger = require('../logger');

app.set('port', config.EXPRESS.PORT);

if (config.NODE_ENV === 'dev') {
  app.set('json spaces', 4);
}

app.use(logger.accessLogger);

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(express.static(path.join(__dirname, '../../public')));

app.use(router);

app.use((req, res) => {
  res.status(404).json({ message: 'Route not found.' });
});

app.use((err, req, res, _next) => {
  res.status(500).json({ message: 'Something horribly went wrong' });
  logger.error(err);
});

module.exports = {
  app,
  router,
  start: (routes) => {
    if (routes) {
      router.use(routes);
    }

    return new Promise((resolve, reject) => {
      try {
        const s = server.listen(app.get('port'), app.get('interface'), () => {
          resolve();
        });
        s.on('error', (err) => {
          reject(err);
        });
      } catch (err) {
        reject(err);
      }
    });
  }
};
