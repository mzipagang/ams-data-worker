const logger = require('../../logger');
const entity = require('../models/apm_entity');
const provider = require('../models/apm_provider');

module.exports = {
  up: async () => {
    await entity.syncIndexes();
    await provider.syncIndexes();
  },
  down: async () => {
    logger.info('Down is not supported for this migration');
  }
};
