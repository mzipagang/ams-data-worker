const mongoose = require('mongoose');

module.exports = {
  up: async () => {
    try {
      await mongoose.connection.db.dropCollection('beneficiary_mdms');
    } catch (ex) {
      if (ex.code === 26) {
        // ns not found (ns = namespace, i.e. collection not found, which is fine.)
        return;
      }
      throw ex;
    }
  },
  down: async () => {}
};
