const logger = require('../../logger');
const mongoose = require('mongoose');
const moment = require('moment-timezone');
const QuarterlyReportConfig = require('../models/quarterly_report_config');

module.exports = {
  up: async () => {
    try {
      await mongoose.connection.db.dropCollection('quarterly_report_configs');
    } catch (ex) {
      if (ex.code !== 26) {
        // ns not found (ns = namespace, i.e. collection not found, which is fine. Throw ex for anything else)
        throw ex;
      }
    }

    [2018, 2019, 2020, 2021, 2022].forEach(async (year) => {
      const config = new QuarterlyReportConfig();
      config.year = year;
      const format = 'YYYY-MM-DD';
      const tz = 'America/New_York';
      config.quarter1_start = moment.tz(`${year}-04-21`, format, tz).startOf('day').toDate();
      config.quarter1_end = moment.tz(`${year}-05-10`, format, tz).endOf('day').toDate();
      config.quarter2_start = moment.tz(`${year}-07-21`, format, tz).startOf('day').toDate();
      config.quarter2_end = moment.tz(`${year}-08-10`, format, tz).endOf('day').toDate();
      config.quarter3_start = moment.tz(`${year}-10-21`, format, tz).startOf('day').toDate();
      config.quarter3_end = moment.tz(`${year}-11-10`, format, tz).endOf('day').toDate();
      config.quarter4_start = moment.tz(`${year + 1}-01-21`, format, tz).startOf('day').toDate();
      config.quarter4_end = moment.tz(`${year + 1}-02-10`, format, tz).endOf('day').toDate();
      await config.save();
    });
  },
  down: async () => {
    logger.info('Down is not supported for this migration');
  }
};
