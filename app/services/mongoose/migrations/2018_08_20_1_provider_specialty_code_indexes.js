const logger = require('../../logger');
const providerSpecialityCodes = require('../models/provider_specialty_code');

module.exports = {
  up: async () => {
    await providerSpecialityCodes.syncIndexes();
  },
  down: async () => {
    logger.info('Down is not supported for this migration');
  }
};
