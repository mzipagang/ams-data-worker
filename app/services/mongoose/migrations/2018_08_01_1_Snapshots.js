const logger = require('../../logger');
const PacSnapshot = require('../models/pac_snapshot');
const FileImportGroup = require('../models/file_import_group');

module.exports = {
  up: async () => {
    const existingSnapshots = await PacSnapshot.find({}, '-_id file_id').lean();
    const existingFileIds = existingSnapshots.map(snapshot => snapshot.file_id);
    const newSnapshots = [];
    const fileImportGroups = await FileImportGroup.find({}, {
      _id: false,
      'files.status': true,
      'files.import_file_type': true,
      'files.performance_year': true,
      'files.run_snapshot': true,
      'files.run_number': true,
      'files.id': true,
      'files.type': true,
      'files.createdAt': true
    }).lean();

    fileImportGroups.forEach((group) => {
      group.files.forEach((file) => {
        // Ignore invalid records since these fields are required
        if (!file.import_file_type) { return; }
        if (!file.performance_year) { return; }
        if (!file.createdAt) { return; }

        // Businesss logic
        if (file.status === 'removed') { return; }
        if (file.import_file_type.indexOf('pac') !== 0 && file.import_file_type.indexOf('abridged') !== 0) { return; }
        if (file.import_file_type === 'pac-lvt') { return; }

        // Ignore existing
        if (existingFileIds.indexOf(file.id) !== -1) { return; }

        newSnapshots.push(new PacSnapshot({
          performance_year: file.performance_year,
          run_snapshot: file.run_snapshot,
          run_number: file.run_number,
          file_id: file.id,
          type: file.import_file_type,
          createdAt: file.createdAt
        }));
      });
    });

    await Promise.all(newSnapshots.map(snapshot => snapshot.save()));
  },
  down: async () => {
    logger.info('Down is not supported for this migration');
  }
};
