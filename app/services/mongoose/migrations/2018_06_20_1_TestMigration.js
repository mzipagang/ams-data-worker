const logger = require('../../logger');

module.exports = {
  up: async () => {
    logger.info('Migration Up');
  },
  down: async () => { logger.info('Migration Down'); }
};
