const mongoose = require('mongoose');
const logger = require('../../logger');
const { decrypt } = require('../../encrypt-decrypt');

const getTinMapping = async (collectionName) => {
  const tinMapping = await mongoose.connection.db.collection(collectionName)
    .distinct('tin', { tin: { $regex: /^[\da-f]{32}$/ } })
    .map(tin => ({ f: tin, t: decrypt(tin) }));
  return { collectionName, tinMapping };
};

const replaceCollection = async ({ collectionName, tinMapping }) => {
  const collection = mongoose.connection.db.collection(collectionName);
  logger.info(`Decrypting TINs on ${collectionName} (${tinMapping.length} replacements)...`);
  await Promise.all(tinMapping.map(({ f, t }) => collection.updateMany({ tin: f }, { $set: { tin: t } })));
};

module.exports = {
  up: async () => {
    try {
      const collections = await mongoose.connection.db.listCollections({}, { nameOnly: true })
        .toArray()
        .map(collection => collection.name);

      let replacements = await Promise.all(collections.sort().map(name => getTinMapping(name)));
      replacements = replacements.filter(({ tinMapping }) => tinMapping.length);

      for (let i = 0; i < replacements.length; i += 1) {
        await replaceCollection(replacements[i]); // eslint-disable-line no-await-in-loop
      }
    } catch (ex) {
      if (ex.code === 26) {
        // ns not found (ns = namespace, i.e. collection not found, which is fine.)
        return;
      }
      throw ex;
    }
  },
  down: async () => {}
};
