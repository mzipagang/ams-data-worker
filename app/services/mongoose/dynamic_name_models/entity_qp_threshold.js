const mongoose = require('mongoose');
const EntityQpThresholdSchema = require('../models/entity_qp_threshold').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`entity_qp_threshold_${name}`] ||
  (modelCache[`entity_qp_threshold_${name}`] =
    mongoose.model(`entity_qp_threshold_${name}`, EntityQpThresholdSchema));
