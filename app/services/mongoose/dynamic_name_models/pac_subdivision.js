const mongoose = require('mongoose');
const PACSubdivisionSchema = require('../models/pac_subdivision').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_subdivision_${name}`] ||
  (modelCache[`pac_subdivision_${name}`] = mongoose.model(`pac_subdivision_${name}`, PACSubdivisionSchema));
