const mongoose = require('mongoose');
const PACEntitySchema = require('../models/pac_entity').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_entity_${name}`] ||
  (modelCache[`pac_entity_${name}`] = mongoose.model(`pac_entity_${name}`, PACEntitySchema));
