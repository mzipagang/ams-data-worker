const mongoose = require('mongoose');
const IndividualQpStatusSchema = require('../models/individual_qp_status').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`individual_qp_status_${name}`] ||
  (modelCache[`individual_qp_status_${name}`] =
    mongoose.model(`individual_qp_status_${name}`, IndividualQpStatusSchema));

