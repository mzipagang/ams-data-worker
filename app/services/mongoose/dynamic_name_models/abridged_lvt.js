const mongoose = require('mongoose');
const PacLvtSchema = require('../models/pac_lvt').schema;

const modelCache = { };
module.exports = snapshotId => // eslint-disable-line no-return-assign
  modelCache[`abridged_lvt_${snapshotId}`] ||
  (modelCache[`abridged_lvt_${snapshotId}`] = mongoose.model(`abridged_lvt_${snapshotId}`, PacLvtSchema));
