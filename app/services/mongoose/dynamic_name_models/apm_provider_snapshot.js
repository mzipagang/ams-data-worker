const mongoose = require('mongoose');
const APMProviderSchema = require('../models/apm_provider').schema;

const modelCache = { };

module.exports = id => // eslint-disable-line no-return-assign
  modelCache[`apm_provider_snapshot_${id}`] ||
  (modelCache[`apm_provider_snapshot_${id}`] = mongoose.model(`apm_provider_snapshot_${id}`, APMProviderSchema));
