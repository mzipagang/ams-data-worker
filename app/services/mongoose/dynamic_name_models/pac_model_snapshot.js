const mongoose = require('mongoose');
const PACModelSchema = require('../models/pac_model').schema;

const modelCache = { };

module.exports = id => // eslint-disable-line no-return-assign
  modelCache[`pac_model_snapshot_${id}`] ||
  (modelCache[`pac_model_snapshot_${id}`] = mongoose.model(`pac_model_snapshot_${id}`, PACModelSchema));
