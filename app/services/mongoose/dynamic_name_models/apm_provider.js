const mongoose = require('mongoose');
const APMProviderSchema = require('../models/apm_provider').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`apm_provider_${name}`] ||
  (modelCache[`apm_provider_${name}`] = mongoose.model(`apm_provider_${name}`, APMProviderSchema));
