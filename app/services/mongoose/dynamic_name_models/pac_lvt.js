const mongoose = require('mongoose');
const PACLvtSchema = require('../models/pac_lvt').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_lvt_${name}`] ||
  (modelCache[`pac_lvt_${name}`] = mongoose.model(`pac_lvt_${name}`, PACLvtSchema));
