const mongoose = require('mongoose');
const PacQpcScoreSummary = require('../models/pac_qp_category_score_summary').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_qp_category_score_summary_${name}`] ||
  (modelCache[`pac_qp_category_score_summary_${name}`] = mongoose.model(`pac_qp_category_score_summary_${name}`, PacQpcScoreSummary));
