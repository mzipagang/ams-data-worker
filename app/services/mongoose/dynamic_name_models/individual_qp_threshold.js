const mongoose = require('mongoose');
const IndividualQpThresholdSchema = require('../models/individual_qp_threshold').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`individual_qp_threshold_${name}`] ||
  (modelCache[`individual_qp_threshold_${name}`] =
    mongoose.model(`individual_qp_threshold_${name}`, IndividualQpThresholdSchema));

