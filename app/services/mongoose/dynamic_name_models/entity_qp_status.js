const mongoose = require('mongoose');
const EntityQpStatusSchema = require('../models/entity_qp_status').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`entity_qp_status_${name}`] ||
  (modelCache[`entity_qp_status_${name}`] =
    mongoose.model(`entity_qp_status_${name}`, EntityQpStatusSchema));

