const mongoose = require('mongoose');
const PACModelSchema = require('../models/pac_model').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_model_${name}`] ||
  (modelCache[`pac_model_${name}`] = mongoose.model(`pac_model_${name}`, PACModelSchema));
