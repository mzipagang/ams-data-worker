const mongoose = require('mongoose');
const APMEntitySchema = require('../models/apm_entity').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`apm_entity_${name}`] ||
  (modelCache[`apm_entity_${name}`] = mongoose.model(`apm_entity_${name}`, APMEntitySchema));
