const mongoose = require('mongoose');
const BeneficiaryMdmSchema = require('../models/beneficiary').schema;

const modelCache = { };
module.exports = snapshotId => // eslint-disable-line no-return-assign
  modelCache[`beneficiary_${snapshotId}`] ||
  (modelCache[`beneficiary__${snapshotId}`] = mongoose.model(`beneficiary_${snapshotId}`, BeneficiaryMdmSchema));
