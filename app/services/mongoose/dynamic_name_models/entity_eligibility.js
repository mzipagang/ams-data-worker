const mongoose = require('mongoose');
const EntityEligibility = require('../models/entity_eligibility').schema;

const modelCache = {};

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`entity_eligibility_${name}`] ||
    (modelCache[`entity_eligibility_${name}`] =
        mongoose.model(`entity_eligibility_${name}`, EntityEligibility));
