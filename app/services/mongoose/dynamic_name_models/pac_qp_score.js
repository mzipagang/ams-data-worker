const mongoose = require('mongoose');
const PACQpScoreSchema = require('../models/pac_qp_score').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_qp_score_${name}`] ||
  (modelCache[`pac_qp_score_${name}`] = mongoose.model(`pac_qp_score_${name}`, PACQpScoreSchema));
