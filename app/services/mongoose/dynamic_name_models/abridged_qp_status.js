const mongoose = require('mongoose');
const PacQpStatus = require('../models/pac_qp_status').schema;

const modelCache = { };

module.exports = snapshotId => // eslint-disable-line no-return-assign
  modelCache[`abridged_qp_status_${snapshotId}`] ||
  (modelCache[`abridged_qp_status_${snapshotId}`] = mongoose.model(`abridged_qp_status_${snapshotId}`, PacQpStatus));
