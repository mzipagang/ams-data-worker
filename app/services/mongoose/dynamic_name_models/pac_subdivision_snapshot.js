const mongoose = require('mongoose');
const PACSubdivisionSchema = require('../models/pac_subdivision').schema;

const modelCache = { };

module.exports = id => // eslint-disable-line no-return-assign
  modelCache[`pac_subdivision_snapshot_${id}`] ||
  (modelCache[`pac_subdivision_snapshot_${id}`] =
      mongoose.model(`pac_subdivision_snapshot_${id}`, PACSubdivisionSchema));
