const mongoose = require('mongoose');
const APMEntitySchema = require('../models/apm_entity').schema;

const modelCache = { };

module.exports = id => // eslint-disable-line no-return-assign
  modelCache[`apm_entity_snapshot_${id}`] ||
  (modelCache[`apm_entity_snapshot_${id}`] = mongoose.model(`apm_entity_snapshot_${id}`, APMEntitySchema));
