const mongoose = require('mongoose');
const PACProviderSchema = require('../models/pac_provider').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_provider_${name}`] ||
  (modelCache[`pac_provider_${name}`] = mongoose.model(`pac_provider_${name}`, PACProviderSchema));
