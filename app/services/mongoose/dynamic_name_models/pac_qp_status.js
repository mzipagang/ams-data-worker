const mongoose = require('mongoose');
const PACQpStatusSchema = require('../models/pac_qp_status').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`pac_qp_status_${name}`] ||
  (modelCache[`pac_qp_status_${name}`] = mongoose.model(`pac_qp_status_${name}`, PACQpStatusSchema));
