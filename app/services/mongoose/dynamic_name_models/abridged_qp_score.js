const mongoose = require('mongoose');
const PACQpScoreSchema = require('../models/pac_qp_score').schema;

const modelCache = { };
module.exports = snapshotId => // eslint-disable-line no-return-assign
  modelCache[`abridged_qp_score_${snapshotId}`] ||
  (modelCache[`abridged_qp_score_${snapshotId}`] = mongoose.model(`abridged_qp_score_${snapshotId}`, PACQpScoreSchema));
