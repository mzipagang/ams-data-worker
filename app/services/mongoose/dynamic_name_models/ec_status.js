const mongoose = require('mongoose');
const EcStatusSchema = require('../models/ec_status').schema;

const modelCache = { };

module.exports = name => // eslint-disable-line no-return-assign
  modelCache[`ec_status_snapshot_${name}`] ||
  (modelCache[`ec_status_snapshot_${name}`] =
    mongoose.model(`ec_status_snapshot_${name}`, EcStatusSchema));

