const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacQpStatus');

const PacQpStatusSchema = new mongoose.Schema(joigoose.convert(Schema));

PacQpStatusSchema.index({ npi: 1 });

module.exports = mongoose.model('pac_qp_status', PacQpStatusSchema);
