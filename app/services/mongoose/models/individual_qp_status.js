const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/IndividualQpStatus');

const IndividualQpStatusSchema = new mongoose.Schema(joigoose.convert(Schema));

IndividualQpStatusSchema.index({ npi: 1 });

module.exports = mongoose.model('individual_qp_status', IndividualQpStatusSchema);
