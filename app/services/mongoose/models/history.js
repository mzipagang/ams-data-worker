const mongoose = require('mongoose');

/**
 * This is a read-only schema intended to simply use with the mongoose-diff-history plugin.
 * You can query histories using this schema, though the built-in utilities are probably a better option.
 * The primary use case for this schema is deleting the history collection after unit/integration tests
 */
const HistorySchema = new mongoose.Schema({
  collectionId: mongoose.Schema.Types.ObjectId,
  collectionName: String,
  diff: mongoose.Schema.Types.Mixed,
  version: Number,
  createdAt: Date,
  updatedAt: Date
});

HistorySchema.pre('save', (next) => {
  next(new Error('This history schema is read only'));
});

module.exports = mongoose.model('histories', HistorySchema);
