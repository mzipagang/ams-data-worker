const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/QppProvider');

const QppProviderSchema = new mongoose.Schema(joigoose.convert(Schema));

QppProviderSchema.index({ performance_year: 1 });
QppProviderSchema.index({ npi: 1, performance_year: 1 });
QppProviderSchema.index({ tin: 1, performance_year: 1 });
QppProviderSchema.index({ entity_id: 1, performance_year: 1 });

module.exports = mongoose.model('qpp_provider', QppProviderSchema);
