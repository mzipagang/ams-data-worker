const mongoose = require('mongoose');

const IndividualQpStatusSchema = new mongoose.Schema({
  file_id: { type: String, require: true },
  file_line_number: { type: Number, require: true },
  npi: { type: String, required: true },
  qp_status: { type: String, required: true },
  performance_year: { type: Number, required: false },
  run_snapshot: { type: String, required: true }
});

IndividualQpStatusSchema.index({ file_id: 1, file_line_number: 1 });

module.exports = mongoose.model('source_file_individual_qp_status', IndividualQpStatusSchema);
