const mongoose = require('mongoose');
const moment = require('moment');

const FileImportErrorSchema = new mongoose.Schema({
  file_import_group_id: { type: String, required: true },
  file_id: { type: String, required: true },
  error: { type: String, required: true },
  line_number: { type: Number, required: true },
  createdAt: { type: Date, required: true, default: () => moment.utc().toDate() }
});

FileImportErrorSchema.methods.toJson = function toJson() {
  return this.toObject();
};

module.exports = mongoose.model('file_import_error', FileImportErrorSchema);
