const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacLvt');

const PacLVTSchema = new mongoose.Schema(joigoose.convert(Schema));

PacLVTSchema.index({ entity_id: 1 });

module.exports = mongoose.model('pac_lvt', PacLVTSchema);
