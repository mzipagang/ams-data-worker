const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacEntity');

const PacEntitySchema = new mongoose.Schema(joigoose.convert(Schema));

PacEntitySchema.index({ id: 1 });

module.exports = mongoose.model('pac_entity', PacEntitySchema);
