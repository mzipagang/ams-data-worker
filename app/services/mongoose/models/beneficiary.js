const mongoose = require('mongoose');

const BeneficiarySchema = new mongoose.Schema({
  program_id: { type: String, required: false },
  entity_id: { type: String, required: false },
  link_key: { type: String, required: false },
  program_org_id: { type: String, required: false },
  id: { type: String, required: false },
  hicn: { type: String, required: true },
  first_name: { type: String, required: false },
  middle_name: { type: String, required: false },
  last_name: { type: String, required: false },
  dob: { type: Date, required: true },
  gender: { type: String, required: true },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  category_code: { type: String, required: false },
  org_payment_track: { type: String, required: false },
  org_payment_track_start_date: { type: Date, required: false },
  org_payment_track_end_date: { type: Date, required: false },
  voluntary_alignment: { type: String, required: false },
  other_id: { type: String, required: false },
  other_id_type: { type: String, required: false },
  mbi: { type: String, required: true }
});

BeneficiarySchema.index({ id: 1 });

module.exports = mongoose.model('beneficiary', BeneficiarySchema);
