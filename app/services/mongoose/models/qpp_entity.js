const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/QppEntity');

const QppEntitySchema = new mongoose.Schema(joigoose.convert(Schema));

module.exports = mongoose.model('qpp_entity', QppEntitySchema);

