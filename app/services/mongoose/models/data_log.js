const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const moment = require('moment');

const DataOriginSchema = new mongoose.Schema({
  source: { type: String },
  meta: { type: Object }
});

const DataLogSchema = new mongoose.Schema({
  origin: { type: DataOriginSchema },
  dataType: { type: String },
  receiptDate: { type: Date, required: true },
  processedDate: { type: Date, required: true },
  action: { type: String },
  value: { type: Object },
  target: { type: String }
});

DataLogSchema.methods.toJson = function toJson() {
  return this.toObject();
};

DataLogSchema.statics.createLog = function createLog(action, newValue, inMeta) {
  const meta = inMeta || {};
  return this.create({
    origin: {
      source: meta.source || 'unknown',
      meta
    },
    dataType: newValue.constructor.modelName,
    receiptDate: meta.received || moment.utc().toDate(),
    processedDate: moment.utc().toDate(),
    action,
    value: newValue.toObject(),
    target: newValue.collection.collectionName
  });
};

DataLogSchema.statics.createLogBatch = function createLog(action, newValuesWithMeta) {
  const values = newValuesWithMeta.map((data) => {
    const meta = data.meta;
    const newValue = data.value;

    return {
      origin: {
        source: meta.source || 'unknown',
        meta
      },
      dataType: newValue.constructor.modelName,
      receiptDate: meta.received || moment.utc().toDate(),
      processedDate: moment.utc().toDate(),
      action,
      value: newValue.toObject(),
      target: newValue.collection.collectionName
    };
  });

  return this.insertMany(values);
};

DataLogSchema.statics.actions = {
  CREATE: 'create',
  DELETE: 'delete',
  UPDATE: 'update',
  UPSERT: 'upsert'
};

module.exports = mongoose.model('data_log', DataLogSchema);
