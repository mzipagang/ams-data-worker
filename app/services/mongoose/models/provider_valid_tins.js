const mongoose = require('mongoose');

const providerValidTins = new mongoose.Schema({
  npi: { type: String, required: true, default: null },
  validTinPrvdr: { type: String, required: true, default: null },
  validTinsEmplr: { type: [String], required: true, default: [] },
  validTinsReasgnmt: { type: [String], required: true, default: [] },
  deleted: { type: Boolean, required: true }
});

providerValidTins.index({ npi: 1 });

module.exports = mongoose.model('provider_valid_tins', providerValidTins);
