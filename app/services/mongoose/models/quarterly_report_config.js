const mongoose = require('mongoose');

const QuarterlyReportConfig = new mongoose.Schema({
  year: { type: Number, required: true, unique: true },
  quarter1_start: { type: Date, required: true },
  quarter1_end: { type: Date, required: true },
  quarter2_start: { type: Date, required: true },
  quarter2_end: { type: Date, required: true },
  quarter3_start: { type: Date, required: true },
  quarter3_end: { type: Date, required: true },
  quarter4_start: { type: Date, required: true },
  quarter4_end: { type: Date, required: true }
});

QuarterlyReportConfig.index({ year: 1 });

module.exports = mongoose.model('quarterly_report_config', QuarterlyReportConfig);
