const mongoose = require('mongoose');

const MigrationMutexSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true
  }
});

module.exports = mongoose.model('migration_mutex', MigrationMutexSchema);
