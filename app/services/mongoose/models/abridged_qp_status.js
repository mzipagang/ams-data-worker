const mongoose = require('mongoose');
const PacQpStatusSchema = require('./pac_qp_status').schema;

module.exports = mongoose.model('abridged_qp_status', PacQpStatusSchema);
