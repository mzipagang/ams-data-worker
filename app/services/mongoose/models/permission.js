const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const Schema = require('../../../schemas/User').Permission;
const diffHistory = require('mongoose-diff-history/diffHistory');

const PermissionSchema = new mongoose.Schema(joigoose.convert(Schema), { timestamps: true });

PermissionSchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('permission', PermissionSchema);
