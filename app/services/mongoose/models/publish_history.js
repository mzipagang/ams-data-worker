const mongoose = require('mongoose');
const uuid = require('uuid');
const moment = require('moment');
mongoose.Promise = require('bluebird');

const CacheSchema = new mongoose.Schema({
  status: { type: String, required: true, default: 'unbuilt' },
  expected: { type: Number, required: true, default: 0 },
  finished: { type: Number, required: true, default: 0 }
});

const PublishHistoryFileSchema = new mongoose.Schema({
  file_id: { type: String, required: true },
  import_file_type: { type: String, required: true },
  apm_id: { type: String, default: null, required: false },
  status: { type: String, required: true },
  new_file: { type: Boolean, required: false, default: true },
  snapshot: { type: String, required: false },
  execution: { type: Number, required: false }
});

const PublishHistorySchema = new mongoose.Schema({
  id: { type: String, required: true, default: uuid.v4 },
  file_import_group_id: { type: String, required: true },
  published_at: { type: Date, required: true, default: () => moment.utc().toDate() },
  files: [PublishHistoryFileSchema],
  group_type: { type: String, required: true },
  performance_year: { type: Number, required: false },
  status: { type: String, required: true },
  cache: { type: CacheSchema, required: true, default: () => ({}) }
});

PublishHistorySchema.index({ published_at: 1 });

module.exports = mongoose.model('publish_history', PublishHistorySchema);
