const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/EntityQpThreshold');

const EntityQpThresholdSchema = new mongoose.Schema(joigoose.convert(Schema));

EntityQpThresholdSchema.index({ npi: 1 });

module.exports = mongoose.model('entity_qp_threshold', EntityQpThresholdSchema);
