const mongoose = require('mongoose');
const PacLvtSchema = require('./pac_lvt').schema;

module.exports = mongoose.model('abridged_lvt', PacLvtSchema);
