const mongoose = require('mongoose');

const specialtyCode = new mongoose.Schema({
  code: { type: String, required: true },
  description: { type: String, required: true },
  eligible_clinician: { type: Boolean, required: true },
  mips_eligibleClinician: { type: Boolean, required: true },
  comment: { type: String, required: false },
  year: { type: Number, required: true }
}, { timestamps: true });

module.exports = mongoose.model('specialty_code', specialtyCode);
