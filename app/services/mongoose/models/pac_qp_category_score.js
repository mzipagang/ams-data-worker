const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacQualityPerformanceCategoryScore');

const QualityPerformanceCategoryScore = new mongoose.Schema(joigoose.convert(Schema));

QualityPerformanceCategoryScore.index({ entity_id: 1 });

module.exports = mongoose.model('pac_quality_performance_category_score', QualityPerformanceCategoryScore);
