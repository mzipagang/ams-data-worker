const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacQualityPerformanceCategoryScoreSummary');

const QpcScoreSummary = new mongoose.Schema(joigoose.convert(Schema));

QpcScoreSummary.index({ entity_id: 1 });

module.exports = mongoose.model('source_file_qpc_score_summary', QpcScoreSummary);
