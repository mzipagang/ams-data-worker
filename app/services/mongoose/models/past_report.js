const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const Schema = require('../../../schemas/PastReport').PastReportSchema;

const PastReportSchema = new mongoose.Schema(joigoose.convert(Schema));

PastReportSchema.index({ performance_year: 1, run_snapshot: 1, date: 1 });

module.exports = mongoose.model('past_report', PastReportSchema);
