const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/IndividualQpThreshold');

const IndividualQpThresholdSchema= new mongoose.Schema(joigoose.convert(Schema));

IndividualQpThresholdSchema.index({ npi: 1 });

module.exports = mongoose.model('individual_qp_threshold', IndividualQpThresholdSchema);
