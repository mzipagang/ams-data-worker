const mongoose = require('mongoose');

const EntityEligibilitySchema = new mongoose.Schema({
  file_id: { type: String, required: true },
  file_line_number: { type: Number, require: true },
  entity_id: { type: String, required: true },
  claims_types: { type: String, required: true },
  lv_status: { type: String, required: true },
  lv_status_reason: { type: String, required: false },
  lv_part_b_expenditures: { type: Number, required: true },
  lv_beneficiary_count: { type: Number, required: true },
  small_status_apm_code: { type: String, required: false },
  small_status_clinician_count: { type: Number, required: false },
  performance_year: { type: Number, required: false },
  run_snapshot: { type: String, required: true },
  complex_patient_score: { type: Number, required: true }
});

EntityEligibilitySchema.index({ file_id: 1, entity_id: 1 });

module.exports = mongoose.model('source_file_entity_eligibility', EntityEligibilitySchema);
