const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/EntityQpStatus');

const EntityQpStatusSchema = new mongoose.Schema(joigoose.convert(Schema));

EntityQpStatusSchema.index({ npi: 1 });

module.exports = mongoose.model('entity_qp_status', EntityQpStatusSchema);
