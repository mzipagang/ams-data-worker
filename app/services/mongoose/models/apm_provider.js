const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const moment = require('moment');

const ValidationSchema = new mongoose.Schema({
  isValid: { type: Boolean, required: false },
  errorCode: { type: String, required: false },
  validationType: { type: String, required: false }
});

const SourceSchema = new mongoose.Schema({
  source_type: { type: String, required: false },
  file_id: { type: String, required: false }
});

const ApmProviderParticipationSchema = new mongoose.Schema({
  source: { type: SourceSchema, required: false },
  start_date: { type: Date, required: false },
  end_date: { type: Date, required: false },
  ccn: { type: String, required: false },
  tin_type_code: { type: String, required: false },
  specialty_code: { type: String, required: false },
  organization_name: { type: String, required: false },
  first_name: { type: String, required: false },
  middle_name: { type: String, required: false },
  last_name: { type: String, required: false },
  participant_type_code: { type: String, required: false },
  validations: { type: [ValidationSchema], required: false }
});

const ApmProviderSchema = new mongoose.Schema({
  entity_id: { type: String, required: true },
  tin: { type: String, required: true },
  npi: { type: String, required: false },
  participation: { type: [ApmProviderParticipationSchema], required: false },
  relationship_code: { type: String, enum: ['P', 'S'], required: false }
});

ApmProviderSchema.index({ entity_id: 1, tin: 1, npi: 1 }, { unique: true });

ApmProviderSchema.methods.toJson = function toJson() {
  const participationData = this.participation.map((data) => {
    const dates = {};

    // istanbul ignore else
    if (data.start_date) {
      dates.start_date = moment.utc(data.start_date).format('YYYY-MM-DD');
    }

    // istanbul ignore else
    if (data.end_date) {
      dates.end_date = moment.utc(data.end_date).format('YYYY-MM-DD');
    }

    return Object.assign({}, data, dates);
  });

  return Object.assign({}, this.toObject(), { participation: participationData });
};

module.exports = mongoose.model('apm_provider', ApmProviderSchema);
