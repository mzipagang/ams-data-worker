const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const uuid = require('uuid');


const PacSnapshotSchema = new mongoose.Schema({
  id: { type: String, default: uuid.v4 },
  createdAt: { type: Date, required: true, default: Date.now },
  performance_year: { type: Number, required: true },
  run_snapshot: { type: String, required: false },
  run_number: { type: String, required: false },
  file_id: { type: String, required: true },
  type: { type: String, required: true }
});

PacSnapshotSchema.methods.toJson = function toJson() {
  return this.toObject();
};

module.exports = mongoose.model('pac_snapshot', PacSnapshotSchema);
