const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/EntityEligibility');

const EntityEligibilitySchema = new mongoose.Schema(joigoose.convert(Schema));

EntityEligibilitySchema.index({ entity_id: 1 });

module.exports = mongoose.model('entity_eligibility', EntityEligibilitySchema);
