const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacQpScore');

const PacQpScoreSchema = new mongoose.Schema(joigoose.convert(Schema));

PacQpScoreSchema.index({ npi: 1 });
PacQpScoreSchema.index({ npi: 1, tin: 1, entity_id: 1 });

module.exports = mongoose.model('pac_qp_score', PacQpScoreSchema);
