const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const moment = require('moment');

const AddressSchema = new mongoose.Schema({
  address_1: { type: String, required: false },
  address_2: { type: String, required: false },
  city: { type: String, required: false },
  zip4: { type: String, required: false },
  zip5: { type: String, required: false },
  state: { type: String, required: false }
});

const ApmEntitySchema = new mongoose.Schema({
  file_id: { type: String, required: true },
  file_line_number: { type: Number, required: true },
  id: { type: String, required: true },
  apm_id: { type: String, required: true },
  subdiv_id: { type: String, required: false },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  name: { type: String, required: true },
  dba: { type: String, required: false },
  type: { type: String, required: false },
  tin: { type: String, required: false },
  npi: { type: String, required: false },
  ccn: { type: String, required: false },
  additional_information: { type: String, required: false },
  address: { type: AddressSchema, required: false }
});

ApmEntitySchema.index({ file_id: 1, file_line_number: 1 });

ApmEntitySchema.methods.toJson = function toJson() {
  const dates = {};

  // istanbul ignore else
  if (this.start_date) {
    dates.start_date = moment.utc(this.start_date).format('YYYY-MM-DD');
  }

  // istanbul ignore else
  if (this.end_date) {
    dates.end_date = moment.utc(this.end_date).format('YYYY-MM-DD');
  }

  return Object.assign({}, this.toObject(), dates);
};

module.exports = mongoose.model('source_file_entity_data', ApmEntitySchema);
