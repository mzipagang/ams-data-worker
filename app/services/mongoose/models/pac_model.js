const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacModel');

const PacModelSchema = new mongoose.Schema(joigoose.convert(Schema));

const buildModelIdsList = () => {
  const modelIds = [];
  for (let i = 55; i < 100; i += 1) {
    const index = i.toString();
    modelIds.push(index);
  }
  return modelIds;
};

PacModelSchema.statics.createNewPacModelId = function createNewPacModelId(models) {
  return buildModelIdsList().find(existingModel => !models.find(mod => mod.id === existingModel));
};

PacModelSchema.index({ id: 1 });

module.exports = mongoose.model('pac_model', PacModelSchema);
