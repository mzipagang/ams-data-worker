const mongoose = require('mongoose');
const moment = require('moment');
const diffHistory = require('mongoose-diff-history/diffHistory');
const uuid = require('uuid');
const NameValueSchema = require('../shared_models/name_value');
const QuarterlyReportConfig = require('./quarterly_report_config');

async function validateQuarter(value) {
  const config = await QuarterlyReportConfig.findOne({ year: moment().year() }).lean();
  const date = moment();

  return config &&
    date.isSameOrAfter(moment(config[`quarter${value}_start`])) &&
    date.isBefore(moment(config[`quarter${value}_end`]));
}

const QuarterlyReport = new mongoose.Schema({
  id: { type: String, default: uuid.v4, required: true },
  model_id: { type: String, required: true },
  model_name: { type: String, required: true },
  model_nickname: { type: String, required: false },
  group: { type: [String], required: false },
  sub_group: { type: String, required: false },
  team_lead: { type: String, required: false },
  statutory_authority: { type: String, required: false },
  waivers: { type: [NameValueSchema], required: false },
  announcement_date: { type: Date, required: false },
  award_date: { type: Date, required: false },
  performance_start_date: { type: Date, required: false },
  performance_end_date: { type: Date, required: false },
  additional_information: { type: String, required: false },
  num_ffs_beneficiaries: { type: Number, required: false },
  num_medicare_beneficiaries: { type: Number, required: false },
  num_advantage_beneficiaries: { type: Number, required: false },
  num_medicaid_beneficiaries: { type: Number, required: false },
  num_chip_beneficiaries: { type: Number, required: false },
  num_dually_eligible_beneficiaries: { type: Number, required: false },
  num_private_insurance_beneficiaries: { type: Number, required: false },
  num_other_beneficiaries: { type: Number, required: false },
  total_beneficiaries: { type: Number, required: false },
  notes_for_beneficiary_counts: { type: String, required: false },
  num_physicians: { type: Number, required: false },
  num_other_individual_human_providers: { type: Number, required: false },
  num_hospitals: { type: Number, required: false },
  num_other_providers: { type: Number, required: false },
  total_providers: { type: Number, required: false },
  notes_provider_count: { type: String, required: false },
  year: { type: Number, required: false },
  quarter: { type: Number, required: false, validate: [validateQuarter, 'Reporting quarter is not valid'] },
  updated_at: { type: Date, required: true, default: Date.now },
  updated_by: { type: String },
  created_at: { type: Date, required: true, default: Date.now },
  created_by: { type: String }
});

QuarterlyReport.plugin(diffHistory.plugin);

module.exports = mongoose.model('quarterly_report', QuarterlyReport);
