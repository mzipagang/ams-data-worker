const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const moment = require('moment');

const ApmProviderSchema = new mongoose.Schema({
  file_id: { type: String, required: true },
  file_line_number: { type: Number, required: true },
  entity_id: { type: String, required: true },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  tin: { type: String, required: true },
  npi: { type: String, required: false },
  ccn: { type: String, required: false },
  tin_type_code: { type: String, required: false },
  specialty_code: { type: String, required: false },
  organization_name: { type: String, required: false },
  first_name: { type: String, required: false },
  middle_name: { type: String, required: false },
  last_name: { type: String, required: false },
  participant_type_code: { type: String, required: false }
});

ApmProviderSchema.index({ file_id: 1, file_line_number: 1 });

ApmProviderSchema.methods.toJson = function toJson() {
  const dates = {};

  // istanbul ignore else
  if (this.start_date) {
    dates.start_date = moment.utc(this.start_date).format('YYYY-MM-DD');
  }

  // istanbul ignore else
  if (this.end_date) {
    dates.end_date = moment.utc(this.end_date).format('YYYY-MM-DD');
  }

  return Object.assign({}, this.toObject(), dates);
};

module.exports = mongoose.model('source_file_provider_data', ApmProviderSchema);
