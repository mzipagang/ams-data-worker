const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacSubdivision');

const PacSubdivisionSchema = new mongoose.Schema(joigoose.convert(Schema));

const getNextAvailable = (array) => {
  for (let i = 0; i <= 99; i += 1) {
    if (!array.includes(i)) {
      const id = i.toString();
      return id.length === 1 ? `0${id}` : id;
    }
  }
  return null;
};

PacSubdivisionSchema.statics.createNewPacSubdivisionId = function createNewPacSubdivisionId(subdivisions) {
  const usedIds = subdivisions.map(sub => parseInt(sub.id));
  return getNextAvailable(usedIds);
};

PacSubdivisionSchema.index({ id: 1 });
PacSubdivisionSchema.index({ id: 1, apm_id: 1 });

module.exports = mongoose.model('pac_subdivision', PacSubdivisionSchema);
