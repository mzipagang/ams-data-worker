const mongoose = require('mongoose');

const IndividualQpThresholdSchema = new mongoose.Schema({
  file_id: { type: String, require: true },
  file_line_number: { type: Number, require: true },
  apm_id: { type: String, required: false },
  subdivision_id: { type: String, required: false },
  entity_id: { type: String, required: false },
  tin: { type: String, required: false },
  npi: { type: String, required: true },
  qp_status: { type: String, required: true },
  expenditures_numerator: { type: Number, required: false },
  expenditures_denominator: { type: Number, required: false },
  payment_threshold_score: { type: Number, required: false },
  payment_threshold_met: { type: String, required: false },
  beneficiaries_numerator: { type: Number, required: false },
  beneficiaries_denominator: { type: Number, required: false },
  patient_threshold_score: { type: Number, required: false },
  patient_threshold_met: { type: String, required: false },
  performance_year: { type: Number, required: false },
  run_snapshot: { type: String, required: true }
});

IndividualQpThresholdSchema.index({ file_id: 1, file_line_number: 1 });

module.exports = mongoose.model('source_file_individual_qp_threshold', IndividualQpThresholdSchema);
