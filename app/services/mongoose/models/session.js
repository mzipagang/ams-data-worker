const config = require('../../../config');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const moment = require('moment');
const jwt = require('jsonwebtoken');
const DataLog = require('./data_log');

const JWT_SECRET = config.JWT.SECRET;

const SessionSchema = new mongoose.Schema({
  token: { type: String, required: true, unique: true },
  expiresAt: { type: Date },
  data: { type: Object }
});

SessionSchema.index({ expiresAt: 1 }, { expireAfterSeconds: 0 });

SessionSchema.methods.toJson = function toJson() {
  return this.toObject();
};

SessionSchema.statics.createSession = function createSession(data, expiresInSeconds, meta) {
  const jwtData = Object.assign({}, data, { expiresInSeconds });
  const token = jwt.sign(jwtData, JWT_SECRET);
  const obj = {
    token,
    data
  };

  if (expiresInSeconds > 0) {
    obj.expiresAt = moment.utc().add(expiresInSeconds, 'seconds').toDate();
  }

  return this.deleteMany({ 'data.username': data.username, 'data.type': data.type })
    .then(() => this.create(obj))
    .then(session =>
      DataLog.createLog(DataLog.actions.CREATE, session, meta).return({ session, token }));
};

SessionSchema.statics.getSession = function getSession(token) {
  return Promise.try(() => this.findOne({ token }))
    .then((session) => {
      if (!session || (session.expiresAt && moment.utc(session.expiresAt).diff(moment.utc()) <= 0)) {
        return { session: undefined };
      }

      return { session };
    });
};

SessionSchema.statics.getAndExtendSession = function getAndExtendSession(token, meta) {
  return Promise.try(() => {
    const tokenData = jwt.verify(token, JWT_SECRET);
    const expiresInSeconds = tokenData.expiresInSeconds || config.SESSION.TIMEOUT_IN_SECONDS;

    if (expiresInSeconds > 0) {
      const expiresAt = moment.utc().add(expiresInSeconds, 'seconds').toDate();
      return this.findOneAndUpdate({ token }, { $set: { expiresAt } }, { new: true })
        .then((session) => {
          if (!session) {
            return undefined;
          } else if (session.expiresAt && moment.utc(session.expiresAt).diff(moment.utc()) <= 0) {
            return { session: undefined };
          }

          return DataLog.createLog(DataLog.actions.UPDATE, session, meta).return(session);
        });
    }

    return this.findOne({ token });
  })
    .then(session => ({ session }));
};

SessionSchema.statics.deleteSession = function deleteSession(token, meta) {
  jwt.verify(token, JWT_SECRET);
  return this.findOneAndRemove({ token })
    .then((session) => {
      if (!session) {
        return undefined;
      }

      return DataLog.createLog(DataLog.actions.DELETE, session, meta);
    });
};

module.exports = mongoose.model('session', SessionSchema);
