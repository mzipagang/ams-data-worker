const mongoose = require('mongoose');

const ecStatus = new mongoose.Schema({
  npi: { type: String, required: true },
  isIndividual: { type: Boolean, required: true },
  eligibleClinician: { type: Boolean, required: true },
  mipsEligibleClinician: { type: Boolean, required: true },
  isNotFound: { type: Boolean, required: true },
  isMissingSpecialityCode: { type: Boolean, required: true },
  codes: [{ type: String, required: true }]
}, { timestamps: true });

ecStatus.index({ npi: 1 });

module.exports = mongoose.model('ec_status', ecStatus);
