const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/FileImportGroup');
const { Statuses } = require('../../../schemas/File');

const FileImportSchema = new mongoose.Schema(joigoose.convert(Schema));

FileImportSchema.statics.getFileById = function getFileById(fileId) {
  return this.findOne({
    'files.id': fileId
  }).then(fileImportGroup => fileImportGroup.files.find(file => file.id === fileId));
};

FileImportSchema.statics.Statuses = Object.freeze(Statuses);

module.exports = mongoose.model('file_import_group', FileImportSchema);
