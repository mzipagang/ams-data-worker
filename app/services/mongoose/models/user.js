const _ = require('lodash');
const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/User');
const diffHistory = require('mongoose-diff-history/diffHistory');
const DataLog = require('./data_log');
const Role = require('./role');

const options = {
  toObject: { virtuals: true },
  toJSON: { virtuals: true },
  timestamps: true
};
const UserSchema = new mongoose.Schema(joigoose.convert(Schema), options);

UserSchema.virtual('name').get(function () {
  return this.firstName;
});

// eslint-disable-next-line func-names
const autoPopulate = function (next) {
  this.populate({ path: 'roles', model: Role });
  next();
};

UserSchema.pre('find', autoPopulate);
UserSchema.pre('findOne', autoPopulate);

UserSchema.options.toJSON.transform = function toJSON(doc, ret) {
  const result = ret;

  if (doc.populated('roles')) {
    const roles = doc.roles;

    const permissions = _.flatten(roles.map(role => role.permissions));

    result.permissions = permissions.map(permission => permission.name);
    result.roles = roles.map(role => role.name);
  } else {
    result.roles = null;
    result.permissions = null;
  }

  return result;
};

UserSchema.statics.upsertUser = async function upsertUser(user) {
  const existingUser = await this.findOne({ username: user.username });

  let result;
  if (existingUser) {
    // Can only update name and mail. Do not update username or roles
    existingUser.firstName = user.firstName;
    existingUser.lastName = user.lastName;
    existingUser.mail = user.mail;
    existingUser.__user = user.username; // Used by the mongoose-diff-history plugin
    existingUser.__reason = 'Updated from LDAP'; // Used by the mongoose-diff-history plugin
    result = await existingUser.save();
  } else {
    user.roles = []; // Cannot set roles here
    await this.create(user);
    result = await this.findOne({ username: user.username });
  }

  // TODO - this DataLog can probably be removed once we're comfortable with mongoose-diff-history
  await DataLog.createLog(DataLog.actions.UPSERT, result);
  return result;
};

UserSchema.statics.getUser = async function getUser(username) {
  const user = await this.findOne({ username });

  if (!user) {
    return undefined;
  }
  return user.toJSON();
};

UserSchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('user', UserSchema);
