const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const uuid = require('uuid');

const CollectionInfoSchema = new mongoose.Schema({
  name: { type: String },
  type: { type: String }
});

const SnapshotSchema = new mongoose.Schema({
  id: { type: String, default: uuid.v4 },
  createdAt: { type: Date, required: true, default: Date.now },
  qppYear: { type: Number, required: true },
  qpPeriod: { type: String, enum: ['Predictive', 'Initial', 'Second', 'Final'], required: true },
  snapshotRun: { type: String, enum: ['firstrun', 'rerun'], required: true },
  createdBy: { type: String },
  reviewedBy: { type: String },
  reviewedAt: { type: Date, default: Date.now },
  collections: { type: [CollectionInfoSchema] },
  initNotes: { type: String },
  rejectNotes: { type: String },
  status: { type: String, enum: ['processing', 'complete', 'error', 'approved', 'rejected'], required: true }
}, { usePushEach: true });

SnapshotSchema.methods.toJson = function toJson() {
  return this.toObject();
};

module.exports = mongoose.model('snapshot', SnapshotSchema);
