const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacProvider');

const PacProviderSchema = new mongoose.Schema(joigoose.convert(Schema));

PacProviderSchema.index({ npi: 1, tin: 1 });
PacProviderSchema.index({ tin: 1, npi: 1 });
PacProviderSchema.index({ entity_id: 1 });
PacProviderSchema.index({ npi: 1, entity_id: 1 });

module.exports = mongoose.model('pac_provider', PacProviderSchema);
