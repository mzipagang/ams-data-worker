const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const moment = require('moment');

const AddressSchema = new mongoose.Schema({
  address_1: { type: String, required: false },
  address_2: { type: String, required: false },
  city: { type: String, required: false },
  zip4: { type: String, required: false },
  zip5: { type: String, required: false },
  state: { type: String, required: false }
});

const ApmEntitySchema = new mongoose.Schema({
  id: { type: String, required: true },
  apm_id: { type: String, required: true },
  subdiv_id: { type: String, required: false },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  tin: { type: String, required: false },
  npi: { type: String, required: false },

  name: { type: String, required: true },
  dba: { type: String, required: false },
  type: { type: String, required: false },
  ccn: { type: String, required: false },
  additional_information: { type: String, required: false },
  address: { type: AddressSchema, required: false }
});

ApmEntitySchema.index(
  { id: 1, apm_id: 1, subdiv_id: 1, start_date: 1, end_date: 1, tin: 1, npi: 1 },
  { unique: true, name: 'idx1' }
);

ApmEntitySchema.methods.toJson = function toJson() {
  const dates = {};

  // istanbul ignore else
  if (this.start_date) {
    dates.start_date = moment.utc(this.start_date).format('YYYY-MM-DD');
  }

  // istanbul ignore else
  if (this.end_date) {
    dates.end_date = moment.utc(this.end_date).format('YYYY-MM-DD');
  }

  return Object.assign({}, this.toObject(), dates);
};

module.exports = mongoose.model('apm_entity', ApmEntitySchema);
