const mongoose = require('mongoose');

const MigrationSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true
  }
});

module.exports = mongoose.model('migration', MigrationSchema);
