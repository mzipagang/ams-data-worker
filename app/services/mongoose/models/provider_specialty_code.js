const mongoose = require('mongoose');

const providerSpecialtyCode = new mongoose.Schema({
  npi: { type: String, required: true, unique: true },
  providerTypeCode: { type: String, required: true },
  specialtyCodes: { type: [String], required: false, default: [] },
  deleted: { type: Boolean, required: false, default: true }
}, { timestamps: true });

providerSpecialtyCode.index({ npi: 1 }, { unique: true });

module.exports = mongoose.model('provider_specialty_code', providerSpecialtyCode);
