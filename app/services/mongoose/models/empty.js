const mongoose = require('mongoose');

// This is an empty collection we use for collections that have optional data.
// It is used by the qpp_provider collection aggergration
const empty = new mongoose.Schema({});

module.exports = mongoose.model('empty', empty);
