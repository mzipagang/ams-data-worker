const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const moment = require('moment');

const BeneficiarySchema = new mongoose.Schema({
  file_id: { type: String, required: true },
  file_line_number: { type: Number, required: true },
  program_id: { type: String, required: false },
  entity_id: { type: String, required: true },
  link_key: { type: String, required: false },
  program_org_id: { type: String, required: false },
  id: { type: String, required: false },
  hicn: { type: String, required: true },
  first_name: { type: String, required: false },
  middle_name: { type: String, required: false },
  last_name: { type: String, required: false },
  dob: { type: Date, required: true },
  gender: { type: String, required: true },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  category_code: { type: String, required: false },
  org_payment_track: { type: String, required: false },
  org_payment_track_start_date: { type: Date, required: false },
  org_payment_track_end_date: { type: Date, required: false },
  voluntary_alignment: { type: String, required: false },
  other_id: { type: String, required: false },
  other_id_type: { type: String, required: false },
  mbi: { type: String, required: true }
});

BeneficiarySchema.index({ file_id: 1, file_line_number: 1 });

BeneficiarySchema.methods.toJson = function toJson() {
  const dates = {};

  // istanbul ignore else
  if (this.start_date) {
    dates.start_date = moment.utc(this.start_date).format('YYYY-MM-DD');
  }

  // istanbul ignore else
  if (this.end_date) {
    dates.end_date = moment.utc(this.end_date).format('YYYY-MM-DD');
  }

  return Object.assign({}, this.toObject(), dates);
};


module.exports = mongoose.model('source_file_beneficiary_data', BeneficiarySchema);
