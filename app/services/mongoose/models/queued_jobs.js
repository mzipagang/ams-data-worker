const mongoose = require('mongoose');
const uuid = require('uuid');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/QueuedJobs');

const QueuedJobsSchema = new mongoose.Schema(joigoose.convert(Schema));

QueuedJobsSchema.index({ id: 1 });

QueuedJobsSchema.pre('validate', function(next) {
  if (!this.id) {
    this.id = uuid.v4();
  }
  next();
});

module.exports = mongoose.model('queued_jobs', QueuedJobsSchema);
