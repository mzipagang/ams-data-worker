const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const Schema = require('../../../schemas/User').Role;
const diffHistory = require('mongoose-diff-history/diffHistory');
const Permission = require('./permission');

const RoleSchema = new mongoose.Schema(joigoose.convert(Schema), { timestamps: true });

// eslint-disable-next-line func-names
const autoPopulatePermissions = function (next) {
  this.populate({ path: 'permissions', model: Permission });
  next();
};

RoleSchema.pre('find', autoPopulatePermissions);
RoleSchema.pre('findOne', autoPopulatePermissions);

RoleSchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('role', RoleSchema);
