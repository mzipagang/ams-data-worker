const mongoose = require('mongoose');
const PacQpScoreSchema = require('./pac_qp_score').schema;

module.exports = mongoose.model('abridged_qp_score', PacQpScoreSchema);
