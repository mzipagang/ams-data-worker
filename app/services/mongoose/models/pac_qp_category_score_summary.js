const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const { Schema } = require('../../../schemas/PacQualityPerformanceCategoryScoreSummary');

const QualityPerformanceCategoryScoreSummary = new mongoose.Schema(joigoose.convert(Schema));

QualityPerformanceCategoryScoreSummary.index({ entity_id: 1 });

module.exports =
    mongoose.model('pac_quality_performance_category_score_summary', QualityPerformanceCategoryScoreSummary);
