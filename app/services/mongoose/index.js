const glob = require('glob');
const path = require('path');
const mongoose = require('mongoose');
const extend = require('mongoose-schema-extend'); // eslint-disable-line no-unused-vars
const logger = require('../logger');
mongoose.Promise = require('bluebird');

glob.sync('models/*', { cwd: __dirname, ignore: '**/*.spec.js' }).forEach((file) => {
  require(path.join(__dirname, file));
});

mongoose.ams_migrations = glob
  .sync('migrations/*.js', { cwd: __dirname, ignore: '**/*.spec.js' })
  .sort()
  .map(file => ({
    migration: async () => {
      const migration = require(path.join(__dirname, file));

      let migrationRecord = await mongoose.models.migration.findOne({
        name: file
      });

      if (migrationRecord == null) {
        migrationRecord = new mongoose.models.migration({ name: file }); // eslint-disable-line new-cap
        try {
          logger.info(`Migration: ${file} starting`);
          await migration.up();
        } catch (ex) {
          logger.error(`Migration: Error in ${file} `);
          logger.error(ex);
          return ex;
        }
        await migrationRecord.save();
        logger.info(`Migration: ${file} finished`);
        return null;
      }

      logger.info(`Migration: ${file} already run`);
      return null;
    }
  }));

mongoose.tryGetMutex = async () => {
  let result = null;
  try {
    result = await mongoose.connection.db.collection('migration_mutexes').insertOne({ _id: 'MIGRATION_MUTEX' }); // eslint-disable-line no-await-in-loop
  } catch (e) {
    if (e.code === 11000) {
      // code 11000 is the mongo error code for duplicate key.
      result = { result: { ok: 0, n: 0 } };
    } else {
      throw e;
    }
  }

  if (result.result.ok === 1 && result.result.n === 1) {
    logger.info('Got the migration mutex.');
    return true;
  }

  return false;
};

mongoose.getMutex = async (maxWait = 10000, maxLoops = Number.MAX_VALUE) => {
  let loops = 0;
  while ((await mongoose.tryGetMutex()) === false && loops < maxLoops) { // eslint-disable-line no-await-in-loop
    loops += 1;
    logger.info('Awaiting migration mutex.');
    const ms = Math.floor(Math.random() * maxWait);
    await new Promise((resolve) => { // eslint-disable-line no-await-in-loop
      setTimeout(resolve, ms);
    });
  }
};

mongoose.releaseMutex = async () => {
  await mongoose.connection.db.collection('migration_mutexes').deleteOne({ _id: 'MIGRATION_MUTEX' });
  logger.info('Released migration mutex.');
};

module.exports = mongoose;
