const mongoose = require('mongoose');


const NameValueSchema = new mongoose.Schema({
  value: { type: String, required: true },
  text: { type: String, required: false }
});

module.exports = NameValueSchema;
