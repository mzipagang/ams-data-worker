const _ = require('lodash');

const asInteger = (val) => {
  if (val !== undefined) {
    const num = _.toInteger(val);
    return Number.isNaN(num) ? 0 : num;
  }
  return undefined;
};

const asDecimal = (val) => {
  if (val !== undefined) {
    const num = _.toNumber(val);
    return Number.isNaN(num) ? 0 : _.toNumber(num.toFixed(2));
  }
  return undefined;
};

const asIntegerIfNumeric = (val) => {
  if (val) {
    return Number.isNaN(_.toNumber(val)) ? val : _.toInteger(val);
  }
  return val;
};

const asDecimalIfNumeric = (val) => {
  if (val) {
    const num = _.toNumber(val);
    return Number.isNaN(num) ? val : _.toNumber(num.toFixed(2));
  }
  return val;
};

module.exports = {
  asInteger,
  asDecimal,
  asIntegerIfNumeric,
  asDecimalIfNumeric
};
