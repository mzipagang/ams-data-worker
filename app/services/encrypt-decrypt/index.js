const crypto = require('crypto');
const logger = require('../logger');
const encryptionConfig = require('../../config').ENCRYPTION;

const encryptionKey = encryptionConfig.KEY;
const iv = encryptionConfig.IV;
const algorithm = 'aes-256-cbc';

const encrypt = (text) => {
  let crypted;
  if (text) {
    const cipher = crypto.createCipheriv(algorithm, encryptionKey, iv);
    crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  }
  return crypted;
};

const decrypt = (text) => {
  let decrypted;
  try {
    if (text) {
      const decipher = crypto.createDecipheriv(algorithm, encryptionKey, iv);
      decrypted = decipher.update(text, 'hex', 'utf8');
      decrypted += decipher.final('utf8');
    }
    return decrypted;
  } catch (err) {
    logger.error(err);
    return decrypted;
  }
};

module.exports = {
  encrypt,
  decrypt
};
