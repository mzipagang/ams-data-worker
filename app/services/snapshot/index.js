const moment = require('moment');

const { activeOn, terminatesBefore, startsAfter } = require('../utils/activityDate');
const {
  participatingInApm,
  isFullOrPartialAdvancedApm,
  isFullOrPartialMipsApm,
  isNextGenerationAco
} = require('../utils/model');
const { sourcedFromMdmProvider, sourcedFromNgacoPreferredProvider } = require('../utils/provider');

class SnapshotService {
  /**
   * Determines the relationship code for the given Provider. A Provider Relationship Code denotes the relationship
   * a Provider (TIN/NPI) has with the APM Entity to which they are associated. There are two possible values:
   * Primary ('P') and Secondary ('S').
   * @param provider - the Provider used to determine the Provider Relationship Code
   * @param entity
   * @param model
   * @param qppYear
   * @param snapshotDate
   * @param subdivision
   * @returns {string} 'P' or 'S'
   */
  static determineRelationshipCode(provider, entity, model, qppYear, snapshotDate, subdivision) {
    const modelUnlessSubdivisionExists = subdivision || model;
    const modelIsFullOrPartialAdvancedApm = isFullOrPartialAdvancedApm(modelUnlessSubdivisionExists);
    const modelIsFullOrPartialMipsApm = isFullOrPartialMipsApm(modelUnlessSubdivisionExists);
    const modelIsNextGenerationAco = isNextGenerationAco(model);

    if (modelIsNextGenerationAco && sourcedFromNgacoPreferredProvider(provider) && !sourcedFromMdmProvider(provider)) {
      return 'S';
    }

    if ((modelIsFullOrPartialAdvancedApm || modelIsFullOrPartialMipsApm) && !activeOn(entity, snapshotDate)) {
      return 'S';
    }

    if (
      modelIsFullOrPartialMipsApm &&
      activeOn(entity, `${qppYear}-01-01`) &&
      terminatesBefore(entity, `${qppYear}-03-31`)
    ) {
      return 'S';
    }

    if (modelIsFullOrPartialMipsApm && startsAfter(entity, `${qppYear}-08-31`)) {
      return 'S';
    }

    return 'P';
  }

  /**
   * Given a start and end date and a date range, returns true if the start and end date overlap the date range.
   * Returns false if not.
   * @param startDate
   * @param endDate
   * @param startDateRange
   * @param endDateRange
   * @returns {boolean}
   */
  static startEndDateWithinRange(startDate, endDate, startDateRange, endDateRange) {
    const validStartDate = moment.utc(startDate).isSameOrBefore(moment.utc(endDateRange));
    const validEndDate = moment.utc(endDate).isSameOrAfter(moment.utc(startDateRange));
    return validStartDate && validEndDate;
  }

  /**
   * Helper function for providerStatusFilter. Does two checks:
   * 1) Check if provider contains a start and end date that overlaps with both the entity start and date range and also
   * the performance year date range
   * 2) Check if the apm id (derived from the entity associated with the provider), is contained in the current source
   * models collection
   * @param providerData
   * @param pacModels - array of the models from the source collection
   * @param entity
   * @param {number} qppYear
   * @returns {{id: *, update: {removed: boolean}}}
   */
  static filterProvider(providerData, pacModels, entity, qppYear) {
    let apmId = null;
    let validDateRange = false;

    if (entity) {
      apmId = entity.apm_id;
      const providerDateWithinEntityDateRange = providerData.participation.some(d =>
        SnapshotService.startEndDateWithinRange(d.start_date, d.end_date, entity.start_date, entity.end_date)
      );
      const providerDateWithinPerformanceYear = providerData.participation.some(d =>
        SnapshotService.startEndDateWithinRange(d.start_date, d.end_date, `${qppYear}-01-01`, `${qppYear}-12-31`)
      );
      validDateRange = providerDateWithinEntityDateRange && providerDateWithinPerformanceYear;
    }

    return {
      id: providerData._id,
      update: {
        removed: !validDateRange || !participatingInApm(apmId, pacModels)
      }
    };
  }

  /**
   * Helper function for entityStatusFilter. Does two checks:
   * 1) Check if entity contains a start and end date that overlaps with the performance year date range
   * 2) Check if the apm id (derived from the entity associated with the provider), is contained in the current source
   * models collection
   * @param entityData
   * @param pacModels
   * @param {number} qppYear
   * @returns {{id: *, update: {removed: boolean}}}
   */
  static filterEntity(entityData, pacModels, qppYear) {
    const apmId = entityData.apm_id;
    const validDateRange = SnapshotService.startEndDateWithinRange(
      entityData.start_date,
      entityData.end_date,
      `${qppYear}-01-01`,
      `${qppYear}-12-31`
    );

    return {
      id: entityData._id,
      update: {
        removed: !validDateRange || !participatingInApm(apmId, pacModels)
      }
    };
  }

  static updateRecords(collectionModel, updates) {
    const bulk = collectionModel.collection.initializeUnorderedBulkOp();
    updates.forEach((result) => {
      bulk
        .find({
          _id: result.id
        })
        .updateOne({ $set: result.update });
    });

    return new Promise((resolve, reject) => {
      bulk.execute((err, result) => {
        if (err && !result) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  /**
   * Filters through every row in the entity snapshot collection and updates records based on whether the entity is considered
   * 'active' or not. Adds a field 'removed' with a boolean value.
   * 1) active entities receive a 'removed: false' value
   * 2) non-active entities receive a 'removed: true' value
   * @param entitySnapshot
   * @param modelSnapshot
   * @param {number} qppYear
   * @returns {Promise<*>}
   */
  static async entityStatusFilter(entitySnapshot, modelSnapshot, qppYear) {
    const pacModels = await modelSnapshot.find({}).lean();

    const entityCursor = entitySnapshot
      .find({ removed: { $ne: true } })
      .lean()
      .cursor();

    const updates = [];
    const updatePromises = [];
    entityCursor.eachAsync((data) => {
      const filterResult = SnapshotService.filterEntity(data, pacModels, qppYear);
      updates.push(filterResult);

      if (updates.length > 200) {
        updatePromises.push(SnapshotService.updateRecords(entitySnapshot, updates));
        updates.length = 0;
      }
    });

    if (updates.length > 0) {
      updatePromises.push(SnapshotService.updateRecords(entitySnapshot, updates));
      updates.length = 0;
    }

    return Promise.all(updatePromises);
  }

  /**
   * Filters through every row in the provider snapshot collection and updates records based on whether the provider is considered
   * 'active' or not. Adds a field 'removed' with a boolean value.
   * 1) active providers receive a 'removed: false' value
   * 2) non-active providers receive a 'removed: true' value
   * @param providerSnapshot
   * @param entitySnapshot
   * @param modelSnapshot
   * @param {number} qppYear
   * @returns {Promise<*>}
   */
  static async providerStatusFilter(providerSnapshot, entitySnapshot, modelSnapshot, qppYear) {
    const pacModels = await modelSnapshot.find({}).lean();

    const providerCursor = providerSnapshot
      .find({ removed: { $ne: true } })
      .lean()
      .cursor();

    const updates = [];
    const updatePromises = [];
    await providerCursor.eachAsync(async (data) => {
      const entity = await entitySnapshot.findOne({ id: data.entity_id, removed: { $ne: true } }).lean();

      const filterResult = SnapshotService.filterProvider(data, pacModels, entity, qppYear);
      updates.push(filterResult);

      if (updates.length > 200) {
        updatePromises.push(SnapshotService.updateRecords(providerSnapshot, updates));
        updates.length = 0;
      }
    });

    if (updates.length > 0) {
      updatePromises.push(SnapshotService.updateRecords(providerSnapshot, updates));
      updates.length = 0;
    }

    return Promise.all(updatePromises);
  }

  static async getSubdivisionFromEntity(entity, subdivisionCollection) {
    if (!entity || !subdivisionCollection) {
      return null;
    }

    return subdivisionCollection.findOne({ apm_id: entity.apm_id, id: entity.subdiv_id });
  }

  /**
   * @param {*} providerSnapshot - Mongoose collection for provider snapshot
   * @param {*} entitySnapshot - Mongoose collection for provider snapshot
   * @param {*} modelSnapshot - Mongoose collection for provider snapshot
   * @param {*} qppYear - qpp year snapshot is being created for
   * @param {*} snapshotDate - datetime stamp of when snapshot is created
   * @param {*} subdivisionSnapshot - Mongoose collection for subdivision snapshot
   */
  static async addRelationshipCode(
    providerSnapshot,
    entitySnapshot,
    modelSnapshot,
    qppYear,
    snapshotDate,
    subdivisionSnapshot
  ) {
    const models = await modelSnapshot.find({}).lean();
    const providerCursor = providerSnapshot
      .find({ removed: { $ne: true } })
      .lean()
      .cursor();

    const updates = [];
    await providerCursor.eachAsync(async (provider) => {
      const entity = await entitySnapshot.findOne({ id: provider.entity_id, remove: { $ne: true } }).lean();

      if (!entity) {
        return;
      }
      const model = models.find(m => m.id === entity.apm_id);
      const subdivsion = await SnapshotService.getSubdivisionFromEntity(entity, subdivisionSnapshot);

      updates.push({
        id: provider._id,
        update: {
          relationship_code: SnapshotService.determineRelationshipCode(
            provider,
            entity,
            model,
            qppYear,
            snapshotDate,
            subdivsion
          )
        }
      });

      if (updates.length > 200) {
        await SnapshotService.updateRecords(providerSnapshot, updates);
        updates.length = 0;
      }
    });

    if (updates.length > 0) {
      await SnapshotService.updateRecords(providerSnapshot, updates);
      updates.length = 0;
    }
  }
}

module.exports = SnapshotService;
