/* eslint-disable max-len */
const schema = require('../../../schemas/EntityEligibility');
const DynamicPACEntity = require('../../mongoose/dynamic_name_models/pac_entity');
const DynamicPacSubdivision = require('../../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPacModel = require('../../mongoose/dynamic_name_models/pac_model');
const rules = require('../rules');
const pacPublishHistoryUtils = require('../../pacPublishHistoryUtils');

const validateList = async (entityEligibility, validate) => {
  const validationResults = [];
  const entityIds = entityEligibility.map(eligibility => eligibility.entity_id);
  const performanceYear = entityEligibility.length > 0 ? entityEligibility[0].performance_year : null;
  const { pacEntityFile,
    pacSubdivisionFile,
    pacModelFile } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);

  const entity = pacEntityFile ?
    await DynamicPACEntity(pacEntityFile.file_id).find({ id: { $in: entityIds } }).lean() : [];

  const mipsSubdivsForEntity = pacSubdivisionFile ?
    await DynamicPacSubdivision(pacSubdivisionFile.file_id).find({ id: { $in: entity.map(p => p.subdiv_id) } }).lean() : [];

  const mipsModelsForEntity = pacModelFile ?
    await DynamicPacModel(pacModelFile.file_id).find({ id: { $in: entity.map(p => p.apm_id) } }).lean() : [];

  const validMipsStatus = ['P', 'Y'];
  entityEligibility.forEach((eligibility) => {
    const pacEntityDep = entity.find(e => e.id === eligibility.entity_id && e.performance_year === eligibility.performance_year);
    const mipsValidations = {};
    mipsValidations.entity =
      (pacEntityDep ? mipsSubdivsForEntity.find(e => (e.id === pacEntityDep.subdiv_id && validMipsStatus.includes(e.mips_apm_flag))) : null) ||
      (pacEntityDep ? mipsModelsForEntity.find(e => (e.id === pacEntityDep.apm_id && validMipsStatus.includes(e.mips_apm_flag))) : null);

    const dependencies = {
      entity: pacEntityDep,
      mipsValidations
    };

    const validationResult = validate(eligibility, schema, rules.entity_eligibility, dependencies);

    const update = {
      invalid: !validationResult.isValid,
      validations: [...validationResult.hardValidationFails, ...validationResult.softValidationFails]
    };

    validationResults.push({
      id: eligibility._id,
      update
    });
  });

  return validationResults;
};

module.exports = {
  validateList
};
