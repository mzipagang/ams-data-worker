const PACModel = require('../../../../app/services/mongoose/models/pac_model');
const PACSubdivision = require('../../../../app/services/mongoose/models/pac_subdivision');
const EntitySourceCollection = require('../../../../app/services/mongoose/models/source_file_entity_data');
const IDRProviderValidTins = require('../../../../app/services/mongoose/models/provider_valid_tins');
const EntitySchema = require('../../../schemas/Entity');
const rules = require('../rules');
const Promise = require('bluebird');

async function getSubdivisions(collection, key) {
  const keys = collection.map(row => ({
    id: row[key] || '00',
    apm_id: row.apm_id
  }));

  return keys.length > 0 ? [...await PACSubdivision.find({ $or: keys }).lean()] : [];
}

async function validateList(entities, validate) {
  const apmIds = entities.map(entity => entity.apm_id);
  const npis = entities.map(entity => entity.npi);
  const models = [...await PACModel.find({ id: { $in: apmIds } }).lean()];
  const subdivisions = await getSubdivisions(entities, 'subdiv_id');

  const validNpis = npis.length > 0 ?
    [...await IDRProviderValidTins.find({ npi: { $in: npis }, deleted: false }).lean()] : [];
  const validTins = apmIds.length > 0 ?
    [...await IDRProviderValidTins.find({ validTinPrvdr: { $in: apmIds }, deleted: false }).lean()] : [];

  const validationResults = [];
  await Promise.all(entities.map(entity => (async function validations() {
    const model = models.find(m => m.id === entity.apm_id);
    const subdivision = subdivisions.find(subiv =>
      subiv.id === entity.subdiv_id && subiv.apm_id === entity.apm_id);
    const entityValidNpis = validNpis.find(p => p.npi === entity.npi);
    const entityValidTins = validTins.find(p => p.validTinPrvdr === entity.tin);

    const validationResult = validate(entity, EntitySchema, rules.entity, {
      model,
      subdivision,
      invalidEntityIdCount: await EntitySourceCollection
        .countDocuments({ id: entity.id, invalid: false, apm_id: { $ne: entity.apm_id } }),
      entityValidTins,
      entityValidNpis
    });

    const update = {
      invalid: !validationResult.isValid,
      validations: [...validationResult.hardValidationFails, ...validationResult.softValidationFails]
    };

    validationResults.push({
      id: entity._id,
      update
    });
  })()));

  return validationResults;
}

module.exports = {
  validateList
};
