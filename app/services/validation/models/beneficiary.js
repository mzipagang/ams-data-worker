const Entity = require('../../../../app/services/mongoose/models/apm_entity');
const PACModel = require('../../../../app/services/mongoose/models/pac_model');
const BeneficiarySchema = require('../../../schemas/Beneficiary');
const rules = require('../rules');

class BeneficiaryDataValidation {
  static isManualBeneficiaryFile(beneficiaryData) {
    return !!beneficiaryData.entity_id && !beneficiaryData.program_org_id;
  }

  static async getValidationDataForManualBeneficiary(beneficiaryData) {
    const entityIds = beneficiaryData.map(beneficiary => beneficiary.entity_id);
    const entities = [...(await Entity.find({ id: { $in: entityIds } }).lean())];
    return {
      entities
    };
  }

  static async getValidationDataForMdmBeneficiary(beneficiaryData) {
    const entityIds = beneficiaryData
      .filter(beneficiary => beneficiary.program_org_id)
      .map(beneficiary => beneficiary.program_org_id);
    const entities = [...(await Entity.find({ id: { $in: entityIds } }).lean())];
    const apmIds = beneficiaryData.map(beneficiary => beneficiary.program_id);
    const models = [...(await PACModel.find({ id: { $in: apmIds } }).lean())];
    return {
      entities,
      models
    };
  }

  static async validateList(beneficiaries, validate) {
    const isManualBeneficiaryFile = BeneficiaryDataValidation.isManualBeneficiaryFile(
      beneficiaries[0]
    );

    const beneficiaryValidationCache = isManualBeneficiaryFile
      ? await BeneficiaryDataValidation.getValidationDataForManualBeneficiary(beneficiaries)
      : await BeneficiaryDataValidation.getValidationDataForMdmBeneficiary(beneficiaries);

    const validationResults = [];
    beneficiaries.forEach((beneficiary) => {
      // TODO: come up with way to get header info to better differentiate file types
      const validationData = {};
      let validationRule;

      if (isManualBeneficiaryFile) {
        validationData.entity = beneficiaryValidationCache.entities.find(
          entity => entity.id === beneficiary.entity_id
        );
        validationRule = rules.beneficiary;
      } else {
        validationData.entity = beneficiaryValidationCache.entities.find(
          entity => entity.id === beneficiary.program_org_id
        );
        validationData.model = beneficiaryValidationCache.models.find(
          model => model.id === beneficiary.program_id
        );
        validationRule = rules.beneficiary_mdm;
      }

      // TODO: come up with way to get header info to better differentiate file types
      const validationResult = validate(
        beneficiary,
        BeneficiarySchema,
        validationRule,
        validationData
      );

      const update = {
        invalid: !validationResult.isValid,
        validations: [
          ...validationResult.hardValidationFails,
          ...validationResult.softValidationFails
        ]
      };

      validationResults.push({
        id: beneficiary._id,
        update
      });
    });

    return validationResults;
  }
}

module.exports = BeneficiaryDataValidation;
