const DynamicPACEntity = require('../../mongoose/dynamic_name_models/pac_entity');
const QpcScoreSummarySchema = require('../../../schemas/PacQualityPerformanceCategoryScoreSummary');
const rules = require('../rules');
const pacPublishHistoryUtils = require('../../pacPublishHistoryUtils');

class QpcScoreValidation {
  static async validateList(qpcScoreSummaryRecords, validate) {
    const performanceYear = qpcScoreSummaryRecords.length > 0 ? qpcScoreSummaryRecords[0].performance_year : null;
    const entityIds = qpcScoreSummaryRecords.map(qpcScoreSummaryRecord => qpcScoreSummaryRecord.entity_id);
    const { pacEntityFile } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);
    const entities =  pacEntityFile ? await DynamicPACEntity(pacEntityFile.file_id).find({ id: { $in: entityIds } }).lean() : [];

    const validationResults = [];
    qpcScoreSummaryRecords.forEach((qpcScoreSummaryRecord) => {
      const entity = entities.find(e => e.id === qpcScoreSummaryRecord.entity_id);

      const validationResult = validate(
        qpcScoreSummaryRecord,
        QpcScoreSummarySchema,
        rules.qpc_score_summary,
        { entity }
      );

      const update = {
        invalid: !validationResult.isValid,
        validations: [
          ...validationResult.hardValidationFails,
          ...validationResult.softValidationFails
        ]
      };

      validationResults.push({
        id: qpcScoreSummaryRecord._id,
        update
      });
    });

    return validationResults;
  }
}

module.exports = QpcScoreValidation;
