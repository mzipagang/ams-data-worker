module.exports = {
  'apm-entity': require('./apm_entity'),
  'apm-provider': require('./apm_provider'),
  individual_qp_status: require('./individual_qp_status'),
  individual_qp_threshold: require('./individual_qp_threshold'),
  'entity-eligibility': require('./entity_eligibility'),
  entity_qp_status: require('./entity_qp_status'),
  entity_qp_threshold: require('./entity_qp_threshold'),
  beneficiary: require('./beneficiary'),
  beneficiary_mdm: require('./beneficiary'),
  'pac-qp_category_score': require('./qpc_score'),
  'pac-qp_category_score_summary': require('./qpc_score_summary')
};
