const EntityQpThresholdSchema = require('../../../schemas/IndividualQpThreshold');
const rules = require('../rules');
const Promise = require('bluebird');
const pacPublishHistoryUtils = require('../../pacPublishHistoryUtils');
const DynamicPacProvider = require('../../mongoose/dynamic_name_models/pac_provider');

const validateList = (individualQpThresholds, validate) => Promise.coroutine(function* validateIndividualQpThreshold() {
  const validationResults = [];
  const npis = individualQpThresholds.map(t => t.npi);
  const performanceYear = individualQpThresholds.length > 0 ? individualQpThresholds[0].performance_year : null;
  const { pacProviderFile } = yield pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);

  const provider = pacProviderFile ?
    yield DynamicPacProvider(pacProviderFile.file_id).find({ npi: { $in: npis } }).lean() : [];

  individualQpThresholds.forEach((individualQpThreshold) => {
    const dependencies = {
      provider: provider.find(p => p.npi === individualQpThreshold.npi
        && p.performance_year === individualQpThreshold.performance_year)
    };

    const validationResult =
      validate(individualQpThreshold, EntityQpThresholdSchema, rules.individual_qp_threshold, dependencies);

    const update = {
      invalid: !validationResult.isValid,
      validations: [...validationResult.hardValidationFails, ...validationResult.softValidationFails]
    };

    validationResults.push({
      id: individualQpThreshold._id,
      update
    });
  });

  return validationResults;
})();

module.exports = {
  validateList
};
