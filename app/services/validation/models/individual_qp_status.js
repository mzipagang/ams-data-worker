const EntityQpThresholdSchema = require('../../../schemas/IndividualQpStatus');
const rules = require('../rules');
const Promise = require('bluebird');
const pacPublishHistoryUtils = require('../../pacPublishHistoryUtils');
const DynamicPacProvider = require('../../mongoose/dynamic_name_models/pac_provider');

const validateList = (individualQpStatuses, validate) => Promise.coroutine(function* validateIndividualQpStatus() {
  const validationResults = [];
  const npis = individualQpStatuses.map(status => status.npi);
  const performanceYear = individualQpStatuses.length > 0 ? individualQpStatuses[0].performance_year : null;
  const { pacProviderFile } = yield pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);

  const provider = pacProviderFile ?
    yield DynamicPacProvider(pacProviderFile.file_id).find({ npi: { $in: npis } }).lean() : [];

  individualQpStatuses.forEach((individualQpStatus) => {
    const dependencies = {
      provider: provider.find(p => p.npi === individualQpStatus.npi
      && p.performance_year === individualQpStatus.performance_year)
    };

    const validationResult =
      validate(individualQpStatus, EntityQpThresholdSchema, rules.individual_qp_status, dependencies);

    const update = {
      invalid: !validationResult.isValid,
      validations: [...validationResult.hardValidationFails, ...validationResult.softValidationFails]
    };

    validationResults.push({
      id: individualQpStatus._id,
      update
    });
  });

  return validationResults;
})();

module.exports = {
  validateList
};
