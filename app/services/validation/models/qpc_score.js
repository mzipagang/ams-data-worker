const DynamicPACEntity = require('../../mongoose/dynamic_name_models/pac_entity');
const QpcScoreSchema = require('../../../schemas/PacQualityPerformanceCategoryScore');
const rules = require('../rules');
const pacPublishHistoryUtils = require('../../pacPublishHistoryUtils');

class QpcScoreValidation {
  static async validateList(qpcScoreRecords, validate) {
    const performanceYear = qpcScoreRecords.length > 0 ? qpcScoreRecords[0].performance_year : null;
    const entityIds = qpcScoreRecords.map(qpcScoreRecord => qpcScoreRecord.entity_id);
    const { pacEntityFile } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);
    const entities =  pacEntityFile ? await DynamicPACEntity(pacEntityFile.file_id).find({ id: { $in: entityIds } }).lean() : [];

    const validationResults = [];
    qpcScoreRecords.forEach((qpcScoreRecord) => {
      const entity = entities.find(e => e.id === qpcScoreRecord.entity_id);

      const validationResult = validate(
        qpcScoreRecord,
        QpcScoreSchema,
        rules.qpc_score,
        { entity }
      );

      const update = {
        invalid: !validationResult.isValid,
        validations: [
          ...validationResult.hardValidationFails,
          ...validationResult.softValidationFails
        ]
      };

      validationResults.push({
        id: qpcScoreRecord._id,
        update
      });
    });

    return validationResults;
  }
}

module.exports = QpcScoreValidation;
