/* eslint-disable max-len */
const EntityQpThresholdSchema = require('../../../schemas/EntityQpThreshold');
const pacPublishHistoryUtils = require('../../pacPublishHistoryUtils');
const DynamicPacModel = require('../../mongoose/dynamic_name_models/pac_model');
const DynamicPacEntity = require('../../mongoose/dynamic_name_models/pac_entity');
const DynamicPacProvider = require('../../mongoose/dynamic_name_models/pac_provider');
const DynamicPacSubdivision = require('../../mongoose/dynamic_name_models/pac_subdivision');
const rules = require('../rules');

const validateList = async (entityQpThresholds, validate) => {
  const validationResults = [];
  const performanceYear = entityQpThresholds[0].performance_year;
  const { pacProviderFile,
    pacEntityFile,
    pacSubdivisionFile,
    pacModelFile } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);

  const apmIds = entityQpThresholds.map(t => t.apm_id);
  const pacModel = pacModelFile ?
    await DynamicPacModel(pacModelFile.file_id).find({ id: { $in: apmIds } }).lean() : [];

  const subdivisionIds = entityQpThresholds.map(t => t.subdivision_id);
  const pacSubdivision = pacSubdivisionFile ?
    await DynamicPacSubdivision(pacSubdivisionFile.file_id).find({ id: { $in: subdivisionIds } }).lean() : [];

  const entityIds = entityQpThresholds.map(t => t.entity_id);
  const pacEntity = pacEntityFile ?
    await DynamicPacEntity(pacEntityFile.file_id).find({ id: { $in: entityIds } }).lean() : [];

  const mipsSubdivsForEntity = pacSubdivisionFile ?
    await DynamicPacSubdivision(pacSubdivisionFile.file_id).find({ id: { $in: pacEntity.map(p => p.subdiv_id) } }).lean() : [];

  const mipsModelsForEntity = pacModelFile ?
    await DynamicPacModel(pacModelFile.file_id).find({ id: { $in: pacEntity.map(p => p.apm_id) } }).lean() : [];

  const tins = entityQpThresholds.map(t => t.tin);
  const pacProviderTin = pacProviderFile ?
    await DynamicPacProvider(pacProviderFile.file_id).find({ tin: { $in: tins } }).lean() : [];
  const mipsEntitiesForTins = pacEntityFile ?
    await DynamicPacEntity(pacEntityFile.file_id).find({ id: { $in: pacProviderTin.map(n => n.entity_id) } }).lean() : [];
  const mipsSubdivsForTins = pacSubdivisionFile ?
    await DynamicPacSubdivision(pacSubdivisionFile.file_id).find({ id: { $in: mipsEntitiesForTins.map(n => n.subdiv_id) } }).lean() : [];
  const mipsModelsForTins = pacModelFile ?
    await DynamicPacModel(pacModelFile.file_id).find({ id: { $in: mipsEntitiesForTins.map(n => n.apm_id) } }).lean() : [];
  const npis = entityQpThresholds.map(t => t.npi);
  const pacProviderNpi = pacProviderFile ?
    await DynamicPacProvider(pacProviderFile.file_id).find({ npi: { $in: npis } }).lean() : [];
  const mipsEntitiesForNpis = pacEntityFile ?
    await DynamicPacEntity(pacEntityFile.file_id).find({ id: { $in: pacProviderNpi.map(n => n.entity_id) } }).lean() : [];
  const mipsSubdivsForNpis = pacSubdivisionFile ?
    await DynamicPacSubdivision(pacSubdivisionFile.file_id).find({ id: { $in: mipsEntitiesForNpis.map(n => n.subdiv_id) } }).lean() : [];
  const mipsModelsForNpis = pacModelFile ?
    await DynamicPacModel(pacModelFile.file_id).find({ id: { $in: mipsEntitiesForNpis.map(n => n.apm_id) } }).lean() : [];

  const validMipsStatus = ['P', 'Y'];
  entityQpThresholds.forEach((entityQpThreshold) => {
    const pacProviderTinDep = pacProviderTin.find(p => p.tin === entityQpThreshold.tin
      && p.performance_year === entityQpThreshold.performance_year);

    if (entityQpThreshold.tin) {
      entityQpThreshold.tinDecrypted = entityQpThreshold.tin;
    }

    const pacEntityDep = pacEntity.find(p => p.id === entityQpThreshold.entity_id
      && p.performance_year === entityQpThreshold.performance_year);
    const pacProviderNpiDep = pacProviderNpi.find(p => p.npi === entityQpThreshold.npi
  && p.performance_year === entityQpThreshold.performance_year);

    const mipsValidations = {};

    mipsValidations.entity =
      (pacEntityDep ? mipsSubdivsForEntity.find(e => (e.id === pacEntityDep.subdiv_id && validMipsStatus.includes(e.mips_apm_flag))) : null) ||
      (pacEntityDep ? mipsModelsForEntity.find(e => (e.id === pacEntityDep.apm_id && validMipsStatus.includes(e.mips_apm_flag))) : null);

    const entityForTin = mipsEntitiesForTins && pacProviderTinDep ? mipsEntitiesForTins.find(e => e.id === pacProviderTinDep.entity_id) : null;
    mipsValidations.tin =
      (entityForTin ? mipsSubdivsForTins.find(e => (e.id === entityForTin.subdiv_id && validMipsStatus.includes(e.mips_apm_flag))) : null) ||
      (entityForTin ? mipsModelsForTins.find(e => (e.id === entityForTin.apm_id && validMipsStatus.includes(e.mips_apm_flag))) : null);

    const entityForNpi = mipsEntitiesForNpis && pacProviderNpiDep ? mipsEntitiesForNpis.find(e => e.id === pacProviderNpiDep.entity_id) : null;
    mipsValidations.npi =
      (entityForNpi ? mipsSubdivsForNpis.find(e => (e.id === entityForNpi.subdiv_id && validMipsStatus.includes(e.mips_apm_flag))) : null) ||
      (entityForNpi ? mipsModelsForNpis.find(e => (e.id === entityForNpi.apm_id && validMipsStatus.includes(e.mips_apm_flag))) : null);

    const dependencies = {
      pacModel: pacModel.find(p => p.id === entityQpThreshold.apm_id
                && p.performance_year === entityQpThreshold.performance_year),
      pacSubdivision: pacSubdivision.find(p => p.id === entityQpThreshold.subdivision_id
                && p.performance_year === entityQpThreshold.performance_year),
      pacEntity: pacEntityDep,
      pacProviderTin: pacProviderTinDep,
      pacProviderNpi: pacProviderNpiDep,
      mipsValidations
    };

    const validationResult = validate(entityQpThreshold, EntityQpThresholdSchema, rules.entity_qp_threshold, dependencies);

    const update = {
      invalid: !validationResult.isValid,
      validations: [...validationResult.hardValidationFails, ...validationResult.softValidationFails]
    };

    validationResults.push({
      id: entityQpThreshold._id,
      update
    });
  });

  return validationResults;
};

module.exports = {
  validateList
};
