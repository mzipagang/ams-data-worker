const ApmEntity = require('../../../../app/services/mongoose/models/apm_entity');
const EntitySourceCollection = require('../../../../app/services/mongoose/models/source_file_entity_data');
const IDRProviderValidTins = require('../../../../app/services/mongoose/models/provider_valid_tins');
const ProviderSchema = require('../../../../app/schemas/Provider');
const rules = require('../../../../app/services/validation/rules');
const Promise = require('bluebird');

function validateList(providers, validate) {
  return Promise.coroutine(function* validateProviderData() {
    const entityKeys = providers.map(provider => provider.entity_id);
    const npis = providers.map(provider => provider.npi);
    const tins = providers.map(provider => `${provider.tin} `);

    const validNpis = npis.length > 0 ?
      [...yield IDRProviderValidTins.find({ npi: { $in: npis }, deleted: false }).lean()] : [];
    const validTins = tins.length > 0 ?
      [...yield IDRProviderValidTins.find({ validTinPrvdr: { $in: tins }, deleted: false }).lean()] : [];

    const entities = [...yield ApmEntity.find({ id: { $in: entityKeys } }).lean()];
    const validationResults = [];

    yield Promise.all(providers.map(provider => Promise.coroutine(function* validations() {
      const entity = entities.find(e => e.id === provider.entity_id);
      const providerValidTins = validTins.find(p => p.validTinPrvdr === `${provider.tin} `);
      const providerValidNpis = validNpis.find(p => p.npi === provider.npi);

      const validationResult = validate(provider, ProviderSchema, rules.provider, {
        entity,
        invalidEntityIdCount: yield EntitySourceCollection
          .countDocuments({ id: provider.entity_id, invalid: false, apm_id: { $ne: provider.apm_id } }),
        providerValidTins,
        providerValidNpis
      });
      const update = {
        invalid: !validationResult.isValid,
        validations: [...validationResult.hardValidationFails, ...validationResult.softValidationFails]
      };
      validationResults.push({
        id: provider._id,
        update
      });
    })()));

    return validationResults;
  })();
}

module.exports = {
  validateList
};
