/* eslint-disable max-len */
const EntityQpThresholdSchema = require('../../../schemas/EntityQpStatus');
const pacPublishHistoryUtils = require('../../pacPublishHistoryUtils');
const DynamicPacModel = require('../../mongoose/dynamic_name_models/pac_model');
const DynamicPacProvider = require('../../mongoose/dynamic_name_models/pac_provider');
const DynamicPacSubdivision = require('../../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPacEntity = require('../../mongoose/dynamic_name_models/pac_entity');
const rules = require('../rules');

const validateList = async (entityQpStatuses, validate) => {
  const validationResults = [];

  const performanceYear = entityQpStatuses[0].performance_year;
  const { pacProviderFile,
    pacSubdivisionFile,
    pacModelFile,
    pacEntityFile } = await pacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(performanceYear);

  const apmIds = entityQpStatuses.map(t => t.apm_id);
  const pacModel = pacModelFile ?
    await DynamicPacModel(pacModelFile.file_id).find({ id: { $in: apmIds } }).lean() : [];

  const subdivisionIds = entityQpStatuses.map(t => t.subdivision_id);
  const pacSubdivision = pacSubdivisionFile ?
    await DynamicPacSubdivision(pacSubdivisionFile.file_id).find({ id: { $in: subdivisionIds } }).lean() : [];

  const npis = entityQpStatuses.map(t => t.npi);
  const pacProviderNpi = pacProviderFile ?
    await DynamicPacProvider(pacProviderFile.file_id).find({ npi: { $in: npis } }).lean() : [];
  const mipsEntitiesForNpis = pacEntityFile ?
    await DynamicPacEntity(pacEntityFile.file_id).find({ id: { $in: pacProviderNpi.map(n => n.entity_id) } }).lean() : [];
  const mipsSubdivsForNpis = pacSubdivisionFile ?
    await DynamicPacSubdivision(pacSubdivisionFile.file_id).find({ id: { $in: mipsEntitiesForNpis.map(n => n.subdiv_id) } }).lean() : [];
  const mipsModelsForNpis = pacModelFile ?
    await DynamicPacModel(pacModelFile.file_id).find({ id: { $in: mipsEntitiesForNpis.map(n => n.apm_id) } }).lean() : [];

  const validMipsStatus = ['Y', 'P'];

  entityQpStatuses.forEach((entityQpStatus) => {
    const mipsValidations = {};
    const pacProviderNpiDep = pacProviderNpi.find(p => p.npi === entityQpStatus.npi
      && p.performance_year === entityQpStatus.performance_year);
    const entityForNpi = mipsEntitiesForNpis && pacProviderNpiDep ? mipsEntitiesForNpis.find(e => e.id === pacProviderNpiDep.entity_id) : null;
    mipsValidations.npi =
      (entityForNpi ? mipsSubdivsForNpis.find(e => (e.id === entityForNpi.subdiv_id && validMipsStatus.includes(e.mips_apm_flag))) : null) ||
      (entityForNpi ? mipsModelsForNpis.find(e => (e.id === entityForNpi.apm_id && validMipsStatus.includes(e.mips_apm_flag))) : null);

    const dependencies = {
      pacModel: pacModel.find(p => p.id === entityQpStatus.apm_id
        && p.performance_year === entityQpStatus.performance_year),
      pacSubdivision: pacSubdivision.find(p => p.id === entityQpStatus.subdivision_id
        && p.performance_year === entityQpStatus.performance_year),
      pacProviderNpi: pacProviderNpiDep,
      mipsValidations

    };
    const validationResult = validate(entityQpStatus, EntityQpThresholdSchema, rules.entity_qp_status, dependencies);

    const update = {
      invalid: !validationResult.isValid,
      validations: [...validationResult.hardValidationFails, ...validationResult.softValidationFails]
    };

    validationResults.push({
      id: entityQpStatus._id,
      update
    });
  });

  return validationResults;
};

module.exports = {
  validateList
};
