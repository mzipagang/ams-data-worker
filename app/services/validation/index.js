const Promise = require('bluebird');
const FileImportGroup = require('../mongoose/models/file_import_group');
const FileImportError = require('../mongoose/models/file_import_error');
const { summarizeValidations} = require('./helper');
const Joi = require('joi');
const ValidationResult = require('./ValidationResult');
const AmsShared = require('ams-shared');
const { ConvertToErrorCodes } = require('../../schemas');
/**
 * For validations there are three components:
 * 1) Model - each file type has a model, which contains a validateList function.
 * The validateList function takes two parameters: (array of file data, function to execute all rules on a single data
 * row)
 * The main purpose of this function is to get all the necessary dependencies for validation, including data from other
 * dependent collections, and executes all the rules on every object in the array
 * 2) Rules Config - each file type has their own set of rules, and each rule has:
 *   ruleName {string} - name for rule
 *   type {string} HARD_VALIDATION, SOFT_VALIDATION - type of validation
 *   errorCode {string} - given from business, references a specific rule in the AMS-Shared repo under the validations
 *     folder
 *   isActive {boolean} - toggles whether the rule is active or inactive
 * 3) Rule - each file type has functions for each rule listed in the rules config file.
 * This function is executed within the Model, and the cache object (if needed) is put together in the Model as well.
 * Each rule function contains four parameters:
 *   data {object} - data row object,
 *   errorCode {string} - error code which comes from the rules config,
 *   type {string} - type of validation error which comes from rules config
 *   cache {object} - options object which contains dependencies or other data outside the data row object needed for
 *     validation
 *   @type {
 *     {
 *       'apm-entity': *,
 *       'apm-provider': *,
 *       individual_qp_status: *,
 *       individual_qp_threshold: *,
 *       'entity-eligibility': *,
 *       entity_qp_status: *,
 *       entity_qp_threshold: *,
 *       beneficiary: BeneficiaryDataValidation,
 *       beneficiary_mdm: BeneficiaryDataValidation
 *     }
 *   }
 */
const validationFunctions = require('./models');

const schemaModelValidation = (rowData, schemaModel, validationRules) => {
  const modelValidation = Joi.validate(rowData, schemaModel.Schema, { abortEarly: false, allowUnknown: true });

  if (modelValidation.error) {
    if (schemaModel.MapFieldNameToErrorRule) {
      const combinedErrorRules = [...validationRules.hardValidations, ...validationRules.softValidations];
      const errorCodes = ConvertToErrorCodes(modelValidation, schemaModel.MapFieldNameToErrorRule, combinedErrorRules);
      return errorCodes.map(errorCode => new ValidationResult(false, errorCode, 'HARD_VALIDATION'));
    }

    // This was causing double-error messages due to the validation process not completely using Joi. There were
    // conflicting error messages between Joi and the custom validations written in the `services/validation/rules`
    // directory.
    // TODO: Implement all validations within Joi and apply custom error messages to their failures.
    // return [new ValidationResult(false, modelValidation.error.toString(), 'HARD_VALIDATION')];
  }
  return [new ValidationResult(true)];
};

/**
 * Function used to execute all rules on a single data row/object, and separates the results of the hard and soft validations
 * @param rowData
 * @param schemaModel
 * @param validationRules
 * @param dependencies
 * @returns {{isValid: boolean, softValidationFails: T[], hardValidationFails: T[]}}
 */
const validate = (rowData, schemaModel, validationRules, dependencies) => {
  const hardValidations = validationRules.hardValidations;
  const softValidations = validationRules.softValidations;

  const hardValidationResults = hardValidations.map(rule =>
    rule.ruleFunction(rowData, rule.errorCode || rule.description, rule.type, dependencies)
  );
  hardValidationResults.push(...schemaModelValidation(rowData, schemaModel, validationRules));

  const softValidationResults = softValidations.map(rule =>
    rule.ruleFunction(rowData, rule.errorCode || rule.description, rule.type, dependencies)
  );

  const hardValidationFails = hardValidationResults.filter(result => !result.isValid);
  const softValidationFails = softValidationResults.filter(result => !result.isValid);

  return {
    isValid: hardValidationFails.length === 0,
    softValidationFails,
    hardValidationFails
  };
};

const processValidationsResults = Promise.coroutine(function* processValidations(collectionModel, validationData) {
  const bulk = collectionModel.collection.initializeUnorderedBulkOp();

  validationData.forEach((result) => {
    bulk
      .find({
        _id: result.id
      })
      .updateOne({ $set: result.update });
  });
  if (validationData.length > 0) {
    yield new Promise((resolve, reject) => {
      bulk.execute((err, result) => {
        if (err && !result) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }
});

const getValidations = async (collectionModels, file) => {
  const collectionModelsArray = Array.isArray(collectionModels) ? collectionModels : [collectionModels];
  return collectionModelsArray.reduce(async (acc, model) => {
    const validations = await acc;
    const currentValidations = await model.find({ file_id: file.id }).lean();
    return Promise.resolve(validations.concat(currentValidations));
  }, Promise.resolve([]));
};

const updateValidationSummary = (collectionModel, file) =>
  Promise.coroutine(function* updateSummary() {

    const validations = yield getValidations(collectionModel, file);
    const FileImportErrors = yield FileImportError.find({ file_id: file.id }).lean();

    if (validations.length > 0) {
      const update = {
        'files.$.validations': summarizeValidations(validations, FileImportErrors)
      };
      yield FileImportGroup.findOneAndUpdate({ 'files.id': { $eq: file.id } }, { $set: update });
    }
  })();

  const validateCollection = async (model, fileType, file) => {
    await validateCollectionWithoutSummary(model, fileType, file);
    await updateValidationSummary(model, file);
  };

const validateCollectionWithoutSummary = async (model, fileType, file) => {
  const cursor = model
      .find({ file_id: file.id })
      .lean()
      .cursor();
    const validationFunction = validationFunctions[fileType].validateList;

    const modelData = [];
    const processData = async (data) => {
      modelData.push(data);
      if (modelData.length > 200) {
        await processValidationsResults(model, await validationFunction(modelData, validate, file.id), file);
        modelData.length = 0;
      }
    };
    await cursor.eachAsync(processData);

    if (modelData.length > 0) {
      await processValidationsResults(model, await validationFunction(modelData, validate, file.id), file);
      modelData.length = 0;
    }
};

const validateMultipleTypesInSingleFileAndSummarize = async (modelAndTypeObjectArray, file) => {
  const validationPromises = modelAndTypeObjectArray.map(({ collection, type }) => validateCollectionWithoutSummary(collection, type, file));
  await Promise.all(validationPromises);

  const models = modelAndTypeObjectArray.map(({ collection }) => collection);
  await updateValidationSummary(models, file);
}

const applyValidationHierarchy = (validations, fileType) => {
  const newValidations = [];

  validations.forEach((validation) => {
    const currentError = AmsShared.validations[fileType].errorCodes[validation.errorCode];

    if (currentError) {
      const indexToReplace = newValidations.findIndex(
        storedError => storedError.errorMetadata.groupCode === currentError.groupCode
      );

      if (indexToReplace === -1) {
        newValidations.push({
          error_type: validation.validationType,
          error: currentError.message,
          error_code: validation.errorCode,
          errorMetadata: currentError
        });
      } else if (currentError.errorPriority <= newValidations[indexToReplace].errorMetadata.errorPriority) {
        newValidations[indexToReplace] = {
          error_type: validation.validationType,
          error: currentError.message,
          error_code: validation.errorCode,
          errorMetadata: currentError
        };
      }
    }
  });

  return newValidations;
};

module.exports = {
  validate,
  validateCollection,
  validateCollectionWithoutSummary,
  applyValidationHierarchy,
  validateMultipleTypesInSingleFileAndSummarize,
  updateValidationSummary
};
