class ValidationResult {
  constructor(isValid, errorCode, validationType) {
    this.isValid = isValid || false;
    this.errorCode = errorCode;
    this.validationType = validationType;
  }
}

module.exports = ValidationResult;
