const summarizeValidations = (records, fileImportErrors) => {
  let validationCount = 0;
  const validationTypes = {
    error: 'HARD_VALIDATION',
    warning: 'SOFT_VALIDATION'
  };
  const perFileValidationsByType = {
    HARD_VALIDATION: {},
    SOFT_VALIDATION: {}
  };
  const summary = {};
  summary.totalRecords = records.length;
  summary.warningRecordCount = 0;
  summary.errorRecordCount = 0;
  summary.validRecordCount = 0;

  records.forEach((res) => {
    if (res.validations) {
      validationCount += res.validations.length;

      if (res.invalid) {
        summary.errorRecordCount += 1;
      }
      if (res.validations.find(v => v.validationType === validationTypes.warning)) {
        summary.warningRecordCount += 1;
      } else if (!res.validations.find(v => v.validationType === validationTypes.error)) {
        summary.validRecordCount += 1;
      }

      res.validations.forEach((validation) => {
        if (!perFileValidationsByType[validation.validationType][validation.errorCode]) {
          perFileValidationsByType[validation.validationType][validation.errorCode] = {
            validationType: validation.validationType,
            rowsAffected: 0
          };
        }
        perFileValidationsByType[validation.validationType][validation.errorCode].rowsAffected += 1;
      });
    } else {
      summary.validRecordCount += 1;
    }
  });

  fileImportErrors.forEach((error) => {
    if (!perFileValidationsByType.HARD_VALIDATION[error.error]) {
      perFileValidationsByType.HARD_VALIDATION[error.error] = { validationType: 'HARD_VALIDATION', rowsAffected: 0 };
    }
    perFileValidationsByType.HARD_VALIDATION[error.error].rowsAffected += 1;
    summary.errorRecordCount += 1;
    validationCount += 1;
    summary.totalRecords += 1;
  });

  summary.totalErrorCount = validationCount;
  summary.validationErrors = {
    HARD_VALIDATION: Object.keys({ ...perFileValidationsByType.HARD_VALIDATION }).map(error => ({
      error,
      type: perFileValidationsByType.HARD_VALIDATION[error].validationType,
      rowsAffected: perFileValidationsByType.HARD_VALIDATION[error].rowsAffected
    })),
    SOFT_VALIDATION: Object.keys({ ...perFileValidationsByType.SOFT_VALIDATION }).map(error => ({
      error,
      type: perFileValidationsByType.SOFT_VALIDATION[error].validationType,
      rowsAffected: perFileValidationsByType.SOFT_VALIDATION[error].rowsAffected
    }))
  };

  return summary;
};

module.exports = {
  summarizeValidations
};
