const moment = require('moment');
const ValidationResult = require('../ValidationResult');

module.exports = {
  rules: {
    program_id_valid: (data, errorCode, type, cache) =>
      new ValidationResult(!!cache.model && cache.model.id === data.program_id, errorCode, type),
    program_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.program_id, errorCode, type),
    program_org_id_valid: (data, errorCode, type, cache) =>
      new ValidationResult(
        !!cache.entity && cache.entity.id === data.program_org_id,
        errorCode,
        type
      ),
    entity_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.entity_id, errorCode, type),
    entity_id_valid: (data, errorCode, type, cache) =>
      new ValidationResult(!!cache.entity && cache.entity.id === data.entity_id, errorCode, type),
    start_date_required: (data, errorCode, type) =>
      new ValidationResult(!!data.start_date, errorCode, type),
    start_date_valid_date: (data, errorCode, type) => {
      // TODO: ask Phil if there is an instance where we have an invalid date
      const res = new ValidationResult(true, errorCode, type);

      const startDate = moment.utc(data.start_date);
      if (!data.start_date || !startDate.isValid()) {
        res.isValid = false;
      }
      return res;
    },
    start_date_before_end_date: (data, errorCode, type) => {
      const res = new ValidationResult(false, errorCode, type);

      if (moment.utc(data.start_date).isSameOrBefore(moment.utc(data.end_date))) {
        res.isValid = true;
      }
      return res;
    },
    start_date_after_entity_start_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const entity = cache.entity;
      if (!!entity && !moment.utc(data.start_date).isSameOrAfter(moment.utc(entity.start_date))) {
        res.isValid = false;
      }

      return res;
    },
    start_date_before_entity_end_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const entity = cache.entity;
      if (!!entity && !moment.utc(data.start_date).isSameOrBefore(moment.utc(entity.end_date))) {
        res.isValid = false;
      }

      return res;
    },
    end_date_required: (data, errorCode, type) =>
      new ValidationResult(!!data.end_date, errorCode, type),
    end_date_valid_date: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);

      const endDate = moment.utc(data.end_date);
      if (!data.end_date || !endDate.isValid()) {
        res.isValid = false;
      }
      return res;
    },
    end_date_after_start_date: (data, errorCode, type) => {
      const res = new ValidationResult(false, errorCode, type);

      if (moment.utc(data.end_date).isSameOrAfter(moment.utc(data.start_date))) {
        res.isValid = true;
      }

      return res;
    },
    end_date_before_entity_end_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const entity = cache.entity;
      if (entity && !moment.utc(data.end_date).isSameOrBefore(moment.utc(entity.end_date))) {
        res.isValid = false;
      }

      return res;
    },
    end_date_after_entity_start_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const entity = cache.entity;
      if (entity && !moment.utc(data.end_date).isSameOrAfter(moment.utc(entity.start_date))) {
        res.isValid = false;
      }
      return res;
    },
    beneficiary_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.id, errorCode, type),
    beneficiary_id_length: (data, errorCode, type) =>
      new ValidationResult(!!data.id && data.id.length === 10, errorCode, type),
    bene_link_key_required: (data, errorCode, type) =>
      new ValidationResult(!!data.link_key, errorCode, type),
    bene_link_key_length: (data, errorCode, type) =>
      new ValidationResult(data.link_key.length === 10, errorCode, type),
    hicn_length: (data, errorCode, type) =>
      new ValidationResult(!data.hicn || data.hicn.length === 11, errorCode, type),
    mbi_length: (data, errorCode, type) =>
      new ValidationResult(!data.mbi || data.mbi.length === 11, errorCode, type),
    dob_valid: (data, errorCode, type) =>
      new ValidationResult(!!data.dob || data.dob === undefined, errorCode, type),
    gender_valid: (data, errorCode, type) =>
      new ValidationResult(!data.gender || /[012]/.test(data.gender), errorCode, type)
  }
};
