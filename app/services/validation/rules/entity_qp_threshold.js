const ValidationResult = require('../ValidationResult');

module.exports = {
  rules: {
    apm_id_exists_in_pac_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const pacModel = cache.pacModel;
      if (!pacModel || pacModel.id !== data.apm_id) {
        res.isValid = false;
      }
      return res;
    },
    apm_id_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacModel = cache.pacModel;

      if (pacModel) {
        // TODO: Use these rules, when we switch to validate off of the snapshot collections, instead of PAC collections

        /*        const startDateValid = moment.utc(pacModel.start_date).year() === data.performance_year;
                      const endDateValid = moment.utc(pacModel.end_date).year() === data.performance_year;
                      const dateRangeValid =
                                moment.utc(pacModel.start_date).year() < data.performance_year &&
                                moment.utc(pacModel.end_date).year() > data.performance_year;
                      if (startDateValid || endDateValid || dateRangeValid) {
                        res.isValid = true;
                      } */
        res.isValid = data.performance_year === pacModel.performance_year;
      }
      return res;
    },

    apm_id_participant_or_partial_mips_apm: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacModel = cache.pacModel;
      const validMipsCodes = ['Y', 'P'];
      if (pacModel && validMipsCodes.includes(pacModel.mips_apm_flag)) {
        res.isValid = true;
      }
      return res;
    },
    subdivision_id_exists_in_pac_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const pacSubdivision = cache.pacSubdivision;
      if (!pacSubdivision || pacSubdivision.id !== data.subdivision_id) {
        res.isValid = false;
      }
      return res;
    },
    subdivision_id_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacSubdivision = cache.pacSubdivision;

      if (pacSubdivision) {
        // TODO: Use these rules, when we switch to validate off of the snapshot collections, instead of PAC collections
        /*        const startDateValid = moment.utc(pacSubdivision.start_date).year() === data.performance_year;
                      const endDateValid = moment.utc(pacSubdivision.end_date).year() === data.performance_year;
                      const dateRangeValid =
                                moment.utc(pacSubdivision.start_date).year() < data.performance_year &&
                                moment.utc(pacSubdivision.end_date).year() > data.performance_year;
                      if (startDateValid || endDateValid || dateRangeValid) {
                        res.isValid = true;
                      } */
        res.isValid = data.performance_year === pacSubdivision.performance_year;
      }
      return res;
    },

    subdivision_id_participant_mips_subdivision: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacSubdivision = cache.pacSubdivision;
      const validMipsCodes = ['Y', 'P'];
      if (pacSubdivision && validMipsCodes.includes(pacSubdivision.mips_apm_flag)) {
        res.isValid = true;
      }
      return res;
    },

    entity_id_exists_in_pac_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const pacEntity = cache.pacEntity;
      if (!pacEntity || pacEntity.id !== data.entity_id) {
        res.isValid = false;
      }
      return res;
    },
    entity_id_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacEntity = cache.pacEntity;

      if (pacEntity) {
        // TODO: Use these rules, when we switch to validate off of the snapshot collections, instead of PAC collections
        /*        const startDateValid = moment.utc(pacEntity.start_date).year() === data.performance_year;
                      const endDateValid = moment.utc(pacEntity.end_date).year() === data.performance_year;
                      const dateRangeValid =
                                moment.utc(pacEntity.start_date).year() < data.performance_year &&
                                moment.utc(pacEntity.end_date).year() > data.performance_year;
                      if (startDateValid || endDateValid || dateRangeValid) {
                        res.isValid = true;
                      } */
        res.isValid = data.performance_year === pacEntity.performance_year;
      }
      return res;
    },

    entity_id_participant_mips_entity: (data, description, type, cache) =>
      new ValidationResult(!!cache.mipsValidations.entity, description, type),
    npi_10_characters: (data, description, type) => new ValidationResult(data.npi.length === 10, description, type),
    npi_exists_in_pac_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const pacProviderNpi = cache.pacProviderNpi;
      if (!pacProviderNpi || pacProviderNpi.npi !== data.npi) {
        res.isValid = false;
      }
      return res;
    },
    npi_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacProviderNpi = cache.pacProviderNpi;

      if (pacProviderNpi) {
        // if (pacProviderNpi && pacProviderNpi.start_date) {
        // TODO: Use these rules, when we switch to validate off of the snapshot collections, instead of PAC collections
        /*        const startDateValid = moment.utc(pacProviderNpi.start_date).year() === data.performance_year;
                      const endDateValid = moment.utc(pacProviderNpi.end_date).year() === data.performance_year;
                      const dateRangeValid =
                                moment.utc(pacProviderNpi.start_date).year() < data.performance_year &&
                                moment.utc(pacProviderNpi.end_date).year() > data.performance_year;
                      if (startDateValid || endDateValid || dateRangeValid) {
                        res.isValid = true;
                      } */
        res.isValid = data.performance_year === pacProviderNpi.performance_year;
      }
      return res;
    },

    npi_participant_mips_apm: (data, description, type, cache) =>
      new ValidationResult(!!cache.mipsValidations.npi, description, type),
    tin_9_characters: (data, description, type) =>
      new ValidationResult(data.tinDecrypted && data.tinDecrypted.length === 9, description, type),
    tin_exists_in_pac_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const pacProviderTin = cache.pacProviderTin;
      if (!pacProviderTin || pacProviderTin.tin !== data.tin) {
        res.isValid = false;
      }
      return res;
    },
    tin_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacProviderTin = cache.pacProviderTin;

      if (pacProviderTin) {
        // if (pacProviderTin && pacProviderTin.start_date) {
        // TODO: Use these rules, when we switch to validate off of the snapshot collections, instead of PAC collections
        /*        const startDateValid = moment.utc(pacProviderTin.start_date).year() === data.performance_year;
                      const endDateValid = moment.utc(pacProviderTin.end_date).year() === data.performance_year;
                      const dateRangeValid =
                                moment.utc(pacProviderTin.start_date).year() < data.performance_year &&
                                moment.utc(pacProviderTin.end_date).year() > data.performance_year;
                      if (startDateValid || endDateValid || dateRangeValid) {
                        res.isValid = true;
                      } */
        res.isValid = data.performance_year === pacProviderTin.performance_year;
      }
      return res;
    },

    tin_participant_mips_entity: (data, description, type, cache) =>
      new ValidationResult(!!cache.mipsValidations.tin, description, type),
    valid_qp_status: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.qp_status), description, type),
    valid_payment_threshold_met: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.payment_threshold_met), description, type),
    valid_patient_threshold_met: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.patient_threshold_met), description, type)
  }
};
