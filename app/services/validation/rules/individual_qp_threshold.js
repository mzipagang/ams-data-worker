const ValidationResult = require('../ValidationResult');

module.exports = {
  rules: {
    apm_id_blank_or_null: (data, description, type) => new ValidationResult(!data.apm_id, description, type),
    subdivision_id_blank_or_null: (data, description, type) =>
      new ValidationResult(!data.subdivision_id || data.subdivision_id === '00', description, type),
    entity_id_blank_or_null: (data, description, type) => new ValidationResult(!data.entity_id, description, type),
    tin_blank_or_null: (data, description, type) => new ValidationResult(!data.tin, description, type),
    npi_10_characters: (data, description, type) =>
      new ValidationResult(data.npi && data.npi.length === 10, description, type),
    npi_exists_in_master_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const provider = cache.provider;
      if (!provider || provider.npi !== data.npi) {
        res.isValid = false;
      }
      return res;
    },
    npi_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const provider = cache.provider;

      if (provider) {
        // These rules are for when validating off of the "Master" collections
        // const startDateValid =
        //   provider.participation.filter(p => moment.utc(p.start_date).year() === data.performance_year)[0];
        // const endDateValid =
        //   provider.participation.filter(p => moment.utc(p.end_date).year() === data.performance_year)[0];
        // // for case when start_date year < PY and end_date year > PY (date range encompasses PY)
        // const dateRangeValid =  provider.participation.filter(p =>
        //   moment.utc(p.start_date).year() < data.performance_year &&
        //   moment.utc(p.end_date).year() > data.performance_year
        // )[0];
        // if (startDateValid || endDateValid || dateRangeValid) {
        //   res.isValid = true;
        // }

        res.isValid = data.performance_year === provider.performance_year;
      }
      return res;
    },
    // TODO: Rule is set to inactive until business rules clarified
    npi_participant_mips_apm: (data, description, type) => new ValidationResult(true, description, type),
    valid_qp_status: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.qp_status), description, type),
    valid_payment_threshold_met: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.payment_threshold_met), description, type),
    valid_patient_threshold_met: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.patient_threshold_met), description, type)
  }
};
