const config = {
  entity: require('./entity.json'),
  model: require('./model.json'),
  provider: require('./provider.json'),
  subdivision: require('./subdivision.json'),
  individual_qp_status: require('./individual_qp_status'),
  individual_qp_threshold: require('./individual_qp_threshold'),
  entity_eligibility: require('./entity_eligibility'),
  entity_qp_status: require('./entity_qp_status'),
  entity_qp_threshold: require('./entity_qp_threshold'),
  beneficiary: require('./beneficiary.json'),
  beneficiary_mdm: require('./beneficiary_mdm'),
  beneficiary_base: require('./beneficiary_base'),
  qpc_score: require('./qpc_score.json'),
  qpc_score_summary: require('./qpc_score_summary.json')
};

module.exports = config;
