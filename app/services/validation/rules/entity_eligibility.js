const _ = require('lodash');
const ValidationResult = require('../ValidationResult');

module.exports = {
  rules: {
    entity_id_exists_master_collection: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);
      const entity = cache.entity;
      if (!entity || entity.id !== data.entity_id) {
        res.isValid = false;
      }
      return res;
    },
    entity_id_active_during_py: (data, errorCode, type, cache) => {
      const res = new ValidationResult(false, errorCode, type);
      const entity = cache.entity;

      if (entity) {
        // TODO: Use these rules, when we switch to validate off of the snapshot collections, instead of PAC collections
        // const startYear = moment.utc(entity.start_date).year();
        // const endYear = moment.utc(entity.end_date).year();
        //
        // if ((startYear === data.performance_year || endYear === data.performance_year)
        //   || (startYear < data.performance_year && endYear > data.performance_year)) {
        //   res.isValid = true;
        // }

        res.isValid = data.performance_year === entity.performance_year;
      }
      return res;
    },
    // TODO: Rule is set to inactive until business rules clarified
    entity_id_participant_in_mips: (data, errorCode, type, cache) =>
      new ValidationResult(!!cache.mipsValidations.entity, errorCode, type),
    claims_types_valid: (data, errorCode, type) => {
      const isValid = !!data.claims_types && ['BOTH', 'PHYS', 'CAH2'].includes(data.claims_types.toUpperCase());
      return new ValidationResult(isValid, errorCode, type);
    },
    lv_status_code: (data, errorCode, type) => {
      const result = new ValidationResult(true, errorCode, type);
      result.isValid = !!data.lv_status_code &&
        (data.lv_status_code.toUpperCase() === 'Y' || data.lv_status_code.toUpperCase() === 'N');
      return result;
    },
    lv_status_reason_valid: (data, errorCode, type) => {
      let isValid = true;
      if (!!data.lv_status_code && data.lv_status_code.toUpperCase() === 'Y') {
        isValid = ['BENE', 'CHRG', 'BOTH'].includes(data.lv_status_reason.toUpperCase());
      }
      return new ValidationResult(isValid, errorCode, type);
    },
    lv_status_reason_blank: (data, errorCode, type) => {
      let isValid = true;
      if (!!data.lv_status_code && data.lv_status_code.toUpperCase() === 'N') {
        isValid = !data.lv_status_reason;
      }
      return new ValidationResult(isValid, errorCode, type);
    },
    small_status_apm_entity_code: (data, errorCode, type) => {
      const result = new ValidationResult(true, errorCode, type);
      if (data.performance_year !== 2017) {
        result.isValid = !!data.small_status_apm_entity_code &&
          (data.small_status_apm_entity_code.toUpperCase() === 'Y'
            || data.small_status_apm_entity_code.toUpperCase() === 'N');
      }

      return result;
    },
    small_status_clinician_code: (data, errorCode, type) => {
      const result = new ValidationResult(true, errorCode, type);
      if (data.performance_year !== 2017) {
        result.isValid = !!data.small_status_clinician_count && !isNaN(data.small_status_clinician_count);
      }
      return result;
    },
    complex_patient_score: (data, errorCode, type) => {
      const result = new ValidationResult(true, errorCode, type);
      const complexPatientScore = parseInt(data.complex_patient_score);
      result.isValid = !!complexPatientScore
        && _.isNumber(complexPatientScore)
        && complexPatientScore >= 1
        && complexPatientScore <= 5;
      return result;
    }
  }
};
