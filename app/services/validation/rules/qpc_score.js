const ValidationResult = require('../ValidationResult');
const YesNoSchema = require('../../../schemas/YesNo').Schema;
const Joi = require('joi');

module.exports = {
  rules: {
    entity_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.entity_id, errorCode, type),
    entity_id_valid: (data, errorCode, type, cache) =>
      new ValidationResult(!!cache.entity && cache.entity.id === data.entity_id, errorCode, type),
    measure_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.measure_id, errorCode, type),
    measure_score_required: (data, errorCode, type) =>
      new ValidationResult(!!data.measure_score || data.measure_score === 0, errorCode, type),
    measure_score_number: (data, errorCode, type) => {
      const measureScore = parseFloat(data.measure_score);
      const isValidNumber = !isNaN(measureScore) && measureScore >= 0 && measureScore <= 999.99;
      return new ValidationResult(isValidNumber, errorCode, type);
    },
    decile_score_required: (data, errorCode, type) =>
      new ValidationResult(!!data.decile_score || data.decile_score === 0, errorCode, type),
    decile_score_number: (data, errorCode, type) => {
      const decileScore = parseFloat(data.decile_score);
      const isValidNumber = !isNaN(decileScore) && decileScore >= 0 && decileScore <= 999.99;
      return new ValidationResult(isValidNumber, errorCode, type);
    },
    meets_scoring_criteria_valid: (data, errorCode, type) =>
      new ValidationResult(!Joi.validate(data.meets_scoring_criteria, YesNoSchema).error, errorCode, type),
    high_priority_measure_valid: (data, errorCode, type) =>
      new ValidationResult(!Joi.validate(data.high_priority_measure, YesNoSchema).error, errorCode, type),
    cehrt_measure_valid: (data, errorCode, type) =>
      new ValidationResult(!Joi.validate(data.cehrt_measure, YesNoSchema).error, errorCode, type)
  }
};
