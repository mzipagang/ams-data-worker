const moment = require('moment');
const ValidationResult = require('../ValidationResult');
const validationHelpers = require('./helpers');
const TinNpiValidation = require('./helpers/tin_npi');

function isValidEntityType(entityType) {
  const types = ['ACO', 'CBO', 'PRA', 'OTH', 'HSP', 'MDC', 'PAC'];
  return types.includes((entityType || '').toUpperCase());
}

function isMedicareEntityType(entityType) {
  const types = ['PRA', 'HSP', 'MDC', 'PAC'];
  return types.includes((entityType || '').toUpperCase());
}

module.exports = {
  rules: {
    apm_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.apm_id, errorCode, type),
    apm_id_valid: (data, errorCode, type, cache) =>
      new ValidationResult(!!cache.model && !!cache.model.id, errorCode, type),
    subdiv_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.subdiv_id, errorCode, type),
    subdiv_id_valid: (data, errorCode, type, cache) =>
      new ValidationResult(
        (data.subdiv_id === '00') || (!!cache.subdivision && !!cache.subdivision.id), errorCode, type
      ),
    entity_id_required: (data, errorCode, type) =>
      new ValidationResult(!!data.id, errorCode, type),
    entity_id_unique: (data, errorCode, type, cache) =>
      new ValidationResult(cache.invalidEntityIdCount === 0, errorCode, type),
    start_date_required: (data, errorCode, type) =>
      new ValidationResult(!!data.start_date, errorCode, type),
    start_date_valid_date: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);

      const startDate = moment.utc(data.start_date);
      if (!data.start_date || !startDate.isValid()) {
        res.isValid = false;
      }
      return res;
    },
    start_date_before_end_date: (data, errorCode, type) => {
      const res = new ValidationResult(false, errorCode, type);

      if (moment.utc(data.start_date).isSameOrBefore(moment.utc(data.end_date).add(1, 'day'))) {
        res.isValid = true;
      }
      return res;
    },
    start_date_after_apm_start_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const apm = cache.model;
      if (!data.subdiv_id) {
        if (!!apm && !moment.utc(data.start_date).isSameOrAfter(moment.utc(apm.start_date))) {
          res.isValid = false;
        }
      }

      return res;
    },
    start_date_before_apm_end_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const apm = cache.model;
      if (!data.subdiv_id) {
        if (!!apm && !moment.utc(data.start_date).isBefore(moment.utc(apm.end_date))) {
          res.isValid = false;
        }
      }

      return res;
    },
    start_date_after_subdivision_start_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);
      if (data.subdiv_id) {
        const subdiv = cache.subdivision;
        if (!!subdiv && !moment.utc(data.start_date).isSameOrAfter(moment.utc(subdiv.start_date))) {
          res.isValid = false;
        }
      }
      return res;
    },
    start_date_before_subdivision_end_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);
      if (data.subdiv_id) {
        const subdiv = cache.subdivision;
        if (!!subdiv && !moment.utc(data.start_date).isBefore(moment.utc(subdiv.end_date))) {
          res.isValid = false;
        }
      }
      return res;
    },
    end_date_required: (data, errorCode, type) => new ValidationResult(!!data.end_date, errorCode, type),
    end_date_valid_date: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);

      const endDate = moment.utc(data.end_date);
      if (!data.end_date || !endDate.isValid()) {
        res.isValid = false;
      }
      return res;
    },
    end_date_after_start_date: (data, errorCode, type) => {
      const res = new ValidationResult(false, errorCode, type);

      if (moment.utc(data.end_date).isSameOrAfter(moment.utc(data.start_date).subtract(1, 'day'))) {
        res.isValid = true;
      }

      return res;
    },
    end_date_before_apm_end_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const apm = cache.model;
      if (!data.subdiv_id) {
        if (apm && !moment.utc(data.end_date).isSameOrBefore(moment.utc(apm.end_date))) {
          res.isValid = false;
        }
      }
      return res;
    },
    end_date_after_apm_start_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const apm = cache.model;
      if (!data.subdiv_id) {
        if (apm && !moment.utc(data.end_date).isSameOrAfter(moment.utc(apm.start_date).subtract(1, 'day'))) {
          res.isValid = false;
        }
      }
      return res;
    },
    end_date_before_subdivision_end_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);
      if (data.subdiv_id) {
        const subdiv = cache.subdivision;
        if (subdiv && !moment.utc(data.end_date).isSameOrBefore(moment.utc(subdiv.end_date))) {
          res.isValid = false;
        }
      }
      return res;
    },
    end_date_after_subdivision_start_date: (data, errorCode, type, cache) => {
      const res = new ValidationResult(true, errorCode, type);

      const subdiv = cache.subdivision;
      if (data.subdiv_id) {
        if (subdiv && !moment.utc(data.end_date).isSameOrAfter(moment.utc(subdiv.start_date).subtract(1, 'day'))) {
          res.isValid = false;
        }
      }
      return res;
    },
    entity_name: (data, errorCode, type) =>
      new ValidationResult(!!data.name, errorCode, type),
    entity_type: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = !data.type || isValidEntityType(data.type);
      return res;
    },
    entity_tin: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = !data.tin || data.tin.length === 9;
      return res;
    },
    tin_invalid: (data, errorCode, type, cache) => { // E6
      const results = TinNpiValidation.tinValidation(data.tin, cache.entityValidTins);
      return new ValidationResult(results.validTin, errorCode, type);
    },
    entity_npi: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = !data.npi || data.npi.length === 10;
      return res;
    },
    npi_invalid: (data, errorCode, type, cache) => { // F3
      const results = TinNpiValidation.npiValidation(data.npi, cache.entityValidNpis);
      return new ValidationResult(results.validNpi, errorCode, type);
    },
    entity_ccn: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = !data.ccn || data.ccn.length === 6;
      return res;
    },
    address_1_blank_if_medicare: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (isMedicareEntityType(data.type)) {
        res.isValid = !data.address.address_1;
      } else {
        res.isValid = true;
      }
      return res;
    },
    address_1_required: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = !!data.address.address_1;
      } else {
        res.isValid = true;
      }
      return res;
    },
    address_1_requires_city: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.address_1) ? !!data.address.city : true;
      }
      return res;
    },
    address_1_requires_state: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid =
          (data.address && data.address.address_1) ?
            !!validationHelpers.listOfStates[(data.address.state || '').toUpperCase()] : true;
      }
      return res;
    },
    address_1_requires_zip5: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = (data.address && data.address.address_1) ? !!data.address.zip5 : true;
      return res;
    },
    address_2_blank_if_medicare: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (isMedicareEntityType(data.type)) {
        res.isValid = !data.address.address_2;
      }
      return res;
    },
    address_2_requires_address_1: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.address_2) ? !!data.address.address_1 : true;
      }
      return res;
    },
    address_2_requires_city: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.address_2) ? !!data.address.city : true;
      }
      return res;
    },
    address_2_requires_valid_state: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid =
          (data.address && data.address.address_2) ?
            !!validationHelpers.listOfStates[(data.address.state || '').toUpperCase()] : true;
      }
      return res;
    },
    address_2_requires_zip5: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.address_2) ? !!data.address.zip5 : true;
      }
      return res;
    },
    city_blank_if_medicare: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (isMedicareEntityType(data.type)) {
        res.isValid = !data.address.city;
      } else {
        res.isValid = true;
      }
      return res;
    },
    city_required: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = !!data.address.city;
      } else {
        res.isValid = true;
      }
      return res;
    },
    city_requires_address_1: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.city) ? !!data.address.address_1 : true;
      }
      return res;
    },
    city_requires_valid_state: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.city) ?
          !!validationHelpers.listOfStates[(data.address.state || '').toUpperCase()] : true;
      }
      return res;
    },
    city_requires_zip: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.city) ? !!data.address.zip5 : true;
      }
      return res;
    },
    state_blank_if_medicare: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (isMedicareEntityType(data.type)) {
        res.isValid = !data.address.state;
      } else {
        res.isValid = true;
      }
      return res;
    },
    state_required: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = !!data.address.state;
      } else {
        res.isValid = true;
      }
      return res;
    },
    // state_not_2_characters: (data, errorCode, type) => {
    //   const res = new ValidationResult(true, errorCode, type);
    //   if (!isMedicareEntityType(data.type)) {
    //     res.isValid = (data.address && data.address.state) ? data.address.state.length === 2 : true;
    //   }
    //   return res;
    // },
    // {
    //   "ruleName": "state_not_2_characters",
    //   "ruleDisplayName": "state is 2 characters",
    //   "type": "SOFT_VALIDATION",
    //   "errorCode": "state is not 2 characters",
    //   "isActive": true
    // },
    state_requires_address_1: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.state) ? !!data.address.address_1 : true;
      }
      return res;
    },
    state_requires_city: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.state) ? !!data.address.city : true;
      }
      return res;
    },
    state_requires_zip5: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.state) ? !!data.address.zip5 : true;
      }
      return res;
    },
    state_requires_valid_code: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.state) ?
          !!validationHelpers.listOfStates[(data.address.state || '').toUpperCase()] : true;
      }
      return res;
    },
    zip5_blank_if_medicare: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (isMedicareEntityType(data.type)) {
        res.isValid = !data.address.zip5;
      } else {
        res.isValid = true;
      }
      return res;
    },
    zip5_required: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = !!data.address.zip5;
      } else {
        res.isValid = true;
      }
      return res;
    },
    zip5_is_not_5_characters: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip5) ? data.address.zip5.length === 5 : true;
      }
      return res;
    },
    zip5_requires_address_1: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip5) ? !!data.address.address_1 : true;
      }
      return res;
    },
    zip5_requires_city: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip5) ? !!data.address.city : true;
      }
      return res;
    },
    zip5_requires_valid_state: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip5) ?
          !!validationHelpers.listOfStates[(data.address.state || '').toUpperCase()] : true;
      }
      return res;
    },
    zip4_blank_if_medicare: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (isMedicareEntityType(data.type)) {
        res.isValid = !data.address.zip4;
      }
      return res;
    },
    zip4_not_4_characters: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip4) ? data.address.zip4.length === 4 : true;
      }
      return res;
    },
    zip4_requires_address_1: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip4) ? !!data.address.address_1 : true;
      }
      return res;
    },
    zip4_requires_city: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip4) ? !!data.address.city : true;
      }
      return res;
    },
    zip4_requires_valid_state: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      if (!isMedicareEntityType(data.type)) {
        res.isValid = (data.address && data.address.zip4) ?
          !!validationHelpers.listOfStates[(data.address.state || '').toUpperCase()] : true;
      }
      return res;
    },
    tin_npi_valid: (data, errorCode, type, cache) => { // H2
      const results = TinNpiValidation.tinNpiValidation(
        data.tin,
        data.npi,
        cache.entityValidTins,
        cache.entityValidNpis
      );

      return new ValidationResult(
        (results.employeeOfTin || results.billingReassignedToTin),
        errorCode,
        type
      );
    }
  }
};
