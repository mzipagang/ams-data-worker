const ValidationResult = require('../ValidationResult');

const isWholeNumber = (num) => Number.isInteger(num);
const between = (min, max) => (num) => num >= min && num <= max;

const between0and999point9 = between(0, 999.9);
const between0And99 = between(0, 99);
const between0And10 = between(0, 10);

module.exports = {
  rules: {
    entity_id_exists: (data, errorCode, type, cache) =>
      new ValidationResult(!!cache.entity && cache.entity.id === data.entity_id, errorCode, type),
    final_qpc_score_in_range: (data, errorCode, type) =>
      new ValidationResult(between0and999point9(data.final_qpc_score), errorCode, type),
    current_achievement_points_in_range: (data, errorCode, type) =>
      new ValidationResult(between0and999point9(data.current_achievement_points), errorCode, type),
    prior_achievement_points_in_range: (data, errorCode, type) =>
      new ValidationResult(between0and999point9(data.prior_achievement_points), errorCode, type),
    high_priority_bonus_points_in_range: (data, errorCode, type) =>
      new ValidationResult(between0And99(data.high_priority_bonus_points), errorCode, type),
    high_priority_bonus_points_is_whole: (data, errorCode, type) =>
      new ValidationResult(isWholeNumber(data.high_priority_bonus_points), errorCode, type),
    cehrt_reporting_bonus_points_in_range: (data, errorCode, type) =>
      new ValidationResult(between0And99(data.cehrt_reporting_bonus), errorCode, type),
    cehrt_reporting_bonus_points_is_whole: (data, errorCode, type) =>
      new ValidationResult(isWholeNumber(data.cehrt_reporting_bonus), errorCode, type),
    quality_improvement_bonus_points_in_range: (data, errorCode, type) =>
      new ValidationResult(between0And10(data.quality_improvement_points), errorCode, type),
    scored_measures_in_range: (data, errorCode, type) =>
      new ValidationResult(between0And99(data.scored_measures), errorCode, type),
    scored_measures_is_whole: (data, errorCode, type) =>
      new ValidationResult(isWholeNumber(data.scored_measures), errorCode, type),
    excluded_measures_in_range: (data, errorCode, type) =>
      new ValidationResult(between0And99(data.excluded_measures), errorCode, type),
    excluded_measures_is_whole: (data, errorCode, type) =>
      new ValidationResult(isWholeNumber(data.excluded_measures), errorCode, type),
    excluded_measures_pay_for_reporting_in_range: (data, errorCode, type) =>
      new ValidationResult(between0And99(data.excluded_measures_pay_for_reporting), errorCode, type),
    excluded_measures_pay_for_reporting_is_whole: (data, errorCode, type) =>
      new ValidationResult(isWholeNumber(data.excluded_measures_pay_for_reporting), errorCode, type),
    excluded_measures_case_size_in_range: (data, errorCode, type) =>
      new ValidationResult(between0And99(data.excluded_measures_case_size), errorCode, type),
    excluded_measures_case_size_is_whole: (data, errorCode, type) =>
      new ValidationResult(isWholeNumber(data.excluded_measures_case_size), errorCode, type)
  }
};
