class TinNpiHelper {
  static npiValidation(npi, providerValidNpis) {
    return {
      validNpi: !!providerValidNpis && providerValidNpis.npi === npi
    };
  }

  static tinValidation(tin, providerValidTins) {
    // Adding because the data from IDR is padded currently in the DB
    if (!!providerValidTins && providerValidTins.validTinPrvdr.length > 9) {
      tin = tin.padEnd(providerValidTins.validTinPrvdr.length);
    }
    return {
      validTin: !!providerValidTins && providerValidTins.validTinPrvdr === tin
    };
  }

  static tinNpiValidation(tin, npi, providerValidTins, providerValidNpis) {
    const npiValidationResult = TinNpiHelper.npiValidation(npi, providerValidNpis);
    const tinValidationResult = TinNpiHelper.tinValidation(tin, providerValidTins);

    return {
      validNpi: npiValidationResult.validNpi,
      validTin: tinValidationResult.validTin,
      billingReassignedToTin: !!providerValidNpis && providerValidNpis.validTinsReasgnmt.includes(tin),
      employeeOfTin: !!providerValidNpis && providerValidNpis.validTinsEmplr.includes(tin)
    };
  }
}

module.exports = TinNpiHelper;
