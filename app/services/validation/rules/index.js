const rulesConfig = require('./config');

const ruleFunctions = {
  entity: require('./entity'),
  provider: require('./provider'),
  individual_qp_status: require('./individual_qp_status'),
  individual_qp_threshold: require('./individual_qp_threshold'),
  entity_eligibility: require('./entity_eligibility'),
  entity_qp_status: require('./entity_qp_status'),
  entity_qp_threshold: require('./entity_qp_threshold'),
  beneficiary: require('./beneficiary'),
  qpc_score: require('./qpc_score'),
  qpc_score_summary: require('./qpc_score_summary')
};

module.exports = {
  entity: {
    hardValidations: rulesConfig.entity
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.entity
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity.rules[rule.ruleName]
        })
      )
  },
  provider: {
    hardValidations: rulesConfig.provider
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.provider.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.provider
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.provider.rules[rule.ruleName]
        })
      )
  },
  individual_qp_status: {
    hardValidations: rulesConfig.individual_qp_status
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.individual_qp_status.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.individual_qp_status
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.individual_qp_status.rules[rule.ruleName]
        })
      )
  },
  individual_qp_threshold: {
    hardValidations: rulesConfig.individual_qp_threshold
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.individual_qp_threshold.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.individual_qp_threshold
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.individual_qp_threshold.rules[rule.ruleName]
        })
      )
  },
  entity_eligibility: {
    hardValidations: rulesConfig.entity_eligibility
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity_eligibility.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.entity_eligibility
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity_eligibility.rules[rule.ruleName]
        })
      )
  },
  entity_qp_status: {
    hardValidations: rulesConfig.entity_qp_status
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity_qp_status.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.entity_qp_status
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity_qp_status.rules[rule.ruleName]
        })
      )
  },
  entity_qp_threshold: {
    hardValidations: rulesConfig.entity_qp_threshold
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity_qp_threshold.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.entity_qp_threshold
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.entity_qp_threshold.rules[rule.ruleName]
        })
      )
  },
  beneficiary: {
    hardValidations: rulesConfig.beneficiary_base
      .concat(rulesConfig.beneficiary)
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.beneficiary.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.beneficiary_base
      .concat(rulesConfig.beneficiary)
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.beneficiary.rules[rule.ruleName]
        })
      )
  },
  beneficiary_mdm: {
    hardValidations: rulesConfig.beneficiary_base
      .concat(rulesConfig.beneficiary_mdm)
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.beneficiary.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.beneficiary_base
      .concat(rulesConfig.beneficiary_mdm)
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.beneficiary.rules[rule.ruleName]
        })
      )
  },
  qpc_score: {
    hardValidations: rulesConfig.qpc_score
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.qpc_score.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.qpc_score
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.qpc_score.rules[rule.ruleName]
        })
      )
  },
  qpc_score_summary: {
    hardValidations: rulesConfig.qpc_score_summary
      .filter(config => config.type === 'HARD_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.qpc_score_summary.rules[rule.ruleName]
        })
      ),
    softValidations: rulesConfig.qpc_score_summary
      .filter(config => config.type === 'SOFT_VALIDATION' && config.isActive)
      .map(rule =>
        Object.assign({}, rule, {
          ruleFunction: ruleFunctions.qpc_score_summary.rules[rule.ruleName]
        })
      )
  }
};
