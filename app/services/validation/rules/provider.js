const moment = require('moment');
const ValidationResult = require('../ValidationResult');
const TinNpiValidation = require('./helpers/tin_npi');

module.exports = {
  rules: {
    entity_id_required: (data, errorCode, type) => { // B1
      const res = new ValidationResult(true, errorCode, type);
      if (!data.entity_id) {
        res.isValid = false;
        return res;
      }
      return res;
    },
    entity_id_unique: (data, errorCode, type, cache) =>// B2
      new ValidationResult(cache.invalidEntityIdCount === 0, errorCode, type),
    start_date_required: (data, errorCode, type) => { // C1
      const res = new ValidationResult(true, errorCode, type);
      if (!data.start_date) {
        res.isValid = false;
        return res;
      }
      return res;
    },
    start_date_valid: (data, errorCode, type) => { // C2
      const res = new ValidationResult(false, errorCode, type);
      const startDate = moment.utc(data.start_date);

      if (startDate.isValid() && (
        startDate.isSameOrAfter(moment.utc('2010-01-01')) && startDate.isSameOrBefore(moment.utc('2030-12-31'))
      )) {
        res.isValid = true;
        return res;
      }
      return res;
    },
    start_date_before_end_date: (data, errorCode, type) => { // C3
      const res = new ValidationResult(true, errorCode, type);
      if (!moment.utc(data.start_date).isSameOrBefore(moment.utc(data.end_date))) {
        res.isValid = false;
        return res;
      }
      return res;
    },
    start_date_after_entity_start_date: (data, errorCode, type, cache) => { // C4
      const res = new ValidationResult(true, errorCode, type);
      if (!!cache.entity) { // eslint-disable-line no-extra-boolean-cast
        if (!moment.utc(data.start_date).isSameOrAfter(moment.utc(cache.entity.start_date))) {
          res.isValid = false;
        }
      }
      return res;
    },
    start_date_before_entity_end_date: (data, errorCode, type, cache) => { // C5
      const res = new ValidationResult(true, errorCode, type);
      if (!!cache.entity && !moment.utc(data.start_date).isSameOrBefore(moment.utc(cache.entity.end_date))) {
        res.isValid = false;
      }
      return res;
    },
    end_date_required: (data, errorCode, type) => { // D1
      const res = new ValidationResult(true, errorCode, type);
      if (!data.end_date) {
        res.isValid = false;
        return res;
      }
      return res;
    },
    end_date_valid: (data, errorCode, type) => { // D2
      const res = new ValidationResult(false, errorCode, type);
      const endDate = moment.utc(data.end_date);

      if (endDate.isValid() && (
        (endDate.isSameOrAfter(moment.utc('2010-01-01')) && endDate.isSameOrBefore(moment.utc('2030-12-31'))
        ) || endDate.isSame(moment.utc('9999-12-31')))) {
        res.isValid = true;
        return res;
      }
      return res;
    },
    end_date_after_start_date: (data, errorCode, type) => { // D3
      const res = new ValidationResult(true, errorCode, type);
      if (!moment.utc(data.end_date).isSameOrAfter(moment.utc(data.start_date))) {
        res.isValid = false;
        return res;
      }
      return res;
    },
    end_date_before_entity_end_date: (data, errorCode, type, cache) => { // D4
      const res = new ValidationResult(true, errorCode, type);
      const endDate = moment.utc(data.end_date);
      if (!!cache.entity && !endDate.isSameOrBefore(moment.utc(cache.entity.end_date))) {
        res.isValid = false;
      }
      return res;
    },
    end_date_after_entity_start_date: (data, errorCode, type, cache) => { // D5
      const res = new ValidationResult(true, errorCode, type);
      if (!!cache.entity && !moment.utc(data.end_date).isSameOrAfter(moment.utc(cache.entity.start_date))) {
        res.isValid = false;
      }
      return res;
    },
    tin_required: (data, errorCode, type) =>// E1
      new ValidationResult(!!data.tin, errorCode, type),
    tin_length: (data, errorCode, type) => // E2
      new ValidationResult(!!data.tin && data.tin.length === 9, errorCode, type),
    tin_invalid: (data, errorCode, type, cache) => { // E3
      const results = TinNpiValidation.tinValidation(data.tin, cache.providerValidTins);
      return new ValidationResult(results.validTin, errorCode, type);
    },
    npi_required: (data, errorCode, type) => // F1
      new ValidationResult(!!data.npi, errorCode, type),
    npi_length: (data, errorCode, type) => { // F2
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = !!data.npi && data.npi.length === 10;
      return res;
    },
    npi_invalid: (data, errorCode, type, cache) => { // F3
      const results = TinNpiValidation.npiValidation(data.npi, cache.providerValidNpis);
      return new ValidationResult(results.validNpi, errorCode, type);
    },
    ccn_length: (data, errorCode, type) => { // G1
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = !data.ccn || (!!data.ccn && data.ccn.length === 6);
      return res;
    },
    tin_type_code_value: (data, errorCode, type) => {
      const res = new ValidationResult(true, errorCode, type);
      res.isValid = (!!data.tin_type_code && ['ITIN', 'EIN', 'SSN'].indexOf(data.tin_type_code) !== -1)
        || !data.tin_type_code;
      return res;
    },
    tin_npi_valid: (data, errorCode, type, cache) => { // H1
      const results = TinNpiValidation.tinNpiValidation(
        data.tin,
        data.npi,
        cache.providerValidTins,
        cache.providerValidNpis
      );

      return new ValidationResult(
        (results.employeeOfTin || results.billingReassignedToTin),
        errorCode,
        type
      );
    }
  }
};
