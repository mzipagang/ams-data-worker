const ValidationResult = require('../ValidationResult');

module.exports = {
  rules: {
    npi_10_characters: (data, description, type) =>
      new ValidationResult(data.npi && data.npi.length === 10, description, type),
    npi_exists_in_master_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const provider = cache.provider;
      if (!provider || provider.npi !== data.npi) {
        res.isValid = false;
      }
      return res;
    },
    npi_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const provider = cache.provider;

      if (provider) {
        // These rules are for when validating off of the "Master" collections
        // const startYear = moment.utc(provider.start_date).year();
        // const endYear = moment.utc(provider.end_date).year();
        //
        // if ((startYear === data.performance_year || endYear === data.performance_year)
        // || (startYear < data.performance_year && endYear > data.performance_year)) {
        //   res.isValid = true;
        // }
        res.isValid = data.performance_year === provider.performance_year;
      }
      return res;
    },
    // TODO: Rule is set to inactive until business rules clarified
    npi_participant_mips_apm: (data, description, type) => new ValidationResult(true, description, type),
    valid_qp_status: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.qp_status), description, type)
  }
};
