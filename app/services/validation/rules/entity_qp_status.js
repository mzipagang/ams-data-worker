const ValidationResult = require('../ValidationResult');

module.exports = {
  rules: {
    npi_10_characters: (data, description, type) => new ValidationResult(data.npi.length === 10, description, type),
    npi_exists_in_pac_collection: (data, description, type, cache) => {
      const res = new ValidationResult(true, description, type);
      const pacProviderNpi = cache.pacProviderNpi;
      if (!pacProviderNpi || pacProviderNpi.npi !== data.npi) {
        res.isValid = false;
      }
      return res;
    },
    npi_active_during_performance_year: (data, description, type, cache) => {
      const res = new ValidationResult(false, description, type);
      const pacProviderNpi = cache.pacProviderNpi;

      if (pacProviderNpi) {
      // if (pacProviderNpi && pacProviderNpi.start_date) {
        // TODO: Use these rules, when we switch to validate off of the snapshot collections, instead of PAC collections
        /*        const startDateValid = moment.utc(pacProviderNpi.start_date).year() === data.performance_year;
        const endDateValid = moment.utc(pacProviderNpi.end_date).year() === data.performance_year;
        const dateRangeValid =
                  moment.utc(pacProviderNpi.start_date).year() < data.performance_year &&
                  moment.utc(pacProviderNpi.end_date).year() > data.performance_year;
        if (startDateValid || endDateValid || dateRangeValid) {
          res.isValid = true;
        } */
        res.isValid = data.performance_year === pacProviderNpi.performance_year;
      }
      return res;
    },
    npi_participant_mips_apm: (data, description, type, cache) =>
      new ValidationResult(!!cache.mipsValidations.npi, description, type),
    valid_qp_status: (data, description, type) =>
      new ValidationResult(['Y', 'N', 'P'].includes(data.qp_status), description, type)
  }
};
