const Promise = require('bluebird');
const config = require('../../config');
const AWS = require('../aws');

const SQS = AWS.SQS;

class SQSMessageApi {
  static create(priority, jobName, body) {
    if ([1, 2].indexOf(priority) === -1) {
      throw new Error('Priority can only be values 1 to 2.');
    }

    const sendMessage = Promise.promisify(SQS.sendMessage.bind(SQS));
    const params = {
      DelaySeconds: 0,
      MessageBody: JSON.stringify({ jobName, body }),
      QueueUrl: `${config.AWS.SQS_REQUEST_QUEUE_URL}-${priority}`
    };

    return sendMessage(params);
  }
}

module.exports = SQSMessageApi.create;
module.exports.SQSMessageApi = SQSMessageApi;
