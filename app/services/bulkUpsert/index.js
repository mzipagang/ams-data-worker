const Promise = require('bluebird');

/* istanbul ignore next */ module.exports = Promise.coroutine(function* bulkUpsert(Model, datas, keyFn) {
  if (datas.length === 0) {
    return [];
  }

  const bulk = Model.collection.initializeUnorderedBulkOp();
  datas.forEach((data) => {
    if (keyFn) {
      bulk
        .find(keyFn(data))
        .upsert()
        .replaceOne(data);
    } else {
      bulk.insert(data);
    }
  });

  const bulkResult = yield new Promise((resolve, reject) => {
    bulk.execute((err, result) => {
      if (err && !result) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });

  const results = datas.map(() => ({ success: true }));
  if (bulkResult && bulkResult.getWriteErrors) {
    const bulkErrors = bulkResult.getWriteErrors().map(writeError => writeError.toJSON());

    if (bulkErrors.length > 0) {
      // Attempt a second time in case of race condition
      const bulkSecondAttempt = Model.collection.initializeUnorderedBulkOp();
      bulkErrors.forEach((writeError) => {
        const data = datas[writeError.index];
        if (keyFn) {
          bulkSecondAttempt
            .find(keyFn(data))
            .upsert()
            .replaceOne(data);
        } else {
          bulkSecondAttempt.insert(data);
        }
      });

      const bulkSecondAttemptResult = yield new Promise((resolve, reject) => {
        bulkSecondAttempt.execute((err, result) => {
          if (err && !result) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });

      if (bulkSecondAttemptResult && bulkSecondAttemptResult.getWriteErrors) {
        const bulkSecondAttemptErrors = bulkSecondAttemptResult.getWriteErrors().map(writeError => writeError.toJSON());
        bulkSecondAttemptErrors.forEach((writeError) => {
          const index = bulkErrors[writeError.index].index;
          Object.assign(results[index], { success: false });
        });
      }
    }
  }

  return results;
});
