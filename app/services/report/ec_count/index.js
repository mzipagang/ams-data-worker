const totalec = require('./totalec_query');
const totalECByAPM = require('./totalec_by_apm_query');
const totalECBySubdivision = require('./totalec_by_apm_subdivision_query');
const apms = require('../qp_metrics/apms');
const PastReport = require('../../../services/mongoose/models/past_report');

module.exports = {
  report: async (performanceYear, runSnapshot, date, bypassCache, snapshot) => {
    const query = {
      performance_year: performanceYear,
      run_snapshot: runSnapshot,
      ec_count: { $exists: true, $ne: null }
    };

    if (bypassCache !== '1') {
      if (date) {
        query.date = { $lte: date };
      }

      const cachedResult = await PastReport.findOne(query, {
        _id: false,
        __v: false,
        performance_year: false,
        run_snapshot: false,
        date: false,
        'ec_count._id': false,
        'ec_count.apms._id': false,
        'ec_count.mipsECbyAPM._id': false,
        'ec_count.mipsECbySubdivision._id': false,
        'ec_count.nonMipsECbyAPM._id': false,
        'ec_count.nonMipsECbySubdivision._id': false
      }).sort({ date: -1 });

      if (cachedResult) {
        return cachedResult.ec_count;
      }
    }

    const tasks = await Promise.all([
      apms.get(snapshot),
      totalec.get(snapshot),
      totalECByAPM(snapshot, ['Y', 'P'], true),
      totalECBySubdivision(snapshot, true),
      totalECByAPM(snapshot, ['N'], false),
      totalECBySubdivision(snapshot, false)
    ]);

    const result = {
      apms: tasks[0],
      totalec: tasks[1],
      mipsECbyAPM: tasks[2],
      mipsECbySubdivision: tasks[3],
      nonMipsECbyAPM: tasks[4],
      nonMipsECbySubdivision: tasks[5]
    };

    await new PastReport({
      performance_year: performanceYear,
      run_snapshot: runSnapshot,
      date,
      ec_count: result
    }).save();

    return result;
  }
};
