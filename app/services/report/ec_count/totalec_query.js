module.exports = {
  get: async (snapshot) => {
    const result = await snapshot.PacModel.aggregate([
      {
        $lookup: {
          from: snapshot.SubdivisionName,
          localField: 'id',
          foreignField: 'apm_id',
          as: 'subdivision'
        }
      },
      { $unwind: { path: '$subdivision', preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: snapshot.EntityName,
          localField: 'id',
          foreignField: 'apm_id',
          as: 'entity'
        }
      },
      {
        $unwind: '$entity'
      },

      {
        $addFields: {
          isValid: {
            $cond: {
              if: {
                $or: [{ $eq: ['$subdivision.id', '$entity.subdiv_id'] }, { $eq: ['$entity.subdiv_id', '00'] }]
              },
              then: 1,
              else: 0
            }
          }
        }
      },

      { $match: { isValid: 1 } },

      {
        $lookup: {
          from: snapshot.ProviderName,
          localField: 'entity.id',
          foreignField: 'entity_id',
          as: 'provider'
        }
      },

      {
        $unwind: '$provider'
      },
      {
        $project: {
          ec: { $concat: ['$provider.npi', '-', '$provider.tin'] }
        }
      },
      {
        $group: {
          _id: '$ec'
        }
      },
      { $count: 'total_number_ec' }
    ]);

    return result[0].total_number_ec;
  }
};
