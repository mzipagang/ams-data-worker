module.exports = async (snapshot, isMipsAPM) => snapshot.PacModel.aggregate([
  {
    $lookup: {
      from: snapshot.SubdivisionName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'subdivision'
    }
  },
  { $unwind: { path: '$subdivision', preserveNullAndEmptyArrays: false } },
  {
    $match: { 'subdivision.mips_apm_flag': isMipsAPM ? 'Y' : 'N' }
  },
  {
    $lookup: {
      from: snapshot.EntityName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'entity'
    }
  },
  {
    $unwind: '$entity'
  },

  {
    $addFields: {
      isValid: {
        $cond:
        {
          if: {
            $eq: ['$subdivision.id', '$entity.subdiv_id']
          },
          then: 1,
          else: 0
        }
      }
    }
  },
  { $match: { isValid: 1 } },
  {
    $lookup: {
      from: snapshot.ProviderName,
      localField: 'entity.id',
      foreignField: 'entity_id',
      as: 'provider'
    }

  },
  {
    $unwind: '$provider'
  },
  {
    $project:
    {
      ec: { $concat: ['$provider.npi', '-', '$provider.tin'] },
      apm_id: '$id',
      apm_name: '$name',
      subdiv_id: '$subdivision.id',
      subdiv_name: '$subdivision.name'

    }
  },
  {
    $group: {
      _id: { apm_id: '$apm_id', subdiv_id: '$subdiv_id', apm_name: '$apm_name', subdiv_name: '$subdiv_name' },
      unique_ecs: { $addToSet: '$ec' }
    }

  },
  {
    $project: {
      _id: 0,
      apm_id: '$_id.apm_id',
      subdiv_id: '$_id.subdiv_id',
      apm_name: '$_id.apm_name',
      subdiv_name: '$_id.subdiv_name',
      unique_ec_count: { $size: '$unique_ecs' }
    }
  },
  {
    $sort: { apm_name: 1, subdivision_id: -1 }
  }
]);
