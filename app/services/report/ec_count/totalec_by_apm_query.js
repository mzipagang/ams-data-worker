module.exports = async (snapshot, mipsApmFlagArray, isMipsApmFlag) => snapshot.PacModel.aggregate([
  {
    $addFields: {
      validModel: { $in: ['$mips_apm_flag', mipsApmFlagArray] }
    }
  },
  { $match: { validModel: true } },
  {
    $lookup: {
      from: snapshot.SubdivisionName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'subdivision'
    }

  },
  { $unwind: { path: '$subdivision', preserveNullAndEmptyArrays: true } },

  {
    $addFields: {
      isValidSubDivFlag: {
        $cond:
        {
          if: {
            $ne: ['$subdivision.mips_apm_flag', isMipsApmFlag ? 'N' : 'Y'] // We have to use the neq and the ! on isMipsApmFlag so we include apms that don't have a subdivison
          },
          then: 1,
          else: 0
        }
      }
    }
  },
  {
    $lookup: {
      from: snapshot.EntityName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'entity'
    }
  },
  {
    $unwind: '$entity'
  },
  {
    $addFields: {
      isValid: {
        $cond:
        {
          if: {
            $or: [
              {
                $and: [
                  { $eq: ['$subdivision.id', '$entity.subdiv_id'] },
                  { $eq: ['$isValidSubDivFlag', 1] }]
              },
              { $eq: ['$entity.subdiv_id', '00'] }
            ]
          },
          then: 1,
          else: 0
        }
      }
    }
  },
  { $match: { isValid: 1 } },
  {
    $lookup: {
      from: snapshot.ProviderName,
      localField: 'entity.id',
      foreignField: 'entity_id',
      as: 'provider'
    }
  },
  {
    $unwind: '$provider'
  },
  {
    $project:
    {
      ec: { $concat: ['$provider.npi', '-', '$provider.tin'] },
      apm: '$id',
      name: '$name'
    }
  },
  {
    $group: {
      _id: { apm: '$apm', name: '$name' },
      unique_ecs: { $addToSet: '$ec' }
    }
  },
  {
    $project: { _id: 0, apm_id: '$_id.apm', apm_name: '$_id.name', unique_ec_count: { $size: '$unique_ecs' } }
  },
  {
    $sort: { apm_name: 1 }
  }
]);
