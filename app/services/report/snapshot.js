const _ = require('lodash');
const PacSnapshots = require('../../services/mongoose/models/pac_snapshot');
const DynamicPACModel = require('../../services/mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../../services/mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../../services/mongoose/dynamic_name_models/pac_entity');
const DynamicPACProvider = require('../../services/mongoose/dynamic_name_models/pac_provider');
const DynamicPACQPStatus = require('../../services/mongoose/dynamic_name_models/pac_qp_status');
const { CORE_FILE_TYPES } = require('../../constants');
const logger = require('../../services/logger');

const getModelByType = (files, fileType) => {
  const obj = _.find(files, { type: fileType });

  if (!obj) {
    return null;
  }

  const fileid = obj.file_id;

  switch (fileType) {
    case 'pac-model':
      return DynamicPACModel(fileid);
    case 'pac-entity':
      return DynamicPACEntity(fileid);
    case 'pac-provider':
      return DynamicPACProvider(fileid);
    case 'pac-qp_status':
      return DynamicPACQPStatus(fileid);
    case 'pac-subdivision':
      return DynamicPACSubdivision(fileid);
    default:
      return null;
  }
};

const getModels = async (performanceYear, runSnapshot) => {
  const files = await PacSnapshots.find({ performance_year: performanceYear, run_snapshot: runSnapshot })
    .sort({ createdAt: -1 })
    .lean();

  const missingFiles = CORE_FILE_TYPES.filter(fileType => !files.some(file => file.type === fileType));

  if (missingFiles.length) {
    const error = `Missing file(s) for ${missingFiles.join(', ')}`;
    logger.warn(error);
    return { error };
  }

  const temp = {
    PacModel: getModelByType(files, 'pac-model'),
    Subdivision: getModelByType(files, 'pac-subdivision'),
    Entity: getModelByType(files, 'pac-entity'),
    Provider: getModelByType(files, 'pac-provider'),
    QPStatus: getModelByType(files, 'pac-qp_status')
  };

  temp.PacModelName = temp.PacModel.collection.name;
  temp.SubdivisionName = temp.Subdivision.collection.name;
  temp.EntityName = temp.Entity.collection.name;
  temp.ProviderName = temp.Provider.collection.name;
  temp.QPStatusName = temp.QPStatus.collection.name;

  return temp;
};

module.exports = {
  all: async () => PacSnapshots.find().lean(),
  byYear: async () =>
    PacSnapshots.aggregate([
      { $group: { _id: { performance_year: '$performance_year', run_snapshot: '$run_snapshot' } } },
      {
        $project: {
          _id: false,
          performance_year: '$_id.performance_year',
          run_snapshot: '$_id.run_snapshot'
        }
      }
    ]),
  getModels,
  getModelByType
};
