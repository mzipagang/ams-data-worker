const apms = require('./apms.js');
const totalQps = require('./total_number_of_qps');
const totalQpsByAPM = require('./total_number_of_qps_by_apm');
const totalQpsBySubDivision = require('./total_number_of_qps_by_subdivisions');
const totalPartialQpsByAPM = require('./total_number_of_partial_qps_by_apm');
const totalPartialQpsBySubdivision = require('./total_number_of_partial_qps_by_subdivision');
const totalNotQpsByAPM = require('./total_number_of_not_qps_by_apm');
const totalNotQpsBySubdivision = require('./total_number_of_not_qps_by_subdivision');
const _ = require('lodash');
const PastReport = require('../../../services/mongoose/models/past_report');

module.exports = {
  report: async (performanceYear, runSnapshot, date, bypassCache, snapshot) => {
    const query = {
      performance_year: performanceYear,
      run_snapshot: runSnapshot,
      qp_metrics: { $exists: true, $ne: null, $not: { $size: 0 } }
    };

    if (bypassCache !== '1') {
      if (date) {
        query.date = { $lte: date };
      }

      const cachedResult = await PastReport.findOne(query, {
        _id: false,
        __v: false,
        performance_year: false,
        run_snapshot: false,
        date: false,
        'qp_metrics._id': false
      }).sort({ date: -1 });

      if (cachedResult) {
        return cachedResult.qp_metrics;
      }
    }

    // We are going to execute all the qurries at the same time to try and speed up getting the result back to client.
    const tasks = await Promise.all([
      apms.get(snapshot),
      totalQps.get(snapshot),
      totalQpsByAPM.get(snapshot),
      totalQpsBySubDivision.get(snapshot),
      totalPartialQpsByAPM.get(snapshot),
      totalPartialQpsBySubdivision.get(snapshot),
      totalNotQpsByAPM.get(snapshot),
      totalNotQpsBySubdivision.get(snapshot)
    ]);

    // Take our completed tasks and put them into an object to make them eaiser to work with.
    const input = {
      apms: tasks[0],
      totalQps: tasks[1],
      totalQpsByAPM: tasks[2],
      totalQpsBySubDivision: tasks[3],
      totalPartialQpsByAPM: tasks[4],
      totalPartialQpsBySubdivision: tasks[5],
      totalNotQpsByAPM: tasks[6],
      totalNotQpsBySubdivision: tasks[7]
    };

    const output = [];

    const seenApms = [];

    input.apms.forEach((apm) => {
      if (!seenApms.includes(apm.apm_id) && apm.subdiv_id) {
        seenApms.push(apm.apm_id);

        const justModel = JSON.parse(JSON.stringify(apm));
        delete justModel.subdivision_name;
        delete justModel.subdiv_id;

        output.push(justModel);
        output.push(apm);
      } else {
        seenApms.push(apm.apm_id);
        output.push(apm);
      }
    });

    input.totalQpsByAPM.forEach((total) => {
      _.find(output, { apm_id: total.apm_id }).total_qp = total.unique_npi_count;
    });
    input.totalPartialQpsByAPM.forEach((total) => {
      _.find(output, { apm_id: total.apm_id }).total_partial_qp = total.unique_npi_count;
    });
    input.totalNotQpsByAPM.forEach((total) => {
      _.find(output, { apm_id: total.apm_id }).total_not_qp = total.unique_npi_count;
    });

    input.totalQpsBySubDivision.forEach((total) => {
      _.find(output, { apm_id: total.apm_id, subdiv_id: total.subdiv_id }).total_qp = total.unique_npi_count;
    });

    input.totalPartialQpsBySubdivision.forEach((total) => {
      _.find(output, { apm_id: total.apm_id, subdiv_id: total.subdiv_id }).total_partial_qp = total.unique_npi_count;
    });
    input.totalNotQpsBySubdivision.forEach((total) => {
      _.find(output, { apm_id: total.apm_id, subdiv_id: total.subdiv_id }).total_not_qp = total.unique_npi_count;
    });

    output.forEach((apm) => {
      if (!apm.total_qp) {
        apm.total_qp = 0;
      }
      if (!apm.total_partial_qp) {
        apm.total_partial_qp = 0;
      }
      if (!apm.total_not_qp) {
        apm.total_not_qp = 0;
      }
    });

    output.push({
      model_name: 'Total QP\'s',
      total_qp: input.totalQps && input.totalQps.length ? input.totalQps[0].total_number_qp : undefined
    });

    await new PastReport({
      performance_year: performanceYear,
      run_snapshot: runSnapshot,
      date,
      qp_metrics: output
    }).save();

    return output;
  },
  apms,
  totalQps,
  totalQpsByAPM,
  totalQpsBySubDivision,
  totalPartialQpsByAPM,
  totalPartialQpsBySubdivision,
  totalNotQpsByAPM,
  totalNotQpsBySubdivision
};
