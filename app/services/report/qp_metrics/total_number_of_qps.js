module.exports = {
  get: async snapshot => snapshot.PacModel.aggregate([
    {
      $lookup: {
        from: snapshot.SubdivisionName,
        localField: 'id',
        foreignField: 'apm_id',
        as: 'subdivision'
      }

    },
    { $unwind: { path: '$subdivision', preserveNullAndEmptyArrays: true } },
    {
      $lookup: {
        from: snapshot.EntityName,
        localField: 'id',
        foreignField: 'apm_id',
        as: 'entity'
      }
    },
    {
      $unwind: '$entity'
    },

    {
      $addFields: {
        isValid: {
          $cond:
          {
            if: {
              $or: [
                { $eq: ['$subdivision.id', '$entity.subdiv_id'] },
                { $eq: ['$subdivision.id', '00'] }
              ]
            },
            then: 1,
            else: 0
          }
        }
      }
    },

    { $match: { isValid: 1 } },

    {
      $lookup: {
        from: snapshot.ProviderName,
        localField: 'entity.id',
        foreignField: 'entity_id',
        as: 'provider'
      }

    },

    {
      $unwind: '$provider'
    },
    {
      $lookup: {
        from: snapshot.QPStatusName,
        localField: 'provider.npi',
        foreignField: 'npi',
        as: 'npi'
      }
    },
    {
      $unwind: '$npi'
    },
    {

      $project:
      {
        npi: '$npi.npi'

      }
    },
    {
      $group: {
        _id: '$npi'
      }
    },
    { $count: 'total_number_qp' }

  ])
};
