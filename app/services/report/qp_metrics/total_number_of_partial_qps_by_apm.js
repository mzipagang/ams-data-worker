const query = require('./totalqp_query.js');

module.exports = {
  get: async snapshot => query(snapshot, 'P')
};

