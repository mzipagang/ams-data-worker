module.exports = async (snapshot, qpStatus) => snapshot.PacModel.aggregate([
  {
    $match: {
      advanced_apm_flag: { $in: ['Y', 'P'] }
    }
  },
  {
    $lookup: {
      from: snapshot.SubdivisionName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'subdivision'
    }
  },
  { $unwind: { path: '$subdivision', preserveNullAndEmptyArrays: false } },
  {
    $match: { 'subdivision.advanced_apm_flag': 'Y' }
  },
  {
    $lookup: {
      from: snapshot.EntityName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'entity'
    }
  },
  {
    $unwind: '$entity'
  },

  {
    $addFields: {
      isValid: {
        $cond:
        {
          if: {
            $or: [
              { $eq: ['$subdivision.id', '$entity.subdiv_id'] },
              { $eq: ['$subdivision.id', '00'] }
            ]
          },
          then: 1,
          else: 0
        }
      }
    }
  },
  { $match: { isValid: 1 } },
  {
    $lookup: {
      from: snapshot.ProviderName,
      localField: 'entity.id',
      foreignField: 'entity_id',
      as: 'provider'
    }

  },
  {
    $unwind: '$provider'
  },
  {
    $match: { 'provider.provider_relationship_code': 'P' }
  },
  {
    $lookup: {
      from: snapshot.QPStatusName,
      localField: 'provider.npi',
      foreignField: 'npi',
      as: 'npi'
    }
  },
  {
    $unwind: '$npi'
  },
  {
    $match: { 'npi.qp_status': qpStatus }
  },
  {

    $project:
    {
      npi: '$npi.npi',
      apm_id: '$id',
      apm_name: '$name',
      subdiv_id: '$subdivision.id',
      subdiv_name: '$subdivision.name'

    }
  },
  {
    $group: {
      _id: { apm_id: '$apm_id', subdiv_id: '$subdiv_id', apm_name: '$apm_name', subdiv_name: '$subdiv_name' },
      unique_npis: { $addToSet: '$npi' }
    }

  },
  {
    $project: {
      _id: 0,
      apm_id: '$_id.apm_id',
      subdiv_id: '$_id.subdiv_id',
      apm_name: '$_id.apm_name',
      subdiv_name: '$_id.subdiv_name',
      unique_npi_count: { $size: '$unique_npis' }
    }
  },
  {
    $sort: { apm_name: 1, subdivision_id: -1 }
  }
]);
