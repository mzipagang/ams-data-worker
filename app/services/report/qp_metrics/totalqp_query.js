module.exports = async (snapshot, qpStatus) => snapshot.PacModel.aggregate([
  {
    $match: {
      advanced_apm_flag: { $in: ['Y', 'P'] }
    }
  },
  {
    $lookup: {
      from: snapshot.SubdivisionName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'subdivision'
    }

  },
  { $unwind: { path: '$subdivision', preserveNullAndEmptyArrays: true } },

  {
    $addFields: {
      isValidSubDivAdvAPMFlag: {
        $cond:
        {
          if: {
            $ne: ['$subdivision.advanced_apm_flag', 'N']
          },
          then: 1,
          else: 0
        }
      }
    }
  },

  { $match: { isValidSubDivAdvAPMFlag: 1 } },

  {
    $lookup: {
      from: snapshot.EntityName,
      localField: 'id',
      foreignField: 'apm_id',
      as: 'entity'
    }
  },
  {
    $unwind: '$entity'
  },

  {
    $addFields: {
      isValid: {
        $cond:
        {
          if: {
            $or: [
              { $eq: ['$subdivision.id', '$entity.subdiv_id'] },
              { $eq: ['$subdivision.id', '00'] }
            ]
          },
          then: 1,
          else: 0
        }
      }
    }
  },

  { $match: { isValid: 1 } },

  {
    $lookup: {
      from: snapshot.ProviderName,
      localField: 'entity.id',
      foreignField: 'entity_id',
      as: 'provider'
    }

  },

  {
    $unwind: '$provider'
  },
  {
    $match: { 'provider.provider_relationship_code': 'P' }
  },
  {
    $lookup: {
      from: snapshot.QPStatusName,
      localField: 'provider.npi',
      foreignField: 'npi',
      as: 'npi'
    }
  },
  {
    $unwind: '$npi'
  },
  {
    $match: { 'npi.qp_status': qpStatus }
  },
  {

    $project:
    {
      npi: '$npi.npi',
      apm: '$id',
      name: '$name'

    }
  },
  {
    $group: {
      _id: { apm: '$apm', name: '$name' },
      unique_npis: { $addToSet: '$npi' }
    }

  },
  {
    $project: { _id: 0, apm_id: '$_id.apm', apm_name: '$_id.name', unique_npi_count: { $size: '$unique_npis' } }
  },
  {
    $sort: { apm_name: 1 }
  }
]);
