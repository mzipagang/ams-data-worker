const query = require('./totalqp_subdivision_query.js');

module.exports = {
  get: async snapshot => query(snapshot, 'P')
};
