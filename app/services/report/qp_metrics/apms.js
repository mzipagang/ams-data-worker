module.exports = {
  get: async snapshot => snapshot.PacModel.aggregate([
    {
      $lookup: { from: snapshot.SubdivisionName, localField: 'id', foreignField: 'apm_id', as: 'subdivisions' }
    },
    {
      $unwind: { path: '$subdivisions', preserveNullAndEmptyArrays: true }
    },
    // we duplicate the name and subdivision name to maintain compatablity with the QP and EC metrics reports.
    {
      $project: {
        _id: 0,
        apm_id: '$id',
        apm_name: '$name',
        model_name: '$name',
        model_short_name: '$short_name',
        subdivision_name: '$subdivisions.name',
        subdiv_name: '$subdivisions.name',
        subdiv_id: '$subdivisions.id'
      }
    },
    {
      $sort: { model_name: 1, subdiv_id: 1 }
    }
  ])
};
