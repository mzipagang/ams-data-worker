const config = require('../../../app/config');
const AmsShared = require('ams-shared');

const FileImportSourceMap = {
  ...AmsShared.ams_models.FileImportSourceMap.PacModelsMap,
  ...(config.FEATURE_FLAGS.DISABLE_SOURCE_FILES ? {} : AmsShared.ams_models.FileImportSourceMap.SourceFilesMap)
};

const Models = {
  ...{
    'pac-model': {
      model: require('../mongoose/models/pac_model'),
      dynamic: require('../mongoose/dynamic_name_models/pac_model')
    },
    'pac-subdivision': {
      model: require('../mongoose/models/pac_subdivision'),
      dynamic: require('../mongoose/dynamic_name_models/pac_subdivision')
    },
    'pac-entity': {
      model: require('../mongoose/models/pac_entity'),
      dynamic: require('../mongoose/dynamic_name_models/pac_entity')
    },
    'pac-provider': {
      model: require('../mongoose/models/pac_provider'),
      dynamic: require('../mongoose/dynamic_name_models/pac_provider')
    },
    'pac-qp_score': {
      model: require('../mongoose/models/pac_qp_score'),
      dynamic: require('../mongoose/dynamic_name_models/pac_qp_score')
    },
    'pac-qp_status': {
      model: require('../mongoose/models/pac_qp_status'),
      dynamic: require('../mongoose/dynamic_name_models/pac_qp_status')
    },
    'pac-lvt': {
      model: require('../mongoose/models/pac_lvt'),
      dynamic: require('../mongoose/dynamic_name_models/pac_lvt')
    },
    abridged_qp_status: {
      model: require('../mongoose/models/abridged_qp_status'),
      dynamic: require('../mongoose/dynamic_name_models/abridged_qp_status')
    },
    abridged_qp_score: {
      model: require('../mongoose/models/abridged_qp_score'),
      dynamic: require('../mongoose/dynamic_name_models/abridged_qp_score')
    },
    abridged_lvt: {
      model: require('../mongoose/models/abridged_lvt'),
      dynamic: require('../mongoose/dynamic_name_models/abridged_lvt')
    },
    beneficiary: {
      model: require('../mongoose/models/beneficiary'),
      dynamic: require('../mongoose/dynamic_name_models/beneficiary')
    },
    'pac-qp_category_score': {
      model: require('../mongoose/models/pac_qp_category_score'),
      dynamic: require('../mongoose/dynamic_name_models/pac_qp_category_score'),
    }
  },
  ...(config.FEATURE_FLAGS.DISABLE_SOURCE_FILES
    ? {}
    : {
      'apm-entity': {
        merge: true,
        keyFn: data => ({
          id: data.id,
          apm_id: data.apm_id,
          subdiv_id: data.subdiv_id,
          start_date: data.start_date,
          end_date: data.end_date,
          tin: data.tin,
          npi: data.npi
        }),
        model: require('../mongoose/models/apm_entity'),
        dynamic: require('../mongoose/dynamic_name_models/apm_entity'),
        sourceFileData: require('../mongoose/models/source_file_entity_data')
      },
      'apm-provider': {
        merge: true,
        keyFn: data => ({
          entity_id: data.entity_id,
          tin: data.tin,
          npi: data.npi
        }),
        model: require('../mongoose/models/apm_provider'),
        dynamic: require('../mongoose/dynamic_name_models/apm_provider'),
        sourceFileData: require('../mongoose/models/source_file_provider_data')
      },
      'entity-eligibility': {
        merge: true,
        keyFn: data => ({
          entity_id: data.entity_id
        }),
        model: require('../mongoose/models/entity_eligibility'),
        dynamic: require('../mongoose/dynamic_name_models/entity_eligibility'),
        sourceFileData: require('../mongoose/models/source_file_entity_eligibility')
      },
      individual_qp_status: {
        merge: true,
        keyFn: data => ({
          npi: data.npi
        }),
        model: require('../mongoose/models/individual_qp_status'),
        dynamic: require('../mongoose/dynamic_name_models/individual_qp_status'),
        sourceFileData: require('../mongoose/models/source_file_individual_qp_status_data')
      },
      individual_qp_threshold: {
        merge: true,
        keyFn: data => ({
          npi: data.npi
        }),
        model: require('../mongoose/models/individual_qp_threshold'),
        dynamic: require('../mongoose/dynamic_name_models/individual_qp_threshold'),
        sourceFileData: require('../mongoose/models/source_file_individual_qp_threshold_data')
      },
      entity_qp_status: {
        merge: true,
        keyFn: data => ({
          npi: data.npi
        }),
        model: require('../mongoose/models/entity_qp_status'),
        dynamic: require('../mongoose/dynamic_name_models/entity_qp_status'),
        sourceFileData: require('../mongoose/models/source_file_entity_qp_status_data')
      },
      entity_qp_threshold: {
        merge: true,
        keyFn: data => ({
          apm_id: data.apm_id,
          subdivision_id: data.subdivision_id,
          entity_id: data.entity_id,
          tin: data.tin,
          npi: data.npi
        }),
        model: require('../mongoose/models/entity_qp_threshold'),
        dynamic: require('../mongoose/dynamic_name_models/entity_qp_threshold'),
        sourceFileData: require('../mongoose/models/source_file_entity_qp_threshold_data')
      },
      beneficiary: {
        merge: true,
        keyFn: data => ({
          program_id: data.program_id,
          program_org_id: data.program_org_id,
          id: data.id,
          hicn: data.hicn,
          entity_id: data.entity_id,
          link_key: data.link_key,
          mbi: data.mbi
        }),
        model: require('../mongoose/models/beneficiary'),
        dynamic: require('../mongoose/dynamic_name_models/beneficiary'),
        sourceFileData: require('../mongoose/models/source_file_beneficiary_data')
      }
    })
};

const FILE_IMPORT_GROUP_TYPES = {
  pac: 'pac',
  apm: 'apm'
};

module.exports = {
  Models,
  FileImportSourceMap,
  FILE_IMPORT_GROUP_TYPES
};
