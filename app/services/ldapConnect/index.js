const logger = require('../logger');
const config = require('../../config');
const fs = require('fs');
const Promise = require('bluebird');
const ldap = require('ldapjs');

module.exports = (uri, dn, password) => ({
  acquire: () => {
    const tlsOptions = {
      minDHSize: 768,
      rejectUnauthorized: config.LDAP.REJECT_UNAUTHORIZED
    };

    if (uri.startsWith('ldaps://')) {
      tlsOptions.ca = [
        fs.readFileSync('/etc/openldap/certs/eua.pem')
      ];
    }

    const client = ldap.createClient({
      url: uri,
      tlsOptions
    });

    const newClient = {
      bind: Promise.promisify(client.bind.bind(client)),
      add: Promise.promisify(client.add.bind(client)),
      compare: Promise.promisify(client.compare.bind(client)),
      del: Promise.promisify(client.del.bind(client)),
      exop: Promise.promisify(client.exop.bind(client)),
      modify: Promise.promisify(client.modify.bind(client)),
      modifyDN: Promise.promisify(client.modifyDN.bind(client)),
      search: Promise.promisify(client.search.bind(client)),
      starttls: Promise.promisify(client.starttls.bind(client)),
      unbind: Promise.promisify(client.unbind.bind(client)),
      originalClient: client
    };

    const p = newClient.bind(dn, password)
      .catch((err) => {
        logger.error('Error while binding:', err);
        throw err;
      });

    return p.return(newClient);
  },
  release: client => client.unbind().return(true)
});
