const PublishHistory = require('../../services/mongoose/models/publish_history');

const DynamicPACModel = require('../mongoose/dynamic_name_models/pac_model');
const DynamicPACSubdivision = require('../mongoose/dynamic_name_models/pac_subdivision');
const DynamicPACEntity = require('../mongoose/dynamic_name_models/pac_entity');
const DynamicPACProvider = require('../mongoose/dynamic_name_models/pac_provider');
const DynamicPACLvt = require('../mongoose/dynamic_name_models/pac_lvt');
const DynamicPACQPScore = require('../mongoose/dynamic_name_models/pac_qp_score');
const DynamicPACQPStatus = require('../mongoose/dynamic_name_models/pac_qp_status');
const DynamicPACQPCategoryScore = require('../mongoose/dynamic_name_models/pac_qp_category_score');
const DynamicPACQPCategoryScoreSummary = require('../mongoose/dynamic_name_models/pac_qp_category_score_summary');

const emptyModel = require('../mongoose/models/empty');

const { FILE_IMPORT_GROUP_TYPES } = require('../../services/apm-models');
const { getHighestValue } = require('../../schemas/Snapshot');

const getLatestPacFilesByPerformanceYear = async function find(performanceYear, publishHistoryId) {
  const query = {
    performance_year: performanceYear,
    group_type: FILE_IMPORT_GROUP_TYPES.pac
  };

  if (publishHistoryId) {
    query.id = publishHistoryId;
  } else {
    query.status = 'published';
  }

  let snapshot;
  let pacFilesPublished;

  if (!query.id) {
    const greatestPacFileBySnapshot = await PublishHistory.aggregate([
      { $match: query },
      { $unwind: '$files' },
      {
        $addFields: {
          sortBy: {
            $switch: {
              branches: [
                { case: { $eq: ['$snapshot', 'P'] }, then: '0' }
              ],
              default: '$snapshot'
            }
          }
        }
      },
      { $sort: { sortBy: -1, published_at: -1 } },
      { $limit: 1 }
    ]);

    if (!greatestPacFileBySnapshot.length) {
      return {
        allPACDataExists: false
      };
    }

    snapshot = greatestPacFileBySnapshot[0].snapshot;

    pacFilesPublished = await PublishHistory
      .findOne({ _id: greatestPacFileBySnapshot[0]._id })
      .lean();
  } else {
    pacFilesPublished = await PublishHistory
      .findOne(query)
      .lean();

    if (pacFilesPublished && Array.isArray(pacFilesPublished.files)) {
      snapshot = getHighestValue(pacFilesPublished.files.map(file => file.snapshot));
    }
  }

  if (!pacFilesPublished) {
    return {
      allPACDataExists: false
    };
  }

  const pacModelFile = pacFilesPublished.files.find(file => file.import_file_type === 'pac-model');
  const pacSubdivisionFile = pacFilesPublished.files.find(file => file.import_file_type === 'pac-subdivision');
  const pacEntityFile = pacFilesPublished.files.find(file => file.import_file_type === 'pac-entity');
  const pacProviderFile = pacFilesPublished.files.find(file => file.import_file_type === 'pac-provider');
  const pacLvtFile = pacFilesPublished.files.find(file => file.import_file_type === 'pac-lvt');
  const pacQpStatusFile = pacFilesPublished.files.find(file => file.import_file_type === 'pac-qp_status');
  const pacQpScoreFile = pacFilesPublished.files.find(file => file.import_file_type === 'pac-qp_score');
  const pacQPCategoryScoreFile = pacFilesPublished.files.find(
    file => file.import_file_type === 'pac-qp_category_score'
  );
  const pacQPCategoryScoreSummaryFile = pacFilesPublished.files.find(
    file => file.import_file_type === 'pac-qp_category_score'
  );

  return {
    snapshot,
    publishHistory: pacFilesPublished,
    pacModelFile,
    pacSubdivisionFile,
    pacEntityFile,
    pacProviderFile,
    pacLvtFile,
    pacQpStatusFile,
    pacQpScoreFile,
    pacQPCategoryScoreFile,
    pacQPCategoryScoreSummaryFile,
    allPACDataExists: !!(
      pacModelFile &&
      pacSubdivisionFile &&
      pacEntityFile &&
      pacProviderFile &&
      pacLvtFile &&
      pacQpStatusFile &&
      pacQpScoreFile &&
      pacQPCategoryScoreFile &&
      pacQPCategoryScoreSummaryFile
    )
  };
};

const getPerformanceYears = () => PublishHistory.distinct('performance_year');

const getLatestPacCollectionsByPerformanceYear = async (performanceYear, publishHistoryId = null) => {
  const {
    pacModelFile,
    pacSubdivisionFile,
    pacEntityFile,
    pacProviderFile,
    pacLvtFile,
    pacQpStatusFile,
    pacQpScoreFile,
    pacQPCategoryScore,
    pacQPCategoryScoreSummary,
    allPACDataExists
  } = await getLatestPacFilesByPerformanceYear(performanceYear, publishHistoryId);


  const PACModel = pacModelFile ? DynamicPACModel(pacModelFile.file_id) : emptyModel;
  const PACSubdivision = pacSubdivisionFile ? DynamicPACSubdivision(pacSubdivisionFile.file_id) : emptyModel;
  const PACEntity = pacEntityFile ? DynamicPACEntity(pacEntityFile.file_id) : emptyModel;
  const PACProvider = pacProviderFile ? DynamicPACProvider(pacProviderFile.file_id) : emptyModel;
  const PACLvt = pacLvtFile ? DynamicPACLvt(pacLvtFile.file_id) : emptyModel;
  const PACQPScore = pacQpScoreFile ? DynamicPACQPScore(pacQpScoreFile.file_id) : emptyModel;
  const PACQPStatus = pacQpStatusFile ? DynamicPACQPStatus(pacQpStatusFile.file_id) : emptyModel;

  const PACQPCategoryScore = pacQPCategoryScore ?
    DynamicPACQPCategoryScore(pacQPCategoryScore.file_id) : emptyModel;

  const PACQPCategoryScoreSummary = pacQPCategoryScoreSummary ?
    DynamicPACQPCategoryScoreSummary(pacQPCategoryScoreSummary.file_id) : emptyModel;

  return {
    PACModel,
    PACSubdivision,
    PACEntity,
    PACProvider,
    PACLvt,
    PACQPScore,
    PACQPStatus,
    PACQPCategoryScore,
    PACQPCategoryScoreSummary,
    allPACDataExists,
    PACModelCollectionName: PACModel.collection.name,
    PACSubdivisionCollectionName: PACSubdivision.collection.name,
    PACEntityCollectionName: PACEntity.collection.name,
    PACProviderCollectionName: PACProvider.collection.name,
    PACLvtCollectionName: PACLvt.collection.name,
    PACQPScoreCollectionName: PACQPScore.collection.name,
    PACQPStatusCollectionName: PACQPStatus.collection.name,
    PACQPCategoryScoreCollectionName: PACQPCategoryScore.collection.name,
    PACQPCategoryScoreSummaryCollectionName: PACQPCategoryScoreSummary.collection.name
  };
};


module.exports = {
  getLatestPacFilesByPerformanceYear,
  getLatestPacCollectionsByPerformanceYear,
  getPerformanceYears
};
