from moto import server
application = server.create_backend_app('sqs')
if __name__ == "__main__":
    application.run()