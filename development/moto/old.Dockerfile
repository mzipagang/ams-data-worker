FROM alpine:3.6

RUN apk add --no-cache --update \
    gcc \
    musl-dev \
    python3-dev \
    libffi-dev \
    openssl-dev \
    python3

RUN apk add --no-cache --update \
    python-pip

RUN pip install moto[server]

RUN mkdir /test

COPY . /test/

CMD ["ls"]