#!/bin/sh
set -e

export KONG_NGINX_DAEMON=off

# if [[ "$1" == "kong" ]]; then
#   PREFIX=${KONG_PREFIX:=/usr/local/kong}
#   mkdir -p $PREFIX

#   if [[ "$2" == "docker-start" ]]; then
#     kong prepare -p $PREFIX

#     exec /usr/local/openresty/nginx/sbin/nginx \
#       -p $PREFIX \
#       -c nginx.conf
#   fi
# fi

mkdir -p /usr/local/kong
kong prepare -p /usr/local/kong
kong migrations up

function fix_linux_internal_host() {
  DOCKER_INTERNAL_HOST="host.docker.internal"

  if ! grep $DOCKER_INTERNAL_HOST /etc/hosts > /dev/null ; then
    DOCKER_INTERNAL_IP=`/sbin/ip route | awk '/default/ { print $3 }' | awk '!seen[$0]++'`
    echo -e "$DOCKER_INTERNAL_IP\t$DOCKER_INTERNAL_HOST" | tee -a /etc/hosts > /dev/null
    echo "Added $DOCKER_INTERNAL_HOST to hosts /etc/hosts as $DOCKER_INTERNAL_IP"
  fi
}

fix_linux_internal_host

exec "$@"
