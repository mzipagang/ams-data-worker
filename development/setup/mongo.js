const Promise = require('bluebird');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

mongoose.connect('mongodb://mongodb:27017/ams-api', { useNewUrlParser: true });

module.exports = () =>
  Promise.all([
    // Do whatever necessary setup there is
  ])
    .finally(() => {
      try {
        mongoose.disconnect();
      } catch (err) {
        console.log(err);
      }
    });
