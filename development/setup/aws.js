const Promise = require('bluebird');
const AWS = require('aws-sdk');

const SQS = new AWS.SQS({
  region: 'us-east-1',
  apiVersion: '2012-11-05',
  accessKeyId: 'id',
  secretAccessKey: 'key',
  endpoint: new AWS.Endpoint('http://fakesqs:5000')
});

const SNS = new AWS.SNS({
  region: 'us-east-1',
  apiVersion: '2012-11-05',
  accessKeyId: 'id',
  secretAccessKey: 'key',
  endpoint: new AWS.Endpoint('http://fakesns:5000')
});

const listQueues = Promise.promisify(SQS.listQueues.bind(SQS));
const createQueue = Promise.promisify(SQS.createQueue.bind(SQS));
const listTopics = Promise.promisify(SNS.listTopics.bind(SNS));
const createTopic = Promise.promisify(SNS.createTopic.bind(SNS));

const createQueueIfNeeded = Promise.coroutine(function* createQueueIfNeeded(existingQueues, queueName) {
  const queueUrl = (existingQueues.QueueUrls || []).find(url => url.endsWith(queueName));

  if (!queueUrl) {
    yield createQueue({
      QueueName: queueName,
      Attributes: {
        DelaySeconds: '0',
        MessageRetentionPeriod: '86400',
        ReceiveMessageWaitTimeSeconds: '0',
        WaitTimeSeconds: '0'
      }
    });
    console.log(`Queue "${queueName}" created.`);
  } else {
    console.log(`Queue "${queueName}" already existed.`);
  }
});

const createTopicIfNeeded = Promise.coroutine(function* createTopicIfNeeded(existingTopics, topicName) {
  const topicArn = (existingTopics.Topics || []).find(topic => topic.TopicArn.endsWith(topicName));

  if (!topicArn) {
    const data = yield createTopic({
      Name: topicName
    });
    console.log(`Topic "${topicName}" created.`, data);
  } else {
    console.log(`Topic "${topicName}" already existed.`);
  }
});

module.exports = () =>
    Promise.all([
      listQueues({ })
          .then(queues => Promise.all([
            createQueueIfNeeded(queues, 'ams-uploads-queue'),
            createQueueIfNeeded(queues, 'ams-uploads-scan-results'),
            createQueueIfNeeded(queues, 'ams-request-queue-1'),
            createQueueIfNeeded(queues, 'ams-request-queue-2')
          ])),
      listTopics({ })
          .then(topics => Promise.all([
            createTopicIfNeeded(topics, 'ams-upload-clean'),
            createTopicIfNeeded(topics, 'ams-upload-infected')
          ]))
    ]);
