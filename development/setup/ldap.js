const ldap = require('ldapjs');
const Promise = require('bluebird');

const FAKE_JOB_CODE = 'cn=AMS_DEV_AWS,ou=Groups,dc=cms,dc=hhs,dc=gov';

const client = ldap.createClient({
  url: 'ldap://openldap:389'
});

const bind = Promise.promisify(client.bind.bind(client));
const unbind = Promise.promisify(client.unbind.bind(client));
const add = Promise.promisify(client.add.bind(client));

const catchEntryAlreadyExistsError = (err) => {
  if (!(err instanceof ldap.EntryAlreadyExistsError)) {
    throw err;
  }

  console.log('Entry Already Exists');
};

module.exports = async () => {
  await bind('cn=admin,dc=cms,dc=hhs,dc=gov', 'admin');

  // Create System Accounts group
  await add('ou=People,dc=cms,dc=hhs,dc=gov', {
    ou: 'System Accounts',
    objectClass: ['organizationalUnit']
  }).catch(catchEntryAlreadyExistsError);

  const users = [
    { euaId: 'BBID', firstName: 'Paul', lastName: 'Baumgartner' },
    { euaId: 'V3HN', firstName: 'Peter', lastName: 'Um' },
    { euaId: 'JEFW', firstName: 'Lester', lastName: 'Jackson' },
    { euaId: 'MVNG', firstName: 'Laren', lastName: 'Milburn' },
    { euaId: 'T4QK', firstName: 'Ben', lastName: 'Traynham' },
    { euaId: 'CUM7', firstName: 'Philip', lastName: 'Clapper' },
    { euaId: 'MQ0X', firstName: 'Aaron', lastName: 'Manne' },
    { euaId: 'OQCQ', firstName: 'Kalada', lastName: 'Opuiyo' },
    { euaId: 'GC7R', firstName: 'Nicholas ', lastName: 'Galantowicz' },
    { euaId: 'SNJU', firstName: 'Nicholas', lastName: 'Shater' },
    { euaId: 'SOEU', firstName: 'Nicholas', lastName: 'Schleicher' },
    { euaId: 'MBWW', firstName: 'Patrick', lastName: 'McConnell' },
    { euaId: 'RN6L', firstName: 'Brad', lastName: 'Richardson' },
    { euaId: 'B2QK', firstName: 'Adam', lastName: 'Beck' },
    { euaId: 'CIBF', firstName: 'Yong', lastName: 'Choi' },
    { euaId: 'VMYJ', firstName: 'Mark', lastName: 'Zipagang' },
    { euaId: 'BKXW', firstName: 'Steven', lastName: 'Barnhurst' },
    { euaId: 'BLMX', firstName: 'Andrew', lastName: 'Bird' },
    { euaId: 'AWTA', firstName: 'Roye', lastName: 'Avraham' },
    { euaId: 'BBNC', firstName: 'James', lastName: 'Becwar' },
    { euaId: 'MJ2L', firstName: 'Jerell', lastName: 'Mendoza' },
    { euaId: 'K9YJ', firstName: 'Bryan', lastName: 'Knight' },
    { euaId: 'VHXB', firstName: 'Gopal', lastName: 'Unnikrishnan' },
  ];

  // Create Users
  await Promise.all(users.map(async (user) => {
    return await add(`uid=${user.euaId},ou=People,dc=cms,dc=hhs,dc=gov`, {
      cn: `${user.firstName} ${user.lastName}`,
      sn: user.lastName,
      uid: user.euaId,
      mail: `${user.euaId.toLowerCase()}@fake.com`,
      objectClass: ['inetOrgPerson', 'cmsperson'],
      ismemberof: FAKE_JOB_CODE,
      givenName: user.firstName,
      userPassword: 'test'
    }).catch(catchEntryAlreadyExistsError);
  }));

  await unbind();
};
