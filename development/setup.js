/* istanbul ignore */

const Promise = require('bluebird');

const AWSSetup = require('./setup/aws');
const LDAPSetup = require('./setup/ldap');
const MongoSetup = require('./setup/mongo');

new Promise((resolve, reject) => {
  let timer = 30;
  const check = () => {
    console.log(timer);

    timer--;

    if (timer === 0) {
      resolve();
    } else {
      setTimeout(() => check(), 1000);
    }
  };

  check();
})
    .then(() => {
      return Promise.all([
        AWSSetup().catch((err) => console.log(err)),
        LDAPSetup().catch((err) => console.log(err)),
        MongoSetup().catch((err) => console.log(err)),
      ]);
    })
    .then(() => {
      console.log('Success');
      process.exit(0);
    })
    .catch((err) => {
      console.error(err);
      process.exit(1);
    });
