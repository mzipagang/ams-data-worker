#!/bin/sh
set -e

export KONG_NGINX_DAEMON=off

mkdir -p /usr/local/kong
kong prepare -p /usr/local/kong
kong migrations up

exec "$@"
