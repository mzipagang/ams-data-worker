const pm2 = require('pm2');
const sinon = require('sinon');
const { expect } = require('chai');
const ProcessManager = require('../../batch-processing/ProcessManager');
const JobQueueService = require('../../app/services/JobQueueService');
const QueuedJobs = require('../../app/services/mongoose/models/queued_jobs');
const batchConfig = require('../../batch-processing/batchConfig');
const {
  JOB_TYPES
} = require('../../app/constants').BATCH;

describe('ProcessManager Integration Tests', () => {
  let manager;

  beforeEach(() => {
    manager = new ProcessManager();

    batchConfig.MONGOOSE.URI = process.env.MONGODB; // Use the db specified in mocha setup
  });

  afterEach((done) => {
    // Connect, kill the daemon, and disconnect using PM2's raw callback-hell approach
    pm2.connect((err1) => {
      if (err1) {
        console.err(err1); // eslint-disable-line no-console
      }
      pm2.killDaemon((err2) => {
        if (err2) {
          console.err(err2); // eslint-disable-line no-console
        }
        pm2.disconnect();
        done();
      });
    });
  });

  describe('#connect', () => {
    it('should connect to the PM2 daemon using async/await interface', async () => {
      const result = await manager.connect();
      expect(result).to.be.true;
    });
  });

  describe('#list', () => {
    it('should list processes from the PM2 daemon', async () => {
      await manager.connect();
      const result = await manager.list();
      expect(result).to.be.ofSize(0);
    });
  });

  describe('#killWorker', () => {
    it('should not blow up if worker is already dead', async () => {
      await manager.connect();
      const listResult = await manager.list();
      expect(listResult).to.be.ofSize(0);
      const killResult = await manager.killWorker(-234); // no such worker running
      expect(killResult).to.be.false;
    });
  });

  describe('#disconnect', () => {
    it('should disconnect from the PM2 daemon (synchronous)', async () => {
      await manager.connect();
      const result = await manager.list();
      expect(result).to.be.ofSize(0);
      manager.disconnect();
      try {
        await manager.list();
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err).to.exist;
      }
    });
  });

  describe('#startScript', () => {
    it('should start the specified script in the daemon', async () => {
      await manager.connect();

      const processes = await manager.startScript('test/batch-processing/fast-ready.js');

      expect(processes).to.be.ofSize(1);
      expect(processes[0].pid).to.be.greaterThan(0);
      expect(processes[0].pm2_env.name).to.equal('fast-ready');
    });

    it('should start the script with the specified name', async () => {
      await manager.connect();

      const processes = await manager.startScript('test/batch-processing/fast-ready.js', { name: 'custom-name' });

      expect(processes).to.be.ofSize(1);
      expect(processes[0].pid).to.be.greaterThan(0);
      expect(processes[0].pm2_env.name).to.equal('custom-name');
    });

    it('should reject with an error if the script cannot be found', async () => {
      await manager.connect();
      try {
        await manager.startScript('gibberish');
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err.message).to.equal('Script "gibberish" not found');
      }
    });
  });

  describe('Parent/child communication', async () => {
    const TOPIC = 'some topic name';
    const REQUEST_TYPE = 'PARENT_TO_CHILD';
    const RESPONSE_TYPE = 'CHILD_TO_PARENT';
    const PAYLOAD = { some: 'data', hello: true };

    // This helper just eliminates a bit of promise/callback ugliness
    // since we cannot use async and done in the same mocha test
    async function runTest(done) {
      await manager.connect();
      await manager.startScript('test/batch-processing/message-echo.js', { name: 'Echo 1' });
      await manager.startScript('test/batch-processing/message-echo.js', { name: 'Echo 2' });

      const [process1, process2] = await manager.list();

      const pmId1 = process1.pm2_env.pm_id;
      const pmId2 = process2.pm2_env.pm_id;

      const bus = await manager.launchBus();

      let responseCount = 0;
      bus.on(RESPONSE_TYPE, (packet) => {
        expect(packet.data.echo_success).to.be.true;
        expect(packet.data.received.topic).to.equal(TOPIC);
        expect(packet.data.received.type).to.equal(REQUEST_TYPE);
        expect(packet.data.received.data).to.eql(PAYLOAD);

        if (packet.process.pm_id === pmId1) {
          expect(packet.data.received.id).to.equal(pmId1);
          expect(packet.process.name).to.equal('Echo 1');
          responseCount += 1;
        } else if (packet.process.pm_id === pmId2) {
          expect(packet.data.received.id).to.equal(pmId2);
          expect(packet.process.name).to.equal('Echo 2');
          responseCount += 1;
        } else {
          expect.fail(0, 1, 'Unexpected PM ID');
        }

        if (responseCount === 2) {
          done();
        }
      });

      await manager.sendDataToProcessId(pmId1, REQUEST_TYPE, PAYLOAD, TOPIC);
      await manager.sendDataToProcessId(pmId2, REQUEST_TYPE, PAYLOAD, TOPIC);
    }

    it('should successfully send and receive a message', (done) => {
      runTest(done).catch((err) => {
        done(err);
      });
    });
  });

  describe('#lockAndRetrieveJob', () => {
    let job;

    beforeEach(async () => {
      job = await JobQueueService.createJob('test-type', ['param1', 'param2']);
    });

    afterEach(async () => {
      await QueuedJobs.deleteMany();
    });

    it('should lock the job and fetch it from mongo', async () => {
      const result = await manager.lockAndRetrieveJob(job.id);
      expect(result.id).to.equal(job.id);
      expect(result.parameters).to.be.equalTo(job.parameters);
      expect(result.type).to.equal(job.type);
      expect(result.status).to.equal('running');
    });

    it('should return null if unable to lock job', async () => {
      job.status = 'error';
      await job.save();
      const result = await manager.lockAndRetrieveJob(job.id);
      expect(result).to.be.null;
    });
  });

  describe('#startBatchWorker', () => {
    it('should start the "startBatchWorker" script as a daemon process', async () => {
      await manager.connect();

      const processes = await manager.startBatchWorker();

      expect(processes).to.be.ofSize(1);
      expect(processes[0].pid).to.be.greaterThan(0);
      expect(processes[0].pm2_env.name).to.equal('Worker 0');
    });
  });

  describe('#startProcessManager', () => {
    let messages;
    let fakeHandleMessage;

    beforeEach(() => {
      messages = [];
      fakeHandleMessage = sinon.fake((message) => {
        messages.push(message);
      });
      sinon.replace(manager, 'handleWaitingMessage', fakeHandleMessage);
    });

    afterEach(() => {
      sinon.restore();
    });

    it('should start and kill two BatchWorker processes', async () => {
      const processes = await manager.startProcessManager();

      expect(processes).to.be.ofSize(2);
      expect(messages).to.be.ofSize(2);
      expect(messages[0].data.success).to.be.true;
      expect(messages[1].data.success).to.be.true;

      const result1 = await manager.killWorker(processes[0].pm_id);
      expect(result1).to.be.true;

      const result2 = await manager.killWorker(processes[1].pm_id);
      expect(result2).to.be.true;

      expect(await manager.list()).to.be.ofSize(0);
    });
  });

  describe('Test Job execution', () => {
    it('should start 2 workers and execute a test job successfully', async () => {
      // Start up the manager/workers
      const processes = await manager.startProcessManager();
      expect(processes).to.be.ofSize(2);

      await new Promise((resolve) => {
        setTimeout(() => {
          resolve(true);
        }, 2500);
      });

      expect(manager.waitingWorkers.size).to.equal(2);

      // Add a task to mongo
      const jobDetails = await JobQueueService.createJob(JOB_TYPES.TEST_JOB, { duration: 1000 }, 'testuser');
      expect(jobDetails.status).to.equal('queued');

      await new Promise((resolve) => {
        setTimeout(() => {
          resolve(true);
        }, 4000);
      });

      const jobResult = await JobQueueService.getJobById(jobDetails.id);
      expect(jobResult.status).to.equal('complete-success');
      expect(manager.waitingWorkers.size).to.be.equal(2);
    });
  });
});
