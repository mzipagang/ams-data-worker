const environment = require('../../environment');
const express = require('express');
const request = require('supertest');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const AwsMock = require('../../mocks/services/aws-mock');

const expect = chai.expect;

describe('Express - /api/v1/file_transfer/get.js', () => {
  const mocks = {};
  const requireAPIGet = () => proxyquire('../../../app/express-routes/api/v1/file_transfer/get.js',
    {
      '../../../../services/aws': mocks.aws
    }
  );

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../../app/express-routes/api/v1/file_transfer/get.js', '../../../app/config']);
      environment.use('dev');

      mocks.aws = AwsMock();
    });

    it('should properly load', () => {
      const route = requireAPIGet();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should respond to a request', () => {
      const route = requireAPIGet();
      const app = express();
      const router = express.Router();
      router.get('/*', route);

      app.use(router);

      return request(app)
        .get('/test.doc')
        .send()
        .expect(200)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;

          expect(mocks.aws.S3.getParams().Bucket).to.not.be.undefined;
          expect(mocks.aws.S3.getParams().Key).to.not.be.undefined;
          expect(mocks.aws.S3.getParams().Key).to.equal('test.doc');
        });
    });

    it('should handle a request error', () => {
      mocks.aws = AwsMock.AwsErrMock();
      const route = requireAPIGet();
      const app = express();
      const router = express.Router();
      router.get('/*', route);

      app.use(router);

      return request(app)
        .get('/test.doc')
        .send()
        .expect(500);
    });

    it('should handle a request error 2', () => {
      mocks.aws = AwsMock.AwsErrMock2();
      const route = requireAPIGet();
      const app = express();
      const router = express.Router();
      router.get('/*', route);

      app.use(router);

      return request(app)
        .get('/test.doc')
        .send()
        .expect(500);
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../../app/express-routes/api/v1/file_transfer/get.js', '../../../app/config']);
      environment.use('production');

      mocks.aws = AwsMock();
    });

    it('should properly load', () => {
      const route = requireAPIGet();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should respond to a request', () => {
      const route = requireAPIGet();
      const app = express();
      const router = express.Router();
      router.get('/*', route);

      app.use(router);

      return request(app)
        .get('/test.doc')
        .send()
        .expect(200)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;

          expect(mocks.aws.S3.getParams().Bucket).to.not.be.undefined;
          expect(mocks.aws.S3.getParams().Key).to.not.be.undefined;
          expect(mocks.aws.S3.getParams().Key).to.equal('test.doc');
        });
    });
  });
});
