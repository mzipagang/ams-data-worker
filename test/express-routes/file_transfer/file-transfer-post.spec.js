const express = require('express');
const request = require('supertest');
const proxyquire = require('proxyquire');
const { expect } = require('chai');
const events = require('events');
const stream = require('stream');
const AwsMock = require('../../mocks/services/aws-mock');
const route = require('../../../app/express-routes/api/v1/file_transfer/post').handler;

const BusboyMock = (emitFile = true) => {
  function Stub() {
    const writeableStream = new stream.Writable();
    const eventEmitter = new events.EventEmitter();
    writeableStream.on = eventEmitter.on;

    const emit = writeableStream.emit.bind(writeableStream);
    writeableStream.emit = () => {};

    if (emitFile) {
      setTimeout(() => {
        emit('file', 'file', { pipe: () => {}, on: () => {} }, 'file.png', 'utf8', 'application/octet-stream');
      }, 1);
    }

    setTimeout(() => {
      emit('finish', {});
    }, 2);

    return writeableStream;
  }

  return Stub;
};

const BusboyErrorMock = () => {
  function Stub() {
    const writeableStream = new stream.Writable();
    const eventEmitter = new events.EventEmitter();
    writeableStream.on = eventEmitter.on;

    const emit = writeableStream.emit.bind(writeableStream);
    writeableStream.emit = () => {};

    setTimeout(() => {
      emit('file', 'file', {
        pipe: () => { throw new Error(false); },
        on: () => {}
      }, 'file.png', 'utf8', 'application/octet-stream');
    }, 1);

    setTimeout(() => {
      emit('finish', {});
    }, 2);

    return writeableStream;
  }

  return Stub;
};


describe('Express - /api/v1/file_transfer/post.js', () => {
  const mocks = {};

  describe('in production', () => {
    const createRouteHandler = (overrideMocks = {}) => proxyquire('../../../app/express-routes/api/v1/file_transfer/post.js', Object.assign({
      busboy: overrideMocks.busboy || mocks.busboy,
      '../../../../services/aws': overrideMocks.aws || mocks.aws
    })).handler;
    beforeEach(() => {
      mocks.busboy = BusboyMock();
      mocks.aws = AwsMock();
    });

    it('should respond to a request', () => {

      const app = express();
      const router = express.Router();
      router.post('/*', createRouteHandler());

      app.use(router);

      return request(app)
      .post('/test.doc?meta[APM-File-Type]=cmmi-model&meta[Original-File-Name]=test.doc')
      .type('form')
      .attach('file', 'test/test_files/sampleFile')
      .expect(200)
      .then((res) => {
        const data = res.body.data;
        expect(data.success).to.equal(true);
        expect(mocks.aws.S3.getParams().Bucket).to.equal('MOCK_BUCKET');
        expect(mocks.aws.S3.getParams().Key).to.equal('test.doc');
        expect(mocks.aws.S3.getParams().ContentType).to.equal('application/octet-stream');
        expect(mocks.aws.S3.getParams().Metadata['Original-File-Name']).to.equal('test.doc');
        expect(mocks.aws.S3.getParams().Metadata['APM-File-Type']).to.equal('cmmi-model');
      });
    });

    it('should handle a request error', () => {
      mocks.busboy = BusboyErrorMock();
      const app = express();
      const router = express.Router();
      router.post('/*', createRouteHandler());

      app.use(router);

      return request(app)
      .post('/test.doc?meta[APM-File-Type]=cmmi-model&meta[Original-File-Name]=test.doc')
      .type('form')
      .attach('file', 'test/test_files/sampleFile')
      .expect(500);
    });

    it('should handle an external error', () => {
      mocks.aws = AwsMock.S3ErrorMock();
      const app = express();
      const router = express.Router();
      router.post('/*', createRouteHandler());

      app.use(router);

      return request(app)
      .post('/test.doc?meta[APM-File-Type]=cmmi-model&meta[Original-File-Name]=test.doc')
      .type('form')
      .attach('file', 'test/test_files/sampleFile')
      .expect(500);
    });

    it('should throw error if no upload files', () => {
      mocks.aws = AwsMock.S3ErrorMock();
      mocks.busboy = BusboyMock(false);
      const app = express();
      const router = express.Router();
      router.post('/*', createRouteHandler());

      app.use(router);

      return request(app)
      .post('/test.doc?meta[APM-File-Type]=cmmi-model&meta[Original-File-Name]=test.doc')
      .type('form')
      .expect(500);
    });
  });
});

