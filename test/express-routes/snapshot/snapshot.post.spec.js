const request = require('supertest');
const express = require('express');
const chai = require('chai');
const sinon = require('sinon');
const bodyParser = require('body-parser');
const Promise = require('bluebird');
require('sinon-mongoose');

const PACModel = require('../../../app/services/mongoose/models/pac_model');
const PACSubdivision = require('../../../app/services/mongoose/models/pac_subdivision');
const ApmEntity = require('../../../app/services/mongoose/models/apm_entity');
const ApmProvider = require('../../../app/services/mongoose/models/apm_provider');
const mongoose = require('../../../app/services/mongoose');
const proxyquire = require('proxyquire');

class SnapshotClass {
  constructor() {
    this.id = '1234567890';
    this.collections = [];
    this.status = 'processing';
    this.qppYear = 2017;
    this.qpPeriod = 'Initial';
    this.snapshotRun = 'firstrun';
    this.initNotes = '';
    this.createdBy = '';
    this.status = 'processing';
  }

  save() {
    return Promise.resolve({});
  }
}

const mocks = {
  snapshot: SnapshotClass.bind(this),
  snapshotService: {
    statusValidation: () => Promise.resolve({})
  }
};

const getSnapshot = () => proxyquire('../../../app/express-routes/api/v1/snapshot/post.js', {
  '../../../../services/mongoose/models/snapshot': mocks.snapshot,
  '../../../../services/snapshot': mocks.snapshotService
}).handler;

const SnapshotService = require('../../../app/services/snapshot');

const expect = chai.expect;

describe('Express - /api/v1/snapshot/post.js', () => {
  let PACModelMock;
  let PACSubdivisionMock;
  let ApmEntityMock;
  let ApmProviderMock;
  let mongooseMock;
  let SnapshotServiceMock;

  beforeEach(() => {
    PACModelMock = sinon.mock(PACModel);
    PACModelMock
      .expects('aggregate')
      .once()
      .resolves({});
    PACSubdivisionMock = sinon.mock(PACSubdivision);
    PACSubdivisionMock
      .expects('aggregate')
      .once()
      .resolves({});
    ApmEntityMock = sinon.mock(ApmEntity);
    ApmEntityMock
      .expects('aggregate')
      .once()
      .resolves({});
    ApmProviderMock = sinon.mock(ApmProvider);
    ApmProviderMock
      .expects('aggregate')
      .once()
      .resolves({});
    mongooseMock = sinon.mock(mongoose);
    mongooseMock
      .expects('model')
      .atLeast(1)
      .returns({
        init: () => {},
        collection: {
          name: ''
        }
      });
    SnapshotServiceMock = sinon.mock(SnapshotService);
    SnapshotServiceMock
      .expects('entityStatusFilter')
      .atLeast(1)
      .resolves({});
    SnapshotServiceMock
      .expects('providerStatusFilter')
      .atLeast(1)
      .resolves({});
  });

  afterEach(() => {
    PACModelMock.restore();
    PACSubdivisionMock.restore();
    ApmEntityMock.restore();
    ApmProviderMock.restore();
    mongooseMock.restore();
    SnapshotServiceMock.restore();
  });

  it('should load module', () => {
    expect(getSnapshot()).to.not.be.undefined;
  });

  it('should create copies of all 4 collections', (done) => {
    const app = express();
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(getSnapshot());

    request(app)
      .post('/')
      .send({
        qppYear: '2017',
        qpPeriod: '',
        snapshotRun: '',
        name: '',
        initNotes: '',
        user: { username: '' }
      })
      .expect(200)
      .then(() => {
        setImmediate(() => {
          PACModelMock.verify();
          PACSubdivisionMock.verify();
          ApmEntityMock.verify();
          ApmProviderMock.verify();
          done();
        });
      });
  });
});
