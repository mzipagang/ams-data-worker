const sinon = require('sinon');
const getValidations = require('../../../../../app/express-routes/api/v1/files/id/validations/get').handler;
const request = require('supertest');
const express = require('express');
const expect = require('chai').expect;
require('sinon-mongoose');
const { Readable } = require('stream');

const FileImportGroup = require('../../../../../app/services/mongoose/models/file_import_group');
const SourceFileEntityData = require('../../../../../app/services/mongoose/models/source_file_entity_data');
const SourceFileProviderData = require('../../../../../app/services/mongoose/models/source_file_provider_data');
const QpcScore = require('../../../../../app/services/mongoose/dynamic_name_models/pac_qp_category_score');
const QpcScoreSummary = require('../../../../../app/services/mongoose/dynamic_name_models/pac_qp_category_score_summary');

describe('Express - /api/v1/files/id/validations/get.js', () => {
  let FileImportGroupMock;
  let SourceFileEntityDataMock;
  let SourceFileProviderDataMock;

  const mockData = {
    validations: [{ validationType: 'HARD_VALIDATION', errorCode: 'A1' }],
    meta: {
      file_line_number: 1
    }
  };

  let eventCount = 0;
  const mockEventStream = new Readable({
    objectMode: true,
    read(_size) {
      if (eventCount < 1) {
        eventCount += 1;
        return this.push(mockData);
      }
      return this.push(null);
    }
  });

  beforeEach(() => {
    FileImportGroupMock = sinon.mock(FileImportGroup);
    FileImportGroupMock
      .expects('findOne')
      .chain('elemMatch')
      .chain('exec')
      .resolves({
        files: [{
          import_file_type: 'cmmi-entity'
        }]
      });

    SourceFileEntityDataMock = sinon.mock(SourceFileEntityData);
    SourceFileEntityDataMock
      .expects('find')
      .chain('lean')
      .chain('cursor')
      .returns(mockEventStream);

    SourceFileProviderDataMock = sinon.mock(SourceFileProviderData);
    SourceFileProviderDataMock
      .expects('find')
      .chain('lean')
      .chain('cursor')
      .returns(mockEventStream);
  });

  afterEach(() => {
    FileImportGroupMock.restore();
    SourceFileEntityDataMock.restore();
    SourceFileProviderDataMock.restore();
  });

  it('should respond with 200', () => {
    const app = express();
    app.use(getValidations);

    return request(app)
      .get('/')
      .send()
      .expect(200)
      .then((res) => {
        expect(res.body).to.not.be.undefined;
      });
  });

  it('should respond with 400 if no files found', () => {
    const app = express();
    app.use(getValidations);

    FileImportGroupMock.restore();
    FileImportGroupMock = sinon.mock(FileImportGroup);
    FileImportGroupMock
      .expects('findOne')
      .chain('elemMatch')
      .chain('exec')
      .resolves({
        files: []
      });

    return request(app)
      .get('/')
      .send()
      .expect(404);
  });

  it('should respond with 422 if invalid file type', () => {
    const app = express();
    app.use(getValidations);

    FileImportGroupMock.restore();
    FileImportGroupMock = sinon.mock(FileImportGroup);
    FileImportGroupMock
      .expects('findOne')
      .chain('elemMatch')
      .chain('exec')
      .resolves({
        files: [{
          import_file_type: 'pac-provider'
        }]
      });

    return request(app)
      .get('/')
      .send()
      .expect(422);
  });

  it('should handle an error', () => {
    const app = express();
    app.use(getValidations);

    FileImportGroupMock.restore();
    FileImportGroupMock = sinon.mock(FileImportGroup);
    FileImportGroupMock
      .expects('findOne')
      .chain('elemMatch')
      .chain('exec')
      .rejects(new Error('Fake error'));

    return request(app)
      .get(`/`)
      .send()
      .expect(500)
      .then((res) => {
        const body = res.body;
        expect(body).to.not.be.undefined;
        expect(body.message).to.not.be.undefined;
      });
  });
});

describe('should get CSV for dynamic collections', () => {

  let FileImportGroupMock;
  let QpcScoreMock;
  let QpcScoreSummaryMock;

  const mockData = {
    validations: [{ validationType: 'HARD_VALIDATION', errorCode: 'A1' }],
    meta: {
      file_line_number: 1
    }
  };

  beforeEach(() => {
    FileImportGroupMock = sinon.mock(FileImportGroup);
    FileImportGroupMock
      .expects('findOne')
      .chain('elemMatch')
      .chain('exec')
      .resolves({
        files: [{
          import_file_type: 'pac-qp_category_score'
        }]
      });

    QpcScoreMock = sinon.mock(QpcScore());
    QpcScoreMock
      .expects('find')
      .chain('lean')
      .returns([mockData]);

      QpcScoreSummaryMock = sinon.mock(QpcScoreSummary());
      QpcScoreSummaryMock
      .expects('find')
      .chain('lean')
      .returns([mockData]);
  });

  afterEach(() => {
    FileImportGroupMock.restore();
    QpcScoreMock.restore();
    QpcScoreSummaryMock.restore();
  });

  it('should respond with 200', () => {
    const app = express();
    app.use(getValidations);

    return request(app)
      .get('/')
      .send()
      .expect(200)
      .then((res) => {
        expect(res.body).to.not.be.undefined;
      });
  });
});
