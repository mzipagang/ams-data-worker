const sinon = require('sinon');
const User = require('../../../app/services/mongoose/models/user');
const getHealthCheck = require('../../../app/express-routes/api/v1/health_check/get').handler;
const request = require('supertest');
const express = require('express');
const expect = require('chai').expect;




describe('Express - /api/v1/health_check/get.js', () => {
  let mockUser;
  const _originalSetTimeout = global.setTimeout;

  const longResolve = (time = 20000) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, time);
    });
  };

  beforeEach(() => {
    mockUser = sinon.mock(User);
  });

  afterEach(() => {
    mockUser.restore();
    global.setTimeout = _originalSetTimeout;
  });

  it('should respond with 200 and success as true if mongo returns a response', () => {
    const app = express();
    app.use(getHealthCheck);

    mockUser
      .expects('count')
      .resolves(1);

    return request(app)
      .get('/')
      .send()
      .expect(200)
      .then((res) => {
        expect(res.body).to.not.be.undefined;
        expect(res.body.success).to.be.true;
      });
  });

  it('should respond with 500 and success as false if mongo is down', () => {
    const app = express();
    app.use(getHealthCheck);

    mockUser
      .expects('count')
      .rejects(new Error('fake error'));

    return request(app)
      .get('/')
      .send()
      .expect(500)
      .then((res) => {
        expect(res.body.success).to.equal(false);
        expect(res.body.success).to.equal(false);
      });
  });

  it('should time out', (done) => {
    const app = express();
    app.use(getHealthCheck);

    mockUser
      .expects('count')
      .returns(longResolve(25000));

    global.setTimeout = (fn, ms) => {
      fn();
    };

    request(app)
      .get('/')
      .send()
      .expect(500)
      .then((res) => {
        expect(res.body.success).to.equal(false);
        done();
      });

  });
});
