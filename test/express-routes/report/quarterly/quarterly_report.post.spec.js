const environment = require('../../../environment');
const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');
const bodyParser = require('body-parser');

const expect = chai.expect;

const app = express();
app.use(bodyParser.json());
app.use('/', require('../../../../app/express-routes/api/v1/report/quarterly/post.js').handler);
const QuarterlyReport = require('../../../../app/services/mongoose/models/quarterly_report');


describe('Express - /api/v1/report/quarterly/post.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    const reportObject =
      {
        model_name: 'model_name2',
        model_id: '66',
        group: 'group',
        team_lead: 'test',
        statutory_authority: 'auth',
        announcement_date: '2018-02-22T04:01:45.836Z',
        award_date: '2018-02-22T04:01:45.836Z',
        performance_start_date: '2018-02-22T04:01:45.836Z',
        performance_end_date: '2018-02-22T04:01:45.836Z',
        additional_information: 'info',
        num_ffs_beneficiaries: 5,
        num_advantage_beneficiaries: 5,
        num_medicaid_beneficiaries: 5,
        num_chip_beneficiaries: 5,
        num_dually_eligible_beneficiaries: 5,
        num_private_insurance_beneficiaries: 5,
        num_other_beneficiaries: 5,
        total_beneficiaries: 5,
        notes_for_beneficiary_counts: 'notes',
        num_physicians: 5,
        num_other_individual_human_providers: 5,
        num_hospitals: 5,
        num_other_providers: 5,
        total_providers: 5,
        notes_provider_count: 'notes',
        year: 2018,
        quarter: '1',
        created_at: '2018-07-12T11:30:53.359Z',
        updated_at: '2018-07-12T11:30:53.359Z',
        waivers: [
          {
            value: 'payment',
            text: 'freeform text'
          }
        ],
        id: 'c850eb70-9cae-4201-a2fb-f20edc2727be',
        user: { username: 'test' }
      };
    before(() => {
      sinon.stub(QuarterlyReport.prototype, 'save').resolves(new QuarterlyReport(reportObject));
    });

    after(() => {
      QuarterlyReport.prototype.save.restore();
    });

    afterEach(() => {
      QuarterlyReport.prototype.save.resetHistory();
    });

    it('should succeed', () => request(app)
      .post('/')
      .send(reportObject)
      .expect(200)
      .then((res) => {
        const body = res.body;
        expect(body).to.not.be.undefined;
        expect(body.data).to.not.be.undefined;
        expect(body.data.quarterly_report).to.not.be.undefined;
      }));

    it('should handle an error', () => {
      QuarterlyReport.prototype.save.callsFake(() => Promise.reject(new Error('Fake Error')));

      return request(app)
        .post('/')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });
});
