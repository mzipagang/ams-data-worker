const sinon = require('sinon');
const chai = require('chai');
const mongoose = require('mongoose');
const QuarterlyReport = require('../../../../app/services/mongoose/models/quarterly_report');
const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const QuarterlyReportMock = require('../../../mocks/data/quarterly_report');
const DataLog = require('../../../../app/services/mongoose/models/data_log');

const expect = chai.expect;

describe('QuarterlyReportController', () => {
  const app = express();
  app.use(bodyParser.json());
  app.use('/:id', require('../../../../app/express-routes/api/v1/report/quarterly/id/put').handler);
  const reportObject =
    {
      model_name: 'model_name2',
      model_id: '66',
      group: 'group',
      team_lead: 'test',
      statutory_authority: 'auth',
      announcement_date: '2018-02-22T04:01:45.836Z',
      award_date: '2018-02-22T04:01:45.836Z',
      performance_start_date: '2018-02-22T04:01:45.836Z',
      performance_end_date: '2018-02-22T04:01:45.836Z',
      additional_information: 'info',
      num_ffs_beneficiaries: 5,
      num_advantage_beneficiaries: 5,
      num_medicaid_beneficiaries: 5,
      num_chip_beneficiaries: 5,
      num_dually_eligible_beneficiaries: 5,
      num_private_insurance_beneficiaries: 5,
      num_other_beneficiaries: 5,
      total_beneficiaries: 5,
      notes_for_beneficiary_counts: 'notes',
      num_physicians: 5,
      num_other_individual_human_providers: 5,
      num_hospitals: 5,
      num_other_providers: 5,
      total_providers: 5,
      notes_provider_count: 'notes',
      year: 2018,
      quarter: '1',
      createdAt: '2018-07-12T11:30:53.359Z',
      updated: '2018-07-12T11:30:53.359Z',
      waivers: [
        {
          value: 'payment',
          text: 'freeform text'
        }
      ],
      id: 'c850eb70-9cae-4201-a2fb-f20edc2727be',
      user: {
        username: 'test'
      }
    };
  const res = {
    json: () => res,
    status: () => res
  };

  beforeEach(() => {
    sinon.stub(QuarterlyReport, 'findOneAndUpdate');
    sinon.stub(DataLog, 'createLog');
    sinon.spy(res, 'status');
    sinon.spy(res, 'json');

    const report = QuarterlyReportMock();
    report._id = mongoose.Types.ObjectId('5b4ce12a264d2c6c2e030662');
    report.save();
  });

  afterEach(() => {
    QuarterlyReport.findOneAndUpdate.restore();
    DataLog.createLog.restore();
    res.status.restore();
    res.json.restore();
    QuarterlyReport.deleteMany();
  });

  describe('updateReport', () => {
    it('should update the given id', (done) => {
      QuarterlyReport.findOneAndUpdate.resolves(reportObject);
      DataLog.createLog.resolves({});

      request(app)
        .put(`/${reportObject.model_id}`)
        .send(reportObject)
        .expect('Content-Type', /json/)
        .expect(200)
        .expect({ result: reportObject })
        .end((err) => {
          if (err) {
            return done(err);
          }

          expect(QuarterlyReport.findOneAndUpdate).to.have.been.always.calledWith({
            id: reportObject.model_id, quarter: reportObject.quarter, year: reportObject.year
          });

          return done();
        });
    });


    it('should handle an error', (done) => {
      QuarterlyReport.findOneAndUpdate.restore();
      sinon.stub(QuarterlyReport, 'findOneAndUpdate');
      QuarterlyReport.findOneAndUpdate.rejects();
      DataLog.createLog.resolves({});

      request(app)
        .put(`/${reportObject.model_id}`)
        .send(reportObject)
        .expect('Content-Type', /json/)
        .expect(500)
        .end(err => done(err));
    });
  });
});
