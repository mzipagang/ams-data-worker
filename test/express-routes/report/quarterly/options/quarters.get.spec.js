const sinon = require('sinon');
const chai = require('chai');
const QuarterlyReport = require('../../../../../app/services/mongoose/models/quarterly_report');
const QuarterlyReportMock = require('../../../../mocks/data/quarterly_report');
const { ReportOptions, handler } =
  require('../../../../../app/express-routes/api/v1/report/quarterly/options/quarters/get');

const expect = chai.expect;

describe('ReportOptions', () => {
  const res = {
    json: () => res,
    status: () => res
  };

  beforeEach(() => {
    sinon.spy(res, 'status');
    sinon.spy(res, 'json');

    const report = QuarterlyReportMock();
    report.save();

    const report2 = QuarterlyReportMock();
    report2.quarter = '2';
    report2.save();
  });

  afterEach(() => {
    res.status.restore();
    res.json.restore();
    QuarterlyReport.deleteMany();
  });

  describe('handler', () => {
    describe('error handling', () => {
      it('should return an error for an invalid year', async () => {
        const req = {
          query: {
            year: 'test'
          }
        };
        await handler(req, res);
        expect(res.status).to.have.been.calledWith(422);
      });

      it('should return an error if no year is provided', async () => {
        const req = {
          query: {}
        };
        await handler(req, res);
        expect(res.status).to.have.been.calledWith(422);
      });
    });

    it('should successfully process a request', async () => {
      const getAvailableQuartersForYearStub = sinon.stub(ReportOptions, 'getAvailableQuartersForYear')
        .callsFake(() => Promise.resolve(['1', '2']));
      const req = {
        query: {
          year: 2018
        }
      };
      await handler(req, res);
      expect(res.status).to.have.been.calledWith(200);
      expect(res.json.getCall(0).args[0].options.length).to.equal(2);
      expect(res.json.getCall(0).args[0].options[0]).to.equal('1');

      getAvailableQuartersForYearStub.restore();
    });

    it('should properly handle an error in the request', async () => {
      sinon.stub(ReportOptions, 'getAvailableQuartersForYear')
        .callsFake(() => { throw new Error('fake error'); });
      const req = {
        query: {
          year: 2018
        }
      };
      await handler(req, res);
      expect(res.status).to.have.been.calledWith(500);
      expect(res.json).to.have.been.calledWith({
        message: 'There was an error in getting the quarter for year provided'
      });

      ReportOptions.getAvailableQuartersForYear.restore();
    });
  });
});
