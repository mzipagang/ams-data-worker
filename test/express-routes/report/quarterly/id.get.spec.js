const sinon = require('sinon');
const chai = require('chai');
const mongoose = require('mongoose');
const QuarterlyReport = require('../../../../app/services/mongoose/models/quarterly_report');
const Controller = require('../../../../app/express-routes/api/v1/report/quarterly/id/get').handler;

const expect = chai.expect;

const createQuarterlyReport = () => new QuarterlyReport({
  model_name: 'model_name2',
  model_id: '66',
  group: 'group',
  team_lead: 'test',
  statutory_authority: 'test',
  announcement_date: '2018-02-22T04:01:45.836Z',
  award_date: '2018-02-22T04:01:45.836Z',
  performance_start_date: '2018-02-22T04:01:45.836Z',
  performance_end_date: '2018-02-22T04:01:45.836Z',
  num_ffs_beneficiaries: 5,
  num_advantage_beneficiaries: 5,
  num_medicaid_beneficiaries: 5,
  num_dually_eligible_beneficiaries: 5,
  num_private_insurance_beneficiaries: 5,
  num_other_beneficiaries: 5,
  total_beneficiaries: 5,
  notes_for_beneficiary_counts: 'notes',
  num_physicians: 5,
  num_other_individual_human_providers: 5,
  num_hospitals: 5,
  num_other_providers: 5,
  num_chip_beneficiaries: 5,
  additional_information: 'test',
  total_providers: 5,
  notes_provider_count: 'notes',
  year: 2018,
  createdAt: '2018-07-12T11:30:53.359Z',
  updated: '2018-07-12T11:30:53.359Z',
  waivers: [
    {
      value: 'payment',
      text: 'freeform text'
    }
  ]
});

describe('QuarterlyReportController', () => {
  const res = {
    json: () => res,
    status: () => res
  };

  beforeEach(async () => {
    sinon.spy(res, 'status');
    sinon.spy(res, 'json');

    const report = createQuarterlyReport();
    report._id = mongoose.Types.ObjectId('5b4ce12a264d2c6c2e030662');
    await report.save();
  });

  afterEach(async () => {
    res.status.restore();
    res.json.restore();
    await QuarterlyReport.deleteMany();
  });

  describe('getById', () => {
    it('should return an error for an invalid id', async () => {
      const req = {
        params: {
          id: 'invalid'
        }
      };
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(422);
    });

    it('should return an error if no id is provided', async () => {
      const req = {
        params: {}
      };
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(422);
    });

    it('should return a 404 if no matching id is provided', async () => {
      sinon.stub(QuarterlyReport, 'findOne').returns({ lean: () => null });
      const req = {
        params: {
          id: '5b4ce12a264d2c6c2e030663'
        }
      };
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(404);
      QuarterlyReport.findOne.restore();
    });

    it('should return a 500 if an unhandled error occurs', async () => {
      sinon.stub(QuarterlyReport, 'findOne').returns({});
      const req = {
        params: {
          id: '5b4ce12a264d2c6c2e030663'
        }
      };
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(500);
      QuarterlyReport.findOne.restore();
    });

    it('should successfully process a request', async () => {
      sinon.stub(QuarterlyReport, 'findOne').returns({ lean: () => ({ quarter: '2' }) });
      const req = {
        params: {
          id: '5b4ce12a264d2c6c2e030662'
        }
      };
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(200);
      QuarterlyReport.findOne.restore();
    });
  });
});
