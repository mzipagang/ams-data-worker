const sinon = require('sinon');
const chai = require('chai');
const QuarterlyReportConfig = require('../../../../../app/services/mongoose/models/quarterly_report_config');
const Controller = require('../../../../../app/express-routes/api/v1/report/quarterly/config/get').handler;

const expect = chai.expect;

describe('QuarterlyReportConfigController', () => {
  const req = {};

  const res = {
    json: () => res,
    status: () => res
  };

  beforeEach(() => {
    sinon.spy(res, 'status');
    sinon.spy(res, 'json');
  });

  afterEach(() => {
    res.status.restore();
    res.json.restore();
    QuarterlyReportConfig.findOne.restore();
  });

  describe('get', () => {
    it('should return a 404 if no data is returned', async () => {
      sinon.stub(QuarterlyReportConfig, 'findOne').returns({ lean: () => null });
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(404);
    });

    it('should return a 500 if an unhandled error occurs', async () => {
      sinon.stub(QuarterlyReportConfig, 'findOne').throws(new Error('unhandled error'));
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(500);
      expect(res.json).to.have.been.calledWith({
        message: 'There was an error getting a quarterly report config: Error: unhandled error'
      });
    });

    it('should successfully process a request', async () => {
      sinon.stub(QuarterlyReportConfig, 'findOne').returns({ lean: () => ({}) });
      await Controller(req, res);
      expect(res.status).to.have.been.calledWith(200);
    });
  });
});
