const sinon = require('sinon');
const chai = require('chai');
const QuarterlyReport = require('../../../../../app/services/mongoose/models/quarterly_report');
const QuarterlyReportMock = require('../../../../mocks/data/quarterly_report');
const { HistoricalPastReports, handler } =
  require('../../../../../app/express-routes/api/v1/report/quarterly/historical_reports/get');

const expect = chai.expect;

describe('HistoricalPastReports', () => {
  const res = {
    json: () => res,
    status: () => res
  };

  beforeEach(() => {
    sinon.spy(res, 'status');
    sinon.spy(res, 'json');

    const report = QuarterlyReportMock();
    report.save();

    const report2 = QuarterlyReportMock();
    report2.quarter = '2';
    report2.save();
  });

  afterEach(() => {
    res.status.restore();
    res.json.restore();
    QuarterlyReport.deleteMany();
  });

  describe('handler', () => {
    describe('error handling', () => {
      it('should return an error for an invalid year', async () => {
        const req = {
          query: {
            quarter: null,
            year: 'test'
          }
        };
        await handler(req, res);
        expect(res.status).to.have.been.calledWith(422);
      });

      it('should return 200 even if no quarter is provided', async () => {
        const req = {
          query: {
            quarter: null
          }
        };
        await handler(req, res);
        expect(res.status).to.have.been.calledWith(200);
      });

      it('should return an error for an invalid quarter', async () => {
        const req = {
          query: {
            quarter: 'test',
            year: null
          }
        };
        await handler(req, res);
        expect(res.status).to.have.been.calledWith(422);
      });

      it('should return 200 even if no year is provided', async () => {
        const req = {
          query: {
            year: null
          }
        };
        await handler(req, res);
        expect(res.status).to.have.been.calledWith(200);
      });
    });

    it('should successfully process a request', async () => {
      const getReportsByYearAndQuarterStub =
        sinon.stub(HistoricalPastReports, 'getReportsByYearAndQuarter').callsFake(() =>
          Promise.resolve([{ quarter: '2' }]));
      const req = {
        query: {
          quarter: '2',
          year: 2018
        }
      };
      await handler(req, res);
      expect(res.status).to.have.been.calledWith(200);
      expect(res.json.getCall(0).args[0].reports.length).to.equal(1);
      expect(res.json.getCall(0).args[0].reports[0].quarter).to.equal('2');

      getReportsByYearAndQuarterStub.restore();
    });

    it('should properly handle an error in the request', async () => {
      sinon.stub(HistoricalPastReports, 'getReportsByYearAndQuarter');
      HistoricalPastReports.getReportsByYearAndQuarter.callsFake(() => {
        throw new Error('fake error');
      });
      const req = {
        query: {
          quarter: 1,
          year: 2018
        }
      };
      await handler(req, res);
      expect(res.status).to.have.been.calledWith(500);
      expect(res.json).to.have.been.calledWith({ message: 'There was an error in getting the historical reports' });

      HistoricalPastReports.getReportsByYearAndQuarter.restore();
    });
  });
});
