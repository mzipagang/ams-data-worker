const { QppSnapshotRun, handler } = require('../../../../../app/express-routes/api/v1/report/qp/snapshot_run/get');
const PacSnapshots = require('../../../../../app/services/mongoose/models/pac_snapshot');
const sinon = require('sinon');
const chai = require('chai');

const expect = chai.expect;
require('sinon-mongoose');

describe('QppSnapshotRun', () => {
  let PacSnapshotsMock;
  let res;

  beforeEach(() => {
    PacSnapshotsMock = sinon.mock(PacSnapshots);
    PacSnapshotsMock
      .expects('distinct')
      .resolves(['P', 'S']);

    res = {
      status: sinon.stub(),
      json: sinon.stub()
    };

    res.status.returns(res);
    res.json.returns(res);
  });

  afterEach(() => {
    PacSnapshotsMock.restore();
  });

  describe('getSnapshotRun()', () => {
    it('should query pac snapshot collection for distinct snapshot_run', async () => {
      const run = await QppSnapshotRun.getSnapshotRun(2017);
      expect(run[0]).to.equal('P');
      expect(run[1]).to.equal('S');
    });
  });

  describe('handler()', () => {
    it('should successfully return snapshot [] on undefined', async () => {
      const req = {
        query: {
          performance_year: undefined
        }
      };

      await handler(req, res);

      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({ snapshot_run: [] });
    });

    it('should successfully return snapshot [] on the string undefined', async () => {
      const req = {
        query: {
          performance_year: 'undefined'
        }
      };

      await handler(req, res);

      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({ snapshot_run: [] });
    });

    it('should successfully return snapshot run values', async () => {
      const req = {
        query: {
          performance_year: 2017
        }
      };

      await handler(req, res);

      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({ snapshot_run: ['P', 'S'] });
    });

    it('should properly handle an error', async () => {
      PacSnapshotsMock.restore();
      PacSnapshotsMock = sinon.mock(PacSnapshots);
      PacSnapshotsMock
        .expects('distinct')
        .rejects('fake error');
      const req = {
        query: {
          performance_year: 2017
        }
      };

      await handler(req, res);
      expect(res.status).to.have.been.calledWith(500);
      expect(res.json).to.have.been.calledWith({
        message: 'There was an error in getting the snapshot run'
      });
    });
  });
});
