const { QppPerformanceYear, handler } =
  require('../../../../../app/express-routes/api/v1/report/qp/performance_year/get');
const PacSnapshots = require('../../../../../app/services/mongoose/models/pac_snapshot');
const sinon = require('sinon');
const chai = require('chai');

const expect = chai.expect;
require('sinon-mongoose');

describe('QppPerformanceYear', () => {
  let PacSnapshotsMock;
  let res;

  beforeEach(() => {
    PacSnapshotsMock = sinon.mock(PacSnapshots);
    PacSnapshotsMock
      .expects('distinct')
      .resolves([2017, 2018]);

    res = {
      status: sinon.stub(),
      json: sinon.stub()
    };

    res.status.returns(res);
    res.json.returns(res);
  });

  afterEach(() => {
    PacSnapshotsMock.restore();
  });

  describe('getPerformanceYear()', () => {
    it('should query pac snapshot collection for distinct performance years', async () => {
      const years = await QppPerformanceYear.getPerformanceYears();
      expect(years[0]).to.equal(2017);
      expect(years[1]).to.equal(2018);
    });
  });

  describe('handler()', () => {
    it('should successfully return performance years', async () => {
      const req = {};

      await handler(req, res);

      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({ performance_years: [2017, 2018] });
    });

    it('should properly handle an error', async () => {
      PacSnapshotsMock.restore();
      PacSnapshotsMock = sinon.mock(PacSnapshots);
      PacSnapshotsMock
        .expects('distinct')
        .rejects('fake error');
      const req = {};

      await handler(req, res);
      expect(res.status).to.have.been.calledWith(500);
      expect(res.json).to.have.been.calledWith({
        message: 'There was an error in getting the performance year'
      });
    });
  });
});
