const sinon = require('sinon');
const chai = require('chai');
const Report = require('../../../../../app/services/report');
const QPMetrics = require('../../../../../app/express-routes/api/v2/report/qp/metrics/get').handler;

const expect = chai.expect;
require('sinon-mongoose');

describe('QP Metrics Report', () => {
  let res;
  let reportMock = null;
  let snapshotMock = null;

  beforeEach(() => {
    res = {
      status: sinon.stub(),
      json: sinon.stub()
    };

    res.status.returns(res);
    res.json.returns(res);

    snapshotMock = sinon.stub(Report.snapshot, 'getModels').returns({});
    reportMock = sinon.stub(Report.qpMetrics, 'report').returns({});
  });

  afterEach(() => {
    reportMock.restore();
    snapshotMock.restore();
  });

  describe('Request', () => {
    it('It should use the current date as the date field.', async () => {
      const req = {
        query: {
          performance_year: 2017,
          run_snapshot: 'F'
        }
      };

      await QPMetrics(req, res);

      expect(res.status).to.have.been.calledWith(200);
    });

    it('It should use the request date', async () => {
      const req = {
        query: {
          performance_year: 2017,
          run_snapshot: 'F',
          date: new Date().toString()
        }
      };

      await QPMetrics(req, res);

      expect(res.status).to.have.been.calledWith(200);
    });

    it('should properly handle an error', async () => {
      snapshotMock.restore();
      snapshotMock = sinon.stub(Report.snapshot, 'getModels');
      snapshotMock.rejects('fake error');
      const req = {
        query: {
          performance_year: 2017,
          run_snapshot: 'F'
        }
      };

      await QPMetrics(req, res);
      expect(res.status).to.have.been.calledWith(500);
    });

    it('should properly handle a snapshot error', async () => {
      snapshotMock.restore();
      snapshotMock = sinon.stub(Report.snapshot, 'getModels').returns({ error: 'error' });
      const req = {
        query: {
          performance_year: 2017,
          run_snapshot: 'F'
        }
      };

      await QPMetrics(req, res);
      expect(res.status).to.have.been.calledWith(500);
    });
  });
});
