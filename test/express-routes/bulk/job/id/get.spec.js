const QueuedJobs = require('../../../../../app/services/mongoose/models/queued_jobs');
const JobQueueService = require('../../../../../app/services/JobQueueService');
const getQueuedJobStatus = require('../../../../../app/express-routes/api/v1/bulk/job/id/get');
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);

describe('Express - /api/v1/bulk/job/:id/get', () => {
  const req = {
    params: {
      id: '1234567890'
    }
  };
  const res = {
    status: () => res,
    json: () => res
  };
  let statusSpy;

  before(async () => {
    await new QueuedJobs({
      id: '1234567890',
      type: 'testing-get-endpoint',
      status: 'complete-success'
    }).save();
  });

  beforeEach(() => {
    statusSpy = sinon.spy(res, 'status');
  });

  afterEach(() => {
    statusSpy.restore();
  });

  it('should return a 400 if the job id is not found', async () => {
    const invalidIdReq = {
      params: {
        id: '0'
      }
    };
    await getQueuedJobStatus(invalidIdReq, res);
    expect(statusSpy).to.have.been.calledWith(404);
  });

  it('should return status code 200 for a valid job id', async () => {
    await getQueuedJobStatus(req, res);
    expect(statusSpy).to.have.been.calledWith(200);
  });

  it('should return status code 500 for errors', async () => {
    sinon.stub(JobQueueService, 'getJobById').callsFake(() => { throw new Error('fake db error'); });
    await getQueuedJobStatus(req, res);
    expect(statusSpy).to.have.been.calledWith(500);
    JobQueueService.getJobById.restore();
  });
});
