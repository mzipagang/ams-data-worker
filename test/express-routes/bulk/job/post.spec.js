const JobQueueService = require('../../../../app/services/JobQueueService');
const { handler } = require('../../../../app/express-routes/api/v1/bulk/job/post');
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);

describe('Express - /api/v1/bulk/job/post', () => {
  const req = {
    body: {
      type: 'test-job-create-type',
      parameters: {
        year: 2018
      }
    }
  };
  const res = {
    status: () => res,
    json: () => res
  };
  let statusSpy;

  beforeEach(() => {
    statusSpy = sinon.spy(res, 'status');
  });

  afterEach(() => {
    statusSpy.restore();
  });

  it('should create a job entry and return a 202', async () => {
    await handler(req, res);
    expect(statusSpy).to.have.been.calledWith(202);
  });

  it('should return a 400 if request parameters are bad', async () => {
    const invalidRequestParams = {
      body: {
        type: null
      }
    };
    await handler(invalidRequestParams, res);
    expect(statusSpy).to.have.been.calledWith(400);
  });

  it('should return a 500 for a mongo error in creating job', async () => {
    sinon.stub(JobQueueService, 'createJob').callsFake(() => { throw new Error('fake db error'); });
    await handler(req, res);
    expect(statusSpy).to.have.been.calledWith(500);
    JobQueueService.createJob.restore();
  });
});
