const environment = require('../environment');
const express = require('express');
const request = require('supertest');
const chai = require('chai');

const expect = chai.expect;

describe('Express - /api/v1/get.js', () => {
  const requireAPIGet = () => require('../../app/express-routes/api/v1/get.js').handler;

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/express-routes/api/v1/get.js', '../../app/config']);
      environment.use('dev');
    });

    it('should properly load', () => {
      const route = requireAPIGet();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should respond to a request', () => {
      const route = requireAPIGet();
      const app = express();
      app.use(route);

      return request(app)
        .get('/')
        .send()
        .expect(200)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/express-routes/api/v1/get.js', '../../app/config']);
      environment.use('production');
    });

    it('should properly load', () => {
      const route = requireAPIGet();
      expect(route).to.not.be.undefined;
      expect(route).to.satisfy(r => r instanceof Function);
    });

    it('should respond to a request', () => {
      const route = requireAPIGet();
      const app = express();
      app.use(route);

      return request(app)
        .get('/')
        .send()
        .expect(200)
        .then((res) => {
          const data = res.body;
          expect(data.message).to.not.be.undefined;
        });
    });
  });
});
