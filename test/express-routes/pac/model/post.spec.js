const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
const bodyParser = require('body-parser');
const Model = require('../../../../app/services/mongoose/models/pac_model');
const DataLog = require('../../../../app/services/mongoose/models/data_log');
require('../../../../app/services/mongoose');

const expect = chai.expect;

const app = express();
app.use(bodyParser.json());
app.use('/', require('../../../../app/express-routes/api/v1/pac/model/post').handler);

const modelValues = {
  id: '99',
  name: 'foobar',
  team_lead_name: 'Leader',
  advanced_apm_flag: 'Y',
  mips_apm_flag: 'Y',
  quality_reporting_category_code: '2',
  start_date: '2018-08-15T04:00:00.000Z',
  end_date: '2018-08-15T04:00:00.000Z',
  model_category: {
    text: 'this is the freeform text',
    value: 'test'
  },
  group_center: [
    'test',
    'test2'
  ],
  group_cmmi: 'cmmi',
  waivers: [
    {
      text: 'this is the freeform text',
      value: 'test'
    }
  ],
  target_participant: [
    {
      text: 'this is the freeform text',
      value: 'test'
    }
  ],
  target_provider: [
    {
      text: 'this is the freeform text',
      value: 'test'
    }
  ],
  statutory_authority_tier_1: 'test1',
  statutory_authority_tier_2: 'test2',
  target_beneficiary: [
    {
      text: 'this is the freeform text2',
      value: 'test'
    },
    {
      text: 'this is the freeform text3',
      value: 'test'
    }
  ]
};

describe('PAC Model Creation', () => {
  before(() => {
    sinon.stub(Model.prototype, 'save');
    sinon.stub(Model, 'find');
    sinon.stub(Model, 'createNewPacModelId');
    sinon.stub(DataLog, 'createLog');
    Model.find.returns({ lean: () => Promise.resolve([]) });
  });

  afterEach(() => {
    Model.prototype.save.resetHistory();
    Model.find.resetHistory();
    Model.createNewPacModelId.resetHistory();
    DataLog.createLog.resetHistory();
  });

  after(() => {
    // Model.prototype.save.reset();
    // Model.find.resetHistory();
    // Model.createNewPacModelId.restore();
    // DataLog.createLog.reset();

    Model.prototype.save.restore();
    Model.find.restore();
    Model.createNewPacModelId.restore();
    DataLog.createLog.restore();
  });

  it('should save the model', (done) => {
    const expectedValue = {};
    Model.prototype.save.resolves(expectedValue);
    DataLog.createLog.resolves(expectedValue);
    Model.createNewPacModelId.resolves('11');

    request(app)
      .post('/')
      .send(modelValues)
      .expect('Content-Type', /json/)
      .expect({ pac_model: expectedValue })
      .expect(200)
      .end((err) => {
        if (err) {
          return done(err);
        }
        expect(Model.prototype.save).to.have.been.called;
        return done();
      });
  });

  it('should handle errors', (done) => {
    Model.prototype.save.rejects();

    request(app)
      .post('/')
      .send(modelValues)
      .expect('Content-Type', /json/)
      .expect(500)
      .expect({ message: 'There was an error while creating a new PAC Model' })
      .end(err => done(err));
  });

  it('should handle request with start and end dates', (done) => {
    const expectedValue = {};
    Model.prototype.save.resolves(expectedValue);
    DataLog.createLog.resolves(expectedValue);
    Model.createNewPacModelId.resolves('11');

    modelValues.start_date = '2017-01-01T00:00:00.000Z';
    modelValues.end_date = '2018-01-01T00:00:00.000Z';
    request(app)
      .post('/')
      .send(modelValues)
      .expect('Content-Type', /json/)
      .expect({ pac_model: expectedValue })
      .expect(200)
      .end((err) => {
        if (err) {
          return done(err);
        }
        expect(Model.prototype.save).to.have.been.called;
        return done();
      });
  });

  it('should create a new model id (apmId) if none is provided', (done) => {
    modelValues.id = '';
    const expectedValue = {};
    Model.prototype.save.resolves(expectedValue);
    DataLog.createLog.resolves(expectedValue);
    Model.createNewPacModelId.resolves('11');

    request(app)
      .post('/')
      .send(modelValues)
      .expect('Content-Type', /json/)
      .expect({ pac_model: expectedValue })
      .expect(200)
      .end((err) => {
        if (err) {
          return done(err);
        }
        expect(Model.createNewPacModelId).to.have.been.called;
        return done();
      });
  });


  it('should handle array-type values for targets', (done) => {
    const m = new Model(modelValues);
    expect(m.target_provider[0]).to.exist;
    expect(m.group_center.length).equals(2);
    done();
  });
  it('should use freeFormSchema', (done) => {
    const m = new Model(modelValues);
    expect(m.target_provider[0]._id).to.exist;
    done();
  });

  it('should allow select values to be created', (done) => {
    const m = new Model(modelValues);
    expect(m.model_category).to.exist;
    expect(m.group_center).to.exist;
    expect(m.group_cmmi).to.exist;
    expect(m.waivers).to.exist;
    expect(m.target_participant).to.exist;
    expect(m.target_provider).to.exist;
    expect(m.statutory_authority_tier_1).to.exist;
    expect(m.statutory_authority_tier_2).to.exist;
    expect(m.target_beneficiary).to.exist;
    done();
  });

  it('should handle error when not passed correct data', (done) => {
    request(app)
      .post('/')
      .send({ id: 12 })
      .expect('Content-Type', /json/)
      .expect(422)
      .end(err => done(err));
  });

  it('should fail when trying to enter a new model with already existing id', (done) => {
    const expectedValue = {};
    Model.prototype.save.resolves(expectedValue);
    DataLog.createLog.resolves(expectedValue);

    modelValues.id = '10';

    Model.find.returns({ lean: () => Promise.resolve([{ id: '10' }]) });

    request(app)
      .post('/')
      .send(modelValues)
      .then(() => {
        request(app)
          .post('/')
          .send(modelValues)
          .expect(409, done);
      });
  });
});
