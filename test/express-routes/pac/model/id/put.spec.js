const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
const bodyParser = require('body-parser');
const Model = require('../../../../../app/services/mongoose/models/pac_model');
const DataLog = require('../../../../../app/services/mongoose/models/data_log');

const expect = chai.expect;

const app = express();
app.use(bodyParser.json());
app.use('/:apmId', require('../../../../../app/express-routes/api/v1/pac/model/id/put').handler);


describe('PAC Model Updating', () => {
  const updatedModel = {
    id: '99',
    name: 'Foobar',
    team_lead_name: 'Leader',
    advanced_apm_flag: 'N',
    mips_apm_flag: 'N',
    quality_reporting_category_code: '3',
    start_date: '2018-08-15T04:00:00.000Z',
    end_date: '2018-08-15T04:00:00.000Z',
    user: {
      username: 'testuser'
    }
  };
  before(() => {
    sinon.stub(Model, 'findOneAndUpdate');
    sinon.stub(DataLog, 'createLog');
  });

  afterEach(() => {
    Model.findOneAndUpdate.resetHistory();
    DataLog.createLog.resetHistory();
  });

  after(() => {
    Model.findOneAndUpdate.restore();
    DataLog.createLog.restore();
  });

  it('should update the given id', (done) => {
    Model.findOneAndUpdate.resolves(updatedModel);
    DataLog.createLog.resolves({});

    request(app)
      .put('/10')
      .send(updatedModel)
      .expect('Content-Type', /json/)
      .expect(200)
      .expect({ result: updatedModel })
      .end((err) => {
        if (err) {
          return done(err);
        }

        expect(Model.findOneAndUpdate).to.have.been.always.calledWith({ id: '10' });

        return done();
      });
  });

  it('should handle request with start and end dates', (done) => {
    Model.findOneAndUpdate.resolves(updatedModel);
    DataLog.createLog.resolves({});
    updatedModel.start_date = '2017-01-01T00:00:00.000Z';
    updatedModel.end_date = '2018-01-01T00:00:00.000Z';

    request(app)
      .put('/10')
      .send(updatedModel)
      .expect(200)
      .expect({ result: updatedModel })
      .end((err) => {
        if (err) {
          return done(err);
        }
        expect(Model.findOneAndUpdate).to.have.been.called;
        return done();
      });
  });

  it('should handle errors', (done) => {
    Model.findOneAndUpdate.rejects();

    request(app)
      .put('/10')
      .send(updatedModel)
      .expect(500)
      .expect({ message: 'There was an error while updating the PAC Model' })
      .end(err => done(err));
  });

  it('should handle error when not passed correct data', (done) => {
    request(app)
      .post('/10')
      .send({ id: 12 })
      .expect('Content-Type', /json/)
      .expect(422)
      .end(err => done(err));
  });
});
