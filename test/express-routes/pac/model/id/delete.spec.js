const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
const bodyParser = require('body-parser');
const PacModel = require('../../../../../app/services/mongoose/models/pac_model');
const DataLog = require('../../../../../app/services/mongoose/models/data_log');

const expect = chai.expect;

const app = express();
app.use(bodyParser.json());
app.use('/:apmId', require('../../../../../app/express-routes/api/v1/pac/model/id/delete').handler);

describe('PAC Model Deletion', () => {
  before(() => {
    sinon.stub(PacModel, 'findOneAndRemove');
    sinon.stub(DataLog, 'create');
  });

  afterEach(() => {
    PacModel.findOneAndRemove.resetHistory();
    DataLog.create.resetHistory();
  });

  after(() => {
    PacModel.findOneAndRemove.restore();
    DataLog.create.restore();
  });

  it('should delete the given id', (done) => {
    PacModel.findOneAndRemove.resolves(new PacModel({
      id: '22',
      name: 'Name',
      advanced_apm_flag: 'Y',
      mips_apm_flag: 'Y',
      quality_reporting_category_code: '1'
    }));

    DataLog.create.resolves({});

    request(app)
      .delete('/10')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect({ success: true })
      .end((err) => {
        if (err) {
          return done(err);
        }
        expect(PacModel.findOneAndRemove).to.have.been.calledWith({ id: '10' });
        return done();
      });
  });

  it('should handle errors', () => {
    PacModel.findOneAndRemove.rejects();

    return request(app)
      .delete('/10')
      .expect('Content-Type', /json/)
      .expect(500)
      .expect({ message: 'There was an error while deleting the PAC Model' });
  });
});
