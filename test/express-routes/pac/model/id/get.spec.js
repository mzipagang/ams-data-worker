const express = require('express');
const request = require('supertest');
const sinon = require('sinon');
const bodyParser = require('body-parser');
const Model = require('../../../../../app/services/mongoose/models/pac_model');
const DataLog = require('../../../../../app/services/mongoose/models/data_log');

const app = express();
app.use(bodyParser.json());
app.use('/:id', require('../../../../../app/express-routes/api/v1/pac/model/id/get').handler);

describe('PAC Model Retreiveing', () => {
  const updatedModel = {
    id: '99',
    name: 'Foobar',
    advanced_apm_flag: 'N',
    mips_apm_flag: 'N',
    quality_reporting_category_code: '3'
  };

  before(() => {
    sinon.stub(Model, 'findOne');
    sinon.stub(DataLog, 'createLog');
  });

  afterEach(() => {
    Model.findOne.resetHistory();
    DataLog.createLog.resetHistory();
  });

  after(() => {
    Model.findOne.restore();
    DataLog.createLog.restore();
  });

  it('should get the given model by id', (done) => {
    Model.findOne.resolves(updatedModel);
    DataLog.createLog.resolves({});

    request(app)
      .get('/99')
      .expect(200, done);
  });

  it('should return null if model doesn\'t exist by id', (done) => {
    Model.findOne.resolves(null);
    DataLog.createLog.resolves({});

    request(app)
      .get('/99')
      .expect(200, {
        pac_model: null
      }, done);
  });
});
