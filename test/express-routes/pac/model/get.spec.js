const express = require('express');
const request = require('supertest');
const sinon = require('sinon');
const bodyParser = require('body-parser');
const Model = require('../../../../app/services/mongoose/models/pac_model');

const app = express();
app.use(bodyParser.json());
app.use('/', require('../../../../app/express-routes/api/v1/pac/model/get').handler);

describe('PAC Model Retrieval', () => {
  before(() => {
    sinon.stub(Model, 'find');
  });

  afterEach(() => {
    Model.find.resetHistory();
  });

  after(() => {
    Model.find.restore();
  });

  it('should retrieve all the models and count', (done) => {
    const expectedValue = {};
    Model.find.resolves([expectedValue]);

    request(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect({ pac_models: [expectedValue] })
      .expect(200)
      .end((err) => {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it('should handle errors', (done) => {
    Model.find.rejects();

    request(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(500)
      .expect({ message: 'There was an error while retrieving a list of PAC Models' })
      .end(err => done(err));
  });
});
