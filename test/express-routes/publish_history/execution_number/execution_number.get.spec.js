const express = require('express');
const environment = require('../../../environment');
const request = require('supertest');
const bodyParser = require('body-parser');
const chai = require('chai');
const publish_history = require('../../../../app/services/mongoose/models/publish_history');

const V1_PATH = '/api/v1/publish_history/execution_number';

const app = express();
app.use(bodyParser.json());
app.use(V1_PATH, require('../../../../app/express-routes/api/v1/publish_history/execution_number/get').handler);

describe('Publish History Execution Number', () => {

  const expect = chai.expect;

  const publishHistoryEntry = {
    id: '20ba039e-48da-587b-86e9-8d3b99b6d18b',
    file_import_group_id: 'test',
    published_at: new Date(),
    files: [
      {
        file_id: '1',
        import_file_type: 'pac-model',
        apm_id: 'test',
        status: 'test',
        new_file: true,
        snapshot: 'P',
        execution: '1'
      }
    ],
    group_type: 'test',
    performance_year: 2018,
    status: 'test',
    cache: {
      status: 'test',
      expected: 0,
      finished: 0
    }
  };

  beforeEach(() => {
    environment.use('dev');
  });

  before(async () => {
    try {
      await (new publish_history(publishHistoryEntry)).save()
    } catch (err) {
      // ???
    }
  });

  after(async () => {
    try {
      await publish_history.remove({ id: publishHistoryEntry.id })
    } catch (err) {
      // ???
    }
  });

  it('no params, respond 400', (done) => {
    request(app)
      .get(V1_PATH)
      .expect('Content-Type', /json/)
      .expect(400)
      .end(err => done(err));
  });

  it('no performance_year, responds 400', (done) => {
    request(app)
      .get(`${V1_PATH}?snapshot=123&import_file_type=pac-model`)
      .expect('Content-Type', /json/)
      .expect(400)
      .end(err => done(err));
  });

  it('performance_year < 2017 responds 400', (done) => {
    request(app)
      .get(`${V1_PATH}?performance_year=2016&snapshot=123&import_file_type=pac-model`)
      .expect('Content-Type', /json/)
      .expect(400)
      .end(err => done(err));
  });

  it('no snapshot, responds 400', (done) => {
    request(app)
      .get(`${V1_PATH}?performance_year=2017&import_file_type=pac-model`)
      .expect('Content-Type', /json/)
      .expect(400)
      .end(err => done(err));
  });

  it('no import_file_type, responds 400', (done) => {
    request(app)
      .get(`${V1_PATH}?performance_year=2017&snapshot=123`)
      .expect('Content-Type', /json/)
      .expect(400)
      .end(err => done(err));
  });

  it('no existing execution, responds 200, 0 execution', (done) => {
    request(app)
      .get(`${V1_PATH}?snapshot=123&performance_year=2017&import_file_type=pac-model`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        const body = res.body;
        expect(body.execution).to.eql(0);
        done();
      });
  });

  it ('should find last execution number', (done) => {
    request(app)
      .get(`${V1_PATH}?snapshot=P&performance_year=2018&import_file_type=pac-model`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        const body = res.body;
        expect(body.execution).to.eql(1);
        done();
      });
  });
});
