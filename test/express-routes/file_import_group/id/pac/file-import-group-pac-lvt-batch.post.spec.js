const environment = require('../../../../environment');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const API = require(
  '../../../../../app/express-routes/api/v1/file_import_group/id/file/file_id/pac/lvt/batch/post'
).handler;
const FileImportGroup = require('../../../../../app/services/mongoose/models/file_import_group');
const DataLog = require('../../../../../app/services/mongoose/models/data_log');
const PacLvtDynamic = require('../../../../../app/services/mongoose/dynamic_name_models/pac_lvt');

const file = {
  id: '123456789',
  import_file_type: 'pac-lvt'
};

const fileImportGroup = {
  id: '1234',
  status: 'open',
  files: [
    file
  ]
};

const lvt = {
  apm_entity_id: '1',
  apm_lvt_status: '2',
  lvt_payments: 3,
  lvt_patients: 4
};

const DynamicNamePacLvt = PacLvtDynamic(file.id);

describe('Express - /api/v1/file_import_group/id/pac/lvt/batch/post.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    let FileImportGroupMock;

    before(() => {
      sinon.stub(DynamicNamePacLvt, 'insertMany').callsFake(datas =>
        Promise.resolve(datas.map(data => new DynamicNamePacLvt(data))));
      sinon.stub(DataLog, 'createLogBatch').callsFake(() => Promise.resolve());
    });

    after(() => {
      DynamicNamePacLvt.insertMany.restore();
      DataLog.createLogBatch.restore();
    });

    beforeEach(() => {
      FileImportGroupMock = sinon.mock(FileImportGroup);
    });

    afterEach(() => {
      DynamicNamePacLvt.insertMany.resetHistory();
      DataLog.createLogBatch.resetHistory();
      FileImportGroupMock.restore();
    });

    it('should receive a 404 if no file import group is found', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(null);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send()
        .expect(404)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should receive a 400 if file import group is not open', () => {
      const publishedFileImportGroup = Object.assign({}, fileImportGroup, {
        status: 'publish'
      });
      FileImportGroupMock
        .expects('findOne').withArgs({ id: publishedFileImportGroup.id })
        .resolves(publishedFileImportGroup);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${publishedFileImportGroup.id}/file/${file.id}`)
        .send()
        .expect(400)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should insert many', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send({
          data: {
            meta: null,
            values: [
              {
                meta: null,
                value: lvt
              }
            ]
          }
        })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
        });
    });

    it('should handle an error', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .rejects(new Error('Fake Error'));

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send({
          data: {
            meta: null,
            values: [
              {
                meta: null,
                value: lvt
              }
            ]
          }
        })
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });
});
