const environment = require('../../../../environment');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const API =
  require('../../../../../app/express-routes/api/v1/file_import_group/id/file/file_id/pac/qp_score/batch/post').handler;
const FileImportGroup = require('../../../../../app/services/mongoose/models/file_import_group');
const DataLog = require('../../../../../app/services/mongoose/models/data_log');
const PacQpScoreDynamic = require('../../../../../app/services/mongoose/dynamic_name_models/pac_qp_score');

const file = {
  id: '123456789',
  import_file_type: 'pac-qp_score'
};

const fileImportGroup = {
  id: '1234',
  status: 'open',
  files: [
    file
  ]
};

const qpScore = {
  apm_entity_id: '1',
  provider_tin: '1234',
  provider_npi: '4321',
  payment_threshold_score: 5,
  patient_threshold_score: 10
};
const qpScoreIndv = {
  entity_id: '',
  provider_tin: '',
  provider_npi: '4321',
  payment_threshold_score: 5,
  patient_threshold_score: 10
};

const qpScoreEntity = {
  entity_id: '1',
  provider_tin: '1234',
  tin: '12345',
  provider_npi: '4321',
  payment_threshold_score: 5,
  patient_threshold_score: 10
};

const DynamicNamePacQpScore = PacQpScoreDynamic(file.id);

describe('Express - /api/v1/file_import_group/id/pac/qpScore/batch/post.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    let FileImportGroupMock;

    before(() => {
      sinon.stub(DynamicNamePacQpScore, 'insertMany').callsFake(datas =>
        Promise.resolve(datas.map(data => new DynamicNamePacQpScore(data))));
      sinon.stub(DataLog, 'createLogBatch').callsFake(() => Promise.resolve());
    });

    after(() => {
      DynamicNamePacQpScore.insertMany.restore();
      DataLog.createLogBatch.restore();
    });

    beforeEach(() => {
      FileImportGroupMock = sinon.mock(FileImportGroup);
    });

    afterEach(() => {
      DynamicNamePacQpScore.insertMany.resetHistory();
      DataLog.createLogBatch.resetHistory();
      FileImportGroupMock.restore();
    });

    it('should receive a 404 if no file import group is found', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(null);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send()
        .expect(404)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should receive a 400 if file import group is not open', () => {
      const publishedFileImportGroup = Object.assign({}, fileImportGroup, {
        status: 'publish'
      });
      FileImportGroupMock
        .expects('findOne').withArgs({ id: publishedFileImportGroup.id })
        .resolves(publishedFileImportGroup);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${publishedFileImportGroup.id}/file/${file.id}`)
        .send()
        .expect(400)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should insert many', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send({
          data: {
            meta: null,
            values: [
              {
                meta: null,
                value: qpScore
              }
            ]
          }
        })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
        });
    });

    it('should insert individual score', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send({
          data: {
            meta: null,
            values: [
              {
                meta: null,
                value: qpScoreIndv
              }
            ]
          }
        })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
        });
    });

    it('should detect individual score type', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send({
          data: {
            meta: null,
            values: [
              {
                meta: null,
                value: qpScoreIndv
              }
            ]
          }
        })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(DynamicNamePacQpScore.insertMany).calledWith([{
            entity_id: '',
            patient_threshold_score: 10,
            payment_threshold_score: 5,
            provider_npi: '4321',
            provider_tin: '',
            score_type: 'I',
            tin: undefined
          }]);
        });
    });
    it('should detect entity score type', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send({
          data: {
            meta: null,
            values: [
              {
                meta: null,
                value: qpScoreEntity
              }
            ]
          }
        })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(DynamicNamePacQpScore.insertMany).calledWith([{
            entity_id: '1',
            patient_threshold_score: 10,
            payment_threshold_score: 5,
            provider_npi: '4321',
            provider_tin: '1234',
            score_type: 'E',
            tin: '12345'
          }]);
        });
    });

    it('should handle an error', () => {
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .rejects(new Error('Fake Error'));

      const app = express();
      app.use(bodyParser.json({ limit: '50mb' }));
      app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
      const router = express.Router();
      router.post('/:id/file/:file_id', API);
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}/file/${file.id}`)
        .send({
          data: {
            meta: null,
            values: [
              {
                meta: null,
                value: qpScore
              }
            ]
          }
        })
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });
});
