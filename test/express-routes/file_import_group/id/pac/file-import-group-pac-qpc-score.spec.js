const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const { handler } = require('../../../../../app/express-routes/api/v1/file_import_group/id/file/file_id/pac/qpc_score/batch/post');
const FileImportGroup = require('../../../../../app/services/mongoose/models/file_import_group');

const DynamicPacQpcScore = require('../../../../../app/services/mongoose/dynamic_name_models/pac_qp_category_score');
const DynamicPacQpcScoreSummary = require('../../../../../app/services/mongoose/dynamic_name_models/pac_qp_category_score_summary');

const file = {
  id: '123456789',
  status: 'processing',
  import_file_type: 'pac-qpc_score'
};

const fileImportGroup = {
  id: '1234',
  status: 'open',
  files: [
    file
  ]
};

const createFile = (overrides) => ({
  id: '123456789',
  status: 'processing',
  import_file_type: 'pac-qpc_score',
  ...overrides
});
const createFileImportGroup = (overrides) => ({
  id: '1234',
  status: 'open',
  files: [createFile()],
  ...overrides
});
const createDetailRecord = (overrides) => ({ ...overrides });
const createSummaryRecord = (overrides) => ({
  final_qpc_score: '123.1',
  ...overrides
});

const PacQpcScore = DynamicPacQpcScore(file.id);
const PacQpcScoreSummary = DynamicPacQpcScoreSummary(file.id);

const createServer = () => {
  const server = express();
  server.use(bodyParser.json({ limit: '50mb' }));
  server.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
  server.post('/:id/file/:file_id', handler);
  return server;
}

describe('PAC QP Category Score Batch Routes', () => {

  let FileImportGroupMock;

  before(() => {
    sinon.stub(PacQpcScore, 'insertMany').callsFake(datas =>
      Promise.resolve(datas.map(data => new PacQpcScore(data))));
    sinon.stub(PacQpcScoreSummary, 'insertMany').callsFake(datas =>
      Promise.resolve(datas.map(data => new PacQpcScoreSummary(data))));
  });

  after(() => {
    PacQpcScore.insertMany.restore();
    PacQpcScoreSummary.insertMany.restore();
  });

  beforeEach(() => {
    FileImportGroupMock = sinon.mock(FileImportGroup);
  });

  afterEach(() => {
    PacQpcScore.insertMany.resetHistory();
    PacQpcScoreSummary.insertMany.resetHistory();
    FileImportGroupMock.restore();
  });

  it('should receive a 404 if no file import group is found', () => {
    FileImportGroupMock
      .expects('findOne').withArgs({ id: fileImportGroup.id })
      .resolves(null);

    const server = createServer();

    return request(server)
      .post(`/${fileImportGroup.id}/file/${file.id}`)
      .send()
      .expect(404)
      .then((res) => {
        const body = res.body;
        expect(body).to.not.be.undefined;
        expect(body.message).to.not.be.undefined;
      });
  });

  it('should receive a 400 if file import group is not open', () => {
    const publishedFileImportGroup = Object.assign({}, fileImportGroup, {
      status: 'publish'
    });
    FileImportGroupMock
      .expects('findOne').withArgs({ id: publishedFileImportGroup.id })
      .resolves(publishedFileImportGroup);

    const server = createServer();

    return request(server)
      .post(`/${publishedFileImportGroup.id}/file/${file.id}`)
      .send()
      .expect(400)
      .then((res) => {
        const body = res.body;
        expect(body).to.not.be.undefined;
        expect(body.message).to.not.be.undefined;
      });
  });

  it('should return a 404 if the file does not exist', () => {
    FileImportGroupMock
    .expects('findOne').withArgs({ id: fileImportGroup.id })
    .resolves(fileImportGroup);

    const server = createServer();

    return request(server)
    .post(`/${fileImportGroup.id}/file/xxxxx`)
    .send()
    .expect(404)
    .then((res) => {
      expect(res && res.body && res.body.message).to.equal('File does not exist');
    });
  });

  it('should return a 400 if the file is not accepting new data', () => {
    const fileNotProcessing = createFile({ status: 'not-processing' });
    const fileImportGroup = createFileImportGroup({ files: [fileNotProcessing] });

    FileImportGroupMock
    .expects('findOne').withArgs({ id: fileImportGroup.id })
    .resolves(fileImportGroup);

    const server = createServer();

    return request(server)
    .post(`/${fileImportGroup.id}/file/${fileNotProcessing.id}`)
    .send()
    .expect(400)
    .then((res) => {
      expect(res && res.body && res.body.message).to.equal('This file is no longer accepting new data');
    });
  });

  it('should insert both detail and summary records', () => {
    const fileImportGroup = createFileImportGroup();
    const file = fileImportGroup.files[0];

    FileImportGroupMock
      .expects('findOne').withArgs({ id: fileImportGroup.id })
      .resolves(fileImportGroup);

    const server = createServer();

    const detailRecords = [
      {
        value: createDetailRecord(),
        meta: 'foo'
      },
      {
        value: createDetailRecord(),
        meta: 'foo'
      }
    ];
    const summaryRecords = [
      {
        value: createSummaryRecord({ final_qpc_score: '123.4' }),
        meta: 'foo'
      }
    ];

    return request(server)
      .post(`/${fileImportGroup.id}/file/${file.id}`)
      .send({
        data: {
          meta: null,
          values: [...detailRecords, ...summaryRecords]
        }
      })
      .expect(200)
      .then((res) => {
        const body = res.body;
        expect(body.data.values).to.eql([ { success: true }, { success: true }, { success: true }]);
        expect(PacQpcScore.insertMany).to.have.been.calledOnceWith([ { file_id: file.id, meta: 'foo' }, {file_id: file.id, meta: 'foo' } ]);
        expect(PacQpcScoreSummary.insertMany).to.have.been.calledOnceWith([ {file_id: file.id, final_qpc_score: '123.4', meta: 'foo' } ]);
      });
  });

  it('should handle an error', () => {
    FileImportGroupMock
      .expects('findOne').withArgs({ id: fileImportGroup.id })
      .rejects(new Error('Fake Error'));

    const server = createServer();

    return request(server)
      .post(`/${fileImportGroup.id}/file/${file.id}`)
      .send({
        data: {
          meta: null,
          values: [
            {
              meta: null,
              value: {}
            }
          ]
        }
      })
      .expect(500)
      .then((res) => {
        expect(res && res.body && res.body.message).to.equal('Error while importing data');
      });
  });
});
