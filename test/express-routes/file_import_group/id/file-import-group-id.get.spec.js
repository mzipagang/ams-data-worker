const environment = require('../../../environment');
const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const API = require('../../../../app/express-routes/api/v1/file_import_group/id/get.js').handler;
const FileImportGroup = require('../../../../app/services/mongoose/models/file_import_group');

describe('Express - /api/v1/file_import_group/get.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    let FileImportGroupMock;

    beforeEach(() => {
      FileImportGroupMock = sinon.mock(FileImportGroup);
    });

    afterEach(() => {
      FileImportGroupMock.restore();
    });

    it('should succeed', () => {
      const fileImportGroup = {
        id: '1234'
      };
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);

      const app = express();
      const router = express.Router();
      router.get('/:id', API);
      app.use(router);

      return request(app)
        .get(`/${fileImportGroup.id}`)
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_group).to.not.be.undefined;
          expect(body.data.file_import_group).to.deep.equal(fileImportGroup);
        });
    });

    it('should handle an error', () => {
      const fileImportGroup = {
        id: '1234'
      };
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .rejects(new Error('Fake error'));

      const app = express();
      const router = express.Router();
      router.get('/:id', API);
      app.use(router);

      return request(app)
        .get(`/${fileImportGroup.id}`)
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });
});
