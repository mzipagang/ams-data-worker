const environment = require('../../../../../../../../environment');
const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');
const bodyParser = require('body-parser');


const expect = chai.expect;
const rewire = require('rewire');
const Promise = require('bluebird');

const apiPath = 'app/express-routes/api/v1/file_import_group/id/file/file_id/apm/entity/batch/_post.js';
const API = rewire(`../../../../../../../../../${apiPath}`);
const FileImportGroup = require('../../../../../../../../../app/services/mongoose/models/file_import_group');

const file = {
  id: '12',
  status: 'processing'
};

const fileImportGroup = {
  id: '1234',
  status: 'open',
  files: [
    file
  ]
};

API.__set__('bulkUpsert', () => new Promise(resolve => resolve('some resolve')));

describe('Express - /api/v1/file_import_group/id/file/file_id/apm/entity/batch/post.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    describe('when fileImportGroup is not found', () => {
      before(() => {
        sinon.stub(FileImportGroup, 'findOne').resolves(null);
      });

      after(() => {
        FileImportGroup.findOne.restore();
      });

      it('should return a 404 with message `File Import Group does not exist`', (done) => {
        const app = express();
        app.use(API);

        request(app)
          .post('/')
          .send()
          .expect(404)
          .then((res) => {
            const body = res.body;
            expect(body).to.not.be.undefined;
            expect(body.data).to.be.undefined;
            expect(body.message).to.not.be.undefined;
            expect(body.message).to.equal('File Import Group does not exist');
            done();
          });
      });
    });

    describe('when fileImportGroup status is `open`', () => {
      before(() => {
        sinon.stub(FileImportGroup, 'findOne').resolves(new FileImportGroup({ status: 'pending' }));
      });

      after(() => {
        FileImportGroup.findOne.restore();
      });

      it('should return a 400 with message `This file import group is no longer accepting new data`', (done) => {
        const app = express();
        app.use(API);

        request(app)
          .post('/')
          .send()
          .expect(400)
          .then((res) => {
            const body = res.body;
            expect(body).to.not.be.undefined;
            expect(body.data).to.be.undefined;
            expect(body.message).to.not.be.undefined;
            expect(body.message).to.equal('This file import group is no longer accepting new data');
            done();
          });
      });
    });

    describe('when file id is not found', () => {
      before(() => {
        sinon.stub(FileImportGroup, 'findOne').resolves(new FileImportGroup({ status: 'open', files: [{ id: 12 }] }));
      });

      after(() => {
        FileImportGroup.findOne.restore();
      });

      it('should return a 404 with message `File does not exist`', (done) => {
        const app = express();
        app.use(API);

        request(app)
          .post('/')
          .send()
          .expect(404)
          .then((res) => {
            const body = res.body;
            expect(body).to.not.be.undefined;
            expect(body.data).to.be.undefined;
            expect(body.message).to.not.be.undefined;
            expect(body.message).to.equal('File does not exist');
            done();
          });
      });
    });

    describe('when file status is not `processing`', () => {
      before(() => {
        sinon.stub(FileImportGroup, 'findOne')
          .resolves(new FileImportGroup({ status: 'open', files: [{ id: 12, status: 'not processing' }] }));
      });

      after(() => {
        FileImportGroup.findOne.restore();
      });

      it('should return a 400 with message `This file is no longer accepting new data`', (done) => {
        const app = express();
        app.use(bodyParser.json({ limit: '50mb' }));
        app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
        const router = express.Router();
        router.post('/:id/file/:file_id/apm/entity/batch', API);
        app.use(router);

        request(app)
          .post(`/${fileImportGroup.id}/file/${file.id}/apm/entity/batch`)
          .send({
            data: {
              meta: { file_line_number: 67 },
              values: [{ value: {}, meta: {} }]
            }
          })
          .expect(400)
          .then((res) => {
            const body = res.body;
            expect(body).to.not.be.undefined;
            expect(body.data).to.be.undefined;
            expect(body.message).to.equal('This file is no longer accepting new data');
            done();
          });
      });
    });

    describe('upon success', () => {
      before(() => {
        sinon.stub(FileImportGroup, 'findOne')
          .resolves(new FileImportGroup({ status: 'open', files: [{ id: 12, status: 'processing' }] }));
      });

      after(() => {
        FileImportGroup.findOne.restore();
      });

      it('should return a 200 with data values', (done) => {
        const app = express();
        app.use(bodyParser.json({ limit: '50mb' }));
        app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
        const router = express.Router();
        router.post('/:id/file/:file_id/apm/entity/batch', API);
        app.use(router);

        request(app)
          .post(`/${fileImportGroup.id}/file/${file.id}/apm/entity/batch`)
          .send({
            data: {
              meta: { file_line_number: 67 },
              values: [{ value: {}, meta: {} }]
            }
          })
          .expect(200)
          .then((res) => {
            const body = res.body;
            expect(body).to.not.be.undefined;
            expect(body.data).to.not.be.undefined;
            expect(body.data).to.include({ values: 'some resolve' });
            expect(body.message).to.be.undefined;
            done();
          });
      });
    });
    describe('when apm_id is 21', () => {
      before(() => {
        sinon.stub(FileImportGroup, 'findOne')
          .resolves(new FileImportGroup({ files: [{ id: 12, status: 'processing' }] }));
        API.__set__('bulkUpsert', ({}, entityData, fn) => new Promise(resolve => resolve(entityData[0])));
      });

      after(() => {
        FileImportGroup.findOne.restore();
      });

      it('should set subdiv_id to 00', (done) => {
        const app = express();
        app.use(bodyParser.json({ limit: '50mb' }));
        app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
        const router = express.Router();
        router.post('/:id/file/:file_id/apm/entity/batch', API);
        app.use(router);

        request(app)
          .post(`/${fileImportGroup.id}/file/${file.id}/apm/entity/batch`)
          .send({
            data: {
              meta: { file_line_number: 67 },
              values: [{ value: { apm_id: '21', subdiv_id: '99' }, meta: {} }]
            }
          })
          .expect(200)
          .then((res) => {
            const body = res.body;
            expect(body).to.not.be.undefined;
            expect(body.data).to.not.be.undefined;
            expect(body.data.values).to.include({
              apm_id: '21',
              subdiv_id: '00'
            });
            expect(body.message).to.be.undefined;
            done();
          });
      });
    });

    describe('when apm_id is not 21', () => {
      before(() => {
        sinon.stub(FileImportGroup, 'findOne')
          .resolves(new FileImportGroup({ files: [{ id: 12, status: 'processing' }] }));
        API.__set__('bulkUpsert', ({}, entityData, fn) => new Promise(resolve => resolve(entityData[0])));
      });

      after(() => {
        FileImportGroup.findOne.restore();
      });

      it('should set subdiv_id to passed in subdiv_id', (done) => {
        const app = express();
        app.use(bodyParser.json({ limit: '50mb' }));
        app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
        const router = express.Router();
        router.post('/:id/file/:file_id/apm/entity/batch', API);
        app.use(router);

        request(app)
          .post(`/${fileImportGroup.id}/file/${file.id}/apm/entity/batch`)
          .send({
            data: {
              meta: { file_line_number: 67 },
              values: [{ value: { apm_id: '22', subdiv_id: '99' }, meta: {} }]
            }
          })
          .expect(200)
          .then((res) => {
            const body = res.body;
            expect(body).to.not.be.undefined;
            expect(body.data).to.not.be.undefined;
            expect(body.data.values).to.include({
              apm_id: '22',
              subdiv_id: '99'
            });
            expect(body.message).to.be.undefined;
            done();
          });
      });
    });
  });
});
