const environment = require('../../../environment');
const bodyParser = require('body-parser');
const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const API = require('../../../../app/express-routes/api/v1/file_import_group/id/patch.js').handler;
const FileImportGroup = require('../../../../app/services/mongoose/models/file_import_group');
const PublishHistory = require('../../../../app/services/mongoose/models/publish_history');
const PacSnapshot = require('../../../../app/services/mongoose/models/pac_snapshot');

describe('Express - /api/v1/file_import_group/patch.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    let FileImportGroupMock;
    let PublishHistoryMock;
    let PacSnapshotMock;
    let ObjectMock;

    beforeEach(() => {
      sinon.stub(PublishHistory.prototype, 'save');
      sinon.stub(PacSnapshot.prototype, 'save');
      FileImportGroupMock = sinon.mock(FileImportGroup);
      PublishHistoryMock = sinon.mock(PublishHistory);
      PacSnapshotMock = sinon.mock(PacSnapshot);
      ObjectMock = sinon.mock(Object);
    });

    afterEach(() => {
      PublishHistory.prototype.save.resetHistory();
      PacSnapshot.prototype.save.resetHistory();
      PublishHistory.prototype.save.restore();
      PacSnapshot.prototype.save.restore();
      FileImportGroupMock.restore();
      PublishHistoryMock.restore();
      PacSnapshotMock.restore();
      ObjectMock.restore();
    });

    it('should succeed', () => {
      PublishHistory.prototype.save.resolves({});
      PacSnapshot.prototype.save.resolves({});
      const fileImportGroup = {
        id: '1234',
        status: 'open'
      };
      const fileImportGroupUpdated = {
        id: '1234',
        status: 'publish',
        files: [
          { performance_year: 2017,
            run_snapshot: 'F',
            import_file_type: 'pac-subdivision',
            file_id: '777',
            id: '778',
            apm_id: '1234' }
        ]
      };
      const publishedHist = {
        file_import_group_id: fileImportGroup.id,
        status: 'publish',
        files: [{ performance_year: 2017,
          run_snapshot: 'F',
          import_file_type: 'pac-subdivision',
          file_id: '777',
          id: '778',
          apm_id: '1234' }],
        group_type: 'pac',
        performance_year: 2017
      };
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);
      FileImportGroupMock
        .expects('findOneAndUpdate')
        .chain('lean')
        .resolves(fileImportGroupUpdated);
      PublishHistoryMock
        .expects('findOne')
        .chain('sort')
        .resolves(publishedHist);

      const app = express();
      const router = express.Router();
      router.post('/:id', API);
      app.use(bodyParser.json());
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}`)
        .send({ status: 'publish' })
        .expect(202)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_group).to.not.be.undefined;
        });
    });

    it('should do nothing if not publish and not restricted', () => {
      PublishHistory.prototype.save.resolves({});
      PacSnapshot.prototype.save.resolves({});
      const fileImportGroup = {
        id: '1234',
        status: 'open'
      };
      const fileImportGroupUpdated = {
        id: '1234',
        status: 'publish',
        files: [
          { performance_year: 2017,
            run_snapshot: 'F',
            import_file_type: 'pac-subdivision',
            file_id: '777',
            id: '778',
            apm_id: '1234' }
        ]
      };
      const publishedHist = {
        file_import_group_id: fileImportGroup.id,
        status: 'publish',
        files: [{ performance_year: 2017,
          run_snapshot: 'F',
          import_file_type: 'pac-subdivision',
          file_id: '777',
          id: '778',
          apm_id: '1234' }],
        group_type: 'pac',
        performance_year: 2017
      };
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);
      FileImportGroupMock
        .expects('findOneAndUpdate')
        .chain('lean')
        .resolves(fileImportGroupUpdated);
      PublishHistoryMock
        .expects('findOne')
        .chain('sort')
        .resolves(publishedHist);

      const app = express();
      const router = express.Router();
      router.post('/:id', API);
      app.use(bodyParser.json());
      app.use(router);

      return request(app)
        .post(`/${fileImportGroup.id}`)
        .send({ status: 'open2' })
        .expect(202)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_group).to.not.be.undefined;
        });
    });

    it('should reject a restricted status', () => {
      const fileImportGroup = {
        id: '1234',
        status: 'published'
      };
      const fileImportGroupUpdated = {
        id: '1234',
        status: 'publish',
        files: [
          { performance_year: 2017,
            import_file_type: 'pac-subdivision',
            file_id: '777',
            id: '778',
            apm_id: '1234' }
        ]
      };
      const publishedHist = {
        file_import_group_id: fileImportGroup.id,
        status: 'publish',
        files: [{ performance_year: 2017,
          import_file_type: 'pac-subdivision',
          file_id: '777',
          id: '778',
          apm_id: '1234' }],
        group_type: 'pac',
        performance_year: 2017
      };
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .resolves(fileImportGroup);
      FileImportGroupMock
        .expects('findOneAndUpdate')
        .chain('lean')
        .resolves(fileImportGroupUpdated);
      PublishHistoryMock
        .expects('findOne')
        .chain('sort')
        .resolves(publishedHist);

      const app = express();
      const router = express.Router();
      router.patch('/:id', API);
      app.use(router);

      return request(app)
        .patch(`/${fileImportGroup.id}`)
        .send({ status: 'publish' })
        .expect(400)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should handle an error', () => {
      const fileImportGroup = {
        id: '1234'
      };
      FileImportGroupMock
        .expects('findOne').withArgs({ id: fileImportGroup.id })
        .rejects(new Error('Fake error'));

      const app = express();
      const router = express.Router();
      router.patch('/:id', API);
      app.use(router);

      return request(app)
        .patch(`/${fileImportGroup.id}`)
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });
});
