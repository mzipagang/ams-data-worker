const environment = require('../../environment');
const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const API = require('../../../app/express-routes/api/v1/file_import_group/post.js').handler;
const FileImportGroup = require('../../../app/services/mongoose/models/file_import_group');

describe('Express - /api/v1/file_import_group/post.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    before(() => {
      sinon.stub(FileImportGroup.prototype, 'save').resolves(new FileImportGroup());
    });

    after(() => {
      FileImportGroup.prototype.save.restore();
    });

    afterEach(() => {
      FileImportGroup.prototype.save.resetHistory();
    });

    it('should succeed', () => {
      const app = express();
      app.use(API);

      return request(app)
        .post('/')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_group).to.not.be.undefined;
        });
    });

    it('should handle an error', () => {
      FileImportGroup.prototype.save.callsFake(() => Promise.reject(new Error('Fake Error')));
      const app = express();
      app.use(API);

      return request(app)
        .post('/')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });
  });
});
