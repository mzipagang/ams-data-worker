const environment = require('../../environment');
const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const API = require('../../../app/express-routes/api/v1/file_import_group/get.js').handler;
const FileImportGroup = require('../../../app/services/mongoose/models/file_import_group');

describe('Express - /api/v1/file_import_group/get.js', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('on a request', () => {
    let FileImportGroupMock;

    beforeEach(() => {
      FileImportGroupMock = sinon.mock(FileImportGroup);
    });

    afterEach(() => {
      FileImportGroupMock.restore();
    });

    it('should succeed', () => {
      const fileImportGroup = { id: 1234 };
      FileImportGroupMock
        .expects('find')
        .chain('sort').withArgs({ createdAt: -1 })
        .chain('limit')
        .withArgs(10)
        .resolves([
          fileImportGroup
        ]);

      const app = express();
      app.use(API);

      return request(app)
        .get('/')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_groups).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.deep.equal(fileImportGroup);
        });
    });

    it('should handle an error', () => {
      FileImportGroupMock
        .expects('find')
        .chain('sort').withArgs({ createdAt: -1 })
        .chain('limit')
        .withArgs(10)
        .rejects(new Error('fake error'));

      const app = express();
      app.use(API);

      return request(app)
        .get('/')
        .send()
        .expect(500)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.message).to.not.be.undefined;
        });
    });

    it('should add the status that was passed to the find method', () => {
      const fileImportGroup = { id: 1234 };
      FileImportGroupMock
        .expects('find').withArgs({ status: 'open' })
        .chain('sort').withArgs({ createdAt: -1 })
        .chain('limit')
        .withArgs(10)
        .resolves([
          fileImportGroup
        ]);

      const app = express();
      app.use(API);

      return request(app)
        .get('/?status=open')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_groups).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.deep.equal(fileImportGroup);
        });
    });

    it('should use the given limit', () => {
      const fileImportGroup = { id: 1234 };
      FileImportGroupMock
        .expects('find')
        .chain('sort').withArgs({ createdAt: -1 })
        .chain('limit')
        .withArgs(20)
        .resolves([
          fileImportGroup
        ]);

      const app = express();
      app.use(API);

      return request(app)
        .get('/?limit=20')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_groups).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.deep.equal(fileImportGroup);
        });
    });

    it('should use 1000 as the max limit', () => {
      const fileImportGroup = { id: 1234 };
      FileImportGroupMock
        .expects('find')
        .chain('sort').withArgs({ createdAt: -1 })
        .chain('limit')
        .withArgs(1000)
        .resolves([
          fileImportGroup
        ]);

      const app = express();
      app.use(API);

      return request(app)
        .get('/?limit=2000')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_groups).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.deep.equal(fileImportGroup);
        });
    });

    it('should use 10 as the default limit', () => {
      const fileImportGroup = { id: 1234 };
      FileImportGroupMock
        .expects('find')
        .chain('sort').withArgs({ createdAt: -1 })
        .chain('limit')
        .withArgs(10)
        .resolves([
          fileImportGroup
        ]);

      const app = express();
      app.use(API);

      return request(app)
        .get('/?limit=abc')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.file_import_groups).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.not.be.undefined;
          expect(body.data.file_import_groups[0]).to.deep.equal(fileImportGroup);
        });
    });
  });
});
