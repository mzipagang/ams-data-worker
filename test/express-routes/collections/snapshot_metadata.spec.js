const environment = require('../../environment');
const express = require('express');
const Promise = require('bluebird');
const request = require('supertest');
const sinon = require('sinon');
const chai = require('chai');
require('sinon-mongoose');
const bodyParser = require('body-parser');

const expect = chai.expect;

const API = require('../../../app/express-routes/api/v1/snapshot/post').handler;

const mongoose = require('../../../app/services/mongoose');
const PacModel = require('../../../app/services/mongoose/models/pac_model');
const PacSubdivision = require('../../../app/services/mongoose/models/pac_subdivision');
const ApmEntity = require('../../../app/services/mongoose/models/apm_entity');
const ApmProvider = require('../../../app/services/mongoose/models/apm_provider');
const SnapshotMetadata = require('../../../app/services/mongoose/models/snapshot');

describe('Express - /api/v1/collections/snapshot/post.js', () => {
  let EntityMock;
  let ModelMock;
  let ProviderMock;
  let SubdivisionMock;
  let MongooseMock;
  let MockSnapshotMetadata;
  let SnapshotMock;

  beforeEach(() => {
    environment.use('dev');
    ModelMock = sinon.mock(PacModel);
    SubdivisionMock = sinon.mock(PacSubdivision);
    EntityMock = sinon.mock(ApmEntity);
    ProviderMock = sinon.mock(ApmProvider);
    MongooseMock = sinon.mock(mongoose);
    MockSnapshotMetadata = sinon.mock(SnapshotMetadata);
    sinon.stub(SnapshotMetadata.prototype, 'save').resolves();
    SnapshotMock = sinon.mock(SnapshotMetadata);

    EntityMock
      .expects('aggregate')
      .resolves();
    ModelMock
      .expects('aggregate')
      .resolves();
    ProviderMock
      .expects('aggregate')
      .resolves();
    EntityMock
      .expects('aggregate')
      .resolves();
    SubdivisionMock
      .expects('aggregate')
      .resolves();
    MongooseMock
      .expects('model')
      .returns({
        find: () => Promise.resolve(),
        init: () => Promise.resolve(),
        collection: {
          name: 'fakeCollectionName'
        }
      })
      .atLeast(1);
    SnapshotMock.expects('findOne')
      .resolves({
        _id: '5a812d6dd02c99fb77fbe034',
        id: 'b227bdee-dc3a-4194-bbf9-5c0f4905e63a',
        createdBy: 'test-user-1',
        status: 'complete',
        __v: 1,
        collections: [
          {
            name: 'pac_models_snapshot_b227bdee-dc3a-4194-bbf9-5c0f4905e63as',
            type: 'pac-model',
            _id: '5a812d6dd02c99fb77fbe035'
          },
          {
            name: 'pac_subdivisions_snapshot_b227bdee-dc3a-4194-bbf9-5c0f4905e63as',
            type: 'pac-subdivision',
            _id: '5a812d6dd02c99fb77fbe036'
          },
          {
            name: 'apm_entities_snapshot_b227bdee-dc3a-4194-bbf9-5c0f4905e63as',
            type: 'apm-entity',
            _id: '5a812d6dd02c99fb77fbe037'
          },
          {
            name: 'apm_providers_snapshot_b227bdee-dc3a-4194-bbf9-5c0f4905e63as',
            type: 'apm-provider',
            _id: '5a812d6dd02c99fb77fbe038'
          }
        ],
        createdAt: '2018-02-12T06:00:13.906Z'
      });
  });

  afterEach(() => {
    EntityMock.restore();
    ModelMock.restore();
    ProviderMock.restore();
    SubdivisionMock.restore();
    MongooseMock.restore();
    MockSnapshotMetadata.restore();
    SnapshotMetadata.prototype.save.restore();
    SnapshotMock.restore();
  });

  it('should take a request', () => {
    const req = {
      name: 'snapshot_test',
      user: { username: 'test_user' }
    };

    const app = express();
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
    app.use(API);

    return request(app)
      .post('/')
      .send(req)
      .expect(200)
      .then((err) => {
        expect(err).to.not.be.undefined;
        expect(err.body.message).to.be.undefined;
      });
  });
});
