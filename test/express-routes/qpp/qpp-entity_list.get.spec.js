const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const QPPService = require('../../../app/services/qpp');

describe('Express - QPP Entity List', () => {
  let QPPApiEntitylistStub;
  let APIv1;
  let APIv2;
  let APIv3;

  before(() => {
    QPPApiEntitylistStub = sinon.stub(QPPService, 'QPPApiEntitylist');
    APIv1 = require('../../../app/express-routes/api/v1/qpp/entity_list/get.js').handler;
    APIv2 = require('../../../app/express-routes/api/v2/qpp/entity_list/get.js').handler;
    APIv3 = require('../../../app/express-routes/api/v3/qpp/entity_list/get.js').handler;
  });

  afterEach(() => {
    QPPApiEntitylistStub.resetBehavior();
  });

  after(() => {
    QPPApiEntitylistStub.restore();
  });

  describe('/api/v1/qpp/entity_list/get.js', () => {
    afterEach(() => {
      QPPApiEntitylistStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          id: 'A1001',
          name: 'Palm Beach Accountable Care Organization, LLC'
        }
      ];

      QPPApiEntitylistStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should respond handle an error', () => {
      QPPApiEntitylistStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(500);
    });
  });

  describe('/api/v2/qpp/entity_list/get.js', () => {
    afterEach(() => {
      QPPApiEntitylistStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          id: 'A1001',
          name: 'Palm Beach Accountable Care Organization, LLC'
        }
      ];

      QPPApiEntitylistStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should respond handle an error', () => {
      QPPApiEntitylistStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(500);
    });
  });

  describe('/api/v3/qpp/entity_list/get.js', () => {
    afterEach(() => {
      QPPApiEntitylistStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          id: 'A1001',
          name: 'Palm Beach Accountable Care Organization, LLC'
        }
      ];

      QPPApiEntitylistStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body; // eslint-disable-line no-unused-vars
          // expect(body).to.not.be.undefined;
          // expect(body.data).to.not.be.undefined;
          // expect(body.data.apm_data).to.not.be.undefined;
          // expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should respond handle an error', () => {
      QPPApiEntitylistStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send(); // .expect(500);
    });
  });
});
