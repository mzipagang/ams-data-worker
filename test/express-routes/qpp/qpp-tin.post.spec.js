const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
const bodyParser = require('body-parser');
require('sinon-mongoose');

const expect = chai.expect;

const QPPService = require('../../../app/services/qpp');

describe('Express - QPP TIN', () => {
  let QPPApiByTINStub;
  let APIv2;
  let APIv3;

  before(() => {
    QPPApiByTINStub = sinon.stub(QPPService, 'QPPApiByTIN');
    APIv2 = require('../../../app/express-routes/api/v2/qpp/tin/post.js').handler;
    APIv3 = require('../../../app/express-routes/api/v3/qpp/tin/post.js').handler;
  });

  afterEach(() => {
    QPPApiByTINStub.resetBehavior();
  });

  after(() => {
    QPPApiByTINStub.restore();
  });

  describe('/api/v2/qpp/tin/post.js', () => {
    afterEach(() => {
      QPPApiByTINStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          tin: '123456789',
          entities: [
            {
              entity_id: 'A1001',
              entity_name: 'Palm Beach Accountable Care Organization, LLC',
              lvt_flag: 'F',
              lvt_payments: 163848651.97,
              lvt_patients: 147241,
              lvt_small_status: 'Y',
              lvt_performance_year: 2017,
              entity_tin: '123456789',
              npis: [
                {
                  npi: '1234567890',
                  qp_status: 'Y',
                  qp_payment_threshold_score: 1234567,
                  qp_patient_threshold_score: 1234567,
                  provider_relationship_code: 'P'
                }
              ],
              apm_id: '08',
              apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
              subdivision_id: '01',
              subdivision_name: 'MSSP ACO - Track 1',
              advanced_apm_flag: 'Y',
              mips_apm_flag: 'N'
            }
          ]
        }
      ];

      QPPApiByTINStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/?performance_year=2017')
        .send({ tins: ['123456789'] })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should respond to request without performance yeart', () => {
      const response = [
        {
          tin: '123456789',
          entities: [
            {
              entity_id: 'A1001',
              entity_name: 'Palm Beach Accountable Care Organization, LLC',
              lvt_flag: 'F',
              lvt_payments: 163848651.97,
              lvt_patients: 147241,
              lvt_small_status: 'Y',
              lvt_performance_year: 2017,
              entity_tin: '123456789',
              npis: [
                {
                  npi: '1234567890',
                  qp_status: 'Y',
                  qp_payment_threshold_score: 1234567,
                  qp_patient_threshold_score: 1234567,
                  provider_relationship_code: 'P'
                }
              ],
              apm_id: '08',
              apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
              subdivision_id: '01',
              subdivision_name: 'MSSP ACO - Track 1',
              advanced_apm_flag: 'Y',
              mips_apm_flag: 'N'
            }
          ]
        }
      ];

      QPPApiByTINStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send({ tins: ['123456789'] })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send()
        .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByTINStub.callsFake(() => Promise.reject(new Error('fake error')));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send({ tins: ['123456789'] })
        .expect(500);
    });
  });

  describe('/api/v3/qpp/tin/post.js', () => {
    afterEach(() => {
      QPPApiByTINStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          tin: '123456789',
          entities: [
            {
              entity_id: 'A1001',
              entity_name: 'Palm Beach Accountable Care Organization, LLC',
              lvt_flag: 'F',
              lvt_payments: 163848651.97,
              lvt_patients: 147241,
              lvt_small_status: 'Y',
              lvt_performance_year: 2017,
              entity_tin: '123456789',
              npis: [
                {
                  npi: '1234567890',
                  qp_status: 'Y',
                  qp_payment_threshold_score: 1234567,
                  qp_patient_threshold_score: 1234567,
                  provider_relationship_code: 'P'
                }
              ],
              apm_id: '08',
              apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
              subdivision_id: '01',
              subdivision_name: 'MSSP ACO - Track 1',
              advanced_apm_flag: 'Y',
              mips_apm_flag: 'N'
            }
          ]
        }
      ];

      QPPApiByTINStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/?performance_year=2017')
        .send({ tins: ['123456789'] })
        .expect(200)
        .then((res) => {
          const body = res.body; // eslint-disable-line no-unused-vars
          // expect(body).to.not.be.undefined;
          // expect(body.data).to.not.be.undefined;
          // expect(body.data.apm_data).to.not.be.undefined;
          // expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should respond to request without performance yeart', () => {
      const response = [
        {
          tin: '123456789',
          entities: [
            {
              entity_id: 'A1001',
              entity_name: 'Palm Beach Accountable Care Organization, LLC',
              lvt_flag: 'F',
              lvt_payments: 163848651.97,
              lvt_patients: 147241,
              lvt_small_status: 'Y',
              lvt_performance_year: 2017,
              entity_tin: '123456789',
              npis: [
                {
                  npi: '1234567890',
                  qp_status: 'Y',
                  qp_payment_threshold_score: 1234567,
                  qp_patient_threshold_score: 1234567,
                  provider_relationship_code: 'P'
                }
              ],
              apm_id: '08',
              apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
              subdivision_id: '01',
              subdivision_name: 'MSSP ACO - Track 1',
              advanced_apm_flag: 'Y',
              mips_apm_flag: 'N'
            }
          ]
        }
      ];

      QPPApiByTINStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send({ tins: ['123456789'] })
        .expect(200)
        .then((res) => {
          const body = res.body; // eslint-disable-line no-unused-vars
          // expect(body).to.not.be.undefined;
          // expect(body.data).to.not.be.undefined;
          // expect(body.data.apm_data).to.not.be.undefined;
          // expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send(); // .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByTINStub.callsFake(() => Promise.reject(new Error('fake error')));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send({ tins: ['123456789'] }); // .expect(500);
    });
  });
});
