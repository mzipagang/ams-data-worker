const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');
const expect = chai.expect;

const QPPService = require('../../../app/services/qpp');

describe('Express - QPP Generic', () => {
  let GenericQPPAPIStub;
  let APIv1;
  let APIv2;

  before(() => {
    GenericQPPAPIStub = sinon.stub(QPPService, 'GenericQPPAPI');
    APIv1 = require('../../../app/express-routes/api/v1/qpp/get.js').handler;
    APIv2 = require('../../../app/express-routes/api/v2/qpp/get.js').handler;
  });

  afterEach(() => {
    GenericQPPAPIStub.resetBehavior();
  });

  after(() => {
    GenericQPPAPIStub.restore();
  });

  describe('/api/v1/qpp/get.js', () => {
    afterEach(() => {
      GenericQPPAPIStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          tin: '123456789',
          npi: '1234567890',
          qp_status: 'Y',
          entity_id: 'A1001',
          entity_name: 'Care LLC',
          lvt_flag: 'F',
          lvt_payments: 135886.75,
          lvt_patients: 127013,
          lvt_small_status: 'Y',
          lvt_performance_year: 2017,
          entity_tin: '123456789',
          provider_relationship_code: 'P',
          qp_payment_threshold_score: '24',
          qp_patient_threshold_score: '35',
          apm_id: '01',
          apm_name: 'Accountable Care Organizations',
          subdivision_id: '03',
          subdivision_name: 'Track 3',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        }
      ];

      GenericQPPAPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-tin', '123456789')
        .set('x-npi', '123456789')
        .send()
        .expect(200)
        .then((res) => {
          const newResponse = JSON.parse(JSON.stringify(response));
          newResponse.forEach((data) => {
            delete data.lvt_performance_year;
            delete data.lvt_small_status;
          });

          const body = res.body;
          expect(body).to.not.equal(undefined);
          expect(body.data).to.not.equal(undefined);
          expect(body.data.apm_data).to.deep.equal(newResponse);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(400);
    });

    it('should handle an error', () => {
      GenericQPPAPIStub.callsFake(() => Promise.reject(new Error('Fake error')));

      const app = express();
      app.use('/', APIv1);
      app.use((_req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-entity', 'A1001')
        .send()
        .expect(500);
    });
  });

  describe('/api/v2/qpp/get.js', () => {
    afterEach(() => {
      GenericQPPAPIStub.reset();
    });

    it('should respond to request', () => {
      const response = [
        {
          tin: '123456789',
          npi: '1234567890',
          qp_status: 'Y',
          entity_id: 'A1001',
          entity_name: 'Care LLC',
          lvt_flag: 'F',
          lvt_payments: 135886.75,
          lvt_patients: 127013,
          lvt_small_status: 'Y',
          lvt_performance_year: 2017,
          entity_tin: '123456789',
          provider_relationship_code: 'P',
          qp_payment_threshold_score: '24',
          qp_patient_threshold_score: '35',
          apm_id: '01',
          apm_name: 'Accountable Care Organizations',
          subdivision_id: '03',
          subdivision_name: 'Track 3',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        }
      ];

      GenericQPPAPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv2);
      app.use((_req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .set('x-tin', '123456789')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.equal(undefined);
          expect(body.data).to.not.equal(undefined);
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should respond to request without performance year', () => {
      const response = [
        {
          tin: '123456789',
          npi: '1234567890',
          qp_status: 'Y',
          entity_id: 'A1001',
          entity_name: 'Care LLC',
          lvt_flag: 'F',
          lvt_payments: 135886.75,
          lvt_patients: 127013,
          lvt_small_status: 'Y',
          lvt_performance_year: 2017,
          entity_tin: '123456789',
          provider_relationship_code: 'P',
          qp_payment_threshold_score: '24',
          qp_patient_threshold_score: '35',
          apm_id: '01',
          apm_name: 'Accountable Care Organizations',
          subdivision_id: '03',
          subdivision_name: 'Track 3',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        }
      ];

      GenericQPPAPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv2);
      app.use((_req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-entity', 'A1001')
        .set('x-npi', '123456789')
        .set('x-tin', '123456789')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.equal(undefined);
          expect(body.data).to.not.equal(undefined);
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle an error', () => {
      GenericQPPAPIStub.callsFake(() => Promise.reject(new Error('Fake error')));

      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .set('x-entity', 'A1001')
        .send()
        .expect(500);
    });

    it('should sort by peformance year when multiple values are passed in', () => {
      const response = [
        {
          tin: '123456789',
          npi: '1234567890',
          qp_status: 'Y',
          entity_id: 'A1001',
          entity_name: 'Care LLC',
          lvt_flag: 'F',
          lvt_payments: 135886.75,
          lvt_patients: 127013,
          lvt_small_status: 'Y',
          lvt_performance_year: 2017,
          entity_tin: '123456789',
          provider_relationship_code: 'P',
          qp_payment_threshold_score: '24',
          qp_patient_threshold_score: '35',
          apm_id: '01',
          apm_name: 'Accountable Care Organizations',
          subdivision_id: '03',
          subdivision_name: 'Track 3',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        }
      ];

      GenericQPPAPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv2);
      app.use((_req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performanceYears%5B0%5D=2018&performanceYears%5B1%5D=2017')
        .set('x-tin', '123456789')
        .send()
        .expect(200)
        .then(() => {
          expect(GenericQPPAPIStub).to.have.callCount(2);
          expect(GenericQPPAPIStub.getCall(0).args[3]).to.equal(2017);
          expect(GenericQPPAPIStub.getCall(1).args[3]).to.equal(2018);
        });
    });

    it('should return an error if more results than threshold', () => {
      GenericQPPAPIStub.throws(new Error('Search Limit Exceeded'));

      const app = express();
      app.use('/', APIv2);
      app.use((_req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performanceYears%5B0%5D=2018&performanceYears%5B1%5D=2017')
        .set('x-tin', '123456789')
        .send()
        .expect(403);
    });
  });
});
