const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const QPPService = require('../../../app/services/qpp');

describe('Express - QPP NPI', () => {
  let QPPApiByNPIStub;
  let APIv1;
  let APIv2;

  before(() => {
    QPPApiByNPIStub = sinon.stub(QPPService, 'QPPApiByNPI');
    APIv1 = require('../../../app/express-routes/api/v1/qpp/npi/get.js').handler;
    APIv2 = require('../../../app/express-routes/api/v2/qpp/npi/get.js').handler;
  });

  afterEach(() => {
    QPPApiByNPIStub.resetBehavior();
  });

  after(() => {
    QPPApiByNPIStub.restore();
  });

  describe('/api/v1/qpp/npi/get.js', () => {
    afterEach(() => {
      QPPApiByNPIStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          npi: '123456789',
          qp_status: 'Y',
          tins: [
            {
              tin: '123456789',
              entities: [
                {
                  entity_id: 'A1001',
                  entity_name: 'Palm Beach Accountable Care Organization, LLC',
                  lvt_flag: 'F',
                  lvt_payments: 163848651.97,
                  lvt_patients: 147241,
                  lvt_small_status: 'Y',
                  lvt_performance_year: 2017,
                  entity_tin: '123456789',
                  provider_relationship_code: 'P',
                  qp_payment_threshold_score: 44,
                  qp_patient_threshold_score: 46,
                  apm_id: '08',
                  apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
                  subdivision_id: '01',
                  subdivision_name: 'MSSP ACO - Track 1',
                  advanced_apm_flag: 'Y',
                  mips_apm_flag: 'N'
                }
              ]
            }
          ]
        }
      ];

      QPPApiByNPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-npi', '123456789')
        .send()
        .expect(200)
        .then((res) => {
          const newReponse = JSON.parse(JSON.stringify(response));
          newReponse.forEach(npiData => npiData.tins.forEach(tinData => tinData.entities.forEach((entityData) => {
            delete entityData.lvt_performance_year;
            delete entityData.lvt_small_status;
          })));

          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(newReponse);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByNPIStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .set('x-npi', '123456789')
        .send()
        .expect(500);
    });
  });

  describe('/api/v2/qpp/npi/get.js', () => {
    afterEach(() => {
      QPPApiByNPIStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          npi: '123456789',
          qp_status: 'Y',
          tins: [
            {
              tin: '123456789',
              entities: [
                {
                  entity_id: 'A1001',
                  entity_name: 'Palm Beach Accountable Care Organization, LLC',
                  lvt_flag: 'F',
                  lvt_payments: 163848651.97,
                  lvt_patients: 147241,
                  lvt_small_status: 'Y',
                  lvt_performance_year: 2017,
                  entity_tin: '123456789',
                  provider_relationship_code: 'P',
                  qp_payment_threshold_score: 44,
                  qp_patient_threshold_score: 46,
                  apm_id: '08',
                  apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
                  subdivision_id: '01',
                  subdivision_name: 'MSSP ACO - Track 1',
                  advanced_apm_flag: 'Y',
                  mips_apm_flag: 'N'
                }
              ]
            }
          ]
        }
      ];

      QPPApiByNPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .set('x-npi', '123456789')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByNPIStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-npi', '123456789')
        .send()
        .expect(500);
    });
  });
});
