const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const QPPService = require('../../../app/services/qpp');

describe('Express - QPP Entity', () => {
  let QPPApiByEntityStub;
  let APIv1;
  let APIv2;
  let APIv3;

  before(() => {
    QPPApiByEntityStub = sinon.stub(QPPService, 'QPPApiByEntity');
    APIv1 = require('../../../app/express-routes/api/v1/qpp/entity/get.js').handler;
    APIv2 = require('../../../app/express-routes/api/v2/qpp/entity/get.js').handler;
    APIv3 = require('../../../app/express-routes/api/v3/qpp/entity/get.js').handler;
  });

  afterEach(() => {
    QPPApiByEntityStub.resetBehavior();
  });

  after(() => {
    QPPApiByEntityStub.restore();
  });

  describe('/api/v1/qpp/entity/get.js', () => {
    afterEach(() => {
      QPPApiByEntityStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          entity_id: 'A1001',
          entity_name: 'Palm Beach Accountable Care Organization, LLC',
          lvt_flag: 'F',
          lvt_payments: 163848651.97,
          lvt_patients: 147241,
          lvt_small_status: 'Y',
          lvt_performance_year: 2017,
          entity_tin: '123456789',
          apm_id: '08',
          apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
          subdivision_id: '01',
          subdivision_name: 'MSSP ACO - Track 1',
          advanced_apm_flag: 'N',
          mips_apm_flag: 'Y',
          tins: [
            {
              tin: '123456789',
              npis: [
                {
                  npi: '9876543210',
                  qp_status: 'Y',
                  qp_payment_threshold_score: 1234567,
                  qp_patient_threshold_score: 1234567,
                  provider_relationship_code: 'Y'
                }
              ]
            }
          ]
        }
      ];

      QPPApiByEntityStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-entity', 'A1001')
        .send()
        .expect(200)
        .then((res) => {
          const newReponse = JSON.parse(JSON.stringify(response));
          newReponse.forEach((entityData) => {
            delete entityData.lvt_performance_year;
            delete entityData.lvt_small_status;
          });

          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(newReponse);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByEntityStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv1);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-entity', 'A1001')
        .send()
        .expect(500);
    });
  });

  describe('/api/v2/qpp/entity/get.js', () => {
    afterEach(() => {
      QPPApiByEntityStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          entity_id: 'A1001',
          entity_name: 'Palm Beach Accountable Care Organization, LLC',
          lvt_flag: 'F',
          lvt_payments: 163848651.97,
          lvt_patients: 147241,
          lvt_small_status: 'Y',
          lvt_performance_year: 2017,
          entity_tin: '123456789',
          apm_id: '08',
          apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
          subdivision_id: '01',
          subdivision_name: 'MSSP ACO - Track 1',
          advanced_apm_flag: 'N',
          mips_apm_flag: 'Y',
          tins: [
            {
              tin: '123456789',
              npis: [
                {
                  npi: '9876543210',
                  qp_status: 'Y',
                  qp_payment_threshold_score: 1234567,
                  qp_patient_threshold_score: 1234567,
                  provider_relationship_code: 'Y'
                }
              ]
            }
          ]
        }
      ];

      QPPApiByEntityStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .set('x-entity', 'A1001')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send()
        .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByEntityStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-entity', 'A1001')
        .send()
        .expect(500);
    });
  });

  describe('/api/v3/qpp/entity/get.js', () => {
    afterEach(() => {
      QPPApiByEntityStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          entity_id: 'A1001',
          entity_name: 'Palm Beach Accountable Care Organization, LLC',
          lvt_flag: 'F',
          lvt_payments: 163848651.97,
          lvt_patients: 147241,
          lvt_small_status: 'Y',
          lvt_performance_year: 2017,
          entity_tin: '123456789',
          apm_id: '08',
          apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
          subdivision_id: '01',
          subdivision_name: 'MSSP ACO - Track 1',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'N',
          complex_patient_score: 5,
          tins: [
            {
              tin: '123456789',
              npis: [
                {
                  npi: '1234567890',
                  qp_status: 'Y',
                  qp_payment_threshold_score: 44,
                  qp_patient_threshold_score: 46,
                  provider_relationship_code: 'P',
                  mips_ec_indicator: 'Y',
                  score_type: 'E'
                }
              ]
            }
          ]
        }
      ];

      QPPApiByEntityStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/?performance_year=2017')
        .set('x-entity', 'A1001')
        .send()
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .send(); // .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByEntityStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .get('/')
        .set('x-entity', 'A1001')
        .send(); // .expect(500);
    });
  });
});
