const express = require('express');
const request = require('supertest');
const chai = require('chai');
const sinon = require('sinon');
const bodyParser = require('body-parser');
require('sinon-mongoose');

const expect = chai.expect;

const QPPService = require('../../../app/services/qpp');

describe('Express - QPP NPI', () => {
  let QPPApiByNPIStub;
  let APIv2;
  let APIv3;

  before(() => {
    QPPApiByNPIStub = sinon.stub(QPPService, 'QPPApiByNPI');
    APIv2 = require('../../../app/express-routes/api/v2/qpp/npi/post.js').handler;
    APIv3 = require('../../../app/express-routes/api/v3/qpp/npi/post.js').handler;
  });

  afterEach(() => {
    QPPApiByNPIStub.resetBehavior();
  });

  after(() => {
    QPPApiByNPIStub.restore();
  });

  describe('/api/v2/qpp/npi/post.js', () => {
    afterEach(() => {
      QPPApiByNPIStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          npi: '1234567890',
          qp_status: 'Y',
          tins: [
            {
              tin: '123456789',
              entities: [
                {
                  entity_id: 'A1001',
                  entity_name: 'Palm Beach Accountable Care Organization, LLC',
                  lvt_flag: 'F',
                  lvt_payments: 163848651.97,
                  lvt_patients: 147241,
                  lvt_small_status: 'Y',
                  lvt_performance_year: 2017,
                  entity_tin: '123456789',
                  provider_relationship_code: 'P',
                  qp_payment_threshold_score: 44,
                  qp_patient_threshold_score: 46,
                  apm_id: '08',
                  apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
                  subdivision_id: '01',
                  subdivision_name: 'MSSP ACO - Track 1',
                  advanced_apm_flag: 'Y',
                  mips_apm_flag: 'N'
                }
              ]
            }
          ]
        }
      ];

      QPPApiByNPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/?performance_year=2017')
        .send({ npis: ['1234567890'] })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send()
        .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByNPIStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv2);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send({ npis: ['123456789'] })
        .expect(500);
    });
  });

  describe('/api/v3/qpp/npi/post.js', () => {
    afterEach(() => {
      QPPApiByNPIStub.resetBehavior();
    });

    it('should respond to request', () => {
      const response = [
        {
          npi: '1234567890',
          qp_status: 'Y',
          tins: [
            {
              tin: '123456789',
              entities: [
                {
                  entity_id: 'A1001',
                  entity_name: 'Palm Beach Accountable Care Organization, LLC',
                  lvt_flag: 'F',
                  lvt_payments: 163848651.97,
                  lvt_patients: 147241,
                  lvt_small_status: 'Y',
                  lvt_performance_year: 2017,
                  entity_tin: '123456789',
                  provider_relationship_code: 'P',
                  qp_payment_threshold_score: 44,
                  qp_patient_threshold_score: 46,
                  apm_id: '08',
                  apm_name: 'Medicare Shared Savings Program Accountable Care Organizations',
                  subdivision_id: '01',
                  subdivision_name: 'MSSP ACO - Track 1',
                  advanced_apm_flag: 'Y',
                  mips_apm_flag: 'N',
                  complex_patient_score: 5,
                  score_type: 'E',
                  mips_ec_indicator: 'Y'
                }
              ]
            }
          ]
        }
      ];

      QPPApiByNPIStub.callsFake(() => Promise.resolve(response));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/?performance_year=2017')
        .send({ npis: ['1234567890'] })
        .expect(200)
        .then((res) => {
          const body = res.body;
          expect(body).to.not.be.undefined;
          expect(body.data).to.not.be.undefined;
          expect(body.data.apm_data).to.not.be.undefined;
          expect(body.data.apm_data).to.deep.equal(response);
        });
    });

    it('should handle a bad request', () => {
      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send(); // .expect(400);
    });

    it('should handle an error', () => {
      QPPApiByNPIStub.callsFake(() => Promise.reject(new Error('Fake Error')));

      const app = express();
      app.use(bodyParser.json());
      app.use('/', APIv3);
      app.use((req, res) => {
        res.status(404).end();
      });
      return request(app)
        .post('/')
        .send({ npis: ['123456789'] }); // .expect(500);
    });
  });
});
