const environment = require('./environment');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const amsMongoose = require('../app/services/mongoose');
const sinon = require('sinon');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();

const expect = chai.expect;

const ExpressMock = require('./mocks/services/express-mock');
const LoggerMock = require('./mocks/services/logger-mock');
const ExpressFileRouterMock = require('./mocks/services/expressFileRouter-mock');

const RedisMock = () => ({ getClient: () => {}, connectToRedis: () => Promise.resolve({}) });
const LoggerMongoose = () => ({ connect: () => Promise.resolve(), disconnect: () => Promise.resolve({}) });

describe('App', () => {
  let mocks;
  let requireApp;
  before(() => {
    sinon.stub(process, 'exit').callsFake(() => {});

    mocks = {};
    requireApp = () =>
      proxyquire('../app', {
        './services/redis': mocks.redis,
        './services/express': mocks.express,
        './services/logger': mocks.logger,
        './services/expressFileRouter': mocks.expressFileRouter
      });
  });

  beforeEach(async () => {
    await mongoose.disconnect();
  });

  after(async () => {
    process.exit.restore();
    await mongoose.connect(process.env.MONGODB, { useNewUrlParser: true });
  });

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../app', '../app/config']);
      environment.use('dev');

      mocks.redis = RedisMock();
      mocks.express = ExpressMock();
      mocks.logger = LoggerMock();
      mocks.mongoose = LoggerMongoose();
      mocks.expressFileRouter = ExpressFileRouterMock();
    });

    it('should handle a bad migration', () => {
      process.env.NEW_RELIC_LICENSE_KEY = 'fake key';
      process.env.NODE_ENV = '';

      const loggerError = sinon.spy(mocks.logger, 'error');
      amsMongoose.ams_migrations.push({
        migration: () => 'Test Error'
      });

      const app = requireApp();

      return app.express.then(() => {
        expect(loggerError).to.have.been.called; // eslint-disable-line no-unused-expressions
      });
    });

    it('should require index.js', () => {
      const loggerInfo = sinon.spy(mocks.logger, 'info');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerInfo).to.have.been.called; // eslint-disable-line no-unused-expressions
      });
    });

    it('should handle an error when starting express', () => {
      sinon.stub(mocks.express, 'start').callsFake(() =>
        Promise.try(() => {
          throw new Error('Failed to start server');
        })
      );

      const loggerError = sinon.spy(mocks.logger, 'error');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerError).to.have.been.called; // eslint-disable-line no-unused-expressions
      });
    });

    it('should handle an error when starting mongoose', () => {
      sinon.stub(mocks.mongoose, 'connect').callsFake(() =>
        Promise.try(() => {
          throw new Error('Failed to start mongoose');
        })
      );

      const loggerError = sinon.spy(mocks.logger, 'error');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerError).to.have.been.called; // eslint-disable-line no-unused-expressions
      });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../app', '../app/config']);
      environment.use('production');

      mocks.express = ExpressMock();
      mocks.logger = LoggerMock();
      mocks.expressFileRouter = ExpressFileRouterMock();
    });

    it('should require index.js', () => {
      const loggerInfo = sinon.spy(mocks.logger, 'info');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerInfo).to.have.been.called; // eslint-disable-line no-unused-expressions
      });
    });

    it('should handle an error when starting express', () => {
      sinon.stub(mocks.express, 'start').callsFake(() =>
        Promise.try(() => {
          throw new Error('Failed to start server');
        })
      );

      const loggerError = sinon.spy(mocks.logger, 'error');
      const app = requireApp();

      return app.express.then(() => {
        expect(loggerError).to.have.been.called; // eslint-disable-line no-unused-expressions
      });
    });
  });
});
