const moment = require('moment');
const Beneficiary = require('../../../app/services/mongoose/models/beneficiary');

module.exports = data => new Beneficiary({
  program_id: '12345',
  entity_id: 'L33T',
  link_key: '123456',
  program_org_id: 'abc123',
  id: '',
  hicn: '123456789',
  first_name: '',
  middle_name: '',
  last_name: '',
  dob: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  gender: '1',
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  category_code: '',
  org_payment_track: '',
  org_payment_track_start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  org_payment_track_end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  voluntary_alignment: '',
  other_id: '',
  other_id_type: '',
  mbi: '123456789',
  ...data
});
