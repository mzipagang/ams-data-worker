const QuarterlyReport = require('../../../app/services/mongoose/models/quarterly_report');

module.exports = data => new QuarterlyReport(data || {
  model_name: 'model_name2',
  model_id: '66',
  group: 'group',
  team_lead: 'test',
  statutory_authority: 'test',
  announcement_date: '2018-02-22T04:01:45.836Z',
  award_date: '2018-02-22T04:01:45.836Z',
  performance_start_date: '2018-02-22T04:01:45.836Z',
  performance_end_date: '2018-02-22T04:01:45.836Z',
  num_ffs_beneficiaries: 5,
  num_advantage_beneficiaries: 5,
  num_medicaid_beneficiaries: 5,
  num_dually_eligible_beneficiaries: 5,
  num_private_insurance_beneficiaries: 5,
  num_other_beneficiaries: 5,
  total_beneficiaries: 5,
  notes_for_beneficiary_counts: 'notes',
  num_physicians: 5,
  num_other_individual_human_providers: 5,
  num_hospitals: 5,
  num_other_providers: 5,
  num_chip_beneficiaries: 5,
  additional_information: 'test',
  total_providers: 5,
  notes_provider_count: 'notes',
  year: 2018,
  quarter: '1',
  createdAt: '2018-07-12T11:30:53.359Z',
  updated: '2018-07-12T11:30:53.359Z',
  waivers: [
    {
      value: 'payment',
      text: 'freeform text'
    }
  ]
});
