const IndvidualQPStatus = require('../../../app/services/mongoose/models/individual_qp_status');

module.exports = data => new IndvidualQPStatus(data || {
  npi: '1231231230',
  qp_status: 'P',
  performance_year: '2017',
  run_snapshot: '123'
}
);
