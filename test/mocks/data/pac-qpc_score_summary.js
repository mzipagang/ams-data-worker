const QpcScoreSummary = require('../../../app/services/mongoose/models/pac_qp_category_score_summary');

module.exports = data => new QpcScoreSummary(Object.assign({}, {
  entity_id: '0000-0000-0000-0000',
  final_qpc_score: 123.1,
  current_achievement_points: 123.1,
  prior_achievement_points: 123.1,
  high_priority_bonus_points: 5,
  cehrt_reporting_bonus: 5,
  quality_improvement_points: 5,
  scored_measures: 5,
  excluded_measures: 5,
  excluded_measures_pay_for_reporting: 5,
  excluded_measures_case_size: 5,
  file_id: '123',
}, data));
