const EntityQP = require('../../../app/services/mongoose/models/individual_qp_threshold');

module.exports = data => new EntityQP(data || {
  apm_id: 'string',
  subdivision_id: 'string',
  entity_id: 'string',
  tin: 'string',
  npi: 'string',
  qp_status: 'string',
  expenditures_numerator: 0,
  expenditures_denominator: 0,
  payment_threshold_score: 0,
  payment_threshold_met: 'string',
  beneficiaries_numerator: 0,
  beneficiaries_denominator: 0,
  patient_threshold_score: 0,
  patient_threshold_met: 'string',
  performance_year: 2017,
  run_snapshot: 'I'

}
);
