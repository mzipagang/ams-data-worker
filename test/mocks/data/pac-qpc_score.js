const QpcScore = require('../../../app/services/mongoose/models/pac_qp_category_score');

module.exports = data => new QpcScore(Object.assign({}, {
  entity_id: '0000-0000-0000-0000',
  measure_id: 'ABCD0017',
  measure_score: 0.798,
  decile_score: 8.8,
  meets_scoring_criteria: 'Y',
  high_priority_measure: 'Y',
  cehrt_measure: 'Y'
}, data));
