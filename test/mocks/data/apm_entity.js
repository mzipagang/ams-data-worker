const moment = require('moment');
const Entity = require('../../../app/services/mongoose/models/apm_entity');

module.exports = data => new Entity(Object.assign({}, {
  apm_id: '0000-0000-0000-0000',
  subdiv_id: '0000-0000-0000-0000',
  id: '0000-0000-0000-0000',
  start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  name: 'Semantic Health',
  dba: 'dba',
  type: '',
  tin: '219718706',
  npi: '1234567890',
  ccn: '',
  additional_information: '',
  address: {
    zip4: '',
    zip5: '20171',
    state: 'VA',
    city: 'Herndon',
    address_2: 'Suite 420',
    address_1: '13921 Park Center Road'
  }
}, data));
