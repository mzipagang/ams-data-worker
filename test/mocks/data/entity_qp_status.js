const EntityQP = require('../../../app/services/mongoose/models/entity_qp_status');

module.exports = data => new EntityQP(data || {
  npi: '1234567890',
  qp_status: 'Y',
  performance_year: 2017,
  run_snapshot: 'I'
}
);
