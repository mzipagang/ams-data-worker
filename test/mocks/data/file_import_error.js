const FileImportError = require('../../../app/services/mongoose/models/file_import_error');

module.exports = obj => new FileImportError({
  file_import_id: (obj ? obj.file_import_id : null) || '1234',
  error: 'Failed for a fake reason',
  line_number: 2
});
