const moment = require('moment');
const Provider = require('../../../app/services/mongoose/models/source_file_provider_data');

module.exports = data => new Provider(data || {
  apm_id: '21',
  entity_id: '0000-0000-0000-0000',
  start_date: moment.utc()
    .add(-2, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  tin: '000002048',
  npi: '1234567890',
  ccn: '123456',
  tin_type_code: 'SSN',
  specialty_code: '',
  organization_name: '',
  first_name: '',
  middle_name: '',
  last_name: '',
  participant_type_code: ''
});
