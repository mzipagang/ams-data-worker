const moment = require('moment');
const Model = require('../../../app/services/mongoose/models/pac_model');

module.exports = data => new Model(data || {
  id: '00',
  name: 'Semantic Health',
  short_name: 'SH',
  start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  advanced_apm_flag: 'Y',
  mips_special_scoring_flag: 'Y',
  apm_quality_reporting_category: 1
});
