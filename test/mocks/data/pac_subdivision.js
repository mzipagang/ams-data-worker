const moment = require('moment');
const Subdivision = require('../../../app/services/mongoose/models/pac_subdivision');

module.exports = data => new Subdivision(data || {
  id: '0000-0000-0000-0000',
  apm_id: '0000-0000-0000-0000',
  name: 'Semantic Health',
  start_date: moment.utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  end_date: moment.utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  advanced_apm_flag: 'Y',
  mips_special_scoring_flag: 'Y'
});
