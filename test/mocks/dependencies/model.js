const Promise = require('bluebird');

const ModelMock = (data) => {
  const result = Promise.try(() => (data));

  const query = function query() {
    return Object.assign(result, {
      create: query,
      find: query,
      findOne: query,
      findOneAndUpdate: query,
      findOneAndRemove: query,
      limit: query,
      skip: query,
      sort: query,
      updateOne: query,
      upsert: query,
      exec: query,
      execute: query,
      aggregate: query
    });
  };

  return Object.assign(result, {
    create: query,
    find: query,
    findOne: query,
    findOneAndUpdate: query,
    findOneAndRemove: query,
    limit: query,
    skip: query,
    sort: query,
    updateOne: query,
    upsert: query,
    exec: query,
    execute: query,
    aggregate: query
  });
};

module.exports = ModelMock;
