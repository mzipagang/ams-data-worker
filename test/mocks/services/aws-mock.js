const stream = require('stream');

let inputParams = {};
const events2 = {};

const getObjectMock = (params) => {
  inputParams = params;
  return {
    on: (event, handler) => {
      const self = {
        response: {
          httpResponse: {
            createUnbufferedStream: () => ({
              pipe: (res) => {
                res.json({ message: 'Success' });
              }
            })
          }
        }
      };

      return {
        send: () => {
          handler.call(self, 200, { 'content-length': 100, 'content-type': 'application/json' });
        }
      };
    }
  };
};

const uploadMock = (params) => {
  inputParams = params;

  const echoStream = new stream.Writable();

  echoStream._write = (chunk, encoding, done) => {
    done();
  };

  echoStream.on = (event, handler) => {
    events2[event] = handler;
  };

  setTimeout(() => {
    events2.uploaded();
  }, 1);

  return echoStream;
};

module.exports.S3UploadMock = uploadMock;

module.exports = () => ({
  S3: {
    getParams: () => inputParams,
    getObject: getObjectMock,
    upload: uploadMock
  },

  util: {
    adam: 'test',
    getSignedUrl: () => { throw new Error('TODO: Implment this.'); },
    getObject: (key, bucket = 'MOCK_BUCKET') => getObjectMock({ Bucket: bucket, Key: key }),
    uploadS3: (key, readStream, fileType, metadata, bucket = 'MOCK_BUCKET') =>
      new Promise((resolve, reject) => {
        try {
          const upload = uploadMock({
            Bucket: bucket,
            Key: key,
            ContentType: fileType,
            Metadata: metadata
          });

          upload.on('error', reject);
          upload.on('uploaded', resolve);

          readStream.pipe(upload);
        } catch (err) {
          reject(err);
        }
      })
  }
});

module.exports.AwsErrMock = () => ({
  S3: {
    getObject: (_params) => {
      throw new Error(false);
    }
  }
});

module.exports.AwsErrMock2 = () => ({
  S3: {
    getObject: _params => ({
      on: (_event, _handler) => {
        throw new Error(false);
      }
    })
  }
});

const S3ErrorMock = () => {
  return Promise.reject();
};

module.exports.S3ErrorMock = () => ({
  S3: {
    getParams: () => inputParams,
    getObject: getObjectMock,
    upload: S3ErrorMock
  },

  util: {
    getSignedUrl: () => { throw new Error('TODO: Implment this.'); },
    getObject: (key, bucket = 'MOCK_BUCKET') => getObjectMock({ Bucket: bucket, Key: key }),
    uploadS3: (key, readStream, fileType, metadata, bucket = 'MOCK_BUCKET') =>
      new Promise((resolve, reject) => {
        try {
          const upload = S3ErrorMock({
            Bucket: bucket,
            Key: key,
            ContentType: fileType,
            Metadata: metadata
          });

          upload.on('error', reject);
          upload.on('uploaded', resolve);

          readStream.pipe(upload);
        } catch (err) {
          reject(err);
        }
      })
  }
});
