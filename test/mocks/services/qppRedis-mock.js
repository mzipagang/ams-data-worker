const Promise = require('bluebird');

module.exports = () => Promise.resolve();
module.exports.getByEntityId = () => Promise.resolve();
module.exports.setByEntityId = () => Promise.resolve();
module.exports.getByNpi = () => Promise.resolve();
module.exports.setByNpi = () => Promise.resolve();
module.exports.getByTin = () => Promise.resolve();
module.exports.setByTin = () => Promise.resolve();
module.exports.getEntityList = () => Promise.resolve();
module.exports.setEntityList = () => Promise.resolve();
