const express = require('express');

module.exports = () => ({
  load: () => express.Router()
});
