const environment = require('./environment');
const chai = require('chai');

const expect = chai.expect;

describe('Config', () => {
  const requireConfig = () => require('../app/config');

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../app/config']);
      environment.use('dev');
    });

    it('should have created a config object', () => {
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty AWS_SECRET_ACCESS_KEY', () => {
      delete process.env.AWS_SECRET_ACCESS_KEY;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty AWS_ACCESS_KEY_ID', () => {
      delete process.env.AWS_ACCESS_KEY_ID;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty NODE_ENV', () => {
      delete process.env.NODE_ENV;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty PORT', () => {
      delete process.env.PORT;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty MONGODB', () => {
      delete process.env.MONGODB;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty SQS_QUEUE_URL', () => {
      delete process.env.SQS_QUEUE_URL;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../app/config']);
      environment.use('production');
    });

    it('should have created a config object', () => {
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty AWS_SECRET_ACCESS_KEY', () => {
      delete process.env.AWS_SECRET_ACCESS_KEY;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty AWS_ACCESS_KEY_ID', () => {
      delete process.env.AWS_ACCESS_KEY_ID;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty NODE_ENV', () => {
      delete process.env.NODE_ENV;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty PORT', () => {
      delete process.env.PORT;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty MONGODB', () => {
      delete process.env.MONGODB;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });

    it('should have created a config object with empty SQS_QUEUE_URL', () => {
      delete process.env.SQS_QUEUE_URL;
      const config = requireConfig();
      expect(config).to.not.be.undefined;
    });
  });
});
