const mongoose = require('mongoose');
const dookie = require('dookie');
const chai = require('chai');
const chaiArrays = require('chai-arrays');
const sinonChai = require('sinon-chai');

const dbGuid = require('./environment/test_db_guid.js');

chai.use(chaiArrays);
chai.use(sinonChai);

before(async () => {
  mongoose.Promise = global.Promise;
  console.log('**** UNIT TESTS, OVERRIDING ENV VARS ****'); // eslint-disable-line no-console
  process.env.NODE_ENV = 'development';
  process.env.MONGODB = process.env.TEST_MONGODB || `mongodb://localhost:20017/ams-api-test-${dbGuid}`;

  const data = require('./sample_data/export.json');
  await dookie.push(process.env.MONGODB, data);

  await mongoose.connect(
    process.env.MONGODB,
    { useNewUrlParser: true }
  );
  mongoose.set('useCreateIndex', true);
  mongoose.set('useFindAndModify', false);
  console.log('Successfully connected to Test MongoDB'); // eslint-disable-line no-console
});

after(async () => {
  await mongoose.connection.db.dropDatabase();
  await mongoose.disconnect();
  console.log('Successfully disconnected from Test MongoDB'); // eslint-disable-line no-console
});
