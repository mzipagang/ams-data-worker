const { expect } = require('chai');
const sinon = require('sinon');
const faker = require('faker');
const constants = require('../../app/constants');
const BatchJobFactory = require('../../batch-processing/BatchJobFactory');
const JobQueueService = require('../../app/services/JobQueueService');
const TestJob = require('../../batch-processing/jobs/TestJob');
const { JOBS } = require('../../batch-processing/batchConfig');

describe('BatchJobFactory', () => {
  const JOB_ID = faker.random.number({ min: 1, max: 999 });
  const FAKE_JOB_TYPE = 'fake-job-type';
  const FAKE_JOB_IMPL = { fake: 'impl' };

  let queuedJob;
  let getJobStub;

  beforeEach(() => {
    JOBS[FAKE_JOB_TYPE] = FAKE_JOB_IMPL;
    queuedJob = { type: constants.BATCH.JOB_TYPES.TEST_JOB, the: 'job', id: '7' };

    getJobStub = sinon.stub(JobQueueService, 'getJobById');
    getJobStub.resolves(queuedJob);
  });

  afterEach(() => {
    delete JOBS[FAKE_JOB_TYPE];
    getJobStub.restore();
  });

  describe('#createJobInstance', () => {
    it('should validate the required job id parameter', async () => {
      try {
        await BatchJobFactory.createJobInstance();
        expect.fail(0, 1, 'Exception not thrown');
      } catch (err) {
        expect(err.message).to.equal('Job Id is required');
      }
    });

    it('should retrieve the QueuedJob from MongoDB', async () => {
      await BatchJobFactory.createJobInstance(JOB_ID);
      expect(JobQueueService.getJobById).to.have.been.calledWith(JOB_ID);
    });

    it('should throw an error if the job cannot be found in Mongo', async () => {
      getJobStub.resolves(null);
      try {
        await BatchJobFactory.createJobInstance(JOB_ID);
        expect.fail(1, 0, 'Exception not thrown');
      } catch (err) {
        expect(err.message).to.equal(`Cannot find job with ID "${JOB_ID}"`);
      }
    });

    it('should throw an error if the job type cannot be determined', async () => {
      queuedJob.type = 'unsupported type';
      try {
        await BatchJobFactory.createJobInstance(JOB_ID);
        expect.fail(1, 0, 'Exception not thrown');
      } catch (err) {
        expect(err.message).to.equal('Cannot find job definition for job type "unsupported type"');
      }
    });

    it('should return an instance of the job type in question', async () => {
      const result = await BatchJobFactory.createJobInstance(JOB_ID);
      expect(result).to.be.an.instanceOf(TestJob);
      expect(result.jobDetails).to.equal(queuedJob);
    });
  });

  describe('#getJobImplementationByType', () => {
    it('should throw an error if the type cannot be found', () => {
      try {
        BatchJobFactory.getJobImplementationByType('unsupportedType');
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err.message).to.equal('Cannot find job definition for job type "unsupportedType"');
      }
    });

    it('should return the BatchJob implementation for the specified type if found', () => {
      const result = BatchJobFactory.getJobImplementationByType(FAKE_JOB_TYPE);
      expect(result).to.equal(FAKE_JOB_IMPL);
    });
  });
});
