const { expect } = require('chai');
const BatchJob = require('../../batch-processing/BatchJob');

describe('BatchJob', () => {
  describe('constructor', () => {
    it('should set the job details in the constructor', () => {
      const jobInstance = new BatchJob('whatever');
      expect(jobInstance.jobDetails).to.equal('whatever');
    });
  });

  describe('#executeJob', () => {
    it('should always throw an error, ensuring base classes override it correctly', async () => {
      const jobInstance = new BatchJob('whatever');
      try {
        await jobInstance.executeJob();
        expect.fail(0, 1, 'Exception not thrown');
      } catch (err) {
        expect(err.message).to.equal('BatchJob subclasses must implement an executeJob method');
      }
    });
  });

  describe('#getJobTimeoutDuration', () => {
    it('should always throw an error, ensuring base classes override it correctly', () => {
      const jobInstance = new BatchJob('whatever');
      try {
        jobInstance.getJobTimeoutDuration();
        expect.fail(0, 1, 'Exception not thrown');
      } catch (err) {
        expect(err.message).to.equal('BatchJob subclasses must implement a getJobTimeoutDuration method');
      }
    });
  });
});
