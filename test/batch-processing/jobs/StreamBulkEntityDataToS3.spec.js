const uuid = require('uuid');
const { expect } = require('chai');
const BatchJob = require('../../../batch-processing/jobs/StreamBulkEntityDataToS3');
const QueuedJob = require('../../../app/services/mongoose/models/queued_jobs');
const QppEntity = require('../../../app/services/mongoose/models/qpp_entity');
const request = require('request');

const downloadJson = url => new Promise((resolve, reject) => {
  request.get(url, (error, response, body) => {
    if (error) {
      reject(error);
    } else {
      resolve(body);
    }
  });
});

describe('StreamBulkEntityDataToS3', () => {
  describe('#executeJob', () => {
    it('should stream to s3 correctly', async () => {
      const jobDetails = {
        id: uuid.v4(),
        type: 'stream-bulk-entity-data-to-s3',
        parameters: {
          performanceYear: 2017
        }
      };

      (new QueuedJob(Object.assign({}, jobDetails, { status: 'running' }))).save();

      const jobInstance = new BatchJob(jobDetails);

      try {
        const { results } = await jobInstance.executeJob();

        const s3File = await downloadJson(results.signedUrlConfirmationId);
        const s3EntityIds = JSON.parse(s3File).data.apm_data.map(entity => entity.entity_id);

        const timeout = jobInstance.getJobTimeoutDuration();
        expect(timeout).to.be.greaterThan(0);

        expect(jobInstance.jobDetails).to.equal(jobDetails);
        expect(s3EntityIds).to.contain('A1001');
      } catch (err) {
        if (err.indexOf('ECONNREFUSED') === -1) {
          throw err;
        }
      }
    });
  });
});
