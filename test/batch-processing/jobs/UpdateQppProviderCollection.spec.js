const uuid = require('uuid');
const { expect } = require('chai');
const BatchJob = require('../../../batch-processing/jobs/UpdateQppProviderCollection');
const QueuedJob = require('../../../app/services/mongoose/models/queued_jobs');
const mongoose = require('../../../app/services/mongoose');


describe('UpdateQppProviderCollection', () => {
  before(async () => {
    await mongoose.connection.db.renameCollection('qpp_providers', 'qpp_providers_bk', { dropTarget: true });
  });

  describe('#executeJob', () => {
    it('should update qpp_providers collection', async () => {
      const jobDetails = {
        id: uuid.v4(),
        type: 'UpdateQppProviderCollection',
        parameters: {
          performanceYear: 2017
        }
      };

      (new QueuedJob(Object.assign({}, jobDetails, { status: 'running' }))).save();

      const jobInstance = new BatchJob(jobDetails);
      const timeout = jobInstance.getJobTimeoutDuration();
      await jobInstance.executeJob();
      expect(timeout).to.be.greaterThan(0);
      expect(jobInstance.jobDetails).to.equal(jobDetails);
    });
  });

  after(async () => {
    await mongoose.connection.db.renameCollection('qpp_providers_bk', 'qpp_providers', { dropTarget: true });
  });
});
