const uuid = require('uuid');
const { expect } = require('chai');
const BatchJob = require('../../../batch-processing/jobs/CreateQppEntitiesCollection');
const QueuedJob = require('../../../app/services/mongoose/models/queued_jobs');
const createQppEntityCollection = require('../../../app/services/qpp/createQppEntityCollection');
const sinon = require('sinon');

describe('CreateQppEntitiesCollection', () => {
  let CreateQppEntityCollectionMock;

  describe('#executeJob', () => {
    beforeEach(() => {
      CreateQppEntityCollectionMock = sinon.mock(createQppEntityCollection);
    });
    afterEach(() => {
      CreateQppEntityCollectionMock.restore();
    });
    it('should create the collection correctly', async () => {
      const jobDetails = {
        id: uuid.v4(),
        type: 'create-qpp-entities-collection'
      };

      (new QueuedJob(Object.assign({}, jobDetails, { status: 'running' }))).save();

      const jobInstance = new BatchJob(jobDetails);
      await jobInstance.executeJob();

      expect(jobInstance.jobDetails).to.equal(jobDetails);
    });

    it('should handle an error', async () => {
      CreateQppEntityCollectionMock = sinon.mock(createQppEntityCollection);

      CreateQppEntityCollectionMock.expects('create').rejects(new Error('Fake Error'));
      const jobDetails = {
        id: uuid.v4(),
        type: 'create-qpp-entities-collection'
      };

      (new QueuedJob(Object.assign({}, jobDetails, { status: 'running' }))).save();

      const jobInstance = new BatchJob(jobDetails);
      await jobInstance.executeJob().catch(res => expect(res.message).to.equal('Fake Error'));
    });
  });

  describe('#getJobTimeoutDuration', () => {
    it('should return timeout duration', () => {
      const jobInstance = new BatchJob({});
      expect(jobInstance.getJobTimeoutDuration()).to.equal(300000);
    });
  });
});
