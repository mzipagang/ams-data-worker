setTimeout(() => {
  // This console.log should never be sent anywhere. We're redirecting all child logs to /dev/null
  // Normally they'd be under ${HOME}/.pm2/logs
  console.log('ready log'); // eslint-disable-line no-console
  process.send('ready');
}, 10);
setInterval(() => {
  process.send('ping');
}, 3000);
