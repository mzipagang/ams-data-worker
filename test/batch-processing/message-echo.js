process.on('message', (packet) => {
  // packet has the following properties:
  //   id    - the pm_id
  //   topic - the topic provided
  //   type  - the type provided
  //   data  - the data provided;
  process.send({
    type: 'CHILD_TO_PARENT',
    data: {
      echo_success: true,
      received: packet
    }
  });
});
process.send('ready');
