const pm2 = require('pm2');
const sinon = require('sinon');
const faker = require('faker');
const _ = require('lodash');
const mongoose = require('mongoose');
const { expect } = require('chai');
const ProcessManager = require('../../batch-processing/ProcessManager');
const logger = require('../../app/services/logger');
const JobQueueService = require('../../app/services/JobQueueService');
const batchConfig = require('../../batch-processing/batchConfig');
const BatchJobFactory = require('../../batch-processing/BatchJobFactory');
const {
  JOB_TOPIC,
  WORKER_WAITING_MESSAGE_TYPE,
  WORKER_ERROR_MESSAGE_TYPE,
  WORKER_COMPLETE_MESSAGE_TYPE,
  JOB_START_MESSAGE_TYPE
} = require('../../app/constants').BATCH;

describe('ProcessManager', () => {
  const ERROR = new Error('foo');
  const PROCESS_ID = faker.random.number({ min: 1, max: 999 });
  const JOB_ID = faker.random.number({ min: 1, max: 999 });

  let manager;
  let connectStub;
  let disconnectStub;
  let listStub;
  let startStub;
  let sendStub;
  let launchBusStub;
  let deleteStub;
  let loggerErrorStub;
  let loggerInfoStub;
  let loggerWarnStub;
  let waitingMessage;
  let jobDetails;

  beforeEach(() => {
    process.env.NODE_ENV = 'dev';

    waitingMessage = {
      data: {},
      process: {
        pm_id: PROCESS_ID
      }
    };

    jobDetails = {
      id: JOB_ID,
      job: 'details',
      type: 'fake-type',
      resultsError: {
        results: 'error'
      }
    };

    manager = new ProcessManager();
    connectStub = sinon.stub(pm2, 'connect');
    disconnectStub = sinon.stub(pm2, 'disconnect');
    listStub = sinon.stub(pm2, 'list');
    startStub = sinon.stub(pm2, 'start');
    sendStub = sinon.stub(pm2, 'sendDataToProcessId');
    launchBusStub = sinon.stub(pm2, 'launchBus');
    deleteStub = sinon.stub(pm2, 'delete');
    loggerErrorStub = sinon.stub(logger, 'error');
    loggerInfoStub = sinon.stub(logger, 'info');
    loggerWarnStub = sinon.stub(logger, 'warn');
  });

  afterEach(() => {
    connectStub.restore();
    disconnectStub.restore();
    listStub.restore();
    startStub.restore();
    sendStub.restore();
    launchBusStub.restore();
    deleteStub.restore();
    loggerErrorStub.restore();
    loggerInfoStub.restore();
    loggerWarnStub.restore();
  });

  describe('constructor', () => {
    it('should initialize waitingWorkers, workerJobMapping, and workerTimeoutMapping', () => {
      manager = new ProcessManager();
      expect(manager.waitingWorkers).to.eql(new Set([]));
      expect(manager.workerJobMapping).to.be.eql({});
      expect(manager.workerTimeoutMapping).to.be.eql({});
      expect(manager.workerCount).to.equal(0);
    });
  });

  describe('#connect', () => {
    it('should resolve true when connecting successfully', async () => {
      const promise = manager.connect();
      const callback = connectStub.getCall(0).args[0];
      callback();
      expect(loggerErrorStub).to.not.have.been.called;
      const result = await promise;
      expect(result).to.be.true;
    });

    it('should handle errors', async () => {
      const promise = manager.connect();
      const callback = connectStub.getCall(0).args[0];
      callback(ERROR);
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
      try {
        await promise;
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err).to.equal(ERROR);
      }
    });
  });

  describe('#disconnect', () => {
    it('should call PM2s disconnect', () => {
      manager.disconnect();
      expect(disconnectStub).to.have.been.called;
    });
  });

  describe('#list', () => {
    it('should resolve the list of processes when successful', async () => {
      const promise = manager.list();
      const callback = listStub.getCall(0).args[0];
      callback(null, 'foo');
      expect(loggerErrorStub).to.not.have.been.called;
      const result = await promise;
      expect(result).to.equal('foo');
    });

    it('should handle errors', async () => {
      const promise = manager.list();
      const callback = listStub.getCall(0).args[0];
      callback(ERROR, 'foo');
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
      try {
        await promise;
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err).to.equal(ERROR);
      }
    });
  });

  describe('#startScript', () => {
    it('should resolve with an newly created processes on success', async () => {
      const promise = manager.startScript('script');
      const options = startStub.getCall(0).args[0];
      expect(options).to.eql({
        script: 'script',
        waitReady: true
      });
      const callback = startStub.getCall(0).args[1];
      callback(null, ['process1']);
      expect(loggerErrorStub).to.not.have.been.called;
      const result = await promise;
      expect(result).to.eql(['process1']);
    });

    it('should reject with an error if the script cannot be found', async () => {
      const promise = manager.startScript('script');
      const options = startStub.getCall(0).args[0];
      expect(options).to.eql({
        script: 'script',
        waitReady: true
      });
      const callback = startStub.getCall(0).args[1];
      callback(null, []);
      expect(loggerErrorStub).to.have.been.calledWith('Script "script" not found');
      try {
        await promise;
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err.message).to.equal('Script "script" not found');
      }
    });

    it('should handle errors', async () => {
      const promise = manager.startScript('script');
      const options = startStub.getCall(0).args[0];
      expect(options).to.eql({
        script: 'script',
        waitReady: true
      });
      const callback = startStub.getCall(0).args[1];
      callback(ERROR, []);
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
      try {
        await promise;
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err).to.equal(ERROR);
      }
    });
  });

  describe('#sendDataToProcessId', () => {
    it('should resolve the new process info on success', async () => {
      const promise = manager.sendDataToProcessId();
      const callback = sendStub.getCall(0).args[2];
      callback(null, 'foo');
      expect(loggerErrorStub).to.not.have.been.called;
      const result = await promise;
      expect(result).to.equal('foo');
    });

    it('should handle errors', async () => {
      const promise = manager.sendDataToProcessId();
      const callback = sendStub.getCall(0).args[2];
      callback(ERROR, 'foo');
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
      try {
        await promise;
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err).to.equal(ERROR);
      }
    });
  });

  describe('#launchBus', () => {
    it('should resolve the newly created message bus', async () => {
      const promise = manager.launchBus();
      const callback = launchBusStub.getCall(0).args[0];
      callback(null, 'foo');
      expect(loggerErrorStub).to.not.have.been.called;
      const result = await promise;
      expect(result).to.equal('foo');
    });

    it('should handle errors', async () => {
      const promise = manager.launchBus();
      const callback = launchBusStub.getCall(0).args[0];
      callback(ERROR, 'foo');
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
      try {
        await promise;
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err).to.equal(ERROR);
      }
    });
  });

  describe('#startProcessManager', () => {
    let startWorkerStub;
    let managerConnectStub;
    let managerLaunchBusStub;
    let handleCompleteMessageStub;
    let handleWaitingMessageStub;
    let handleErrorMessageStub;
    let managerListStub;
    let messageBus;
    let mongooseConnectStub;

    beforeEach(() => {
      messageBus = {
        on: sinon.stub()
      };

      startWorkerStub = sinon.stub(manager, 'startBatchWorker');
      startWorkerStub.resolves(true);
      managerConnectStub = sinon.stub(manager, 'connect');
      managerConnectStub.resolves(true);
      managerLaunchBusStub = sinon.stub(manager, 'launchBus');
      managerLaunchBusStub.resolves(messageBus);
      handleCompleteMessageStub = sinon.stub(manager, 'handleJobCompleteMessage');
      handleWaitingMessageStub = sinon.stub(manager, 'handleWaitingMessage');
      handleErrorMessageStub = sinon.stub(manager, 'handleErrorMessage');
      managerListStub = sinon.stub(manager, 'list');
      managerListStub.resolves(['process1', 'process2']);
      mongooseConnectStub = sinon.stub(mongoose, 'connect');
      mongooseConnectStub.resolves(true);
    });

    afterEach(() => {
      startWorkerStub.restore();
      managerConnectStub.restore();
      managerLaunchBusStub.restore();
      handleCompleteMessageStub.restore();
      handleWaitingMessageStub.restore();
      handleErrorMessageStub.restore();
      mongooseConnectStub.restore();
    });

    it('should launch the message bus and listen for messages', async () => {
      const result = await manager.startProcessManager();

      expect(loggerInfoStub).to.have.been.calledWith('Starting ProcessManager');

      expect(mongooseConnectStub).to.have.been.calledWith(batchConfig.MONGOOSE.URI, { useNewUrlParser: true });

      expect(managerConnectStub).to.have.been.called;
      expect(loggerInfoStub).to.have.been.calledWith('Successfully connected to PM2');

      expect(manager.messageBus).to.equal(messageBus);
      expect(managerLaunchBusStub).to.have.been.calledOnce;
      expect(messageBus.on).to.have.callCount(3);

      const completeCall = messageBus.on.getCalls()[0];
      expect(completeCall).to.have.been.calledWith(WORKER_COMPLETE_MESSAGE_TYPE, sinon.match.func);
      const completeCallback = completeCall.args[1];
      completeCallback('foo');
      expect(handleCompleteMessageStub).to.have.been.calledWith('foo');

      const waitingCall = messageBus.on.getCalls()[1];
      expect(waitingCall).to.have.been.calledWith(WORKER_WAITING_MESSAGE_TYPE, sinon.match.func);
      const waitingCallback = waitingCall.args[1];
      waitingCallback('foo');
      expect(handleWaitingMessageStub).to.have.been.calledWith('foo');

      const errorCall = messageBus.on.getCalls()[2];
      expect(errorCall).to.have.been.calledWith(WORKER_ERROR_MESSAGE_TYPE, sinon.match.func);
      const errorCallback = errorCall.args[1];
      errorCallback('bar');
      expect(handleErrorMessageStub).to.have.been.calledWith('bar');

      expect(managerLaunchBusStub).to.have.been.calledOnce;

      expect(loggerInfoStub).to.have.been.calledWith('Message Bus initialized successfully');
      expect(loggerInfoStub).to.have.been.calledWith('All workers started successfully');

      expect(result).to.eql(['process1', 'process2']);
    });

    it('should start as many workers as configured', async () => {
      const results = await manager.startProcessManager();
      expect(startWorkerStub).to.have.callCount(batchConfig.BATCH_WORKER_COUNT);
      expect(results).to.be.ofSize(2);
      expect(loggerInfoStub).to.have.been.calledWith('Starting 2 BatchWorkers');
    });
  });

  describe('#startBatchWorker', () => {
    let startScriptStub;

    beforeEach(() => {
      manager.workerCount = 7;
      startScriptStub = sinon.stub(manager, 'startScript');
    });

    afterEach(() => {
      startScriptStub.restore();
    });

    it('should call "startScript" specifying the batch worker start script and unique worker name', async () => {
      await manager.startBatchWorker();
      expect(startScriptStub).to.have.been.calledWith('batch-processing/startBatchWorker.js', { name: 'Worker 7' });

      await manager.startBatchWorker();
      expect(startScriptStub).to.have.been.calledWith('batch-processing/startBatchWorker.js', { name: 'Worker 8' });
    });
  });

  describe('#killWorker', () => {
    it('should resolve true if worker killed successfully', async () => {
      const promise = manager.killWorker();
      const callback = deleteStub.getCall(0).args[1];
      callback(null);
      expect(loggerErrorStub).to.not.have.been.called;
      const result = await promise;
      expect(result).to.equal(true);
    });

    it('should resolve false if specified process ID cannot be found', async () => {
      const notFoundError = new Error('Process not found');

      const promise = manager.killWorker();
      const callback = deleteStub.getCall(0).args[1];
      callback(notFoundError);
      expect(loggerErrorStub).to.not.have.been.called;
      const result = await promise;
      expect(result).to.equal(false);
    });

    it('should handle errors', async () => {
      const promise = manager.killWorker();
      const callback = deleteStub.getCall(0).args[1];
      callback(ERROR);
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
      try {
        await promise;
        expect.fail(0, 1, 'Exception not caught');
      } catch (err) {
        expect(err).to.equal(ERROR);
      }
    });
  });

  describe('#handleWaitingMessage', () => {
    let identifyWorkMock;

    beforeEach(() => {
      identifyWorkMock = sinon.stub(manager, 'identifyNextJobToWork');
      identifyWorkMock.resolves(true);
    });

    afterEach(() => {
      identifyWorkMock.restore();
    });

    it('should add the waiting process ID to the array of waiting workers', () => {
      manager.waitingWorkers.add(-123);
      manager.handleWaitingMessage(waitingMessage);
      expect(manager.waitingWorkers).to.be.eql(new Set([-123, PROCESS_ID]));
      expect(loggerInfoStub).to.have.been.calledWith(`Worker ${PROCESS_ID} is waiting for work`);
    });

    it('should not add duplicate waiting workers to the set', () => {
      manager.waitingWorkers.add(PROCESS_ID);
      manager.handleWaitingMessage(waitingMessage);
      expect(manager.waitingWorkers).to.be.eql(new Set([PROCESS_ID]));
      expect(loggerInfoStub).to.have.been.calledWith(`Worker ${PROCESS_ID} is waiting for work`);
    });

    it('should call identifyNextJobToWork after adding worker to waiting queue', () => {
      manager.handleWaitingMessage(waitingMessage);
      expect(identifyWorkMock).to.have.been.called;
    });

    it('should return a promise with the result of identifyNextJobToWork', () => {
      const promise = manager.handleWaitingMessage(waitingMessage);
      return promise.then((result) => {
        expect(result).to.be.true;
      });
    });
  });

  describe('#identifyNextJobToWork', () => {
    let lockAndRetrieveStub;
    let assignJobToWorkerStub;
    let clock;

    beforeEach(() => {
      clock = sinon.useFakeTimers();

      lockAndRetrieveStub = sinon.stub(manager, 'lockAndRetrieveJob');
      lockAndRetrieveStub.resolves(jobDetails);

      assignJobToWorkerStub = sinon.stub(manager, 'assignJobToWorker');
      assignJobToWorkerStub.resolves(true);

      manager.waitingWorkers.add(PROCESS_ID);
    });

    afterEach(() => {
      clock.restore();
      lockAndRetrieveStub.restore();
      assignJobToWorkerStub.restore();
    });

    it('should clear the setTimeout ref as soon as it starts', async () => {
      manager.jobPollTimer = { id: 5 };
      manager.waitingWorkers.clear();
      await manager.identifyNextJobToWork();
      expect(manager.jobPollTimer).to.not.exist;
    });

    it('should return if there are no available workers', async () => {
      manager.waitingWorkers.clear();
      await manager.identifyNextJobToWork();
      expect(lockAndRetrieveStub).to.have.not.been.called;
      expect(assignJobToWorkerStub).to.have.not.been.called;
    });

    it('should remove the first worker from the waiting queue', async () => {
      manager.waitingWorkers = new Set([PROCESS_ID, -123, -456]);
      await manager.identifyNextJobToWork();
      expect(lockAndRetrieveStub).to.have.been.called;
      expect(assignJobToWorkerStub).to.have.been.calledWith(jobDetails, PROCESS_ID);
      expect(manager.waitingWorkers).to.eql(new Set([-123, -456]));
    });

    it('should schedule an invocation of this same method if no jobs are available', async () => {
      manager.waitingWorkers = new Set([PROCESS_ID, -123, -456]);
      lockAndRetrieveStub.resolves(null);
      await manager.identifyNextJobToWork();
      expect(lockAndRetrieveStub).to.have.been.called;
      expect(assignJobToWorkerStub).to.not.have.been.called;
      expect(manager.waitingWorkers).to.eql(new Set([PROCESS_ID, -123, -456]));
      const identifyStub = sinon.stub(manager, 'identifyNextJobToWork');
      clock.tick(batchConfig.IDLE_POLL_DURATION);
      expect(identifyStub).to.have.been.calledOnce;
      expect(manager.jobPollTimer).to.exist;
    });

    it('should log any errors encountered', async () => {
      manager.waitingWorkers = new Set([PROCESS_ID]);
      lockAndRetrieveStub.rejects(ERROR);
      try {
        await manager.identifyNextJobToWork();
        expect.fail(0, 1, 'Exception not caught');
      } catch (e) {
        expect(loggerErrorStub).to.have.been.calledWith('Error identifying next job to execute');
        expect(loggerErrorStub).to.have.been.calledWith(ERROR);
        expect(e).to.equal(ERROR);
      }
    });
  });

  describe('#assignJobToWorker', async () => {
    let sendMessageStub;
    let scheduleExpirationMock;

    beforeEach(() => {
      sendMessageStub = sinon.stub(manager, 'sendDataToProcessId');
      scheduleExpirationMock = sinon.stub(manager, 'scheduleJobExpiration');
    });

    afterEach(() => {
      sendMessageStub.restore();
      scheduleExpirationMock.restore();
    });

    it('should send a "start work" message to the worker', async () => {
      await manager.assignJobToWorker(jobDetails, PROCESS_ID);
      expect(sendMessageStub).to.have.been.calledWith(PROCESS_ID, JOB_START_MESSAGE_TYPE, { jobId: JOB_ID }, JOB_TOPIC);
      expect(loggerInfoStub).to.have.been.calledWith(`Assigning job ${JOB_ID} to worker ${PROCESS_ID}`);
    });

    it('should update our workerJobMapping to keep track of what workers are doing', async () => {
      expect(_.keys(manager.workerJobMapping).length).to.equal(0);
      await manager.assignJobToWorker(jobDetails, PROCESS_ID);
      expect(_.keys(manager.workerJobMapping).length).to.equal(1);
      expect(manager.workerJobMapping[PROCESS_ID]).to.equal(jobDetails);
    });

    it('should schedule a timeout to kill the job if it runs too long', async () => {
      await manager.assignJobToWorker(jobDetails, PROCESS_ID);
      expect(scheduleExpirationMock).to.have.been.calledWith(jobDetails, PROCESS_ID);
    });
  });

  describe('#handleJobCompleteMessage', () => {
    let identifyWorkStub;
    let cancelExpirationStub;
    let completeMessage;

    beforeEach(() => {
      completeMessage = {
        data: { jobId: JOB_ID },
        process: {
          pm_id: PROCESS_ID
        }
      };

      manager.workerJobMapping[PROCESS_ID] = JOB_ID;

      identifyWorkStub = sinon.stub(manager, 'identifyNextJobToWork');
      cancelExpirationStub = sinon.stub(manager, 'cancelJobExpiration');
    });

    afterEach(() => {
      identifyWorkStub.restore();
      cancelExpirationStub.restore();
    });

    it('should update the workerJobMapping of job IDs => worker IDs', () => {
      expect(_.keys(manager.workerJobMapping).length).to.equal(1);
      manager.handleJobCompleteMessage(completeMessage);

      expect(manager.workerJobMapping[PROCESS_ID]).to.be.undefined;
      expect(_.keys(manager.workerJobMapping).length).to.equal(0);
      expect(loggerInfoStub).to.have.been.calledWith(`Worker ${PROCESS_ID} has finished job ${JOB_ID}`);
    });

    it('should add the worker to the queue of waiting workers', () => {
      expect(manager.waitingWorkers.size).to.equal(0);
      manager.handleJobCompleteMessage(completeMessage);
      expect(manager.waitingWorkers.size).to.be.equal(1);
      expect(manager.waitingWorkers.has(PROCESS_ID)).to.be.true;
    });

    it('should identify the next job to work (now that a worker is free)', () => {
      manager.handleJobCompleteMessage(completeMessage);
      expect(identifyWorkStub).to.have.been.called;
    });

    it('should cancel the expiration timeout of this job', () => {
      manager.handleJobCompleteMessage(completeMessage);
      expect(cancelExpirationStub).to.have.been.calledWith(JOB_ID);
    });
  });

  describe('#handleErrorMessage', () => {
    let errorMessage;
    let terminateJobMock;

    beforeEach(() => {
      errorMessage = {
        data: {
          jobId: JOB_ID,
          error: ERROR
        },
        process: {
          pm_id: PROCESS_ID
        }
      };

      terminateJobMock = sinon.stub(manager, 'terminateJob');
      terminateJobMock.resolves(true);
    });

    afterEach(() => {
      terminateJobMock.restore();
    });

    it('should log the details of the error and call terminateJob', async () => {
      const resultPromise = manager.handleErrorMessage(errorMessage);

      // Log the error
      expect(loggerErrorStub).to.have.been.calledTwice;
      expect(loggerErrorStub).to.have.been.calledWith(
        `Worker ${PROCESS_ID} encountered an error executing job ${JOB_ID}`);
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);

      // Terminate the job
      expect(terminateJobMock).to.have.been.calledWith(JOB_ID, PROCESS_ID);

      // Resolve the terminate job promise
      await resultPromise;

      // Log "terminate success" message
      expect(loggerInfoStub).to.have.been.calledWith(
        `The error on job ${JOB_ID} by worker ${PROCESS_ID} was handled gracefully`);
    });

    it('should log an error if unable to terminate the job for some reason', async () => {
      terminateJobMock.rejects(ERROR);
      const resultPromise = manager.handleErrorMessage(errorMessage);

      // Resolve the terminate job promise
      await resultPromise;

      // Log "terminate success" message
      expect(loggerErrorStub).to.have.been.calledWith(
        `There was a problem handling the error on job ${JOB_ID} by worker ${PROCESS_ID}`);
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
    });
  });

  describe('#cancelJobExpiration', () => {
    let timeoutRef;

    beforeEach(() => {
      timeoutRef = setTimeout(() => {
        expect.fail('this should never execute');
      }, 60000);

      manager.workerTimeoutMapping[JOB_ID] = timeoutRef;
    });

    afterEach(() => {
      clearTimeout(timeoutRef);
    });

    it('should just log a warning if the timeoutRef cannot be found', () => {
      manager.workerTimeoutMapping[JOB_ID] = null;
      manager.cancelJobExpiration(JOB_ID);
      expect(loggerWarnStub).to.have.been.calledWith(`Timeout ref for job ${JOB_ID} is missing`);
    });

    it('should clear the timeout and delete the workerTimeoutMapping', () => {
      manager.cancelJobExpiration(JOB_ID);
      expect(manager.workerTimeoutMapping[JOB_ID]).to.not.exist;
      expect(timeoutRef._onTimeout).to.equal(null);
      expect(timeoutRef._called).to.be.false;
    });
  });

  describe('#scheduleJobExpiration', () => {
    const TIMEOUT_DURATION = 60000;
    let getJobTypeStub;
    let terminateJobStub;

    beforeEach(() => {
      this.clock = sinon.useFakeTimers();

      class FakeJobType extends BatchJobFactory {
        getJobTimeoutDuration() { return TIMEOUT_DURATION; } // eslint-disable-line class-methods-use-this
      }

      getJobTypeStub = sinon.stub(BatchJobFactory, 'getJobImplementationByType');
      getJobTypeStub.returns(FakeJobType);
      terminateJobStub = sinon.stub(manager, 'terminateJob');
      terminateJobStub.resolves();
    });

    afterEach(() => {
      this.clock.restore();
      getJobTypeStub.restore();
      terminateJobStub.restore();
    });

    it('should create an instance of the job type and retrieve timeout details', () => {
      manager.scheduleJobExpiration(jobDetails, PROCESS_ID);
      expect(getJobTypeStub).to.have.been.calledWith(jobDetails.type);
    });

    it('should terminate the job and log an error on failure', async () => {
      terminateJobStub.rejects(ERROR);
      const terminateFunction = manager.scheduleJobExpiration(jobDetails, PROCESS_ID);
      await terminateFunction();
      expect(loggerErrorStub).to.have.been.callCount(3);
      expect(loggerErrorStub).to.have.been.calledWith(
        `Job ${JOB_ID} on worker ${PROCESS_ID} has timed out and must be terminated`);
      expect(terminateJobStub).to.have.been.calledWith(JOB_ID, PROCESS_ID);
      expect(loggerErrorStub).to.have.been.calledWith(`Error terminating stuck job ${JOB_ID} on worker ${PROCESS_ID}`);
      expect(loggerErrorStub).to.have.been.calledWith(ERROR);
    });

    it('should terminate the job and do nothing on success', async () => {
      const terminateFunction = manager.scheduleJobExpiration(jobDetails, PROCESS_ID);
      await terminateFunction();
      expect(loggerErrorStub).to.have.been.calledOnce;
    });

    it('should execute the terminate function after a specified timeout', () => {
      manager.scheduleJobExpiration(jobDetails, PROCESS_ID);
      expect(terminateJobStub).to.not.have.been.called;
      this.clock.tick(TIMEOUT_DURATION);
      expect(terminateJobStub).to.have.been.called;
    });

    it('should save a reference to the timeout so we can cancel it later', () => {
      manager.scheduleJobExpiration(jobDetails, PROCESS_ID);
      const timeoutRef = manager.workerTimeoutMapping[JOB_ID];
      expect(timeoutRef).to.exist;
    });
  });

  describe('#terminateJob', () => {
    let getJobByIdStub;
    let cancelExpirationStub;
    let killWorkerStub;
    let startWorkerStub;
    let updateJobErrorStub;

    beforeEach(() => {
      manager.workerJobMapping[PROCESS_ID] = JOB_ID;
      manager.waitingWorkers.add(PROCESS_ID);

      getJobByIdStub = sinon.stub(JobQueueService, 'getJobById');
      getJobByIdStub.resolves(jobDetails);
      cancelExpirationStub = sinon.stub(manager, 'cancelJobExpiration');
      killWorkerStub = sinon.stub(manager, 'killWorker');
      killWorkerStub.resolves(false);
      startWorkerStub = sinon.stub(manager, 'startBatchWorker');
      startWorkerStub.resolves([]);
      updateJobErrorStub = sinon.stub(JobQueueService, 'updateJobError');
    });

    afterEach(() => {
      getJobByIdStub.restore();
      cancelExpirationStub.restore();
      killWorkerStub.restore();
      startWorkerStub.restore();
      updateJobErrorStub.restore();
    });

    it('should cancel any expiration, kill the worker, and restart the worker', async () => {
      await manager.terminateJob(JOB_ID, PROCESS_ID);

      expect(manager.workerJobMapping[PROCESS_ID]).to.be.undefined;
      expect(manager.waitingWorkers.has(PROCESS_ID)).to.be.false;
      expect(getJobByIdStub).to.have.been.calledWith(JOB_ID);
      expect(cancelExpirationStub).to.have.been.calledWith(JOB_ID);
      expect(updateJobErrorStub).to.have.been.calledWith(JOB_ID, jobDetails.resultsError);
      expect(loggerInfoStub).to.have.been.calledWith(`Worker process ${PROCESS_ID} had terminated gracefully`);
      expect(killWorkerStub).to.have.been.calledWith(PROCESS_ID);
      expect(startWorkerStub).to.have.been.calledWith();
      expect(loggerInfoStub).to.have.been.calledWith('Starting replacement worker process');
    });

    it('should log a message indicating that the process had to be forcefully killed', async () => {
      killWorkerStub.resolves(true);
      await manager.terminateJob(JOB_ID, PROCESS_ID);
      expect(loggerErrorStub).to.have.been.calledWith(`Worker process ${PROCESS_ID} had to be terminated forcefully`);
    });
  });

  describe('#lockAndRetrieveJob', () => {
    let lockStub;

    beforeEach(() => {
      lockStub = sinon.stub(JobQueueService, 'lockNextQueuedJob');
    });

    afterEach(() => {
      lockStub.restore();
    });

    it('should call "lockNextQueuedJob" on the JobQueueService', async () => {
      await manager.lockAndRetrieveJob(JOB_ID);
      expect(lockStub).to.have.been.calledWith(JOB_ID);
    });
  });
});
