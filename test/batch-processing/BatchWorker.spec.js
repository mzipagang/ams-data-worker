const sinon = require('sinon');
const faker = require('faker');
const { expect } = require('chai');
const mongoose = require('mongoose');
const BatchWorker = require('../../batch-processing/BatchWorker');
const logger = require('../../app/services/logger');
const BatchJobFactory = require('../../batch-processing/BatchJobFactory');
const {
  WORKER_ERROR_MESSAGE_TYPE,
  WORKER_READY_MESSAGE_TYPE,
  WORKER_WAITING_MESSAGE_TYPE,
  WORKER_COMPLETE_MESSAGE_TYPE,
  JOB_START_MESSAGE_TYPE
} = require('../../app/constants').BATCH;

describe('BatchWorker', () => {
  const ERROR = new Error('Expected error');
  const JOB_ID = faker.random.number({ min: 1, max: 999 });

  let message;
  let batchConfig;
  let worker;
  let loggerInfo;
  let loggerWarn;
  let loggerError;

  beforeEach(() => {
    message = { hello: 'world', data: {} };

    batchConfig = {
      MONGOOSE: {
        URI: process.env.MONGODB // overridden in mocha setup
      }
    };

    worker = new BatchWorker(batchConfig);
    loggerInfo = sinon.stub(logger, 'info');
    loggerWarn = sinon.stub(logger, 'warn');
    loggerError = sinon.stub(logger, 'error');
  });

  afterEach(() => {
    loggerInfo.restore();
    loggerWarn.restore();
    loggerError.restore();
  });

  describe('constructor', () => {
    it('should set the config property', () => {
      worker = new BatchWorker('the config');
      expect(worker).to.not.be.null;
    });
  });

  describe('#startWorker', () => {
    let onStub;
    let sendStub;
    let handleMessageStub;
    let handleErrorStub;
    let killWorkerStub;
    let sendWaitingStub;
    let connectStub;

    beforeEach(() => {
      onStub = sinon.stub(process, 'on');
      if (!process.send) {
        process.send = () => {
        }; // PM2 might have set this in another test
      }
      sendStub = sinon.stub(process, 'send');
      handleMessageStub = sinon.stub(worker, 'handleMessage');
      handleErrorStub = sinon.stub(worker, 'handleError');
      killWorkerStub = sinon.stub(worker, 'killWorker');
      sendWaitingStub = sinon.stub(worker, 'sendWaitingMessageToProcessManager');
      connectStub = sinon.stub(mongoose, 'connect');
    });

    afterEach(() => {
      onStub.restore();
      sendStub.restore();
      handleMessageStub.restore();
      handleErrorStub.restore();
      killWorkerStub.restore();
      sendWaitingStub.restore();
      connectStub.restore();
    });

    it('should listen for messages and SIGINT', async () => {
      await worker.startWorker();
      expect(onStub).to.have.been.calledWith('message', sinon.match.func);
      expect(onStub).to.have.been.calledWith('SIGINT', sinon.match.func);
    });

    it('should pass the message to #handleMessage when received', async () => {
      await worker.startWorker();
      const callback = onStub.getCalls()[0].args[1];
      callback(message);
      expect(handleMessageStub).to.have.been.calledWith(message);
    });

    it('should call killWorker on a SIGINT event', async () => {
      await worker.startWorker();
      const callback = onStub.getCalls()[1].args[1];
      callback();
      expect(killWorkerStub).to.have.been.calledWith();
    });


    it('should notify the PM2 daemon (ProcessManager) that the worker is ready', async () => {
      await worker.startWorker();
      expect(sendStub).to.have.been.calledWith(WORKER_READY_MESSAGE_TYPE);
    });

    it('should notify ProcessManager that the worker is waiting', async () => {
      await worker.startWorker();
      expect(sendWaitingStub).to.have.been.called;
    });

    it('should handle errors with the "handleError" method', async () => {
      connectStub.rejects(ERROR);
      await worker.startWorker();
      expect(handleErrorStub).to.have.been.calledWith(ERROR, null);
    });
  });

  describe('#executeJob', () => {
    let jobInstance;
    let createJobInstanceStub;
    let sendCompletedStub;
    let handleErrorStub;

    beforeEach(() => {
      jobInstance = {
        executeJob: sinon.stub()
      };

      createJobInstanceStub = sinon.stub(BatchJobFactory, 'createJobInstance');
      createJobInstanceStub.resolves(jobInstance);

      sendCompletedStub = sinon.stub(worker, 'sendCompletedMessageToProcessManager');
      handleErrorStub = sinon.stub(worker, 'handleError');
    });

    afterEach(() => {
      createJobInstanceStub.restore();
      sendCompletedStub.restore();
      handleErrorStub.restore();
    });

    it('should get a job instance using the BatchJobFactory', async () => {
      await worker.executeJob(JOB_ID);
      expect(createJobInstanceStub).to.have.been.calledWith(JOB_ID);
    });

    it('should handle any exceptions by calling handleError()', async () => {
      createJobInstanceStub.rejects(ERROR);
      await worker.executeJob(JOB_ID);
      expect(sendCompletedStub).to.not.have.been.called;
      expect(handleErrorStub).to.have.been.calledWith(ERROR, JOB_ID);
    });

    it('should execute the jobInstance by calling executeJob()', async () => {
      await worker.executeJob(JOB_ID);
      expect(jobInstance.executeJob).to.have.been.called;
    });

    it('should send the "completed" message to the ProcessManager after the job is done', async () => {
      await worker.executeJob(JOB_ID);
      expect(sendCompletedStub).to.have.been.calledWith(JOB_ID);
    });
  });

  describe('#handleError', () => {
    let sendStub;
    let killWorkerStub;

    beforeEach(() => {
      sendStub = sinon.stub(process, 'send');
      killWorkerStub = sinon.stub(worker, 'killWorker');
    });

    afterEach(() => {
      sendStub.restore();
      killWorkerStub.restore();
    });

    it('should log the error and notify the ProcessManager with a specified job ID', () => {
      worker.handleError(ERROR, JOB_ID);

      // Log the error
      expect(loggerError).to.have.been.calledWith(`BatchWorker encountered an error. Current Job ID: ${JOB_ID}`);
      expect(loggerError).to.have.been.calledWith(ERROR);

      // Notify process manager
      expect(sendStub).to.have.been.calledWith({
        type: WORKER_ERROR_MESSAGE_TYPE,
        data: {
          error: ERROR.message,
          jobId: JOB_ID
        }
      });
    });

    it('should log the error and notify the ProcessManager with no job ID specified', () => {
      worker.handleError(ERROR, null);

      // Log the error
      expect(loggerError).to.have.been.calledWith('BatchWorker encountered an error. Not currently executing a job');
      expect(loggerError).to.have.been.calledWith(ERROR);

      // Notify process manager
      expect(sendStub).to.have.been.calledWith({
        type: WORKER_ERROR_MESSAGE_TYPE,
        data: {
          error: ERROR.message,
          jobId: null
        }
      });
    });

    it('should kill the data worker on an error', () => {
      worker.handleError(ERROR);
      expect(killWorkerStub).to.have.been.calledWith(1);
    });
  });

  describe('#killWorker', () => {
    let disconnectStub;
    let exitStub;

    beforeEach(() => {
      disconnectStub = sinon.stub(mongoose, 'disconnect');
      exitStub = sinon.stub(process, 'exit');
    });

    afterEach(() => {
      disconnectStub.restore();
      exitStub.restore();
    });

    it('should log an info message notifying shutdown', async () => {
      await worker.killWorker();
      expect(loggerInfo).to.have.been.calledWith('Killing data worker process');
    });

    it('should disconnect from Mongo', async () => {
      await worker.killWorker();
      expect(disconnectStub).to.have.been.calledWith();
    });

    it('should exit the process with 0 by default', async () => {
      await worker.killWorker();
      expect(exitStub).to.have.been.calledWith(0);
    });

    it('should exit the process with explicit code if specified', async () => {
      await worker.killWorker(1);
      expect(exitStub).to.have.been.calledWith(1);
    });

    it('should always exit the process, even if another Error is thrown', async () => {
      disconnectStub.rejects(ERROR);

      try {
        await worker.killWorker();
        expect.fail(0, 1, 'Exception not caught');
      } catch (e) {
        expect(e).to.equal(ERROR);
      }
      expect(exitStub).to.have.been.calledWith(1);
    });
  });

  describe('#sendWaitingMessageToProcessManager', () => {
    let sendStub;

    beforeEach(() => {
      sendStub = sinon.stub(process, 'send');
    });

    afterEach(() => {
      sendStub.restore();
    });

    it('should notify ProcessManager that the worker is waiting', () => {
      worker.sendWaitingMessageToProcessManager();
      expect(sendStub).to.have.been.calledWith({
        type: WORKER_WAITING_MESSAGE_TYPE,
        data: {
          success: true
        }
      });
    });
  });

  describe('#sendCompletedMessageToProcessManager', () => {
    let sendStub;

    beforeEach(() => {
      sendStub = sinon.stub(process, 'send');
    });

    afterEach(() => {
      sendStub.restore();
    });

    it('should notify ProcessManager that the job is complete', () => {
      worker.sendCompletedMessageToProcessManager(JOB_ID);
      expect(sendStub).to.have.been.calledWith({
        type: WORKER_COMPLETE_MESSAGE_TYPE,
        data: {
          jobId: JOB_ID
        }
      });
    });
  });

  describe('#handleMessage', () => {
    let executeJobStub;

    beforeEach(() => {
      executeJobStub = sinon.stub(worker, 'executeJob');
    });

    afterEach(() => {
      executeJobStub.restore();
    });

    it('should call "executeJob" when the "start job" message type is received', () => {
      message.type = JOB_START_MESSAGE_TYPE;
      message.data.jobId = 1234;
      worker.handleMessage(message);
      expect(executeJobStub).to.have.been.calledWith(1234);
      expect(logger.warn).to.not.have.been.called;
    });

    it('should log a warning for unhandled message types', () => {
      message.type = 'unhandled';
      worker.handleMessage(message);
      expect(logger.warn).to.have.been.calledOnce;
      expect(logger.warn).to.have.been.calledWith('BatchWorker is unable to handle message of type "unhandled"');
    });
  });
});
