const guid = require('uuid/v4');

const id = guid();

module.exports = id;
