const dbGuid = require('./test_db_guid.js');

module.exports = {
  use: () => {
    delete process.env.AWS_ACCESS_KEY_ID;
    delete process.env.AWS_SECRET_ACCESS_KEY;
    process.env.PORT = 12345;
    process.env.NODE_ENV = 'dev';
    process.env.MONGODB = `mongodb://localhost:20017/ams-api-test-${dbGuid}`;
  }
};
