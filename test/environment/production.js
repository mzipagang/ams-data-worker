const dbGuid = require('./test_db_guid.js');

module.exports = {
  use: () => {
    process.env.PORT = 54321;
    process.env.AWS_ACCESS_KEY_ID = 'somekey';
    process.env.AWS_SECRET_ACCESS_KEY = 'somesecretkey';
    process.env.NODE_ENV = 'production';
    process.env.MONGODB = `mongodb://localhost:20017/ams-api-test-${dbGuid}`;
  }
};
