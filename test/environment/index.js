const path = require('path');

const envStash = Object.assign({}, process.env);

module.exports = {
  reset: (dir, files) => {
    if (dir && files) {
      files.forEach(file => delete require.cache[require.resolve(path.join(dir, file))]);
    }

    process.env = Object.assign({}, envStash);
    return this;
  },
  use: (environment) => {
    require(`./${environment}`).use();
    return this;
  }
};
