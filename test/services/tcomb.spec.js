const schemaService = require('../../app/schemas');
const ProviderSchema = require('../../app/schemas/Provider');
const EntitySchema = require('../../app/schemas/Entity');
const errorConfig = require('../../app/services/validation/rules/config');
const { expect } = require('chai');

describe('service: schemas', () => {
  describe('ConvertToErrorCodes()', () => {
    const validationResults = {
      error: {
        details: [
          {
            path: ['start_date']
          },
          {
            path: ['end_date']
          }
        ]
      }
    };

    const unmappableValidationResults = {
      error: {
        details: [
          {
            path: ['fake_field_1']
          },
          {
            path: ['fake_field_2']
          }
        ]
      }
    };

    describe('Provider File Type', () => {
      it('should convert schema properly to an error code', () => {
        const results = schemaService.ConvertToErrorCodes(
          validationResults,
          ProviderSchema.MapFieldNameToErrorRule,
          errorConfig.provider
        );
        expect(results.length).to.equal(2);
        expect(results[0]).to.equal('C1');
        expect(results[1]).to.equal('D1');
      });

      it('should skip schema error if there is no error code to map to', () => {
        const results = schemaService.ConvertToErrorCodes(
          unmappableValidationResults,
          ProviderSchema.MapFieldNameToErrorRule,
          errorConfig.provider
        );
        expect(results.length).to.equal(0);
      });
    });

    describe('Entity File Type', () => {
      it('should convert schema properly to an error code', () => {
        const results = schemaService.ConvertToErrorCodes(
          validationResults,
          EntitySchema.MapFieldNameToErrorRule,
          errorConfig.entity
        );
        expect(results.length).to.equal(2);
        expect(results[0]).to.equal('D1');
        expect(results[1]).to.equal('E1');
      });

      it('should skip schema error if there is no error code to map to', () => {
        const results = schemaService.ConvertToErrorCodes(
          unmappableValidationResults,
          EntitySchema.MapFieldNameToErrorRule,
          errorConfig.entity
        );
        expect(results.length).to.equal(0);
      });
    });
  });
});
