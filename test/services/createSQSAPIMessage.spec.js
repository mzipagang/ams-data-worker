const chai = require('chai');
const createSQSAPIMessage = require('../../app/services/createSQSAPIMessage/index.js');

const expect = chai.expect;

describe('Service: createSQSAPIMessage', () => {
  it('it should filter invalid priorities', () => {
    try {
      createSQSAPIMessage(0, 'test', {});
    } catch (err) {
      expect(err).to.not.be.undefined;
    }
  });
});
