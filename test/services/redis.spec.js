const sinon = require('sinon');
const redis = require('redis');
const proxyquire = require('proxyquire');

const Promise = require('bluebird');
const EventEmitter = require('events');
const logger = require('../../app/services/logger');

const delay = 500;

describe('Service: Redis', () => {
  let redisService;
  let RedisServiceMock;
  let createClientStub;
  let eventEmitter;
  let loggerStub;

  before(() => {
    eventEmitter = new EventEmitter();
    eventEmitter.quit = () => {};
    createClientStub = sinon.stub(redis, 'createClient');
    createClientStub.callsFake(() => {
      setTimeout(() => eventEmitter.emit('connect'), 1000);
      return eventEmitter;
    });
    loggerStub = sinon.stub(logger, 'error');
    loggerStub.callsFake(() => {});
  });

  beforeEach(() => {
    redisService = proxyquire('../../app/services/redis', {});
    RedisServiceMock = sinon.mock(redisService);
  });

  afterEach(() => {
    RedisServiceMock.restore();
  });

  after(() => {
    createClientStub.restore();
    loggerStub.restore();
  });

  it('it should connect to redis and disconnect', () => {
    redisService.connectToRedis();
    return Promise.delay(delay).then(() => redisService.disconnectFromRedis());
  }).timeout(2000);

  it('it should connect to redis and disconnect and not allow a reconnect', (done) => {
    const EXPECTED_TIMEOUT = 1500;
    const timeout = setTimeout(done, EXPECTED_TIMEOUT);

    redisService.connectToRedis();
    Promise.delay(delay)
      .then(() => redisService.disconnectFromRedis())
      .then(() => redisService.connectToRedis())
      .then(() => {
        clearTimeout(timeout);
        // this should never happen, is the expected behavior.
        done(new Error('Unexpected call'));
      });
  }).timeout(2000);

  it('it should should properly disconnect after an error is thrown', () => {
    redisService.connectToRedis();

    return Promise.delay(delay)
      .then(() => {
        eventEmitter.emit('error', new Error('Fake Error'));
      })
      .then(() => Promise.delay(100))
      .then(() => redisService.disconnectFromRedis());
  }).timeout(2000);
});
