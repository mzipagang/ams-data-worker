const moment = require('moment');
const sinon = require('sinon');
const { expect } = require('chai');
const SessionService = require('../../app/services/SessionService');
const logger = require('../../app/services/logger');
const Session = require('../../app/services/mongoose/models/session');
const User = require('../../app/services/mongoose/models/user');
const config = require('../../app/config');

describe('service: SessionService', () => {
  const FUTURE_TIME = moment
    .utc()
    .add(1, 'day')
    .valueOf();
  const PAST_TIME = moment
    .utc()
    .subtract(1, 'day')
    .valueOf();
  const USERNAME = 'Phil';
  const ERROR_BODY = 'ERROR';
  const SUCCESS_BODY = 'SUCCESS';
  const ERROR = new Error('Expected error message');

  let req;
  let res;
  let user;
  let session;
  let sessionInfo;
  let token;

  beforeEach(() => {
    token = 'Some really secret token';

    user = {
      email: 'email@b.c'
    };

    session = {
      data: {
        username: USERNAME
      }
    };

    sessionInfo = { info: true };

    req = {
      body: SUCCESS_BODY
    };

    res = {
      status: sinon.stub(),
      json: sinon.stub()
    };

    res.status.returns(res);

    sinon.stub(logger, 'error');
    sinon.stub(User, 'getUser');

    User.getUser.withArgs(USERNAME).resolves(user);
  });

  afterEach(() => {
    User.getUser.restore();
    logger.error.restore();
  });

  describe('#handleError', () => {
    it('should log an error and return 500 if an error occurs', () => {
      const message = 'An expected error';
      SessionService.handleError(res, ERROR, message);
      expect(logger.error).to.have.been.calledWith(ERROR);
      expect(res.status).to.have.been.calledWith(500);
      expect(res.json).to.have.been.calledWith({ message });
    });
  });

  describe('#getSessionInfo', () => {
    it('should return the defaults if no parameter supplied', () => {
      const result = SessionService.getSessionInfo();
      expect(result.success).to.be.true;
      expect(result.expiresInSeconds).to.equal(0);
      expect(result.isActive).to.be.false;
    });

    it('should set the expiration to max value if not specified', () => {
      const result = SessionService.getSessionInfo({ expiresAt: null });
      expect(result.expiresInSeconds).to.equal(Number.MAX_VALUE);
    });

    it('should set the expiration to negative value and isActive to false if expired in the past', () => {
      const result = SessionService.getSessionInfo({ expiresAt: PAST_TIME });
      expect(result.expiresInSeconds).to.be.lessThan(0);
      expect(result.isActive).to.be.false;
    });

    it('should set the expiration to positive value and isActive to true if expired in the future', () => {
      const result = SessionService.getSessionInfo({ expiresAt: FUTURE_TIME });
      expect(result.expiresInSeconds).to.be.greaterThan(0);
      expect(result.isActive).to.be.true;
    });

    it('should NOT set the username if no data supplied', () => {
      let result = SessionService.getSessionInfo({ expiresAt: FUTURE_TIME });
      expect(result.username).to.be.undefined;

      result = SessionService.getSessionInfo({ data: {} });
      expect(result.username).to.be.undefined;
    });

    it('should set the username to the username in the data if available', () => {
      const result = SessionService.getSessionInfo({
        data: { username: 'Phil' }
      });
      expect(result.username).to.equal('Phil');
    });
  });

  describe('#createSession', () => {
    const TIMEOUT = config.SESSION.TIMEOUT_IN_SECONDS;

    beforeEach(() => {
      sinon.stub(Session, 'createSession');
      sinon.stub(SessionService, 'getSessionInfo');
      sinon.stub(SessionService, 'handleError');

      Session.createSession.withArgs(ERROR_BODY, TIMEOUT).rejects(ERROR);
      Session.createSession.withArgs(SUCCESS_BODY, TIMEOUT).resolves({ session, token });
      SessionService.getSessionInfo.withArgs(session).returns(sessionInfo);
    });

    afterEach(() => {
      Session.createSession.restore();
      SessionService.getSessionInfo.restore();
      SessionService.handleError.restore();
    });

    it('should log an error and return 500 if an exception occurs', async () => {
      req.body = ERROR_BODY;
      await SessionService.createSession(req, res);
      expect(SessionService.handleError).to.have.been.calledWith(
        res,
        ERROR,
        'There was an error while getting the session'
      );
    });

    it('should get the user and session info', async () => {
      await SessionService.createSession(req, res);

      expect(Session.createSession).to.have.been.calledWith(SUCCESS_BODY, TIMEOUT);
      expect(User.getUser).to.have.been.calledWith(USERNAME);
      expect(SessionService.getSessionInfo).to.have.been.calledWith(session);
      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({
        data: {
          user: {
            email: 'email@b.c',
            token,
            session: sessionInfo
          }
        }
      });
    });
  });

  describe('#deleteSession', () => {
    beforeEach(() => {
      req = {
        query: {
          token: SUCCESS_BODY
        }
      };

      sinon.stub(Session, 'deleteSession');
      sinon.stub(SessionService, 'handleError');

      Session.deleteSession.withArgs(ERROR_BODY).rejects(ERROR);
      Session.deleteSession.withArgs(SUCCESS_BODY).resolves();
    });

    afterEach(() => {
      Session.deleteSession.restore();
      SessionService.handleError.restore();
    });

    it('should log an error and return 500 if an exception occurs', async () => {
      req.query.token = ERROR_BODY;
      await SessionService.deleteSession(req, res);
      expect(SessionService.handleError).to.have.been.calledWith(
        res,
        ERROR,
        'There was an error while deleting the session'
      );
    });

    it('should delete the session successfully', async () => {
      await SessionService.deleteSession(req, res);

      expect(Session.deleteSession).to.have.been.calledWith(SUCCESS_BODY);
      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({
        data: {
          success: true
        }
      });
    });
  });

  describe('#getSession', () => {
    beforeEach(() => {
      req = {
        query: {
          token: SUCCESS_BODY,
          extend: 'true'
        }
      };

      sinon.stub(Session, 'getAndExtendSession');
      sinon.stub(Session, 'getSession');
      sinon.stub(SessionService, 'getSessionInfo');
      sinon.stub(SessionService, 'handleError');

      Session.getAndExtendSession.withArgs(ERROR_BODY).rejects(ERROR);
      Session.getAndExtendSession.withArgs(SUCCESS_BODY).resolves({ session: null });
      Session.getSession.withArgs(SUCCESS_BODY).resolves({ session });
      SessionService.getSessionInfo.withArgs(session).returns(sessionInfo);
    });

    afterEach(() => {
      Session.getAndExtendSession.restore();
      Session.getSession.restore();
      SessionService.getSessionInfo.restore();
      SessionService.handleError.restore();
    });

    it('should log an error and return 500 if an exception occurs', async () => {
      req.query.token = ERROR_BODY;
      await SessionService.getSession(req, res);
      expect(SessionService.handleError).to.have.been.calledWith(
        res,
        ERROR,
        'There was an error while getting the session'
      );
    });

    it('should extend the session if instructed to do so', async () => {
      req.query.extend = 'true';
      await SessionService.getSession(req, res);
      expect(Session.getAndExtendSession).to.have.been.calledWith(SUCCESS_BODY);
      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({
        data: {
          user: null
        }
      });
    });

    it('should return the session if not instructed to extend. Also return the user if session found', async () => {
      req.query.extend = 'false';
      await SessionService.getSession(req, res);
      expect(Session.getSession).to.have.been.calledWith(SUCCESS_BODY);
      expect(User.getUser).to.have.been.calledWith(USERNAME);
      expect(SessionService.getSessionInfo).to.have.been.calledWith(session);
      expect(res.status).to.have.been.calledWith(200);
      expect(res.json).to.have.been.calledWith({
        data: {
          user: {
            email: 'email@b.c',
            token: SUCCESS_BODY,
            session: sessionInfo
          }
        }
      });
    });
  });
});
