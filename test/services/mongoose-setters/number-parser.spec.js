const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const parseNumber = require('../../../app/services/mongoose/setters/number-parser');

describe('Mongoose Setter: Number Parser', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('asInteger', () => {
    describe('when empty string is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asInteger(' ');
        expect(result).to.equal(0);
      });
    });
    describe('when null is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asInteger(null);
        expect(result).to.equal(0);
      });
    });
    describe('when string null is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asInteger('null');
        expect(result).to.equal(0);
      });
    });
    describe('when undefined is passed in', () => {
      it('should return undefined', () => {
        const result = parseNumber.asInteger(undefined);
        expect(result).to.be.undefined;
      });
    });
    describe('when non-numeric string is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asInteger('not a number');
        expect(result).to.equal(0);
      });
    });
    describe('when a number is passed in', () => {
      it('should return that number', () => {
        const result = parseNumber.asInteger(14641275);
        expect(result).to.equal(14641275);
      });
    });
    describe('when a decimal number is passed in', () => {
      it('should return that number as integer', () => {
        const result = parseNumber.asInteger(14641275.9);
        expect(result).to.equal(14641275);
      });
    });
    describe('when a string decimal number is passed in', () => {
      it('should return that parsed number as integer', () => {
        const result = parseNumber.asInteger('14641275.9');
        expect(result).to.equal(14641275);
      });
    });
    describe('when string number is passed in', () => {
      it('should return parsed number', () => {
        const result = parseNumber.asInteger('42');
        expect(result).to.equal(42);
      });
    });
    describe('when a number like 123.0X is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asInteger('123.0X');
        expect(result).to.equal(0);
      });
    });
    describe('when a number like 23X23 is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asInteger('23X23');
        expect(result).to.equal(0);
      });
    });
  });

  describe('asDecimal', () => {
    describe('when empty string is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asDecimal('');
        expect(result).to.equal(0);
      });
    });
    describe('when null is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asDecimal(null);
        expect(result).to.equal(0);
      });
    });
    describe('when string null is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asDecimal('null');
        expect(result).to.equal(0);
      });
    });
    describe('when undefined is passed in', () => {
      it('should return undefined', () => {
        const result = parseNumber.asDecimal(undefined);
        expect(result).to.be.undefined;
      });
    });
    describe('when non-numeric string is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asDecimal('not a number');
        expect(result).to.equal(0);
      });
    });
    describe('when a number is passed in', () => {
      it('should return that number', () => {
        const result = parseNumber.asDecimal(14641275);
        expect(result).to.equal(14641275);
      });
    });
    describe('when a decimal number is passed in', () => {
      it('should return that number with 2 decimal places', () => {
        const result = parseNumber.asDecimal(14641275.9);
        expect(result).to.equal(14641275.90);
      });
    });
    describe('when a string decimal number is passed in', () => {
      it('should return that parsed number with 2 decimal places', () => {
        const result = parseNumber.asDecimal('14641275.92');
        expect(result).to.equal(14641275.92);
      });
    });
    describe('when string number is passed in', () => {
      it('should return parsed number', () => {
        const result = parseNumber.asDecimal('42');
        expect(result).to.equal(42.00);
      });
    });
    describe('when a number like 123.0X is passed in', () => {
      it('should return 0', () => {
        const result = parseNumber.asDecimal('123.0X');
        expect(result).to.equal(0);
      });
    });
    describe('when a number like 23X23 is passed in', () => {
      it('should should return 0', () => {
        const result = parseNumber.asDecimal('23X23');
        expect(result).to.equal(0);
      });
    });
  });

  describe('asDecimalIfNumeric', () => {
    describe('when empty string is passed in', () => {
      it('should return an empty string', () => {
        const result = parseNumber.asDecimalIfNumeric('');
        expect(result).to.equal('');
      });
    });
    describe('when null is passed in', () => {
      it('should return null', () => {
        const result = parseNumber.asDecimalIfNumeric(null);
        expect(result).to.equal(null);
      });
    });
    describe('when string null is passed in', () => {
      it('should return string null', () => {
        const result = parseNumber.asDecimalIfNumeric('null');
        expect(result).to.equal('null');
      });
    });
    describe('when undefined is passed in', () => {
      it('should return undefined', () => {
        const result = parseNumber.asDecimalIfNumeric(undefined);
        expect(result).to.be.undefined;
      });
    });
    describe('when non-numeric string is passed in', () => {
      it('should return the same non-numeric string', () => {
        const result = parseNumber.asDecimalIfNumeric('not a number');
        expect(result).to.equal('not a number');
      });
    });
    describe('when a number is passed in', () => {
      it('should return that number', () => {
        const result = parseNumber.asDecimalIfNumeric(14641275);
        expect(result).to.equal(14641275);
      });
    });
    describe('when a decimal number is passed in', () => {
      it('should return that number with 2 decimal places', () => {
        const result = parseNumber.asDecimalIfNumeric(14641275.9);
        expect(result).to.equal(14641275.90);
      });
    });
    describe('when a string decimal number is passed in', () => {
      it('should return that parsed number with 2 decimal places', () => {
        const result = parseNumber.asDecimalIfNumeric('14641275.92');
        expect(result).to.equal(14641275.92);
      });
    });
    describe('when string number is passed in', () => {
      it('should return parsed number', () => {
        const result = parseNumber.asDecimalIfNumeric('42');
        expect(result).to.equal(42.00);
      });
    });
    describe('when a number like 123.0X is passed in', () => {
      it('should return  a string like 123.0X', () => {
        const result = parseNumber.asDecimalIfNumeric('123.0X');
        expect(result).to.equal('123.0X');
      });
    });
    describe('when a number like 23X23 is passed in', () => {
      it('should should return a string like 23X23', () => {
        const result = parseNumber.asDecimalIfNumeric('23X23');
        expect(result).to.equal('23X23');
      });
    });
  });

  describe('asIntegerIfNumeric', () => {
    describe('when empty string is passed in', () => {
      it('should return an empty string', () => {
        const result = parseNumber.asIntegerIfNumeric('');
        expect(result).to.equal('');
      });
    });
    describe('when null is passed in', () => {
      it('should return null', () => {
        const result = parseNumber.asIntegerIfNumeric(null);
        expect(result).to.equal(null);
      });
    });
    describe('when string null is passed in', () => {
      it('should return string null', () => {
        const result = parseNumber.asIntegerIfNumeric('null');
        expect(result).to.equal('null');
      });
    });
    describe('when undefined is passed in', () => {
      it('should return undefined', () => {
        const result = parseNumber.asIntegerIfNumeric(undefined);
        expect(result).to.be.undefined;
      });
    });
    describe('when non-numeric string is passed in', () => {
      it('should return the same non-numeric string', () => {
        const result = parseNumber.asIntegerIfNumeric('not a number');
        expect(result).to.equal('not a number');
      });
    });
    describe('when a number is passed in', () => {
      it('should return that number', () => {
        const result = parseNumber.asIntegerIfNumeric(14641275);
        expect(result).to.equal(14641275);
      });
    });
    describe('when a decimal number is passed in', () => {
      it('should return that number without decimals', () => {
        const result = parseNumber.asIntegerIfNumeric(14641275.9);
        expect(result).to.equal(14641275);
      });
    });
    describe('when a string decimal number is passed in', () => {
      it('should return that parsed number without decimals', () => {
        const result = parseNumber.asIntegerIfNumeric('14641275.92');
        expect(result).to.equal(14641275);
      });
    });
    describe('when string number is passed in', () => {
      it('should return parsed number', () => {
        const result = parseNumber.asIntegerIfNumeric('42');
        expect(result).to.equal(42.00);
      });
    });
    describe('when a number like 123.0X is passed in', () => {
      it('should return  a string like 123.0X', () => {
        const result = parseNumber.asIntegerIfNumeric('123.0X');
        expect(result).to.equal('123.0X');
      });
    });
    describe('when a number like 23X23 is passed in', () => {
      it('should should return a string like 23X23', () => {
        const result = parseNumber.asIntegerIfNumeric('23X23');
        expect(result).to.equal('23X23');
      });
    });
  });
});
