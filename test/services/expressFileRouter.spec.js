const environment = require('../environment');
const express = require('express');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();

const expect = chai.expect;

describe('Service: ExpressFileRouter', () => {
  const requireExpressFileRouter = (overrideMocks = {}) =>
    proxyquire(
      '../../app/services/expressFileRouter',
      Object.assign(
        {
          'express-file-router': {
            load: () => express.Router()
          }
        },
        overrideMocks
      )
    );

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/expressFileRouter', '../../app/config']);
      environment.use('dev');
    });

    it('should properly load', () => {
      const expressFileRouter = requireExpressFileRouter();

      expect(expressFileRouter).to.not.be.undefined;
      expect(expressFileRouter.load).to.not.be.undefined;
      expect(expressFileRouter.load()).to.not.be.undefined;
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/expressFileRouter', '../../app/config']);
      environment.use('production');
    });

    it('should properly load', () => {
      const expressFileRouter = requireExpressFileRouter();

      expect(expressFileRouter).to.not.be.undefined;
      expect(expressFileRouter.load).to.not.be.undefined;
      expect(expressFileRouter.load()).to.not.be.undefined;
    });
  });
});
