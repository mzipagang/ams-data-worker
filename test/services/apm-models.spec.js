const { expect } = require('chai');
const { FileImportSourceMap, Models } = require('../../app/services/apm-models');
const config = require('../../app/config');
const environment = require('../environment');

describe('Service: Apm Models', () => {
  describe('#FileImportsourceMap', () => {
    afterEach(() => {
      environment.reset(__dirname, ['../../app/config']);
    });

    it('should return pac models map', () => {
      expect(FileImportSourceMap['pac-model']).to.not.be.undefined;
    });

    it('should return source files map if DISABLE_SOURCE_FILES=false', () => {
      config.FEATURE_FLAGS.DISABLE_SOURCE_FILES = false;
      environment.reset(__dirname, ['../../app/services/apm-models']);
      expect(FileImportSourceMap['cmmi-entity']).to.not.be.undefined;
    });

    it('should not return source files map if DISABLE_SOURCE_FILES=true', () => {
      environment.reset(__dirname, ['../../app/config']);
      environment.reset(__dirname, ['../../app/services/apm-models']);
      config.FEATURE_FLAGS.DISABLE_SOURCE_FILES = true;
      expect(FileImportSourceMap['cmmi-entity']).to.not.be.undefined;
    });
  });

  describe('#Models', () => {
    describe('Pac Models', () => {
      describe('pac-model', () => {
        it('should contain the correct models', () => {
          expect(Models['pac-model'].model).to.not.be.undefined;
          expect(Models['pac-model'].dynamic).to.not.be.undefined;
        });
      });

      describe('pac-subdivision', () => {
        it('should contain the correct models', () => {
          expect(Models['pac-subdivision'].model).to.not.be.undefined;
          expect(Models['pac-subdivision'].dynamic).to.not.be.undefined;
        });
      });

      describe('pac-entity', () => {
        it('should contain the correct models', () => {
          expect(Models['pac-entity'].model).to.not.be.undefined;
          expect(Models['pac-entity'].dynamic).to.not.be.undefined;
        });
      });

      describe('pac-provider', () => {
        it('should contain the correct models', () => {
          expect(Models['pac-provider'].model).to.not.be.undefined;
          expect(Models['pac-provider'].dynamic).to.not.be.undefined;
        });
      });

      describe('pac-qp_score', () => {
        it('should contain the correct models', () => {
          expect(Models['pac-qp_score'].model).to.not.be.undefined;
          expect(Models['pac-qp_score'].dynamic).to.not.be.undefined;
        });
      });

      describe('pac-qp_status', () => {
        it('should contain the correct models', () => {
          expect(Models['pac-qp_status'].model).to.not.be.undefined;
          expect(Models['pac-qp_status'].dynamic).to.not.be.undefined;
        });
      });

      describe('pac-lvt', () => {
        it('should contain the correct models', () => {
          expect(Models['pac-lvt'].model).to.not.be.undefined;
          expect(Models['pac-lvt'].dynamic).to.not.be.undefined;
        });
      });

      describe('abridged_qp_status', () => {
        it('should contain the correct models', () => {
          expect(Models.abridged_qp_status.model).to.not.be.undefined;
          expect(Models.abridged_qp_status.dynamic).to.not.be.undefined;
        });
      });

      describe('abridged_qp_score', () => {
        it('should contain the correct models', () => {
          expect(Models.abridged_qp_score.model).to.not.be.undefined;
          expect(Models.abridged_qp_score.dynamic).to.not.be.undefined;
        });
      });

      describe('abridged_lvt', () => {
        it('should contain the correct models', () => {
          expect(Models.abridged_lvt.model).to.not.be.undefined;
          expect(Models.abridged_lvt.dynamic).to.not.be.undefined;
        });
      });

      describe('beneficiary', () => {
        it('should contain the correct models', () => {
          expect(Models.beneficiary.model).to.not.be.undefined;
          expect(Models.beneficiary.dynamic).to.not.be.undefined;
        });
      });
    });

    describe('Source Files Models', () => {
      describe('apm-entity', () => {
        it('should contain the correct models and functions', () => {
          const startDate = new Date();
          const endDate = new Date();
          const data = {
            id: 'A1234',
            subdiv_id: '01',
            start_date: startDate,
            end_date: endDate,
            tin: '123456789',
            npi: '1234567890'
          };

          const keyFnData = Models['apm-entity'].keyFn(data);
          expect(Models['apm-entity'].merge).to.be.true;
          expect(keyFnData.id).to.equal('A1234');
          expect(keyFnData.subdiv_id).to.equal('01');
          expect(keyFnData.start_date).to.equal(startDate);
          expect(keyFnData.end_date).to.equal(endDate);
          expect(keyFnData.tin).to.equal('123456789');
          expect(keyFnData.npi).to.equal('1234567890');
          expect(Models['apm-entity'].model).to.not.be.undefined;
          expect(Models['apm-entity'].dynamic).to.not.be.undefined;
          expect(Models['apm-entity'].sourceFileData).to.not.be.undefined;
        });
      });

      describe('apm-provider', () => {
        it('should contain the correct models and functions', () => {
          const data = {
            entity_id: 'A1234',
            tin: '123456789',
            npi: '1234567890'
          };

          const keyFnData = Models['apm-provider'].keyFn(data);
          expect(keyFnData.entity_id).to.equal('A1234');
          expect(keyFnData.tin).to.equal('123456789');
          expect(keyFnData.npi).to.equal('1234567890');
          expect(Models['apm-provider'].merge).to.be.true;
          expect(Models['apm-provider'].model).to.not.be.undefined;
          expect(Models['apm-provider'].dynamic).to.not.be.undefined;
          expect(Models['apm-provider'].sourceFileData).to.not.be.undefined;
        });
      });

      describe('entity-eligibility', () => {
        it('should contain the correct models and functions', () => {
          const data = {
            entity_id: 'A1234'
          };

          const keyFnData = Models['entity-eligibility'].keyFn(data);
          expect(keyFnData.entity_id).to.equal('A1234');
          expect(Models['entity-eligibility'].merge).to.be.true;
          expect(Models['entity-eligibility'].model).to.not.be.undefined;
          expect(Models['entity-eligibility'].dynamic).to.not.be.undefined;
          expect(Models['entity-eligibility'].sourceFileData).to.not.be.undefined;
        });
      });

      describe('individual_qp_status', () => {
        it('should contain the correct models and functions', () => {
          const data = {
            npi: '1234567890'
          };

          const keyFnData = Models.individual_qp_status.keyFn(data);
          expect(keyFnData.npi).to.equal('1234567890');
          expect(Models.individual_qp_status.merge).to.be.true;
          expect(Models.individual_qp_status.model).to.not.be.undefined;
          expect(Models.individual_qp_status.dynamic).to.not.be.undefined;
          expect(Models.individual_qp_status.sourceFileData).to.not.be.undefined;
        });
      });

      describe('individual_qp_threshold', () => {
        it('should contain the correct models and functions', () => {
          const data = {
            npi: '1234567890'
          };

          const keyFnData = Models.individual_qp_threshold.keyFn(data);
          expect(keyFnData.npi).to.equal('1234567890');
          expect(Models.individual_qp_threshold.merge).to.be.true;
          expect(Models.individual_qp_threshold.model).to.not.be.undefined;
          expect(Models.individual_qp_threshold.dynamic).to.not.be.undefined;
          expect(Models.individual_qp_threshold.sourceFileData).to.not.be.undefined;
        });
      });

      describe('entity_qp_status', () => {
        it('should contain the correct models and functions', () => {
          const data = {
            npi: '1234567890'
          };

          const keyFnData = Models.entity_qp_status.keyFn(data);
          expect(keyFnData.npi).to.equal('1234567890');
          expect(Models.entity_qp_status.merge).to.be.true;
          expect(Models.entity_qp_status.model).to.not.be.undefined;
          expect(Models.entity_qp_status.dynamic).to.not.be.undefined;
          expect(Models.entity_qp_status.sourceFileData).to.not.be.undefined;
        });
      });

      describe('entity_qp_threshold', () => {
        it('should contain the correct models and functions', () => {
          const data = {
            apm_id: '01',
            subdivision_id: '02',
            entity_id: 'A1234',
            tin: '123456789',
            npi: '1234567890'
          };

          const keyFnData = Models.entity_qp_threshold.keyFn(data);
          expect(keyFnData.apm_id).to.equal('01');
          expect(keyFnData.subdivision_id).to.equal('02');
          expect(keyFnData.entity_id).to.equal('A1234');
          expect(keyFnData.tin).to.equal('123456789');
          expect(keyFnData.npi).to.equal('1234567890');
          expect(Models.entity_qp_threshold.merge).to.be.true;
          expect(Models.entity_qp_threshold.model).to.not.be.undefined;
          expect(Models.entity_qp_threshold.dynamic).to.not.be.undefined;
          expect(Models.entity_qp_threshold.sourceFileData).to.not.be.undefined;
        });
      });

      describe('beneficiary', () => {
        it('should contain the correct models and functions', () => {
          const data = {
            program_id: '12345',
            program_org_id: 'abc123',
            id: '001',
            hicn: '123456789',
            entity_id: 'A1234',
            link_key: '123456',
            mbi: '123456789'
          };

          const keyFnData = Models.beneficiary.keyFn(data);
          expect(keyFnData.program_id).to.equal('12345');
          expect(keyFnData.program_org_id).to.equal('abc123');
          expect(keyFnData.id).to.equal('001');
          expect(keyFnData.hicn).to.equal('123456789');
          expect(keyFnData.entity_id).to.equal('A1234');
          expect(keyFnData.link_key).to.equal('123456');
          expect(keyFnData.mbi).to.equal('123456789');
          expect(Models.beneficiary.merge).to.be.true;
          expect(Models.beneficiary.model).to.not.be.undefined;
          expect(Models.beneficiary.dynamic).to.not.be.undefined;
          expect(Models.beneficiary.sourceFileData).to.not.be.undefined;
        });
      });
    });
  });
});
