const chai = require('chai');
const snapshot = require('../../../app/services/report/snapshot');
const reports = require('../../../app/services/report');

const expect = chai.expect;
require('sinon-mongoose');

describe('Report Snapshot', () => {
  before(async () => { });

  after(async () => { });

  describe('All', () => {
    it('It should return an object', async () => {
      const result = await snapshot.all();
      expect(result).to.be.not.undefined;
    });
  });

  describe('By Year', () => {
    it('It should return an object', async () => {
      const result = await snapshot.byYear();
      expect(result).to.be.not.undefined;
    });
  });

  describe('getModels', () => {
    it('It should return an object', async () => {
      const result = await snapshot.getModels(2017, 'F');
      expect(result).to.be.not.undefined;
    });
  });

  describe('ec count report', () => {
    it('It should return an object', async () => {
      const snapshot2 = await reports.snapshot.getModels(2017, 'F');
      const result = await reports.ecCount.report(2017, 'F', new Date(), null, snapshot2);
      expect(result.apms.length).to.be.greaterThan(0);
    });
  });

  describe('ec count report', () => {
    it('It should return an object when no date', async () => {
      const snapshot2 = await reports.snapshot.getModels(2017, 'F');
      const result = await reports.ecCount.report(2017, 'F', null, null, snapshot2);
      expect(result.apms.length).to.be.greaterThan(0);
    });
  });

  describe('ec count report', () => {
    it('It should return an object when bypass cache', async () => {
      const snapshot2 = await reports.snapshot.getModels(2017, 'F');
      const result = await reports.ecCount.report(2017, 'F', null, 1, snapshot2);
      expect(result.apms.length).to.be.greaterThan(0);
    });
  });


  describe('qp metrics report', () => {
    it('It should return an object', async () => {
      const snapshot2 = await reports.snapshot.getModels(2017, 'F');
      const result = await reports.qpMetrics.report(2017, 'F', new Date(), null, snapshot2);
      expect(result.length).to.be.greaterThan(0);
    });
  });

  describe('empty results', () => {
    it('It should not crash', async () => {
      const snapshot2 = await reports.snapshot.getModels(2016, 'F');
      expect(snapshot2.error).to.not.be.undefined;
    });

    it('It should not crash', async () => {
      const models = await reports.snapshot.getModelByType([], 'test');
      expect(models).to.be.null;
    });
  });
});
