const sinon = require('sinon');
const chai = require('chai');
const environment = require('../environment');
const proxyquire = require('proxyquire');
const Promise = require('bluebird');
const LoggerMock = require('../mocks/services/logger-mock');
const MockProvider = require('../mocks/data/apm_provider');

const expect = chai.expect;

const mockSourceModel = {
  collection: {
    collectionName: 'sourceCollection',
    rename: (name, cb) => {
      cb();
    },
    drop: (cb) => {
      cb();
    }
  },
  find: () => mockSourceModel,
  lean: () => mockSourceModel,
  cursor: () => ({
    eachAsync: (cb) => {
      const data = {};
      return Promise.resolve(cb(data));
    }
  })
};

const mockBulk = {
  find: () => mockBulk,
  upsert: () => mockBulk,
  updateOne: () => mockBulk,
  execute: (cb) => {
    cb();
  }
};

const mockTargetModel = {
  collection: {
    collectionName: 'targetCollection',
    rename: (name, cb) => {
      cb();
    },
    initializeUnorderedBulkOp: () => mockBulk
  }
};

const listCollections = {
  toArray: () => Promise.resolve([])
};

const mongooseMock = {
  connection: {
    db: {
      listCollections: () => listCollections
    }
  }
};

const keyFn = data => data.id;

const publisher = proxyquire('../../app/services/publisher', {
  mongoose: mongooseMock,
  '../logger': LoggerMock()
});

describe('Service: publisher', () => {
  beforeEach(() => {
    environment.reset(__dirname, ['../../app/services/publisher']);
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    before(() => {
      sinon.stub(mockSourceModel.collection, 'rename').callThrough();
      sinon.stub(mockSourceModel.collection, 'drop').callThrough();
      sinon.stub(mockTargetModel.collection, 'rename').callThrough();
      sinon.stub(mockBulk, 'updateOne').callThrough();
      sinon.stub(mockBulk, 'execute').callThrough();
      sinon.stub(mockTargetModel.collection, 'initializeUnorderedBulkOp').callThrough();
      sinon
        .stub(listCollections, 'toArray')
        .callsFake(() => Promise.resolve([mockTargetModel.collection.collectionName]));
    });

    after(() => {
      mockSourceModel.collection.rename.restore();
      mockSourceModel.collection.drop.restore();
      mockTargetModel.collection.rename.restore();
      mockBulk.updateOne.restore();
      mockTargetModel.collection.initializeUnorderedBulkOp.restore();
      listCollections.toArray.restore();
    });

    afterEach(() => {
      mockSourceModel.collection.rename.resetHistory();
      mockTargetModel.collection.rename.resetHistory();
      mockTargetModel.collection.initializeUnorderedBulkOp.resetHistory();
      listCollections.toArray.resetHistory();
    });

    it('should rename collections correctly', () =>
      publisher.publish(mockSourceModel, mockTargetModel).then(() => {
        expect(mockSourceModel.collection.rename.calledOnce).to.equal(true);
        expect(mockTargetModel.collection.rename.calledOnce).to.equal(true);

        const sourceRenameArgs = mockSourceModel.collection.rename.getCall(0).args;
        const targetRenameArgs = mockTargetModel.collection.rename.getCall(0).args;

        expect(sourceRenameArgs[0]).to.equal(mockTargetModel.collection.collectionName);
        expect(targetRenameArgs[0]).to.match(
          new RegExp(`${mockTargetModel.collection.collectionName}_(\\d|-|T|:|Z)*`, 'g')
        );
      }));

    it('should merge collections correctly', () =>
      publisher.merge(null, mockSourceModel, mockTargetModel, keyFn).then(() => {
        expect(mockTargetModel.collection.initializeUnorderedBulkOp).to.have.been.called;
        expect(mockBulk.updateOne).to.have.been.called;
        expect(mockBulk.execute).to.have.been.called;
        // expect(mockSourceModel.collection.drop).to.have.been.called;
      }));

    describe('merging collections', () => {
      describe('with over 500 lines', () => {
        it('should execute once 500 lines are entered', () => {
          sinon.stub(mockSourceModel, 'cursor').callsFake(() => ({
            eachAsync: (cb) => {
              const data = Array(...new Array(501)).map(Number.prototype.valueOf, 0);
              return Promise.resolve(data.map(cb));
            }
          }));

          return publisher.merge(null, mockSourceModel, mockTargetModel, keyFn).then(() => {
            expect(mockTargetModel.collection.initializeUnorderedBulkOp).to.have.been.called;
            expect(mockBulk.updateOne).to.have.been.called;
            expect(mockBulk.execute).to.have.been.called;
            mockSourceModel.cursor.restore();
          });
        });

        describe('with errors', () => {
          before(() => {
            mockBulk.execute.restore();
            sinon.stub(mockSourceModel, 'cursor').callsFake(() => ({
              eachAsync: (cb) => {
                const data = Array(...new Array(501)).map(Number.prototype.valueOf, 0);
                return Promise.resolve(data.map(cb));
              }
            }));

            sinon.stub(mockBulk, 'execute').callsFake((cb) => {
              cb(new Error('fake error'), null);
            });
          });

          after(() => {
            mockSourceModel.cursor.restore();
            mockBulk.execute.restore();
            sinon.stub(mockBulk, 'execute').callThrough();
          });

          it('should handle error', () =>
            publisher.merge(null, mockSourceModel, mockTargetModel, keyFn).catch((err) => {
              expect(err.message).to.equal('fake error');
            }));
        });
      });

      describe('with provider participation data', () => {
        const mockSourceModelProviderData = {
          collection: {
            collectionName: 'source_file_provider_datas',
            rename: (name, cb) => {
              cb();
            },
            drop: (cb) => {
              cb();
            }
          },
          find: () => mockSourceModel,
          lean: () => mockSourceModel,
          cursor: () => ({
            eachAsync: (cb) => {
              const data = {};
              return Promise.resolve(cb(data));
            }
          })
        };

        it('should receive provider data and insert into database', () => {
          sinon.stub(mockSourceModelProviderData, 'cursor').callsFake(() => ({
            eachAsync: (cb) => {
              const data = Array(...new Array(1)).map(() => MockProvider());
              return Promise.resolve(data.map(cb));
            }
          }));

          return publisher.merge(null, mockSourceModelProviderData, mockTargetModel, keyFn).then(() => {
            expect(mockTargetModel.collection.initializeUnorderedBulkOp).to.have.been.called;
            expect(mockBulk.updateOne).to.have.been.called;
            expect(mockBulk.execute).to.have.been.called;
          });
        });
      });
    });
  });

  describe('without existing targetCollection', () => {
    before(() => {
      sinon.stub(mockSourceModel.collection, 'rename').callThrough();
      sinon.stub(mockTargetModel.collection, 'rename').callThrough();
      sinon.stub(listCollections, 'toArray').callsFake(() => Promise.resolve([]));
    });

    after(() => {
      mockSourceModel.collection.rename.restore();
      mockTargetModel.collection.rename.restore();
      listCollections.toArray.restore();
    });

    afterEach(() => {
      mockSourceModel.collection.rename.resetHistory();
      mockTargetModel.collection.rename.resetHistory();
      listCollections.toArray.resetHistory();
    });

    it('should rename collections correctly', () =>
      publisher.publish(mockSourceModel, mockTargetModel).then(() => {
        expect(mockSourceModel.collection.rename.calledOnce).to.equal(true);
        expect(mockTargetModel.collection.rename.calledOnce).to.equal(false);

        const sourceRenameArgs = mockSourceModel.collection.rename.getCall(0).args;

        expect(sourceRenameArgs[0]).to.equal(mockTargetModel.collection.collectionName);
      }));
  });
});
