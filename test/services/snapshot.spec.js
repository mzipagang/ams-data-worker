const ApmEntity = require('../../app/services/mongoose/models/apm_entity');
const sinon = require('sinon');
const chai = require('chai');
const Promise = require('bluebird');
require('sinon-mongoose');

const expect = chai.expect;

const snapshotService = require('../../app/services/snapshot');
const createMockEntity = require('../mocks/data/apm_entity');
const createMockProvider = require('../mocks/data/apm_provider');
const PacModel = require('../../app/services/mongoose/models/pac_model');

/* eslint-disable */
const createEntity = data => Object.assign({}, createMockEntity()._doc, data);
const createProvider = data => Object.assign({}, createMockProvider()._doc, data);
/* eslint-enable */

describe('Service: Snapshots', () => {
  describe('Status Filters', () => {
    let ApmEntityMock;
    let PacModelMock;
    let updateSpy;
    let updateExecuteSpy;
    let MongoosePacModel;
    let MongooseModel;

    const entity = {
      id: 'ABCD01',
      apm_id: '01',
      start_date: '2017-01-01',
      end_date: '2017-12-31'
    };

    beforeEach(() => {
      updateSpy = sinon.spy();
      updateExecuteSpy = sinon.spy();

      MongoosePacModel = data => ({
        find: () => ({
          lean: () => Promise.try(() => data)
        })
      });
      MongooseModel = data => ({
        collection: {
          initializeUnorderedBulkOp: () => ({
            find: () => ({
              updateOne: updateSpy
            }),
            execute: (cb) => {
              updateExecuteSpy();
              cb(null, []);
            }
          })
        },
        find: () => ({
          lean: () => ({
            cursor: () => ({
              eachAsync: cb => Promise.all(data.map(d => cb(d)))
            })
          })
        }),
        findOne: () => ({
          lean: () => Promise.try(() => data)
        })
      });

      ApmEntityMock = sinon.mock(ApmEntity);
      PacModelMock = sinon.mock(PacModel);
      PacModelMock.expects('find')
        .chain('lean')
        .resolves([{ id: '01' }]);
    });

    afterEach(() => {
      ApmEntityMock.restore();
      PacModelMock.restore();
    });

    describe('entityStatusFilter()', () => {
      describe('performance year validation', () => {
        it('should pass validating start and end date range within performance year', () => {
          const entitySnapshotCollection = MongooseModel([
            createEntity({
              id: 'ABCD01',
              apm_id: '01',
              start_date: '2017-01-01',
              end_date: '2017-12-31'
            })
          ]);

          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2017)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: false } });
            });
        });

        it('should pass validating start and end date range encompasses performance year', () => {
          const entitySnapshotCollection = MongooseModel([
            createEntity({
              id: 'ABCD01',
              apm_id: '01',
              start_date: '2017-01-01',
              end_date: '2019-12-31'
            })
          ]);

          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2018)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: false } });
            });
        });

        it('should pass if start date is before performance year but end date is within', () => {
          const entitySnapshotCollection = MongooseModel([
            createEntity({
              id: 'ABCD01',
              apm_id: '01',
              start_date: '2017-11-11',
              end_date: '2018-05-05'
            })
          ]);

          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2018)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: false } });
            });
        });

        it('should pass if start date is during performance year but end date is after', () => {
          const entitySnapshotCollection = MongooseModel([
            createEntity({
              id: 'ABCD01',
              apm_id: '01',
              start_date: '2018-10-31',
              end_date: '2019-10-31'
            })
          ]);

          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2018)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: false } });
            });
        });

        it('should fail validating if end date is before performance year', () => {
          const entitySnapshotCollection = MongooseModel([
            createEntity({
              id: 'ABCD01',
              apm_id: '01',
              start_date: '2016-01-01',
              end_date: '2016-12-31'
            })
          ]);

          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2017)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
            });
        });

        it('should fail validating if start date is after performance year', () => {
          const entitySnapshotCollection = MongooseModel([
            createEntity({
              id: 'ABCD01',
              apm_id: '01',
              start_date: '2019-01-01',
              end_date: '2020-12-31'
            })
          ]);

          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2018)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
            });
        });
      });

      it('should pass validating apm_id', () => {
        const entitySnapshotCollection = MongooseModel([
          createEntity({
            id: 'ABCD01',
            apm_id: '01',
            start_date: '2017-01-01',
            end_date: '2017-12-31'
          })
        ]);
        const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
        return snapshotService.entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2017).then(() => {
          expect(updateSpy).to.have.been.called;
          expect(updateSpy).to.have.calledWith({ $set: { removed: false } });
        });
      });

      it('should fail validating apm_id', () => {
        const entitySnapshotCollection = MongooseModel([
          createEntity({
            id: 'ABCD01',
            apm_id: '02',
            start_date: '2017-01-01',
            end_date: '2017-12-31'
          })
        ]);
        const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
        return snapshotService.entityStatusFilter(entitySnapshotCollection, modelSnapshotCollection, 2017).then(() => {
          expect(updateSpy).to.have.been.called;
          expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
        });
      });
    });

    describe('providerStatusFilter()', () => {
      it('should pass validating start and end date range', () => {
        const providerSnapshotCollection = MongooseModel([
          createProvider({
            entity_id: 'ABCD01',
            participation: [
              {
                start_date: '2017-01-01',
                end_date: '2017-12-31'
              }
            ]
          })
        ]);
        const entitySnapshotCollection = MongooseModel(entity);
        const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
        return snapshotService
          .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection, 2017)
          .then(() => {
            expect(updateSpy).to.have.been.called;
            expect(updateSpy).to.have.calledWith({ $set: { removed: false } });
          });
      });

      it('should fail validating start and end date range based on entity date range', () => {
        const providerSnapshotCollection = MongooseModel([
          createProvider({
            entity_id: 'ABCD01',
            participation: [
              {
                start_date: '2018-01-01',
                end_date: '2018-12-31'
              }
            ]
          })
        ]);
        const entitySnapshotCollection = MongooseModel(entity);
        const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
        return snapshotService
          .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection, 2018)
          .then(() => {
            expect(updateSpy).to.have.been.called;
            expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
          });
      });

      describe('performance year validation', () => {
        it('should fail if overlaps with entity date range, but provider end date is before performance year', () => {
          const badEntity = {
            id: 'ABCD01',
            apm_id: '01',
            start_date: '2017-03-03',
            end_date: '2019-02-02'
          };
          const providerSnapshotCollection = MongooseModel([
            createProvider({
              entity_id: 'ABCD01',
              participation: [
                {
                  start_date: '2017-03-03',
                  end_date: '2017-12-12'
                }
              ]
            })
          ]);
          const entitySnapshotCollection = MongooseModel(badEntity);
          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection, 2018)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
            });
        });

        it('should fail if overlaps with entity date range, but provider start date is after performance year', () => {
          const badEntity = {
            id: 'ABCD01',
            apm_id: '01',
            start_date: '2017-03-03',
            end_date: '2019-02-02'
          };
          const providerSnapshotCollection = MongooseModel([
            createProvider({
              entity_id: 'ABCD01',
              participation: [
                {
                  start_date: '2019-03-03',
                  end_date: '2019-12-12'
                }
              ]
            })
          ]);
          const entitySnapshotCollection = MongooseModel(badEntity);
          const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
          return snapshotService
            .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection, 2018)
            .then(() => {
              expect(updateSpy).to.have.been.called;
              expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
            });
        });
      });

      it('should pass validating apm_id', () => {
        const providerSnapshotCollection = MongooseModel([
          createProvider({
            entity_id: 'ABCD01',
            participation: [
              {
                start_date: '2017-01-01',
                end_date: '2017-12-31'
              }
            ]
          })
        ]);
        const entitySnapshotCollection = MongooseModel(entity);
        const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
        return snapshotService
          .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection, 2017)
          .then(() => {
            expect(updateSpy).to.have.been.called;
            expect(updateSpy).to.have.calledWith({ $set: { removed: false } });
          });
      });

      it('should fail validating apm_id', () => {
        const providerSnapshotCollection = MongooseModel([
          createProvider({
            entity_id: 'ABCD02',
            participation: [
              {
                start_date: '2018-01-01',
                end_date: '2018-12-31'
              }
            ]
          })
        ]);
        const entitySnapshotCollection = MongooseModel({
          id: 'ABCD02',
          apm_id: null,
          start_date: '2017-01-01',
          end_date: '2017-12-31'
        });
        const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
        return snapshotService
          .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection)
          .then(() => {
            expect(updateSpy).to.have.been.called;
            expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
          });
      });

      it('should remove provider when no entity is found', () => {
        const providerSnapshotCollection = MongooseModel([
          createProvider({
            entity_id: 'ABCD01',
            participation: [
              {
                start_date: '2017-01-01',
                end_date: '2017-12-31'
              }
            ]
          })
        ]);
        const entitySnapshotCollection = MongooseModel(null);
        const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
        return snapshotService
          .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection, 2017)
          .then(() => {
            expect(updateSpy).to.have.been.called;
            expect(updateSpy).to.have.calledWith({ $set: { removed: true } });
          });
      });
    });

    it('should update in batches of 200', () => {
      const providerSnapshotCollection = MongooseModel(
        Array(...Array(201)).map(() =>
          createProvider({
            entity_id: 'ABCD01',
            participation: [
              {
                start_date: '2017-01-01',
                end_date: '2017-12-31'
              }
            ]
          })
        )
      );
      const entitySnapshotCollection = MongooseModel(entity);
      const modelSnapshotCollection = MongoosePacModel([{ id: '01' }]);
      return snapshotService
        .providerStatusFilter(providerSnapshotCollection, entitySnapshotCollection, modelSnapshotCollection, 2017)
        .then(() => {
          expect(updateExecuteSpy).to.have.been.calledOnce;
        });
    });

    it('should set the secondary status when entity is next generation aco', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABCD02',
          participation: [
            {
              source: {
                source_type: 'ngaco-preferred-provider'
              },
              start_date: '2018-01-01',
              end_date: '2018-12-31'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABCD02',
        apm_id: '01',
        start_date: '2017-01-01',
        end_date: '2017-12-31'
      });
      const modelSnapshotCollection = MongoosePacModel([
        { id: '01', advanced_apm_flag: 'Y', name: 'Next Generation ACO Model' }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,

          '2018-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set the secondary status when model is advanced apm', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABCD02',
          participation: [
            {
              source: {
                source_type: 'ngaco-preferred-provider'
              },
              start_date: '2018-01-01',
              end_date: '2018-12-31'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABCD02',
        apm_id: '01',
        start_date: '2017-01-01',
        end_date: '2017-12-31'
      });
      const modelSnapshotCollection = MongoosePacModel([{ id: '01', advanced_apm_flag: 'Y', mips_apm_flag: 'Y' }]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2019',
          '2019-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set the secondary status when model is mips apm', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABCD02',
          participation: [
            {
              source: {
                source_type: 'ngaco-preferred-provider'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABCD02',
        apm_id: '01',
        start_date: '2017-01-01',
        end_date: '2017-03-10'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y',
          start_date: '2017-01-01',
          end_date: '2017-01-30'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set the secondary status when entity terminates before 3/31', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABCD02',
          participation: [
            {
              source: {
                source_type: 'ngaco-preferred-provider'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABCD02',
        apm_id: '01',
        start_date: '2016-12-31',
        end_date: '2017-03-10'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          mips_apm_flag: 'Y',
          start_date: '2017-01-01',
          end_date: '2017-01-30'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2017-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set the secondary status when entity starts after 8/31', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABCD02',
          participation: [
            {
              source: {
                source_type: 'ngaco-preferred-provider'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABCD02',
        apm_id: '01',
        start_date: '2017-09-01',
        end_date: '2017-09-30'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          mips_apm_flag: 'Y',
          start_date: '2017-01-01',
          end_date: '2017-01-30'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2017-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set status to primary by default', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABCD02',
          participation: [
            {
              source: {
                source_type: 'foo'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABCD02',
        apm_id: '01',
        start_date: '2017-09-01',
        end_date: '2017-09-30'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          mips_apm_flag: 'N',
          start_date: '2017-01-01',
          end_date: '2017-01-30'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2017-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'P' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set status to secondary when entity is not active on snapshot date and is advanced apm', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABC123',
          participation: [
            {
              source: {
                source_type: 'foo'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABC123',
        apm_id: '01',
        start_date: '2017-09-01',
        end_date: '2017-09-30'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          advanced_apm_flag: 'Y',
          start_date: '2017-01-01',
          end_date: '2017-01-30'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2019-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set status to secondary when entity is not active on snapshot date and is mips apm', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABC123',
          participation: [
            {
              source: {
                source_type: 'foo'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABC123',
        apm_id: '01',
        start_date: '2017-09-01',
        end_date: '2017-09-30'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          mips_apm_flag: 'Y',
          start_date: '2017-01-01',
          end_date: '2017-01-30'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2019-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set status to secondary when entity starts and ends during q1', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABC123',
          participation: [
            {
              source: {
                source_type: 'foo'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABC123',
        apm_id: '01',
        start_date: '2017-01-01',
        end_date: '2017-03-30'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          mips_apm_flag: 'P',
          advanced_apm_flag: 'P',
          start_date: '2017-01-01',
          end_date: '2017-12-31'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2017-01-10'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should set status to secondary when entity starts after end of q3', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABC123',
          participation: [
            {
              source: {
                source_type: 'foo'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel({
        id: 'ABC123',
        apm_id: '01',
        start_date: '2017-09-01',
        end_date: '2017-09-10'
      });
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          mips_apm_flag: 'P',
          advanced_apm_flag: 'P',
          start_date: '2017-01-01',
          end_date: '2017-12-31'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2017-09-03'
        )
        .then(() => {
          const callToUpdate = updateSpy.getCall(0).args[0];
          const expected = { $set: { relationship_code: 'S' } };
          expect(callToUpdate).to.eql(expected);
        });
    });

    it('should ignore if no matching entity', () => {
      const providerSnapshotCollection = MongooseModel([
        createProvider({
          entity_id: 'ABC123',
          participation: [
            {
              source: {
                source_type: 'foo'
              },
              start_date: '2018-01-02',
              end_date: '2018-12-30'
            }
          ]
        })
      ]);
      const entitySnapshotCollection = MongooseModel(null);
      const modelSnapshotCollection = MongoosePacModel([
        {
          id: '01',
          mips_apm_flag: 'P',
          advanced_apm_flag: 'P',
          start_date: '2017-01-01',
          end_date: '2017-12-31'
        }
      ]);

      return snapshotService
        .addRelationshipCode(
          providerSnapshotCollection,
          entitySnapshotCollection,
          modelSnapshotCollection,
          '2017',
          '2017-01-10'
        )
        .then(() => {
          expect(updateExecuteSpy).not.to.have.been.called;
        });
    });
  });
});
