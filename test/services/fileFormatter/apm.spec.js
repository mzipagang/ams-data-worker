const apmFormatter = require('../../../app/services/fileFormatter/apm');
const expect = require('chai').expect;

describe('Services: fileFormatter/apm', () => {
  it('should add invalid field to object', () => {
    const formattedResult = apmFormatter.format([{ value: { start_date: '', end_date: '' } }], { meta: '' });
    expect(formattedResult[0].invalid).to.not.be.undefined;
    expect(formattedResult[0].invalid).to.equal(false);
  });

  it('should add meta to object', () => {
    const formattedResult = apmFormatter.format([{ value: { start_date: '', end_date: '' } }], { meta: '' });
    expect(formattedResult[0].meta).to.not.be.undefined;
    expect(JSON.stringify(formattedResult[0].meta)).to.equal(JSON.stringify({ meta: '' }));
  });

  it('should format dates and return correctly', () => {
    const formattedResult =
      apmFormatter.format([{ value: { start_date: '2017-01-01', end_date: '2018-01-01' } }], { meta: '' });
    expect(formattedResult[0]).to.not.be.undefined;
  });

  it('should parse string expenditures_numerator into decimal', () => {
    const formattedResult =
      apmFormatter.format([{ value: { expenditures_numerator: '34242432.87' } }], { meta: '' });
    expect(formattedResult[0].expenditures_numerator).to.equal(34242432.87);
  });

  it('should handle undefined', () => {
    const formattedResult =
      apmFormatter.format([{ value: {} }], { meta: '' });
    expect(formattedResult[0].expenditures_numerator).to.be.undefined;
  });

  it('should handle null', () => {
    const formattedResult =
      apmFormatter.format([{ value: { expenditures_numerator: null } }], { meta: '' });
    expect(formattedResult[0].expenditures_numerator).to.equal(null);
  });

  it('should parse string expenditures_numerator into decimal', () => {
    const formattedResult =
      apmFormatter.format([{ value: { expenditures_numerator: '34242432.87' } }], { meta: '' });
    expect(formattedResult[0].expenditures_numerator).to.equal(34242432.87);
  });

  it('should parse string expenditures_denominator into decimal', () => {
    const formattedResult =
      apmFormatter.format([{ value: { expenditures_denominator: '34242432.87' } }], { meta: '' });
    expect(formattedResult[0].expenditures_denominator).to.equal(34242432.87);
  });

  it('should parse string beneficiaries_numerator into decimal', () => {
    const formattedResult =
      apmFormatter.format([{ value: { beneficiaries_numerator: '34242432.87' } }], { meta: '' });
    expect(formattedResult[0].beneficiaries_numerator).to.equal(34242432.87);
  });

  it('should parse string beneficiaries_denominator into decimal', () => {
    const formattedResult =
      apmFormatter.format([{ value: { beneficiaries_denominator: '34242432.87' } }], { meta: '' });
    expect(formattedResult[0].beneficiaries_denominator).to.equal(34242432.87);
  });

  it('should parse string patient_threshold_score into decimal', () => {
    const formattedResult =
      apmFormatter.format([{ value: { patient_threshold_score: '057' } }], { meta: '' });
    expect(formattedResult[0].patient_threshold_score).to.equal(57);
  });

  it('should parse string payment_threshold_score into decimal', () => {
    const formattedResult =
      apmFormatter.format([{ value: { payment_threshold_score: '058' } }], { meta: '' });
    expect(formattedResult[0].payment_threshold_score).to.equal(58);
  });

  it('should handle undefined', () => {
    const formattedResult =
      apmFormatter.format([{ value: {} }], { meta: '' });
    expect(formattedResult[0].expenditures_numerator).to.be.undefined;
  });

  it('should handle null', () => {
    const formattedResult =
      apmFormatter.format([{ value: { expenditures_numerator: null } }], { meta: '' });
    expect(formattedResult[0].expenditures_numerator).to.equal(null);
  });
});
