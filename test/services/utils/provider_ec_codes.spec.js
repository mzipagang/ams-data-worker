const { expect } = require('chai');
const ECCodes = require('../../../app/services/utils/provider_ec_codes.js');
const providerCodesModel = require('../../../app/services/mongoose/models/provider_specialty_code.js');
const ecStatusDynamicModel = require('../../../app/services/mongoose/dynamic_name_models/ec_status.js');

const apmProviderDynamicModel = require('../../../app/services/mongoose/dynamic_name_models/apm_provider.js');
const SpecialityCode = require('../../../app/services/mongoose/models/specialty_code');

let model = null;
let apmProviderModel = null;

describe('Process EC Codes for providers', () => {
  before(async () => {
    model = ecStatusDynamicModel('testing');
    await model.createIndexes();

    apmProviderModel = apmProviderDynamicModel('testing');
    await apmProviderModel.createIndexes();

    await apmProviderModel.insertMany([
      {
        entity_id: 'NRA',
        npi: '1',
        tin: '6003d71c06c7e1326e3e109359de6b56',
        participation: [
          {
            source: {
              source_type: 'cmmi-provider',
              file_id: 'ab3051f6-0984-4623-a978-206c0215842c'
            },
            start_date: new Date(),
            end_date: new Date(),
            ccn: '',
            file_id: 'ab3051f6-0984-4623-a978-206c0215842c',
            validations: [],
            tin_type_code: null,
            specialty_code: null,
            organization_name: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            participant_type_code: null
          }
        ]
      },
      {
        entity_id: 'NRA',
        npi: '2',
        tin: '6003d71c06c7e1326e3e109359de6b56',
        participation: [
          {
            source: {
              source_type: 'cmmi-provider',
              file_id: 'ab3051f6-0984-4623-a978-206c0215842c'
            },
            start_date: new Date(),
            end_date: new Date(),
            ccn: '',
            file_id: 'ab3051f6-0984-4623-a978-206c0215842c',
            validations: [],
            tin_type_code: null,
            specialty_code: null,
            organization_name: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            participant_type_code: null
          }
        ]
      },
      {
        entity_id: 'NRA',
        npi: '3',
        tin: '6003d71c06c7e1326e3e109359de6b56',
        participation: [
          {
            source: {
              source_type: 'cmmi-provider',
              file_id: 'ab3051f6-0984-4623-a978-206c0215842c'
            },
            start_date: new Date(),
            end_date: new Date(),
            ccn: '',
            file_id: 'ab3051f6-0984-4623-a978-206c0215842c',
            validations: [],
            tin_type_code: null,
            specialty_code: null,
            organization_name: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            participant_type_code: null
          }
        ]
      },
      {
        entity_id: 'NRA',
        npi: '4',
        tin: '6003d71c06c7e1326e3e109359de6b56',
        participation: [
          {
            source: {
              source_type: 'cmmi-provider',
              file_id: 'ab3051f6-0984-4623-a978-206c0215842c'
            },
            start_date: new Date(),
            end_date: new Date(),
            ccn: '',
            file_id: 'ab3051f6-0984-4623-a978-206c0215842c',
            validations: [],
            tin_type_code: null,
            specialty_code: null,
            organization_name: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            participant_type_code: null
          }
        ]
      },
      {
        entity_id: 'NRA',
        npi: '5',
        tin: '6003d71c06c7e1326e3e109359de6b56',
        participation: [
          {
            source: {
              source_type: 'cmmi-provider',
              file_id: 'ab3051f6-0984-4623-a978-206c0215842c'
            },
            start_date: new Date(),
            end_date: new Date(),
            ccn: '',
            file_id: 'ab3051f6-0984-4623-a978-206c0215842c',
            validations: [],
            tin_type_code: null,
            specialty_code: null,
            organization_name: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            participant_type_code: null
          }
        ]
      },
      {
        entity_id: 'NRA',
        npi: '6',
        tin: '6003d71c06c7e1326e3e109359de6b56',
        participation: [
          {
            source: {
              source_type: 'cmmi-provider',
              file_id: 'ab3051f6-0984-4623-a978-206c0215842c'
            },
            start_date: new Date(),
            end_date: new Date(),
            ccn: '',
            file_id: 'ab3051f6-0984-4623-a978-206c0215842c',
            validations: [],
            tin_type_code: null,
            specialty_code: null,
            organization_name: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            participant_type_code: null
          }
        ]
      }
    ]);

    await providerCodesModel.insertMany([
      {
        npi: 1,
        providerTypeCode: 'BP',
        specialtyCodes: [],
        deleted: false
      },
      {
        npi: 2,
        providerTypeCode: 'BP',
        specialtyCodes: ['01a'],
        deleted: false
      },
      {
        npi: 3,
        providerTypeCode: 'A',
        specialtyCodes: ['01a'],
        deleted: false
      },
      {
        npi: 4,
        providerTypeCode: 'BP',
        specialtyCodes: ['01a', '02a'],
        deleted: false
      },
      {
        npi: 5,
        providerTypeCode: 'BP',
        specialtyCodes: ['01a', '02a'],
        deleted: true
      }
    ]);

    // insert our own so we dont' have to run migrations.
    await SpecialityCode.insertMany([
      {
        code: '01a',
        description: 'General Practice',
        eligible_clinician: true,
        mips_eligibleClinician: true,
        comment: null,
        year: 2017
      },
      {
        code: '02a',
        description: 'General Surgery',
        eligible_clinician: true,
        mips_eligibleClinician: true,
        comment: null,
        year: 2017
      },
      {
        code: '03a',
        description: 'Allergy/Immunology',
        eligible_clinician: true,
        mips_eligibleClinician: true,
        comment: null,
        year: 2017
      },
      {
        code: '04a',
        description: 'Otolaryngology',
        eligible_clinician: true,
        mips_eligibleClinician: true,
        comment: null,
        year: 2017
      }
    ]);


    await ECCodes(2017, apmProviderModel, model.collection.name);
  });

  it('Should have a list of providers', async () => {
    const providers = await model.find({});
    expect(providers.length).to.be.greaterThan(0);
  });

  it('npi 1', async () => {
    const provider = await model.findOne({ npi: '1' });
    expect(provider.mipsEligibleClinician).to.be.equal(false);
    expect(provider.eligibleClinician).to.be.equal(false);
    expect(provider.isIndividual).to.be.equal(true);
    expect(provider.isNotFound).to.be.equal(false);
    expect(provider.codes.length).to.be.equal(0);
    expect(provider.isMissingSpecialityCode).to.be.equal(true);
  });

  it('npi 2 ', async () => {
    const provider = await model.findOne({ npi: '2' });
    expect(provider.mipsEligibleClinician).to.be.equal(true);
    expect(provider.eligibleClinician).to.be.equal(true);
    expect(provider.isIndividual).to.be.equal(true);
    expect(provider.isNotFound).to.be.equal(false);
    expect(provider.codes.length).to.be.equal(1);
    expect(provider.isMissingSpecialityCode).to.be.equal(false);
  });

  it('npi 3 ', async () => {
    const provider = await model.findOne({ npi: '3' });
    expect(provider.mipsEligibleClinician).to.be.equal(false);
    expect(provider.eligibleClinician).to.be.equal(false);
    expect(provider.isIndividual).to.be.equal(false);
    expect(provider.isNotFound).to.be.equal(false);
    expect(provider.codes.length).to.be.equal(1);
    expect(provider.isMissingSpecialityCode).to.be.equal(false);
  });

  it('npi 4 ', async () => {
    const provider = await model.findOne({ npi: '4' });
    expect(provider.mipsEligibleClinician).to.be.equal(true);
    expect(provider.eligibleClinician).to.be.equal(true);
    expect(provider.isIndividual).to.be.equal(true);
    expect(provider.isNotFound).to.be.equal(false);
    expect(provider.codes.length).to.be.equal(2);
    expect(provider.isMissingSpecialityCode).to.be.equal(false);
  });

  // npi that is in upload file, and idr, but is marked as deleted.
  it('npi 5', async () => {
    const provider = await model.findOne({ npi: '5' });
    expect(provider.mipsEligibleClinician).to.be.equal(false);
    expect(provider.eligibleClinician).to.be.equal(false);
    expect(provider.isIndividual).to.be.equal(false);
    expect(provider.isNotFound).to.be.equal(true);
    expect(provider.codes.length).to.be.equal(0);
    expect(provider.isMissingSpecialityCode).to.be.equal(true);
  });

  // npi that is in upload file, but not in our idr list.
  it('npi 6', async () => {
    const provider = await model.findOne({ npi: '6' });
    expect(provider.mipsEligibleClinician).to.be.equal(false);
    expect(provider.eligibleClinician).to.be.equal(false);
    expect(provider.isIndividual).to.be.equal(false);
    expect(provider.isNotFound).to.be.equal(true);
    expect(provider.codes.length).to.be.equal(0);
    expect(provider.isMissingSpecialityCode).to.be.equal(true);
  });

  after(async () => {
    await providerCodesModel.deleteMany({});
    await SpecialityCode.deleteMany({});
  });
});
