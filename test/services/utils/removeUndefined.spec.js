const { expect } = require('chai');
const removeUndefined = require('../../../app/services/utils/removeUndefined');

describe('removeUndefined utility', () => {
  it('should remove undefined properties from an object', () => {
    const testObject = {
      foo: '',
      count: 0,
      bar: false,
      zzz: undefined,
      alpha: null
    };

    const expected = {
      foo: '',
      count: 0,
      bar: false,
      alpha: null
    };

    const actual = removeUndefined(testObject);

    expect(actual).to.eql(expected);
  });
});
