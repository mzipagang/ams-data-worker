const toChunks = require('../../../app/services/utils/toChunks');
const { expect } = require('chai');

describe('toChunks', () => {
  it('should split to chunks', () => {
    const array = [1, 2, 3, 4, 5];
    const pairs = toChunks(array, 2);
    const triples = toChunks(array, 3);
    expect(pairs.length).to.equal(3);
    expect(pairs[0][0]).to.equal(1);
    expect(pairs[1][0]).to.equal(3);
    expect(pairs[2][0]).to.equal(5);
    expect(triples.length).to.equal(2);
    expect(triples[0][0]).to.equal(1);
    expect(triples[1][0]).to.equal(4);
  });
});
