const { expect } = require('chai');
const {
  participatingInApm,
  isFullAdvancedApm,
  isPartialAdvancedApm,
  isFullMipsApm,
  isPartialMipsApm,
  isNextGenerationAco,
  isFullOrPartialAdvancedApm,
  isFullOrPartialMipsApm
} = require('../../../app/services/utils/model');

describe('AMS Model Utilities', () => {
  const pacModels = [
    { id: '1' },
    { id: '22' },
    { id: '50' }
  ];

  const advancedApm = { advanced_apm_flag: 'Y' };
  const partialAdvancedApm = { advanced_apm_flag: 'P' };
  const mipsApm = { mips_apm_flag: 'Y' };
  const partialMipsApm = { mips_apm_flag: 'P' };
  const ngaco = { name: 'Next Generation ACO Model' };

  it('should determine if a given AMS object is participating in an APM based on ID', () => {
    expect(participatingInApm('1', pacModels)).to.equal(true);
    expect(participatingInApm('22', pacModels)).to.equal(true);
    expect(participatingInApm('99', pacModels)).to.equal(false);
  });

  it('should determine if the model is an Advanced APM', () => {
    expect(isFullAdvancedApm(advancedApm)).to.equal(true);
    expect(isFullAdvancedApm(mipsApm)).to.equal(false);
  });

  it('should determine if the mode; is a partial Advanced APM', () => {
    expect(isPartialAdvancedApm(partialAdvancedApm)).to.equal(true);
    expect(isPartialAdvancedApm(partialMipsApm)).to.equal(false);
  });

  it('should determine if the model is a MIPS APM', () => {
    expect(isFullMipsApm(advancedApm)).to.equal(false);
    expect(isFullMipsApm(mipsApm)).to.equal(true);
  });

  it('should determine if the mode is a partial MIPS APM', () => {
    expect(isPartialMipsApm(partialMipsApm)).to.equal(true);
    expect(isPartialMipsApm(partialAdvancedApm)).to.equal(false);
  });

  it('should determine if the given model is an Next Generation ACO', () => {
    expect(isNextGenerationAco(ngaco)).to.equal(true);
  });

  it('should determine if the given model is fully or partially an Advanced APM', () => {
    expect(isFullOrPartialAdvancedApm(advancedApm)).to.equal(true);
    expect(isFullOrPartialAdvancedApm(partialAdvancedApm)).to.equal(true);
    expect(isFullOrPartialAdvancedApm(partialMipsApm)).to.equal(false);
  });

  it('should determine if the given model is fully or partially an MIPS APM', () => {
    expect(isFullOrPartialMipsApm(mipsApm)).to.equal(true);
    expect(isFullOrPartialMipsApm(partialMipsApm)).to.equal(true);
    expect(isFullOrPartialMipsApm(advancedApm)).to.equal(false);
  });
});
