const { expect } = require('chai');
const {
  startsAfter,
  terminatesBefore,
  activeOn
} = require('../../../app/services/utils/activityDate');

describe('Activity Date Utilities', () => {
  const activityObject = {
    start_date: '2020-06-01',
    end_date: '2020-06-30'
  };

  it('should not be active if the given date is before the start date of the object', () => {
    const expected = false;
    const actual = activeOn(activityObject, '2020-01-01');
    expect(actual).to.equal(expected);
  });

  it('should not be active if the given date is after the start date of the object', () => {
    const expected = false;
    const actual = activeOn(activityObject, '2020-01-12');
    expect(actual).to.equal(expected);
  });

  it('should be active if the given date is between the start and end date of the object', () => {
    const expected = true;
    const actual = activeOn(activityObject, '2020-06-10');
    expect(actual).to.equal(expected);
  });

  it('should be active if the given date is on the start date of the object', () => {
    const expected = true;
    const actual = activeOn(activityObject, '2020-06-01');
    expect(actual).to.equal(expected);
  });

  it('should be active if the given date is on the end date of the object', () => {
    const expected = true;
    const actual = activeOn(activityObject, '2020-06-30');
    expect(actual).to.equal(expected);
  });

  it('should determine if the given obj starts after the given date', () => {
    expect(startsAfter(activityObject, '2020-01-01')).to.equal(true);
    expect(startsAfter(activityObject, '2020-06-01')).to.equal(false);
    expect(startsAfter(activityObject, '2020-08-01')).to.equal(false);
  });

  it('should determine if the given obj terminates before the given date', () => {
    expect(terminatesBefore(activityObject, '2020-12-31')).to.equal(true);
    expect(terminatesBefore(activityObject, '2020-06-01')).to.equal(false);
    expect(terminatesBefore(activityObject, '2020-06-30')).to.equal(false);
  });
});
