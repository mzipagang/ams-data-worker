const { expect } = require('chai');
const {
  sourcedFrom,
  sourcedFromNgacoPreferredProvider,
  sourcedFromMdmProvider
} = require('../../../app/services/utils/provider');

describe('Provider Utilities', () => {
  it('should list where the provider was sourced from', () => {
    const provider = {
      participation: [
        { source: { source_type: 'foo' } },
        { source: { source_type: 'bar' } }
      ]
    };

    const expected = ['foo', 'bar'];
    const actual = sourcedFrom(provider);

    expect(actual).to.eql(expected);
  });

  it('should determine if the provider was sourced from NGACO Preferred Provider file', () => {
    const provider = {
      participation: [
        { source: { source_type: 'ngaco-preferred-provider' } }
      ]
    };

    const expected = true;
    const actual = sourcedFromNgacoPreferredProvider(provider);

    expect(actual).to.equal(expected);
  });

  it('should determine if the provider was sourced from MDM Provider file', () => {
    const provider = {
      participation: [
        { source: { source_type: 'mdm-provider' } }
      ]
    };

    const expected = true;
    const actual = sourcedFromMdmProvider(provider);

    expect(actual).to.equal(expected);
  });
});
