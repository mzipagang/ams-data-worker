const { expect } = require('chai');
const sinon = require('sinon');
const jwt = require('jsonwebtoken');

const config = require('../../../app/config');
const jwtUtil = require('../../../app/services/utils/jwt');

describe('JWT Util', () => {
  it('should not return token data if no request', () => {
    const expected = null;
    const actual = jwtUtil.getTokenData();

    expect(actual).to.equal(expected);
  });

  it('should not return token data if no headers', () => {
    const expected = null;
    const actual = jwtUtil.getTokenData({ headers: undefined });

    expect(actual).to.equal(expected);
  });

  it('should not return token data if no authorization header', () => {
    const expected = null;
    const actual = jwtUtil.getTokenData({ headers: { authorization: undefined } });

    expect(actual).to.equal(expected);
  });

  it('should throw for malformed jwt', () => {
    const wrapper = () => jwtUtil.getTokenData({ headers: { authorization: 'Bearer foobar' } });

    expect(wrapper).to.throw();
  });

  it('should return token data given a valid token', () => {
    const userObject = { name: 'foo' };
    const token = jwt.sign(userObject, config.JWT.SECRET);
    const actual = jwtUtil.getTokenData({ headers: { authorization: `Bearer ${token}` } });

    expect(actual).to.include(userObject);
  });

  it('should throw given an invalid token', () => {
    const userObject = { username: 'foo' };
    const token = jwt.sign(userObject, `${config.JWT.SECRET}INVALID`);
    const wrapper = () => jwtUtil.getTokenData({ headers: { authorization: `Bearer ${token}` } });

    expect(wrapper).to.throw();
  });

  it('should return the username', () => {
    const username = 'foo';
    const userObject = { username };
    const token = jwt.sign(userObject, `${config.JWT.SECRET}`);
    const expected = username;
    const actual = jwtUtil.getUsername({ headers: { authorization: `Bearer ${token}` } });

    expect(actual).to.equal(expected);
  });

  it('should not require authentication for certain routes', () => {
    const expected = false;
    const actual = jwtUtil.requiresAuthentication('GET', '/api/v1/health_check');

    expect(actual).to.equal(expected);
  });

  it('should require authentication for all routes not explicitly excluded', () => {
    const expected = true;
    const actual = jwtUtil.requiresAuthentication('GET', '/api/v1/any_other_route');

    expect(actual).to.equal(expected);
  });
});
