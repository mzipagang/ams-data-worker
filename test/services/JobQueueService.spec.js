const JobQueueService = require('../../app/services/JobQueueService');
const QueuedJobs = require('../../app/services/mongoose/models/queued_jobs');
const { expect } = require('chai');

describe('Service: JobQueueService', () => {
  beforeEach(async () => {
    await QueuedJobs.insertMany([
      {
        id: '10',
        type: 'entity',
        status: 'complete-success',
        parameters: {
          year: 2018
        },
        results: {
          signedUrlConfirmationId: '01'
        },
        details: {
          submittedOn: new Date(),
          lastUpdatedOn: new Date(),
          sqsJobRequestId: '12345',
          s3Bucket: 's3_bucket_1',
          s3FileName: 's3_file_name'
        }
      },
      {
        id: '11',
        type: 'entity',
        status: 'error',
        parameters: {
          year: 2017
        },
        resultsError: {
          code: '01',
          message: 'failed to upload'
        },
        details: {
          submittedOn: new Date(),
          lastUpdatedOn: new Date(),
          sqsJobRequestId: '12345',
          s3Bucket: 's3_bucket_1',
          s3FileName: 's3_file_name'
        }
      }
    ]);
  });

  afterEach(async () => {
    await QueuedJobs.remove();
  });

  describe('#getJobById', () => {
    it('should query mongo for the queued job status', async () => {
      const queuedJob = await JobQueueService.getJobById('10');
      expect(queuedJob.id).to.equal('10');
      expect(queuedJob.type).to.equal('entity');
      expect(queuedJob.status).to.equal('complete-success');
      expect(queuedJob.parameters.year).to.equal(2018);
      expect(queuedJob.results.signedUrlConfirmationId).to.equal('01');
      expect(queuedJob.details.submittedOn).to.be.a('Date');
      expect(queuedJob.details.lastUpdatedOn).to.be.a('Date');
      expect(queuedJob.details.sqsJobRequestId).to.equal('12345');
      expect(queuedJob.details.s3Bucket).to.equal('s3_bucket_1');
      expect(queuedJob.details.s3FileName).to.equal('s3_file_name');
    });

    it('should query mongo for queued job with error status', async () => {
      const queuedJob = await JobQueueService.getJobById('11');
      expect(queuedJob.id).to.equal('11');
      expect(queuedJob.type).to.equal('entity');
      expect(queuedJob.status).to.equal('error');
      expect(queuedJob.parameters.year).to.equal(2017);
      expect(queuedJob.resultsError.code).to.equal('01');
      expect(queuedJob.resultsError.message).to.equal('failed to upload');
      expect(queuedJob.details.submittedOn).to.be.a('Date');
      expect(queuedJob.details.lastUpdatedOn).to.be.a('Date');
      expect(queuedJob.details.sqsJobRequestId).to.equal('12345');
      expect(queuedJob.details.s3Bucket).to.equal('s3_bucket_1');
      expect(queuedJob.details.s3FileName).to.equal('s3_file_name');
    });

    it('should return null for an invalid job id', async () => {
      const queuedJob = await JobQueueService.getJobById('test');
      expect(queuedJob).to.be.null;
    });
  });

  describe('#createJob', () => {
    it('should create a job entry in the database', async () => {
      const job = await JobQueueService.createJob('entity', { year: 2018 }, 'test-username');
      const queuedJob = await QueuedJobs.findOne({ id: job.id }).lean();
      expect(queuedJob).to.not.be.undefined;
      expect(queuedJob.id).to.equal(job.id);
      expect(queuedJob.parameters.year).to.equal(2018);
      expect(queuedJob.status).to.equal('queued');
      expect(queuedJob.details.updatedBy).to.equal('test-username');
      expect(queuedJob.type).to.equal('entity');
    });

    it('should create a job entry with no parameters in the database', async () => {
      const job = await JobQueueService.createJob('entity');
      const queuedJob = await QueuedJobs.findOne({ id: job.id }).lean();
      expect(queuedJob).to.not.be.undefined;
      expect(queuedJob.id).to.equal(job.id);
      expect(queuedJob.parameters).to.be.undefined;
      expect(queuedJob.type).to.equal('entity');
      expect(queuedJob.status).to.equal('queued');
    });

    it('should return null if a job is attempted to be created without a type', async () => {
      const jobUndefined = await JobQueueService.createJob();
      const jobEmptyString = await JobQueueService.createJob('');
      expect(jobUndefined).to.be.null;
      expect(jobEmptyString).to.be.null;
    });
  });

  describe('#shapeJobStatusResponse', () => {
    it('should not return details for any response', () => {
      const jobStatus = {
        id: '123',
        type: 'qpp-entities',
        status: 'queued',
        parameters: {
          year: '2018'
        },
        results: {
          signedUrlConfirmationId: '0000000000'
        },
        details: {
          submittedOn: '2012-04-23T18:25:43.511Z',
          lastUpdatedOn: '2012-04-23T18:25:43.511Z',
          sqsJobId: '1234567890',
          s3Bucket: 'ams-batch-data',
          s3File: '2012-04-23T18:25:43.511Z'
        }
      };
      const jobStatusResponse = JobQueueService.shapeJobStatusResponse(jobStatus);
      expect(jobStatusResponse.details).to.be.undefined;
    });

    it('should return resultsError if status is "complete-error"', () => {
      const jobStatus = {
        id: '123',
        type: 'qpp-entities',
        status: 'complete-error',
        parameters: {
          year: '2018'
        },
        results: {
          signedUrlConfirmationId: '0000000000'
        },
        resultsError: {
          code: 1234,
          message: 'job already submitted and being processed'
        },
        details: {
          submittedOn: '2012-04-23T18:25:43.511Z',
          lastUpdatedOn: '2012-04-23T18:25:43.511Z',
          sqsJobId: '1234567890',
          s3Bucket: 'ams-batch-data',
          s3File: '2012-04-23T18:25:43.511Z'
        }
      };
      const jobStatusResponse = JobQueueService.shapeJobStatusResponse(jobStatus);
      expect(jobStatusResponse.details).to.be.undefined;
      expect(jobStatusResponse.resultsError).to.not.be.undefined;
    });

    it('should return with results if status is "complete-success"', () => {
      const jobStatus = {
        id: '123',
        type: 'qpp-entities',
        status: 'complete-success',
        parameters: {
          year: '2018'
        },
        results: {
          signedUrlConfirmationId: '0000000000'
        },
        details: {
          submittedOn: '2012-04-23T18:25:43.511Z',
          lastUpdatedOn: '2012-04-23T18:25:43.511Z',
          sqsJobId: '1234567890',
          s3Bucket: 'ams-batch-data',
          s3File: '2012-04-23T18:25:43.511Z'
        }
      };
      const jobStatusResponse = JobQueueService.shapeJobStatusResponse(jobStatus);
      expect(jobStatusResponse.details).to.be.undefined;
      expect(jobStatusResponse.results).to.not.be.undefined;
      expect(jobStatusResponse.results.signedUrlConfirmationId).to.equal('0000000000');
    });
  });

  describe('lockNextQueuedJob', () => {
    let runningJob;

    beforeEach(async () => {
      await JobQueueService.createJob('test-type', { name: 'Q1' });
      await JobQueueService.createJob('test-type', { name: 'Q2' });
      runningJob = await JobQueueService.createJob('test-type', { name: 'R1' });
      runningJob.status = 'running';
      await runningJob.save();
    });

    it('should find the first queued job, update its status to "running" and return that job', async () => {
      let result = await JobQueueService.lockNextQueuedJob();
      expect(result).to.exist;
      expect(result.status).to.equal('running');
      expect(result.parameters.name).to.equal('Q1');

      result = await JobQueueService.lockNextQueuedJob();
      expect(result).to.exist;
      expect(result.status).to.equal('running');
      expect(result.parameters.name).to.equal('Q2');

      result = await JobQueueService.lockNextQueuedJob();
      expect(result).to.be.null;
    });
  });

  describe('#updateJobStatus', () => {
    before(async () => {
      await new QueuedJobs({
        id: '11',
        type: 'entity',
        status: 'queued'
      }).save();
    });

    it('should change the status of a given job', async () => {
      const queuedJob = await JobQueueService.updateJobStatus('11', 'test');
      expect(queuedJob.status).to.equal('test');
    });
  });

  describe('#updateJobStatusToCompleted', () => {
    before(async () => {
      await new QueuedJobs({
        id: '13',
        type: 'entity',
        status: 'running'
      }).save();
    });

    it('should change the status of a job to complete-success', async () => {
      const queuedJob = await JobQueueService.updateJobStatusToCompleted('13');
      expect(queuedJob.status).to.equal('complete-success');
    });
  });

  describe('#updateJobError', () => {
    before(async () => {
      await new QueuedJobs({
        id: '14',
        type: 'entity',
        status: 'running'
      }).save();
    });

    it('should add update the job with an error object', async () => {
      const error = { code: '01', message: 'this is an error' };
      await JobQueueService.updateJobError('14', error);
      const queuedJobWithError = await JobQueueService.getJobById('14');
      expect(queuedJobWithError.status).to.equal('complete-error');
      expect(queuedJobWithError.resultsError.code).to.equal(error.code);
      expect(queuedJobWithError.resultsError.message).to.equal(error.message);
    });
  });
});
