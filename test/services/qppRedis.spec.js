const proxyquire = require('proxyquire');
const Promise = require('bluebird');

const requireQppRedis = () =>
  proxyquire('../../app/services/qppRedis', {
    '../redis': {
      getClient: () => ({
        getAsync: () => Promise.resolve(JSON.stringify({})),
        setAsync: () => Promise.resolve()
      })
    }
  });
const sinon = require('sinon');
const redis = require('../../app/services/redis');
const chai = require('chai');

const expect = chai.expect;

describe('Service: qppRedis', () => {
  let redisMock;

  beforeEach(() => {
    redisMock = sinon.mock(redis);
    redisMock
      .expects('getClient')
      .returns({ getAsync: () => Promise.resolve(JSON.stringify({})), setAsync: () => Promise.resolve() });
  });

  afterEach(() => {
    redisMock.restore();
  });

  it('it should load', () => {
    const qppRedis = requireQppRedis();
    expect(qppRedis).to.not.be.undefined;
  });

  it('getQppCache() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis();
  });

  it('getByEntityId() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.getByEntityId();
  });

  it('setByEntityId() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.setByEntityId();
  });

  it('getByNpi() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.getByNpi();
  });

  it('setByNpi() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.setByNpi();
  });

  it('getByTin() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.getByTin();
  });

  it('setByTin() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.setByTin();
  });

  it('getEntityList() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.getEntityList();
  });

  it('setEntityList() should execute', () => {
    const qppRedis = requireQppRedis();
    return qppRedis.setEntityList();
  });
});
