const environment = require('../environment');
const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

const expect = chai.expect;

const MockLogger = require('../mocks/services/logger-mock');

describe('Service: Encrypt-Decrypt', () => {
  const mocks = {};

  const requireEncryptDecrypt = (overrideMocks = {}) =>
    proxyquire(
      '../../app/services/encrypt-decrypt',
      Object.assign(
        {
          '../logger': mocks.logger
        },
        overrideMocks
      )
    );

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/config']);
      environment.use('dev');
      mocks.logger = MockLogger();
    });

    it('should properly load', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      expect(encryptDecrypt).to.not.be.undefined;
      expect(encryptDecrypt.encrypt).to.not.be.undefined;
      expect(encryptDecrypt.decrypt).to.not.be.undefined;
      expect(typeof encryptDecrypt.encrypt).to.equal('function');
      expect(typeof encryptDecrypt.decrypt).to.equal('function');
    });

    it('should encrypt a given text', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'test text';
      const result = encryptDecrypt.encrypt(inputText);
      expect(result).to.not.be.undefined;
      expect(result).to.not.equal(inputText);
    });

    it('should output same results when same input text is encrypted', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'test text';
      const encryptedText = encryptDecrypt.encrypt(inputText);
      const resultArr = [];

      for (let i = 0; i < 20; i += 1) {
        resultArr.push(encryptDecrypt.encrypt(inputText));
      }

      const result = resultArr.every(res => encryptedText === res);
      expect(result).to.not.be.undefined;
      expect(result).to.be.true;
    });

    it('should properly decrypt to original text', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'test text';
      const encryptedResult = encryptDecrypt.encrypt(inputText);
      const decryptedResult = encryptDecrypt.decrypt(encryptedResult);
      expect(decryptedResult).to.not.be.undefined;
      expect(decryptedResult).to.equal(inputText);
    });

    it('should properly handle empty text input for encryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = '';
      const result = encryptDecrypt.encrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle null input for encryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = null;
      const result = encryptDecrypt.encrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle empty text input for decryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = '';
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle null input for decryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = null;
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle an invalid/failed decryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'XXXXXXXXX';
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should log error for failed decryption', () => {
      const loggerSpy = sinon.spy(mocks.logger, 'error');
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'XXXXXXXXX';
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
      expect(loggerSpy).to.have.been.called;
    });
  });

  describe('in prod', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/config']);
      environment.use('production');
      mocks.logger = MockLogger();
    });

    it('should properly load', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      expect(encryptDecrypt).to.not.be.undefined;
      expect(encryptDecrypt.encrypt).to.not.be.undefined;
      expect(encryptDecrypt.decrypt).to.not.be.undefined;
      expect(typeof encryptDecrypt.encrypt).to.equal('function');
      expect(typeof encryptDecrypt.decrypt).to.equal('function');
    });

    it('should encrypt a given text', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'test text';
      const result = encryptDecrypt.encrypt(inputText);
      expect(result).to.not.be.undefined;
      expect(result).to.not.equal(inputText);
    });

    it('should output same results when same input text is encrypted', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'test text';
      const encryptedText = encryptDecrypt.encrypt(inputText);
      const resultArr = [];

      for (let i = 0; i < 20; i += 1) {
        resultArr.push(encryptDecrypt.encrypt(inputText));
      }

      const result = resultArr.every(res => encryptedText === res);
      expect(result).to.not.be.undefined;
      expect(result).to.be.true;
    });

    it('should properly decrypt to original text', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'test text';
      const encryptedResult = encryptDecrypt.encrypt(inputText);
      const decryptedResult = encryptDecrypt.decrypt(encryptedResult);
      expect(decryptedResult).to.not.be.undefined;
      expect(decryptedResult).to.equal(inputText);
    });

    it('should properly handle empty text input for encryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = '';
      const result = encryptDecrypt.encrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle null input for encryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = null;
      const result = encryptDecrypt.encrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle empty text input for decryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = '';
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle null input for decryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = null;
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should properly handle an invalid/failed decryption', () => {
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'XXXXXXXXX';
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
    });

    it('should log error for failed decryption', () => {
      const loggerSpy = sinon.spy(mocks.logger, 'error');
      const encryptDecrypt = requireEncryptDecrypt();
      const inputText = 'XXXXXXXXX';
      const result = encryptDecrypt.decrypt(inputText);
      expect(result).to.be.undefined;
      expect(loggerSpy).to.have.been.called;
    });
  });
});
