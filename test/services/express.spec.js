const environment = require('../environment');
const EventEmitter = require('events');
const express = require('express');
const request = require('supertest');
const sinon = require('sinon');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();

const expect = chai.expect;

const LoggerMock = require('../mocks/services/logger-mock');

describe('Service: Express', () => {
  const mocks = {};
  const requireExpress = (overrideMocks = {}) =>
    proxyquire(
      '../../app/services/express',
      Object.assign(
        {
          '../logger': mocks.logger
        },
        overrideMocks
      )
    );

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/config']);
      environment.use('dev');
      mocks.logger = LoggerMock();
    });

    it('should properly load', () => {
      const expressService = requireExpress();
      expect(expressService).to.not.be.undefined;
    });

    it('should handle GET requests', () => {
      const expressService = requireExpress();

      return request(expressService.app)
        .get('/')
        .send()
        .expect(200);
    });

    it('should handle 404 GET requests', () => {
      const expressService = requireExpress();

      return request(expressService.app)
        .get('/somethingdoesnotexist')
        .send()
        .expect(404);
    });

    it('should handle error in router', () => {
      const expressService = requireExpress();

      expressService.router.get('/fake', () => {
        throw new Error('fake error');
      });

      return request(expressService.app)
        .get('/fake')
        .send()
        .expect(500);
    });

    it('should handle listening on a port with routes', () => {
      const eventEmmiter = new EventEmitter();

      const Server = {
        listen: (port, host, cb) => {
          expect(port).to.equal(process.env.PORT);
          setTimeout(cb, 1);
          return eventEmmiter;
        }
      };

      const expressService = requireExpress({
        http: {
          Server: () => Server
        }
      });

      const listen = sinon.spy(Server, 'listen');
      const routes = express.Router();

      return expressService.start(routes).then(() => {
        expect(listen).to.have.been.calledWith(process.env.PORT);
      });
    });

    it('should handle error on attempt at listening on a un-open port', () => {
      const bindError = new Error('Failed to bind to port');
      const eventEmmiter = new EventEmitter();

      const Server = {
        listen: (_port, _cb) => {
          setTimeout(() => {
            eventEmmiter.emit('error', bindError);
          }, 1);
          return eventEmmiter;
        }
      };

      const expressService = requireExpress({
        http: {
          Server: () => Server
        }
      });

      return expressService
        .start()
        .then(() => {
          throw new Error('Should have throw an error');
        })
        .catch((err) => {
          expect(err).to.equal(bindError);
        });
    });

    it('should handle error on attempt at listening on a un-open port', () => {
      const bindError = new Error('Failed to bind to port');

      const Server = {
        listen: (_port, _cb) => {
          throw bindError;
        }
      };

      const expressService = requireExpress({
        http: {
          Server: () => Server
        }
      });

      return expressService
        .start()
        .then(() => {
          throw new Error('Should have throw an error');
        })
        .catch((err) => {
          expect(err).to.equal(bindError);
        });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app', '../../app/config']);
      environment.use('production');
      mocks.logger = LoggerMock();
    });

    it('should properly load', () => {
      const expressService = requireExpress();
      expect(expressService).to.not.be.undefined;
    });

    it('should handle GET requests', () => {
      const expressService = requireExpress();

      return request(expressService.app)
        .get('/')
        .set('Origin', 'http://api.ams-dev.semanticbits.com')
        .send()
        .expect(200);
    });

    it('should handle 404 GET requests', () => {
      const expressService = requireExpress();

      return request(expressService.app)
        .get('/somethingdoesnotexist')
        .set('Origin', 'http://api.ams-dev.semanticbits.com')
        .send()
        .expect(404);
    });

    it('should handle error in router', () => {
      const expressService = requireExpress();

      expressService.router.get('/fake', () => {
        throw new Error('fake error');
      });

      return request(expressService.app)
        .get('/fake')
        .set('Origin', 'http://api.ams-dev.semanticbits.com')
        .send()
        .expect(500);
    });

    it('should handle listening on a port with routes', () => {
      const eventEmmiter = new EventEmitter();

      const Server = {
        listen: (port, host, cb) => {
          expect(port).to.equal(process.env.PORT);
          setTimeout(cb, 1);
          return eventEmmiter;
        }
      };

      const expressService = requireExpress({
        http: {
          Server: () => Server
        }
      });

      const listen = sinon.spy(Server, 'listen');
      const routes = express.Router();

      return expressService.start(routes).then(() => {
        expect(listen).to.have.been.calledWith(process.env.PORT);
      });
    });

    it('should handle error on attempt at listening on a un-open port', () => {
      const bindError = new Error('Failed to bind to port');
      const eventEmmiter = new EventEmitter();

      const Server = {
        listen: (_port, _cb) => {
          setTimeout(() => {
            eventEmmiter.emit('error', bindError);
          }, 1);
          return eventEmmiter;
        }
      };

      const expressService = requireExpress({
        http: {
          Server: () => Server
        }
      });

      return expressService
        .start()
        .then(() => {
          throw new Error('Should have throw an error');
        })
        .catch((err) => {
          expect(err).to.equal(bindError);
        });
    });

    it('should handle error on attempt at listening on a un-open port', () => {
      const bindError = new Error('Failed to bind to port');

      const Server = {
        listen: (_port, _cb) => {
          throw bindError;
        }
      };

      const expressService = requireExpress({
        http: {
          Server: () => Server
        }
      });

      return expressService
        .start()
        .then(() => {
          throw new Error('Should have throw an error');
        })
        .catch((err) => {
          expect(err).to.equal(bindError);
        });
    });
  });
});
