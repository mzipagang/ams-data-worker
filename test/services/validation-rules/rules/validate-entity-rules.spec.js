const environment = require('../../../environment');
const moment = require('moment');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();

const expect = chai.expect;

const LoggerMock = require('../../../mocks/services/logger-mock');
const Model = require('../../../mocks/dependencies/model');
const CreateEntity = require('../../../mocks/data/apm_entity');
const CreateModel = require('../../../mocks/data/pac_model');
const CreateSubdiv = require('../../../mocks/data/pac_subdivision');

const config = require('../../../../app/services/validation/rules/config').entity;

const rulesConfig = {};
config.forEach((rule) => { rulesConfig[rule.ruleName] = rule.type; });

describe('Service: validations/rules/entity', () => {
  const mocks = {};
  const requireRules = (overrideMocks = {}) =>
    proxyquire('../../../../app/services/validation/rules/entity', Object.assign({
      '../logger': mocks.logger
    }, overrideMocks));
  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, [
        '../../../../app/services/validation/rules/entity',
        '../../../../app/config'
      ]);
      environment.use('dev');
      mocks.logger = LoggerMock();
    });

    it('should load properly', () => {
      const rules = requireRules();
      expect(rules).to.not.be.undefined;
      expect(rules.rules).to.not.be.undefined;
    });

    describe('apm id', () => {
      it('should properly check apm_id', () => {
        const entity = CreateEntity();
        const model = CreateModel();

        const rules = requireRules();
        const rule = rules.rules.apm_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.apm_id_valid;
        const result = rule(entity, errorCode, type, { model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check apm_id if it doesn\'t exist', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.apm_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.apm_id_valid;
        const result = rule(entity, errorCode, type, { model: {}, entity: {} });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require an apm_id', () => {
        const entity = CreateEntity({ apm_id: null });

        const rules = requireRules();
        const rule = rules.rules.apm_id_required;
        const errorCode = 'A1';
        const type = rulesConfig.apm_id_required;
        const result = rule(entity, errorCode, type, { model: {}, entity: {} });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('subdivision id', () => {
      it('should require a subdiv_id', () => {
        const entity = CreateEntity();
        entity.subdiv_id = null;
        mocks.apm_entity = Model(entity);
        mocks.apm_subdiv = Model(null);

        const rules = requireRules();
        const rule = rules.rules.subdiv_id_required;
        const errorCode = 'A1';
        const type = rulesConfig.subdiv_id_required;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check subdiv_id', () => {
        const entity = CreateEntity();
        const rules = requireRules();
        const rule = rules.rules.subdiv_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.subdiv_id_valid;
        const result = rule(entity, errorCode, type, { subdivision: { id: '123' } });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check subdiv_id if it doesn\'t exist', () => {
        const entity = CreateEntity();
        const rules = requireRules();
        const rule = rules.rules.subdiv_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.subdiv_id_valid;
        const result = rule(entity, errorCode, type, { subdivision: null });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('start date', () => {
      it('should properly check start_date is before end_date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_end_date;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require start_date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: null,
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_required;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_required;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should fail if start_date is not a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: null,
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.start_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_valid_date;

        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require start_date to be a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.start_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_valid_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if start date is equal to 1 day after end date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_end_date;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly check start_date is after APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check start_date is after APM start_date when it is not', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly pass if start_date is after APM end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if start_date is not before APM end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check start_date is after Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should fail when start_date is before Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if there is no subdiv_id when checking start Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = Object.assign({}, CreateEntity().toJson(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        delete entity.subdiv_id;

        const rules = requireRules();
        const rule = rules.rules.start_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if start_date is before Subdiv end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if start_date is not before Subdiv end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should be valid if no subdivision is found', () => {
        const rules = requireRules();
        const rule = rules.rules.start_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_subdivision_end_date;
        const result = rule({}, errorCode, type, { subdivision: null, model: null });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('end date', () => {
      it('should properly check end_date is after start_date', () => {
        const model = CreateModel();
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        mocks.apm_model = Model(model);
        mocks.apm_entity = Model(entity);
        mocks.apm_subdiv = Model(subdiv);

        const rules = requireRules();
        const rule = rules.rules.end_date_after_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_start_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require end_date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_required;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_required;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require end_date to be a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.end_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_valid_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if end date is equal to 1 before after start date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.end_date_after_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_start_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should fail if end_date is not a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: null
        });

        const rules = requireRules();
        const rule = rules.rules.end_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_valid_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly check end_date is before APM end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check end_date is before APM end_date when it is not', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly pass if end_date is after APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if end_date is equal to one day before APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2017-12-31').toDate()
        }));
        const subdiv = null;
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if end_date is not after APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check end_date is before Subdiv end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check end_date is before Subdiv end_date when it is not', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if no subdivision found', () => {
        const rules = requireRules();
        const rule = rules.rules.end_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_subdivision_end_date;
        const result = rule({}, errorCode, type, { subdivision: null, model: null });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if end_date is after Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if end_date equals one day before Subdiv start_date', () => {
        const model = null;
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2017-12-31').toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if end_date is not after Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should be valid if no subdivision is found', () => {
        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule({}, errorCode, type, { subdivision: null, model: null });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('entity type', () => {
      it('should pass if no type is provided', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.entity_type;
        const errorCode = 'A1';
        const type = rulesConfig.entity_type;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly all types', () => {
        const types = ['ACO', 'CBO', 'MDC', 'PRA', 'OTH', 'HSP', 'PAC'];
        const entity = types.map(type => CreateEntity(Object.assign({}, CreateEntity().toObject(), { type })));

        const rules = requireRules();
        const rule = rules.rules.entity_type;
        const errorCode = 'A1';
        const type = rulesConfig.entity_type;

        for (let i = 0; i < entity.length; i += 1) {
          const result = rule(entity[i], errorCode, type);
          expect(result).to.not.be.undefined;
          expect(result.isValid).to.equal(true);
        }
      });

      it('should properly check type if it doesn\'t exist', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'test' }));

        const rules = requireRules();
        const rule = rules.rules.entity_type;
        const errorCode = 'A1';
        const type = rulesConfig.entity_type;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });
    });

    describe('entity tin', () => {
      it('should properly check tin', () => {
        const entity = CreateEntity();
        const rules = requireRules();
        const rule = rules.rules.entity_tin;
        const errorCode = 'A1';
        const type = rulesConfig.entity_tin;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check tin if it doesn\'t exist', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { tin: '1' }));

        const rules = requireRules();
        const rule = rules.rules.entity_tin;
        const errorCode = 'A1';
        const type = rulesConfig.entity_tin;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('entity npi', () => {
      it('should properly check npi', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.entity_npi;
        const errorCode = 'A1';
        const type = rulesConfig.entity_npi;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check npi if it doesn\'t exist', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { npi: '1' }));

        const rules = requireRules();
        const rule = rules.rules.entity_npi;
        const errorCode = 'A1';
        const type = rulesConfig.entity_npi;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('entity ccn', () => {
      it('should properly check ccn', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.entity_ccn;
        const errorCode = 'A1';
        const type = rulesConfig.entity_ccn;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check ccn length', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { ccn: '1234567' }));

        const rules = requireRules();
        const rule = rules.rules.entity_ccn;
        const errorCode = 'A1';
        const type = rulesConfig.entity_ccn;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('address_1', () => {
      it('should be blank if medicare type', () => {
        const medicareTypes = ['PRA', 'HSP', 'MDC', 'PAC'];
        medicareTypes.forEach(type => {
          const entity = CreateEntity({ type, address: { address_1: '' } });

          const rules = requireRules();
          const rule = rules.rules.address_1_blank_if_medicare;
          const errorCode = 'A1';
          const ruleType = rulesConfig.address_1_blank_if_medicare;
          const result = rule(entity, errorCode, ruleType);
          expect(result).to.not.be.undefined;
          expect(result.isValid).to.equal(true);
        });
      });

      it('should not be blank if none medicare type', () => {
        const entity = CreateEntity({ type: 'ACO', address: { address_1: '' } });

        const rules = requireRules();
        const rule = rules.rules.address_1_required;
        const errorCode = 'A1';
        const ruleType = rulesConfig.address_1_required;
        const result = rule(entity, errorCode, ruleType);
        expect(!!result).to.be.true;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check address_1', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check address_1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when address_1 is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_1 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should return valid for address_1_requires_state if address 1 is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_1: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check address_1_requires_state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_1 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check address_1 - requires state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result.isValid).to.equal(false);
        expect(result).to.not.be.undefined;
        expect(result.validationType).to.equal('SOFT_VALIDATION');
        expect(result.errorCode).to.equal('A1');
      });

      it('should properly check address_1 - requires zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should be valid when checking address_1_requires_zip5 with no address_1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_1: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('address_2', () => {
      it('should be blank if type is medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { address_2: '' } });

        const rules = requireRules();
        const rule = rules.rules.address_2_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check address_2 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when address_2 is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check address_2 - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check address_2 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should not check address_2 - require city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking address_2 - requires city when there is no address_2', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_2: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2 - requires valid state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should not check address_2 - require state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking address_2 - requires valid state with no address_2', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_2: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2 - requires zip', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should not check address_2 - require zip when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking address_2 - requires zip with no address_2', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_2: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('city', () => {
      it('should be blank if medicare entity', () => {
        const entity = CreateEntity({ type: 'MDC', address: { city: '' } });

        const rules = requireRules();
        const rule = rules.rules.city_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.city_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not be blank if non-medicare entity', () => {
        const entity = CreateEntity({ type: 'ACO', address: { city: '' } });

        const rules = requireRules();
        const rule = rules.rules.city_required;
        const errorCode = 'A1';
        const type = rulesConfig.city_required;
        const result = rule(entity, errorCode, type);
        expect(!!result).to.be.true;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check city', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when city is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { city: '' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check city when other address info is missing', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check city - requires address 1', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check city - requires valid state', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check city - requires valid state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking city - requires valid state with no city', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check city - requires zip5', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_zip;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_zip;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check city - requires zip5 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_zip;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_zip;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking city_requires_zip with no city', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_zip;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_zip;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('zip4', () => {
      it('should be blank if type is medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { zip4: null } });

        const rules = requireRules();
        const rule = rules.rules.zip4_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check zip4 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4 length', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: 'CA', zip4: '123', zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip4 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip4 - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check zip4 - require address_1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should skip zip4 requires city if it is a medicare entity type', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4 - requires valid state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should skip zip4 requires state if it is a medicare entity type', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });


    describe('zip5', () => {
      it('should be blank if type is medicare type', () => {
        const medicareTypes = ['PRA', 'HSP', 'MDC', 'PAC'];
        medicareTypes.forEach(type => {
          const entity = CreateEntity({ type, address: { zip5: null } });

          const rules = requireRules();
          const rule = rules.rules.zip5_blank_if_medicare;
          const errorCode = 'A1';
          const ruleType = rulesConfig.zip5_blank_if_medicare;
          const result = rule(entity, errorCode, ruleType);
          expect(result).to.not.be.undefined;
          expect(result.isValid).to.equal(true);
        });
      });

      it('should not be blank if a non-medicare type', () => {
        const entity = CreateEntity({ type: 'ACO', address: { zip5: null } });

        const rules = requireRules();
        const rule = rules.rules.zip5_required;
        const errorCode = 'A1';
        const ruleType = rulesConfig.zip5_required;
        const result = rule(entity, errorCode, ruleType);
        expect(result).to.exist;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check zip5', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check zip5 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when zip5 is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip5 length', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: 'CA', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip5 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip5 - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check zip5 - requires address 1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking zip5_requires_address_1 with no zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip5 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should return valid when checking zip5_requires_city with no zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check zip5 - requires city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip5 - requires valid state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check zip5 - requires state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking zip5_requires_valid_state with no zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('state', () => {
      it('should be blank if type is medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { state: null } });

        const rules = requireRules();
        const rule = rules.rules.state_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.state_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not be blank if type is non-medicare', () => {
        const entity = CreateEntity({ type: 'ACO', address: { state: null } });

        const rules = requireRules();
        const rule = rules.rules.state_required;
        const errorCode = 'A1';
        const type = rulesConfig.state_required;
        const result = rule(entity, errorCode, type);
        expect(result).to.exist;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check state', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when state is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      // it('should properly check for valid 2 char state code', () => {
      //   const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
      //     { address: { address_1: 'test', city: 'test', state: 'NEW', zip4: '1234', zip5: '1' } }));
      //
      //   const rules = requireRules();
      //   const rule = rules.rules.state_not_2_characters;
      //   const errorCode = 'A1';
      //   const type = rulesConfig.state_not_2_characters;
      //   const result = rule(entity, errorCode, type);
      //   expect(result).to.not.be.undefined;
      //   expect(result.isValid).to.equal(false);
      //   expect(result.errorCode).to.equal('A1');
      //   expect(result.validationType).to.equal('SOFT_VALIDATION');
      // });
      //
      // it('should not check if state 2 char when entity type is medicare', () => {
      //   const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));
      //
      //   const rules = requireRules();
      //   const rule = rules.rules.state_not_2_characters;
      //   const errorCode = 'A1';
      //   const type = rulesConfig.state_not_2_characters;
      //   const result = rule(entity, errorCode, type);
      //   expect(result).to.not.be.undefined;
      //   expect(result.isValid).to.equal(true);
      // });
      //
      // it('should return valid when checking state_not_2_characters with no state', () => {
      //   const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
      //     { address: { address_1: 'test', city: 'test', state: undefined, zip4: '1234', zip5: '1' } }));
      //
      //   const rules = requireRules();
      //   const rule = rules.rules.state_not_2_characters;
      //   const errorCode = 'A1';
      //   const type = rulesConfig.state_not_2_characters;
      //   const result = rule(entity, errorCode, type);
      //   expect(result).to.not.be.undefined;
      //   expect(result.isValid).to.equal(true);
      // });

      it('should properly check for valid char state code', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: 'XX', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_valid_code;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_valid_code;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state valid when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_valid_code;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_valid_code;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking state_requires_valid_code with no state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: undefined, zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_valid_code;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_valid_code;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check state when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check state - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state requires city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check state - requires zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state requires zip5 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking state_requires_zip5 with no state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check state - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state requires address 1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking state_requires_address_1 with no state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, [
        '../../../../app/services/validation/rules/entity',
        '../../../../app/config'
      ]);
      environment.use('production');
      mocks.logger = LoggerMock();
    });

    it('should load properly', () => {
      const rules = requireRules();
      expect(rules).to.not.be.undefined;
      expect(rules.rules).to.not.be.undefined;
    });

    describe('apm id', () => {
      it('should properly check apm_id', () => {
        const entity = CreateEntity();
        const model = CreateModel();

        const rules = requireRules();
        const rule = rules.rules.apm_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.apm_id_valid;
        const result = rule(entity, errorCode, type, { model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check apm_id if it doesn\'t exist', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.apm_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.apm_id_valid;
        const result = rule(entity, errorCode, type, { model: {}, entity: {} });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require an apm_id', () => {
        const entity = CreateEntity({ apm_id: null });

        const rules = requireRules();
        const rule = rules.rules.apm_id_required;
        const errorCode = 'A1';
        const type = rulesConfig.apm_id_required;
        const result = rule(entity, errorCode, type, { model: {}, entity: {} });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('subdivision id', () => {
      it('should require a subdiv_id', () => {
        const entity = CreateEntity();
        entity.subdiv_id = null;
        mocks.apm_entity = Model(entity);
        mocks.apm_subdiv = Model(null);

        const rules = requireRules();
        const rule = rules.rules.subdiv_id_required;
        const errorCode = 'A1';
        const type = rulesConfig.subdiv_id_required;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check subdiv_id', () => {
        const entity = CreateEntity();
        const rules = requireRules();
        const rule = rules.rules.subdiv_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.subdiv_id_valid;
        const result = rule(entity, errorCode, type, { subdivision: { id: '123' } });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check subdiv_id if it doesn\'t exist', () => {
        const entity = CreateEntity();
        const rules = requireRules();
        const rule = rules.rules.subdiv_id_valid;
        const errorCode = 'A1';
        const type = rulesConfig.subdiv_id_valid;
        const result = rule(entity, errorCode, type, { subdivision: null });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('start date', () => {
      it('should properly check start_date is before end_date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_end_date;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require start_date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: null,
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_required;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_required;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should fail if start_date is not a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: null,
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.start_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_valid_date;

        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require start_date to be a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.start_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_valid_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if start date is equal to 1 day after end date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_end_date;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly check start_date is after APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check start_date is after APM start_date when it is not', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly pass if start_date is after APM end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if start_date is not before APM end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check start_date is after Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should fail when start_date is before Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if there is no subdiv_id when checking start Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = Object.assign({}, CreateEntity().toJson(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        delete entity.subdiv_id;

        const rules = requireRules();
        const rule = rules.rules.start_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if start_date is before Subdiv end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if start_date is not before Subdiv end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.start_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should be valid if no subdivision is found', () => {
        const rules = requireRules();
        const rule = rules.rules.start_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.start_date_before_subdivision_end_date;
        const result = rule({}, errorCode, type, { subdivision: null, model: null });

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('end date', () => {
      it('should properly check end_date is after start_date', () => {
        const model = CreateModel();
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        mocks.apm_model = Model(model);
        mocks.apm_entity = Model(entity);
        mocks.apm_subdiv = Model(subdiv);

        const rules = requireRules();
        const rule = rules.rules.end_date_after_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_start_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require end_date', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_required;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_required;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should require end_date to be a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.end_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_valid_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if end date is equal to 1 before after start date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate()
        });

        const rules = requireRules();
        const rule = rules.rules.end_date_after_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_start_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should fail if end_date is not a date', () => {
        const entity = Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: null
        });

        const rules = requireRules();
        const rule = rules.rules.end_date_valid_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_valid_date;
        const result = rule(entity, errorCode, type);

        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly check end_date is before APM end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check end_date is before APM end_date when it is not', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_apm_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_apm_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should properly pass if end_date is after APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if end_date is equal to one day before APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2017-12-31').toDate()
        }));
        const subdiv = null;
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if end_date is not after APM start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          subdiv_id: null
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_apm_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_apm_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check end_date is before Subdiv end_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check end_date is before Subdiv end_date when it is not', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_subdivision_end_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });

      it('should be valid if no subdivision found', () => {
        const rules = requireRules();
        const rule = rules.rules.end_date_before_subdivision_end_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_before_subdivision_end_date;
        const result = rule({}, errorCode, type, { subdivision: null, model: null });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if end_date is after Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly pass if end_date equals one day before Subdiv start_date', () => {
        const model = null;
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2017-12-31').toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc('2017-01-01').toDate(),
          end_date: moment.utc('2016-12-31').toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly fail if end_date is not after Subdiv start_date', () => {
        const model = CreateModel(Object.assign({}, CreateModel().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const subdiv = CreateSubdiv(Object.assign({}, CreateSubdiv().toObject(), {
          start_date: moment.utc()
            .add(-1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(1, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          start_date: moment.utc()
            .add(-3, 'month')
            .startOf('month')
            .startOf('day')
            .toDate(),
          end_date: moment.utc()
            .add(-2, 'month')
            .startOf('month')
            .startOf('day')
            .toDate()
        }));

        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule(entity, errorCode, type, { subdivision: subdiv, model });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should be valid if no subdivision is found', () => {
        const rules = requireRules();
        const rule = rules.rules.end_date_after_subdivision_start_date;
        const errorCode = 'A1';
        const type = rulesConfig.end_date_after_subdivision_start_date;
        const result = rule({}, errorCode, type, { subdivision: null, model: null });
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('entity type', () => {
      it('should pass if no type is provided', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.entity_type;
        const errorCode = 'A1';
        const type = rulesConfig.entity_type;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly all types', () => {
        const types = ['ACO', 'CBO', 'MDC', 'PRA', 'OTH', 'HSP', 'PAC'];
        const entity = types.map(type => CreateEntity(Object.assign({}, CreateEntity().toObject(), { type })));

        const rules = requireRules();
        const rule = rules.rules.entity_type;
        const errorCode = 'A1';
        const type = rulesConfig.entity_type;

        for (let i = 0; i < entity.length; i += 1) {
          const result = rule(entity[i], errorCode, type);
          expect(result).to.not.be.undefined;
          expect(result.isValid).to.equal(true);
        }
      });

      it('should properly check type if it doesn\'t exist', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'test' }));

        const rules = requireRules();
        const rule = rules.rules.entity_type;
        const errorCode = 'A1';
        const type = rulesConfig.entity_type;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });
    });

    describe('entity tin', () => {
      it('should properly check tin', () => {
        const entity = CreateEntity();
        const rules = requireRules();
        const rule = rules.rules.entity_tin;
        const errorCode = 'A1';
        const type = rulesConfig.entity_tin;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check tin if it doesn\'t exist', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { tin: '1' }));

        const rules = requireRules();
        const rule = rules.rules.entity_tin;
        const errorCode = 'A1';
        const type = rulesConfig.entity_tin;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('entity npi', () => {
      it('should properly check npi', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.entity_npi;
        const errorCode = 'A1';
        const type = rulesConfig.entity_npi;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check npi if it doesn\'t exist', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { npi: '1' }));

        const rules = requireRules();
        const rule = rules.rules.entity_npi;
        const errorCode = 'A1';
        const type = rulesConfig.entity_npi;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('entity ccn', () => {
      it('should properly check ccn', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.entity_ccn;
        const errorCode = 'A1';
        const type = rulesConfig.entity_ccn;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check ccn length', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { ccn: '1234567' }));

        const rules = requireRules();
        const rule = rules.rules.entity_ccn;
        const errorCode = 'A1';
        const type = rulesConfig.entity_ccn;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('HARD_VALIDATION');
      });
    });

    describe('address_1', () => {
      it('should be blank if medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { address_1: '' } });

        const rules = requireRules();
        const rule = rules.rules.address_1_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_1', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check address_1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when address_1 is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_1 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should return valid for address_1_requires_state if address 1 is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_1: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check address_1_requires_state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_1 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check address_1 - requires state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_state;
        const result = rule(entity, errorCode, type);
        expect(result.isValid).to.equal(false);
        expect(result).to.not.be.undefined;
        expect(result.validationType).to.equal('SOFT_VALIDATION');
        expect(result.errorCode).to.equal('A1');
      });

      it('should properly check address_1 - requires zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_1: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should be valid when checking address_1_requires_zip5 with no address_1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_1: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_1_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_1_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('address_2', () => {
      it('should be blank if type is medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { address_2: '' } });

        const rules = requireRules();
        const rule = rules.rules.address_2_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check address_2 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when address_2 is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check address_2 - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should properly check address_2 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should not check address_2 - require city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking address_2 - requires city when there is no address_2', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_2: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2 - requires valid state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should not check address_2 - require state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking address_2 - requires valid state with no address_2', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_2: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check address_2 - requires zip', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { address_2: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
      });

      it('should not check address_2 - require zip when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking address_2 - requires zip with no address_2', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
          address: { address_2: undefined }
        }));

        const rules = requireRules();
        const rule = rules.rules.address_2_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.address_2_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('city', () => {
      it('should be blank if medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { city: '' } });

        const rules = requireRules();
        const rule = rules.rules.city_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.city_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check city', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when city is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { city: '' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check city when other address info is missing', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check city - requires address 1', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check city - requires valid state', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check city - requires valid state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking city - requires valid state with no city', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check city - requires zip5', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: 'test' } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_zip;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_zip;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check city - requires zip5 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_zip;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_zip;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking city_requires_zip with no city', () => {
        const entity =
          CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '', city: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.city_requires_zip;
        const errorCode = 'A1';
        const type = rulesConfig.city_requires_zip;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('zip4', () => {
      it('should be blank if type is medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { zip4: null } });

        const rules = requireRules();
        const rule = rules.rules.zip4_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check zip4 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4 length', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: 'CA', zip4: '123', zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip4 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_not_4_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_not_4_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip4 - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check zip4 - require address_1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should skip zip4 requires city if it is a medicare entity type', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip4 - requires valid state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: '', city: '', state: '', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip4_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip4_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });
    });

    it('should skip zip4 requires state if it is a medicare exntity type', () => {
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
        { type: 'MDC' }));

      const rules = requireRules();
      const rule = rules.rules.zip4_requires_valid_state;
      const errorCode = 'A1';
      const type = rulesConfig.zip4_requires_valid_state;
      const result = rule(entity, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    describe('zip5', () => {
      it('should be blank if type is medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { zip5: null } });

        const rules = requireRules();
        const rule = rules.rules.zip5_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip5', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check zip5 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when zip5 is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip5 length', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: 'CA', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip5 when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_is_not_5_characters;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_is_not_5_characters;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check zip5 - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check zip5 - requires address 1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking zip5_requires_address_1 with no zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip5 - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should return valid when checking zip5_requires_city with no zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should not check zip5 - requires city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check zip5 - requires valid state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: '12345' } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check zip5 - requires state when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking zip5_requires_valid_state with no zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { zip5: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.zip5_requires_valid_state;
        const errorCode = 'A1';
        const type = rulesConfig.zip5_requires_valid_state;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });

    describe('state', () => {
      it('should be blank if type is medicare', () => {
        const entity = CreateEntity({ type: 'MDC', address: { state: null } });

        const rules = requireRules();
        const rule = rules.rules.state_blank_if_medicare;
        const errorCode = 'A1';
        const type = rulesConfig.state_blank_if_medicare;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check state', () => {
        const entity = CreateEntity();

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when state is empty', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: '' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      // it('should properly check for valid 2 char state code', () => {
      //   const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
      //     { address: { address_1: 'test', city: 'test', state: 'NEW', zip4: '1234', zip5: '1' } }));
      //
      //   const rules = requireRules();
      //   const rule = rules.rules.state_not_2_characters;
      //   const errorCode = 'A1';
      //   const type = rulesConfig.state_not_2_characters;
      //   const result = rule(entity, errorCode, type);
      //   expect(result).to.not.be.undefined;
      //   expect(result.isValid).to.equal(false);
      //   expect(result.errorCode).to.equal('A1');
      //   expect(result.validationType).to.equal('SOFT_VALIDATION');
      // });
      //
      // it('should not check if state 2 char when entity type is medicare', () => {
      //   const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));
      //
      //   const rules = requireRules();
      //   const rule = rules.rules.state_not_2_characters;
      //   const errorCode = 'A1';
      //   const type = rulesConfig.state_not_2_characters;
      //   const result = rule(entity, errorCode, type);
      //   expect(result).to.not.be.undefined;
      //   expect(result.isValid).to.equal(true);
      // });
      //
      // it('should return valid when checking state_not_2_characters with no state', () => {
      //   const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
      //     { address: { address_1: 'test', city: 'test', state: undefined, zip4: '1234', zip5: '1' } }));
      //
      //   const rules = requireRules();
      //   const rule = rules.rules.state_not_2_characters;
      //   const errorCode = 'A1';
      //   const type = rulesConfig.state_not_2_characters;
      //   const result = rule(entity, errorCode, type);
      //   expect(result).to.not.be.undefined;
      //   expect(result.isValid).to.equal(true);
      // });

      it('should properly check for valid char state code', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: 'XX', zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_valid_code;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_valid_code;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state valid when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_valid_code;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_valid_code;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking state_requires_valid_code with no state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(),
          { address: { address_1: 'test', city: 'test', state: undefined, zip4: '1234', zip5: '1' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_valid_code;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_valid_code;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check state when other address info is missing', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should properly check state - requires city', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state requires city when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_city;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_city;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check state - requires zip5', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state requires zip5 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking state_requires_zip5 with no state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_zip5;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_zip5;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should properly check state - requires address 1', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: 'NJJ' } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(false);
        expect(result.errorCode).to.equal('A1');
        expect(result.validationType).to.equal('SOFT_VALIDATION');
      });

      it('should not check if state requires address 1 when entity type is medicare', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { type: 'MDC' }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });

      it('should return valid when checking state_requires_address_1 with no state', () => {
        const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), { address: { state: undefined } }));

        const rules = requireRules();
        const rule = rules.rules.state_requires_address_1;
        const errorCode = 'A1';
        const type = rulesConfig.state_requires_address_1;
        const result = rule(entity, errorCode, type);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.equal(true);
      });
    });
  });
});

