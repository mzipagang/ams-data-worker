const environment = require('../../../environment');
const chai = require('chai');

const expect = chai.expect;

describe('Service: validations/rules', () => {
  const requireValidationRules = () => require('../../../../app/services/validation/rules');

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, [
        '../../../../app/services/validation/rules'
      ]);
      environment.use('dev');
    });

    it('should load properly', () => {
      const validationRules = requireValidationRules();
      expect(validationRules).to.not.be.undefined;
      expect(validationRules.entity).to.not.be.undefined;
      expect(validationRules.provider).to.not.be.undefined;
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, [
        '../../../../app/services/validation/rules'
      ]);
      environment.use('production');
    });

    it('should load properly', () => {
      const validationRules = requireValidationRules();
      expect(validationRules).to.not.be.undefined;
      expect(validationRules.entity).to.not.be.undefined;
      expect(validationRules.provider).to.not.be.undefined;
    });
  });
});
