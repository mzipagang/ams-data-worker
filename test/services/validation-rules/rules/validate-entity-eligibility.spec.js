const chai = require('chai');

const expect = chai.expect;

const eligibilityEntity = require('../../../../app/services/validation/rules/entity_eligibility');
const rulesConfiguration = require('../../../../app/services/validation/rules/config/entity_eligibility');

const mockData = {
  entity_id: 'A0001',
  claims_types: 'PHYS',
  lv_status_code: 'Y',
  lv_status_reason: 'BENE',
  lv_part_b_expenditures: '14641275.95',
  lv_bene_count: '19259',
  small_status_apm_entity_code: 'N',
  small_status_clinician_count: '101',
  performance_year: 2018,
  run_number: '2',
  complex_patient_score: 1
};

const mockCache = {
  entity: {
    performance_year: 2018,
    id: 'A0001'
  },
  mipsValidations: {
    entity: { id: 1 }
  }
};

describe('Service: validations/rules/entity_eligibility', () => {
  it('should contain a list of rules', () => {
    const rules = eligibilityEntity.rules;
    expect(Object.keys(rules).length).to.be.greaterThan(0);
  });

  const rules = eligibilityEntity.rules;
  const rulesConfig = rulesConfiguration.filter(r => !!r.isActive);

  rulesConfig.forEach((ruleInfo) => {
    describe(ruleInfo.ruleName, () => {
      it('should execute properly', () => {
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockData, ruleInfo.description, ruleInfo.type, mockCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.true;
      });

      it('should fail tests', () => {
        const mockFailData = {
          entity_id: '',
          claims_types: '',
          lv_status_code: 'test',
          lv_status_reason: 'test',
          lv_part_b_expenditures: 'test',
          lv_bene_count: 'test',
          small_status_apm_entity_code: '',
          small_status_clinician_count: 'test',
          performance_year: 2018,
          run_number: '2',
          complex_patient_score: 9
        };

        const mockFailCache = {
          entity: {
            performance_year: 2017,
            id: 'A101'
          },
          mipsValidations: {
          }
        };
        const rule = rules[ruleInfo.ruleName];
        if (ruleInfo.ruleName === 'lv_status_reason_valid') {
          mockFailData.lv_status_code = 'Y';
        } else if (ruleInfo.ruleName === 'lv_status_reason_blank') {
          mockFailData.lv_status_code = 'N';
        }
        const result = rule(mockFailData, ruleInfo.description, ruleInfo.type, mockFailCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.false;
      });
    });
  });

  it('entity_id_active_during_py should be invalid if no entity is found', () => {
    const rule = rules.entity_id_active_during_py;
    const result = rule(mockData, '', '', { entity: null });
    expect(result).to.not.be.undefined;
    expect(result.isValid).to.be.false;
  });

  it('lv_status_reason_valid should be valid if no lvt_status_code is entered', () => {
    const rule = rules.lv_status_reason_valid;
    const result = rule(Object.assign({}, mockData, { lv_status_code: null }), '', '', mockCache);
    expect(result).to.not.be.undefined;
    expect(result.isValid).to.be.true;
  });

  it('small_status_apm_entity_code should not validate if performance year is 2017', () => {
    const rule = rules.small_status_apm_entity_code;
    const result = rule(Object.assign({}, mockData, { performance_year: 2017 }), '', '', mockCache);
    expect(result).to.not.be.undefined;
    expect(result.isValid).to.be.true;
  });

  it('small_status_clinician_code should not validate if performance year is 2017', () => {
    const rule = rules.small_status_clinician_code;
    const result = rule(Object.assign({}, mockData, { performance_year: 2017 }), '', '', mockCache);
    expect(result).to.not.be.undefined;
    expect(result.isValid).to.be.true;
  });
});
