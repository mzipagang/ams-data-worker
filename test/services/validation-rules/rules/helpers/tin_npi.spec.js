const TinNpiHelper = require('../../../../../app/services/validation/rules/helpers/tin_npi');
const { expect } = require('chai');

describe('tin_npi helper', () => {
  describe('npiValidation()', () => {
    it('should verify that the npi matches', () => {
      const providerValidTins = {
        npi: '1234567890',
        validTinPrvdr: '123456789',
        validTinsEmplr: ['123456789'],
        validTinsReasgnmt: ['123456789'],
        deleted: false
      };

      const validationResult = TinNpiHelper.npiValidation('1234567890', providerValidTins);
      expect(validationResult.validNpi).to.equal(true);
    });

    it('should return a false value when npi does not match', () => {
      const providerValidTins = {
        npi: '2345678901',
        validTinPrvdr: '123456789',
        validTinsEmplr: ['123456789'],
        validTinsReasgnmt: ['123456789'],
        deleted: true
      };

      const validationResult = TinNpiHelper.npiValidation('1234567890', providerValidTins);
      expect(validationResult.validNpi).to.equal(false);
    });

    it('should return false if the npi is not found in idr', () => {
      const validationResult = TinNpiHelper.npiValidation('1234567890', null);
      expect(validationResult.validNpi).to.equal(false);
    });
  });

  describe('tinValidation()', () => {
    it('should verify that the tin is valid', () => {
      const providerValidTins = {
        npi: '1234567890',
        validTinPrvdr: '123456789',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const validationResult = TinNpiHelper.tinValidation('123456789', providerValidTins);
      expect(validationResult.validTin).to.equal(true);
    });

    it('validTin should return false if tin is not found', () => {
      const providerValidTins = {
        npi: '1234567890',
        validTinPrvdr: '123456788',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const validationResult = TinNpiHelper.tinValidation('123456789', providerValidTins);
      expect(validationResult.validTin).to.equal(false);
    });
  });

  describe('tinNpiValidation()', () => {
    it('should properly validate both tin and npi', () => {
      const providerValidTins = {
        npi: null,
        validTinPrvdr: '123456789',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const providerValidNpis = {
        npi: '23456789012',
        validTinPrvdr: null,
        validTinsEmplr: ['123456789'],
        validTinsReasgnmt: ['123456789'],
        deleted: false
      };

      const validationResult = TinNpiHelper.tinNpiValidation(
        '123456789',
        '23456789012',
        providerValidTins,
        providerValidNpis
      );
      expect(validationResult.validTin).to.equal(true);
      expect(validationResult.validNpi).to.equal(true);
      expect(validationResult.employeeOfTin).to.equal(true);
      expect(validationResult.billingReassignedToTin).to.equal(true);
    });

    it('billingReassignedToTin should be true if provider tin has reassigned billing rights', () => {
      const providerValidTins = {
        npi: null,
        validTinPrvdr: '123456789',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const providerValidNpis = {
        npi: '23456789012',
        validTinPrvdr: null,
        validTinsEmplr: [],
        validTinsReasgnmt: ['123456789'],
        deleted: false
      };

      const validationResult = TinNpiHelper.tinNpiValidation(
        '123456789',
        '23456789012',
        providerValidTins,
        providerValidNpis
      );
      expect(validationResult.billingReassignedToTin).to.equal(true);
    });

    it('billingReassignedToTin should be false if provider tin has does not have reassigned billing rights', () => {
      const providerValidTins = {
        npi: null,
        validTinPrvdr: '123456789',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const providerValidNpis = {
        npi: '23456789012',
        validTinPrvdr: null,
        validTinsEmplr: ['123456789'],
        validTinsReasgnmt: [],
        deleted: false
      };

      const validationResult = TinNpiHelper.tinNpiValidation(
        '123456789',
        '23456789012',
        providerValidTins,
        providerValidNpis
      );
      expect(validationResult.billingReassignedToTin).to.equal(false);
    });

    it('employeeOfTin should be true if provider is an employee of the tin', () => {
      const providerValidTins = {
        npi: null,
        validTinPrvdr: '123456789',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const providerValidNpis = {
        npi: '23456789012',
        validTinPrvdr: null,
        validTinsEmplr: ['123456789'],
        validTinsReasgnmt: [],
        deleted: false
      };

      const validationResult = TinNpiHelper.tinNpiValidation(
        '123456789',
        '23456789012',
        providerValidTins,
        providerValidNpis
      );
      expect(validationResult.employeeOfTin).to.equal(true);
    });

    it('employeeOfTin should be false if provider is not an employee of the tin', () => {
      const providerValidTins = {
        npi: null,
        validTinPrvdr: '123456789',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const providerValidNpis = {
        npi: '23456789012',
        validTinPrvdr: null,
        validTinsEmplr: [],
        validTinsReasgnmt: ['123456789'],
        deleted: false
      };

      const validationResult = TinNpiHelper.tinNpiValidation(
        '123456789',
        '23456789012',
        providerValidTins,
        providerValidNpis
      );
      expect(validationResult.employeeOfTin).to.equal(false);
    });
  });
});
