const moment = require('moment');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();

const expect = chai.expect;
const config = require('../../../../app/services/validation/rules/config').provider;

const rulesConfig = {};
config.forEach((rule) => { rulesConfig[rule.ruleName] = rule.type; });
const LoggerMock = require('../../../mocks/services/logger-mock');
const Model = require('../../../mocks/dependencies/model');
const CreateEntity = require('../../../mocks/data/apm_entity');
const CreateProvider = require('../../../mocks/data/apm_provider');

describe('Service: validations/rules/provider', () => {
  const mocks = {};
  const requireRules = (overrideMocks = {}) =>
    proxyquire('../../../../app/services/validation/rules/provider', Object.assign({
      '../logger': mocks.logger
    }, overrideMocks));

  beforeEach(() => {
    mocks.logger = LoggerMock();
  });

  it('should load properly', () => {
    const rules = requireRules();
    expect(rules).to.not.be.undefined;
    expect(rules.rules).to.not.be.undefined;
  });

  describe('entity_id', () => {
    it('should properly check entity_id', () => {
      const provider = CreateProvider();

      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.entity_id_required;
      const errorCode = 'A1';
      const type = rulesConfig.entity_id_required;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should properly check entity_id if it does not exist', () => {
      const provider = CreateProvider({});
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.entity_id_required;
      const errorCode = 'A1';
      const type = rulesConfig.entity_id_required;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });
  });

  describe('start_date', () => {
    it('should require start_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: null,
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.start_date_required;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_required;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should require start_date to be a date', () => {
      const provider = Object.assign({}, CreateProvider().toObject(), {
        start_date: 'null',
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      });
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.start_date_valid;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_valid;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should properly check start_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const requiredRule = rules.rules.start_date_required;
      const validRule = rules.rules.start_date_valid;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_valid;
      const requiredResult = requiredRule(provider, errorCode, type);
      const validResult = validRule(provider, errorCode, type);
      expect(requiredResult).to.not.be.undefined;
      expect(requiredResult.isValid).to.equal(true);
      expect(validResult).to.not.be.undefined;
      expect(validResult.isValid).to.equal(true);
    });

    it('should properly check start_date is before end_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.start_date_before_end_date;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_before_end_date;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should properly fail when start_date is after end_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.start_date_before_end_date;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_before_end_date;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should not check start_date if Entity is not found', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      const rules = requireRules();
      const rule = rules.rules.start_date_after_entity_start_date;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_after_entity_start_date;
      const result = rule(provider, errorCode, type, { entity: null });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should properly check start_date is after Entity start_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      const rules = requireRules();
      const rule = rules.rules.start_date_after_entity_start_date;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_after_entity_start_date;
      const result = rule(provider, errorCode, type, { entity });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should properly check start_date is before Entity end_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      const rules = requireRules();
      const rule = rules.rules.start_date_before_entity_end_date;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_before_entity_end_date;
      const result = rule(provider, errorCode, type, { entity });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should properly check start_date is after Entity start_date when it is not', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-3, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.start_date_after_entity_start_date;
      const errorCode = 'A1';
      const type = rulesConfig.start_date_after_entity_start_date;
      const result = rule(provider, errorCode, type, { entity });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });
  });

  describe('end_date', () => {
    it('should require end_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: null
      }));
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_required;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_required;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should require end_date to be a date', () => {
      const provider = Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: 'null'
      });
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_valid;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_valid;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should properly check end_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const requiredRule = rules.rules.end_date_required;
      const validRule = rules.rules.end_date_valid;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_valid;
      const requiredResult = requiredRule(provider, errorCode, type);
      const validResult = validRule(provider, errorCode, type);
      expect(requiredResult).to.not.be.undefined;
      expect(requiredResult.isValid).to.equal(true);
      expect(validResult).to.not.be.undefined;
      expect(validResult.isValid).to.equal(true);
    });

    it('should properly check end_date is after start_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_after_start_date;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_after_start_date;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should properly fail if end_date is before start_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_after_start_date;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_after_start_date;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should properly check end_date is before Entity end_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_before_entity_end_date;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_before_entity_end_date;
      const result = rule(provider, errorCode, type, { entity });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should properly check end_date is after Entity start_date', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_after_entity_start_date;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_after_entity_start_date;
      const result = rule(provider, errorCode, type, { entity });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should properly check end_date is before Entity end_date when it is not', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(3, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_before_entity_end_date;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_before_entity_end_date;
      const result = rule(provider, errorCode, type, { entity });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should be valid for end date of 9999-12-31', () => {
      const provider = CreateProvider(Object.assign({}, CreateProvider().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc('9999-12-31').toDate()
      }));
      const entity = CreateEntity(Object.assign({}, CreateEntity().toObject(), {
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }));

      mocks.apm_entity = Model(entity);
      mocks.apm_provider = Model(provider);

      const rules = requireRules();
      const rule = rules.rules.end_date_valid;
      const errorCode = 'A1';
      const type = rulesConfig.end_date_valid;
      const result = rule(provider, errorCode, type, { entity });
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });
  });

  describe('tin', () => {
    it('should properly check tin', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin = '123456789';

      const rules = requireRules();
      const rule = rules.rules.tin_length;
      const errorCode = 'A1';
      const type = rulesConfig.tin_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should require tin', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin = null;

      const rules = requireRules();
      const rule = rules.rules.tin_length;
      const errorCode = 'A1';
      const type = rulesConfig.tin_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should properly check if tin value is valid', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin = '123456789';
      const cache = {
        providerValidTins: {
          validTinPrvdr: '123456789',
          validTinsReasgnmt: [],
          validTinsEmplr: [],
          deleted: false
        }
      };

      const rules = requireRules();
      const rule = rules.rules.tin_invalid;
      const errorCode = 'A1';
      const type = rulesConfig.tin_invalid;
      const result = rule(provider, errorCode, type, cache);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });
  });

  describe('npi', () => {
    it('should properly check npi', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.npi = '0001234567';

      const rules = requireRules();
      const rule = rules.rules.npi_length;
      const errorCode = 'A1';
      const type = rulesConfig.npi_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should require npi', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.npi = null;

      const rules = requireRules();
      const rule = rules.rules.npi_length;
      const errorCode = 'A1';
      const type = rulesConfig.npi_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should check if the npi value is valid', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.npi = '1234567890';
      const cache = {
        providerValidNpis: {
          npi: '1234567890',
          validTinPrvdr: '123456789',
          validTinsReasgnmt: ['123456789'],
          validTinsEmplr: ['123456789'],
          deleted: false
        }
      };

      const rules = requireRules();
      const rule = rules.rules.npi_invalid;
      const errorCode = 'A1';
      const type = rulesConfig.npi_invalid;
      const result = rule(provider, errorCode, type, cache);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });
  });

  describe('ccn', () => {
    it('should allow a valid ccn', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.ccn = '123456';

      const rules = requireRules();
      const rule = rules.rules.ccn_length;
      const errorCode = 'A1';
      const type = rulesConfig.ccn_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should allow ccn to be null', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.ccn = null;

      const rules = requireRules();
      const rule = rules.rules.ccn_length;
      const errorCode = 'A1';
      const type = rulesConfig.ccn_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should validate ccn', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.ccn = 'XXXX';

      const rules = requireRules();
      const rule = rules.rules.ccn_length;
      const errorCode = 'A1';
      const type = rulesConfig.ccn_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });
  });

  describe('tin_type_code', () => {
    it('should allow a valid tin_type_code - EIN', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin_type_code = 'EIN';

      const rules = requireRules();
      const rule = rules.rules.tin_type_code_value;
      const errorCode = 'A1';
      const type = rulesConfig.tin_type_code_value;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('SOFT_VALIDATION');
    });

    it('should allow a valid tin_type_code - ITIN', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin_type_code = 'ITIN';

      const rules = requireRules();
      const rule = rules.rules.tin_type_code_value;
      const errorCode = 'A1';
      const type = rulesConfig.tin_type_code_value;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('SOFT_VALIDATION');
    });

    it('should allow a valid tin_type_code - SSN', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin_type_code = 'SSN';

      const rules = requireRules();
      const rule = rules.rules.tin_type_code_value;
      const errorCode = 'A1';
      const type = rulesConfig.tin_type_code_value;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('SOFT_VALIDATION');
    });

    it('should allow tin_type_code to be null', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin_type_code = null;

      const rules = requireRules();
      const rule = rules.rules.tin_type_code_value;
      const errorCode = 'A1';
      const type = rulesConfig.tin_type_code_value;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('SOFT_VALIDATION');
    });

    it('should validate tin_type_code', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin_type_code = 'XXXX';

      const rules = requireRules();
      const rule = rules.rules.tin_type_code_value;
      const errorCode = 'A1';
      const type = rulesConfig.tin_type_code_value;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('SOFT_VALIDATION');
    });
  });

  describe('tin_npi_valid', () => {
    it('should be valid if the provider is an employee of the tin', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin = '123456789';

      const providerValidTins = {
        npi: null,
        validTinPrvdr: '123456789',
        validTinsEmplr: [],
        validTinsReasgnmt: [],
        deleted: false
      };

      const providerValidNpis = {
        npi: '23456789012',
        validTinPrvdr: null,
        validTinsEmplr: ['123456789'],
        validTinsReasgnmt: ['123456789'],
        deleted: false
      };

      const cache = {
        providerValidTins,
        providerValidNpis
      };

      const rules = requireRules();
      const rule = rules.rules.tin_npi_valid;
      const errorCode = 'A1';
      const type = rulesConfig.tin_npi_valid;
      const result = rule(provider, errorCode, type, cache);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });

    it('should require tin', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin = null;

      const rules = requireRules();
      const rule = rules.rules.tin_length;
      const errorCode = 'A1';
      const type = rulesConfig.tin_length;
      const result = rule(provider, errorCode, type);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should properly check if tin value is valid', () => {
      const provider = CreateProvider();
      mocks.apm_provider = Model(provider);
      provider.tin = '123456789';
      const cache = {
        providerValidTins: {
          validTinPrvdr: '123456789',
          validTinsReasgnmt: [],
          validTinsEmplr: [],
          deleted: false
        }
      };

      const rules = requireRules();
      const rule = rules.rules.tin_invalid;
      const errorCode = 'A1';
      const type = rulesConfig.tin_invalid;
      const result = rule(provider, errorCode, type, cache);
      expect(result).to.not.be.undefined;
      expect(result.isValid).to.equal(true);
    });
  });
});
