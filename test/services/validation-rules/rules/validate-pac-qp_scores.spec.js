const moment = require('moment');
const chai = require('chai');

const expect = chai.expect;
const CreateEntity = require('../../../mocks/data/apm_entity');
const createQpcScore = require('../../../mocks/data/pac-qpc_score');

const qpcScoreRules = require('../../../../app/services/validation/rules/qpc_score').rules;

const executeRule = (model, rule, deps, overrides = {}) => {
  const type = overrides.type || 'HARD_VALIDATION';
  const code = overrides.code || 'A1';
  return rule(model, code, type, deps);
};

describe('Validations for QPC Score files', () => {
  it('should pass all validations for manual bene with valid values', () => {
    const qpcScore = createQpcScore({
      entity_id: 'abc123',
      measure_id: 'ABCD0017',
      measure_score: 0.798,
      decile_score: 8.8,
      meets_scoring_criteria: 'Y',
      high_priority_measure: 'Y',
      cehrt_measure: 'Y'
    });
    const entity = CreateEntity({
      id: 'abc123',
      start_date: moment.utc().subtract(10, 'days'),
      end_date: moment.utc().add(10, 'days')
    });
    const allRulesPassed = Object.keys(qpcScoreRules)
      .map((key) => {
        const result = executeRule(qpcScore, qpcScoreRules[key], {
          entity
        });
        return result.isValid || key.ruleName;
      })
      .every(rulePassed => rulePassed === true);

    // eslint-disable-next-line
    expect(allRulesPassed).to.equal(true);
  });

  it('should pass validation with an entity_id', () => {
    const qpcScore = createQpcScore({ entity_id: 'abc123' });
    const result = executeRule(qpcScore, qpcScoreRules.entity_id_required);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation without an entity_id', () => {
    const qpcScore = createQpcScore({ entity_id: null });
    const result = executeRule(qpcScore, qpcScoreRules.entity_id_required);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation with an measure_id', () => {
    const qpcScore = createQpcScore({ measure_id: 'abc123' });
    const result = executeRule(qpcScore, qpcScoreRules.measure_id_required);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation without a measure_id', () => {
    const qpcScore = createQpcScore({ measure_id: '' });
    const result = executeRule(qpcScore, qpcScoreRules.measure_id_required);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation with an measure_score', () => {
    const qpcScore = createQpcScore({ measure_score: '0.873' });
    const result = executeRule(qpcScore, qpcScoreRules.measure_score_required);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation without a measure_score', () => {
    const qpcScore = createQpcScore({ measure_score: '' });
    const result = executeRule(qpcScore, qpcScoreRules.measure_score_required);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if measure_score is a number', () => {
    const qpcScore = createQpcScore({ measure_score: '0.873' });
    const result = executeRule(qpcScore, qpcScoreRules.measure_score_required);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if measure_score is not a number', () => {
    const qpcScore = createQpcScore({ measure_score: 'test' });
    const result = executeRule(qpcScore, qpcScoreRules.measure_score_required);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation with a decile_score', () => {
    const qpcScore = createQpcScore({ decile_score: '9.5' });
    const result = executeRule(qpcScore, qpcScoreRules.decile_score_required);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation without a decile_score', () => {
    const qpcScore = createQpcScore({ decile_score: '' });
    const result = executeRule(qpcScore, qpcScoreRules.decile_score_required);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if decile_score is a number', () => {
    const qpcScore = createQpcScore({ decile_score: '9.5' });
    const result = executeRule(qpcScore, qpcScoreRules.decile_score_required);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if decile_score is not a number', () => {
    const qpcScore = createQpcScore({ decile_score: 'test' });
    const result = executeRule(qpcScore, qpcScoreRules.decile_score_required);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if meets_scoring_criteria is a Y', () => {
    const qpcScore = createQpcScore({ meets_scoring_criteria: 'Y' });
    const result = executeRule(qpcScore, qpcScoreRules.meets_scoring_criteria_valid);

    expect(result.isValid).to.equal(true);
  });

  it('should pass validation if meets_scoring_criteria is a N', () => {
    const qpcScore = createQpcScore({ meets_scoring_criteria: 'N' });
    const result = executeRule(qpcScore, qpcScoreRules.meets_scoring_criteria_valid);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if meets_scoring_criteria is not a Y or N', () => {
    const qpcScore = createQpcScore({ meets_scoring_criteria: 'A' });
    const result = executeRule(qpcScore, qpcScoreRules.meets_scoring_criteria_valid);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if high_priority_measure is a Y', () => {
    const qpcScore = createQpcScore({ high_priority_measure: 'Y' });
    const result = executeRule(qpcScore, qpcScoreRules.high_priority_measure_valid);

    expect(result.isValid).to.equal(true);
  });

  it('should pass validation if high_priority_measure is a N', () => {
    const qpcScore = createQpcScore({ high_priority_measure: 'N' });
    const result = executeRule(qpcScore, qpcScoreRules.high_priority_measure_valid);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if high_priority_measure is not a Y or N', () => {
    const qpcScore = createQpcScore({ high_priority_measure: 'A' });
    const result = executeRule(qpcScore, qpcScoreRules.high_priority_measure_valid);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if cehrt_measure is a Y', () => {
    const qpcScore = createQpcScore({ cehrt_measure: 'Y' });
    const result = executeRule(qpcScore, qpcScoreRules.cehrt_measure_valid);

    expect(result.isValid).to.equal(true);
  });

  it('should pass validation if cehrt_measure is a N', () => {
    const qpcScore = createQpcScore({ cehrt_measure: 'N' });
    const result = executeRule(qpcScore, qpcScoreRules.cehrt_measure_valid);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if cehrt_measure is not a Y or N', () => {
    const qpcScore = createQpcScore({ cehrt_measure: 'A' });
    const result = executeRule(qpcScore, qpcScoreRules.cehrt_measure_valid);

    expect(result.isValid).to.equal(false);
  });


});
