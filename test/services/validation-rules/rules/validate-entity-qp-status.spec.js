const chai = require('chai');

const expect = chai.expect;

const qpRulesModule = require('../../../../app/services/validation/rules/entity_qp_status');
const rulesConfiguration = require('../../../../app/services/validation/rules/config/entity_qp_status');

const mockData = {
  npi: '1234567890',
  qp_status: 'Y',
  performance_year: 2017,
  run_snapshot: 'I'
};

const mockCache = {
  pacProviderNpi: {
    performance_year: 2017,
    npi: '1234567890'
  },
  mipsValidations: {
    npi: { id: 1 }
  }
};

describe('Service: validations/rules/entity_qp_status', () => {
  it('should contain a list of rules', () => {
    const rules = qpRulesModule.rules;
    expect(Object.keys(rules).length).to.be.greaterThan(0);
  });

  const rules = qpRulesModule.rules;
  const rulesConfig = rulesConfiguration.filter(r => !!r.isActive);
  const inactiveRulesConfig = rulesConfiguration.filter(r => !r.isActive);

  rulesConfig.forEach((ruleInfo) => {
    describe(ruleInfo.ruleName, () => {
      it('should execute properly', () => {
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockData, ruleInfo.description, ruleInfo.type, mockCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.true;
      });

      it('should fail tests', () => {
        const mockFailData = {
          npi: '12345679',
          qp_status: 'z',
          performance_year: 2018,
          run_snapshot: 'I'
        };

        const mockFailCache = {
          pacProviderNpi: {
            performance_year: 2017,
            npi: '1234567890'
          },
          mipsValidations: {}
        };
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockFailData, ruleInfo.description, ruleInfo.type, mockFailCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.false;
      });

      it('should fail tests if no cache is found', () => {
        const mockFailData = {
          npi: '123456790',
          qp_status: 'z',
          performance_year: 2018,
          run_snapshot: 'I'
        };

        const mockFailCache = {
          pacProviderNpi: null,
          mipsValidations: {

          }
        };
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockFailData, ruleInfo.description, ruleInfo.type, mockFailCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.false;
      });
    });
  });

  inactiveRulesConfig.forEach((ruleInfo) => {
    describe(`inactive rule: ${ruleInfo.ruleName}`, () => {
      it('should execute properly', () => {
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockData, ruleInfo.description, ruleInfo.type, mockCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.true;
      });
    });
  });
});
