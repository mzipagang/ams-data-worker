const moment = require('moment');
const chai = require('chai');

const expect = chai.expect;
const CreateEntity = require('../../../mocks/data/apm_entity');
const CreateModel = require('../../../mocks/data/pac_model');
const createBeneficiary = require('../../../mocks/data/beneficiary');

const beneficiaryRules = require('../../../../app/services/validation/rules/beneficiary').rules;
const manualBeneRules = require('../../../../app/services/validation/rules').beneficiary;
const mdmBeneRules = require('../../../../app/services/validation/rules').beneficiary_mdm;

const allManualBeneRules = manualBeneRules.hardValidations.concat(manualBeneRules.softValidations);
const allMdmBeneRules = mdmBeneRules.hardValidations.concat(mdmBeneRules.softValidations);
const executeRule = (model, rule, deps, overrides = {}) => {
  const type = overrides.type || 'HARD_VALIDATION';
  const code = overrides.code || 'A1';
  return rule(model, code, type, deps);
};

describe('Validations for beneficiary files', () => {
  describe('Manual beneficiary files', () => {
    it('should pass all validations for manual bene with valid values', () => {
      const beneficiary = createBeneficiary({
        entity_id: 'abc123',
        start_date: moment.utc(),
        end_date: moment.utc().add(1, 'days'),
        link_key: '1234567890',
        hicn: '1234567890a',
        mbi: '1234567890a',
        dob: moment.utc(),
        gender: '1',
        program_org_id: null
      });
      const entity = CreateEntity({
        id: 'abc123',
        start_date: moment.utc().subtract(10, 'days'),
        end_date: moment.utc().add(10, 'days')
      });
      const failingRules = allManualBeneRules
        .map((key) => {
          const result = executeRule(beneficiary, beneficiaryRules[key.ruleName], {
            entity
          });
          return result.isValid || key.ruleName;
        })
        .filter(rule => rule !== true);

      // eslint-disable-next-line
      expect(failingRules).to.be.empty;
    });

    it('should pass validation with an entity_id', () => {
      const beneficiary = createBeneficiary({ entity_id: 'abc123' });
      const result = executeRule(beneficiary, beneficiaryRules.entity_id_required);

      expect(result.isValid).to.equal(true);
    });

    it('should fail validation without an entity_id', () => {
      const beneficiary = createBeneficiary({ entity_id: undefined });
      const result = executeRule(beneficiary, beneficiaryRules.entity_id_required, null, {
        type: 'HARD_VALIDATION',
        code: 'A2'
      });

      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A2');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should be valid if entity_id exists in system', () => {
      const beneficiary = createBeneficiary({ entity_id: 'abc123' });
      const entity = CreateEntity({ id: 'abc123' });
      const result = executeRule(beneficiary, beneficiaryRules.entity_id_valid, { entity });

      expect(result.isValid).to.equal(true);
    });

    it('should be invalid if entity_id does not exist in system', () => {
      const beneficiary = createBeneficiary({ entity_id: 'abc123' });
      const entity = CreateEntity({ id: 'xyz789' });

      const result = executeRule(beneficiary, beneficiaryRules.entity_id_valid, { entity });

      expect(result.isValid).to.equal(false);
    });
  });

  describe('MDM Beneficiary Extract File', () => {
    it('should pass all validations for mdm bene with valid values', () => {
      const beneficiary = createBeneficiary({
        program_org_id: 'abc123',
        program_id: '08',
        start_date: moment.utc(),
        end_date: moment.utc().add(1, 'days'),
        id: '1234567890',
        hicn: '1234567890a',
        mbi: '1234567890a',
        dob: moment.utc(),
        gender: '1',
        entity_id: null
      });
      const entity = CreateEntity({
        id: 'abc123',
        apm_id: '08',
        start_date: moment.utc().subtract(10, 'days'),
        end_date: moment.utc().add(10, 'days')
      });
      const model = CreateModel({
        id: '08'
      });
      const failingRules = allMdmBeneRules
        .map((key) => {
          const result = executeRule(beneficiary, beneficiaryRules[key.ruleName], {
            entity,
            model
          });
          return result.isValid || key.ruleName;
        })
        .filter(rule => rule !== true);

      // eslint-disable-next-line
      expect(failingRules).to.be.empty;
    });

    it('should pass validation with an program_id', () => {
      const beneficiary = createBeneficiary({ program_id: 'abc123' });
      const result = executeRule(beneficiary, beneficiaryRules.program_id_required);

      expect(result.isValid).to.equal(true);
    });

    it('should fail validation without an program_id', () => {
      const beneficiary = createBeneficiary({ program_id: undefined });
      const result = executeRule(beneficiary, beneficiaryRules.program_id_required, null, {
        type: 'HARD_VALIDATION',
        code: 'A1'
      });

      expect(result.isValid).to.equal(false);
      expect(result.errorCode).to.equal('A1');
      expect(result.validationType).to.equal('HARD_VALIDATION');
    });

    it('should be valid if program_id exists in system', () => {
      const beneficiary = createBeneficiary({ program_id: 'abc123' });
      const model = CreateModel({ id: 'abc123' });
      const result = executeRule(beneficiary, beneficiaryRules.program_id_valid, {
        model
      });

      expect(result.isValid).to.equal(true);
    });

    it('should be invalid if program_id does not exist in system', () => {
      const beneficiary = createBeneficiary({ program_id: 'abc123' });
      const model = CreateModel({ id: 'xyz789' });

      const result = executeRule(beneficiary, beneficiaryRules.program_id_valid, {
        model
      });

      expect(result.isValid).to.equal(false);
    });

    it('should pass validation with an id', () => {
      const beneficiary = createBeneficiary({ id: 'abc123' });
      const result = executeRule(beneficiary, beneficiaryRules.beneficiary_id_required);

      expect(result.isValid).to.equal(true);
    });

    it('should pass validation with an id length of 10', () => {
      const beneficiary = createBeneficiary({ id: '1234567890' });
      const result = executeRule(beneficiary, beneficiaryRules.beneficiary_id_length);

      expect(result.isValid).to.equal(true);
    });

    it('should fail validation with an id length not 10', () => {
      const beneficiary = createBeneficiary({ id: '12345678900' });
      const result = executeRule(beneficiary, beneficiaryRules.beneficiary_id_length);

      expect(result.isValid).to.equal(false);
    });

    it('should be valid if program_org_id exists in system', () => {
      const beneficiary = createBeneficiary({ program_org_id: 'abc123' });
      const entity = CreateEntity({ id: 'abc123' });
      const result = executeRule(beneficiary, beneficiaryRules.program_org_id_valid, {
        entity
      });

      expect(result.isValid).to.equal(true);
    });

    it('should be invalid if program_org_id does not exist in system', () => {
      const beneficiary = createBeneficiary({ program_org_id: 'abc123' });
      const entity = CreateEntity({ id: 'xyz789' });

      const result = executeRule(beneficiary, beneficiaryRules.program_org_id_valid, {
        entity
      });

      expect(result.isValid).to.equal(false);
    });
  });

  it('should pass validation with a start_date', () => {
    const beneficiary = createBeneficiary({ start_date: Date.now() });
    const result = executeRule(beneficiary, beneficiaryRules.start_date_required);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation without a start_date', () => {
    const beneficiary = createBeneficiary({ start_date: undefined });
    const result = executeRule(beneficiary, beneficiaryRules.start_date_required);

    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if start_date is invalid', () => {
    const beneficiary = createBeneficiary({ start_date: 'INVALID' });
    const result = executeRule(beneficiary, beneficiaryRules.start_date_valid_date);

    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if start_date is before end_date', () => {
    const beneficiary = createBeneficiary({
      start_date: moment(),
      end_date: moment().subtract(1, 'day')
    });
    const result = executeRule(beneficiary, beneficiaryRules.start_date_before_end_date);
    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if start_date is same as end_date', () => {
    const sameDate = moment();
    const beneficiary = createBeneficiary({
      start_date: sameDate,
      end_date: sameDate
    });
    const result = executeRule(beneficiary, beneficiaryRules.start_date_before_end_date);
    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if start_date is before entity start_date', () => {
    const beneficiary = createBeneficiary({ start_date: moment() });
    const entity = CreateEntity({ start_date: moment().add(1, 'days') });

    const result = executeRule(beneficiary, beneficiaryRules.start_date_after_entity_start_date, {
      entity
    });
    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if start_date is after entity end_date', () => {
    const beneficiary = createBeneficiary({ start_date: moment().add(1, 'days') });
    const entity = CreateEntity({ end_date: moment() });

    const result = executeRule(beneficiary, beneficiaryRules.start_date_before_entity_end_date, {
      entity
    });
    expect(result.isValid).to.equal(false);
  });

  it('should fail validation without an end_date', () => {
    const beneficiary = createBeneficiary({ end_date: undefined });
    const result = executeRule(beneficiary, beneficiaryRules.end_date_required);

    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if end_date is invalid', () => {
    const beneficiary = createBeneficiary({ end_date: 'INVALID' });
    const result = executeRule(beneficiary, beneficiaryRules.end_date_valid_date);

    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if end_date is before start_date', () => {
    const beneficiary = createBeneficiary({
      start_date: moment(),
      end_date: moment().subtract(1, 'day')
    });
    const result = executeRule(beneficiary, beneficiaryRules.end_date_after_start_date);
    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if end_date is after entity end_date', () => {
    const beneficiary = createBeneficiary({ end_date: moment().add(1, 'day') });
    const entity = CreateEntity({ end_date: moment() });
    const result = executeRule(beneficiary, beneficiaryRules.end_date_before_entity_end_date, {
      entity
    });
    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if end_date is before entity start_date', () => {
    const beneficiary = createBeneficiary({ end_date: moment().subtract(1, 'day') });
    const entity = CreateEntity({ start_date: moment() });
    const result = executeRule(beneficiary, beneficiaryRules.end_date_after_entity_start_date, {
      entity
    });
    expect(result.isValid).to.equal(false);
  });

  it('should fail validation without a link_key', () => {
    const beneficiary = createBeneficiary({ link_key: undefined });
    const result = executeRule(beneficiary, beneficiaryRules.bene_link_key_required);

    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if link_key is not a length of 10 digits', () => {
    let beneficiary = createBeneficiary({ link_key: '123456789012' });
    let result = executeRule(beneficiary, beneficiaryRules.bene_link_key_length);

    expect(result.isValid).to.equal(false);

    beneficiary = createBeneficiary({ link_key: 'abcdefghijh' });
    result = executeRule(beneficiary, beneficiaryRules.bene_link_key_length);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if hicn is 11 alphanumeric characters', () => {
    const beneficiary = createBeneficiary({ hicn: '12345678901' });
    const result = executeRule(beneficiary, beneficiaryRules.hicn_length);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if hicn is not a length of 11 alphanumeric characters', () => {
    const beneficiary = createBeneficiary({ hicn: '123456890123' });
    const result = executeRule(beneficiary, beneficiaryRules.hicn_length);

    expect(result.isValid).to.equal(false);
  });

  it('should pass validation if mbi length of 11 alphanumeric characters', () => {
    const beneficiary = createBeneficiary({ mbi: '12345678900' });
    const result = executeRule(beneficiary, beneficiaryRules.mbi_length);

    expect(result.isValid).to.equal(true);
  });

  it('should fail validation if mbi is not a length of 11 alphanumeric characters', () => {
    const beneficiary = createBeneficiary({ mbi: '1234567890123' });
    const result = executeRule(beneficiary, beneficiaryRules.mbi_length);

    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if dob is not valid', () => {
    const beneficiary = createBeneficiary({ dob: null });
    const result = executeRule(beneficiary, beneficiaryRules.dob_valid);

    expect(result.isValid).to.equal(false);
  });

  it('should fail validation if gender is not 0, 1, or 2', () => {
    const beneficiary = createBeneficiary({ gender: 'A' });
    const result = executeRule(beneficiary, beneficiaryRules.gender_valid);

    expect(result.isValid).to.equal(false);
  });
});
