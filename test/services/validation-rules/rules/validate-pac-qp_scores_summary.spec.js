const moment = require('moment');
const chai = require('chai');

const expect = chai.expect;
const CreateEntity = require('../../../mocks/data/apm_entity');
const createQpcScoreSummary = require('../../../mocks/data/pac-qpc_score_summary');

const qpcScoreSummaryRules = require('../../../../app/services/validation/rules/qpc_score_summary').rules;

const executeRule = (model, rule, deps, overrides = {}) => {
  const type = overrides.type || 'HARD_VALIDATION';
  const code = overrides.code || 'A1';
  return rule(model, code, type, deps);
};

describe('Validations for QPC Score Summary files', () => {
  it('should pass all validations for manual bene with valid values', () => {
    const qpcScoreSummary = createQpcScoreSummary({
      entity_id: 'abc123',
      final_qpc_score: 123.1,
      current_achievement_points: 123.1,
      prior_achievement_points: 123.1,
      high_priority_bonus_points: 5,
      cehrt_reporting_bonus: 5,
      quality_improvement_points: 5,
      scored_measures: 5,
      excluded_measures: 5,
      excluded_measures_pay_for_reporting: 5,
      excluded_measures_case_size: 5,
      file_id: '123',
    });
    const entity = CreateEntity({
      id: 'abc123',
      start_date: moment.utc().subtract(10, 'days'),
      end_date: moment.utc().add(10, 'days')
    });
    const allRulesPassed = Object.keys(qpcScoreSummaryRules)
      .map((key) => {
        const result = executeRule(qpcScoreSummary, qpcScoreSummaryRules[key], {
          entity
        });
        return result.isValid || key.ruleName;
      })
      .every(rulePassed => rulePassed === true);

    // eslint-disable-next-line
    expect(allRulesPassed).to.equal(true);
  });

});
