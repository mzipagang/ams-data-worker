const chai = require('chai');

const expect = chai.expect;

const qpRulesModule = require('../../../../app/services/validation/rules/entity_qp_threshold');
const rulesConfiguration = require('../../../../app/services/validation/rules/config/entity_qp_threshold');

const mockData = {
  apm_id: '1234567890',
  subdivision_id: '1234567890',
  entity_id: '1234567890',
  tin: '000002048',
  tinDecrypted: '123456789',
  npi: '1234567890',
  qp_status: 'Y',
  expenditures_numerator: 0,
  expenditures_denominator: 0,
  payment_threshold_score: 0,
  payment_threshold_met: 'Y',
  beneficiaries_numerator: 0,
  beneficiaries_denominator: 0,
  patient_threshold_score: 0,
  patient_threshold_met: 'Y',
  performance_year: 2017,
  run_snapshot: 'I'
};

const mockCache = {
  pacModel: {
    performance_year: 2017,
    id: '1234567890',
    mips_apm_flag: 'Y'
  },
  pacSubdivision: {
    performance_year: 2017,
    id: '1234567890',
    mips_apm_flag: 'P'
  },
  pacEntity: {
    performance_year: 2017,
    id: '1234567890'
  },
  pacProviderTin: {
    performance_year: 2017,
    tin: '000002048'
  },
  pacProviderNpi: {
    performance_year: 2017,
    npi: '1234567890'
  },
  mipsValidations: {
    entity: { id: 1 },
    npi: { id: 1 },
    tin: { id: 1 }
  }
};


describe('Service: validations/rules/entity_qp_threshold', () => {
  it('should contain a list of rules', () => {
    const rules = qpRulesModule.rules;
    expect(Object.keys(rules).length).to.be.greaterThan(0);
  });

  const rules = qpRulesModule.rules;
  const rulesConfig = rulesConfiguration.filter(r => !!r.isActive);
  const inactiveRulesConfig = rulesConfiguration.filter(r => !r.isActive);

  rulesConfig.forEach((ruleInfo) => {
    describe(ruleInfo.ruleName, () => {
      it('should execute properly', () => {
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockData, ruleInfo.description, ruleInfo.type, mockCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.true;
      });

      it('should fail tests', () => {
        const mockFailData = {
          apm_id: '01',
          subdivision_id: '01',
          entity_id: 'A0001',
          tin: '1234567890',
          npi: '123456789',
          qp_status: 'A',
          expenditures_numerator: 0,
          expenditures_denominator: 0,
          payment_threshold_score: 0,
          payment_threshold_met: 'B',
          beneficiaries_numerator: 0,
          beneficiaries_denominator: 0,
          patient_threshold_score: 0,
          patient_threshold_met: 'C',
          performance_year: 2018,
          run_snapshot: ''
        };

        const mockFailCache = {
          pacModel: {
            performance_year: 2017,
            id: '1234567890',
            mips_apm_flag: 'N'
          },
          pacSubdivision: {
            performance_year: 2017,
            id: '1234567890',
            mips_apm_flag: 'N'
          },
          pacEntity: {
            performance_year: 2017,
            id: '1234567890'
          },
          pacProviderTin: {
            performance_year: 2017,
            tin: '1234567899'
          },
          pacProviderNpi: {
            performance_year: 2017,
            npi: '1234567890'
          },
          mipsValidations: {

          }
        };
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockFailData, ruleInfo.description, ruleInfo.type, mockFailCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.false;
      });

      it('should fail tests if no cache is found', () => {
        const mockFailData = {
          apm_id: '01',
          subdivision_id: '01',
          entity_id: 'A0001',
          tin: '1234567890',
          npi: '123456789',
          qp_status: 'A',
          expenditures_numerator: 0,
          expenditures_denominator: 0,
          payment_threshold_score: 0,
          payment_threshold_met: 'B',
          beneficiaries_numerator: 0,
          beneficiaries_denominator: 0,
          patient_threshold_score: 0,
          patient_threshold_met: 'C',
          performance_year: 2018,
          run_snapshot: ''
        };

        const mockFailCache = {
          pacModel: null,
          pacSubdivision: null,
          pacEntity: null,
          pacProviderTin: null,
          pacProviderNpi: null,
          mipsValidations: {

          }
        };
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockFailData, ruleInfo.description, ruleInfo.type, mockFailCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.false;
      });
    });
  });

  inactiveRulesConfig.forEach((ruleInfo) => {
    describe(`inactive rule: ${ruleInfo.ruleName}`, () => {
      it('should execute properly', () => {
        const rule = rules[ruleInfo.ruleName];
        const result = rule(mockData, ruleInfo.description, ruleInfo.type, mockCache);
        expect(result).to.not.be.undefined;
        expect(result.isValid).to.be.true;
      });
    });
  });
});
