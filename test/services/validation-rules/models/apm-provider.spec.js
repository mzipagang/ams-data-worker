const ApmEntity = require('../../../../app/services/mongoose/models/apm_entity');
const IDRProviderValidTins = require('../../../../app/services/mongoose/models/provider_valid_tins');
const validateProviderList = require('../../../../app/services/validation/models/apm_provider').validateList;
const validate = require('../../../../app/services/validation/').validate;
const environment = require('../../../environment');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');
const moment = require('moment');

const expect = chai.expect;

const createMockProvider = require('../../../mocks/data/apm_provider');

describe('Service: validations/providers', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('validateList()', () => {
    let entityMock;
    let entity;
    let providerValidTins;

    before(async () => {
      entity = await new ApmEntity({
        name: 'entity 01',
        apm_id: '01',
        id: '0000-0000-0000-0000',
        tin: '123456789',
        npi: '1234567890',
        start_date: moment.utc()
          .add(-3, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }).save();

      providerValidTins = await new IDRProviderValidTins({
        npi: '1234567890',
        validTinPrvdr: '123456789 ',
        validTinsEmplr: ['123456789'],
        validTinsReasgnmt: ['123456789'],
        deleted: false
      }).save();
    });

    after(async () => {
      await ApmEntity.findOneAndDelete({ _id: entity._id });
      await IDRProviderValidTins.findOneAndDelete({ _id: providerValidTins._id });
    });

    it('should successfully validate a list of providers', () => {
      const providers = [{
        apm_id: '21',
        entity_id: '0000-0000-0000-0000',
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(1, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        tin: '123456789',
        npi: '1234567890',
        ccn: '123456',
        tin_type_code: 'SSN',
        specialty_code: '',
        organization_name: '',
        first_name: '',
        middle_name: '',
        last_name: '',
        participant_type_code: ''
      }];
      return validateProviderList(providers, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.false;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.equal(0);
        });
    });

    it('should handle providers that fail validation', () => {
      const mocks = [createMockProvider()];
      mocks[0].npi = '0000000000';
      mocks[0].start_date = null;
      mocks[0].end_date = null;

      return validateProviderList(mocks, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.true;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.be.greaterThan(1);
        });
    });

    it('should handle errors in validating', (done) => {
      entityMock = sinon.mock(ApmEntity);
      entityMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .rejects(new Error('Fake Error'));

      validateProviderList([], validate)
        .catch((err) => {
          expect(err).to.not.be.undefined;
          expect(err.message).to.equal('Fake Error');
          entityMock.restore();
          done();
        });
    });

    it('should accept 9999-12-31', () => {
      const providers = [{
        apm_id: '21',
        entity_id: '0000-0000-0000-0000',
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc('9999-12-31'),
        tin: '000002048',
        npi: 'xxxxxxxxxx',
        ccn: '123456',
        tin_type_code: 'SSN',
        specialty_code: '',
        organization_name: '',
        first_name: '',
        middle_name: '',
        last_name: '',
        participant_type_code: ''
      }];
      return validateProviderList(providers, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
        });
    });
  });
});
