const chai = require('chai');
const validation = require('../../../../app/services/validation/models/individual_qp_threshold');
const DynamicPacProvider = require('../../../../app/services/mongoose/dynamic_name_models/pac_provider');
const pacPublishHistoryUtils = require('../../../../app/services/pacPublishHistoryUtils');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const individualThresholds = [{
  _id: '',
  npi: '1234567890'
}];

const validateFn = () => ({
  isValid: true,
  hardValidationFails: [],
  softValidationFails: []
});

const DynamicNamePacProvider = DynamicPacProvider('1');

describe('Service: validations/individual-qp-threshold', () => {
  let ProviderMock;
  let PacPublishHistoryUtilsMock;

  beforeEach(() => {
    ProviderMock = sinon.mock(DynamicNamePacProvider);
    ProviderMock
      .expects('find')
      .chain('lean')
      .once()
      .resolves([{}]);
    PacPublishHistoryUtilsMock = sinon.mock(pacPublishHistoryUtils);
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .resolves({ pacProviderFile: { file_id: '1' } });
  });

  afterEach(() => {
    ProviderMock.restore();
    PacPublishHistoryUtilsMock.restore();
  });

  it('should load properly', () => {
    expect(validation.validateList).to.not.be.undefined;
  });

  it('should get providers from apm_provider collection', () =>
    validation.validateList(individualThresholds, validateFn)
      .then(() => {
        ProviderMock.verify();
      }));

  it('should call validation function', () => {
    const validateFnSpy = sinon.spy(validateFn);
    return validation.validateList(individualThresholds, validateFnSpy)
      .then(() => {
        expect(validateFnSpy).to.have.been.called;
      });
  });

  it('should return a list of validation results', () => validation.validateList(individualThresholds, validateFn)
    .then((results) => {
      expect(results).to.not.be.undefined;
      expect(results.length).to.equal(1);
    }));

  it('should return an empty array', () => validation.validateList([], validateFn)
    .then((results) => {
      expect(results.length).to.equal(0);
    }));
});
