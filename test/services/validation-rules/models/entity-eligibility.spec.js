require('../../../../app/services/mongoose');
const DynamicPACEntity = require('../../../../app/services/mongoose/dynamic_name_models/pac_entity');
const DynamicPacSubdivision = require('../../../../app/services/mongoose/dynamic_name_models/pac_subdivision');
const DynamicPacModel = require('../../../../app/services/mongoose/dynamic_name_models/pac_model');
const { validateList } = require('../../../../app/services/validation/models/entity_eligibility');
const validate = require('../../../../app/services/validation').validate;
const environment = require('../../../environment');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');
const pacPublishHistoryUtils = require('../../../../app/services/pacPublishHistoryUtils');

const expect = chai.expect;

const mockMongo = {
  find: () => mockMongo,
  lean: () => mockMongo,
  update: () => mockMongo,
  execute: (cb) => {
    cb(null, []);
  },
  eachAsync: cb => Promise.resolve([].map(data => cb(data))),
  close: cb => cb(),
  collection: {
    initializeUnorderedBulkOp: () => mockMongo
  }
};

const DynamicPACNameEntity = DynamicPACEntity('1');

describe('Service: validations/entity-eligibility', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('validateList()', () => {
    let entityMock;
    let PacPublishHistoryUtilsMock;
    let dynamicPacModelMock;
    let dynamicPacSubdivisionMock;


    beforeEach(() => {
      entityMock = sinon.mock(DynamicPACNameEntity);
      dynamicPacModelMock = sinon.mock(DynamicPacModel('1'));
      dynamicPacSubdivisionMock = sinon.mock(DynamicPacSubdivision('1'));
      entityMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          id: '0000-0000-0000-0000',
          tin: 'XXXXXXXXX',
          npi: 'XXXXXXXXXX',
          performance_year: 2017,
          subdiv_id: '0000-0000-0000-0000',
          apm_id: '0000-0000-0000-0000'
        }]);
      PacPublishHistoryUtilsMock = sinon.mock(pacPublishHistoryUtils);
      PacPublishHistoryUtilsMock
        .expects('getLatestPacFilesByPerformanceYear')
        .resolves({
          pacEntityFile: { file_id: '1' },
          pacModelFile: { file_id: '1' },
          pacSubdivisionFile: { file_id: '1' }
        });
      dynamicPacModelMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          performance_year: 2017,
          id: '0000-0000-0000-0000',
          mips_apm_flag: 'Y'
        }]);
      dynamicPacSubdivisionMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          performance_year: 2017,
          id: '0000-0000-0000-0000',
          mips_apm_flag: 'Y'
        }]);
    });

    afterEach(() => {
      entityMock.restore();
      PacPublishHistoryUtilsMock.restore();
      dynamicPacModelMock.restore();
      dynamicPacSubdivisionMock.restore();
    });

    it('should successfully validate a list of entity eligibilities', () => {
      const entityElig = [{
        entity_id: '0000-0000-0000-0000',
        claims_types: 'PHYS',
        lv_status_code: 'Y',
        lv_status_reason: 'BENE',
        lv_part_b_expenditures: 14641275.95,
        lv_bene_count: 19259,
        small_status_apm_entity_code: 'N',
        small_status_clinician_count: 101,
        performance_year: 2017,
        run_number: '2',
        complex_patient_score: 1
      }];
      return validateList(entityElig, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.false;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.equal(0);
        });
    });

    it('should still validate if no performance year is given', () => {
      const entityElig = [{
        entity_id: '0000-0000-0000-0000',
        claims_types: 'PHYS',
        lv_status_code: 'Y',
        lv_status_reason: 'BENE',
        lv_part_b_expenditures: 14641275.95,
        lv_bene_count: 19259,
        small_status_apm_entity_code: 'N',
        small_status_clinician_count: 101,
        performance_year: null,
        run_number: '2',
        complex_patient_score: 1
      }];
      return validateList(entityElig, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.true;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.be.greaterThan(1);
        });
    });

    it('should still validate if no files are found', () => {
      PacPublishHistoryUtilsMock.restore();
      PacPublishHistoryUtilsMock = sinon.mock(pacPublishHistoryUtils);
      PacPublishHistoryUtilsMock
        .expects('getLatestPacFilesByPerformanceYear')
        .resolves({ pacEntityFile: null, pacModelFile: null, pacSubdivisionFile: null });
      const entityElig = [{
        entity_id: '0000-0000-0000-0000',
        claims_types: 'PHYS',
        lv_status_code: 'Y',
        lv_status_reason: 'BENE',
        lv_part_b_expenditures: 14641275.95,
        lv_bene_count: 19259,
        small_status_apm_entity_code: 'N',
        small_status_clinician_count: 101,
        performance_year: null,
        run_number: '2',
        complex_patient_score: 1
      }];
      return validateList(entityElig, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.true;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.be.greaterThan(1);
        });
    });

    it('should handle entity eligibilities that fail validation', () => {
      const mocks = [{
        entity_id: '0000-0000-0000-0000',
        claims_types: 'PHYS',
        lv_status_code: 'Y',
        lv_status_reason: 'BENE',
        lv_part_b_expenditures: 14641275.95,
        lv_bene_count: 'test',
        small_status_apm_entity_code: 'N',
        small_status_clinician_count: 101,
        performance_year: 2016,
        run_number: '2',
        complex_patient_score: 1
      }];

      return validateList(mocks, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.true;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.be.at.least(1);
        });
    });

    it('should handle errors in validating', (done) => {
      entityMock.restore();
      entityMock = sinon.mock(DynamicPACNameEntity);
      entityMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .rejects(new Error('Fake Error'));
      const mocks = [{
        entity_id: '0000-0000-0000-0000',
        claims_types: 'PHYS',
        lv_status_code: 'Y',
        lv_status_reason: 'BENE',
        lv_part_b_expenditures: 14641275.95,
        lv_bene_count: 19259,
        small_status_apm_entity_code: 'N',
        small_status_clinician_count: 101,
        performance_year: 2018,
        run_number: '2',
        complex_patient_score: 1
      }];
      validateList(mocks, validate)
        .catch((err) => {
          expect(err).to.not.be.undefined;
          expect(err.message).to.equal('Fake Error');
          done();
        });
    });
  });
});
