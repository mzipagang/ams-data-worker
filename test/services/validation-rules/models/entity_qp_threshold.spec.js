const EntityQpThreshold = require('../../../../app/services/mongoose/models/entity_qp_threshold');
const validateList = require('../../../../app/services/validation/models/entity_qp_threshold').validateList;
const pacPublishHistoryUtils = require('../../../../app/services/pacPublishHistoryUtils');
const DynamicPacModel = require('../../../../app/services/mongoose/dynamic_name_models/pac_model');
const DynamicPacEntity = require('../../../../app/services/mongoose/dynamic_name_models/pac_entity');
const DynamicPacProvider = require('../../../../app/services/mongoose/dynamic_name_models/pac_provider');
const DynamicPacSubdivision = require('../../../../app/services/mongoose/dynamic_name_models/pac_subdivision');
const environment = require('../../../environment');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const mockMongo = {
  find: () => mockMongo,
  lean: () => mockMongo,
  update: () => mockMongo,
  close: cb => cb(),
  eachAsync: cb => Promise.resolve([].map(data => cb(data))),
  execute: (cb) => {
    cb(null, []);
  },
  collection: {
    initializeUnorderedBulkOp: () => mockMongo
  }
};

const CreateMock = require('../../../mocks/data/entity_qp_threshold');

describe('Service: validations/entity_qp_threshold', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('validateList()', () => {
    let modelMock;
    let pacPubMock;
    let dynamicPacModelMock;
    let dynamicPacEntityMock;
    let dynamicPacProviderMock;
    let dynamicPacSubdivisionMock;

    beforeEach(() => {
      modelMock = sinon.mock(EntityQpThreshold);
      dynamicPacModelMock = sinon.mock(DynamicPacModel('1'));
      dynamicPacEntityMock = sinon.mock(DynamicPacEntity('1'));
      dynamicPacProviderMock = sinon.mock(DynamicPacProvider('1'));
      dynamicPacSubdivisionMock = sinon.mock(DynamicPacSubdivision('1'));

      modelMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{ id: '0000-0000-0000-0000' }]);
      dynamicPacModelMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          performance_year: 2017,
          id: '1234567890'
        }]);
      dynamicPacEntityMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          performance_year: 2017,
          id: '1234567890',
          subdiv_id: '1234567890',
          apm_id: '1234567890'
        }]);
      dynamicPacProviderMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          performance_year: 2017,
          tin: '123456789',
          npi: '1234567890',
          entity_id: '1234567890'
        }]);
      dynamicPacSubdivisionMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          performance_year: 2017,
          id: '1234567890'
        }]);
    });

    afterEach(() => {
      modelMock.restore();
      dynamicPacModelMock.restore();
      dynamicPacEntityMock.restore();
      dynamicPacProviderMock.restore();
      dynamicPacSubdivisionMock.restore();
    });

    describe('Validations', () => {
      beforeEach(() => {
        pacPubMock = sinon.mock(pacPublishHistoryUtils);
        pacPubMock
          .expects('getLatestPacFilesByPerformanceYear')
          .atLeast(1)
          .resolves({
            pacProviderFile: { file_id: '1' },
            pacEntityFile: { file_id: '1' },
            pacSubdivisionFile: { file_id: '1' },
            pacModelFile: { file_id: '1' }
          });
      });

      afterEach(() => {
        pacPubMock.restore();
      });

      it('should validate', (done) => {
        const mocks = [CreateMock({
          apm_id: '1234567890',
          subdivision_id: '1234567890',
          entity_id: '1234567890',
          tin: '123456789',
          npi: '1234567890',
          qp_threshold: 'Y',
          performance_year: 2017,
          run_snapshot: 'I'
        })];
        validateList(mocks, () => ({ isValid: true, hardValidationFails: [], softValidationFails: [] }))
          .then((results) => {
            expect(results[0]).to.not.be.undefined;
            expect(results[0].update.invalid).to.be.false;
            done();
          });
      });
    });

    describe('Errors', () => {
      beforeEach(() => {
        pacPubMock = sinon.mock(pacPublishHistoryUtils);
        pacPubMock
          .expects('getLatestPacFilesByPerformanceYear')
          .atLeast(1)
          .resolves({});
      });

      afterEach(() => {
        pacPubMock.restore();
      });

      it('should handle errors in validating', (done) => {
        const mocks = [CreateMock()];
        validateList(mocks, () => {
          throw new Error('Fake Error');
        })
          .catch((err) => {
            expect(err).to.not.be.undefined;
            expect(err.message).to.equal('Fake Error');
            done();
          });
      });
    });
  });
});
