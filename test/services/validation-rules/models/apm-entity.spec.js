const PACModel = require('../../../../app/services/mongoose/models/pac_model');
const PACSubdivision = require('../../../../app/services/mongoose/models/pac_subdivision');
const EntitySourceCollection = require('../../../../app/services/mongoose/models/source_file_entity_data');
const IDRProviderValidTins = require('../../../../app/services/mongoose/models/provider_valid_tins');
const validateEntityList = require('../../../../app/services/validation/models/apm_entity').validateList;
const validate = require('../../../../app/services/validation').validate;
const environment = require('../../../environment');
const chai = require('chai');
const sinon = require('sinon');
const moment = require('moment');
require('sinon-mongoose');

const expect = chai.expect;

const CreateMockEntity = require('../../../mocks/data/apm_entity');

describe('Service: validations/entities', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('validateList()', () => {
    let modelMock;
    let providerValidTins;
    let subdivisionMock;
    let entitySourceCollectionMock;

    beforeEach(async () => {

      modelMock = await new PACModel({
        id: '000000000',
        name: 'modelName',
        quality_reporting_category_code: '4'
      }).save();
      subdivisionMock = await new PACSubdivision({
        name: 'subdivName',
        id: '000000000',
        apm_id: '000000000',
        start_date: moment.utc()
          .add(-2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate(),
        end_date: moment.utc()
          .add(2, 'month')
          .startOf('month')
          .startOf('day')
          .toDate()
      }).save();
      providerValidTins = await new IDRProviderValidTins({
        npi: '1234567890',
        validTinPrvdr: '000000000',
        validTinsEmplr: ['000000000'],
        validTinsReasgnmt: ['000000000'],
        deleted: false
      }).save();
      entitySourceCollectionMock = await new EntitySourceCollection({
        file_id: '12',
        file_line_number: 1,
        id: 60,
        apm_id: 55,
        subdiv_id: 45,
        start_date: Date(),
        end_date: Date(),
        name: 'name'
      }).save();
    });

    afterEach(async () => {
      await Promise.all([
        PACSubdivision.findByIdAndDelete(subdivisionMock._id),
        PACModel.findByIdAndDelete(modelMock._id),
        IDRProviderValidTins.findByIdAndDelete(providerValidTins._id),
        EntitySourceCollection.findByIdAndDelete(entitySourceCollectionMock._id)
      ]);
    });


    it('should successfully validate a list of entities', () => {
      const entities = [CreateMockEntity()];
      entities[0].tin = '000000000';
      entities[0].npi = '1234567890';
      entities[0].subdiv_id = '000000000';
      entities[0].apm_id = '000000000';

      return validateEntityList(entities, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.false;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.equal(0);
        });
    });

    it('should handle entities that fail validation', () => {
      const entities = [CreateMockEntity({ apm_id: '60', subdiv_id: '000000000' })];
      entities[0].start_date = null;
      entities[0].end_date = null;

      return validateEntityList(entities, validate)
        .then((results) => {
          expect(results).to.not.be.undefined;
          expect(results.length).to.equal(1);
          expect(results[0].update.invalid).to.be.true;
          expect(results[0].update).to.not.be.undefined;
          expect(results[0].update.validations.length).to.be.greaterThan(1);
        });
    });

    describe('Errors', () => {
      afterEach(() => {
        modelMock.restore();
      });
      it('should handle errors in validating', (done) => {
        modelMock = sinon.mock(PACModel);

        modelMock
          .expects('find')
          .atLeast(1)
          .chain('lean')
          .atLeast(1)
          .rejects(new Error('Fake Error'));
        const entities = [CreateMockEntity({ apm_id: '60' })];
        validateEntityList(entities, () => {
          throw new Error('Fake Error');
        })
          .catch((err) => {
            expect(err).to.not.be.undefined;
            expect(err.message).to.equal('Fake Error');
            done();
          });
      });
    });
  });
});
