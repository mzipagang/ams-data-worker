const chai = require('chai');
const validation = require('../../../../app/services/validation/models/beneficiary');
const Entity = require('../../../../app/services/mongoose/models/apm_entity');
const Model = require('../../../../app/services/mongoose/models/pac_model');
const createBeneficiary = require('../../../mocks/data/beneficiary');
const sinon = require('sinon');
require('sinon-mongoose');

const expect = chai.expect;

const beneficiaries = [
  createBeneficiary({
    program_org_id: 'abc123',
    entity_id: null
  }),
  createBeneficiary({
    program_org_id: null,
    entity_id: 'abc123'
  })
];

const validateFn = () => ({
  isValid: true,
  hardValidationFails: [],
  softValidationFails: []
});

describe('Service: validations/beneficiary', () => {
  let EntityMock;
  let ModelMock;

  beforeEach(() => {
    EntityMock = sinon.mock(Entity);
    EntityMock.expects('find')
      .chain('lean')
      .once()
      .resolves([]);
    ModelMock = sinon.mock(Model);
    ModelMock.expects('find')
      .chain('lean')
      .once()
      .resolves([]);
  });

  afterEach(() => {
    EntityMock.restore();
    ModelMock.restore();
  });

  it('should load properly', () => {
    expect(validation.validateList).to.not.be.undefined;
  });

  it('should get entities from apm_entity collection', () =>
    validation.validateList(beneficiaries, validateFn).then(() => {
      EntityMock.verify();
    }));

  it('should call validation function', () => {
    const validateFnSpy = sinon.spy(validateFn);
    return validation.validateList(beneficiaries, validateFnSpy).then(() => {
      expect(validateFnSpy).to.have.been.called;
    });
  });

  it('should return a list of validation results', () =>
    validation.validateList(beneficiaries, validateFn).then((results) => {
      expect(results).to.not.be.undefined;
      expect(results.length).to.equal(2);
    }));
});
