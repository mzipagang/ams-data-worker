const chai = require('chai');
const validation = require('../../../../app/services/validation/models/qpc_score');
const DynamicPACEntity = require('../../../../app/services/mongoose/dynamic_name_models/pac_entity');
const pacPublishHistoryUtils = require('../../../../app/services/pacPublishHistoryUtils');
const createQpcScore = require('../../../mocks/data/pac-qpc_score');
const sinon = require('sinon');
require('sinon-mongoose');

const DynamicPACNameEntity = DynamicPACEntity('1');

const expect = chai.expect;

const qpcScores = [
  createQpcScore({
    entity_id: 'abc123',
    measure_id: 'ABCD1234'
  }),
  createQpcScore({
    entity_id: null,
    measure_id: null
  })
];

const validateFn = () => ({
  isValid: true,
  hardValidationFails: [],
  softValidationFails: []
});

describe('Service: validations/qpc_score', () => {
  let EntityMock;
  let PacPublishHistoryUtilsMock;

  beforeEach(() => {

    PacPublishHistoryUtilsMock = sinon.mock(pacPublishHistoryUtils);
      PacPublishHistoryUtilsMock
        .expects('getLatestPacFilesByPerformanceYear')
        .resolves({
          pacEntityFile: { file_id: '1' },
          pacModelFile: { file_id: '1' },
          pacSubdivisionFile: { file_id: '1' }
        });

    EntityMock = sinon.mock(DynamicPACNameEntity);
    EntityMock
        .expects('find')
        .atLeast(1)
        .chain('lean')
        .atLeast(1)
        .resolves([{
          id: '0000-0000-0000-0000',
          tin: 'XXXXXXXXX',
          npi: 'XXXXXXXXXX',
          performance_year: 2017,
          subdiv_id: '0000-0000-0000-0000',
          apm_id: '0000-0000-0000-0000'
        }]);
  });

  afterEach(() => {
    EntityMock.restore();
    PacPublishHistoryUtilsMock.restore();
  });

  it('should load properly', () => {
    expect(validation.validateList).to.not.be.undefined;
  });

  it('should get entities from apm_entity collection', () =>
    validation.validateList(qpcScores, validateFn).then(() => {
      EntityMock.verify();
    }));

  it('should call validation function', () => {
    const validateFnSpy = sinon.spy(validateFn);
    return validation.validateList(qpcScores, validateFnSpy).then(() => {
      expect(validateFnSpy).to.have.been.called;
    });
  });

  it('should return a list of validation results', () =>
    validation.validateList(qpcScores, validateFn).then((results) => {
      expect(results).to.not.be.undefined;
      expect(results.length).to.equal(2);
    }));
});
