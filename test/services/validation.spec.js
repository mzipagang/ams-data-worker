const environment = require('../environment');
const moment = require('moment');
const chai = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');
const Promise = require('bluebird');

const expect = chai.expect;
require('../../app/services/mongoose');
const ModelMockResponse = require('../mocks/data/pac_model');
const ModelSubdivResponse = require('../mocks/data/pac_subdivision');
const validation = require('../../app/services/validation');
const APMEntity = require('../../app/services/mongoose/models/apm_entity');
const APMProviderValidationModel = require('../../app/services/validation/models/apm_provider');
const createMockProvider = require('../mocks/data/apm_provider');
const fileImportGroup = require('../../app/services/mongoose/models/file_import_group');
const fileImportError = require('../../app/services/mongoose/models/file_import_error');

const schemas = {
  model: require('../../app/schemas/Model'),
  subdivision: require('../../app/schemas/Subdivision'),
  entity: require('../../app/schemas/Entity'),
  provider: require('../../app/schemas/Provider')
};

const mockRules = {
  hardValidations: [],
  softValidations: []
};

const createProvider = (entityId, npi, tin, startDate, endDate) => ({
  entity_id: entityId,
  start_date: startDate,
  end_date: endDate,
  tin,
  npi,
  ccn: null,
  tin_type_code: null,
  specialty_code: null,
  organization_name: null,
  first_name: null,
  middle_name: null,
  last_name: null,
  participant_type_code: null
});

const provider = createProvider(
  'A1001',
  '1234567890',
  '123456789',
  moment
    .utc()
    .add(-1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate(),
  moment
    .utc()
    .add(1, 'month')
    .startOf('month')
    .startOf('day')
    .toDate()
);

const dependencies = {
  model: ModelMockResponse(),
  subdivision: ModelSubdivResponse(),
  entity: {},
  provider: {}
};

describe('Service: Validations', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('validate()', () => {
    describe('with a valid data row', () => {
      it('should return isValid as true', () => {
        mockRules.hardValidations = [
          {
            ruleFunction: () => ({
              isValid: true
            })
          }
        ];
        mockRules.softValidations = [
          {
            ruleFunction: () => ({
              isValid: true
            })
          }
        ];
        const data = validation.validate(provider, schemas.provider, mockRules, dependencies);
        expect(data).to.not.be.undefined;
        expect(data.isValid).to.be.true;
      });

      it('should have no hard/soft validations errors', () => {
        const data = validation.validate(provider, schemas.provider, mockRules, dependencies);

        expect(data).to.not.be.undefined;
        expect(data.hardValidationFails.length).to.equal(0);
        expect(data.softValidationFails.length).to.equal(0);
      });
    });

    describe('with an invalid data row', () => {
      describe('with hard validations rule failures', () => {
        beforeEach(() => {
          mockRules.hardValidations = [
            {
              ruleFunction: () => ({
                isValid: false
              })
            }
          ];
          mockRules.softValidations = [];
        });

        it('should return isValid as false', () => {
          const data = validation.validate(provider, schemas.provider, mockRules, dependencies);
          expect(data).to.not.be.undefined;
          expect(data.isValid).to.be.false;
        });

        it('should return hard validations failures', () => {
          const data = validation.validate(provider, schemas.provider, mockRules, dependencies);
          expect(data).to.not.be.undefined;
          expect(data.hardValidationFails.length).to.be.at.least(1);
        });

        it('should return a schema failed validations', () => {
          const data = validation.validate(provider, schemas.model, mockRules, dependencies);
          expect(data).to.not.be.undefined;
          expect(data.isValid).to.be.false;
        });
      });
      describe('with soft validations rule failures', () => {
        beforeEach(() => {
          mockRules.softValidations = [
            {
              ruleFunction: () => ({
                isValid: false
              })
            }
          ];
          mockRules.hardValidations = [];
        });

        it('should return isValid as true', () => {
          const data = validation.validate(provider, schemas.provider, mockRules, dependencies);
          expect(data).to.not.be.undefined;
          expect(data.isValid).to.be.true;
        });

        it('should return soft validations failures', () => {
          const data = validation.validate(provider, schemas.provider, mockRules, dependencies);
          expect(data).to.not.be.undefined;
          expect(data.softValidationFails.length).to.be.at.least(1);
        });
      });
    });

    describe('with a thrown error', () => {
      describe('with an error in executing rule', () => {
        beforeEach(() => {
          mockRules.hardValidations = [
            {
              ruleFunction: () => {
                throw new Error('fake error');
              }
            }
          ];
        });

        it('should throw error', () => {
          try {
            validation.validate(provider, schemas.provider, mockRules, dependencies);
          } catch (err) {
            expect(err.message).to.equal('fake error');
          }
        });
      });
    });
  });

  describe('validateCollection()', () => {
    let fileImportGroupMock;
    let apmEntityMock;
    let apmProviderValidationModelStub;
    let fileImportErrorMock;
    const file = {};
    const mockProvider = createMockProvider();

    let data = [];

    const result = Promise.try(() => [{ data: 'data' }]);

    const query = () =>
      Object.assign(result, {
        find: query,
        lean: query,
        cursor: () => ({
          eachAsync: cb => Promise.each(data, cb)
        })
      });

    const model = Object.assign(result, {
      find: query,
      lean: query,
      cursor: () => ({
        eachAsync: cb => Promise.each(data, cb)
      }),
      collection: {
        initializeUnorderedBulkOp: () => ({
          find: () => ({
            updateOne: () => ({})
          }),
          execute: (cb) => {
            setImmediate(() => {
              cb(null, true);
            });
          }
        })
      }
    });

    beforeEach(() => {
      data = [];
      fileImportGroupMock = sinon.mock(fileImportGroup);
      fileImportGroupMock
        .expects('findOneAndUpdate')
        .once()
        .resolves({});

      apmEntityMock = sinon.mock(APMEntity);
      apmEntityMock
        .expects('find')
        .once()
        .resolves([]);

      apmProviderValidationModelStub = sinon.stub(APMProviderValidationModel, 'validateList').callsFake(() =>
        Promise.resolve([
          {
            validationType: 'HARD_VALIDATION',
            errorCode: 'A2'
          }
        ])
      );

      fileImportErrorMock = sinon.mock(fileImportError);
      fileImportErrorMock
        .expects('find')
        .chain('lean')
        .once()
        .resolves([]);
    });

    afterEach(() => {
      fileImportGroupMock.restore();
      apmEntityMock.restore();
      apmProviderValidationModelStub.restore();
      fileImportErrorMock.restore();
    });

    it('should process a collection', () => {
      data = [mockProvider];
      return validation.validateCollection(model, 'apm-provider', file);
    });
    it('should process a collection', () => {
      data = [mockProvider];
      return validation.validateCollection(model, 'apm-provider', file);
    });

    it('should process in batches of under 201 records', () => {
      data = Array.apply({}, new Array(199)).map(() => mockProvider);
      return validation.validateCollection(model, 'apm-provider', file);
    });

    it('should process in batches of exactly 201 records', () => {
      data = Array.apply({}, new Array(201)).map(() => mockProvider);
      return validation.validateCollection(model, 'apm-provider', file);
    });

    it('should process in batches of over 201 records', () => {
      data = Array.apply({}, new Array(210)).map(() => mockProvider);
      return validation.validateCollection(model, 'apm-provider', file);
    });

    it('should update fileImportGroup with validation summary', () => {
      data = [mockProvider];
      return validation.validateCollection(model, 'apm-provider', file).then(() => {
        fileImportGroupMock.verify();
      });
    });

    it('should properly handle an error on inserting validation results to the database', () => {
      data = [mockProvider];
      const modelWithFailingDb = Object.assign(result, {
        find: query,
        lean: query,
        cursor: () => ({
          eachAsync: cb => Promise.resolve(data.map(cb))
        }),
        collection: {
          initializeUnorderedBulkOp: () => ({
            find: () => ({
              updateOne: () => ({})
            }),
            execute: (cb) => {
              cb(new Error('Fake Error'), null);
            }
          })
        }
      });
      return validation.validateCollection(modelWithFailingDb, 'apm-provider', file).catch((err) => {
        expect(err).to.not.be.undefined;
        expect(err.message).to.equal('Fake Error');
      });
    });

    it('should not update fileImportGroup if there are no validations', () => {
      data = [];
      const emptyResult = Promise.try(() => []);

      const query2 = () =>
        Object.assign(emptyResult, {
          find: query2,
          lean: query2,
          cursor: () => ({
            eachAsync: cb => Promise.each(data, cb)
          })
        });

      const modelWithNoValidationsInDb = Object.assign(emptyResult, {
        find: query2,
        lean: query2,
        cursor: () => ({
          eachAsync: cb => Promise.resolve(data.map(cb))
        }),
        collection: {
          initializeUnorderedBulkOp: () => ({
            find: () => ({
              updateOne: () => ({})
            }),
            execute: (cb) => {
              cb(null, true);
            }
          })
        }
      });

      fileImportGroupMock.restore();
      fileImportGroupMock = sinon.mock(fileImportGroup);
      fileImportGroupMock.expects('findOneAndUpdate').never();

      return validation
        .validateCollection(modelWithNoValidationsInDb, 'apm-provider', file)
        .then(() => fileImportGroupMock.verify());
    });
  });

  describe('applyValidationHierarchy()', () => {
    const applyValidationHierarchy = validation.applyValidationHierarchy;

    it('should return formatted validation results', () => {
      const validations = [
        {
          validationType: 'HARD_VALIDATION',
          errorCode: 'A2'
        }
      ];
      const results = applyValidationHierarchy(validations, 'apm_entity');
      expect(results.length).to.equal(1);
      expect(results[0].error_type).to.equal('HARD_VALIDATION');
      expect(results[0].error).to.not.be.undefined;
      expect(results[0].errorMetadata).to.not.be.undefined;
    });

    it('should return the error with the greatest priority for a group of errors', () => {
      const validations = [
        {
          validationType: 'HARD_VALIDATION',
          errorCode: 'A2'
        },
        {
          validationType: 'HARD_VALIDATION',
          errorCode: 'A1'
        }
      ];
      const results = applyValidationHierarchy(validations, 'apm_entity');
      expect(results.length).to.equal(1);
      expect(results[0].error_type).to.equal('HARD_VALIDATION');
      expect(results[0].error).to.not.be.undefined;
      expect(results[0].errorMetadata).to.not.be.undefined;
      expect(results[0].errorMetadata.errorPriority).to.equal(1);
    });

    it('should validate fileType apm_provider', () => {
      const validations = [
        {
          validationType: 'HARD_VALIDATION',
          errorCode: 'A1'
        }
      ];
      const results = applyValidationHierarchy(validations, 'apm_provider');
      expect(results).to.not.be.undefined;
    });

    it('should return the error_code', () => {
      const validations = [
        {
          validationType: 'HARD_VALIDATION',
          errorCode: 'A1'
        }
      ];
      const results = applyValidationHierarchy(validations, 'apm_provider');
      expect(results).to.not.be.undefined;
      expect(results[0].error_code).to.not.be.undefined;
    });

    it('should skip validation entry if error code does not exist', () => {
      const validations = [
        {
          validationType: 'HARD_VALIDATION',
          errorCode: 'A3'
        }
      ];
      const results = applyValidationHierarchy(validations, 'apm_entity');
      expect(results.length).to.equal(0);
    });
  });

  describe('validateMultipleTypesInSingleFileAndSummarize', () => {
    it('should validate for each model passed in', () => {
      const validationWithoutSummaryStub = sinon.stub(validation, 'validateCollectionWithoutSummary').resolves({});
      const updateSummaryStub = sinon.stub(validation, 'updateValidationSummary').resolves({});

      validation.validateMultipleTypesInSingleFileAndSummarize([
        {
          collection: {},
          type: 'pac-qp_category_score'
        },
        {
          collection: {},
          type: 'pac-qp_category_score_summary'
        }
      ], {});

      validationWithoutSummaryStub.restore();
      updateSummaryStub.restore();
    });
  });

  describe('updateValidationSummary', () => {
    it('should handle array passed in for collection models', () => {
      validation.updateValidationSummary([{}], {});
    });
  });
});
