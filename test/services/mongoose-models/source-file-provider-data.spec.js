const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const SourceFileProviderData = require('../../../app/services/mongoose/models/source_file_provider_data');

describe('Mongoose Model: Apm Provider', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the correct model', () => {
      expect(SourceFileProviderData).to.not.be.undefined;
      expect(SourceFileProviderData.collection.collectionName).to.equal('source_file_provider_datas');
    });

    it('should export JSON', () => {
      const model = new SourceFileProviderData({
        id: '1234',
        apm_id: '22',
        subdiv_id: '33',
        start_date: new Date(),
        end_date: new Date(),
        name: 'Name'
      });

      const json = model.toJson();

      expect(model).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });
  });
});
