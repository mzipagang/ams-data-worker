const chai = require('chai');
const moment = require('moment');
const diffHistory = require('mongoose-diff-history/diffHistory');
const RoleBuilder = require('ams-shared/test-utils/builders/RoleBuilder.js');
const PermissionBuilder = require('ams-shared/test-utils/builders/PermissionBuilder.js');
const UserBuilder = require('ams-shared/test-utils/builders/UserBuilder.js');
const Role = require('../../../app/services/mongoose/models/role');
const Permission = require('../../../app/services/mongoose/models/permission');
const User = require('../../../app/services/mongoose/models/user');
const History = require('../../../app/services/mongoose/models/history');

const expect = chai.expect;

describe('Mongoose Model: User', () => {
  let roleBuilder;
  let permissionBuilder;
  let userBuilder;
  let role1;
  let role2;
  let permission1;
  let permission2;
  let user;
  let now;

  beforeEach(async () => {
    now = moment();
    permissionBuilder = new PermissionBuilder().withConstructor(Permission);
    roleBuilder = new RoleBuilder().withConstructor(Role);
    userBuilder = new UserBuilder().withConstructor(User);

    permission1 = await permissionBuilder
      .fullyPopulated()
      .build()
      .save();
    permission2 = await permissionBuilder
      .fullyPopulated()
      .build()
      .save();
    role1 = await roleBuilder
      .fullyPopulated()
      .withPermissions([permission1])
      .build()
      .save();
    role2 = await roleBuilder
      .fullyPopulated()
      .withPermissions([permission2])
      .build()
      .save();

    user = await userBuilder
      .fullyPopulated()
      .withRoles([role1, role2])
      .build()
      .save();
  });

  afterEach(async () => {
    await User.deleteMany();
    await Role.deleteMany();
    await Permission.deleteMany();
    await History.deleteMany();
  });

  function assertLoadedUser(loadedUser) {
    // Assert user properties
    expect(loadedUser).to.be.an.instanceOf(User);
    expect(loadedUser.mail).to.equal(user.mail);
    expect(loadedUser.username).to.equal(user.username);
    expect(loadedUser.firstName).to.equal(user.firstName);
    expect(loadedUser.lastName).to.equal(user.lastName);
    expect(moment(loadedUser.createdAt).isSameOrAfter(now)).to.be.true;
    expect(moment(loadedUser.updatedAt).isSameOrAfter(now)).to.be.true;

    // Assert role properties
    expect(loadedUser.roles).to.be.ofSize(2);
    expect(loadedUser.roles[0].name).to.equal(role1.name);
    expect(loadedUser.roles[0].description).to.equal(role1.description);
    expect(loadedUser.roles[0].permissions).to.be.ofSize(1);
    expect(loadedUser.roles[1].name).to.equal(role2.name);
    expect(loadedUser.roles[1].description).to.equal(role2.description);
    expect(loadedUser.roles[1].permissions).to.be.ofSize(1);

    // Assert permissions properties
    const p1 = loadedUser.roles[0].permissions[0];
    const p2 = loadedUser.roles[1].permissions[0];
    expect(p1.name).to.equal(permission1.name);
    expect(p1.description).to.equal(permission1.description);
    expect(p2.name).to.equal(permission2.name);
    expect(p2.description).to.equal(permission2.description);
  }

  it('should automatically populate children when you "findOne" User', async () => {
    const loadedUser = await User.findOne(user._id);
    assertLoadedUser(loadedUser);
  });

  it('should automatically populate children when you "find" a User', async () => {
    const loadedUsers = await User.find({ mail: user.mail });
    expect(loadedUsers).to.be.ofSize(1);
    assertLoadedUser(loadedUsers[0]);
  });

  it('should update a user and log the differences', async () => {
    user.firstName = 'New value';
    await user.save();

    const updatedUser = await User.findOne(user._id);
    expect(updatedUser.firstName).to.equal('New value');

    // Assert we're keeping the history
    const histories = await diffHistory.getDiffs('user', updatedUser._id);
    expect(histories).to.be.ofSize(1);
    const history = histories[0];
    expect(history.collectionName).to.equal('user');
    expect(history.diff.updatedAt).to.be.ofSize(2);
  });

  describe('toJSON', () => {
    function assertJson(json) {
      expect(json.mail).to.equal(user.mail);
      expect(json.username).to.equal(user.username);
      expect(json.firstName).to.equal(user.firstName);
      expect(json.lastName).to.equal(user.lastName);

      expect(json.roles).to.be.ofSize(2);
      expect(json.roles[0]).to.equal(role1.name);
      expect(json.roles[1]).to.equal(role2.name);

      expect(json.permissions).to.be.ofSize(2);
      expect(json.permissions[0]).to.equal(permission1.name);
      expect(json.permissions[1]).to.equal(permission2.name);
    }

    it('should include a "roles" array of just strings (not objects)', async () => {
      const loadedUser = await User.findOne(user._id);
      const json = loadedUser.toJSON();
      assertJson(json);
    });

    it('should include a "permissions" array of just strings (not objects)', async () => {
      const loadedUser = await User.findOne(user._id);
      const json = loadedUser.toJSON();
      assertJson(json);
    });

    it('should return null roles and null permissions if not populated with child data', async () => {
      const json = user.toJSON(); // Not populated with child data
      expect(json.mail).to.equal(user.mail);
      expect(json.username).to.equal(user.username);
      expect(json.firstName).to.equal(user.firstName);
      expect(json.lastName).to.equal(user.lastName);
      expect(json.roles).to.not.exist;
      expect(json.permissions).to.be.null;
    });
  });

  describe('#upsertUser', () => {
    it('should create a new user if not found', async () => {
      const newUser = UserBuilder.randomUser(); // not saved
      newUser.roles = [role1, role2]; // Should NOT get saved
      const upserResult = await User.upsertUser(newUser);
      expect(upserResult).to.exist;

      // Retrieve and verify saved value
      const savedUser = await User.findOne({ username: newUser.username });
      expect(savedUser).to.be.an.instanceOf(User);
      expect(savedUser.mail).to.equal(newUser.mail);
      expect(savedUser.username).to.equal(newUser.username);
      expect(savedUser.firstName).to.equal(newUser.firstName);
      expect(savedUser.lastName).to.equal(newUser.lastName);
      expect(savedUser.roles).to.be.ofSize(0);
    });

    it('should update an existing user (but not its roles)', async () => {
      user.firstName = 'new name';
      user.mail = 'new mail';

      const upserResult = await User.upsertUser(user);
      expect(upserResult).to.exist;

      // Retrieve and verify saved value
      const savedUser = await User.findOne({ username: user.username });
      expect(savedUser).to.be.an.instanceOf(User);
      expect(savedUser.mail).to.equal(user.mail);
      expect(savedUser.username).to.equal(user.username);
      expect(savedUser.firstName).to.equal(user.firstName);
      expect(savedUser.lastName).to.equal(user.lastName);
      expect(savedUser.roles).to.be.ofSize(2); // Keep old roles

      // Assert we're keeping the history
      const histories = await diffHistory.getDiffs('user', savedUser._id);
      expect(histories).to.be.ofSize(1);
      const history = histories[0];
      expect(history.collectionName).to.equal('user');
      expect(history.user).to.equal(savedUser.username);
      expect(history.reason).to.equal('Updated from LDAP');
      expect(history.diff.mail).to.be.ofSize(2);
    });
  });

  describe('#getUser', () => {
    it('should return undefined if a user is not found', async () => {
      const result = await User.getUser('jibberish');
      expect(result).to.be.undefined;
    });

    it('should return the User JSON if a user is found', async () => {
      const result = await User.getUser(user.username);
      expect(result).to.not.be.an.instanceOf(User);
      expect(result.permissions).to.be.ofSize(2);
    });
  });

  describe('#virtual "name"', () => {
    it('should expose a virtual name property that mirrors first name', () => {
      expect(user.name).to.equal(user.firstName);
    });

    it('should expose virtual properties in the toJSON', () => {
      expect(user.toJSON().name).to.equal(user.firstName);
    });

    it('should expose virtual properties in the toObject', () => {
      expect(user.toObject().name).to.equal(user.firstName);
    });
  });
});
