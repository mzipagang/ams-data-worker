const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const ApmEntity = require('../../../app/services/mongoose/models/apm_entity');

describe('Mongoose Model: ApmEntity', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the correct model', () => {
      expect(ApmEntity).to.not.be.undefined;
      expect(ApmEntity.collection.collectionName).to.equal('apm_entities');
    });

    it('should export JSON', () => {
      const model = new ApmEntity({
        id: '1234',
        apm_id: '22',
        subdiv_id: '33',
        start_date: new Date(),
        end_date: new Date(),
        name: 'Name'
      });

      const json = model.toJson();

      expect(model).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });
  });
});
