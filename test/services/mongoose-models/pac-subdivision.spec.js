const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

let PacSubdivision;

describe('Mongoose Subdivision: PacSubdivision', () => {
  beforeEach(() => {
    environment.use('dev');
    PacSubdivision = require('../../../app/services/mongoose/models/pac_subdivision');
  });

  it('should load', () => {
    expect(PacSubdivision).to.not.be.undefined;
  });

  it('should create a new instance of a subdivision', () => {
    const subdivision = new PacSubdivision();
    expect(subdivision).to.not.be.undefined;
  });

  describe('createNewPacSubdivisionId', () => {
    it('should return a new subdivision id', () => {
      const subdivisionsList = [{ id: '00' }, { id: '01' }];
      const subdivisionId = PacSubdivision.createNewPacSubdivisionId(subdivisionsList);
      expect(subdivisionId).to.equal('02');
    });
  });
});
