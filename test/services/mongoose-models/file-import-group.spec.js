const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const FileImportGroup = require('../../../app/services/mongoose/models/file_import_group');

describe('Mongoose Model: FileImportGroup', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the DataLog model', () => {
      expect(FileImportGroup).to.not.be.undefined;
      expect(FileImportGroup.collection.collectionName).to.equal('file_import_groups');
    });

    it('should export JSON', () => {
      const log = new FileImportGroup({
        status: 'publish',
        files: [
          {
            file_name: 'fake file name.txt',
            file_location: 's3:somewhere/over/the/rainbow.txt',
            status: 'pending',
            import_file_type: 'pac-model'
          }
        ]
      });

      const json = log.toObject();

      expect(log).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });
  });

  describe('#getFileById', () => {
    it('should get the correct file from a file import group', async () => {
      const file = await FileImportGroup.getFileById('8d299a19-ffb1-4873-ad46-603090de31ce');
      expect(file.import_file_type).to.equal('pac-model');
      expect(file.performance_year).to.equal(2017);
      expect(file.id).to.equal('8d299a19-ffb1-4873-ad46-603090de31ce');
    });
  });
});
