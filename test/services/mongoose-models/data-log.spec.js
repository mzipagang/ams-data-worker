const sinon = require('sinon');
const chai = require('chai');
const environment = require('../../environment');
const Promise = require('bluebird');
const uuid = require('uuid');
const moment = require('moment');

const expect = chai.expect;

const DataLog = require('../../../app/services/mongoose/models/data_log');

describe('Mongoose Model: DataLog', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    before(() => {
      sinon.stub(DataLog, 'create').callsFake(data => Promise.resolve(new DataLog(data)));
      sinon.stub(DataLog, 'insertMany').callsFake(datas => Promise.resolve(datas.map(data => new DataLog(data))));
    });

    after(() => {
      DataLog.create.restore();
      DataLog.insertMany.restore();
    });

    afterEach(() => {
      DataLog.create.resetHistory();
      DataLog.insertMany.resetHistory();
    });

    it('should export the DataLog model', () => {
      expect(DataLog).to.not.be.undefined;
      expect(DataLog.collection.collectionName).to.equal('data_logs');
    });

    it('should export JSON', () => {
      const log = new DataLog({
        id: uuid.v4(),
        receiptDate: moment.utc().toDate(),
        processedDate: moment.utc().toDate()
      });

      const json = log.toJson();

      expect(log).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });

    it('should create a log', () => {
      const action = 'upsert';
      const newValue = new DataLog({
        id: uuid.v4(),
        receiptDate: moment.utc().toDate(),
        processedDate: moment.utc().toDate()
      });
      const inMeta = null;

      return DataLog.createLog(action, newValue, inMeta);
    });

    it('should create a batch of logs', () => {
      const action = 'batch';
      const newValues = [{
        meta: {},
        value: new DataLog({
          id: uuid.v4(),
          receiptDate: moment.utc().toDate(),
          processedDate: moment.utc().toDate()
        })
      }];

      return DataLog.createLogBatch(action, newValues);
    });
  });
});
