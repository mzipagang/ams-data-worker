const chai = require('chai');
const moment = require('moment');
const diffHistory = require('mongoose-diff-history/diffHistory');
const PermissionBuilder = require('ams-shared/test-utils/builders/PermissionBuilder.js');
const Permission = require('../../../app/services/mongoose/models/permission');
const History = require('../../../app/services/mongoose/models/history');

const expect = chai.expect;

describe('Mongoose Model: Permission', () => {
  afterEach(async () => {
    await Permission.deleteMany();
    await History.deleteMany();
  });

  it('should save a permission to MongoDB', async () => {
    const now = moment();
    const newPermission = new PermissionBuilder().withConstructor(Permission).build();

    await newPermission.save();

    const loadedPermission = await Permission.findOne(newPermission);
    expect(loadedPermission).to.not.be.undefined;
    expect(loadedPermission.name).to.equal(newPermission.name);
    expect(loadedPermission.description).to.equal(newPermission.description);
    expect(moment(loadedPermission.createdAt).isSameOrAfter(now)).to.be.true;
    expect(moment(loadedPermission.updatedAt).isSameOrAfter(now)).to.be.true;
  });

  it('should update a permission and log the differences', async () => {
    const newPermission = new PermissionBuilder().withConstructor(Permission).build();
    await newPermission.save();

    const loadedPermission = await Permission.findOne(newPermission);
    loadedPermission.description = 'New value';
    await loadedPermission.save();

    const updatedPermission = await Permission.findOne(loadedPermission);
    expect(updatedPermission.description).to.equal('New value');

    // Assert we're keeping the history
    const histories = await diffHistory.getDiffs('permission', updatedPermission._id);
    expect(histories).to.be.ofSize(1);
    const history = histories[0];
    expect(history.collectionName).to.equal('permission');
    expect(history.diff.description).to.be.ofSize(2);
    expect(history.diff.updatedAt).to.be.ofSize(2);
  });
});
