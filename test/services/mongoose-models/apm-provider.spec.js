const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const ApmProvider = require('../../../app/services/mongoose/models/apm_provider');

describe('Mongoose Model: ApmProvider', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the correct model', () => {
      expect(ApmProvider).to.not.be.undefined;
      expect(ApmProvider.collection.collectionName).to.equal('apm_providers');
    });


    it('should export JSON', () => {
      const model = new ApmProvider({
        entity_id: '12',
        start_date: new Date(),
        end_date: new Date(),
        tin: '123456789'
      });

      const json = model.toJson();

      expect(model).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });

    it('should export JSON', () => {
      const model = new ApmProvider({
        entity_id: '12',
        start_date: new Date(),
        end_date: new Date(),
        tin: '123456789',
        participation: [{
          start_date: new Date(),
          end_date: new Date()
        }]
      });

      const json = model.toJson();

      expect(model).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });
  });
});
