const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

let PacSnapshot;
const snapshotData = {
  performance_year: 2017,
  run_snapshot: 'F',
  run_number: 1,
  file_id: 'edc7d856-d954-4d3c-ac91-727ab64ac6fb',
  type: 'pac-subdivision'
};

describe('Mongoose Subdivision: PacSnapshot', () => {
  beforeEach(() => {
    environment.use('dev');
    PacSnapshot = require('../../../app/services/mongoose/models/pac_snapshot');
  });

  it('should load', () => {
    expect(PacSnapshot).to.not.be.undefined;
  });

  it('should create a new instance of a subdivision', () => {
    const snapshot = new PacSnapshot(snapshotData);
    expect(snapshot).to.not.be.undefined;
  });
});
