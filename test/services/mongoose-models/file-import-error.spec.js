const chai = require('chai');
const environment = require('../../environment');
const uuid = require('uuid');

const expect = chai.expect;

const FileImportError = require('../../../app/services/mongoose/models/file_import_error');

describe('Mongoose Model: FileImportError', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the DataLog model', () => {
      expect(FileImportError).to.not.be.undefined;
      expect(FileImportError.collection.collectionName).to.equal('file_import_errors');
    });

    it('should export JSON', () => {
      const log = new FileImportError({
        file_import_group_id: uuid.v4(),
        file_id: uuid.v4(),
        error: 'Fake error',
        line_number: 123
      });

      const json = log.toJson();

      expect(log).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });
  });
});
