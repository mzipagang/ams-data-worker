const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

let PacModel;

describe('Mongoose Model: PacModel', () => {
  beforeEach(() => {
    environment.use('dev');
    PacModel = require('../../../app/services/mongoose/models/pac_model');
  });

  it('should load', () => {
    expect(PacModel).to.not.be.undefined;
  });

  it('should create a new instance of a model', () => {
    const model = new PacModel();
    expect(model).to.not.be.undefined;
  });

  it('should allow creation with correct attributes', () => {
    const modelValues = {
      name: 'foobar',
      advanced_apm_flag: 'Y',
      mips_apm_flag: 'Y',
      quality_reporting_category_code: '2',
      model_category: {
        text: 'this is the freeform text',
        value: 'test'
      },
      group_center: [
        'test',
        'test2'
      ],
      group_cmmi: 'cmmi',
      waivers: [
        {
          text: 'this is the freeform text',
          value: 'test'
        }
      ],
      target_participant: [
        {
          text: 'this is the freeform text',
          value: 'test'
        }
      ],
      target_provider: [
        {
          text: 'this is the freeform text',
          value: 'test'
        }
      ],
      statutory_authority_tier_1: 'test1',
      statutory_authority_tier_2: 'test2',
      target_beneficiary: [
        {
          text: 'this is the freeform text2',
          value: 'test'
        },
        {
          text: 'this is the freeform text3',
          value: 'test'
        }
      ]
    };
    const model = new PacModel(modelValues);
    expect(model).to.not.be.undefined;
    expect(model.name).to.equal('foobar');
    expect(model.advanced_apm_flag).to.equal('Y');
    expect(model.mips_apm_flag).to.equal('Y');
    expect(model.quality_reporting_category_code).to.equal('2');
    expect(model.waivers[0].text).to.equal('this is the freeform text');
    expect(model.waivers[0].value).to.equal('test');
    expect(model.group_center[0]).to.equal('test');
    expect(model.group_center[1]).to.equal('test2');
    expect(model.group_cmmi).to.equal('cmmi');
    expect(model.target_participant[0].text).to.equal('this is the freeform text');
    expect(model.target_participant[0].value).to.equal('test');
    expect(model.target_provider[0].text).to.equal('this is the freeform text');
    expect(model.target_provider[0].value).to.equal('test');
    expect(model.statutory_authority_tier_1).to.equal('test1');
    expect(model.statutory_authority_tier_2).to.equal('test2');
    expect(model.target_beneficiary[0].text).to.equal('this is the freeform text2');
    expect(model.target_beneficiary[0].value).to.equal('test');
    expect(model.target_beneficiary[1].text).to.equal('this is the freeform text3');
    expect(model.target_beneficiary[1].value).to.equal('test');
  });

  describe('createNewPacModelId', () => {
    it('should return a new model id', () => {
      const modelsList = [{ id: '01' }];
      const modelId = PacModel.createNewPacModelId(modelsList);
      expect(modelId).to.equal('55');
    });
  });
});
