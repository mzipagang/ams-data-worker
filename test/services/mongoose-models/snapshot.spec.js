const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const Snapshot = require('../../../app/services/mongoose/models/snapshot');

describe('Mongoose Model: Snapshot', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the correct model', () => {
      expect(Snapshot).to.not.be.undefined;
      expect(Snapshot.collection.collectionName).to.equal('snapshots');
    });

    it('should export JSON', () => {
      const model = new Snapshot({
        qppYear: 2018,
        qpPeriod: 'Second',
        snapshotRun: 'rerun',
        status: 'approved'
      });

      const json = model.toJson();

      expect(model).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });
  });
});
