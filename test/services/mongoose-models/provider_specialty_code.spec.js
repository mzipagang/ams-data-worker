const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const providerSpecialtyCode = require('../../../app/services/mongoose/models/provider_specialty_code');

describe('Mongoose Model: providerSpecialtyCode', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the correct model', () => {
      expect(providerSpecialtyCode.collection.collectionName).to.equal('provider_specialty_codes');
    });
  });
});
