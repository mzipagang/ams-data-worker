const environment = require('../../environment');
const mongoose = require('mongoose');
const Promise = require('bluebird');
const jwt = require('jsonwebtoken');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const DataLog = require('../../../app/services/mongoose/models/data_log');
const momentProto = require('moment').fn;

const expect = chai.expect;
const MongooseMock = () => mongoose;

const DataLogMock = (data) => {
  const mock = require('../../mocks/dependencies/model')(data);
  mock.getLog = () => ({
    action: mock.action,
    newValue: mock.newValue,
    inMeta: mock.inMeta
  });
  mock.createLog = (action, newValue, inMeta) => Promise.try(() => {
    mock.action = action;
    mock.newValue = newValue;
    mock.inMeta = inMeta;
  });
  mock.actions = DataLog.actions;
  return mock;
};

describe('Mongoose Model: Session', () => {
  const mocks = {};
  const requireModel = (overrideMocks = {}) =>
    proxyquire('../../../app/services/mongoose/models/session.js', Object.assign({
      mongoose: mocks.mongoose,
      './data_log': mocks.dataLog
    }, overrideMocks));

  describe('in dev', () => {
    beforeEach(() => {
      delete mongoose.models.session;
      delete mongoose.modelSchemas.session;
      environment.reset(__dirname, ['../../../app/config', '../../../app/services/mongoose/models/session.js']);
      environment.use('dev');
      mocks.mongoose = MongooseMock();
      mocks.dataLog = DataLogMock();
    });

    afterEach(() => {
      delete mongoose.models.session;
      delete mongoose.modelSchemas.session;
    });

    it('should properly load', () => {
      const mongooseModel = requireModel();
      expect(mongooseModel).to.not.be.undefined;
    });

    describe('toJson()', () => {
      it('should export JSON', () => {
        const Session = requireModel();
        const session = new Session({
          token: '123456789',
          expiresAt: new Date(),
          data: {}
        });

        const json = session.toJson();

        expect(session).to.not.be.undefined;
        expect(json).to.not.be.undefined;
      });
    });

    describe('createSession()', () => {
      it('should properly create a session', () => {
        const mongooseModel = requireModel();
        sinon.stub(mongooseModel, 'remove').callsFake(() => Promise.resolve({}));
        sinon.stub(mongooseModel, 'create').callsFake(() => Promise.resolve({}));

        return mongooseModel.createSession({ username: 'test', type: 'session' }, null, { value: 'test' })
          .then((token) => {
            expect(token).to.not.be.undefined;

            const log = mocks.dataLog.getLog();
            expect(log.action).to.equal('create');
            // expect(log.newValue.token).to.not.be.undefined;
            // expect(log.newValue.data.username).to.equal('test');
            // expect(log.newValue.data.type).to.equal('session');
            expect(log.inMeta.value).to.equal('test');
          });
      });

      it('should properly create a session that expires', () => {
        const mongooseModel = requireModel();
        sinon.stub(mongooseModel, 'remove').callsFake(() => Promise.resolve({}));
        sinon.stub(mongooseModel, 'create').callsFake(() => Promise.resolve({}));

        return mongooseModel.createSession({ username: 'test', type: 'session' }, 10 * 60)
          .then((token) => {
            expect(token).to.not.be.undefined;
          });
      });
    });

    describe('getAndExtendSession()', () => {
      it('should properly extend a session that expires', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: 10 * 10 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndUpdate').callsFake(() => Promise.resolve({ token }));
        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve({ token }));

        return mongooseModel.getAndExtendSession(token, { value: 'test' })
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.not.be.undefined;
            expect(returnedValue.session.token).to.not.be.undefined;
            expect(returnedValue.session.token).to.equal(token);

            const log = mocks.dataLog.getLog();
            expect(log.action).to.equal('update');
            expect(log.newValue.token).to.equal(token);
            expect(log.inMeta.value).to.equal('test');
          });
      });

      it('should properly get a session that does not expire', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve({ token }));

        return mongooseModel.getAndExtendSession(token)
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.not.be.undefined;
            expect(returnedValue.session.token).to.not.be.undefined;
            expect(returnedValue.session.token).to.equal(token);
          });
      });

      it('should return undefined if no session is found', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: 10 * 10 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndUpdate').callsFake(() => Promise.resolve(null));

        return mongooseModel.getAndExtendSession(token, { value: 'test' })
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.be.undefined;
          });
      });
    });

    describe('getSession()', () => {
      it('should properly get a session', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve({ token }));

        return mongooseModel.getSession(token)
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.not.be.undefined;
            expect(returnedValue.session.token).to.not.be.undefined;
            expect(returnedValue.session.token).to.equal(token);
          });
      });

      it('should return undefined if no session is found', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve(null));

        return mongooseModel.getSession(token)
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.equal(undefined);
          });
      });
    });

    describe('deleteSession()', () => {
      it('should properly delete a session by token', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndRemove').callsFake(() => Promise.resolve({}));

        return mongooseModel.deleteSession(token, { value: 'test' })
          .then(() => {
            const log = mocks.dataLog.getLog();
            expect(log.action).to.equal('delete');
            expect(log.inMeta.value).to.equal('test');
          });
      });

      it('should return undefined if no session is found', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndRemove').callsFake(() => Promise.resolve(null));

        return mongooseModel.deleteSession(token, { value: 'test' })
          .then((results) => {
            expect(results).to.be.undefined;
          });
      });
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      delete mongoose.models.session;
      delete mongoose.modelSchemas.session;
      environment.reset(__dirname, ['../../../app/config', '../../../app/services/mongoose/models/session.js']);
      environment.use('production');
      mocks.mongoose = MongooseMock();
      mocks.dataLog = DataLogMock();
    });

    afterEach(() => {
      delete mongoose.models.session;
      delete mongoose.modelSchemas.session;
    });

    it('should properly load', () => {
      const mongooseModel = requireModel();
      expect(mongooseModel).to.not.be.undefined;
    });

    describe('toJson()', () => {
      it('should export JSON', () => {
        const Session = requireModel();
        const session = new Session({
          token: '123456789',
          expiresAt: new Date(),
          data: {}
        });

        const json = session.toJson();

        expect(session).to.not.be.undefined;
        expect(json).to.not.be.undefined;
      });
    });

    describe('createSession()', () => {
      it('should properly create a session', () => {
        const mongooseModel = requireModel();
        sinon.stub(mongooseModel, 'remove').callsFake(() => Promise.resolve({}));
        sinon.stub(mongooseModel, 'create').callsFake(() => Promise.resolve({}));

        return mongooseModel.createSession({ username: 'test', type: 'session' }, null, { value: 'test' })
          .then((token) => {
            expect(token).to.not.be.undefined;

            const log = mocks.dataLog.getLog();
            expect(log.action).to.equal('create');
            // expect(log.newValue.token).to.not.be.undefined;
            // expect(log.newValue.data.username).to.equal('test');
            // expect(log.newValue.data.type).to.equal('session');
            expect(log.inMeta.value).to.equal('test');
          });
      });

      it('should properly create a session that expires', () => {
        const mongooseModel = requireModel();
        sinon.stub(mongooseModel, 'remove').callsFake(() => Promise.resolve({}));
        sinon.stub(mongooseModel, 'create').callsFake(() => Promise.resolve({}));

        return mongooseModel.createSession({ username: 'test', type: 'session' }, 10 * 60)
          .then((token) => {
            expect(token).to.not.be.undefined;
          });
      });
    });

    describe('getAndExtendSession()', () => {
      it('should properly extend a session that expires', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: 10 * 10 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndUpdate').callsFake(() => Promise.resolve({ token }));
        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve({ token }));

        return mongooseModel.getAndExtendSession(token, { value: 'test' })
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.not.be.undefined;
            expect(returnedValue.session.token).to.not.be.undefined;
            expect(returnedValue.session.token).to.equal(token);

            const log = mocks.dataLog.getLog();
            expect(log.action).to.equal('update');
            expect(log.newValue.token).to.equal(token);
            expect(log.inMeta.value).to.equal('test');
          });
      });

      it('should properly get a session that does not expire', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve({ token }));

        return mongooseModel.getAndExtendSession(token)
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.not.be.undefined;
            expect(returnedValue.session.token).to.not.be.undefined;
            expect(returnedValue.session.token).to.equal(token);
          });
      });

      it('should return undefined if no session is found', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: 10 * 10 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndUpdate').callsFake(() => Promise.resolve(null));

        return mongooseModel.getAndExtendSession(token, { value: 'test' })
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.be.undefined;
          });
      });

      it('should use the default session timeout if not provided in token', () => {
        const expiresInSeconds = require('../../../app/config').SESSION.TIMEOUT_IN_SECONDS;
        const momentAddSpy = sinon.spy(momentProto, 'add');
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: null }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndUpdate').callsFake(() => Promise.resolve({ token }));
        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve({ token }));

        return mongooseModel.getAndExtendSession(token, { value: 'test' })
          .then(() => {
            expect(momentAddSpy).to.have.been.calledWith(expiresInSeconds, 'seconds');
          });
      });
    });

    describe('getSession()', () => {
      it('should properly get a session', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve({ token }));

        return mongooseModel.getSession(token)
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.not.be.undefined;
            expect(returnedValue.session.token).to.not.be.undefined;
            expect(returnedValue.session.token).to.equal(token);
          });
      });

      it('should return undefined if no session is found', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOne').callsFake(() => Promise.resolve(null));

        return mongooseModel.getSession(token)
          .then((returnedValue) => {
            expect(returnedValue).to.not.be.undefined;
            expect(returnedValue.session).to.equal(undefined);
          });
      });
    });

    describe('deleteSession()', () => {
      it('should properly delete a session by token', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndRemove').callsFake(() => Promise.resolve({}));

        return mongooseModel.deleteSession(token, { value: 'test' })
          .then(() => {
            const log = mocks.dataLog.getLog();
            expect(log.action).to.equal('delete');
            expect(log.inMeta.value).to.equal('test');
          });
      });

      it('should return undefined if no session is found', () => {
        const mongooseModel = requireModel();

        const token = jwt.sign({ expiresInSeconds: -1 }, 'secret');

        sinon.stub(mongooseModel, 'findOneAndRemove').callsFake(() => Promise.resolve(null));

        return mongooseModel.deleteSession(token, { value: 'test' })
          .then((results) => {
            expect(results).to.be.undefined;
          });
      });
    });
  });
});
