const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

const SourceFileBeneficiaryData = require('../../../app/services/mongoose/models/source_file_beneficiary_data');

describe('Mongoose Model: Source File Beneficiary Data', () => {
  beforeEach(() => {
    environment.use('dev');
  });

  describe('with existing targetCollection', () => {
    it('should export the correct model', () => {
      expect(SourceFileBeneficiaryData).to.not.be.undefined;
      expect(SourceFileBeneficiaryData.collection.collectionName).to.equal('source_file_beneficiary_datas');
    });

    it('should export JSON', () => {
      const model = new SourceFileBeneficiaryData({
        file_id: '1234',
        file_line_number: '22',
        entity_id: '33',
        dob: new Date('08/23/1980'),
        gender: '1',
        start_date: new Date(),
        end_date: new Date(),
        mbi: 'mbi'
      });

      const json = model.toJson();

      expect(model).to.not.be.undefined;
      expect(json).to.not.be.undefined;
    });
  });
});
