const chai = require('chai');
const moment = require('moment');
const diffHistory = require('mongoose-diff-history/diffHistory');
const RoleBuilder = require('ams-shared/test-utils/builders/RoleBuilder.js');
const PermissionBuilder = require('ams-shared/test-utils/builders/PermissionBuilder.js');
const Role = require('../../../app/services/mongoose/models/role');
const Permission = require('../../../app/services/mongoose/models/permission');
const History = require('../../../app/services/mongoose/models/history');

const expect = chai.expect;

describe('Mongoose Model: Role', () => {
  let roleBuilder;
  let permissionBuilder;
  let role;
  let permission1;
  let permission2;
  let now;

  beforeEach(async () => {
    now = moment();
    permissionBuilder = new PermissionBuilder().withConstructor(Permission);
    roleBuilder = new RoleBuilder().withConstructor(Role);

    permission1 = await permissionBuilder
      .fullyPopulated()
      .build()
      .save();
    permission2 = await permissionBuilder
      .fullyPopulated()
      .build()
      .save();
    role = roleBuilder.withPermissions([permission1, permission2]).build();
    await role.save();
  });

  afterEach(async () => {
    await Role.deleteMany();
    await Permission.deleteMany();
    await History.deleteMany();
  });

  function assertLoadedRole(loadedRole) {
    expect(loadedRole).to.not.be.undefined;
    expect(loadedRole.name).to.equal(role.name);
    expect(loadedRole.description).to.equal(role.description);
    expect(moment(loadedRole.createdAt).isSameOrAfter(now)).to.be.true;
    expect(moment(loadedRole.updatedAt).isSameOrAfter(now)).to.be.true;

    expect(loadedRole.permissions).to.be.ofSize(2);
    expect(loadedRole.permissions[0].name).to.equal(permission1.name);
    expect(loadedRole.permissions[0].description).to.equal(
      permission1.description
    );
    expect(loadedRole.permissions[1].name).to.equal(permission2.name);
    expect(loadedRole.permissions[1].description).to.equal(
      permission2.description
    );
  }

  it('should automatically populate permissions when you "findOne" Role', async () => {
    const loadedRole = await Role.findOne(role);
    assertLoadedRole(loadedRole);
  });

  it('should automatically populate permissions when you "find" a Role', async () => {
    const loadedRoles = await Role.find({ name: role.name });
    expect(loadedRoles).to.be.ofSize(1);
    assertLoadedRole(loadedRoles[0]);
  });

  it('should update a role and log the differences', async () => {
    role.description = 'New value';
    await role.save();

    const updatedRole = await Role.findOne(role);
    expect(updatedRole.description).to.equal('New value');

    // Assert we're keeping the history
    const histories = await diffHistory.getDiffs('role', updatedRole._id);
    expect(histories).to.be.ofSize(1);
    const history = histories[0];
    expect(history.collectionName).to.equal('role');
    expect(history.diff.description).to.be.ofSize(2);
    expect(history.diff.updatedAt).to.be.ofSize(2);
  });
});
