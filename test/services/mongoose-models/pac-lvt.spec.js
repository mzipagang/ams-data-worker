const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

let PacLvt;

describe('Mongoose lvt: PacLvt', () => {
  beforeEach(() => {
    environment.use('dev');
    PacLvt = require('../../../app/services/mongoose/models/pac_lvt');
  });

  it('should load', () => {
    expect(PacLvt).to.not.be.undefined;
  });

  it('should create a new instance of a LVT', () => {
    const lvt = new PacLvt();
    expect(lvt).to.not.be.undefined;
  });

  it('should allow creation with correct attributes', () => {
    const lvtValues = {
      entity_id: 'A1234',
      status: 'Y',
      patients: 12345.67,
      payments: 12345,
      small_status: 'N',
      complex_patient_score: '5',
      performance_year: 2017,
      run: 1
    };

    const lvt = new PacLvt(lvtValues);
    expect(lvt).to.not.be.undefined;
    expect(lvt.entity_id).to.equal('A1234');
    expect(lvt.status).to.equal('Y');
    expect(lvt.patients).to.equal(12345.67);
    expect(lvt.payments).to.equal(12345);
    expect(lvt.small_status).to.equal('N');
    expect(lvt.complex_patient_score).to.equal(5);
    expect(lvt.performance_year).to.equal(2017);
    expect(lvt.run).to.equal(1);
  });
});
