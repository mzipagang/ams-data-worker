const environment = require('../../environment');
const chai = require('chai');

const expect = chai.expect;

describe('Mongoose Dynamic Name Model', () => {
  require('../../../app/services/mongoose');
  const models = [
    require('../../../app/services/mongoose/dynamic_name_models/abridged_lvt'),
    require('../../../app/services/mongoose/dynamic_name_models/abridged_qp_score'),
    require('../../../app/services/mongoose/dynamic_name_models/abridged_qp_status'),
    require('../../../app/services/mongoose/dynamic_name_models/apm_entity'),
    require('../../../app/services/mongoose/dynamic_name_models/apm_entity_snapshot'),
    require('../../../app/services/mongoose/dynamic_name_models/apm_provider'),
    require('../../../app/services/mongoose/dynamic_name_models/apm_provider_snapshot'),
    require('../../../app/services/mongoose/dynamic_name_models/entity_eligibility'),
    require('../../../app/services/mongoose/dynamic_name_models/entity_qp_status'),
    require('../../../app/services/mongoose/dynamic_name_models/entity_qp_threshold'),
    require('../../../app/services/mongoose/dynamic_name_models/individual_qp_status'),
    require('../../../app/services/mongoose/dynamic_name_models/individual_qp_threshold'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_entity'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_lvt'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_model'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_model_snapshot'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_provider'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_qp_score'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_qp_status'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_subdivision'),
    require('../../../app/services/mongoose/dynamic_name_models/pac_subdivision_snapshot')
  ];

  beforeEach(() => {
    environment.use('dev');
  });

  it('should return a model with correct name', () => {
    models.forEach((model) => {
      const newModel = model('test');
      expect(newModel.collection.collectionName).to.match(/^(pac|apm|abridged|entity|individual)_[a-zA-Z0-9_]*_tests$/);

      const newModelCached = model('test');
      expect(newModelCached.collection.collectionName)
        .to.match(/^(pac|apm|abridged|entity|individual)_[a-zA-Z0-9_]*_tests$/);

      expect(newModel).to.equal(newModelCached);
    });
  });

  it('should return a model with correct name when using the cache', () => {
    models.forEach((model) => {
      const newModel = model('test');
      expect(newModel.collection.collectionName).to.match(/^(pac|apm|abridged|entity|individual)_[a-zA-Z0-9_]*_tests$/);

      const newModelCached = model('test');
      expect(newModelCached.collection.collectionName)
        .to.match(/^(pac|apm|abridged|entity|individual)_[a-zA-Z0-9_]*_tests$/);

      expect(newModel).to.equal(newModelCached);
    });
  });
});
