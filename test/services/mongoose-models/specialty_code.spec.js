const chai = require('chai');

const expect = chai.expect;

const specialtyCode = require('../../../app/services/mongoose/models/specialty_code');

describe('Mongoose Model: Specialty Codes', () => {
  it('should export properly', () => expect(specialtyCode).to.not.be.undefined);
});
