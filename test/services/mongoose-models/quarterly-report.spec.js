const chai = require('chai');

const expect = chai.expect;

const QuarterlyReport = require('../../../app/services/mongoose/models/quarterly_report');

const getReportQuarter = () => {
  const date = new Date();
  const year = date.getFullYear();
  const quarter1Start = new Date(year, 3, 21);
  const quarter1End = new Date(year, 4, 10, 23, 59, 59);
  const quarter2Start = new Date(year, 6, 21);
  const quarter2End = new Date(year, 7, 10, 23, 59, 59);
  const quarter3Start = new Date(year, 9, 21);
  const quarter3End = new Date(year, 10, 10, 23, 59, 59);
  const quarter4Start = new Date(year + 1, 0, 21);
  const quarter4End = new Date(year + 1, 1, 10, 23, 59, 59);

  if (date >= quarter1Start && date <= quarter1End) {
    return 1;
  } else if (date >= quarter2Start && date <= quarter2End) {
    return 2;
  } else if (date >= quarter3Start && date <= quarter3End) {
    return 3;
  } else if (date >= quarter4Start && date <= quarter4End) {
    return 4;
  }
  return 0;
};

const validateSaved = async (model) => {
  await model.save();
  const saved = await QuarterlyReport.findOne({ model_id: model.model_id }).lean();
  if (!model.quarter || saved.quarter !== model.quarter) {
    expect(saved).to.not.be.undefined;
    expect(saved.quarter).to.equal(model.quarter);
  }
};

const validateFailed = async (model) => {
  try {
    await model.save();
    expect.fail(null, null, 'Exception not caught');
  } catch (err) {
    expect(err).to.not.be.undefined;
  }
};

describe('Mongoose Model: QuarterlyReport', () => {
  describe('with existing targetCollection', () => {
    afterEach(async () => {
      await QuarterlyReport.deleteMany();
    });

    it('should export the correct model', () => {
      expect(QuarterlyReport).to.not.be.undefined;
      expect(QuarterlyReport.collection.collectionName).to.equal('quarterly_reports');
    });

    it('should allow valid quarter dates', async () => {
      const quarter = getReportQuarter();
      const model = new QuarterlyReport({
        model_id: '1234',
        quarter,
        model_name: 'Name'
      });

      if (!model.quarter || (model.quarter && quarter !== model.quarter)) {
        await validateFailed(model);
      } else {
        await validateSaved(model);
      }
    });

    it('should flag invalid quarter dates', async () => {
      const quarter = getReportQuarter();
      let model = new QuarterlyReport({
        model_id: '1234',
        quarter: 1,
        model_name: 'Name'
      });

      if (!model.quarter || (model.quarter && quarter !== model.quarter)) {
        await validateFailed(model);
      } else {
        await validateSaved(model);
      }

      model = new QuarterlyReport({
        model_id: '1234',
        quarter: 2,
        model_name: 'Name'
      });

      if (!model.quarter || (model.quarter && quarter !== model.quarter)) {
        await validateFailed(model);
      } else {
        await validateSaved(model);
      }

      model = new QuarterlyReport({
        model_id: '1234',
        quarter: 3,
        model_name: 'Name'
      });

      if (!model.quarter || (model.quarter && quarter !== model.quarter)) {
        await validateFailed(model);
      } else {
        await validateSaved(model);
      }

      model = new QuarterlyReport({
        model_id: '1234',
        quarter: 4,
        model_name: 'Name'
      });

      if (!model.quarter || (model.quarter && quarter !== model.quarter)) {
        await validateFailed(model);
      } else {
        await validateSaved(model);
      }

      model = new QuarterlyReport({
        model_id: '1234',
        quarter: 0,
        model_name: 'Name'
      });

      if (!model.quarter || (model.quarter && quarter !== model.quarter)) {
        await validateFailed(model);
      } else {
        await validateSaved(model);
      }
    });
  });
});
