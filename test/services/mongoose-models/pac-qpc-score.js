const chai = require('chai');
const environment = require('../../environment');

const expect = chai.expect;

let PacQpcScore;

describe('Mongoose Model: Pac QPC Score', () => {
  beforeEach(() => {
    environment.use('dev');
    PacQpcScore = require('../../../app/services/mongoose/models/pac_qp_category_score');
  });

  it('should load', () => {
    expect(PacQpcScore).to.not.be.undefined;
  });

  it('should create a new instance of a model', () => {
    const model = new PacQpcScore();
    expect(model).to.not.be.undefined;
  });
});
