const environment = require('../environment');
const chai = require('chai');

const expect = chai.expect;

describe('Service: Validation Helper', () => {
  const requireHelper = () => require('../../app/services/validation/helper');

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/validation/helper']);
      environment.use('dev');
    });

    it('should have created a new helper', () => {
      const helper = requireHelper();
      expect(helper).to.not.be.undefined;
    });

    it('should have summarized validations', () => {
      const helper = requireHelper();
      const validations = [
        {
          validations: [
            { errorCode: 'error1', validationType: 'HARD_VALIDATION' },
            { errorCode: 'error1', validationType: 'HARD_VALIDATION' },
            { errorCode: 'error2', validationType: 'SOFT_VALIDATION' }
          ],
          invalid: true
        },
        {
          validations: [{ errorCode: 'error2', validationType: 'SOFT_VALIDATION' }],
          invalid: false
        },
        { validations: [], invalid: false }
      ];
      const summary = helper.summarizeValidations(validations, [{ error: 'Could not parse row' }]);
      expect(summary.errorRecordCount).to.equals(2);
      expect(summary.warningRecordCount).to.equals(2);
      expect(summary.validRecordCount).to.equals(1);
      expect(summary.totalRecords).to.equals(4);
      expect(summary.validationErrors.HARD_VALIDATION[0].error).to.equals('error1');
      expect(summary.validationErrors.HARD_VALIDATION[1].error).to.equals('Could not parse row');
      expect(summary.validationErrors.HARD_VALIDATION[0].type).to.equals('HARD_VALIDATION');
      expect(summary.validationErrors.HARD_VALIDATION[0].rowsAffected).to.equals(2);
      expect(summary.validationErrors.SOFT_VALIDATION[0].error).to.equals('error2');
      expect(summary.validationErrors.SOFT_VALIDATION[0].type).to.equals('SOFT_VALIDATION');
      expect(summary.validationErrors.SOFT_VALIDATION[0].rowsAffected).to.equals(2);
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/services/logger']);
      environment.use('production');
    });

    it('should have created a new helper', () => {
      const helper = requireHelper();
      expect(helper).to.not.be.undefined;
    });

    it('should have summarized validations', () => {
      const helper = requireHelper();
      const validations = [
        {
          validations: [
            { errorCode: 'error1', validationType: 'HARD_VALIDATION' },
            { errorCode: 'error1', validationType: 'HARD_VALIDATION' },
            { errorCode: 'error2', validationType: 'SOFT_VALIDATION' }
          ],
          invalid: true
        },
        {
          validations: [{ errorCode: 'error2', validationType: 'SOFT_VALIDATION' }],
          invalid: false
        },
        { validations: [], invalid: false }
      ];
      const summary = helper.summarizeValidations(validations, [{ error: 'Could not parse row' }]);
      expect(summary.errorRecordCount).to.equals(2);
      expect(summary.warningRecordCount).to.equals(2);
      expect(summary.validRecordCount).to.equals(1);
      expect(summary.totalRecords).to.equals(4);
      expect(summary.validationErrors.HARD_VALIDATION[0].error).to.equals('error1');
      expect(summary.validationErrors.HARD_VALIDATION[1].error).to.equals('Could not parse row');
      expect(summary.validationErrors.HARD_VALIDATION[0].type).to.equals('HARD_VALIDATION');
      expect(summary.validationErrors.HARD_VALIDATION[0].rowsAffected).to.equals(2);
      expect(summary.validationErrors.SOFT_VALIDATION[0].error).to.equals('error2');
      expect(summary.validationErrors.SOFT_VALIDATION[0].type).to.equals('SOFT_VALIDATION');
      expect(summary.validationErrors.SOFT_VALIDATION[0].rowsAffected).to.equals(2);
    });
  });
});
