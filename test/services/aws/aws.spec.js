const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();


const expect = chai.expect;

let aws = {};

const readStreamMock = () => ({
  upload: () => ({
    on: (event, cb) => {
      if (event === 'uploaded') {
        cb();
      }
    }
  })
});

describe('AWS service', () => {
  beforeEach(async () => {
    aws = proxyquire(
      '../../../app/services/aws',
      { 's3-upload-stream': readStreamMock }
    );
  });

  afterEach(async () => {});

  describe('S3 Signed', () => {
    it('It should generate a url for a file', async () => {
      const url = aws.util.getSignedUrl('test.json');
      expect(url.substring(0, 7)).to.equal('http://');
    });
  });

  describe('S3 getFile', () => {
    it('It should get a file', async () => {
      const result = aws.util.getObject('test.json');
      expect(result).to.not.be.null;
    });
  });


  describe('S3 uploadFile', () => {
    it('It should upload a file', async () => {
      const result = aws.util.uploadS3('test.json', null, 'text/json', {});
      expect(result).to.not.be.null;
    });
  });
});
