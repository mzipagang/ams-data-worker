const environment = require('../environment');
const mongoose = require('mongoose');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();

const expect = chai.expect;
const MongooseMock = () => mongoose;

describe('Service: Mongoose', () => {
  const mocks = {};
  const requireMongoose = (overrideMocks = {}) =>
    proxyquire(
      '../../app/services/mongoose',
      Object.assign(
        {
          mongoose: mocks.mongoose
        },
        overrideMocks
      )
    );

  describe('in dev', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/config', '../../app/services/mongoose']);
      environment.use('dev');
      mocks.mongoose = MongooseMock();
    });

    it('should properly load', () => {
      const mongooseService = requireMongoose();
      expect(mongooseService).to.not.be.undefined;
    });

    it('should properly load models', () => {
      const mongooseService = requireMongoose();

      expect(mongooseService).to.not.be.undefined;
      expect(mongooseService.model('pac_model')).to.not.be.undefined;
      expect(mongooseService.model('pac_entity')).to.not.be.undefined;
      expect(mongooseService.model('pac_provider')).to.not.be.undefined;
      expect(mongooseService.model('pac_subdivision')).to.not.be.undefined;
      expect(mongooseService.model.bind(mongoose, 'fakemodel')).to.throw(Error);
    });
  });

  describe('in production', () => {
    beforeEach(() => {
      environment.reset(__dirname, ['../../app/config', '../../app/services/mongoose']);
      environment.use('production');
      mocks.mongoose = MongooseMock();
    });

    it('should properly load', () => {
      const mongooseService = requireMongoose();
      expect(mongooseService).to.not.be.undefined;
    });

    it('should properly load models', () => {
      const mongooseService = requireMongoose();

      expect(mongooseService).to.not.be.undefined;
      expect(mongooseService.model('pac_model')).to.not.be.undefined;
      expect(mongooseService.model('pac_entity')).to.not.be.undefined;
      expect(mongooseService.model('pac_provider')).to.not.be.undefined;
      expect(mongooseService.model('pac_subdivision')).to.not.be.undefined;
      expect(mongooseService.model.bind(mongoose, 'fakemodel')).to.throw(Error);
    });
  });

  describe('Mutex', () => {
    it('it should return false on trying to get second mutex', async () => {
      await mongoose.getMutex();
      const second = await mongoose.tryGetMutex();
      expect(second).to.equal(false);
      await mongoose.releaseMutex();
    });
  });

  describe('Mutex', () => {
    it('it wait on second mutex', async () => {
      await mongoose.getMutex();
      await mongoose.getMutex(1, 2);
      await mongoose.releaseMutex();
    });
  });

  describe('migrations', () => {
    it('it should load migrations', async () => {
      expect(mongoose.ams_migrations.length).to.be.greaterThan(0);
    });
  });
});
