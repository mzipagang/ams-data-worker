const PacPublishHistoryUtils = require('../../app/services/pacPublishHistoryUtils');
const chai = require('chai');

const expect = chai.expect;

describe('Service: pacPublishHistoryUtils', () => {
  it('it should retrieve data by performance year',
    () => PacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(2017).then((response) => {
      expect(response).to.not.be.undefined;
      expect(response.allPACDataExists).to.equal(true);
    }));

  it('it should retrieve data by publish id',
    () => PacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(2017,
      '8e428136-bdbc-48fb-a80c-72b86cce3d88').then((response) => {
      expect(response).to.not.be.undefined;
      expect(response.allPACDataExists).to.equal(true);
    }));

  it('it should return if no information is found',
    () => PacPublishHistoryUtils.getLatestPacFilesByPerformanceYear(2016).then((response) => {
      expect(response).to.not.be.undefined;
      expect(response.allPACDataExists).to.equal(false);
    }));
  it('it should return static collections if no information is found',
    () => PacPublishHistoryUtils.getLatestPacCollectionsByPerformanceYear(2016).then((response) => {
      expect(response).to.not.be.undefined;
      expect(response.allPACDataExists).to.equal(false);
    }));
});
