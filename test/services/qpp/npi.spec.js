const sinon = require('sinon');
require('sinon-mongoose');
const QppNPI = require('../../../app/services/qpp/npi');
const QppRedis = require('../../../app/services/qppRedis');
const PacPublishHistoryUtils = require('../../../app/services/pacPublishHistoryUtils');
const chai = require('chai');

const expect = chai.expect;

const PacModelDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_model');
const PacSubdivisionDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_subdivision');
const PacEntityDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_entity');
const PacProviderDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_provider');
const PacLVTDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_lvt');
const PacQpStatusDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_status');
const PacQpScoreDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_score');

const DynamicNamePacModel = PacModelDynamic('1');
const DynamicNamePacSubdivision = PacSubdivisionDynamic('2');
const DynamicNamePacEntity = PacEntityDynamic('3');
const DynamicNamePacProvider = PacProviderDynamic('4');
const DynamicNamePacLVT = PacLVTDynamic('5');
const DynamicNamePacQpStatus = PacQpStatusDynamic('6');
const DynamicNamePacQpScore = PacQpScoreDynamic('7');

const config = require('../../../app/config');

describe('Service: qpp/npi.js', () => {
  let PacPublishHistoryUtilsMock;
  let QppRedisMock;

  let DynamicNamePacModelMock;
  let DynamicNamePacSubdivisionMock;
  let DynamicNamePacEntityMock;
  let DynamicNamePacProviderMock;
  let DynamicNamePacLVTMock;
  let DynamicNamePacQpStatusMock;
  let DynamicNamePacQpScoreMock;

  beforeEach(() => {
    PacPublishHistoryUtilsMock = sinon.mock(PacPublishHistoryUtils);
    QppRedisMock = sinon.mock(QppRedis);

    DynamicNamePacModelMock = sinon.mock(DynamicNamePacModel);
    DynamicNamePacSubdivisionMock = sinon.mock(DynamicNamePacSubdivision);
    DynamicNamePacEntityMock = sinon.mock(DynamicNamePacEntity);
    DynamicNamePacProviderMock = sinon.mock(DynamicNamePacProvider);
    DynamicNamePacLVTMock = sinon.mock(DynamicNamePacLVT);
    DynamicNamePacQpStatusMock = sinon.mock(DynamicNamePacQpStatus);
    DynamicNamePacQpScoreMock = sinon.mock(DynamicNamePacQpScore);

    config.CACHE.ENABLED = true;
  });

  afterEach(() => {
    PacPublishHistoryUtilsMock.restore();
    QppRedisMock.restore();

    DynamicNamePacModelMock.restore();
    DynamicNamePacSubdivisionMock.restore();
    DynamicNamePacEntityMock.restore();
    DynamicNamePacProviderMock.restore();
    DynamicNamePacLVTMock.restore();
    DynamicNamePacQpStatusMock.restore();
    DynamicNamePacQpScoreMock.restore();

    config.CACHE.ENABLED = true;
  });

  it('should return an empty array when nothing has been published', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        allPACDataExists: false
      });

    return QppNPI(['1234567890'], null)
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(0);
      });
  });

  it('should return data loaded from the cache', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: {},
        pacSubdivisionFile: {},
        pacEntityFile: {},
        pacProviderFile: {},
        pacLvtFile: {},
        pacQpStatusFile: {},
        pacQpScoreFile: {},
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    const cached = {
      npi: '1234567890',
      tins: [
        {
          tin: '123456789',
          entities: [
            {
              entity_tin: '123456789'
            }
          ]
        },
        {
          tin: null,
          entities: [
            {
              entity_tin: null
            }
          ]
        }
      ]
    };

    QppRedisMock
      .expects('getByNpi')
      .withExactArgs('1234567890', '1', 2017)
      .once()
      .resolves(cached);

    return QppNPI(['1234567890'])
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
        expect(result[0]).to.deep.equal(cached);
      });
  });

  it('should return data loaded from mongo when cache is disabled', () => {
    config.CACHE.ENABLED = false;

    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    QppRedisMock
      .expects('getByNpi')
      .withExactArgs('1234567890', '1', 2017)
      .once()
      .resolves(null);

    QppRedisMock
      .expects('setByNpi')
      .withArgs('1234567890', '1', 2017)
      .once()
      .resolves(null);

    DynamicNamePacProviderMock
      .expects('aggregate')
      .once()
      .resolves([
        {
          npi: '1234567890',
          data: [
            {
              entity_id: '1',
              tin: '123456789',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {

              },
              model_data: {
                advanced_apm_flag: 'P',
                mips_apm_flag: 'P'
              },
              lvt_data: {
                status: 'A'
              },
              qp_score_data: {

              }
            },
            {
              entity_id: '2',
              tin: '234567890',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {
              },
              model_data: {
              },
              lvt_data: {
                status: 'B'
              },
              subdivision_data: {
              },
              qp_score_data: {
              }
            },
            {
              entity_id: '3',
              tin: '234567890',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {
              },
              model_data: {
              },
              lvt_data: {
                status: 'Y'
              },
              subdivision_data: {
              },
              qp_score_data: {
              }
            },
            {
              entity_id: '4',
              tin: '234567890',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {
              },
              model_data: {
              },
              lvt_data: {
                status: 'N'
              },
              subdivision_data: {
              },
              qp_score_data: {
              }
            },
            {
              entity_id: '5',
              tin: '234567890',
              npi: '1234567890'
            }
          ]
        }
      ]);

    return QppNPI(['1234567890'])
      .then((result) => {
        // cached.tins.forEach(tinData => {
        //   tinData.tin = crypto.decrypt(tinData.tin) || null;
        //   tinData.entities.forEach((entityData) => {
        //     entityData.entity_tin = crypto.decrypt(entityData.entity_tin) || null;
        //   });
        // });


        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
      });
  });

  it('should return data loaded from mongo', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    QppRedisMock
      .expects('getByNpi')
      .withExactArgs('1234567890', '1', 2017)
      .once()
      .resolves(null);

    QppRedisMock
      .expects('setByNpi')
      .withArgs('1234567890', '1', 2017)
      .once()
      .resolves(null);

    DynamicNamePacProviderMock
      .expects('aggregate')
      .once()
      .resolves([
        {
          npi: '1234567890',
          data: [
            {
              entity_id: '1',
              tin: '123456789',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {

              },
              model_data: {
                advanced_apm_flag: 'P',
                mips_apm_flag: 'P'
              },
              lvt_data: {
                status: 'A'
              },
              qp_score_data: {

              }
            },
            {
              entity_id: '2',
              tin: '234567890',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {
              },
              model_data: {
              },
              lvt_data: {
                status: 'B'
              },
              subdivision_data: {
              },
              qp_score_data: {
              }
            },
            {
              entity_id: '3',
              tin: '234567890',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {
              },
              model_data: {
              },
              lvt_data: {
                status: 'Y'
              },
              subdivision_data: {
              },
              qp_score_data: {
              }
            },
            {
              entity_id: '4',
              tin: '234567890',
              npi: '1234567890',
              provider_relationship_code: 'P',
              entity_data: {
              },
              model_data: {
              },
              lvt_data: {
                status: 'N'
              },
              subdivision_data: {
              },
              qp_score_data: {
              }
            },
            {
              entity_id: '5',
              tin: '234567890',
              npi: '1234567890'
            }
          ]
        }
      ]);

    // Should handle duplicate NPI's by removing them
    return QppNPI(['1234567890', '1234567890'])
      .then((result) => {
        // cached.tins.forEach(tinData => {
        //   tinData.tin = crypto.decrypt(tinData.tin) || null;
        //   tinData.entities.forEach((entityData) => {
        //     entityData.entity_tin = crypto.decrypt(entityData.entity_tin) || null;
        //   });
        // });


        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
      });
  });

  it('should resolve an error when an error is thrown', () => {
    const error = new Error('Fake Error');

    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .throws(error);

    return QppNPI(['1234567890'])
      .then(() => {
        throw new Error('Should not have gotten here');
      })
      .catch((err) => {
        if (err !== error) {
          throw err;
        }
      });
  });
});
