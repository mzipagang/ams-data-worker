const sinon = require('sinon');
require('sinon-mongoose');
const QppTIN = require('../../../app/services/qpp/tin');
const QppRedis = require('../../../app/services/qppRedis');
const PacPublishHistoryUtils = require('../../../app/services/pacPublishHistoryUtils');
const chai = require('chai');

const expect = chai.expect;
const config = require('../../../app/config');

const PacModelDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_model');
const PacSubdivisionDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_subdivision');
const PacEntityDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_entity');
const PacProviderDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_provider');
const PacLVTDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_lvt');
const PacQpStatusDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_status');
const PacQpScoreDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_score');

const DynamicNamePacModel = PacModelDynamic('1');
const DynamicNamePacSubdivision = PacSubdivisionDynamic('2');
const DynamicNamePacEntity = PacEntityDynamic('3');
const DynamicNamePacProvider = PacProviderDynamic('4');
const DynamicNamePacLVT = PacLVTDynamic('5');
const DynamicNamePacQpStatus = PacQpStatusDynamic('6');
const DynamicNamePacQpScore = PacQpScoreDynamic('7');

describe('Service: qpp/tin.js', () => {
  let PacPublishHistoryUtilsMock;
  let QppRedisMock;

  let DynamicNamePacModelMock;
  let DynamicNamePacSubdivisionMock;
  let DynamicNamePacEntityMock;
  let DynamicNamePacProviderMock;
  let DynamicNamePacLVTMock;
  let DynamicNamePacQpStatusMock;
  let DynamicNamePacQpScoreMock;

  beforeEach(() => {
    PacPublishHistoryUtilsMock = sinon.mock(PacPublishHistoryUtils);
    QppRedisMock = sinon.mock(QppRedis);

    DynamicNamePacModelMock = sinon.mock(DynamicNamePacModel);
    DynamicNamePacSubdivisionMock = sinon.mock(DynamicNamePacSubdivision);
    DynamicNamePacEntityMock = sinon.mock(DynamicNamePacEntity);
    DynamicNamePacProviderMock = sinon.mock(DynamicNamePacProvider);
    DynamicNamePacLVTMock = sinon.mock(DynamicNamePacLVT);
    DynamicNamePacQpStatusMock = sinon.mock(DynamicNamePacQpStatus);
    DynamicNamePacQpScoreMock = sinon.mock(DynamicNamePacQpScore);
  });

  afterEach(() => {
    PacPublishHistoryUtilsMock.restore();
    QppRedisMock.restore();

    DynamicNamePacModelMock.restore();
    DynamicNamePacSubdivisionMock.restore();
    DynamicNamePacEntityMock.restore();
    DynamicNamePacProviderMock.restore();
    DynamicNamePacLVTMock.restore();
    DynamicNamePacQpStatusMock.restore();
    DynamicNamePacQpScoreMock.restore();
    config.CACHE.ENABLED = true;
  });

  it('should return an empty array when nothing has been published', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        allPACDataExists: false
      });

    return QppTIN(['123456789'], null)
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(0);
      });
  });

  it('should return data loaded from the cache', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    const cached = {
      tin: '123456789',
      entities: [
        {
          tin: '123456789'
        }
      ]
    };

    QppRedisMock
      .expects('getByTin')
      .withExactArgs('123456789', '1', 2017)
      .once()
      .resolves(cached);

    return QppTIN(['123456789'])
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
      });
  });

  it('should consider all tins as nonCached if config.CACHE.ENABLED is false', () => {
    config.CACHE.ENABLED = false;
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    DynamicNamePacProviderMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          tin: '123456789',
          npi: '1234567890',
          entity_id: 'A1001'
        },
        {
          tin: '123456789',
          npi: '2345678901',
          entity_id: 'A1002'
        },
        {
          tin: '123456789',
          npi: '3456789012',
          entity_id: 'A1003'
        },
        {
          tin: '123456789',
          npi: '4567890123',
          entity_id: 'A1004'
        },
        {
          tin: '123456789',
          npi: '5678901234',
          entity_id: 'A1005'
        },
        {
          tin: '123456789',
          npi: '6789012345',
          entity_id: 'A1006'
        },
        {
          tin: '123456789',
          npi: '7890123456',
          entity_id: 'A1007'
        }
      ]);

    DynamicNamePacEntityMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: 'A1001',
          apm_id: '01',
          subdiv_id: '01'
        },
        {
          id: 'A1002',
          apm_id: '02',
          subdiv_id: '02'
        },
        {
          id: 'A1003',
          apm_id: '03',
          subdiv_id: '03'
        },
        {
          id: 'A1004',
          apm_id: '04',
          subdiv_id: '04'
        },
        {
          id: 'A1005',
          apm_id: '05',
          subdiv_id: '05'
        },
        {
          id: 'A1006',
          apm_id: '05',
          subdiv_id: '05'
        }
      ]);

    DynamicNamePacModelMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: '01',
          advanced_apm_flag: 'P',
          mips_apm_flag: 'P'
        },
        {
          id: '02',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        },
        {
          id: '03',
          advanced_apm_flag: 'N',
          mips_apm_flag: 'N'
        },
        {
          id: '04'
        },
        {
          id: '05'
        }
      ]);

    DynamicNamePacSubdivisionMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: '01',
          apm_id: '01',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        },
        {
          id: '02',
          apm_id: '02'
        },
        {
          id: '03',
          apm_id: '03'
        },
        {
          id: '04',
          apm_id: '04'
        },
        {
          id: '05',
          apm_id: '05'
        }
      ]);

    DynamicNamePacLVTMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          entity_id: 'A1001',
          status: 'Y'
        },
        {
          entity_id: 'A1002',
          status: 'N'
        },
        {
          entity_id: 'A1003',
          status: 'A'
        },
        {
          entity_id: 'A1004',
          status: 'B'
        }
      ]);

    DynamicNamePacQpStatusMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          npi: '1234567890'
        },
        {
          npi: '2345678901'
        },
        {
          npi: '3456789012'
        },
        {
          npi: '4567890123'
        },
        {
          npi: '5678901234'
        }
      ]);

    DynamicNamePacQpScoreMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          npi: '1234567890',
          tin: '123456789',
          entity_id: 'A1001'
        },
        {
          npi: '2345678901',
          tin: '123456789',
          entity_id: 'A1002'
        },
        {
          npi: '3456789012',
          tin: '123456789',
          entity_id: 'A1003'
        },
        {
          npi: '4567890123',
          tin: '123456789',
          entity_id: 'A1004'
        },
        {
          npi: '5678901234',
          tin: '123456789',
          entity_id: 'A1005'
        }
      ]);

    return QppTIN(['123456789'])
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
      });
  });

  it('should remove repeated providers, entities, subdivisions with the same npi', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    DynamicNamePacProviderMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          tin: '123456789',
          npi: '1234567890',
          entity_id: 'A1001'
        },
        {
          tin: '123456789',
          npi: '1234567890',
          entity_id: 'A1001'
        },
        {
          tin: '123456789',
          npi: '2345678901',
          entity_id: 'A1002'
        },
        {
          tin: '123456789',
          npi: '3456789012',
          entity_id: 'A1003'
        },
        {
          tin: '123456789',
          npi: '4567890123',
          entity_id: 'A1004'
        },
        {
          tin: '123456789',
          npi: '5678901234',
          entity_id: 'A1005'
        },
        {
          tin: '123456789',
          npi: '6789012345',
          entity_id: 'A1006'
        },
        {
          tin: '123456789',
          npi: '7890123456',
          entity_id: 'A1007'
        }
      ]);

    DynamicNamePacEntityMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: 'A1001',
          apm_id: '01',
          subdiv_id: '01'
        },
        {
          id: 'A1001',
          apm_id: '01',
          subdiv_id: '01'
        },
        {
          id: 'A1002',
          apm_id: '02',
          subdiv_id: '02'
        },
        {
          id: 'A1003',
          apm_id: '03',
          subdiv_id: '03'
        },
        {
          id: 'A1004',
          apm_id: '04',
          subdiv_id: '04'
        },
        {
          id: 'A1005',
          apm_id: '05',
          subdiv_id: '05'
        },
        {
          id: 'A1006',
          apm_id: '05',
          subdiv_id: '05'
        }
      ]);

    DynamicNamePacModelMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: '01',
          advanced_apm_flag: 'P',
          mips_apm_flag: 'P'
        },
        {
          id: '02',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        },
        {
          id: '03',
          advanced_apm_flag: 'N',
          mips_apm_flag: 'N'
        },
        {
          id: '04'
        },
        {
          id: '05'
        }
      ]);

    DynamicNamePacSubdivisionMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: '01',
          apm_id: '01',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        },
        {
          id: '01',
          apm_id: '01',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        },
        {
          id: '02',
          apm_id: '02'
        },
        {
          id: '03',
          apm_id: '03'
        },
        {
          id: '04',
          apm_id: '04'
        },
        {
          id: '05',
          apm_id: '05'
        }
      ]);

    DynamicNamePacLVTMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          entity_id: 'A1001',
          status: 'Y'
        },
        {
          entity_id: 'A1002',
          status: 'N'
        },
        {
          entity_id: 'A1003',
          status: 'A'
        },
        {
          entity_id: 'A1004',
          status: 'B'
        }
      ]);

    DynamicNamePacQpStatusMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          npi: '1234567890'
        },
        {
          npi: '2345678901'
        },
        {
          npi: '3456789012'
        },
        {
          npi: '4567890123'
        },
        {
          npi: '5678901234'
        }
      ]);

    DynamicNamePacQpScoreMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          npi: '1234567890',
          tin: '123456789',
          entity_id: 'A1001'
        },
        {
          npi: '2345678901',
          tin: '123456789',
          entity_id: 'A1002'
        },
        {
          npi: '3456789012',
          tin: '123456789',
          entity_id: 'A1003'
        },
        {
          npi: '4567890123',
          tin: '123456789',
          entity_id: 'A1004'
        },
        {
          npi: '5678901234',
          tin: '123456789',
          entity_id: 'A1005'
        }
      ]);

    return QppTIN(['123456789'])
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
      });
  });

  it('should return an empty entities array if no ids are found', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    DynamicNamePacProviderMock
      .expects('find')
      .chain('lean')
      .resolves([]);


    return QppTIN(['123456789'])
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
        expect(result[0].entities.length).to.equal(0);
      });
  });

  it('should return data loaded from mongo', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    QppRedisMock
      .expects('getByTin')
      .withExactArgs('123456789', '1', 2017)
      .once()
      .resolves(null);

    DynamicNamePacProviderMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          tin: '123456789',
          npi: '1234567890',
          entity_id: 'A1001'
        },
        {
          tin: '123456789',
          npi: '2345678901',
          entity_id: 'A1002'
        },
        {
          tin: '123456789',
          npi: '3456789012',
          entity_id: 'A1003'
        },
        {
          tin: '123456789',
          npi: '4567890123',
          entity_id: 'A1004'
        },
        {
          tin: '123456789',
          npi: '5678901234',
          entity_id: 'A1005'
        },
        {
          tin: '123456789',
          npi: '6789012345',
          entity_id: 'A1006'
        },
        {
          tin: '123456789',
          npi: '7890123456',
          entity_id: 'A1007'
        }
      ]);

    DynamicNamePacEntityMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: 'A1001',
          apm_id: '01',
          subdiv_id: '01'
        },
        {
          id: 'A1002',
          apm_id: '02',
          subdiv_id: '02'
        },
        {
          id: 'A1003',
          apm_id: '03',
          subdiv_id: '03'
        },
        {
          id: 'A1004',
          apm_id: '04',
          subdiv_id: '04'
        },
        {
          id: 'A1005',
          apm_id: '05',
          subdiv_id: '05'
        },
        {
          id: 'A1006',
          apm_id: '05',
          subdiv_id: '05'
        }
      ]);

    DynamicNamePacModelMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: '01',
          advanced_apm_flag: 'P',
          mips_apm_flag: 'P'
        },
        {
          id: '02',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        },
        {
          id: '03',
          advanced_apm_flag: 'N',
          mips_apm_flag: 'N'
        },
        {
          id: '04'
        },
        {
          id: '05'
        }
      ]);

    DynamicNamePacSubdivisionMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          id: '01',
          apm_id: '01',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        },
        {
          id: '02',
          apm_id: '02'
        },
        {
          id: '03',
          apm_id: '03'
        },
        {
          id: '04',
          apm_id: '04'
        },
        {
          id: '05',
          apm_id: '05'
        }
      ]);

    DynamicNamePacLVTMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          entity_id: 'A1001',
          status: 'Y'
        },
        {
          entity_id: 'A1002',
          status: 'N'
        },
        {
          entity_id: 'A1003',
          status: 'A'
        },
        {
          entity_id: 'A1004',
          status: 'B'
        }
      ]);

    DynamicNamePacQpStatusMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          npi: '1234567890'
        },
        {
          npi: '2345678901'
        },
        {
          npi: '3456789012'
        },
        {
          npi: '4567890123'
        },
        {
          npi: '5678901234'
        }
      ]);

    DynamicNamePacQpScoreMock
      .expects('find')
      .chain('lean')
      .resolves([
        {
          npi: '1234567890',
          tin: '123456789',
          entity_id: 'A1001'
        },
        {
          npi: '2345678901',
          tin: '123456789',
          entity_id: 'A1002'
        },
        {
          npi: '3456789012',
          tin: '123456789',
          entity_id: 'A1003'
        },
        {
          npi: '4567890123',
          tin: '123456789',
          entity_id: 'A1004'
        },
        {
          npi: '5678901234',
          tin: '123456789',
          entity_id: 'A1005'
        }
      ]);

    // Should handle duplicate TIN's by removing them
    return QppTIN([' 123456789', '123456789'])
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
      });
  });

  it('should resolve an error when an error is thrown', () => {
    const error = new Error('Fake Error');

    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .throws(error);

    return QppTIN(['123456789'])
      .then(() => {
        throw new Error('Should not have gotten here');
      })
      .catch((err) => {
        if (err !== error) {
          throw err;
        }
      });
  });
});
