const CreateQppEntityCollection = require('../../../app/services/qpp/createQppEntityCollection');
const pacPublishHistoryUtils = require('../../../app/services/pacPublishHistoryUtils');
const QppEntity = require('../../../app/services/mongoose/models/qpp_entity');
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);

describe('CreateQppEntityCollection', () => {
  before(async () => {
    const {
      PACModel,
      PACSubdivision,
      PACEntity,
      PACProvider,
      PACLvt,
      PACQPScore,
      PACQPStatus
    } = await pacPublishHistoryUtils.getLatestPacCollectionsByPerformanceYear(2017);

    await PACEntity.insertMany([
      {
        id: 'Z1001',
        tin: '123456789',
        name: 'name',
        apm_id: '50',
        subdiv_id: '50'
      },
      {
        id: 'Z1002',
        tin: '123456789',
        name: 'name',
        apm_id: '51',
        subdiv_id: '51'
      },
      {
        id: 'Z1003',
        tin: '123456789',
        name: 'name',
        apm_id: '52',
        subdiv_id: '52'
      },
      {
        id: 'Z1004',
        tin: '123456789',
        name: 'name',
        apm_id: '52',
        subdiv_id: '52'
      },
      {
        id: 'Z1005',
        tin: '123456789',
        name: 'name',
        apm_id: '52',
        subdiv_id: '52'
      },
      {
        id: 'Z1006',
        tin: '123456789',
        name: 'name',
        apm_id: '52',
        subdiv_id: '52'
      }
    ]);

    await PACLvt.insertMany([
      {
        entity_id: 'Z1001',
        status: 'Y',
        payments: 1,
        patients: 1,
        small_status: 'Y',
        performance_year: 2017
      },
      {
        entity_id: 'Z1002',
        status: 'N',
        payments: 1,
        patients: 1,
        small_status: 'Y',
        performance_year: 2017
      },
      {
        entity_id: 'Z1003',
        status: 'A',
        payments: 1,
        patients: 1,
        small_status: 'N',
        performance_year: 2017
      },
      {
        entity_id: 'Z1004',
        status: 'B',
        payments: 1,
        patients: 1,
        small_status: 'Y',
        performance_year: 2017
      },
      {
        entity_id: 'Z1005',
        status: 'Y',
        payments: 1,
        patients: 1,
        small_status: 'N',
        performance_year: 2017
      }
    ]);

    await PACModel.insertMany([
      {
        id: '50',
        name: 'name',
        advanced_apm_flag: 'P',
        mips_apm_flag: 'P'
      },
      {
        id: '51',
        name: 'name',
        advanced_apm_flag: 'N',
        mips_apm_flag: 'N'
      },
      {
        id: '52',
        name: 'name',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y'
      }
    ]);

    await PACSubdivision.insertMany([
      {
        id: '50',
        apm_id: '50',
        name: 'name',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y'
      },
      {
        id: '51',
        apm_id: '51',
        name: 'name',
        advanced_apm_flag: 'N',
        mips_apm_flag: 'N'
      },
      {
        id: '52',
        apm_id: '52',
        name: 'name',
        advanced_apm_flag: 'P',
        mips_apm_flag: 'P'
      }
    ]);

    await PACProvider.insertMany([
      {
        entity_id: 'Z1001',
        duplicate: false,
        npi: '1234567890',
        tin: '987654321'
      },
      {
        entity_id: 'Z1002',
        duplicate: false,
        npi: '1234567890',
        tin: '987654322'
      },
      {
        entity_id: 'Z1003',
        duplicate: false,
        npi: '1234567890',
        tin: '987654323'
      },
      {
        entity_id: 'Z1004',
        duplicate: false,
        npi: '1234567890',
        tin: '987654324'
      },
      {
        entity_id: 'Z1005',
        duplicate: false,
        npi: '1234567890',
        tin: '987654325'
      }
    ]);

    await PACQPScore.insertMany([
      {
        entity_id: 'Z1001',
        npi: '1234567890',
        tin: '987654321',
        provider_relationship_code: 'P',
        qp_status: 'Y',
        qp_payment_threshold_score: 1,
        qp_patient_threshold_score: 1
      },
      {
        entity_id: 'Z1002',
        npi: '1234567891',
        tin: '987654322'
      },
      {
        entity_id: 'Z1003',
        npi: '1234567890',
        tin: '987654323',
        qp_status: 'Y',
        qp_payment_threshold_score: 1,
        qp_patient_threshold_score: 1
      },
      {
        entity_id: 'Z1004',
        npi: '1234567890',
        tin: '987654324',
        qp_status: 'Y',
        qp_payment_threshold_score: 1,
        qp_patient_threshold_score: 1
      },
      {
        entity_id: 'Z1005',
        npi: '1234567890',
        tin: '987654325',
        qp_status: 'Y',
        qp_payment_threshold_score: 1,
        qp_patient_threshold_score: 1
      }
    ]);

    await PACQPStatus.insertMany([{ npi: '1234567890', qp_status: 'Y' }]);
  });

  beforeEach(async () => {
    await QppEntity.remove();
  });

  describe('#getPerformanceYears', () => {
    it('should get performance years', async () => {
      const helper = CreateQppEntityCollection;
      const py = await helper.getPerformanceYears();
      expect(py.length).to.equal(1);
    });
  });

  describe('#getEntityList', () => {
    it('should get entities', async () => {
      const py = await CreateQppEntityCollection.getPerformanceYears();
      const entities = await CreateQppEntityCollection.getEntityList(py[0]);
      expect(entities.length).to.be.greaterThan(0);
    });
  });

  describe('#getQppEntitiesByPerformanceYear', () => {
    it('should get entities by performance year', async () => {
      const py = await CreateQppEntityCollection.getPerformanceYears();
      const entityList = await CreateQppEntityCollection.getEntityList(py[0]);
      const entityIds = entityList.map(entity => entity.id);
      const entities = await CreateQppEntityCollection.getQppEntitiesByPerformanceYear(entityIds, 2017);
      expect(entities.length).to.be.greaterThan(0);
    });

    it('should take in a null performance year', async () => {
      const py = await CreateQppEntityCollection.getPerformanceYears();
      const entityList = await CreateQppEntityCollection.getEntityList(py[0]);
      const entityIds = entityList.map(entity => entity.id);
      const entities = await CreateQppEntityCollection.getQppEntitiesByPerformanceYear(entityIds);
      expect(entities.length).to.be.greaterThan(0);
    });

    it('should return empty array if not all pac collections exist', async () => {
      const pacPublishHistoryUtilsStub = sinon.stub(pacPublishHistoryUtils, 'getLatestPacFilesByPerformanceYear')
        .callsFake(() => Promise.resolve({ allPACDataExists: false }));
      const py = await CreateQppEntityCollection.getPerformanceYears();
      const entityList = await CreateQppEntityCollection.getEntityList(py[0]);
      const entityIds = entityList.map(entity => entity.id);
      const entities = await CreateQppEntityCollection.getQppEntitiesByPerformanceYear(entityIds, 2017);
      expect(entities.length).to.equal(0);
      pacPublishHistoryUtilsStub.restore();
    });
  });

  it('should save entities', async () => {
    const py = await CreateQppEntityCollection.getPerformanceYears();
    const entityList = await CreateQppEntityCollection.getEntityList(py[0]);
    const entityIds = entityList.map(entity => entity.id);
    const entities = await CreateQppEntityCollection.getQppEntitiesByPerformanceYear(entityIds, 2017);
    await CreateQppEntityCollection.saveEntities(entities);
    const qppEntities = await QppEntity.find({ entity_id: 'entity-1' }).lean();
    expect(qppEntities[0].entity_id).to.equal('entity-1');
    expect(qppEntities[0].entity_name).to.equal('Entity 1');
    expect(qppEntities[0].lvt_flag).to.equal('F');
    expect(qppEntities[0].lvt_payments).to.equal(440959.1);
    expect(qppEntities[0].lvt_patients).to.equal(938);
    expect(qppEntities[0].lvt_small_status).to.equal(null);
    expect(qppEntities[0].lvt_performance_year).to.equal(2017);
    expect(qppEntities[0].entity_tin).to.equal('222000000');
    expect(qppEntities[0].apm_id).to.equal('76');
    expect(qppEntities[0].apm_name).to.equal('QP Metrics Model Seven Subdivs');
    expect(qppEntities[0].subdivision_id).to.equal('00');
    expect(qppEntities[0].advanced_apm_flag).to.equal('Y');
    expect(qppEntities[0].mips_apm_flag).to.equal('Y');
    const providerTins = qppEntities[0].tins.find(provider => provider.tin === '222000008');
    expect(providerTins.tin).to.equal('222000008');
    const providerNpis = providerTins.npis.find(provider => provider.npi === '1110000003');
    expect(providerNpis.npi).to.equal('1110000003');
    expect(providerNpis.qp_status).to.equal('Y');
    expect(providerNpis.qp_payment_threshold_score).to.equal(70);
    expect(providerNpis.qp_patient_threshold_score).to.equal(66);
    expect(providerNpis.provider_relationship_code).to.equal('P');
  });

  it('should create entity collection and populate', async () => {
    await CreateQppEntityCollection.create();
    const qppEntities = await QppEntity.find({ entity_id: 'entity-1' }).lean();
    expect(qppEntities[0].entity_id).to.equal('entity-1');
    expect(qppEntities[0].entity_name).to.equal('Entity 1');
    expect(qppEntities[0].lvt_flag).to.equal('F');
    expect(qppEntities[0].lvt_payments).to.equal(440959.1);
    expect(qppEntities[0].lvt_patients).to.equal(938);
    expect(qppEntities[0].lvt_small_status).to.equal(null);
    expect(qppEntities[0].lvt_performance_year).to.equal(2017);
    expect(qppEntities[0].entity_tin).to.equal('222000000');
    expect(qppEntities[0].apm_id).to.equal('76');
    expect(qppEntities[0].apm_name).to.equal('QP Metrics Model Seven Subdivs');
    expect(qppEntities[0].subdivision_id).to.equal('00');
    expect(qppEntities[0].advanced_apm_flag).to.equal('Y');
    expect(qppEntities[0].mips_apm_flag).to.equal('Y');
    const providerTins = qppEntities[0].tins.find(provider => provider.tin === '222000008');
    expect(providerTins.tin).to.equal('222000008');
    const providerNpis = providerTins.npis.find(provider => provider.npi === '1110000003');
    expect(providerNpis.npi).to.equal('1110000003');
    expect(providerNpis.qp_status).to.equal('Y');
    expect(providerNpis.qp_payment_threshold_score).to.equal(70);
    expect(providerNpis.qp_patient_threshold_score).to.equal(66);
    expect(providerNpis.provider_relationship_code).to.equal('P');
  });
});
