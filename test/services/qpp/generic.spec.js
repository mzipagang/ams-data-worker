const sinon = require('sinon');
require('sinon-mongoose');
const QppGeneric = require('../../../app/services/qpp/generic');
const QppRedis = require('../../../app/services/qppRedis');
const PacPublishHistoryUtils = require('../../../app/services/pacPublishHistoryUtils');
const chai = require('chai');

const expect = chai.expect;

const PacModelDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_model');
const PacSubdivisionDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_subdivision');
const PacEntityDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_entity');
const PacProviderDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_provider');
const PacLVTDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_lvt');
const PacQpStatusDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_status');
const PacQpScoreDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_score');

const DynamicNamePacModel = PacModelDynamic('1');
const DynamicNamePacSubdivision = PacSubdivisionDynamic('2');
const DynamicNamePacEntity = PacEntityDynamic('3');
const DynamicNamePacProvider = PacProviderDynamic('4');
const DynamicNamePacLVT = PacLVTDynamic('5');
const DynamicNamePacQpStatus = PacQpStatusDynamic('6');
const DynamicNamePacQpScore = PacQpScoreDynamic('7');

describe('Service: qpp/generic.js', () => {
  let PacPublishHistoryUtilsMock;
  let QppRedisMock;

  let DynamicNamePacModelMock;
  let DynamicNamePacSubdivisionMock;
  let DynamicNamePacEntityMock;
  let DynamicNamePacProviderMock;
  let DynamicNamePacLVTMock;
  let DynamicNamePacQpStatusMock;
  let DynamicNamePacQpScoreMock;

  beforeEach(() => {
    PacPublishHistoryUtilsMock = sinon.mock(PacPublishHistoryUtils);
    QppRedisMock = sinon.mock(QppRedis);

    DynamicNamePacModelMock = sinon.mock(DynamicNamePacModel);
    DynamicNamePacSubdivisionMock = sinon.mock(DynamicNamePacSubdivision);
    DynamicNamePacEntityMock = sinon.mock(DynamicNamePacEntity);
    DynamicNamePacProviderMock = sinon.mock(DynamicNamePacProvider);
    DynamicNamePacLVTMock = sinon.mock(DynamicNamePacLVT);
    DynamicNamePacQpStatusMock = sinon.mock(DynamicNamePacQpStatus);
    DynamicNamePacQpScoreMock = sinon.mock(DynamicNamePacQpScore);
  });

  afterEach(() => {
    PacPublishHistoryUtilsMock.restore();
    QppRedisMock.restore();

    DynamicNamePacModelMock.restore();
    DynamicNamePacSubdivisionMock.restore();
    DynamicNamePacEntityMock.restore();
    DynamicNamePacProviderMock.restore();
    DynamicNamePacLVTMock.restore();
    DynamicNamePacQpStatusMock.restore();
    DynamicNamePacQpScoreMock.restore();
  });

  it('should return an empty array when nothing has been published', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        allPACDataExists: false
      });

    const npis = [];
    const tins = [];
    const entityIds = ['A1001'];

    return QppGeneric(npis, tins, entityIds, null)
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(0);
      });
  });

  it('should return data loaded from mongo', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    DynamicNamePacProviderMock
      .expects('aggregate')
      .once()
      .resolves([
        {
          tin: '123456789',
          npi: '1234567890',
          qp_status: 'Y',
          entity_id: 'A1001'
        },
        {
          tin: '123456789',
          npi: '2345678902',
          qp_status: 'Y',
          entity_id: 'A1001',
          lvt_flag: 'Y',
          model_advanced_apm_flag: 'P',
          model_mips_apm_flag: 'P',
          subdivision_advanced_apm_flag: 'Y',
          subdivision_mips_apm_flag: 'Y'
        },
        {
          tin: '123456789',
          npi: '3456789012',
          qp_status: 'Y',
          lvt_flag: 'N'
        },
        {
          npi: '4567890123',
          qp_status: 'Y',
          entity_id: 'A1001',
          lvt_flag: 'A'
        },
        {
          tin: '123456789',
          entity_id: 'A1001',
          lvt_flag: 'B'
        }
      ]);

    // Should handle duplicates by removing them
    const npis = ['5678901234', '5678901234'];
    const tins = ['123456789', '123456789'];
    const entityIds = ['A1001', 'A1001'];

    return QppGeneric(npis, tins, entityIds)
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(5);
      });
  });

  it('should resolve an error when an error is thrown', () => {
    const error = new Error('Fake Error');

    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .throws(error);

    const npis = ['1234567890'];
    const tins = [];
    const entityIds = [];

    return QppGeneric(npis, tins, entityIds)
      .then(() => {
        throw new Error('Should not have gotten here');
      })
      .catch((err) => {
        if (err !== error) {
          throw err;
        }
      });
  });

  it('should use the provided query parameters in the mongo search', (done) => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    DynamicNamePacProviderMock
      .expects('aggregate')
      .once()
      .resolves([
        {
          tin: '123456789',
          npi: '1234567890',
          qp_status: 'Y',
          entity_id: 'A1001'
        }
      ]);

    const npis = ['1234567890'];
    const tins = [];
    const entityIds = [];

    const queryParams = {
      modelIds: ['01'],
      modelNames: ['Foo'],
      subdivisionIds: ['02'],
      subdivisionNames: ['Bar'],
      qpStatuses: ['X'],
      lvtFlags: ['Y'],
      smallStatusFlags: ['Z'],
      performanceYears: ['2017'],
      snapshots: ['Fizzbuzz']
    };

    QppGeneric(npis, tins, entityIds, 2017, null, queryParams)
      .then(() => {
        done();
      });
  });

  it('should throw an error if more results than limit are found', (done) => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    DynamicNamePacProviderMock
      .expects('aggregate')
      .once()
      .resolves(new Array(1001));

    const npis = ['1234567890'];
    const tins = [];
    const entityIds = [];

    const queryParams = {
      modelIds: ['01'],
      modelNames: ['Foo'],
      subdivisionIds: ['02'],
      subdivisionNames: ['Bar'],
      qpStatuses: ['X'],
      lvtFlags: ['Y'],
      smallStatusFlags: ['Z'],
      performanceYears: ['2017'],
      snapshots: ['Fizzbuzz']
    };

    QppGeneric(npis, tins, entityIds, 2017, null, queryParams)
      .then(() => done('Should have thrown an error'))
      .catch(() => {
        done();
      });
  });
});
