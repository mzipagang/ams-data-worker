const chai = require('chai');

const expect = chai.expect;
require('sinon-mongoose');

const util = require('../../../app/services/qpp/utility.js');

describe('QPP Utility', () => {
  before(async () => {});

  after(async () => {});

  describe('NPI Parsing', () => {
    it('It should accept a valid NPI', async () => {
      const result = util.parseTins('123456789');
      expect(result.goodTins.length).to.equal(1);
      expect(result.goodTins[0]).to.equal('123456789');
      expect(result.badTins.length).to.equal(0);
    });

    it('It should accept a valid NPI with whitespaces', async () => {
      const result = util.parseTins('    123456789    ');
      expect(result.goodTins.length).to.equal(1);
      expect(result.goodTins[0]).to.equal('123456789');
      expect(result.badTins.length).to.equal(0);
    });

    it('It should find an invalid NPI', async () => {
      const result = util.parseTins('1234');
      expect(result.goodTins.length).to.equal(0);
      expect(result.badTins[0]).to.equal('1234');
      expect(result.badTins.length).to.equal(1);
    });

    it('It should handle crazy', async () => {
      const result = util.parseTins('001025553,,001025553\\t001025553');
      expect(result.goodTins.length).to.equal(1);
      expect(result.badTins.length).to.equal(1);
    });

    it('It should drop empty strings', async () => {
      const result = util.parseTins('');
      expect(result.goodTins.length).to.equal(0);
      expect(result.badTins.length).to.equal(0);
    });


    it('It should take in an array also', async () => {
      const result = util.parseTins(['123456789', '987654321', 'asdf', '', '\tthis bljaoiddioa, asdfjoi']);
      expect(result.goodTins.length).to.equal(2);
      expect(result.badTins.length).to.equal(2);
    });
  });
});
