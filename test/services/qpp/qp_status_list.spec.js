const sinon = require('sinon');
require('sinon-mongoose');
const QppQpStatusList = require('../../../app/services/qpp/qp_status_list');
const QppRedis = require('../../../app/services/qppRedis');
const PacPublishHistoryUtils = require('../../../app/services/pacPublishHistoryUtils');
const chai = require('chai');

const expect = chai.expect;

const PacModelDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_model');
const PacSubdivisionDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_subdivision');
const PacEntityDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_entity');
const PacProviderDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_provider');
const PacLVTDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_lvt');
const PacQpStatusDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_status');
const PacQpScoreDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_score');

const DynamicNamePacModel = PacModelDynamic('1');
const DynamicNamePacSubdivision = PacSubdivisionDynamic('2');
const DynamicNamePacEntity = PacEntityDynamic('3');
const DynamicNamePacProvider = PacProviderDynamic('4');
const DynamicNamePacLVT = PacLVTDynamic('5');
const DynamicNamePacQpStatus = PacQpStatusDynamic('6');
const DynamicNamePacQpScore = PacQpScoreDynamic('7');

describe('Service: qpp/qp_status_list.js', () => {
  let PacPublishHistoryUtilsMock;
  let QppRedisMock;

  let DynamicNamePacModelMock;
  let DynamicNamePacSubdivisionMock;
  let DynamicNamePacEntityMock;
  let DynamicNamePacProviderMock;
  let DynamicNamePacLVTMock;
  let DynamicNamePacQpStatusMock;
  let DynamicNamePacQpScoreMock;

  beforeEach(() => {
    PacPublishHistoryUtilsMock = sinon.mock(PacPublishHistoryUtils);
    QppRedisMock = sinon.mock(QppRedis);

    DynamicNamePacModelMock = sinon.mock(DynamicNamePacModel);
    DynamicNamePacSubdivisionMock = sinon.mock(DynamicNamePacSubdivision);
    DynamicNamePacEntityMock = sinon.mock(DynamicNamePacEntity);
    DynamicNamePacProviderMock = sinon.mock(DynamicNamePacProvider);
    DynamicNamePacLVTMock = sinon.mock(DynamicNamePacLVT);
    DynamicNamePacQpStatusMock = sinon.mock(DynamicNamePacQpStatus);
    DynamicNamePacQpScoreMock = sinon.mock(DynamicNamePacQpScore);
  });

  afterEach(() => {
    PacPublishHistoryUtilsMock.restore();
    QppRedisMock.restore();

    DynamicNamePacModelMock.restore();
    DynamicNamePacSubdivisionMock.restore();
    DynamicNamePacEntityMock.restore();
    DynamicNamePacProviderMock.restore();
    DynamicNamePacLVTMock.restore();
    DynamicNamePacQpStatusMock.restore();
    DynamicNamePacQpScoreMock.restore();
  });

  it('should return an empty array when nothing has been published', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        allPACDataExists: false
      });

    return QppQpStatusList(null)
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result).to.deep.equal({
          performance_year: 2017,
          npis: [],
          snapshot: ''
        });
      });
  });

  it('should return data loaded from mongo', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    const value = [
      {
        npi: '1234567890',
        qp_status: 'Y',
        snapshot: 'P'
      }
    ];

    DynamicNamePacQpStatusMock
      .expects('aggregate')
      .chain('allowDiskUse')
      .once()
      .resolves(value);

    return QppQpStatusList(2018)
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result).to.deep.equal({
          performance_year: 2018,
          npis: value,
          snapshot: undefined
        });
      });
  });

  it('should resolve an error when an error is thrown', () => {
    const error = new Error('Fake Error');

    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .throws(error);

    return QppQpStatusList()
      .then(() => {
        throw new Error('Should not have gotten here');
      })
      .catch((err) => {
        if (err !== error) {
          throw err;
        }
      });
  });
});
