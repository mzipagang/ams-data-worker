const sinon = require('sinon');
require('sinon-mongoose');
const QppEntity = require('../../../app/services/qpp/entity');
const QppRedis = require('../../../app/services/qppRedis');
const PacPublishHistoryUtils = require('../../../app/services/pacPublishHistoryUtils');
const chai = require('chai');

const expect = chai.expect;

const PacModelDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_model');
const PacSubdivisionDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_subdivision');
const PacEntityDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_entity');
const PacProviderDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_provider');
const PacLVTDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_lvt');
const PacQpStatusDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_status');
const PacQpScoreDynamic = require('../../../app/services/mongoose/dynamic_name_models/pac_qp_score');

const DynamicNamePacModel = PacModelDynamic('1');
const DynamicNamePacSubdivision = PacSubdivisionDynamic('2');
const DynamicNamePacEntity = PacEntityDynamic('3');
const DynamicNamePacProvider = PacProviderDynamic('4');
const DynamicNamePacLVT = PacLVTDynamic('5');
const DynamicNamePacQpStatus = PacQpStatusDynamic('6');
const DynamicNamePacQpScore = PacQpScoreDynamic('7');

describe('Service: qpp/entity.js', () => {
  let PacPublishHistoryUtilsMock;
  let QppRedisMock;

  let DynamicNamePacModelMock;
  let DynamicNamePacSubdivisionMock;
  let DynamicNamePacEntityMock;
  let DynamicNamePacProviderMock;
  let DynamicNamePacLVTMock;
  let DynamicNamePacQpStatusMock;
  let DynamicNamePacQpScoreMock;

  beforeEach(() => {
    PacPublishHistoryUtilsMock = sinon.mock(PacPublishHistoryUtils);
    QppRedisMock = sinon.mock(QppRedis);

    DynamicNamePacModelMock = sinon.mock(DynamicNamePacModel);
    DynamicNamePacSubdivisionMock = sinon.mock(DynamicNamePacSubdivision);
    DynamicNamePacEntityMock = sinon.mock(DynamicNamePacEntity);
    DynamicNamePacProviderMock = sinon.mock(DynamicNamePacProvider);
    DynamicNamePacLVTMock = sinon.mock(DynamicNamePacLVT);
    DynamicNamePacQpStatusMock = sinon.mock(DynamicNamePacQpStatus);
    DynamicNamePacQpScoreMock = sinon.mock(DynamicNamePacQpScore);
  });

  afterEach(() => {
    PacPublishHistoryUtilsMock.restore();
    QppRedisMock.restore();

    DynamicNamePacModelMock.restore();
    DynamicNamePacSubdivisionMock.restore();
    DynamicNamePacEntityMock.restore();
    DynamicNamePacProviderMock.restore();
    DynamicNamePacLVTMock.restore();
    DynamicNamePacQpStatusMock.restore();
    DynamicNamePacQpScoreMock.restore();
  });

  it('should return an empty array when nothing has been published', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        allPACDataExists: false
      });

    return QppEntity(['A1001'], null)
      .then((result) => {
        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(0);
      });
  });

  it('should return data loaded from the cache', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: {},
        pacSubdivisionFile: {},
        pacEntityFile: {},
        pacProviderFile: {},
        pacLvtFile: {},
        pacQpStatusFile: {},
        pacQpScoreFile: {},
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    const cached = {
      entity_tin: '123456789',
      tins: [
        {
          tin: '123456789'
        }
      ]
    };

    QppRedisMock
      .expects('getByEntityId')
      .withExactArgs('A1001', '1', 2017)
      .once()
      .resolves(cached);

    return QppEntity(['A1001'])
      .then((result) => {
        const expectedResult = JSON.parse(JSON.stringify(cached));
        expectedResult.entity_tin = expectedResult.entity_tin;

        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(1);
        expect(result[0]).to.deep.equal(expectedResult);
      });
  });

  it('should return data loaded from mongo', () => {
    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .resolves({
        pacModelFile: { file_id: '1' },
        pacSubdivisionFile: { file_id: '2' },
        pacEntityFile: { file_id: '3' },
        pacProviderFile: { file_id: '4' },
        pacLvtFile: { file_id: '5' },
        pacQpStatusFile: { file_id: '6' },
        pacQpScoreFile: { file_id: '7' },
        allPACDataExists: true,
        publishHistory: { id: '1' }
      });

    QppRedisMock
      .expects('getByNpi')
      .withExactArgs('1234567890', '1', 2017)
      .once()
      .resolves(null);

    QppRedisMock
      .expects('setByNpi')
      .withArgs('1234567890', '1', 2017)
      .once()
      .resolves(null);

    DynamicNamePacEntityMock
      .expects('aggregate')
      .once()
      .resolves([
        {
          entity_id: 'A1001',
          entity_name: 'name',
          lvt_flag: 'Y',
          lvt_payments: 1,
          lvt_patients: 1,
          lvt_small_status: 2,
          lvt_performance_year: 2017,
          entity_tin: '1234567890',
          apm_id: '01',
          apm_name: 'name',
          subdivision_id: '01',
          subdivision_name: 'name',
          model_advanced_apm_flag: 'P',
          model_mips_apm_flag: 'P',
          subdivision_advanced_apm_flag: 'Y',
          subdivision_mips_apm_flag: 'Y'
        },
        {
          entity_id: 'A1002',
          entity_name: 'name',
          lvt_flag: 'N'
        },
        {
          entity_id: 'A1003',
          entity_name: 'name',
          lvt_flag: 'A',
          lvt_payments: 1,
          lvt_patients: 1,
          lvt_small_status: 2,
          lvt_performance_year: 2017,
          entity_tin: '1234567890',
          apm_id: '01',
          apm_name: 'name',
          subdivision_id: '01',
          subdivision_name: 'name',
          model_advanced_apm_flag: 'P',
          model_mips_apm_flag: 'P',
          subdivision_advanced_apm_flag: 'Y',
          subdivision_mips_apm_flag: 'Y'
        },
        {
          entity_id: 'A1004',
          entity_name: 'name',
          lvt_flag: 'B',
          lvt_payments: 1,
          lvt_patients: 1,
          lvt_small_status: 2,
          lvt_performance_year: 2017,
          entity_tin: '1234567890',
          apm_id: '01',
          apm_name: 'name',
          subdivision_id: '01',
          subdivision_name: 'name',
          model_advanced_apm_flag: 'N',
          model_mips_apm_flag: 'N',
          subdivision_advanced_apm_flag: 'N',
          subdivision_mips_apm_flag: 'N'
        },
        {
          entity_id: 'A1005',
          entity_name: 'name',
          lvt_flag: null,
          lvt_payments: 1,
          lvt_patients: 1,
          lvt_small_status: 2,
          lvt_performance_year: 2017,
          entity_tin: '1234567890',
          apm_id: '01',
          apm_name: 'name',
          subdivision_id: '01',
          subdivision_name: 'name',
          model_advanced_apm_flag: 'Y',
          model_mips_apm_flag: 'Y',
          subdivision_advanced_apm_flag: 'Y',
          subdivision_mips_apm_flag: 'Y'
        }
      ]);

    DynamicNamePacProviderMock
      .expects('aggregate')
      .once()
      .resolves([
        {
          entity_id: 'A1001',
          tin: '123456789',
          data: [
            {
              npi: '1234567890',
              provider_relationship_code: 'P',
              qp_status: 'Y',
              qp_payment_threshold_score: 1,
              qp_patient_threshold_score: 1
            }
          ]
        },
        {
          entity_id: 'A1002',
          tin: '123456789',
          data: [
            {
              npi: '1234567890'
            }
          ]
        },
        {
          entity_id: 'A1003',
          tin: '123456789',
          data: [
            {
              npi: '1234567890',
              provider_relationship_code: 'P',
              qp_status: 'Y',
              qp_payment_threshold_score: 1,
              qp_patient_threshold_score: 1
            }
          ]
        },
        {
          entity_id: 'A1004',
          tin: '123456789',
          data: [
            {
              npi: '1234567890',
              provider_relationship_code: 'P',
              qp_status: 'Y',
              qp_payment_threshold_score: 1,
              qp_patient_threshold_score: 1
            }
          ]
        },
        {
          entity_id: 'A1005',
          tin: '123456789',
          data: [
            {
              npi: '1234567890',
              provider_relationship_code: 'P',
              qp_status: 'Y',
              qp_payment_threshold_score: 1,
              qp_patient_threshold_score: 1
            }
          ]
        }
      ]);

    // Should handle duplicate entities by removing them
    return QppEntity(['A1001', 'A1001', 'A1002', 'A1002', 'A1003', 'A1004', 'A1005'])
      .then((result) => {
        // cached.tins.forEach(tinData => {
        //   tinData.tin = crypto.decrypt(tinData.tin) || null;
        //   tinData.entities.forEach((entityData) => {
        //     entityData.entity_tin = crypto.decrypt(entityData.entity_tin) || null;
        //   });
        // });


        expect(result).to.not.be.undefined;
        expect(result.length).to.equal(5);
      });
  });

  it('should resolve an error when an error is thrown', () => {
    const error = new Error('Fake Error');

    PacPublishHistoryUtilsMock
      .expects('getLatestPacFilesByPerformanceYear')
      .once()
      .throws(error);

    return QppEntity(['A1001'])
      .then(() => {
        throw new Error('Should not have gotten here');
      })
      .catch((err) => {
        if (err !== error) {
          throw err;
        }
      });
  });
});
