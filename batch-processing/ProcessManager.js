const pm2 = require('pm2');
const mongoose = require('mongoose');
const logger = require('../app/services/logger');
const JobQueueService = require('../app/services/JobQueueService');
const BatchJobFactory = require('./BatchJobFactory');
const {
  JOB_TOPIC,
  WORKER_WAITING_MESSAGE_TYPE,
  WORKER_ERROR_MESSAGE_TYPE,
  WORKER_COMPLETE_MESSAGE_TYPE,
  JOB_START_MESSAGE_TYPE
} = require('../app/constants').BATCH;
const batchConfig = require('./batchConfig');

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

/**
 * This is our programmatic version of the PM2 process manager, using PM2's npm package and API.
 * It is intended to manage a set of BatchWorker processes
 */
class ProcessManager {

  constructor() {
    this.waitingWorkers = new Set([]);
    this.workerJobMapping = {};
    this.workerTimeoutMapping = {};
    this.workerCount = 0;
  }

  /**
   * Starts this ProcessManager by:
   *  - Creating a message bus for parent/child communication
   *  - Registering handlers for incoming messages
   *  - Starting the configured number of BatchWorker processes
   * @returns {Promise}
   */
  async startProcessManager() {
    logger.info('Starting ProcessManager');

    await mongoose.connect(batchConfig.MONGOOSE.URI, {
      useNewUrlParser: true
    });

    await this.connect();
    logger.info('Successfully connected to PM2');

    // Start the message bus to listen to messages from workers
    this.messageBus = await this.launchBus();

    // Listen to the "job finished successfully" messages
    this.messageBus.on(WORKER_COMPLETE_MESSAGE_TYPE, (message) => {
      this.handleJobCompleteMessage(message);
    });

    // Listen to the "ready to work" messages
    this.messageBus.on(WORKER_WAITING_MESSAGE_TYPE, (message) => {
      this.handleWaitingMessage(message);
    });

    // Listen to error events
    this.messageBus.on(WORKER_ERROR_MESSAGE_TYPE, (message) => {
      this.handleErrorMessage(message);
    });

    logger.info('Message Bus initialized successfully');

    // Start a pre-configured number of BatchWorkers
    const workerCount = batchConfig.BATCH_WORKER_COUNT;
    const promises = [];
    logger.info(`Starting ${batchConfig.BATCH_WORKER_COUNT} BatchWorkers`);
    for (let i = 0; i < workerCount; i += 1) {
      promises.push(this.startBatchWorker());
    }
    await Promise.all(promises);
    logger.info('All workers started successfully');

    return this.list();
  }

  /**
   * Starts a single instance of the BatchWorker process
   * @returns {Promise<void>}
   */
  async startBatchWorker() {
    const options = { name: `Worker ${this.workerCount}` };
    this.workerCount += 1;
    return await this.startScript('batch-processing/startBatchWorker.js', options);
  }

  /**
   * Handle a message from child workers indicating that they are waiting for work
   * @param message the message from the BatchWorker
   * @returns {*} a promise with the result of {#identifyNextJobToWork}
   */
  handleWaitingMessage(message) {
    const { process } = message;
    const workerProcessId = process.pm_id;
    this.waitingWorkers.add(workerProcessId);
    logger.info(`Worker ${workerProcessId} is waiting for work`);
    return this.identifyNextJobToWork().catch((err) => {
      // TODO do we need to do any extra error handling here?
    });
  }

  /**
   * When a worker tells us its finished, we cancel its expiration, update our record of what the worker was doing,
   * add the worker back to the "waiting" queue, and try to find something else to work on
   * @param message
   */
  handleJobCompleteMessage(message) {
    const { data, process } = message;
    const { jobId } = data;
    const workerProcessId = process.pm_id;

    logger.info(`Worker ${workerProcessId} has finished job ${jobId}`);

    // Cancel the expiration timeout of this job
    this.cancelJobExpiration(jobId);

    // Update the mapping of workers => jobs
    delete this.workerJobMapping[workerProcessId]; // TODO should we make sure the old value points to what we expect it to?

    // Add the worker back to the waiting worker queue
    this.waitingWorkers.add(workerProcessId);

    // Find something else to work on
    this.identifyNextJobToWork(); // We can ignore the result of the promise here
  }

  /**
   * Handle an error message received from the BatchWorkers. Logs the event and then calls {@link terminateJob}
   * @param message error message from the BatchWorker
   * @returns {Promise<T | never>} the result of the {@link terminateJob} execution
   */
  handleErrorMessage(message) {
    const { data, process } = message;
    const { jobId, error } = data;
    const workerProcessId = process.pm_id;

    logger.error(`Worker ${workerProcessId} encountered an error executing job ${jobId}`);
    logger.error(error);

    return this.terminateJob(jobId, workerProcessId).then(() => {
      logger.info(`The error on job ${jobId} by worker ${workerProcessId} was handled gracefully`);
    }).catch((err) => {
      logger.error(`There was a problem handling the error on job ${jobId} by worker ${workerProcessId}`);
      logger.error(err);
    });
  }

  /**
   * This function is responsible for finding jobs that need to be executed.
   * If no workers are available, this function does nothing. As workers become available, they should call this method.
   * If workers are available, this will fetch the next job from Mongo and pop a worker off the waitingWorker queue.
   * If no jobs are available, this will set a timeout to check for new jobs in the future
   * @returns {Promise<void>}
   */
  async identifyNextJobToWork() {
    try {
      if (this.jobPollTimer) {
        clearTimeout(this.jobPollTimer);
        this.jobPollTimer = undefined;
      }

      // If there are no workers available, simply return. As workers become available, they should call this again
      if (this.waitingWorkers.size === 0) {
        return;
      }

      // Look for the next job to work via Mongo
      const jobDetails = await this.lockAndRetrieveJob();

      if (jobDetails) { // There's a job on the queue
        // Pop the oldest worker from the waiting queue
        const workerProcessId = this.waitingWorkers.values().next().value;
        this.waitingWorkers.delete(workerProcessId);

        // Send that worker a the job to execute
        await this.assignJobToWorker(jobDetails, workerProcessId);
      } else { // No jobs found on the queue
        this.jobPollTimer = setTimeout(() => {
          this.identifyNextJobToWork(); // TODO do we care about this promise?
        }, batchConfig.IDLE_POLL_DURATION);
      }
    } catch (err) {
      logger.error('Error identifying next job to execute');
      logger.error(err);
      throw err;
    }
  }

  /**
   * Tells a specified worker to start working on a given job
   * @param jobDetails the job to work
   * @param workerProcessId the worker doing the job
   * @returns {Promise<void>}
   */
  async assignJobToWorker(jobDetails, workerProcessId) {
    // update mapping of jobId => workerProcessId
    this.workerJobMapping[workerProcessId] = jobDetails; // TODO should we check if this worker already has an entry?

    // send our "START WORK" message to worker
    logger.info(`Assigning job ${jobDetails.id} to worker ${workerProcessId}`);
    await this.sendDataToProcessId(workerProcessId, JOB_START_MESSAGE_TYPE, { jobId: jobDetails.id }, JOB_TOPIC);

    // schedule an expiration time for this job
    this.scheduleJobExpiration(jobDetails, workerProcessId);
  }

  /**
   * Clears the timeout for the specified job
   * @param jobId the ID of the job whose timeout should be cleared
   */
  cancelJobExpiration(jobId) {
    // Look up timeoutRef based on jobId
    const timeoutRef = this.workerTimeoutMapping[jobId];
    if (!timeoutRef) {
      logger.warn(`Timeout ref for job ${jobId} is missing`);
      return;
    }

    clearTimeout(timeoutRef);
    delete this.workerTimeoutMapping[jobId];
  }

  /**
   * Schedules a timeout that will kill the specified job
   * @param jobDetails the job that will be terminated
   * @param workerProcessId the worker processing this job
   * @returns {function(): Promise<void | never>}
   */
  scheduleJobExpiration(jobDetails, workerProcessId) {
    const { id: jobId, type: jobType } = jobDetails;

    // Create an instance of the BatchJob type to get the timeout duration value
    const JobImplementationClass = BatchJobFactory.getJobImplementationByType(jobType);
    const jobInstance = new JobImplementationClass(jobDetails);
    const timeoutDuration = jobInstance.getJobTimeoutDuration();

    const terminateFunction = () => {
      logger.error(`Job ${jobId} on worker ${workerProcessId} has timed out and must be terminated`);
      return this.terminateJob(jobId, workerProcessId).catch((err) => {
        logger.error(`Error terminating stuck job ${jobId} on worker ${workerProcessId}`);
        logger.error(err);
      });
    };

    // Schedule termination of job after specified duration
    const timeoutRef = setTimeout(terminateFunction, timeoutDuration);

    // Save reference to timeout so we can cancel later
    this.workerTimeoutMapping[jobId] = timeoutRef;

    // Return the terminate function (for easier testing)
    return terminateFunction;
  }

  // TODO
  async terminateJob(jobId, workerProcessId) {
    this.cancelJobExpiration(jobId);

    const jobDetails = await JobQueueService.getJobById(jobId);

    // TODO there's kind of a race condition here: what if it JUST finished successfully?
    // TODO we should check the job's status. If it's "complete-success" and the worker is done... do we care?

    // Update the job status to error // TODO should we add more results to resultsError?
    await JobQueueService.updateJobError(jobId, jobDetails.resultsError);

    // TODO check jobDetails retries vs. retryLimit
    //     if (RETRY_ALLOWED)
    //       update mongo: retry ++, status = queued
    //     if (RETRY_EXCEEDED)
    //       update mongo: retry ++ status = error

    const killResult = await this.killWorker(workerProcessId);
    if (killResult) {
      logger.error(`Worker process ${workerProcessId} had to be terminated forcefully`);
    } else {
      logger.info(`Worker process ${workerProcessId} had terminated gracefully`);
    }

    // update mapping of workerProcessId => jobId
    delete this.workerJobMapping[workerProcessId]; // TODO should we make sure the old value points to what we expect it to?

    // Remove the worker from the list of waiting workers just in case
    this.waitingWorkers.delete(workerProcessId);

    logger.info('Starting replacement worker process');
    await this.startBatchWorker();
  }

  /**
   * Updates a specified job's status to "running" in Mongo, effectively locking it out from other workers.
   * Returns newly updated job if successful, otherwise returns null (if another worker locked it first)
   * @param jobId the ID of the job to lock/fetch
   * @returns {Promise<void>} a promise that resolves the QueuedJobs document if lock successful. Null if unsuccessful
   */
  async lockAndRetrieveJob(jobId) {
    return await JobQueueService.lockNextQueuedJob(jobId);
  }

  /**
   * A more convenient promise-based wrapper around PM2's connect function
   * @returns {Promise} that resolves true if connection succeeded
   */
  async connect() {
    return new Promise((resolve, reject) => {
      pm2.connect((err) => {
        if (err) {
          logger.error(err);
          reject(err);
        } else {
          resolve(true);
        }
      });
    });
  }

  /**
   * A simple pass-through to PM2's disconnect
   */
  disconnect() {
    pm2.disconnect();
  }

  /**
   * A more convenient promise-based wrapper around PM2's list function
   * @returns {Promise} that resolves the result of PM2's list function
   */
  async list() {
    return new Promise((resolve, reject) => {
      pm2.list((err, processDescriptionList) => {
        if (err) {
          logger.error(err);
          reject(err);
        } else {
          resolve(processDescriptionList);
        }
      });
    });
  }

  /**
   * A more convenient promise-based wrapper around PM2's start function
   * @param script the name of the script to be started (relative to the root of the project)
   * @param options the set of options to pass to PM2's start function
   * @returns {Promise} that resolves the processes started by PM2's start function
   */
  async startScript(script, options) {
    const defaultOptions = {
      script,
      waitReady: true
    };

    const jobOptions = Object.assign(defaultOptions, options);

    return new Promise((resolve, reject) => {
      pm2.start(jobOptions, (err, processes) => {
        if (err) {
          logger.error(err);
          reject(err);
        } else {
          if (!processes.length) {
            const msg = `Script "${ script }" not found`;
            logger.error(msg);
            reject(new Error(msg));
          } else {
            resolve(processes);
          }
        }
      });
    });
  }

  /**
   * A more convenient, promise-based wrapper around PM2's sendDataToProcessId function.
   * This is used to send messages to child processes
   * @param workerId the pm_id of the child process
   * @param type a parameter describing the type of request
   * @param data an arbitrary data object to pass to the child
   * @param topic a parameter describing the topic of the request
   * @returns {Promise} that resolves the result of PM2's sendDataToProcessId
   */
  async sendDataToProcessId(workerId, type, data, topic) {
    const payload = {
      type,
      data,
      topic
    };
    return new Promise((resolve, reject) => {
      pm2.sendDataToProcessId(workerId, payload, (err, res) => {
        if (err) {
          logger.error(err);
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
  }

  /**
   * A more convenient, promise-based wrapper around PM2's launchBus function.
   * This opens an event bus that can receive messages from child processes
   * @returns {Promise} that resolves the event bus you can subscribe to
   */
  async launchBus() {
    return new Promise((resolve, reject) => {
      pm2.launchBus((err, bus) => {
        if (err) {
          logger.error(err);
          reject(err);
        } else {
          resolve(bus);
        }
      });
    });
  }

  /**
   * Stops a worker process using PM2's delete functionality
   * @param workerProcessId the PM2 ID of the worker to delete
   * @returns {Promise} that resovles true if successful, or an error if not
   */
  async killWorker(workerProcessId) {
    return new Promise((resolve, reject) => {
      pm2.delete(workerProcessId, (err) => {
        if (err) {
          if (err.message === 'Process not found') {
            resolve(false);
          } else {
            logger.error(err);
            reject(err);
          }
        } else {
          resolve(true);
        }
      });
    });
  }
}

module.exports = ProcessManager;
