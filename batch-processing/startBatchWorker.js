const BatchWorker = require('./BatchWorker');
const batchConfig = require('./batchConfig');

const worker = new BatchWorker(batchConfig);
worker.startWorker();
