const mongoose = require('mongoose');
const logger = require('../app/services/logger');
const BatchJobFactory = require('./BatchJobFactory');
const {
  WORKER_READY_MESSAGE_TYPE,
  WORKER_ERROR_MESSAGE_TYPE,
  WORKER_WAITING_MESSAGE_TYPE,
  WORKER_COMPLETE_MESSAGE_TYPE,
  JOB_START_MESSAGE_TYPE
} = require('../app/constants').BATCH;

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

/**
 * Individual process managed by PM2/ProcessManager
 */
class BatchWorker {
  /**
   * Pass the config via the constructor. The config needs the follow properties:
   *  - MONGOOSE.URI
   * @param config
   */
  constructor(config) {
    this.config = config;
  }

  /**
   * Starts the batch worker by:
   *  - connecting to MongoDB
   *  - listening to messages from PM2 daemon and delegating to {@link handleMessage}
   *  - listen to SIGINT messages for graceful shutdown
   *  - retrieve the job information from Mongo
   *  - send "ready" message to PM2
   * @returns {Promise<void>} a promise that resolves if successful, and rejects if an error occurs
   */
  async startWorker() {
    try {
      // connect to mongo
      await mongoose.connect(
        this.config.MONGOOSE.URI,
        { useNewUrlParser: true,
          connectTimeoutMS: 10 * 1000, // Give up initial connection after 10 seconds
          socketTimeoutMS: 20 * 60 * 1000 // Close sockets after 20 minutes of inactivity
        }
      );

      // Listen for messages from ProcessManager
      process.on('message', (packet) => {
        this.handleMessage(packet);
      });

      // Listen for "graceful shutdown" message
      process.on('SIGINT', () => {
        // killWorker() is aysnc, but it calls process.exit(), so no need to handle promise
        // Also, we can't tell here if we're being told to shut down because of an error or not.
        // So we exit with code 0, assuming it's not an error
        this.killWorker();
      });

      // Tell our ProcessManager we're waiting for work
      this.sendWaitingMessageToProcessManager();

      // Tell PM2 system we're starting (best practice, evidently)
      process.send(WORKER_READY_MESSAGE_TYPE);
    } catch (err) {
      this.handleError(err, null);
    }
  }

  /**
   * Executes the specified job
   * @param jobId the ID of the job to execute
   * @returns {Promise<void>} a promise that resolves if successful, and rejects if an error occurs // TODO resolve value?
   */
  async executeJob(jobId) {
    try {
      const jobInstance = await BatchJobFactory.createJobInstance(jobId);

      // TODO register the jobInstance to handle messages (batchJob.handleMessage). This might let the job handle errors

      await jobInstance.executeJob();

      // TODO update Mongo inside the jobInstance, which has specific details of the result
      // Or maybe call jobInstance.markCompleted (or whatever) here, leaving implementation details to BatchJob subclass

      // Let the ProcessManager know we've completed the job
      this.sendCompletedMessageToProcessManager(jobId);
    } catch (err) {
      this.handleError(err, jobId);
    }
  }

  handleError(err, jobId) {
    if (jobId) {
      logger.error(`BatchWorker encountered an error. Current Job ID: ${jobId}`);
    } else {
      logger.error('BatchWorker encountered an error. Not currently executing a job');
    }
    logger.error(err);

    // Notify the ProcessManager
    process.send({
      type: WORKER_ERROR_MESSAGE_TYPE,
      data: {
        error: err.message,
        jobId
      }
    });
    this.killWorker(1); // it's ok to ignore the promise result here, as the process always exits
  }

  /**
   * Kills this worker process. Disconnects from mongo first. Always exits the process
   * @returns {Promise<void>}
   */
  async killWorker(status = 0) {
    try {
      logger.info('Killing data worker process');
      await mongoose.disconnect();
    } catch (e) {
      status = 1;
      throw e;
    } finally {
      process.exit(status);
    }
  }

  /**
   * Let's the ProcessManager know we're waiting for work
   */
  sendWaitingMessageToProcessManager() {
    process.send({
      type: WORKER_WAITING_MESSAGE_TYPE,
      data: {
        success: true
      }
    });
  }

  /**
   * Let's the ProcessManager know we've finished our job
   */
  sendCompletedMessageToProcessManager(jobId) {
    process.send({
      type: WORKER_COMPLETE_MESSAGE_TYPE,
      data: {
        jobId
      }
    });
  }

  /**
   * This listens to and handles all messages received from the ProcessManager
   * @param message the message received by the PM2 daemon (ProcessManager)
   */
  handleMessage(message) {
    const { type, data, topic } = message; // TODO clean up message types/topics
    if (type === JOB_START_MESSAGE_TYPE) {
      const { jobId } = data;
      // Execute the specified job. Missing jobId validated in BatchJobFactory
      // It's ok to ignore the resulting promise: no action is needed on success, and an error will kill this process
      this.executeJob(jobId); // It's ok to ignore the promise result here.
    } else {
      logger.warn(`BatchWorker is unable to handle message of type "${message.type}"`);
    }
  }
}

module.exports = BatchWorker;
