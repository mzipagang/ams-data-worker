const JobQueueService = require('../app/services/JobQueueService');
const batchConfig = require('./batchConfig');

/**
 * This is a factory which looks up a job's details in Mongo by its unique ID, then looks up the job implementation
 * for that job's type. Then it returns an new job instance of that job type
 */
class BatchJobFactory {
  static async createJobInstance(jobId) {
    if (!jobId) {
      throw new Error('Job Id is required');
    }

    // Fetch job details from Mongo
    const jobDetails = await JobQueueService.getJobById(jobId);
    if (!jobDetails) {
      throw new Error(`Cannot find job with ID "${jobId}"`);
    }

    // Fetch the job implementation based on its type
    const BatchJobType = BatchJobFactory.getJobImplementationByType(jobDetails.type);

    // Create an instance of our job type and return it
    return new BatchJobType(jobDetails);
  }

  /**
   * Fetch the job implementation based on its type
   * @param type the type of job whose implementation is being fetched
   */
  static getJobImplementationByType(type) {
    const BatchJobType = batchConfig.JOBS[type];
    if (!BatchJobType) {
      throw new Error(`Cannot find job definition for job type "${type}"`);
    }
    return BatchJobType;
  }
}

module.exports = BatchJobFactory;
