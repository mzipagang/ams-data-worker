const BatchJob = require('../BatchJob');
const JobQueueService = require('../../app/services/JobQueueService');
const createQppProviderCollection = require('../../app/services/qpp/qppProviderCollection.js');
const logger = require('../../app/services/logger');

class Job extends BatchJob {
  async executeJob() {
    const { parameters, id } = this.jobDetails;
    const { performanceYear } = parameters;

    logger.info(`Starting update of QPP Provider Collection for performance year ${performanceYear}`);
    await createQppProviderCollection.updateCollection(performanceYear);
    logger.info(`Finish update of QPP Provider Collection for performance year ${performanceYear}`);

    const jobDetails = await JobQueueService.updateJobStatusToCompleted(id);
    return jobDetails;
  }

  getJobTimeoutDuration() { // eslint-disable-line class-methods-use-this
    return 60 * 60 * 1000; // 1 Hour
  }
}

module.exports = Job;
