const config = require('../../app/config');
const BatchJob = require('../BatchJob');
const { util } = require('../../app/services/aws');
const { getEntities } = require('../../app/services/qpp');
const JobQueueService = require('../../app/services/JobQueueService');
const { Readable } = require('stream');

class Job extends BatchJob {
  async executeJob() {
    const { parameters, id } = this.jobDetails;
    const { performanceYear } = parameters;
    const s3Bucket = config.AWS.JOB_S3_BUCKET;
    const s3FileName = `qpp_entities${performanceYear}.json`;
    const entities = await getEntities(performanceYear);

    const queryStream = new Readable();
    queryStream.push(JSON.stringify(entities));
    queryStream.push(null);

    await util.uploadS3(s3FileName, queryStream, 'application/json', {}, s3Bucket);

    const signedUrlConfirmationId =
      await util.getSignedUrl(s3FileName, config.AWS.SIGNED_URL_TIMEOUT_IN_SECONDS, s3Bucket);

    const jobDetails = await JobQueueService.updateJobStatusToCompleted(id, { signedUrlConfirmationId });
    return jobDetails;
  }

  getJobTimeoutDuration() { // eslint-disable-line class-methods-use-this
    return 60000; // 1 minute
  }
}

module.exports = Job;
