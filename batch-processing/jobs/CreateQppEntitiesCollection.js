const BatchJob = require('../BatchJob');
const JobQueueService = require('../../app/services/JobQueueService');
const createQppEntityCollection = require('../../app/services/qpp/createQppEntityCollection');

class Job extends BatchJob {
  async executeJob() {
    const { id } = this.jobDetails;
    return new Promise(async (resolve, reject) => {
      try {
        await createQppEntityCollection.create();
        const jobDetails = await JobQueueService.updateJobStatusToCompleted(id);
        resolve(jobDetails);
      } catch (err) {
        reject(err);
      }
    });
  }

  getJobTimeoutDuration() {
    return 300000; // 5 minutes
  }
}

module.exports = Job;
