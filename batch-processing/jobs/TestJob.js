const BatchJob = require('../BatchJob');
const JobQueueService = require('../../app/services/JobQueueService');

/**
 * This is a simple batch job for testing purposes. It doesn't do anything useful or interesting.
 * At some point we may want to remove it from the code base
 */
/* istanbul ignore next */
class TestJob extends BatchJob {
  async executeJob() {
    const { parameters, id } = this.jobDetails;
    const duration = (parameters && parameters.duration) ? parameters.duration : 2000;
    const shouldError = (parameters && parameters.shouldError === true) ? parameters.shouldError : false;

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (shouldError) {
          reject(new Error('Something went wrong on purpose'));
        } else {
          JobQueueService.updateJobStatusToCompleted(id).then((jobDetails) => {
            resolve(jobDetails);
          }).catch((err) => {
            reject(err);
          });
        }
      }, duration);
    });
  }

  getJobTimeoutDuration() {
    return this.jobDetails.parameters && this.jobDetails.parameters.timeoutDuration || 3000;
  }
}

module.exports = TestJob;
