/**
 * This is a base class that represents the unit of work that must be done.
 * Child classes would be specific to the type of job being performed.
 */
class BatchJob {
  constructor(jobDetails) {
    this.jobDetails = jobDetails;
  }

  async executeJob() {
    throw new Error('BatchJob subclasses must implement an executeJob method');
  }

  getJobTimeoutDuration() {
    throw new Error('BatchJob subclasses must implement a getJobTimeoutDuration method');
  }
}

module.exports = BatchJob;
