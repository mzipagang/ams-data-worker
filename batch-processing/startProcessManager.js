require('dotenv').config({ path: require('path').join(__dirname, '../.env') });
const ProcessManager = require('./ProcessManager');
const logger = require('../app/services/logger');

const processManager = new ProcessManager();
processManager.startProcessManager().then(() => {
  logger.info('ProcessManager started successfully');
}).catch((err) => {
  logger.error('Error starting ProcessManager');
  logger.error(err);
});
