const constants = require('../app/constants');
const TestJob = require('./jobs/TestJob');
const StreamBulkEntityDataToS3 = require('./jobs/StreamBulkEntityDataToS3');
const CreateQppEntitiesCollection = require('./jobs/CreateQppEntitiesCollection');
const UpdateQppProvidersCollection = require('./jobs/UpdateQppProviderCollection.js');

// This is to contain a mapping of job types to the corresponding BatchJob subclass

module.exports = {
  MONGOOSE: {
    URI: process.env.MONGODB || 'mongodb://localhost:20017/ams-api'
  },
  BATCH_WORKER_COUNT: process.env.BATCH_WORKER_COUNT || 2,
  IDLE_POLL_DURATION: process.env.IDLE_POLL_DURATION || 2000,
  JOBS: {
    [constants.BATCH.JOB_TYPES.TEST_JOB]: TestJob,
    [constants.BATCH.JOB_TYPES.STREAM_BULK_ENTITY_DATA_TO_S3]: StreamBulkEntityDataToS3,
    [constants.BATCH.JOB_TYPES.CREATE_QPP_ENTITIES_COLLECTION]: CreateQppEntitiesCollection,
    [constants.BATCH.JOB_TYPES.UPDATE_QPP_PROVIDER_COLLECTION]: UpdateQppProvidersCollection
  }
};
